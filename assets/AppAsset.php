<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/style.css',
        'css/scales.css',
		'css/toggle.css',
		'css/switch.css'
    ];
    public $js = [
        
        'js/autoSave.js',
        'js/reveal.js',
        'jsControllers/appController.js',
         'jsControllers/motherController.js',
        'js/bootstrap-datepicker.js',
        'js/bootstrap-datepicker.min.js',
		'js/toggle-switch.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'yii\angularjs\AngularAsset'	
    ];
}
