<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ParentChild */
/* @var $form yii\widgets\ActiveForm */
Yii::$app->cache->set('motherId',$motherId);
?>

<div class="parent-child-form">

    <?php $form = ActiveForm::begin(['id' => 'formParentChild']); ?>
    <div id="withBoxShadow" class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
	    <div class="row">
		    <div class="col-lg-4 col-md-4">
			    <?= $form->field($model, 'child_name')->textInput(['maxlength' => true]) ?>
		    </div>
		    <!-- add last date visit -->
	    </div>
	    <div class="row">
		    <div class="col-lg-4 col-md-4">
			    <?= $form->field($model, 'father_name')->textInput(['maxlength' => true]) ?>
		    </div>
		    <!--father id and last date of visit-->
	    </div>
	    <div class="row">
		    <div class="col-lg-4 col-md-4">
			    <?= $form->field($model, 'care_taker')->textInput(['maxlength' => true]) ?>
		    </div>
		    <!--add caretaker id last visit date-->
	    </div>	
    </div>	

    <?php ActiveForm::end(); ?>

</div>
<?php
     $this->registerJs('
	$("#formParentChild").change(function(){
		autoSave({url : "parent-child/ajax",formName : "formParentChild"});	
	});		
')
?>
