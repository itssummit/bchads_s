<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ParentChild */

/*$this->title = 'Create Parent Child';
$this->params['breadcrumbs'][] = ['label' => 'Parent Children', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/
?>
<div class="parent-child-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form',['model' => $model,'motherId' => $motherId]) ?>

</div>
