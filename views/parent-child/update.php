<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ParentChild */

$this->title = 'Update Parent Child: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Parent Children', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="parent-child-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
