<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ChildGrowth */

$this->title = 'Create Child Growth';
$this->params['breadcrumbs'][] = ['label' => 'Child Growths', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="child-growth-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
