<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ChildGrowth */
/* @var $form yii\widgets\ActiveForm */
?>

<div  id="withBoxShadow"  class="child-growth-form">

    <?php $form = ActiveForm::begin(); ?>

    <table>
	<thead>
	<th></th>
	<th>measurements</th>
	<th>8weeks</th>
	<th>6 months</th>
	<th>1 year</th>
	<th>2 year</th>
	</thead>
	<tbody>
	<tr>
	<td></td>
	<td><?= $form->field($model, 'length')->textInput(['maxlength' => true]) ?></td>
	</tr>
	</tbody>
	</table>
    

    <?= $form->field($model, 'weight')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'head_circumference')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'chest_circumference')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mid_arm_circumference')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'waist_circumference')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hip_circumference')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sub_capular_skin_fold_thickness')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
