<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$values = [0 => 'ಚೂಪಾದ ವಸ್ತುಗಳನ್ನು ಬಳಸಿರುವುದು (ಉದಾ: ಕುಯ್ದುಕೊಳ್ಳುವುದು , ಕತ್ತರಿಸಿಕೊಳ್ಳುವುದು ) ',1 =>'ನೇಣು ಹಾಕಿಕೊಳ್ಳುವುದು',2 =>'ಎತ್ತರದ ಜಾಗದಿಂದ ಬೀಳುವುದು' ,3 =>'ಔಷಧಿಗಳನ್ನು ಅಗತ್ಯಕ್ಕಿಂತ ಹೆಚ್ಚಾಗಿ ತೆಗೆದುಕೊಳ್ಳುವುದು',4 =>'ರಾಸಾಯನಿಕ ಮಾದಕ ವಸ್ತುಗಳನ್ನು ತೆಗೆದುಕೊಳ್ಳುವುದು (ಔಷಧಿಗಳನ್ನು ಹೊರತುಪಡಿಸಿ)',5=>' ಇತರೆ '];
$model->method_attempt=($model->method_attempt)?explode(',',$model->method_attempt):'';
$keyPair = [-15 => '-15: Child Not Co-Operative For Assessment',
		  -14 => '-14: Not Available For Assessment',
		  -11 => '-11: Inadequate Information',
		  -10 => '-10: Not Applicable',
		  -9 => '-9: Missing',
		  -8 => '-8: Refused',
		  -7 => '-7: Partner Accompanied Missing',
		  -6 => '-6: Family Accompanied Missing',
		  -5 => '-5: Missing:Not Asked',
		  -3 => '-3: No Such Relationship/No Partner'];
/* @var $this yii\web\View */
/* @var $model app\models\SuicideQuestions */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile('@web/js/scalesome.js',['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<style>
#suicidequestions-not_applicable{
	width:250px !important;
}
</style>
<div class="suicide-questions-form">

    <?php $form = ActiveForm::begin(['id'=>'formSuicidequestions']); ?>
    <div class="col-lg-4 pull-right">
		<h4><b>Is This Applicable?</b></h4>
		<?php echo $form->field($model, 'not_applicable')->dropDownList(['' =>'','YES' => 'Not Applicable', 'NO' => 'Applicable'])->label(false); ?>
		</div>
	
	<div id="withBoxShadow" class="col-xs-12 col-md-12 col-lg-12 col-sm-12">
		<!--<div class="col-lg-4 pull-right">
		<b>Reset</b>< ?php echo $form->field($model, 'not_applicable')->checkbox(['onclick'=>'change("ideas")'])->label(false);?>
		</div>-->
		
		
		
		
		
		<div class="col-lg-12 idform">
			<label>1. ನಿಮ್ಮ ಜೀವನದಲ್ಲಿ ಯಾವ ಸಮಯದಲ್ಲಾದರೂ  ನಿಮ್ಮ ಜೀವನವನ್ನು ಕೊನೆ ಮಾಡಿಕೊಳ್ಳುವ ಆಲೋಚನೆ  ಬಂದಿತ್ತೇ  ?</label>
		</div>
		
		<div class="col-lg-12">
			<div class="col-lg-8 idform">
				<?= $form->field($model, 'ideas')->radioList([0=> 'ಇಲ್ಲವೇ ಇಲ್ಲ' ,1=> ' ಅಪರೂಪವಾಗಿ(ಒಂದು ಬಾರಿ)',2=>' ಕೆಲವೊಮ್ಮೆ(2-ಬಾರಿ) ',3=>' ಪದೇ ಪದೇ(3-4 ಬಾರಿ)',4=> ' ಬಹಳಷ್ಟು ಬಾರಿ (5-6 ಬಾರಿ)'],['itemOptions'=>['class' => "ideas"]])->label(false);?>
			</div>
			<div class="col-lg-4">
				<?= $form->field($model, 'ideas')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
			</div>
			
		</div>
		
		<div class="col-lg-12" id="second" style = "display: <?php echo($model->ideas >= 1)?"display":"none"; ?> ">
			<div class="col-lg-12 idform">
				<label>2.  ನಿಮ್ಮ ಜೀವನದಲ್ಲಿ ಯಾವಾಗಲಾದರೂ  ನಿಮ್ಮ ಜೀವನವನ್ನು ಕೊನೆ ಮಾಡಿಕೊಳ್ಳುವ ಉದ್ದೇಶಿಸಿದ್ದೀರಾ /ಯೋಜಿಸಿದ್ದೀರಾ  ಅಥವಾ ನಿಮಗೆ ನೀವೇ ಹಾನಿ ಮಾಡಿಕೊಳ್ಳಲು ಪ್ರಯತ್ನಿಸಿದ್ದೀರಾ ?</label>
			</div>
		
			<div class="col-lg-12" >
				<div class="col-lg-8 idform">
					<?= $form->field($model, 'plan_attempted')->radioList([0=>  'ಇಲ್ಲವೇ ಇಲ್ಲ.'  ,1=>  ' ಒಮ್ಮೆ ನನ್ನ ಜೀವನವನ್ನು ಕೊನೆಗಾಣಿಸಿಕೊಳ್ಳಲು ಯೋಜಿಸಿದ್ದೆ ಆದರೆ  ಪ್ರಯತ್ನಿಸಿರಲಿಲ್ಲ..'  ,2=> ' ಒಮ್ಮೆ ನನ್ನ ಜೀವನವನ್ನು ಕೊನೆಗಾಣಿಸಿಕೊಳ್ಳಲು ಯೋಜಿಸಿದ್ದೆ ಮತ್ತು  ಖಂಡಿತವಾಗಿಯೂ ಸಾಯಬೇಕೆನಿಸಿತು.'  ,3=>  'ನನ್ನ ಜೀವನವನ್ನು ಕೊನೆ ಮಾಡಿಕೊಳ್ಳಲು ಪ್ರಯತ್ನಿಸಿದ್ದೆ ಆದರೆ   ಸಾಯಬೇಕು ಅನಿಸಿರಲಿಲ್ಲ. ' ,4=>  ' ನನ್ನ ಜೀವನವನ್ನು ಕೊನೆ ಮಾಡಿಕೊಳ್ಳಲು ಪ್ರಯತ್ನಿಸಿದ್ದೆ ಮತ್ತು  ಖಂಡಿತವಾಗಿಯೂ ಸಾಯಲೇ ಬೇಕೆನಿಸಿತು.. ' ],['itemOptions'=>['class' => "plan_attempted"]])->label(false);?>
				</div>
				
				<div class="col-lg-4">
					<?= $form->field($model, 'plan_attempted')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
				</div>
			</div>
		</div>
		
		<div class="col-lg-12 idform">
		<label>3. ನೀವು ಪ್ರಸ್ತುತ ಗರ್ಭಿಣಿ ಯಾಗಿರುವಾಗ, ನಿಮ್ಮ ಜೀವನವನ್ನು ಕೊನೆ ಮಾಡಿಕೊಳ್ಳಲೇಬೇಕೆಂದು ಯಾವಾಗಲಾದರೂ ಯೋಚಿಸಿದ್ದೀರಾ ?</label>
		</div>
		
		<div class="col-lg-12">
			<div class="col-lg-8 idform">
				<?= $form->field($model, 'current_pregnancy_thought_ending_life')->radioList([0=>  'ಇಲ್ಲವೇ ಇಲ್ಲ' ,1=>'  ಅಪರೂಪವಾಗಿ  (ಒಂದು ಬಾರಿ )' , 2=> ' ಕೆಲವೊಮ್ಮೆ (2 ಬಾರಿ )'  ,3=>' ಪದೇ ಪದೇ (3-4 ಬಾರಿ)',4=>'ಬಹಳಷ್ಟು ಬಾರಿ  (5  ಅಥವಾ ಹೆಚ್ಚಿನ ಬಾರಿ)'],['itemOptions'=>['class' => "current_pregnancy_thought_ending_life"]])->label(false);?>
			</div>
			
			<div class="col-lg-4">
				<?= $form->field($model, 'current_pregnancy_thought_ending_life')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
				
			</div>
		</div>	
	
	<div class="col-lg-12" id="fourth" style = "display:<?php echo ($model->current_pregnancy_thought_ending_life >= 1)?"display":"none";?>">	
		<div class="col-lg-12 idform">
			<label>4. ಇದರ ಬಗ್ಗೆ ಹೇಗೆ ಮುಂದುವರಿಯಬಹುದೆಂದು ಏನಾದರೂ ಯೋಜನೆಗಳನ್ನು ಮಾಡಿಕೊಂಡಿದ್ದೀರಾ? </label>
		</div>
	
		<div class="col-lg-12">
			<div class="col-lg-8 idform">
				<?= $form->field($model, 'current_pregnancy_plan')->radioList([0=>' ಇಲ್ಲವೇ ಇಲ್ಲ ',1=>' ಒಮ್ಮೆ ನನ್ನ ಜೀವನವನ್ನು ಕೊನೆಗಾಣಿಸಿಕೊಳ್ಳಲು ಯೋಜಿಸಿದ್ದೆ ಆದರೆ ಪ್ರಯತ್ನಿಸಿರಲಿಲ್ಲ',2=>' ಒಮ್ಮೆ ನನ್ನ ಜೀವನವನ್ನು ಕೊನೆಗಾಣಿಸಿಕೊಳ್ಳಲು ಯೋಜಿಸಿದ್ದೆ ಮತ್ತು ಖಂಡಿತವಾಗಿಯೂ ಸಾಯಬೇಕೆನಿಸಿತ್ತು.'],['itemOptions'=>['class' => "current_pregnancy_plan"]])->label(false);?>
			</div>
			
			<div class="col-lg-4">
				<?= $form->field($model, 'current_pregnancy_plan')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
			</div>
		</div>	
	
		<div class="col-lg-12 idform">
			<label>5. ಈಗ ನೀವು ಪ್ರಸ್ತುತ ಗರ್ಭಿಣಿಯಾಗಿರುವಾಗ ಯಾವಾಗಲಾದರೂ ನಿಮ್ಮನ್ನು ನೀವು  ಹಾನಿ ಮಾಡಿಕೊಳ್ಳಬೇಕೆಂದು ಪ್ರಯತ್ನಿಸಿದ್ದೀರಾ? </label>
		</div>
	
		<div class="col-lg-12">
			<div class="col-lg-8 idform">
				<?= $form->field($model, 'current_pregnancy_attempt')->radioList([0=>' ಇಲ್ಲವೇ ಇಲ್ಲ' ,1=>' ನನ್ನ  ಜೀವನವನ್ನು ಕೊನೆ ಮಾಡಿಕೊಳ್ಳಲು  ಆದರೆ ಸಾಯಬೇಕು ಅನಿಸಲಿಲ್ಲ ',2=>' ನನ್ನ ಜೀವನವನ್ನು ಕೊನೆ ಮಾಡಿಕೊಳ್ಳಲು ಪ್ರಯತ್ನಿಸಿದ್ದೆ ಮತ್ತು ಖಂಡಿತವಾಗಿಯೂ ಸಾಯಲೇ ಬೇಕೆನಿಸಿತ್ತು'],['itemOptions'=>['class' => "current_pregnancy_attempt"]])->label(false);?>
			</div>
			
			<div class="col-lg-4">
				<?= $form->field($model, 'current_pregnancy_attempt')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
			</div>
		</div>	
	</div>	
	<div class="col-lg-12" id="seventh" style = "display:<?php echo ($model->current_pregnancy_attempt >= 1) || ($model->plan_attempted >= 1)?"display":"none";?>">
		<div class="col-lg-12 idform">
			<h4><label>ಈ ಪ್ರಯತ್ನಗಳನ್ನು ಮಾಡಿರುವುದರ ಬಗ್ಗೆ ಇನ್ನೂ ಸ್ವಲ್ಪ ಹೆಚ್ಚಾಗಿ  ಹೇಳುತ್ತೀರಾ ?<label><h4>
		</div>
		
		<div class="col-lg-12 idform">
			<label>6. ಈಗ  ಪ್ರಸ್ತುತ  ಗರ್ಭಿಣಿಯಾಗಿರುವಾಗ  , ನಿಮ್ಮನ್ನು ನೀವು ಎಷ್ಟು ಬಾರಿ   ಹಾನಿ ಮಾಡಿಕೊಳ್ಳಲು  ಪ್ರಯತ್ನಿಸಿದ್ದೀರಾ?</label>
		
			<?= $form->field($model, 'times_attempted')->textInput(['itemOptions'=>['class' => "times_attempted"]])->label(false); ?>
		</div>
	
	
		<div class="col-lg-12 idform">
			<label>7. ನಿಮ್ಮ ಜೀವನವನ್ನು ಯಾವ ಕಾರಣಕ್ಕಾಗಿ ಕೊನೆ ಮಾಡಿಕೊಳ್ಳಬೇಕೆಂದು ಆಲೋಚಿಸಿದ್ದಿರಿ ಅಥವಾ ಪ್ರಯತ್ನಿಸಿದ್ದಿರಿ?</label>
		
			<?= $form->field($model, 'reasons_attempt')->textInput(['itemOptions'=>['class' => "reasons_attempt"]])->label(false); ?>
		</div>
		<div class="col-lg-12 idform">
			<label>8. ನಿಮ್ಮನ್ನು ನೀವು ಹಾನಿ ಮಾಡಿಕೊಳ್ಳಲು ಯಾವ ವಿಧಾನವನ್ನು ಬಳಸಿದ್ದೀರಾ?</label>
		</div>
		
		<div class="col-lg-12">
			<div class="col-lg-8">
			<?= $form->field($model, 'method_attempt')->checkboxList($values,
				['item'=> function($index,$label,$name,$checked,$value){
						return "<td>".Html::checkbox($name,$checked,['value' => $value,'label'=>$label,'class'=>'eight'])."</td>";
					}
				])->label(false); ?>
			
			
			
			</div>
		</div>
	
	</div>
	
	
		<div  style = "display: <?php echo($model->current_pregnancy_thought_ending_life >= 1)?"display":"none"; ?>" >
		<div class="row idform" id="others">	
			<div class="col-lg-4">
			<label>ಇತರೆ ಎಂದಾದಲ್ಲಿ  </label>
			<?= $form->field($model, 'specify_others')->textInput([])->label(false); ?>
		</div>
		</div>	
	</div>
		
		<div id="scaleflag" class="hidden">
		<?= $form->field($model, 'score')->input('number',['readonly'=>true])->label(false);?>
		</div>
		
	
	
	</div>
    
	<!--
	
	
	
	$("select[name=\'SuicideQuestions[plan_attempted]\']").val("");
				$(".plan_attempted").prop("checked",false);
				
	$("select[name=\'SuicideQuestions[current_pregnancy_plan]\']").val("");
				$(".current_pregnancy_plan").prop("checked",false);
				$("select[name=\'SuicideQuestions[current_pregnancy_attempt]\']").val("");
				$(".current_pregnancy_attempt").prop("checked",false);	

$("#suicidequestions-times_attempted").val("");
				$("#suicidequestions-reasons_attempt").val("");				
	
	
	-->
</div>
<?php $this->registerJs('
	$("#"+formObj.formName).one("change",function()
	{
		autoSaveFormObj = formObj;
	});
')?>	
<?php if($model->not_applicable == 'YES'){
	$this->registerJs('
	setNotApplicableOnLoad("#withBoxShadow","#suicidequestions-not_applicable");
	', \yii\web\View::POS_READY);

}
	$this->registerJs('

	$("#suicidequestions-not_applicable").change(function()
		{	
			var value = $(this).val();
			if(value=="YES")
			{	
				setNotApplicableYes("#withBoxShadow","#suicidequestions-not_applicable");
			}
			else
			{
				setNotApplicableNo("#withBoxShadow","#suicidequestions-not_applicable");
			}
		});
		
	$(".ideas").click(function()
		{	
			if($(this).val() >= 1)
			{	
				$(".plan_attempted").prop("checked",false);
				$("#second").show();
			}
			else
			{	 
				$("select[name=\'SuicideQuestions[plan_attempted]\']").val("-10");
				$("select[name=\'SuicideQuestions[current_pregnancy_plan]\']").val("-10");
				$("select[name=\'SuicideQuestions[current_pregnancy_attempt]\']").val("-10");
				$("#suicidequestions-times_attempted").val("-10");
				$("#suicidequestions-reasons_attempt").val("-10");
				$("#suicidequestions-times_attempted").val("-10");
				$("#suicidequestions-reasons_attempt").val("-10");
				$("#suicidequestions-specify_others").val("-10");

				$("#second").hide();
				calc();
				autoSaveFormObj = formObj;
			}
		});
		
	$(".current_pregnancy_thought_ending_life").click(function()
		{	
			if($(this).val() >= 1)
			{	

				$(".current_pregnancy_plan").prop("checked",false);
				$(".current_pregnancy_attempt").prop("checked",false);
				$("#fourth").show();
			}
			else
			{	
				$("select[name=\'SuicideQuestions[current_pregnancy_plan]\']").val("-10");
				$("select[name=\'SuicideQuestions[current_pregnancy_attempt]\']").val("-10");
				$("#suicidequestions-times_attempted").val("-10");
				$("#suicidequestions-reasons_attempt").val("-10");
				$("#suicidequestions-specify_others").val("-10");
				$("#fourth").hide();
				$("#seventh").hide();
				$("#others").hide();
				calc();
				autoSaveFormObj = formObj;
				location.reload();
				
			}
		});	
	
	$(".current_pregnancy_attempt").click(function()
		{	
			if($(this).val() >= 1)
			{	
				$("#suicidequestions-times_attempted").val("");
				$("#suicidequestions-reasons_attempt").val("");
				$("#suicidequestions-times_attempted").prop("checked",false);
				$("#suicidequestions-reasons_attempt").prop("checked",false);
				location.reload();
				$("#seventh").show();
			}
			else
			{	
				$("#suicidequestions-times_attempted").val("-10");
				$("#suicidequestions-reasons_attempt").val("-10");
				calc();
				autoSaveFormObj = formObj;
				$("#seventh").hide();
				$("#others").hide();
				location.reload();
			}
		});
	$(".plan_attempted").click(function()
		{	
			if($(this).val() >= 1)
			{	
				$("#suicidequestions-times_attempted").val("");
				$("#suicidequestions-reasons_attempt").val("");
				$("#suicidequestions-times_attempted").prop("checked",false);
				$("#suicidequestions-reasons_attempt").prop("checked",false);
				$("#seventh").show();
				//$("#others").show();
				autoSaveFormObj = formObj;
				location.reload();
			}
			else
			{	
				$("#suicidequestions-times_attempted").val("-10");
				$("#suicidequestions-reasons_attempt").val("-10");
				$("#seventh").hide();
				$("#others").hide();
				location.reload();
				calc();
				autoSaveFormObj = formObj;
				location.reload();

			}
		});	

		$(".eight").click(function(){
			$("#others").show();
			});
	
', \yii\web\View::POS_READY);
?>
<script type="text/javascript">
	var formObj = {'url' : "suicide-questions/ajax",'formName' : "formSuicidequestions"};
    var calc = function () {
		var totalValue = 0;
		var root = 'suicidequestions';
        var id = '#'+root+'-';
		var clas = '.field-'+root+'-';
        var arr = ['ideas','plan_attempted','current_pregnancy_thought_ending_life','current_pregnancy_plan',
		'current_pregnancy_attempt'];

        for (var i in arr) {
            var val = $(clas + arr[i] + ' input[type=radio]:checked').val();
			if (parseInt(val) >=0) {
                totalValue += parseInt(val);
            }
        }	
        $(id+'score').val(totalValue);
		autoSaveObj = formObj;
	}
</script>