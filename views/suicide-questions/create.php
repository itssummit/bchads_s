<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SuicideQuestions */

$this->title = 'Suicide Questions';
$this->params['breadcrumbs'][] = ['label' => 'Suicide Questions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="suicide-questions-create">

    <h1 align = "center"> ಆತ್ಮಹತ್ಯೆಯ ನಡುವಳಿಕೆಯ ಪ್ರಶ್ನಾವಳಿ   </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
