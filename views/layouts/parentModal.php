<div id="parentModal" class="modal fade" data-backdrop = "static" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h5>Add Member Details</h5>
      </div>
      <div class="modal-body" id="parentBodyModal"></div>
    </div>
  </div>
</div>