<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\bootstrap\ButtonDropdown;
AppAsset::register($this);
use kartik\icons\Icon;
use app\models\Mother;
use app\classes\NavigationHandler;
Icon::map($this,Icon::FA);
$motherId = Yii::$app->session->get('motherId');
$assessmentId = Yii::$app->session->get('assessmentId');
$respondentId = Yii::$app->session->get('respondentId');
$respondentType = Yii::$app->session->get('respondentType');
$patientModel = Mother::findOne(['id' => $motherId]);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="translated-ltr" ng-app="bchadsApp">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    
</head>

<body ng-controller = "AppController"  class=" withBoxShadow">
<?php $this->beginBody() ?>
     <div class="navbar-xs" >
        <div class="navbar-primary">
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar" style="background-color:#000"></span>
                    <span class="icon-bar" style="background-color:#000"></span>
                    <span class="icon-bar" style="background-color:#000"></span> 
                  </button>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="myNavbar">
				<div class="col-lg-6 col-md-6 col-sm-6">
				<ul class="nav navbar-nav navbar-left">
					
			      	<li><img src="/bchads/web/img/newlogo.png" alt="Nimhans" width="20%" height="20%"></li>
					<li><h4 style = "color:white;">BANGALORE CHILD HEALTH AND DEVELOPMENT STUDY</h4></li>
					
				</ul>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6">
                <ul class="nav navbar-nav navbar-right" style="padding:20px;">
                    <!--<li ng-click = "startOfflineDataSync()">
                        <a href="#"><span class="glyphicon glyphicon-refresh"></span></a>
                    </li>-->
                    <li id = "idSearchText" style = "display:none;" ng-init = "showSearchLi();getAllSubject();searchMother = '<?php echo isset($_GET['mother'])?$_GET['mother']:null;?>'"> 
			            <a href="#" ng-init = "searchInput = '<?php echo isset($_GET['mother'])?true:false;?>';" ng-show = "searchInput">
			            <input type="text" ng-model="search.mother_code" autofocus ng-keydown = "setSearchMother($event);" class="form-control" placeHolder = "Search" title = "Search"/>
				        </a>			
			        </li>
                    <li ng-click = "toggleSearch()"> 
			            <a href="#"><span data-toggle="tooltip" data-placement="bottom"title="Search">
				      	    <i class="fa fa-search fa-2x"></i></span>
			            </a>
			        </li>
                    
				    <li><a href="index.php"><span class="fa fa-home fa-2x" data-toggle="tooltip" data-placement="bottom"title="Home"></span></a></li>
				    
                    <li style="display: none;"><a href="?r=mother%2Fcreate"><span class="fa fa-plus-square fa-2x" data-toggle="tooltip" data-placement="bottom"title="Create Mother"></span></a>
                    		
                    </li>
                    <?php if(Yii::$app->user->isSuperadmin):?>
			            <li>
                            <a href="?r=site/admin"><span class="fa fa-key fa-2x" data-toggle="tooltip" data-placement="bottom"title="Administration"></span></a>
				        </li>
                    <?php endif;?>
                    <!--<li><a href="#"><span class="fa fa-pencil fa-2x" data-toggle="tooltip" data-placement="bottom"title="Data input"></span></a></li>-->
                    <li><a href="?r=/site/logout"><span class="fa fa-user fa-2x" data-toggle="tooltip" data-placement="bottom"title="Logout"></span></a></li>
				</ul>
				</div>
                </div>
            </nav>
        </div>
    </div>
	
<div class="wrap">
    <div class="container">
        <?php if($motherId):?>
         <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" >
            <!--<div class="col-md-4 col-lg-3 col-sm-6 col-xs-6">
                 <label style="color: #8a51b5;font-size: 14px;">Mother Name : < ?php echo $patientModel['last_name']?$patientModel['first_name']." ".$patientModel['last_name']:$patientModel['first_name']?></label>
            </div>-->
			<div class="col-md-3 col-lg-3 col-sm-6 col-xs-6">
                 <label style="color: #8a51b5;font-size: 14px;">Mother Code : <?php echo $patientModel['mother_code'];?></label>
            </div>
            
             <?php if($respondentId):?>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6">
                        <label style="color: #8a51b5; font-size: 14px;"> Respondent: <?php echo $respondentType?>    </label>
                </div>
            <?php endif ?>
        </div>
        <?php endif?>
		<?= $content ?>
	  
    </div>
  

<div class="text-center">
    <?php 
        $page = new app\classes\NavigationHandler();
        $page->setNavigation();
        $startDiv  = '<div class = "col-xs-2">';
        $endDiv = '</div>'; 
        if(isset($page->data['prev']['table_name']))
        {
            echo Html::a('Prev',Url::toRoute(['scale/selection','table' => $page->data['prev']['table_name'],'display' => $page->data['prev']['display_sequence'],'respId' => 0]),['class' => 'btn btn-warning'])."&nbsp;";
        } 
        if((isset($page->data['prev']['table_name']))||(isset($page->data['next']['table_name']))){
            echo Html::a('Scales Menu',Url::toRoute(['assessment/scaleform','aid' => $assessmentId]),['class' => 'btn btn-primary'])."&nbsp;";
        }
        if(isset($page->data['next']['table_name']))
        {
            echo Html::a('Next',Url::toRoute(['scale/selection','table' => $page->data['next']['table_name'],'display' => $page->data['next']['display_sequence'],'respId' => 0]),['class' => 'btn btn-warning']);
        }
    ?>
</div>
  </div>
<div>
   <?php
       	include 'parentModal.php';
    ?>
</div>
<div class="col-sm-2 col-md-1 col-lg-1 col-xs-2">
    <a href="http://summitsolutions.in/" class="logo" target="blank">
        <img class="logo" src="/bchads/web/img/summit-white.png" alt="" width="7%" height="8%"/> 
    </a>
</div>

<?php $this->endBody() ?>
<script type="text/javascript">
	window.onbeforeunload = function(event) {		
	    if(autoSaveFormObj.url){
			autoSaveForm(autoSaveFormObj,null);
		}
		if(autoSaveObj.url)
		{
			autoSave(autoSaveObj);
		}
		if(autoSaveObj.length > 0)
		{
			autoSaveObjArr(autoSaveObj);
		}
	};
</script>
</body>
</html>
<?php $this->endPage() ?>
<?php
//BootStrap ToolTip	
$js = <<< 'SCRIPT'
$(function () {
    $("[data-toggle='tooltip']").tooltip();
});
$(function () {
    $("[data-toggle='popover']").popover();
});
SCRIPT;
$this->registerJs($js);
?>
<?php 
if(isset($this->params['searchPatient']))
{
	$this->registerJs("
		setTimeout(function() {
        		$('#searchPopover').popover('hide');
    	}, 2000);
		$('#searchPopover').popover('show');
		$('#searchPopoverText').focus();
		$('#searchPopoverText').val('".$this->params['searchPatient']."');		
	");
}
?>
<style>
#colorNav{
        max-height: 0px;
        overflow: initial;
    }
    #colorNav{
    background-color: #fff;
	} 
    
     #colorNav a:hover {
    background-color: lightpink;
	}
    .glyphicon-refresh{
        font-size: 27px;
    }
</style>	



