<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Assessment;
$keyPair = [-15 => '-15: Child Not Co-Operative For Assessment',
		  -14 => '-14: Not Available For Assessment',
		  -11 => '-11: Inadequate Information',
		  -10 => '-10: Not Applicable',
		  -9 => '-9: Missing',
		  -8 => '-8: Refused',
		  -7 => '-7: Partner Accompanied Missing',
		  -6 => '-6: Family Accompanied Missing',
		  -5 => '-5: Missing:Not Asked',
		  -3 => '-3: No Such Relationship/No Partner'];

/* @var $this yii\web\View */
/* @var $model app\models\Bitsea */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile('@web/js/scales.js',['depends' => [\yii\web\JqueryAsset::className()]]);

$respondentId = Yii::$app->session->get('respondentId');
$motherId = Yii::$app->session->get('motherId');

        $assessmentId = Yii::$app->session->get('assessmentId');
$pModel = Assessment::findOne(['mother_id' => $motherId , 'id' => $assessmentId]);
?>

<div class="bitsea-form">

    <?php $form = ActiveForm::begin(['id' => 'formBitsea']); ?>
	<div id="withBoxShadow" class="col-xs-12 col-md-12 col-lg-12 col-sm-12">
<div id="idbitscale" style="overflow-x:auto;">	
<p><b>ನಿಮ್ಮ ಮಗುವಿನ ಭಾವನೆಗಳು  ಮತ್ತು ವರ್ತನೆಗಳ ಬಗ್ಗೆ ಕೆಲವು ಪ್ರಶ್ನೆಗಳು . </b></p>
<p><b> ಸೂಚನೆಗಳು  :
 ಈ ಕೆಳಗಿನ ನಮೂನೆಯ 12  ರಿಂದ  35  ತಿಂಗಳಿನ ಮಕ್ಕಳಿಗೆ ಸಂಬಂಧಿಸಿದ ವಾಕ್ಯಗಳನ್ನು ಹೊಂದಿದೆ. ಹೆಚ್ಚಿನ ವಾಕ್ಯಗಳು ಮಕ್ಕಳ 
ಸಹಜವಾದ ಭಾವನೆಗಳು ಮತ್ತು ವರ್ತನೆಗಳನ್ನು ವಿವರಿಸುತ್ತದೆ .ಆದರೆ  ಕೆಲವು ವಾಕ್ಯಗಳು ತೊಂದರೆ ಇರುವ ಭಾವನೆಗಳು ಮತ್ತು ವರ್ತನೆಗಳನ್ನು 
ವಿವರಿಸುತ್ತದೆ .ದಯವಿಟ್ಟು ಸಾಧ್ಯವಾದಷ್ಟು ಪ್ರತಿಯೊಂದು ವಾಕ್ಯಗಳಿಗೆ ಉತ್ತರಿಸಿ </b></p>
<p><b>ಕಳೆದ ತಿಂಗಳಿನಲ್ಲಿ ನಿಮ್ಮ ಮಗುವಿನ ವರ್ತನೆಯನ್ನು ಚೆನ್ನಾಗಿ ವಿವರಿಸುವ ಪ್ರಕ್ರಿಯೆಗೆ ವೃತ್ತವನ್ನು ಹಾಕಿರಿ </b></p>
    <table border="1">
            <thead>
                <tr>
					<th></th>
                    <th>ಸತ್ಯವಲ್ಲ / ಅಪರೂಪ </th>
                    <th>ಸತ್ಯ /ಕೆಲವುಬಾರಿ </th>
                    <th>ಖಂಡಿತ ಸತ್ಯ /ಯಾವಾಗಲೂ</th>
                    <th></th>
                    
                </tr> 
            </thead>
			<tbody>
			<tr>
			<td>1)ಗೆದ್ದಾಗ ಖುಷಿಯನ್ನು ತೋರಿಸುತ್ತಾನೆ/ಳೆ (ಉದಾ : ಚಪ್ಪಾಳೆ ಹಾಕುವುದು )</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'happy_won')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'happy_won')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>
			
			<tr>
			<td>2)ಯಾವಾಗಲೂ ಎಷ್ಟು ಪೆಟ್ಟು ಮಾಡಿಕೊಳ್ಳುತ್ತಾನೆ/ಳೆ ಎಂದರೆ, ಅವನ/ಳನ್ನು ಬಿಟ್ಟು ಬೇರೆಡೆ ದೃಷ್ಟಿ  ಹಾಯಿಸಲಾಗುವುದಿಲ್ಲ.</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'hurt_himself')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'hurt_himself')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>

			<tr>
			<td>3)ಮಾನಸಿಕ ಒತ್ತಡ,ಉದ್ವಿಗ್ನ, ಅಥವಾ ಭಯ ಆದಂತೆಕಾಣಿಸುತ್ತಾನೆ/ಳೆ.</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'stress_fear')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'stress_fear')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>

			<tr>
			<td>4)ಚಡಪಡಿಸುತ್ತಾನೆ /ಳೆ  ಮತ್ತು ಸುಮ್ಮನೆ ಕುಳಿತುಕೊಳ್ಳಲು ಆಗುವುದಿಲ್ಲ </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'cant_sit_quitely')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'cant_sit_quitely')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>

    
			<tr>
			<td>5)ನಿಯಮಗಳನ್ನು ಪಾಲಿಸುತ್ತಾನೆ/ಳೆ.</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'follows_rules')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'follows_rules')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>

			<tr>
			<td>6)ರಾತ್ರಿ ಮಧ್ಯಎಚ್ಚರವಾಗುತ್ತಾನೆ/ಳೆ ಮತ್ತು ಮತ್ತೆ ನಿದ್ದೆ ಮಾಡಲು ಸಹಾಯ ಬೇಕಾಗುತ್ತದೆ </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'wakeup_midnight')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'wakeup_midnight')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>

			<tr>
			<td>7)ಅಳುತ್ತಾನೆ/ಳೆ ಅಥವಾ ಸುಸ್ತಾಗುವವರೆಗೂ  ಕೋಪೋದ್ರೇಕಕ್ಕೊಳಗಾಗುತ್ತಾನೆ/ಳೆ.</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'cry')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'cry')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>

			<tr>
			<td>8)ಕೆಲವು ಜಾಗಗಳ, ಪ್ರಾಣಿಗಳ, ಅಥವಾ ವಸ್ತುಗಳ ಬಗ್ಗೆ ಭಯವಿದೆ.ಅವನಿ /ಳಿಗೆ  ಯಾವುದರ ಬಗ್ಗೆ ಭಯವಿದೆ ?
			<?= $form->field($model, 'animal_fear_det')->textInput()->label(false); ?>
			</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'animal_fear')->radio(['label' => $i, 'value' => $i,'class' => 'b1'])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'animal_fear')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>
	
			<tr>
			<td>9)ಬೇರೆ ಮಕ್ಕಳಿಗಿಂತ ಕಮ್ಮಿ ಮೋಜು ಮಾಡುತ್ತಾನೆ/ಳೆ. </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'less_enjoy')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'less_enjoy')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>
			
			<tr>
			<td>10)ಅಸಮಾಧಾನವಾದಾಗ ನಿಮಗಾಗಿ (ಅಥವಾ ಮತ್ತೊಬ್ಬ ಹೆತ್ತವರಿಗಾಗಿ) ಹುಡುಕುತ್ತಾನೆ /ಳೆ. </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'ifsad_searches_parents')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'ifsad_searches_parents')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>

    
			<tr>
			<td>11)ನೀವು ಬಿಟ್ಟು ಹೊರಡಲು ಪ್ರಯತ್ನಿಸಿದಾಗ ಅಳುತ್ತಾನೆ/ಳೆ ಅಥವಾ ನಿಮಗೆ ಗಟ್ಟಿಯಾಗಿ ಅಂಟಿಕೊಳ್ಳುತ್ತಾನೆ/ಳೆ.</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'cry_left')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'cry_left')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>

			<tr>
			<td>12)ತುಂಬಾ ಚಿಂತೆ ಮಾಡುತ್ತಾನೆ/ಳೆ ಅಥವಾ ತುಂಬಾ ಗಂಭೀರವಾಗಿದ್ದಾನೆ/ಳೆ.</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'thinks')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'thinks')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>

			<tr>
			<td>13)ನೀವು ಅವನ/ಳ ಹೆಸರು ಕರೆದ ಕೂಡಲೇ ನಿಮ್ಮನ್ನು ನೋಡುತ್ತಾನೆ/ಳೆ.</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'looked_on_call')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'looked_on_call')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>

			<tr>
			<td>14)ನೋವಾದಾಗ ಪ್ರತಿಕ್ರಿಯಿಸುವುದಿಲ್ಲ.</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'didnt_respond_pain')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'didnt_respond_pain')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>

			<tr>
			<td>15)ಪ್ರೀತಿಪಾತ್ರರ ಜೊತೆಗೆ ಮಮತೆಯಿಂದ ಇರುತ್ತಾನೆ/ಳೆ. </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'happy_with_loved')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'happy_with_loved')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>

			<tr>
			<td>16)ಕೆಲವು ವಸ್ತುಗಳು ಅನುಭವವಾಗುವ ರೀತಿಯ ಕಾರಣದಿಂದ ಅವುಗಳನ್ನು ಮುಟ್ಟುವುದಿಲ್ಲ </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'didnt_touch_things')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'didnt_touch_things')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>

			<tr>
			<td>17) ನಿದ್ದೆಗೆ ಹೋಗಲು ಅಥವಾ ನಿದ್ದೆ ಮಾಡಿಕೊಂಡಿರಲು ತೊಂದರೆಯಿದೆ.</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'problem_in_sleep')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'problem_in_sleep')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>

			<tr>
			<td>18)ಸಾರ್ವಜನಿಕ ಜಾಗಗಳಲ್ಲಿ ಓಡಿಹೋಗುತ್ತಾನೆ.</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'runs')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'runs')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>

			<tr>
			<td>19)ಬೇರೆ ಮಕ್ಕಳೊಂದಿಗೆ ಚೆನ್ನಾಗಿ ಆಟವಾಡುತ್ತಾನೆ/ಳೆ (ಅಣ್ಣ /ತಮ್ಮ ,ಅಕ್ಕ /ತಂಗಿಯರನ್ನು ಬಿಟ್ಟು )</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'play_with_others')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'play_with_others')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>

			<tr>
			<td>20)ತುಂಬಾ ಹೊತ್ತು ಗಮನವನ್ನು ಕೊಡಲು ಸಾಧ್ಯವಾಗುತ್ತದೆ(ಟಿವಿ ನೋಡುವುದನ್ನು ಬಿಟ್ಟು )</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'concentrates')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'concentrates')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>

			<tr>
			<td>21)ಬದಲಾವಣೆಗೆ ಹೊಂದಿಕೊಳ್ಳಲು ಕಷ್ಟವಾಗುತ್ತದೆ .</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'problem_in_udjustment')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'problem_in_udjustment')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>

			<tr>
			<td>22)ಯಾರಿಗಾದರೂ ನೋವಾದಾಗ ಸಹಾಯ ಮಾಡಲು ಪ್ರಯತ್ನಿಸುತ್ತಾನೆ/ಳೆ (ಉದಾ: ಬೊಂಬೆಯನ್ನು ಕೊಡುವುದು )</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'helps_others')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'helps_others')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>

			<tr>
			<td>23)ಯಾವಾಗಲೂ ಬೇಗನೆ ಅಸಾಮಾಧಾನಗೊಳ್ಳುತ್ತಾನೆ/ಳೆ.</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'not_happy')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'not_happy')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>

			<tr>
			<td>24)ಆಹಾರ ಗಂಟಲಿಗೆ ಸಿಕ್ಕುವ ಹಾಗೆ ಊಟ ಮಾಡುತ್ತಾನೆ </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'eating_problem')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'eating_problem')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>

			<tr>
			<td>25)ನೀವು ಅವನಿ /ಳಿಗೆ  ತಮಾಷೆಯ ಶಬ್ದಗಳನ್ನು ಅನುಸರಿಸಲು ಹೇಳಿದರೆ , ಅವನು/ಳು ಅನುಕರಿಸುತ್ತಾಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'imitates_sound')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'imitates_sound')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>

			<tr>
			<td>26)ಊಟ ಮಾಡಲು ನಿರಾಕರಿಸುತ್ತಾನೆ/ಳೆ</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'rejects_food')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'rejects_food')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>

			<tr>
			<td>27)ಬೇರೆ ಮಕ್ಕಳನ್ನು ಹೊಡೆಯುವುದು, ದಬ್ಬುವುದು, ತುಳಿಯುವುದು ಅಥವಾ ಕಚ್ಚುವುದನ್ನು ಮಾಡುತ್ತಾನೆ/ಳೆ  (ಅಣ್ಣ /ತಮ್ಮ ,ಅಕ್ಕ /ತಂಗಿಯರನ್ನು ಬಿಟ್ಟು )</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'fights')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'fights')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>

			<tr>
			<td>28)ವಿನಾಶಕಾರಿಯಾಗಿದ್ದಾನೆ/ಳೆ, ಉದ್ದೇಶಪೂರ್ವಕವಾಗಿ ವಸ್ತುಗಳನ್ನು ಮುರಿಯುವುದು ಅಥವಾ ಹಾಳುಮಾಡುತ್ತಾನೆ/ಳೆ</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'breaks_things')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'breaks_things')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>

			<tr>
			<td>29)ದೂರ ಇರುವುದನ್ನು ನಿಮಗೆ ಬೆರಳು ಮಾಡಿತೋರಿಸುತ್ತಾನೆ/ಳೆ</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'shows_fingers')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'shows_fingers')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>

			<tr>
			<td>30)ನಿಮ್ಮನ್ನು ಹೊಡೆಯುವುದು, ತುಳಿಯುವುದು ಅಥವಾ ಕಚ್ಚುವುದನ್ನು ಮಾಡುತ್ತಾನೆ/ಳೆ (ಅಥವಾ ಮತ್ತೊಬ್ಬ ಹೆತ್ತವರಿಗೆ )</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'beats_bites')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'beats_bites')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>

			<tr>
			<td>31)ಬೊಂಬೆಗಳು ಅಥವಾ ಆಟಿಕೆಯ ಪ್ರಾಣಿಗಳನ್ನು ತಬ್ಬಿಕೊಳ್ಳುವುದು ಅಥವಾ ಊಟಮಾಡಿಸುವುದನ್ನು ಮಾಡುತ್ತಾಳೆ</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'plays_with_toys')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'plays_with_toys')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>

			<tr>
			<td>32)ತುಂಬಾ ಸಂತೋಷವಿಲ್ಲದ, ಬೇಜಾರು, ಖಿನ್ನತೆ ಅಥವಾ ಏಕಾಂತ  ಪ್ರವೃತ್ತಿಯಂತೆ ಕಾಣಿಸುತ್ತಾನೆ/ಳೆ</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'sad')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'sad')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>

			<tr>
			<td>33)ಉದ್ದೇಶಪೂರ್ವಕವಾಗಿ ನಿಮಗೆ ಗಾಯ ಮಾಡುತ್ತಾನೆ/ಳೆ (ಅಥವಾ ಮತ್ತೊಬ್ಬ ಹೆತ್ತವರಿಗೆ )</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'hurts_parents')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'hurts_parents')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

			</td>
			</tr>

			<tr>
			<td>34)ಅಸಮಾಧಾನವಾದಾಗ ಚಲಿಸುವುದಿಲ್ಲ, ಗಟ್ಟಿಯಾಗುತ್ತಾನೆ /ಳೆ, ಅಲ್ಲಾಡುವುದಿಲ್ಲ</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'didnt_move_angry')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'didnt_move_angry')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>
			
			<tr>
			<th>
			<p>ಈ ಕೆಳಗಿನ ವಾಕ್ಯಗಳು ಮಕ್ಕಳಿಗೆ ತೊಂದರೆ ಮಾಡುವ ಭಾವನೆಗಳು ಮತ್ತು ವರ್ತನೆಗಳನ್ನು ವಿವರಿಸುತ್ತದೆ . ಕೆಲವು ವರ್ತನೆಗಳನ್ನು ನಿಮ್ಮ ಮಗುವಿನಲ್ಲಿ ನೀವು ನೋಡದಿದ್ದಲ್ಲಿ ಅಂತಹ ವಿವರಗಳನ್ನು ಅರ್ಥಮಾಡಿಕೊಳ್ಳಲು ಕಷ್ಟವಾಗಬಹುದು . 
			ದಯವಿಟ್ಟು ಸಾಧ್ಯವಾದಷ್ಟು ಪ್ರತಿಯೊಂದು ವಾಕ್ಯಗಳಿಗೆ ಉತ್ತರಿಸಿ . 
			</p>
			
			<p>ಕಳೆದ ತಿಂಗಳಿನಲ್ಲಿ ನಿಮ್ಮ ಮಗುವಿನ ವರ್ತನೆಯನ್ನು ಚೆನ್ನಾಗಿ ವಿವರಿಸುವ ಒಂದು ಪ್ರತಿಕ್ರಿಯೆಗೆ ವೃತ್ತವನ್ನು ಹಾಕಿರಿ </p>
			</th>
			<th>ಸತ್ಯವಲ್ಲ / ಅಪರೂಪ </th>
            <th>ಸತ್ಯ /ಕೆಲವುಬಾರಿ </th>
            <th>ಖಂಡಿತ ಸತ್ಯ /ಯಾವಾಗಲೂ</th>
			</tr>
			<tr>
			<td>35)ವಸ್ತುಗಳನ್ನು ವಿಶೇಷ ರೀತಿಯಲ್ಲಿ ಪದೇ ಪದೇ ಜೋಡಿಸುತ್ತಾನೆ/ಳೆ ಮತ್ತುಅಡಚಣೆಯಾದರೆ ಅಸಮಾಧಾನಗೊಳ್ಳುತ್ತಾನೆ/ಳೆ.</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'keeps_things')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'keeps_things')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>36)ಒಂದೇ ರೀತಿಯ  ಕ್ರಿಯೆಯನ್ನು  ಅಥವಾ ಶಬ್ದವನ್ನು ಸಂತೋಷವಿಲ್ಲದೆ ಪುನರಾವರ್ತಿಸುವುದು. ಉದಾಹರಣೆ ಕೊಡಿ 
			<?= $form->field($model, 'repeats_sounds_det')->textInput()->label(false); ?>
			</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'repeats_sounds')->radio(['label' => $i, 'value' => $i,'class' => 'b2'])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'repeats_sounds')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>
			
			<tr>
			<td>37)ನಿರ್ದಿಷ್ಟವಾದ ಚಲನವಲನಗಳನ್ನು ಪದೇ ಪದೇ(ಉದಾ:ತೂಗುವುದು, ಪುಟಿಯುವುದು)ಪುನರಾವರ್ತಿಸುವುದು. ಉದಾಹರಣೆ ಕೊಡಿ 
			<?= $form->field($model, 'repeats_work_det')->textInput()->label(false); ?>
			</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'repeats_work')->radio(['label' => $i, 'value' => $i,'class' => 'b3'])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'repeats_work')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>
			
			<tr>
			<td>38)ಇತರರಿಂದ ಬೇರೆ ಇರುವುದು. ತನ್ನ ಸುತ್ತ ಏನಾಗುತ್ತಿದೆ ಎಂಬುದರ ಸಂಪೂರ್ಣ ಪ್ರಜ್ಞೆ ಇಲ್ಲದಿರುವುದು </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'separate_from_others')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'separate_from_others')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>
			
			<tr>
			<td>39)ನೇರವಾಗಿ ದೃಷ್ಟಿಯಿಟ್ಟು ನೋಡುವುದಿಲ್ಲ </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'see_straight')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'see_straight')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>
			
			<tr>
			<td>40)ದೈಹಿಕ ಸಂಪರ್ಕ ತಪ್ಪಿಸುತ್ತಾನೆ /ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'physical_contact')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'physical_contact')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>
			
			<tr>
			<td>41)ಉದ್ದೇಶಪೂರ್ವಕವಾಗಿ ತನಗೆ ನೋವು ಮಾಡಿಕೊಳ್ಳುತ್ತಾನೆ/ಳೆ.(ಉದಾ: ತಲೆಯನ್ನು ಚಚ್ಚಿಕೊಳ್ಳುವುದು) ಉದಾಹರಣೆ ಕೊಡಿ
			<?= $form->field($model, 'hurts_himself_det')->textInput()->label(false); ?></td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'hurts_himself')->radio(['label' => $i, 'value' => $i,'class' => 'b4'])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'hurts_himself')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			
			<tr>
			<td>42) ತಿನ್ನಲು ಯೋಗ್ಯವಲ್ಲದ (ಉದಾ: ಕಾಗದ ಅಥವಾ ಬಣ್ಣ ) ವಸ್ತುಗಳನ್ನು ತಿನ್ನು /ಕುಡಿಯುವುದು.ಉದಾಹರಣೆ ಕೊಡಿ  
		
			<?= $form->field($model, 'eats_nonhealthy_det')->textInput()->label(false); ?></td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td>
			<?= $form->field($model, 'eats_nonhealthy')->radio(['label' => $i, 'value' => $i,'class' => 'b5'])?></td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'eats_nonhealthy')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>
			
			
			
			
			</tbody>
		
	</table>
	<br>
	<div id ="bit">
	<table border="1" class="responsive-table">
	<tbody>
		<tr>
			<td>
			ಅ ) ನಿಮ್ಮ ಮಗುವಿನ ವರ್ತನೆ ಭಾವನೆ ಅಥವಾ  ಸಂಬಂಧಗಳ ಬಗ್ಗೆ ಎಷ್ಟು ಚಿಂತೆ ಇದೆ  ?  
			</td>
			<td>
			<?= $form->field($model, 'feelings_about_baby')->radio(['label' => 'ಚಿಂತೆ ಇಲ್ಲ ', 'value' => 0])?>
			</td>
			<td>
			<?= $form->field($model, 'feelings_about_baby')->radio(['label' => 'ಸ್ವಲ್ಪ ಚಿಂತೆ ಇದೆ ', 'value' => 1])?>
			</td>
			<td>
			<?= $form->field($model, 'feelings_about_baby')->radio(['label' => 'ಚಿಂತೆ ಇದೆ ', 'value' => 2])?>
			</td>
			<td>
			<?= $form->field($model, 'feelings_about_baby')->radio(['label' => ' ತುಂಬಾ ಚಿಂತೆ ಇದೆ ', 'value' => 3])?>
			</td>
			<td>
			<?= $form->field($model, 'feelings_about_baby')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>
			
			<tr>
			<td>
			ಬ ) ನಿಮ್ಮ ಮಗುವಿನ ಭಾಷೆಯ ಬೆಳವಣಿಗೆ ಬಗ್ಗೆ ಎಷ್ಟು ಚಿಂತೆ ಇದೆ ?
			</td>
			<td>
			<?= $form->field($model, 'feelings_about_language')->radio(['label' => 'ಚಿಂತೆ ಇಲ್ಲ ', 'value' => 0,'onClick'=>'calc()'])?>
			</td>
			<td>
			<?= $form->field($model, 'feelings_about_language')->radio(['label' => 'ಸ್ವಲ್ಪ ಚಿಂತೆ ಇದೆ ', 'value' => 1,'onClick'=>'calc()'])?>
			</td>
			<td>
			<?= $form->field($model, 'feelings_about_language')->radio(['label' => 'ಚಿಂತೆ ಇದೆ ', 'value' => 2,'onClick'=>'calc()'])?>
			</td>
			<td>
			<?= $form->field($model, 'feelings_about_language')->radio(['label' => ' ತುಂಬಾ ಚಿಂತೆ ಇದೆ ', 'value' => 3,'onClick'=>'calc()'])?>
			</td>
			<td>
			<?= $form->field($model, 'feelings_about_language')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>
	</tbody>
	</table>
	</div>
			<div class="hidden">
				<?= $form->field($model, 'score')->input('number',['readonly'=>true])->label(false);?></td>
			</div>
			
			
</div>    
	</div>
    <?php ActiveForm::end(); ?>

</div>
<?php $this->registerJs('
	$("#"+formObj.formName).one("change",function()
	{
		autoSaveObj = formObj;
	});

	$(".b1").change(function(){
				console.log("calling");
				if($(this).val() == 1 || $(this).val() == 2){
					console.log("entered in if loop");
					$("#bitsea-animal_fear_det").val("");
				}
				else if($(this).val() == 0)
				{
					$("#bitsea-animal_fear_det").val("-10");
				}
				});

				$(".b2").change(function(){
				console.log("calling");
				if($(this).val() == 1 || $(this).val() == 2){
					console.log("entered in if loop");
					$("#bitsea-repeats_sounds_det").val("");
				}
				else if($(this).val() == 0)
				{
					$("#bitsea-repeats_sounds_det").val("-10");
				}
				});

				$(".b3").change(function(){
				console.log("calling");
				if($(this).val() == 1 || $(this).val() == 2){
					console.log("entered in if loop");
					$("#bitsea-repeats_work_det").val("");
				}
				else if($(this).val() == 0)
				{
					$("#bitsea-repeats_work_det").val("-10");
				}
				});

				$(".b4").change(function(){
				console.log("calling");
				if($(this).val() == 1 || $(this).val() == 2){
					console.log("entered in if loop");
					$("#bitsea-hurts_himself_det").val("");
				}
				else if($(this).val() == 0)
				{
					$("#bitsea-hurts_himself_det").val("-10");
				}
				});

				$(".b5").change(function(){
				console.log("calling");
				if($(this).val() == 1 || $(this).val() == 2){
					console.log("entered in if loop");
					$("#bitsea-eats_nonhealthy_det").val("");
				}
				else if($(this).val() == 0)
				{
					$("#bitsea-eats_nonhealthy_det").val("-10");
				}
				});
',\yii\web\View::POS_READY)?>

<?php if($pModel->is_there_acg == 0 && $pModel->is_there_acg != null && $respondentId == 3){
	$this->registerJs('
	$(":input").val("-3");
		autoSaveFormObj = formObj;
		console.log("There is no ACG to enter");
	', \yii\web\View::POS_READY);

}?>
<script type="text/javascript">
	var formObj = {'url' : "bitsea/ajax",'formName' : "formBitsea"};
		var calc = function () {
        var totalValue = 0;
        var root= 'bitsea';
		var id = '#'+root+'-';
		var clas = '.field-'+root+'-';
        var arr = ['happy_won', 'hurt_himself', 'stress_fear', 'cant_sit_quitely', 'follows_rules', 'wakeup_midnight',
		'cry', 'animal_fear', 'less_enjoy', 'ifsad_searches_parents', 'cry_left', 'thinks', 'looked_on_call', 'didnt_respond_pain',
		'happy_with_loved', 'didnt_touch_things', 'problem_in_sleep', 'runs', 'play_with_others', 'concentrates', 'problem_in_udjustment',
		'helps_others', 'not_happy', 'eating_problem', 'imitates_sound', 'rejects_food', 'fights', 'breaks_things', 'shows_fingers', 
		'beats_bites', 'plays_with_toys', 'sad', 'hurts_parents', 'didnt_move_angry', 'keeps_things', 'repeats_sounds', 'repeats_work',
		'separate_from_others', 'hurts_himself', 'eats_nonhealthy', 'feelings_about_baby', 'feelings_about_language', 'separate_from_others','see_straight' ];
        
        for (var i in arr) {
            var val = $(clas + arr[i] + ' input[type=radio]:checked').val();
			if (parseInt(val)>=0) {
                totalValue += parseInt(val);
            }
        }
		

        $(id+'score').val(totalValue)
		autoSaveObj = formObj;
    }

</script>

