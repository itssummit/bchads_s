<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Bitsea */

$this->title = 'Brief Infant Toddler Social and Emotional Assessment';
$this->params['breadcrumbs'][] = ['label' => 'Bitseas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bitsea-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
