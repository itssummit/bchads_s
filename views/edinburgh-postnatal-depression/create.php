<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\EdinburghPostnatalDepression */

$this->title = 'Edinburgh Postnatal Depression';
/*$this->params['breadcrumbs'][] = ['label' => 'Edinburgh Postnatal Depressions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/
?>
<div class="edinburgh-postnatal-depression-create" >
	 <h1 align = "center">  ಎಡಿನ್ ಬರ ಪ್ರಸವಾ ನಂತರ ಖಿನ್ನತೆಯ ಪ್ರಶ್ನಾವಳಿ </h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
