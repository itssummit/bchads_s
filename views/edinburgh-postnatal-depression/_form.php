﻿<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$keyPair = [-15 => '-15: Child Not Co-Operative For Assessment',
          -14 => '-14: Not Available For Assessment',
          -11 => '-11: Inadequate Information',
          -10 => '-10: Not Applicable',
          -9 => '-9: Missing',
          -8 => '-8: Refused',
          -7 => '-7: Partner Accompanied Missing',
          -6 => '-6: Family Accompanied Missing',
          -5 => '-5: Missing:Not Asked',
          -3 => '-3: No Such Relationship/No Partner'];
/* @var $this yii\web\View */
/* @var $model app\models\EdinburghPostnatalDepression */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile('@web/js/scalesome.js',['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<div class="edinburgh-postnatal-depression-form">

    <?php $form = ActiveForm::begin(['id' => 'formEdinburghpostnataldepression']); ?>
    <div id="withBoxShadow" class="col-xs-12 col-md-12 col-lg-12 col-sm-12">
    <h4><b>ನೀವು ಗರ್ಭಿಣಿ ಯಾಗಿದ್ದೀರಿ ಅಥವಾ ನಿಮಗೆ ಈಗ ತಾನೆ ಮಗು ಹುಟ್ಟಿದೆ ಎಂದಾದರೆ ,ನಿಮ್ಮ ಅನಿಸಿಕೆ ಬಗ್ಗೆ ತಿಳಿಯಲು ನಾವು ಬಯಸುತ್ತೇವೆ . ಇಂದಿನ ದಿನ ಮಾತ್ರವಲ್ಲದೆ ,ಕಳೆದ 7  ದಿನಗಳಲ್ಲಿ ,ನಿಮ್ಮ ಭಾವನೆ ಹೇಗಿತ್ತು ಎಂದು ತಿಳಿಸುವ ತೀರಾ ಹತ್ತಿರದ ಉತ್ತರವನ್ನು ಗುರುತುಹಾಕಿ  .</b></h4>
    
    <div class="col-lg-12 idform">
        <label>1) ನನಗೆ ಏನೇ ಎದುರಾದಾಗಲೂ  ಸಹ ಅದರಲ್ಲಿ ತಮಾಷೆಯ ಸಂಗತಿಗಳನ್ನು ಕಾಣಲು ಸಾಧ್ಯವಾಗಿದೆ    </label>
        
    </div><br>
    
    <div class="col-lg-12 idform">
        <div class = "col-lg-8">
            <?= $form->field($model, 'feeling_past_7days_laugh')->radioList([0=>'ನನ್ನಿಂದ ಮುಂಚಿನಂತೆ  ಎಷ್ಟು ಸಾಧ್ಯವೋ ಅಷ್ಟು ' ,1=>  'ಈಗ ಅಷ್ಟಿಲ್ಲ ',2=>  'ಖಂಡಿತ ಈಗ ಮುಂಚಿನಷ್ಟಿಲ್ಲ  ',3=>' ಇಲ್ಲವೇ ಇಲ್ಲ'],['itemOptions'=>['onChange' => "calc()",'name' =>"rr"]])->label(false);?>
        </div>
        
        <div class="col-lg-4">  
            <?= $form->field($model, 'feeling_past_7days_laugh')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
        </div>
    </div>

    
    <div class="col-lg-12 idform">
        <label>2) ದಿನ ನಿತ್ಯದ ಜೀವನದಲ್ಲಿನ ವಿಷಯಗಳನ್ನು ಆನಂದದಿಂದ ಅನುಭವಿಸಲು ಎದುರು ನೋಡುತ್ತೇನೆ   (ಕಾತುರದಿಂದಿರುವೆ )       </label>
    </div>
    
    <div class="col-lg-12 idform">
        <div class="col-lg-8">
            <?= $form->field($model, 'feeling_past_7days_enjoyment')->radioList([0=>' ಹಿಂದಿನಂತೆ ಎಷ್ಟೋ ಅಷ್ಟು',1=>' ಸುಮಾರು ಮುಂಚೆ ಗಿಂತ ಕಡಿಮೆ',2=>' ಖಂಡಿತವಾಗಿಯೂ ',3=>' ಎಲ್ಲೇ ಸ್ವಲ್ಪ ಮಟ್ಟಿಗೆ' ],['itemOptions'=>['onChange' => "calc()"]])->label(false);?></p>
        </div>
    
        <div class="col-lg-4">  
            <?= $form->field($model, 'feeling_past_7days_enjoyment')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
        </div>
        
    </div>  
    
    <div class="col-lg-12 idform">
        <label>3) ಪರಿಸ್ಥಿತಿ ಸರಿ ಹೋಗದಿದ್ದಾಗ ನನ್ನನ್ನೇ ನಾನು  ದೂಷಿಸಿಕೊಂಡಿದ್ದೇನೆ  </label>
    </div>
    
    <div class="col-lg-12 idform">
        <div class="col-lg-8">
            <?= $form->field($model, 'feeling_past_7days_blamingmyself')->radioList([3=>' ಹೌದು ಹೆಚ್ಚಿನ ಸಮಯ',2=>' ಹೌದು ಕೆಲವೊಮ್ಮೆ',0=>' ಇಲ್ಲ ಯಾವಾಗಲೂ ಇಲ್ಲ',1=> 'ಇಲ್ಲ ಯಾವಾಗಲಾದರೊಮ್ಮೆ',],['itemOptions'=>['onChange' => "calc()"]])->label(false);?>
        </div>
        
        <div class="col-lg-4">  
            <?= $form->field($model, 'feeling_past_7days_blamingmyself')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
        </div>
        
    </div>  
    
    <div class="col-lg-12 idform">
        <label>4) ಸಣ್ಣ ಪುಟ್ಟ ವಿಷಯಗಳಿಗೂ ನಾನು ಆತಂಕಗೊಳ್ಳುತ್ತೇನೆ  ಇಲ್ಲವೇ ಚಿಂತೆ ಮಾಡುತ್ತೇನೆ  </label>
    </div>
    
    <div class="col-lg-12 idform">
        <div class="col-lg-8">
            <?= $form->field($model, 'feeling_past_7days_anxious_worried')->radioList([0=>' ಇಲ್ಲವೇ ಇಲ್ಲ',1=>' ಯಾವಾಗಲಾದರೊಮ್ಮೆ',2=>' ಹೌದು ಕೆಲವೊಮ್ಮ',3=>' ಹೌದು ಹಲವು ಬಾರಿ'],['itemOptions'=>['onChange' => "calc()"]])->label(false);?>
        </div>
        
        <div class="col-lg-4">  
            <?= $form->field($model, 'feeling_past_7days_anxious_worried')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
        </div>
        
    </div>  
    
    <div class="col-lg-12 idform">
        <label>5) ಸಣ್ಣ ಪುಟ್ಟ ವಿಷಯಗಳಿಗೂ ನಾನು ಹೆದರುತ್ತೇನೆ ಇಲ್ಲವೇ ಗಾಬರಿಯಾಗುತ್ತೇನೆ  </label>
    </div>
    <div class="col-lg-12 idform">
        <div class="col-lg-8">
            <?= $form->field($model, 'feeling_past_7days_scared_panicky')->radioList([0=>  'ಇಲ್ಲವೇ ಇಲ್ಲ',1=> 'ಅಷ್ಟೇನಿಲ್ಲ' ,2=>' ಹೌದು ಕೆಲವೊಮ್ಮ',3=>' ಹೌದು ಸಾಕಷ್ಟು ಬಾರಿ'], ['itemOptions'=>['onChange' => "calc()"]])->label(false);?>
        </div>
        <div class="col-lg-4">  
            <?= $form->field($model, 'feeling_past_7days_scared_panicky')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
        </div>
    </div>  
    
    <div class="col-lg-12 idform">
        <label>6) ಕೆಲಸ ಕಾರ್ಯ  ಒತ್ತಡಗಳನ್ನು ಸಹಿಸಲು ಅಸಾಧ್ಯವಾಗುತ್ತದೆ  </label>
    </div>
    
    <div class="col-lg-12 idform">
        <div class="col-lg-8">
            <?= $form->field($model, 'feeling_past_7days_things_getting_topofme')->radioList([3=>'ಹೌದು ,ಹೆಚ್ಚಿನ ಸಮಯ ನನಗೆ ಅವುಗಳನ್ನೆಲ್ಲಾ ನಿಭಾಯಿಸಲಾಗುತ್ತಿಲ್ಲ ', 2=>'ಹೌದು ,ಕೆಲವೊಮ್ಮೆ ಎಂದಿನಂತೆ ಅವುಗಳನ್ನು ನಿಭಾಯಿಸಲಾಗುತ್ತೆ ', 1=> 'ಇಲ್ಲ, ಸಾಕಷ್ಟು ಬಾರಿ ನಾನು ಅವುಗಳನ್ನೆಲ್ಲಾ ನಿಭಾಯಿಸಿದ್ದೇನೆ', 0=> ' ಇಲ್ಲ, ಎಂದಿನಂತೆ ಅವುಗಳನ್ನೆಲ್ಲಾ ನಿಭಾಯಿಸಿದ್ದೇನೆ' ,],['itemOptions'=>['onChange' => "calc()"]])->label(false);?></p>
        </div>
        <div class="col-lg-4">  
            <?= $form->field($model, 'feeling_past_7days_things_getting_topofme')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
        </div>
        
    </div>      
    
    <div class="col-lg-12 idform">
    <label> 7)ನನ್ನ ಮನಸ್ಸಿಗೆ ಬೇಸರವಾಗಿರುವುದರಿಂದ ನಾನು  ನಿದ್ದೆ ಮಾಡಲು ಕಷ್ಟವಾಗುತ್ತಿದೆ   </label>
    </div>
    <div class="col-lg-12 idform">
        <div class="col-lg-8">
            <?= $form->field($model, 'feeling_past_7days_unhappy_difficulty_sleeping')->radioList([3=>' ಹೌದು ಹೆಚ್ಚಿನ ಸಮಯ', 1=>' ಇಲ್ಲ, ಯಾವಾಗಲಾದರೊಮ್ಮೆ  ', 2=>' ಹೌದು,  ಕೆಲವೊಮ್ಮೆ ', 0=>' ಇಲ್ಲ , ಯಾವಾಗಲೂ ಇಲ್ಲ '],['itemOptions'=>['onChange' => "calc()"]])->label(false);?></p>
        </div>
        <div class="col-lg-4">  
            <?= $form->field($model, 'feeling_past_7days_unhappy_difficulty_sleeping')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
        </div>
    </div>  
    
    <div class="col-lg-12 idform">
    <label> 8)ನನಗೆ ದುಃಖವಾಗಿದೆ / ಬೇಸರವಾಗಿದೆ  </label>
    </div>
    
    <div class="col-lg-12 idform">
        <div class="col-lg-8">
            <?= $form->field($model, 'feeling_past_7days_sad_miserable')->radioList([0=>' ಇಲ್ಲ , ಯಾವಾಗಲು ಇಲ್ಲ' ,1=>' ಇಲ್ಲ, ಯಾವಾಗಲಾದರೂಮ್ಮೆ',2=>' ಹೌದು ಹಲವಾರು ಬಾರಿ ',3=>' ಹೌದು ಹೆಚ್ಚಿನ ಸಮಯ'],['itemOptions'=>['onChange' => "calc()"]])->label(false);?></p>
        </div>
        
        <div class="col-lg-4">  
            <?= $form->field($model, 'feeling_past_7days_sad_miserable')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
        </div>
    </div>      
    
    <div class="col-lg-12 idform">
    <label> 9)ನನ್ನ ಮನಸ್ಸಿಗೆ ಬೇಸರವಾಗಿರುವುದರಿಂದ , ನಾನು ಅಳುತ್ತಿರುವೆನು </label>
    </div>
    
    <div class="col-lg-12 idform">
        <div class="col-lg-8">
            <?= $form->field($model, 'feeling_past_7days_unhappy_crying')->radioList([0=> ' ಇಲ್ಲವೇ ಇಲ್ಲ',1=>'  ಯಾವಾಗಲಾದರೊಮ್ಮೆ',2=>' ಕೆಲವೊಮ್ಮೆ',3=>' ಹೌದು , ಅನೇಕ ವೇಳೆ' ],['itemOptions'=>['onChange' => "calc()"]])->label(false);?></p>
        </div>
        
        <div class="col-lg-4">  
            <?= $form->field($model, 'feeling_past_7days_unhappy_crying')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
        </div>
    </div>
    
    <div class="col-lg-12 idform">
    <label>10) ನನ್ನನ್ನೇ ನಾನು  ಘಾಸಿ  ಅಥವಾ ಹಾನಿ  ಮಾಡಿಕೊಳ್ಳುವ ಆಲೋಚನೆಗಳು   ನನಗೆ ಬಂದಿವೆ   </label>
    </div>
    
    <div class="col-lg-12 idform">
        <div class="col-lg-8">
            <?= $form->field($model, 'feeling_past_7days_harming_myself')->radioList([0=> ' ಇಲ್ಲವೇ ಇಲ್ಲ',1=> ' ಯಾವಾಗಲಾದರೊಮ್ಮೆ',2=>' ಕೆಲವೊಮ್ಮೆ',3=>' ಹೌದು , ಅನೇಕ ವೇಳೆ'],['itemOptions'=>['onChange' => "calc()"]])->label(false);?></p>
        </div>
        <div class="col-lg-4">  
            <?= $form->field($model, 'feeling_past_7days_harming_myself')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
        </div>
    </div>      
    
    <div class="hidden">
    <label>Score</label>
    <?= $form->field($model, 'score')->input('number',['readonly'=>true])->label(false);?>
    </div>
    
    
    </div>
</div>
<?php $this->registerJs('
	$("#"+formObj.formName).one("change",function()
	{
		autoSaveObj = formObj;
	});
')?>
<script type="text/javascript">
	var formObj = {'url' : "edinburgh-postnatal-depression/ajax",'formName' : "formEdinburghpostnataldepression"};
     var calc = function () {
        var totalValue = 0;
        var root = 'edinburghpostnataldepression';
        var id = '#'+root+'-';
        var clas = '.field-'+root+'-';
        var arr = ['feeling_past_7days_laugh','feeling_past_7days_enjoyment','feeling_past_7days_blamingmyself',
        'feeling_past_7days_anxious_worried','feeling_past_7days_scared_panicky',
        'feeling_past_7days_things_getting_topofme','feeling_past_7days_unhappy_difficulty_sleeping',
        'feeling_past_7days_sad_miserable','feeling_past_7days_unhappy_crying','feeling_past_7days_harming_myself'];
        
        for (var i in arr) {
            var val = $(clas + arr[i] + ' input[type=radio]:checked').val();
            console.log(parseInt(val));
            if (parseInt(val) >= 0) {
                totalValue += parseInt(val);
            }
        }
        
        $(id+'score').val(totalValue);
		autoSaveObj = formObj;

    }
</script>