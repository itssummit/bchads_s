﻿<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$keyPair = [-15 => '-15: Child Not Co-Operative For Assessment',
		  -14 => '-14: Not Available For Assessment',
		  -11 => '-11: Inadequate Information',
		  -10 => '-10: Not Applicable',
		  -9 => '-9: Missing',
		  -8 => '-8: Refused',
		  -7 => '-7: Partner Accompanied Missing',
		  -6 => '-6: Family Accompanied Missing',
		  -5 => '-5: Missing:Not Asked',
		  -3 => '-3: No Such Relationship/No Partner'];
/* @var $this yii\web\View */
/* @var $model app\models\Stai */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile('@web/js/scales.js',['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="stai-form">

    <?php $form = ActiveForm::begin(['id' => 'formStai']); ?>
	<b><i><h1 align="center">  ಸ್ಟಾಯ್  </h1></i></b>
	
    <div id="withBoxShadow" class="col-xs-12 col-md-12 col-lg-12 col-sm-12">
	
		<h3><b>ಫಾರಂ ವೈ  -1</b></h3>
		<p><b>
	ಈ ಕೆಳಗೆ  ಜನರು ತಮ್ಮನ್ನು ತಾವೇ ವರ್ಣಿಸಲು ಉಪಯೋಗಿಸಿರುವ  ಅನೇಕ ವಾಕ್ಯಗಳನ್ನು ಕೊಟ್ಟಿವೆ .ಪ್ರತಿಯೊಂದು ವಾಕ್ಯಗಳನ್ನು ಓದಿ 
ಮತ್ತು ಅದರ ಬಲಬದಿಯಲ್ಲಿ ಇರುವ ಅಂಕೆಗಳನ್ನು ನೋಡಿ .(ಈ ಸಮಯದಲ್ಲಿ ನಿಮಗೆ ಹೇಗೆ ಅನ್ನಿಸುತ್ತದೆ ಅನ್ನುವುದನ್ನು ಸರಿಯಾದ ಅಂಕೆಯ ಸುತ್ತ ವೃತ್ತವನ್ನು ಹಾಕುವುದರಿಂದ ಸೂಚಿಸಿ . ಇದರಲ್ಲಿ ಸರಿ ಅಥವಾ ತಪ್ಪು ಎಂಬ ಉತ್ತರಗಳಿಲ್ಲ ಒಂದೇ  ವಾಕ್ಯದ ಮೇಲೆ ಹೆಚ್ಚು ಸಮಯ ಕಳೆಯಬೇಡಿ . ಹಾಗೂ , ನಿಮ್ಮ ಈಗಿನ ಭಾವನೆಯನ್ನು ಸರಿಯಾಗಿ ವರ್ಣಿಸುವ ಉತ್ತರವನ್ನು ಕೊಡಿ . 
</b></p>
		<table border="1" class="responsive-scales">
			<thead>
                <tr>
					
					<th></th>
				    <th>ಇಲ್ಲವೇ  ಇಲ್ಲ</th>
                    <th>ಸ್ವಲ್ಪವಾಗಿ ಹೌದು </th>
					<th>ಸಾಧಾರಣವಾಗಿ ಹೌದು</th>
                    <th> ಖಂಡಿತವಾಗಿ ಹೌದು  </th>
					<th>Others Answers</th>
				</tr> 
			</thead>
	<tbody>
	<tr>
	
	<td>1)ನಾನು ಶಾಂತವಾಗಿರುವೆನೆಂದು ಅನ್ನಿಸುತ್ತದೆ  </td>
	<?php for($i = 1;$i < 5;$i++):?>
    <td><?= $form->field($model, 'feel_peace')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'feel_peace')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
	</tr>
	
	<tr>
	
	<td>2)ನಾನು ಸುರಕ್ಷಿತವಾಗಿರುವೆನೆಂದು  ಅನ್ನಿಸುತ್ತದೆ  </td>
	<?php for($i = 1;$i < 5;$i++):?>
    <td>
    <?= $form->field($model, 'feel_safe')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'feel_safe')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
	</tr>
	
	<tr>
	
	<td>3) ನಾನು ಒತ್ತಡದಲ್ಲಿದ್ದೇನೆಂದು ಅನ್ನಿಸುತ್ತದೆ   </td>
	<?php for($i = 1;$i < 5;$i++):?>
    <td>
    <?= $form->field($model, 'feel_pressure')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'feel_pressure')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
	</tr>
	
	
	<tr>
	
	<td>4)ನನಗೆ ತುಂಬ ಆಯಾಸವಾಗಿರುವುದೆಂದು  ಅನ್ನಿಸುತ್ತದೆ  </td>
	<?php for($i = 1;$i < 5;$i++):?>
    <td>
    <?= $form->field($model, 'feel_tired')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'feel_tired')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
	</tr>
	
	<tr>
	
	<td>5) ನಾನು ನೆಮ್ಮದಿಯಾಗಿರುವೆನೆಂದು  ಅನ್ನಿಸುತ್ತದೆ  </td>
	<?php for($i = 1;$i < 5;$i++):?>
    <td>
    <?= $form->field($model, 'feel_nemmadi')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'feel_nemmadi')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
	</tr>
	
	<tr>
	
	<td>6)ನನಗೆ ಬೇಜಾರಾಗಿದೆ ಎಂದು  ಅನ್ನಿಸುತ್ತದೆ  </td>
	<?php for($i = 1;$i < 5;$i++):?>
    <td>
    <?= $form->field($model, 'feel_sad')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'feel_sad')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
	</tr>
	
	<tr>
	
	<td>7) ನಾನು ಮುಂದೆ ಆಗಬಹುದಾದ ದುರಾದೃಷ್ಟಗಳ ಬಗ್ಗೆ ಈಗ ಚಿಂತೆ  ಮಾಡುತ್ತಿದ್ದೇನೆಂದು   ಅನ್ನಿಸುತ್ತದೆ  </td>
	<?php for($i = 1;$i < 5;$i++):?>
    <td>
    <?= $form->field($model, 'feel_future_disaster')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'feel_future_disaster')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
	</tr>
	
	<tr>
	
	<td>8)ನಾನು ತೃಪ್ತಿಯಾಗಿರುವಂತೆ  ಅನ್ನಿಸುತ್ತದೆ  </td>
	<?php for($i = 1;$i < 5;$i++):?>
    <td>
    <?= $form->field($model, 'feel_enough')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'feel_enough')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
	</tr>
	
	<tr>
	
	<td>9) ನನಗೆ ಭಯವಾದಂತೆ ಅನ್ನಿಸುತ್ತದೆ </td>
	<?php for($i = 1;$i < 5;$i++):?>
    <td>
    <?= $form->field($model, 'feel_fear')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'feel_fear')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
	</tr>
	
	<tr>
	
	<td>10) ನನಗೆ ಆರಾಮವಾಗಿರುವಂತೆ ಅನ್ನಿಸುತ್ತದೆ </td>
	<?php for($i = 1;$i < 5;$i++):?>
    <td>
    <?= $form->field($model, 'feel_free')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'feel_free')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
	</tr>
	
	<tr>
	
	<td>11)ನನಗೆ ಆತ್ಮವಿಶ್ವಾಸವಿರುವಂತೆ  ಅನ್ನಿಸುತ್ತದೆ </td>
	<?php for($i = 1;$i < 5;$i++):?>
    <td>
    <?= $form->field($model, 'feel_confident')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'feel_confident')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
	</tr>
	
	<tr>
	
	<td>12) ನನಗೆ ದುರ್ಬಲತೆ/ಕಳವಳ ಇರುವಂತೆ  ಅನ್ನಿಸುತ್ತದೆ</td>
	<?php for($i = 1;$i < 5;$i++):?>
    <td>
    <?= $form->field($model, 'feel_weak')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'feel_weak')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
	</tr>
	
	<tr>
	
	<td>13 )ನನಗೆ  ಗಾಬರಿ / ಬೆದರಿದಂತೆ ಅನ್ನಿಸುತ್ತದೆ </td>
	<?php for($i = 1;$i < 5;$i++):?>
    <td>
    <?= $form->field($model, 'feel_feared')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td><?= $form->field($model, 'feel_feared')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
	</tr>
	
	<tr>
	
	<td>14) ನನಗೆ  ನಿರ್ಧಾರ ಮಾಡಲು ಆಗದಂತೆ  ಅನ್ನಿಸುತ್ತದೆ 
</td>
	<?php for($i = 1;$i < 5;$i++):?>
    <td>
    <?= $form->field($model, 'cant_take_decisions')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'cant_take_decisions')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
	</tr>
	
	<tr>
	
	<td>15) ನನಗೆ  ಸಮಾಧಾನವಾಗಿರುವೆನೆಂದು  ಅನ್ನಿಸುತ್ತದೆ 
</td>
	<?php for($i = 1;$i < 5;$i++):?>
    <td>
    <?= $form->field($model, 'feel_samadhana')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'feel_samadhana')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
	</tr>
	
	<tr>
	
	<td>16) ನನಗೆ  ಸಂತುಷ್ಟನಾಗಿರುವೆನೆಂದು  ಅನ್ನಿಸುತ್ತದೆ 
</td>
	<?php for($i = 1;$i < 5;$i++):?>
    <td>
    <?= $form->field($model, 'feel_santhushti')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'feel_santhushti')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
	</tr>
	
	<tr>
	
	<td>17) ನನಗೆ  ಚಿಂತೆಯಾಗಿದೆ </td>
	<?php for($i = 1;$i < 5;$i++):?>
    <td>
    <?= $form->field($model, 'feel_sadness')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'feel_sadness')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
	</tr>
	
	<tr>
	
	<td>18) ನನಗೆ ಗಲಿಬಿಲಿಯಾದಂತೆ  ಅನ್ನಿಸುತ್ತದೆ 
</td>
	<?php for($i = 1;$i < 5;$i++):?>
    <td>
    <?= $form->field($model, 'feel_galibili')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'feel_galibili')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
	</tr>
	
	<tr>
	
	<td>19) ನನಗೆ ಧೃಡಮನಸ್ಸಿರುವಂತೆ  ಅನ್ನಿಸುತ್ತದೆ 
</td>
	<?php for($i = 1;$i < 5;$i++):?>
    <td>
    <?= $form->field($model, 'feel_drudamanasssu')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'feel_drudamanasssu')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
	</tr>
	
	<tr>
	
	<td>20) ನನಗೆ ಆನಂದವಾಗಿರುವಂತೆ  ಅನ್ನಿಸುತ್ತದೆ 
</td>
	<?php for($i = 1;$i < 5;$i++):?>
    <td>
    <?= $form->field($model, 'feel_happy')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'feel_happy')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
	</tr>
	
	</tbody>
	</table><br>
	
	<div id="notapp">
	<table>
	<tbody>
		<tr>
		<td><h4><b>Form[y-2] Is This Scale Applicable?</b></h4><?php echo $form->field($model, 'not_app')->dropDownList(['' =>'','YES' => 'Applicable', 'NO' => 'Not Applicable'],['class' => 'form-control scaleApplicable'])->label(false); ?>
		</td>
		</tr>
	</tbody>	
	</table>	
	</div>
	
	<div id= "stai2" style = "display: <?php echo($model->not_app == 'YES')?"display":"none"; ?> ">
	<b><h3>ಫಾರಂ ವೈ  -2</h3></b>
	<p>
	<b>
	ಈ ಕೆಳಗೆ  ಜನರು ತಮ್ಮನ್ನು ತಾವೇ ವರ್ಣಿಸಲು ಉಪಯೋಗಿಸಿರುವ  ಅನೇಕ ವಾಕ್ಯಗಳನ್ನು ಕೊಟ್ಟಿವೆ .ಪ್ರತಿಯೊಂದು ವಾಕ್ಯಗಳನ್ನು ಓದಿ 
ಮತ್ತು ಅದರ ಬಲಬದಿಯಲ್ಲಿ ಇರುವ ಅಂಕೆಗಳನ್ನು ನೋಡಿ .(ಈ ಸಮಯದಲ್ಲಿ ನಿಮಗೆ ಹೇಗೆ ಅನ್ನಿಸುತ್ತದೆ ಅನ್ನುವುದನ್ನು ಸರಿಯಾದ ಅಂಕೆಯ ಸುತ್ತ ವೃತ್ತವನ್ನು ಹಾಕುವುದರಿಂದ ಸೂಚಿಸಿ . ಇದರಲ್ಲಿ ಸರಿ ಅಥವಾ ತಪ್ಪು ಎಂಬ ಉತ್ತರಗಳಿಲ್ಲ ಒಂದೇ  ವಾಕ್ಯದ ಮೇಲೆ ಹೆಚ್ಚು ಸಮಯ ಕಳೆಯಬೇಡಿ . ಹಾಗೂ , ನಿಮ್ಮ ಈಗಿನ ಭಾವನೆಯನ್ನು ಸರಿಯಾಗಿ ವರ್ಣಿಸುವ ಉತ್ತರವನ್ನು ಕೊಡಿ . 
</b>
</p>
	<table border="1" class="responsive-scales">
	<thead>
                <tr>
					
				    <th></th>
                    <th>ಯಾವಾಗಲೂ ಇಲ್ಲ </th>
					<th>ಯಾವಾಗಲಾದರೂ </th>
                    <th>ಆಗಾಗ್ಗೆ </th>
					<th>ಯಾವಾಗಲೂ </th>
                    <th>Others Answres</th>
                    
                </tr> 
			</thead>
	<tbody>
		<tr>
		
		<td>21)ನನಗೆ ಆನಂದವಾಗಿರುವಂತೆ ಅನ್ನಿಸುತ್ತದೆ.</td>
		<?php for($i = 1;$i < 5;$i++):?>
		<td><?= $form->field($model, 'happy')->radio(['label' => $i, 'value' => $i])?></td>
		<?php endfor;?>
		<td>
		<?= $form->field($model, 'happy')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
		</td>
		</tr>
	<tr>
		
		<td>22)ನನಗೆ ದುರ್ಬಲತೆಯಿರುವಂತೆ ಹಾಗೂ ಚಡಪಡಿಕೆಯಾಗುವಂತೆ   ಅನ್ನಿಸುತ್ತದೆ.</td>
		<?php for($i = 1;$i < 5;$i++):?>	
    <td><?= $form->field($model, 'weak')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'weak')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
		</tr>
	
	<tr>
		
		<td>23)ನನಗೆ ನನ್ನಲ್ಲಿ ತೃಪ್ತಿಯಿರುವಂತೆ ಅನ್ನಿಸುತ್ತದೆ.</td>
		<?php for($i = 1;$i < 5;$i++):?>
		<td>
    <?= $form->field($model, 'thrupthi')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'thrupthi')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
		</tr>
	
	<tr>
		
		<td>24)ಬೇರೆಯವರು ಸಂತೋಷವಾಗಿರುವಂತೆ ನಾನು ಇರಲು ಬಯಸುತ್ತೇನೆ .</td>
		<?php for($i = 1;$i < 5;$i++):?>
		<td>
    <?= $form->field($model, 'happy_like_others')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'happy_like_others')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
		</tr>
	
	<tr>
		
		<td>25)ನಾನು ಸೋತಂತೆ ಅನ್ನಿಸುತ್ತದೆ.</td>
		<?php for($i = 1;$i < 5;$i++):?>
		<td>
    <?= $form->field($model, 'fail')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'fail')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
		</tr>
	
	<tr>
		
		<td>26)ನನಗೆ ಆರಾಮವಾದಂತೆ ಅನ್ನಿಸುತ್ತದೆ.</td>
		<?php for($i = 1;$i < 5;$i++):?>
		<td>
    <?= $form->field($model, 'free')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'free')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
		</tr>
	
	<tr>
		
		<td>27)ನಾನು ಶಾಂತಿಯಿಂದ ಹಾಗು ನೆಮ್ಮದಿಯಿಂದ ಇರುವೆನು </td>
		<?php for($i = 1;$i < 5;$i++):?>
		<td>
		<?= $form->field($model, 'safe')->radio(['label' => $i, 'value' => $i])?></td>
		<?php endfor;?>
		<td>
		<?= $form->field($model, 'safe')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
		</td>
		</tr>
	
	<tr>
		
		<td>28)ಕಷ್ಟಗಳೆಲ್ಲಾ ಹೆಚ್ಚುತ್ತಾ ಹೋಗಿ ಅವುಗಳನ್ನು ನಿವಾರಿಸಲು ಆಗದಂತೆ  ಅನ್ನಿಸುತ್ತದೆ.</td>
		<?php for($i = 1;$i < 5;$i++):?>
		<td>
    <?= $form->field($model, 'more_prob')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'more_prob')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
		</tr>
	
	<tr>
		
		<td>29) ನಾನು  ಅಷ್ಟೇನೂ ಮುಖ್ಯವಲ್ಲದ ವಿಚಾರಗಳ ಬಗ್ಗೆ ತುಂಬಾ ಯೋಚನೆ ಮಾಡುತ್ತೇನೆ .</td>
		<?php for($i = 1;$i < 5;$i++):?>
		<td>
    <?= $form->field($model, 'thinks_lot')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'thinks_lot')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
		</tr>
		
	
	<tr>
		
		<td>30)ನಾನು ಸಂತೋಷವಾಗಿರುವೆನು .</td>
		<?php for($i = 1;$i < 5;$i++):?>
		<td>
    <?= $form->field($model, 'happiest')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'happiest')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
		</tr>
	
	<tr>
		
		<td>31) ನನಗೆ ಅಡಚಣೆ ಉಂಟುಮಾಡುವ  ಯೋಚನೆಗಳಿವೆ.</td>
		<?php for($i = 1;$i < 5;$i++):?>
		<td>
    <?= $form->field($model, 'stopping_thoughts')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'stopping_thoughts')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
		</tr>
	
	<tr>
		
		<td>32)ನನಗೆ ಆತ್ಮವಿಶ್ವಾಸವಿಲ್ಲ.</td>
		<?php for($i = 1;$i < 5;$i++):?>
		<td>
    <?= $form->field($model, 'not_confident')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'not_confident')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
		</tr>
	
	<tr>
		
		<td>33)ನಾನು ಸುರಕ್ಷಿತವಾಗಿರುವೆನೆಂದು  ಅನ್ನಿಸುತ್ತದೆ.</td>
		<?php for($i = 1;$i < 5;$i++):?>
		<td>
    <?= $form->field($model, 'secure')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'secure')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
		</tr>
	
	<tr>
		
		<td>34)ನಾನು ಸುಲಭವಾಗಿ ನಿರ್ಧಾರಗಳನ್ನು ಮಾಡುವೆನು </td>
		<?php for($i = 1;$i < 5;$i++):?>
		<td>
    <?= $form->field($model, 'takes_decisions')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'takes_decisions')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
		</tr>
	
	<tr>
		
		<td>35 ) ನಾನು ಅಸಮರ್ಪಕನೆಂದು  ಅನ್ನಿಸುತ್ತದೆ </td>
		<?php for($i = 1;$i < 5;$i++):?>
		<td>
    <?= $form->field($model, 'asamarpaka')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'asamarpaka')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
		</tr>
	
	<tr>
		
		<td>36)ನಾನು ಸಂತುಷ್ಟನಾಗಿರುವೆನು </td>
		<?php for($i = 1;$i < 5;$i++):?>
		<td>
    <?= $form->field($model, 'santhushta')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'santhushta')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
	</tr>
	
	<tr>
		
		<td>37) ಕೆಲವು ಮುಖ್ಯವಲ್ಲದ ಯೋಚನೆಗಳು ಮತ್ತೆ ಮತ್ತೆ ಮನಸ್ಸಿಗೆ ಬರುತ್ತದೆ ಮತ್ತು ನನ್ನನ್ನು ಪೀಡಿಸುತ್ತದೆ </td>
		<?php for($i = 1;$i < 5;$i++):?>
		<td>
    <?= $form->field($model, 'notimp_thoughts')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'notimp_thoughts')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
	</tr>
	
	<tr>
		
		<td>38) ನಾನು ನಿರಾಶೆಗಳನ್ನು ಎಷ್ಟು ಗಂಭೀರವಾಗಿ ತೆಗೆದುಕೊಳ್ಳುತ್ತೇನೆಂದರೆ ,ಅವುಗಳನ್ನು ಮನಸ್ಸಿನಿಂದ ಆಚೆ ಹಾಕಲು ಆಗುವುದೇ ಇಲ್ಲ </td>
		<?php for($i = 1;$i < 5;$i++):?>
		<td>
    <?= $form->field($model, 'nirashe')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'nirashe')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
	</tr>
	
	<tr>
		
		<td>39) ನಾನೊಬ್ಬ ಧೃಡ ಮನಸ್ಸಿನ ವ್ಯಕ್ತಿ   </td>
		<?php for($i = 1;$i < 5;$i++):?>
		<td>
    <?= $form->field($model, 'dhrudmanassu')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'dhrudmanassu')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
	</tr>
	
	<tr>
		
		<td>40) ನನಗೆ ಸಂಬಂಧಪಟ್ಟ ಈಗಿನ ವಿಷಯಗಳ ಹಾಗು ಆಸಕ್ತಿಗಳ ಬಗ್ಗೆ ಯೋಚನೆ ಮಾಡಿದರೆ ನನಗೆ ಬಹಳ ಉದ್ವೇಗ (ಬಿಗಿತ ) ಮತ್ತು ಗಡಿಬಿಡಿ ಉಂಟಾಗುತ್ತದೆ  </td>
		<?php for($i = 1;$i < 5;$i++):?>
		<td>
    <?= $form->field($model, 'udvega')->radio(['label' => $i, 'value' => $i])?></td>
	<?php endfor;?>
	<td>
		<?= $form->field($model, 'udvega')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
	</td>
	</tr>
	
	
	
	</tbody>
	</table>
    </div>
		<div id="scaleflag" class="hidden">
		<?= $form->field($model, 'score')->input('number',['readonly'=>true])->label(false);?>
		</div>
    
	</div>
   <?php ActiveForm::end(); ?>

</div>

<?php $this->registerJs('
	$("#"+formObj.formName).one("change",function()
	{
		autoSaveObj = formObj;
	});
	
	$("#stai-not_app").change(function(){
		if($(this).val() == "YES")
		{
			setNotApplicableShow("#stai2","#stai-not_app");
		}
		else
		{
			setNotApplicableHide("#stai2","#stai-not_app");
		}
	});
	
')?>


<script type="text/javascript">
	var formObj = {url:"stai/ajax",formName : "formStai"};
    var calc = function () {
        var totalValue = 0;
		var root = 'stai';
        var id = '#'+root+'-';
		var clas = '.field-'+root+'-';
        var arr = ['feel_peace', 'feel_safe', 'feel_pressure', 'feel_tired', 'feel_nemmadi', 'feel_sad', 'feel_future_disaster', 
		'feel_enough', 'feel_fear', 'feel_free', 'feel_confident', 'feel_weak', 'feel_feared', 'cant_take_decisions', 'feel_samadhana',
		'feel_santhushti', 'feel_sadness', 'feel_galibili', 'feel_drudamanasssu', 'feel_happy', 'happy', 'weak', 'thrupthi',
		'happy_like_others', 'fail', 'free', 'safe', 'more_prob', 'thinks_lot', 'happiest', 'stopping_thoughts', 'not_confident',
		'secure', 'takes_decisions', 'asamarpaka', 'santhushta','notimp_thoughts','nirashe','dhrudmanassu','udvega'];

        for (var i in arr) {
            var val = $(clas + arr[i] + ' input[type=radio]:checked').val();
			if (parseInt(val)>=0) {
                totalValue += parseInt(val);
            }
        }
		
        $(id+'score').val(totalValue);
		autoSaveObj = formObj;
    }
	 
</script>
<style>
#notapp table{
	height:95px;
}
</style>

