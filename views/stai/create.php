<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Stai */

$this->title = 'Stai';
$this->params['breadcrumbs'][] = ['label' => 'Stais', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stai-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
