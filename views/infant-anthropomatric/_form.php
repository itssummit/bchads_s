<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$assessType = Yii::$app->session->get('assessmentType');
$keyPair = [-15 => '-15: Child Not Co-Operative For Assessment',
		  -14 => '-14: Not Available For Assessment',
		  -11 => '-11: Inadequate Information',
		  -10 => '-10: Not Applicable',
		  -9 => '-9: Missing',
		  -8 => '-8: Refused',
		  -7 => '-7: Partner Accompanied Missing',
		  -6 => '-6: Family Accompanied Missing',
		  -5 => '-5: Missing:Not Asked',
		  -3 => '-3: No Such Relationship/No Partner'];
/* @var $this yii\web\View */
/* @var $model app\models\InfantAnthropomatric */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="infant-anthropomatric-form">

    <?php $form = ActiveForm::begin(['id' => 'formInfantanthropomatric']); ?>
	<table class="responsive-scales" border  = "1" style = "display:<?php echo ($assessType == 'm6')?"display":"none";?>">
		<thead>
			<th>Measurements</th>
			<th>6 Months</th>
			<th>Others Answers</th>
		</thead>
		<tbody>
			<tr>
				<th>Length</th>
				<td><?= $form->field($model, 'length_6_months')->textInput()->label(false); ?></td>
				<td><?= $form->field($model, 'length_6_months_desc')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?></td>
			</tr>
			<tr>
				<th>Weight</th>
				<td><?= $form->field($model, 'weight_6_months')->textInput()->label(false); ?></td>
				<td><?= $form->field($model, 'weight_6_months_desc')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?></td>
			</tr>
			<tr>
				<th>Head Circumference</th>
				<td><?= $form->field($model, 'head_circumference_6_months')->textInput()->label(false); ?></td>
				<td><?= $form->field($model, 'head_circumference_6_months_desc')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?></td>
			</tr>
			<tr>
				<th>Chest Circumference</th>
				<td><?= $form->field($model, 'chest_circumference_6_months')->textInput()->label(false); ?></td>
				<td><?= $form->field($model, 'chest_circumference_6_months_desc')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?></td>
			</tr>
			<tr>
				<th>Midarm Circumference</th>
				<td><?= $form->field($model, 'midarm_circumference_6_months')->textInput()->label(false); ?></td>
				<td><?= $form->field($model, 'midarm_circumference_6_months_desc')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?></td>
			</tr>
			<tr>
				<th>Waist Circumference</th>
				<td><?= $form->field($model, 'waist_circumference_6_months')->textInput()->label(false); ?></td>
				<td><?= $form->field($model, 'waist_circumference_6_months_desc')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?></td>
			</tr>
			<tr>
				<th>Hip Circumference</th>
				<td><?= $form->field($model, 'hip_circumference_6_months')->textInput()->label(false); ?></td>
				<td><?= $form->field($model, 'hip_circumference_6_months_desc')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?></td>
			</tr>
			
		</tbody>
		
	</table><br>
	
	<table border  = "1" style = "display:<?php echo ($assessType == 'm12')?"display":"none";?>">
		<thead>
			<th> Measurements</th>
			<th>12 Months</th>
		</thead>
		<tbody>
			<tr>
				<th>Length</th>
				<td><?= $form->field($model, 'length_1_year')->textInput()->label(false); ?></td>
				<td><?= $form->field($model, 'length_1_year_desc')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?></td>
			</tr>
			<tr>
				<th>Weight</th>
				<td><?= $form->field($model, 'weight_1_year')->textInput()->label(false); ?></td>
				<td><?= $form->field($model, 'weight_1_year_desc')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?></td>
			</tr>
			<tr>
				<th>Head circumference</th>
				<td><?= $form->field($model, 'head_circumference_1_year')->textInput()->label(false); ?></td>
				<td><?= $form->field($model, 'head_circumference_1_year_desc')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?></td>
			</tr>
			<tr>
				<th>Chest circumference</th>
				<td><?= $form->field($model, 'chest_circumference_1_year')->textInput()->label(false); ?></td>
				<td><?= $form->field($model, 'chest_circumference_1_year_desc')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?></td>
			</tr>
			<tr>
				<th>Midarm circumference</th>
				<td><?= $form->field($model, 'midarm_circumference_1_year')->textInput()->label(false); ?></td>
				<td><?= $form->field($model, 'midarm_circumference_1_year_desc')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?></td>
			</tr>
			<tr>
				<th>Waist circumference</th>
				<td><?= $form->field($model, 'waist_circumference_1_year')->textInput()->label(false); ?></td>
				<td><?= $form->field($model, 'waist_circumference_1_year_desc')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?></td>
			</tr>
			<tr>
				<th>Hip circumference</th>
				<td><?= $form->field($model, 'hip_circumference_1_year')->textInput()->label(false); ?></td>
				<td><?= $form->field($model, 'hip_circumference_1_year_desc')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?></td>
			</tr>
			
		</tbody>
		
	</table><br>
	
	<table border  = "1" style = "display:<?php echo ($assessType == 'm24')?"display":"none";?>">
		<thead>
			<th> Measurements</th>
			<th>24 Months</th>
		</thead>
		<tbody>
			<tr>
				<th>Length</th>
				<td><?= $form->field($model, 'length_2_year')->textInput()->label(false); ?></td>
				<td><?= $form->field($model, 'length_2_year_desc')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?></td>
			</tr>
			<tr>
				<th>Weight</th>
				<td><?= $form->field($model, 'weight_2_year')->textInput()->label(false); ?></td>
				<td><?= $form->field($model, 'weight_2_year_desc')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?></td>
			</tr>
			<tr>
				<th>Head circumference</th>
				<td><?= $form->field($model, 'head_circumference_2_year')->textInput()->label(false); ?></td>
				<td><?= $form->field($model, 'head_circumference_2_year_desc')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?></td>
			</tr>
			<tr>
				<th>Chest circumference</th>
				<td><?= $form->field($model, 'chest_circumference_2_year')->textInput()->label(false); ?></td>
				<td><?= $form->field($model, 'chest_circumference_2_year_desc')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?></td>
			</tr>
			<tr>
				<th>Midarm circumference</th>
				<td><?= $form->field($model, 'midarm_circumference_2_year')->textInput()->label(false); ?></td>
				<td><?= $form->field($model, 'midarm_circumference_2_year_desc')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?></td>
			</tr>
			<tr>
				<th>Waist circumference</th>
				<td><?= $form->field($model, 'waist_circumference_2_year')->textInput()->label(false); ?></td>
				<td><?= $form->field($model, 'waist_circumference_2_year_desc')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?></td>
			</tr>
			<tr>
				<th>Hip circumference</th>
				<td><?= $form->field($model, 'hip_circumference_2_year')->textInput()->label(false); ?></td>
				<td><?= $form->field($model, 'hip_circumference_2_year_desc')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?></td>
			</tr>
			
		</tbody>
		
	</table><br>
	
    <?php ActiveForm::end(); ?>

</div>
<?php $this->registerJs('
	$("#formInfantanthropomatric").change(function(){
		autoSave({url : "infant-anthropomatric/ajax",formName : "formInfantanthropomatric"});
		//autoSaveObj = {url : "infant-anthropomatric/ajax",formName : "formInfantanthropomatric"};
	});
')?>
