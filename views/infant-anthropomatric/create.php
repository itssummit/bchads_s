<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\InfantAnthropomatric */

$this->title = 'Infant Anthropometric Measurements';
$this->params['breadcrumbs'][] = ['label' => 'Infant Anthropometric Measurements', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="infant-anthropomatric-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
