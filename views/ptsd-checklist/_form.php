﻿<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$keyPair = [-15 => '-15: Child Not Co-Operative For Assessment',
		  -14 => '-14: Not Available For Assessment',
		  -11 => '-11: Inadequate Information',
		  -10 => '-10: Not Applicable',
		  -9 => '-9: Missing',
		  -8 => '-8: Refused',
		  -7 => '-7: Partner Accompanied Missing',
		  -6 => '-6: Family Accompanied Missing',
		  -5 => '-5: Missing:Not Asked',
		  -3 => '-3: No Such Relationship/No Partner'];
/* @var $this yii\web\View */
/* @var $model app\models\PtsdChecklist */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile('@web/js/scales.js',['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="ptsd-checklist-form">

    <?php $form = ActiveForm::begin(['id' => 'formPtsdchecklist']); ?>

    <div id="withBoxShadow">
	
	
	<caption><b>ಕಳೆದ ಭೇಟಿಯಿಂದ  ಅಥವಾ ಕಳೆದ  ತಿಂಗಳಿನಿಂದ  ಈ ಕೆಳಗಿನವುಗಳಲ್ಲಿ  ಯಾವುದಾದರನ್ನೂ ಅನುಭವಿಸಿದ್ದೀರಾ ?</b></caption>
	<table border="1" class="responsive-scales">
		<thead>
                <tr>
				    <th></th>
                    <th>ಇಲ್ಲವೇ ಇಲ್ಲ </th>
                    <th>ಸ್ವಲ್ಪ ಮಟ್ಟಿಗೆ </th>
                    <th>ಸಾಧಾರಣವಾಗಿ </th>
                    <th>ಗಮನಾರ್ಹವಾಗಿ </th>
                    <th>ಅತ್ಯಂತ ಅತಿರೇಕ </th>
					<th>Other Answers</th>
                </tr> 
        </thead>
	<tbody>	
		<tr>
		<td>1) ಪದೇ ಪದೇ ಆಘಾತದ ನೆನಪುಗಳು ಮರುಕಳಿಸುವುದರಿಂದ</td>
		<?php for($i = 1;$i < 6;$i++):?>
		<td><?= $form->field($model, 'intrusive_recollect')->radio(['label' => $i, 'value' => $i])?>
				</td>
		<?php endfor;?>
		<td>
			<?= $form->field($model, 'intrusive_recollect')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
		</td>
		</tr>
		
		<tr>
		<td>2)   ಆಘಾತದ / ಮರೆಯಲಾಗದಂತಹ ಮರುಕಳಿಸುವ ನೆನಪುಗಳು</td>
		<?php for($i = 1;$i < 6;$i++):?>
		<td><?= $form->field($model, 'flashbacks')->radio(['label' => $i, 'value' => $i])?>
				</td>
		<?php endfor;?>
		<td>
			<?= $form->field($model, 'flashbacks')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
		</td>
		</tr>
		
		<tr>
		<td>3) ನೆನಪುಗಳಿಂದ ಬೇಜಾರು</td>
		<?php for($i = 1;$i < 6;$i++):?>
		<td><?= $form->field($model, 'upset_remainders')->radio(['label' => $i, 'value' => $i])?>
				</td>
		<?php endfor;?>
		<td>
			<?= $form->field($model, 'upset_remainders')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
		</td>
		</tr>
		
		<tr>
		<td>4) ಕಳವಳಗೊಳ್ಳುವ ಕನಸುಗಳು</td>
		<?php for($i = 1;$i < 6;$i++):?>
		<td><?= $form->field($model, 'distressing_dreams')->radio(['label' => $i, 'value' => $i])?>
				</td>
		<?php endfor;?>
		<td>
			<?= $form->field($model, 'distressing_dreams')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
		</td>
		</tr>
		
		<tr>
		<td>5) ನೆನಪುಗಳಿಗೆ ದೈಹಿಕ ಪ್ರತಿಕ್ರಿಯೆಗಳು</td>
		<?php for($i = 1;$i < 6;$i++):?>
		<td><?= $form->field($model, 'physical_remainders')->radio(['label' => $i, 'value' => $i])?>
				</td>
		<?php endfor;?>
		<td>
			<?= $form->field($model, 'physical_remainders')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
		</td>
		</tr>
		
		<tr>
		<td>6) ಯೋಚನೆಗಳಿಂದ ದೂರವಿರುವುದು/ತಪ್ಪಿಸಿಕೊಳ್ಳುವುದು</td>
		<?php for($i = 1;$i < 6;$i++):?>
		<td><?= $form->field($model, 'avoid_thoughts')->radio(['label' => $i, 'value' => $i])?>
				</td>
		<?php endfor;?>
		<td>
			<?= $form->field($model, 'avoid_thoughts')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
		</td>
		</tr>
		
		<tr>
		<td>7) ಜ್ಞಾಪಕ/ನೆನಪುಗಳಿಂದ ತಪ್ಪಿಸಿಕೊಳ್ಳುವುದು</td>
		<?php for($i = 1;$i < 6;$i++):?>
		<td><?= $form->field($model, 'avoid_remainders')->radio(['label' => $i, 'value' => $i])?>
				</td>
		<?php endfor;?>
		<td>
			<?= $form->field($model, 'avoid_remainders')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
		</td>
		</tr>
		
		<tr>
		<td>8) ಮಾನಸಿಕ ಮೂಲಗಳಿಂದ ಮರೆವು</td>
		<?php for($i = 1;$i < 6;$i++):?>
		<td><?= $form->field($model, 'psychogenic_amnesia')->radio(['label' => $i, 'value' => $i])?>
				</td>
		<?php endfor;?>
		<td>
			<?= $form->field($model, 'psychogenic_amnesia')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
		</td>
		</tr>
		
		<tr>
		<td>9) ಆನಂದ ನೀಡುತ್ತಿದ್ದಂತಹ  ಚಟುವಟಿಕೆಗಳು, ಈಗ ಸಂತೋಷ  ನೀಡುತ್ತಿಲ್ಲ </td>
		<?php for($i = 1;$i < 6;$i++):?>
		<td><?= $form->field($model, 'anhedonia')->radio(['label' => $i, 'value' => $i])?>
				</td>
		<?php endfor;?>
		<td>
			<?= $form->field($model, 'anhedonia')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
		</td>
		</tr>
		
		<tr>
		<td>10) ಇತರರಿಂದ ಆಗಲಿಕೆ </td>
		<?php for($i = 1;$i < 6;$i++):?>
		<td><?= $form->field($model, 'estrangement_others')->radio(['label' => $i, 'value' => $i])?>
				</td>
		<?php endfor;?>
		<td>
			<?= $form->field($model, 'estrangement_others')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
		</td>
		</tr>
		
		<tr>
		<td>11) ಮಾನಸಿಕವಾಗಿ ಸ್ತಬ್ಧಗೊಳ್ಳುವುದು</td>
		<?php for($i = 1;$i < 6;$i++):?>
		<td><?= $form->field($model, 'psychic_numbing')->radio(['label' => $i, 'value' => $i])?>
				</td>
		<?php endfor;?>
		<td>
			<?= $form->field($model, 'psychic_numbing')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
		</td>
		</tr>
		
		<tr>
		<td>12) ಭವಿಷ್ಯದಲ್ಲಿ ನಡೆಯಬಹುದಾದಂತಹ ಘಟನೆ, ಈಗಲೇ ನಡೆಯಬಹುದೆ೦ಬ ಅನಿಸಿಕೆ </td>
		<?php for($i = 1;$i < 6;$i++):?>
		<td><?= $form->field($model, 'foreshortened_future')->radio(['label' => $i, 'value' => $i])?>
				</td>
		<?php endfor;?>
		<td>
			<?= $form->field($model, 'foreshortened_future')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
		</td>
		</tr>
		
		<tr>
		<td>13) ನಿದ್ರೆಯ ತೊಂದರೆ </td>
		<?php for($i = 1;$i < 6;$i++):?>
		<td><?= $form->field($model, 'sleep_difficulty')->radio(['label' => $i, 'value' => $i])?>
				</td>
		<?php endfor;?>
		<td>
			<?= $form->field($model, 'sleep_difficulty')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
		</td>
		</tr>
		
		<tr>
		<td>14) ಕಿರಿ ಕಿರಿ / ಕೋಪ</td>
		<?php for($i = 1;$i < 6;$i++):?>
		<td><?= $form->field($model, 'irritability')->radio(['label' => $i, 'value' => $i])?>
				</td>
		<?php endfor;?>
		<td>
			<?= $form->field($model, 'irritability')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
		</td>
		</tr>
		
		<tr>
		<td>15) ದುರ್ಬಲಗೊಂಡ  ಏಕಾಗ್ರತೆ</td>
		<?php for($i = 1;$i < 6;$i++):?>
		<td><?= $form->field($model, 'concentrattion_impaired')->radio(['label' => $i, 'value' => $i])?>
				</td>
		<?php endfor;?>
		<td>
			<?= $form->field($model, 'concentrattion_impaired')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
		</td>
		</tr>
		
		<tr>
		<td>16) ಅತಿ ಜಾಗರೂಕ /ಎಚ್ಚರವಾಗಿರುವುದು </td>
		<?php for($i = 1;$i < 6;$i++):?>
		<td><?= $form->field($model, 'hyper_vigilant')->radio(['label' => $i, 'value' => $i])?>
				</td>
		<?php endfor;?>
		<td>
			<?= $form->field($model, 'hyper_vigilant')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
		</td>
		</tr>
		
		<tr>
		<td>17) ಅತಿಯಾಗಿ  ಚಕಿತ /ಆಶ್ಚರ್ಯಗೊಳ್ಳುವುದು</td>
		<?php for($i = 1;$i < 6;$i++):?>
		<td><?= $form->field($model, 'exaggerated_startle')->radio(['label' => $i, 'value' => $i])?>
				</td>
		<?php endfor;?>
		<td>
			<?= $form->field($model, 'exaggerated_startle')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
		</td>
		</tr>
		
		<tr>
		<td id="scaleflag" colspan="7" class="hidden"><?= $form->field($model, 'score')->input('number',['readonly'=>true])->label(false);?></td>
		</tr>
    </tbody>
	</table>
	
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $this->registerJs('
	$("#"+formObj.formName).one("change",function()
	{
		autoSaveObj = formObj;
	});
',\yii\web\View::POS_READY)?>

<script type="text/javascript">
	formObj = {url:"ptsd-checklist/ajax",formName : "formPtsdchecklist"};
    var calc = function () {
        var totalValue = 0;
		var root = 'ptsdchecklist';
        var id = '#'+root+'-';
		var clas = '.field-'+root+'-';
        var arr = ['intrusive_recollect','flashbacks','upset_remainders','distressing_dreams','physical_remainders', 'avoid_thoughts', 'avoid_remainders', 'psychogenic_amnesia', 'anhedonia', 'estrangement_others', 'psychic_numbing', 'foreshortened_future', 'sleep_difficulty', 'irritability', 'concentrattion_impaired', 'hyper_vigilant', 'exaggerated_startle'];

        for (var i in arr) {
            var val = $(clas + arr[i] + ' input[type=radio]:checked').val();
			if (parseInt(val)>=0) {
                totalValue += parseInt(val);
            }
        }
		
        $(id+'score').val(totalValue);
		autoSaveObj = formObj;
    }
</script>

