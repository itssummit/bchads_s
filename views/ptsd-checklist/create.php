<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PtsdChecklist */

$this->title = 'Ptsd Checklist';
$this->params['breadcrumbs'][] = ['label' => 'Ptsd Checklists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ptsd-checklist-create">

    <h1 align="center">ಪಿ ಟಿ ಎಸ್ ಡಿ ಪರೀಕ್ಷಾ ಪಟ್ಟಿ </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
