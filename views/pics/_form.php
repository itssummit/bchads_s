﻿<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$keyPair = [-15 => '-15: Child Not Co-Operative For Assessment',
          -14 => '-14: Not Available For Assessment',
          -11 => '-11: Inadequate Information',
          -10 => '-10: Not Applicable',
          -9 => '-9: Missing',
          -8 => '-8: Refused',
          -7 => '-7: Partner Accompanied Missing',
          -6 => '-6: Family Accompanied Missing',
          -5 => '-5: Missing:Not Asked',
          -3 => '-3: No Such Relationship/No Partner'];
/* @var $this yii\web\View */
/* @var $model app\models\Pics */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile('@web/js/scales.js',['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="pics-form">

    <?php $form = ActiveForm::begin(['id' => 'formPics']); ?>
    <div id="idpicsscale">
  <div id="withBoxShadow" class="col-xs-12 col-md-12 col-lg-12 col-sm-12">
  <div style="overflow-x:auto;">
    <table border="1" class="responsive-scales" >
    <thead>
        <tr>
        <th></th>
        <th>ಇಲ್ಲವೇ ಇಲ್ಲ  </th>
        <th>ವಿರಳವಾಗಿ </th>
        <th>ಕೆಲವೊಮ್ಮೆ </th>
        <th>ಪದೇ ಪದೇ </th>
        <th>ಬಹಳಷ್ಟು </th>
        <th>Other Answers</th>
        </tr>
    </thead>
    <tbody>
        <tr>
        <td>1)ನನ್ನ ಮಗುವು ಮುಟ್ಟುತ್ತಿದ್ದರೆ ಇಷ್ಟಪಡುತ್ತದೆ</td>
        <?php for($i = 1;$i < 6;$i++):?>
        
        <td><?= $form->field($model, 'like_touched')->radio(['label' => $i, 'value' => $i])?>
                </td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'like_touched')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </div>
        </td>
        </tr>
        
        <tr>
        <td>2)ನನ್ನ ಮಗುವು ಅವಳ/ಅವನೊಂದಿಗೆ ಮಾತನಾಡಿದರೆ ಇಷ್ಟಪಡುತ್ತದೆ.</td>
        <?php for($i = 1;$i < 6;$i++):?>
        <td><?= $form->field($model, 'like_talked')->radio(['label' => $i, 'value' => $i])?>
                </td>
            <?php endfor;?>
        <td>
        <?= $form->field($model, 'like_talked')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>   
        </tr>
        
        <tr>
        <td>3)ನನ್ನ ಮಗುವಿಗೆ ಅವನ /ಅವಳಿಗೆ ನಾನು ಹಾಡು ಹಾಡಿದರೆ ಇಷ್ಟ . </td>
        <?php for($i = 1;$i < 6;$i++):?>
        <td><?= $form->field($model, 'like_sing')->radio(['label' => $i, 'value' => $i])?>
                </td>
            <?php endfor;?>
        <td>
        <?= $form->field($model, 'like_sing')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>   
        </tr>
        
        <tr>
        <td>4)ನನ್ನ ಮಗುವಿಗೆ ಅವನ /ಅವಳನ್ನು ಒಂಟಿಯಾಗಿ ಬಿಟ್ಟರೆ ಇಷ್ಟ .</td>
        <?php for($i = 1;$i < 6;$i++):?>
        <td><?= $form->field($model, 'like_leave')->radio(['label' => $i, 'value' => $i])?>
                </td>
            <?php endfor;?>
        <td>
        <?= $form->field($model, 'like_leave')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>   
        </tr>
        
        <tr>
        <td>5)ನನ್ನ ಮಗುವಿಗೆ ಅವನ /ಅವಳನ್ನು ಎತ್ತಿಕೊಂಡರೆ ಇಷ್ಟ . </td>
        <?php for($i = 1;$i < 6;$i++):?>
        <td><?= $form->field($model, 'like_hold')->radio(['label' => $i, 'value' => $i])?>
                </td>
            <?php endfor;?>
        <td>
        <?= $form->field($model, 'like_hold')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>   
        </tr>
        
        <tr>
        <td>6)ನನ್ನ ಮಗುವು ,ನಾನು ಕೆಳಗಡೆ ಮಲಗಿಸಿದರೆ /ಬಿಟ್ಟರೆ ಇಷ್ಟಪಡುತ್ತದೆ . </td>
        <?php for($i = 1;$i < 6;$i++):?>
        <td><?= $form->field($model, 'like_put_down')->radio(['label' => $i, 'value' => $i])?>
                </td>
            <?php endfor;?>
        <td>
        <?= $form->field($model, 'like_put_down')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>   
        </tr>
        
        <tr>
        <td>7)ನನ್ನ ಮಗುವು ಕೃತಕ ನಿಪ್ಪಲ್ಲನ್ನು ಚೀಪಲು ಇಷ್ಟಪಡುತ್ತದೆ . </td>
        <?php for($i = 1;$i < 6;$i++):?>
        <td><?= $form->field($model, 'like_dummy')->radio(['label' => $i, 'value' => $i])?>
                </td>
            <?php endfor;?>
        <td>
        <?= $form->field($model, 'like_dummy')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>   
        </tr>
        
        <tr>
        <td>8)ನನ್ನ ಮಗುವು ತನ್ನ ಕೈಯನ್ನು ತಾನೇ ಚೀಪಲು ಪ್ರಯತ್ನಿಸುತ್ತದೆ    </td>
        <?php for($i = 1;$i < 6;$i++):?>
        <td><?= $form->field($model, 'suck_hands')->radio(['label' => $i, 'value' => $i])?>
                </td>
            <?php endfor;?>
        <td>
        <?= $form->field($model, 'suck_hands')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>   
        </tr>
    </tbody>
</table>
</div>
        
    <br><br>
    <div style="overflow-x:auto;">
    <table border="1" class="responsive-scales">
    <thead>
            <tr>
        <th></th>
        <th>ಇಲ್ಲವೇ ಇಲ್ಲ  </th>
        <th>ವಿರಳವಾಗಿ </th>
        <th>ಕೆಲವೊಮ್ಮೆ </th>
        <th>ಪದೇ ಪದೇ </th>
        <th>ಬಹಳಷ್ಟು </th>
        <th>Other Answers</th>
        </tr>
    </thead>
    <tbody>
            <tr>
            <td>1)ನಾನು ನನ್ನ ಮಗುವಿನ ಹೊಟ್ಟೆಯನ್ನು ಸವರುತ್ತೇನೆ . </td>
            <?php for($i = 1;$i < 6;$i++):?>
            <td><?= $form->field($model, 'stroke_tummy')->radio(['label' => $i, 'value' => $i])?>
            </td>
            <?php endfor;?>
            <td>
            <?= $form->field($model, 'stroke_tummy')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </div>
            </td>   
            </tr>
            
            <tr>
            <td>2)ನಾನು ನನ್ನ ಮಗುವಿನ  ಬೆನ್ನನ್ನು ಸವರುತ್ತೇನೆ . </td>
            <?php for($i = 1;$i < 6;$i++):?>
        <td><?= $form->field($model, 'stroke_back')->radio(['label' => $i, 'value' => $i])?>
                </td>
            <?php endfor;?>
            <td>
                <?= $form->field($model, 'stroke_back')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
            </td>
            </tr>
            
            <tr>
            <td>3)ನಾನು ನನ್ನ ಮಗುವಿನ  ಮುಖವನ್ನು  ಸವರುತ್ತೇನೆ . </td>
            <?php for($i = 1;$i < 6;$i++):?>
        <td><?= $form->field($model, 'stroke_face')->radio(['label' => $i, 'value' => $i])?>
                </td>
            <?php endfor;?>
            <td>
                    <?= $form->field($model, 'stroke_face')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
            </td>
            </tr>
            
            <tr>
            <td>4)ನಾನು ನನ್ನ ಮಗುವಿನ ಕೈಕಾಲುಗಳನ್ನು ಸವರುತ್ತೇನೆ .</td>
            <?php for($i = 1;$i < 6;$i++):?>
        <td><?= $form->field($model, 'stroke_arms')->radio(['label' => $i, 'value' => $i])?>
                </td>
            <?php endfor;?>
            <td>
            <?= $form->field($model, 'stroke_arms')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
            </td>
            </tr>
            
            <tr>
            <td>5)ನಾನು ನನ್ನ ಮಗುವನ್ನು ಎತ್ತಿಕೊಳ್ಳುತ್ತೇನೆ .</td>
            <?php for($i = 1;$i < 6;$i++):?>
        <td><?= $form->field($model, 'pick_up')->radio(['label' => $i, 'value' => $i])?>
                </td>
            <?php endfor;?>
            <td>
            <?= $form->field($model, 'pick_up')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
            </td>
            </tr>
            
            <tr>
            <td>6)ನಾನು ಮಗುವಿನೊಂದಿಗೆ ಮಾತನಾಡುತ್ತೇನೆ. </td>
            <?php for($i = 1;$i < 6;$i++):?>
        <td><?= $form->field($model, 'talk')->radio(['label' => $i, 'value' => $i])?>
                </td>
            <?php endfor;?>
            <td>
            <?= $form->field($model, 'talk')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
            </td>
            </tr>
            
            <tr>
            <td>7)ನಾನು ನನ್ನ ಮಗುವನ್ನು ಅಪ್ಪಿಕೊಳ್ಳುತ್ತೇನೆ </td>
            <?php for($i = 1;$i < 6;$i++):?>
        <td><?= $form->field($model, 'cuddle')->radio(['label' => $i, 'value' => $i])?>
                </td>
            <?php endfor;?>
            <td>
            <?= $form->field($model, 'cuddle')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
            </td>
            </tr>
            
            <tr>
            <td>8)ನಾನು ಮಗುವನ್ನು ತೂಗುತ್ತೇನೆ </td>
            <?php for($i = 1;$i < 6;$i++):?>
        <td><?= $form->field($model, 'rock')->radio(['label' => $i, 'value' => $i])?>
                </td>
            <?php endfor;?>
            <td>
            <?= $form->field($model, 'rock')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
            </td>
            </tr>
            
            <tr>
            <td>9)ನಾನು ಮಗುವಿಗೆ ಮುತ್ತು ಕೊಡುತ್ತೇನೆ </td>
            <?php for($i = 1;$i < 6;$i++):?>
        <td><?= $form->field($model, 'kiss')->radio(['label' => $i, 'value' => $i])?>
                </td>
            <?php endfor;?>
            <td>
            <?= $form->field($model, 'kiss')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
            </td>
            </tr>
            
            <tr>
            <td>10)ನಾನು ಮಗುವನ್ನು ಹಿಡಿದುಕೊಂಡಿರುತ್ತೇನೆ </td>
            <?php for($i = 1;$i < 6;$i++):?>
        <td><?= $form->field($model, 'hold')->radio(['label' => $i, 'value' => $i])?>
                </td>
            <?php endfor;?>
            <td>
            <?= $form->field($model, 'hold')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
            </td>
            </tr>
            
            <tr>
            <td>11)ನಾನು ನನ್ನ ಮಗುವನ್ನು ಗಮನಿಸುತ್ತಿರುತ್ತೇನೆ </td>
            <?php for($i = 1;$i < 6;$i++):?>
        <td><?= $form->field($model, 'watch')->radio(['label' => $i, 'value' => $i])?>
                </td>
            <?php endfor;?>
            <td>
            <?= $form->field($model, 'watch')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
            </td>
            </tr>
            
            <tr>
            <td>12)ನಾನು ನನ್ನ ಮಗುವನ್ನು ಕೆಳಗಡೆ ಮಲಗಲು ಬಿಡುತ್ತೇನೆ </td>
            <?php for($i = 1;$i < 6;$i++):?>
        <td><?= $form->field($model, 'leave_lie_down')->radio(['label' => $i, 'value' => $i])?>
                </td>
            <?php endfor;?>
            <td>
            <?= $form->field($model, 'leave_lie_down')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
            </td>
            </tr>
            
            
            
            
            
   
    </tbody>
    </table>
    </div>
    <div class="col-lg-12">
    
        <b>ಎಲ್ಲಾದಿಕ್ಕಿಂತ ಹೆಚ್ಚಾಗಿ ನನ್ನ ಮಗುವು </b>
        
        <p><b><?= $form->field($model, 'best_of_likes')->textInput()->label(false);?>ಇಷ್ಟಪಡುತ್ತೇನೆ</b></p>
        
    </div>
    <div id="scaleflag" class="hidden">
    <?= $form->field($model, 'score')->input('number',['readonly'=>true])->label(false);?>
    </div>

    </div>
    </div>
      <?php ActiveForm::end(); ?>
</div>
<?php $this->registerJs('
    $("#"+formObj.formName).one("change",function()
    {
        autoSaveObj = formObj;
    });
',\yii\web\View::POS_READY)?>
<script type="text/javascript">
    var formObj = {url:"pics/ajax",formName : "formPics"};
    var calc = function () {
        var totalValue = 0;
        var root = 'pics';
        var id = '#'+root+'-';
        var clas = '.field-'+root+'-';
        var arr = ['like_touched','like_talked','like_sing','like_leave','like_hold','like_put_down','like_dummy',
        'suck_hands','stroke_tummy','stroke_back','stroke_face','stroke_arms','pick_up','talk','cuddle','rock','kiss',
        'hold','watch','leave_lie_down'];
        
        for (var i in arr) {
            var val = $(clas + arr[i] + ' input[type=radio]:checked').val();
            if (parseInt(val)>=0) {
                totalValue += parseInt(val);
            }
        }
        
        $(id+'score').val(totalValue);
        autoSaveObj = formObj;
    }
</script>

