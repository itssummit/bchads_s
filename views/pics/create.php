<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Pics */

$this->title = 'Pics';
$this->params['breadcrumbs'][] = ['label' => 'Pics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pics-create">

    <h1 align="center">PARENT INFANT CAREGIVING SCALE</h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
