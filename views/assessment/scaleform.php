<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\ButtonDropdown;
use app\models\Scale;
$motherId = Yii::$app->session->get('motherId');
$assessmentId = Yii::$app->session->get('assessmentId');
$this->registerJsFile('@web/jsControllers/scaleController.js',['depends' => [\app\assets\AppAsset::className()]]);
?>
<div ng-controller="ScaleController">
	<div class = "row">
		<div class="text-center col-md-10 col-lg-10">
			<h2>List of Scales</h2>
	        <div class="text-center" class = "col-md-10 col-lg-10">
	            <div class="col-md-1 col-lg-1">
					<button type = "button" class = "btn btn-primary">Filter</button>
				</div>
				<div class="col-md-2 col-lg-2">    
                    <select class = "form-control" ng-model = "search.type">
						<option value="">ALL</option>
				        <option value = "Maternal">Maternal</option>
				        <option value="Child">Child</option>
			        </select>
				</div>
				<div class="col-md-2 col-lg-2">
			        <select class = "form-control" ng-model = "search.respId">
						<option value="1">Mother</option>
				        <option value = "2">Interviewer</option>
				        <option value="3">CareGiver</option>
			        </select>
			 	</div>
	        </div>
		</div>
	</div>
	<div id="withBoxShadow" class="col-md-12 col-lg-12 col-xs-12 col-sm-12">	
	    <div class="row">
	        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" id="scaleId"
	             ng-class="{'clear-li': ($index % 3 === 0)}" ng-repeat="s in scales| filter : filterMatch(search)" ng-click="scaleFun(s)" style ="cursor: pointer">
	               <b>{{s.scale_name}}</b>
	                <div ng-if="s.value.count" class="progress xs">
	                    <div class="progress-bar progress-bar-green" style="width:{{s.value.count+'%'}}"
	                         role="progressbar" aria-valuenow="{{s.value.count}}" aria-valuemin="0" aria-valuemax="100">{{s.value.count}}</div>
	                </div>
						<label><div ng-if="s.value.score">{{s.value.count1}} Answered Out Of {{s.value.totalcount}}</div></label><br>
				<div>
	            	<label ng-if="s.value.score">Total Score : {{s.value.score}}</label>
				</div>
	        </div>
		</div>
	</div>
	<div class="row">
	    <div class="row col-xs-2 col-sm-2 col-lg-2 col-md-2 pull-right">
	        <a href="?r=mother/update&id=<?php echo $motherId?>" class="btn btn-info">Back</a>
	        <a href="index.php" class="btn btn-info">Home</a>
	    </div>
	</div>	
</div>