<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Assessment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="assessment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'respondent_id')->textInput() ?>

    <?= $form->field($model, 'created_dtm')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_updated_dtm')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'updated_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'visit_date')->textInput(['maxlength' => true]) ?>
<div class="row">
	<div class="col-lg-6">
    <?= $form->field($model, 'assessment_type')->dropDownList(['m6' =>'m6','m12' => 'm12','' => 'm24'])->label(false); ?>
	</div>
</div>
<div class="row">	
	<div class="col-lg-6">
    <?= $form->field($model, 'respondent')->textInput(['maxlength' => true]) ?>
	</div>
</div>	
<div class="row">
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>
    <?php ActiveForm::end(); ?>

</div>
