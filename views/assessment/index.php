<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\Assessment */
/* @var $form yii\widgets\ActiveForm */
?>

<button class="col-lg-12 col-md-12 col-sm-12 col-xs-12 btn btn-primary" data-toggle="collapse" data-target="#demo">6th month</button>

<div id="demo" class="collapse">
	<?php echo $this->render('/sixth-month/_form',['model' => $sixthModel]);?>
</div>


<button class="col-lg-12 col-md-12 col-sm-12 col-xs-12 btn btn-primary" data-toggle="collapse" data-target="#demo1">12th month</button>

<div id="demo1" class="collapse">
	<?php echo $this->render('/twelveth-month/_form',['model' => $twelvethModel]);?>
</div>


<button class="col-lg-12 col-md-12 col-sm-12 col-xs-12 btn btn-primary" data-toggle="collapse" data-target="#demo2">24th month</button>

<div id="demo2" class="collapse">
	<?php echo $this->render('/twentyfourth-month/_form',['model' => $twentyfourthModel]);?>
</div>


