<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ParentalBondingFather */

$this->title = 'Parental Bonding Father';
$this->params['breadcrumbs'][] = ['label' => 'Parental Bonding Fathers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parental-bonding-father-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
