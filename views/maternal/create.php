<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Maternal */

$this->title = 'Maternal';
$this->params['breadcrumbs'][] = ['label' => 'Maternals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="maternal-create">

    <h1 align="center">Maternal Health Checklist</h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
