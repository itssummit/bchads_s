<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
if($model->last_mensural_date)
{
	$model->last_mensural_date = date('d-M-Y',strtotime($model->last_mensural_date));
}
if($model->dob_baby)
{
	$model->dob_baby = date('d-M-Y',strtotime($model->dob_baby));
}
$val= [1=>1,2=>2,3=>3];
$radioOpt1 = [0 => 'No',1 => 'yes,but I have not had treatment',2 => 'yes,treated by GP',3 => 'yes,treated as hospital Outpatient',4 => 'yes,required hospital admission'];
$radioOpt2 = [1 => 'I am still pregnant', 2 => 'I had miscarriege', 3 => 'I had a termination in pregnancy',4 => 'I have given birth to baby'];
$radioOpt3 = [1 => 'none',2 => '1-3 hours',3 => '4-5 hours',4 => '6-7 hours',5 => 'over 7 hours'];

function getTemplate($index, $label, $name, $checked, $value){
	$a = 1;
	$checked = ($checked)?" checked":" ";
	$return = '<label class="modal-radio">';
	$return .= '<input type="radio"  name="' . $name . '"  value="' . $value .'"'.$checked.' tabindex="3" onclick = "calc()">';
	$return .= '<i></i>';
	$return .= '<span> ' .$label. '</span>';
	$return .= '</label><br>';
	return $return;
	$a = $a + 1;
}

/* @var $this yii\web\View */
/* @var $model app\models\Maternal */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile('@web/js/scalesome.js',['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="maternal-form">
	
    <?php $form = ActiveForm::begin(['id' => 'formMaternal']); ?>
	
		<div id="withBoxShadow" class="col-xs-12 col-md-12 col-lg-12 col-sm-12">
		
		<div class="col-lg-4 pull-right">
		<h4><b>Is This Scale Applicable ?</b></h4><?php echo $form->field($model, 'not_applicable')->dropDownList(['' =>'','YES' => 'Not Applicable', 'NO' => 'Applicable'],['class' => 'form-control scaleApplicable'])->label(false); ?>
		</div>
		
		
		<div style="margin:20px;">
    
	<div class="row">
	<div class="col-lg-12 col-md-12">
	<h4><b>1.Have you had any physical problems since your baby was born which require medical attention  </b></h4>
	<?= $form->field($model, 'physical_health_problems')->radioList($radioOpt1,['item' => function($index, $label, $name, $checked, $value) {																				return getTemplate($index, $label, $name, $checked, $value);
										}])?>
	</div>
	</div>
	<div class="row">
	<div class="col-lg-12 col-md-12">
	<label>Please Give Details</label>
	<?= $form->field($model, 'physical_health_problems_det')->textarea(['rows' => '3'])->label(false) ?>
	
	</div>
	</div>
	<div class="row">
	<div class="col-lg-12 col-md-12">
	<h4><b>2.Have you had any emotional problems since your baby was born which require help</b></h4>
    <?= $form->field($model, 'emotional_problem')->radioList($radioOpt1,['item' => function($index, $label, $name, $checked, $value) {
																				return getTemplate($index, $label, $name, $checked, $value);}])?>
	
	</div>
	</div>
	
	<div class="row">
	<div class="col-lg-12 col-md-12">
	<label>Please Give Details</label>
	<?= $form->field($model, 'emotional_problem_det')->textarea(['rows' => '5'])->label(false) ?>
	
	</div>
	</div>
	
	
	
	<div class="row">
	<h4><b>3.Since your baby`s birth have taken medicines prescribed by doctor ?</b></h4>
    <?= $form->field($model, 'medicines_taken')->radioList([1=>'yes',0=>'no'],['itemOptions'=>['onChange' => "calc()"]])->label(false);?>
	</div>
	
	<div class="row">
	<div class="col-lg-12 col-md-12">
	<label>Please Give Details</label>
	<?= $form->field($model, 'medicines_taken_det')->textarea(['rows' => '5'])->label(false) ?>
	
	</div>
	</div>
	
	
    <h4><b>4.Since your baby was born have you taken any of the following sort of medicine</b></h4>
	<table border="1" style="width:100%;height:250px;">
	<thead>
	<tr>
		<th>Please select an option to say how often</th>
		<th>yes,in the first 6 months </th>
		<th>yes,between 6 and 12 months after birth</th>
		<th>no,not since my baby was born </th>
	</thead>
	<tbody>
		<tr>	
				<td><label>Sleeping pills</label></td>
				<td><?= $form->field($model, 'sleepin_pills')->checkbox(array('label'=>'1'));?></td>
				<td><?= $form->field($model, 'sleepin_pills2')->checkbox(array('label'=>'2'));?></td>
				<td><?= $form->field($model, 'sleepin_pills3')->checkbox(array('label'=>'3'));?></td>
				<!--< ?= $form->field($model, 'sleepin_pills')->checkboxList($val,
				['item'=> function($index,$label,$name,$checked,$value){
						return "<td>".Html::checkbox($name,$checked,['value' => $value,'label'=>$label])."</td>";
					}
				]); ?>-->
				
		</tr>
		
		<tr>	
				<td><label>Pills to treat depression</label></td>
				<td><?= $form->field($model, 'depression_pills')->checkbox(array('label'=>'1'));?></td>
				<td><?= $form->field($model, 'depression_pills2')->checkbox(array('label'=>'2'));?></td>
				<td><?= $form->field($model, 'depression_pills3')->checkbox(array('label'=>'3'));?></td>
				<!--< ?= $form->field($model, 'depression_pills')->checkboxList($val,
				['item'=> function($index,$label,$name,$checked,$value){
						return "<td>".Html::checkbox($name,$checked,['value' => $value,'label'=>$label])."</td>";
					}
				]); ?>-->
				
		</tr>
		
		<tr>	
				<td><label>Pills to calm anxiety</label></td>
				<td><?= $form->field($model, 'anxiety_pills')->checkbox(array('label'=>'1'));?></td>
				<td><?= $form->field($model, 'anxiety_pills2')->checkbox(array('label'=>'2'));?></td>
				<td><?= $form->field($model, 'anxiety_pills3')->checkbox(array('label'=>'3'));?></td>
				<!--< ?= $form->field($model, 'anxiety_pills')->checkboxList($val,
				['item'=> function($index,$label,$name,$checked,$value){
						return "<td>".Html::checkbox($name,$checked,['value' => $value,'label'=>$label])."</td>";
					}
				]); ?>-->
				
		</tr>
		
		
	</tbody>
	</table><br>
	<div class="row">
	<div class="col-lg-6 col-md-6">
	<h4><b>5.Do you feel that you are getting enough sleep</b></h4>
    <?= $form->field($model, 'sleep')->radioList([0=>'yes',1=>'no'],['itemOptions'=>['onChange' => "calc()"]])->label(false);?>
	</div>
	</div>
	
	<div class="row">
	<div class="col-lg-8 col-md-8">
	<h4><b>6.During an average night how many hours sleep you will get</b></h4>
	<?= $form->field($model, 'hours_of_sleep')->radioList($radioOpt3,['item' => function($index, $label, $name, $checked, $value) {
																				return getTemplate($index, $label, $name, $checked, $value);}])?>
    
	</div>
	</div>
	
	<div class="row">
	<div class="col-lg-8 col-md-8">
	<h4><b>7.Have you experienced persistence pain in last six months</b></h4>
    <?= $form->field($model, 'persistant_pain')->radioList([1=>'yes',0=>'no'],['itemOptions'=>['onChange' => "calc()",'class'=>'persistant_pain']])->label(false);?>
	</div>
	</div>
	
	
	<div class="col-lg-12 col-md-12" id="sev" style = "display:<?php echo ($model->persistant_pain == 1)?"display":"none";?>" >
	<div class="col-lg-3 col-md-3">
	<?= $form->field($model, 'persistant_pain_dur',['options' => ['title' => 'duration']])->textInput()->label('Duration') ?>
	</div>
	
	<div class="col-lg-3 col-md-3">
	<?= $form->field($model, 'persistant_pain_site',['options' => ['title' => 'site']])->textInput()->label('Site') ?>
	</div>
	</div>
	

	<div class="row">
	<div class="col-lg-8 col-md-8">
	<h4><b>8(a).Since your first baby born have you become pregnant</b></h4>
    <?= $form->field($model, 'pregnant_aftr_firstborn')->radioList([1=>'yes',0=>'no'],['itemOptions'=>['onChange' => "calc()",'class'=>'pregnant_aftr_firstborn']])->label(false);?>
	
	</div>
	</div>
<div id = "8b" style = "display:<?php echo ($model->pregnant_aftr_firstborn == 1)?"display":"none";?>">
	<div class="row">
	<div class="col-lg-6 col-md-6">
	<h4><b>8(b).Last Menstrual Date</b></h4>
	</div>
	</div>
	
	<div class="row">
	<div class="col-lg-6 col-md-6">
		<?= $form->field($model, 'last_mensural_date')->widget(DatePicker::className(),['type' => DatePicker::TYPE_COMPONENT_APPEND,'removeButton' => false,'pluginOptions'=>['endDate' => '+0d','format' => 'dd-M-yyyy','orientation' => 'left','id' => 'demo']])->label(false); ?>	
	</div>
	</div>
	
	
	
	
	<div class="row">
	<div class="col-lg-8 col-md-8">
	<h4><b>8(c).What happened to pregnancy</b></h4>
	<?= $form->field($model, 'status_presentpregnancy')->radioList($radioOpt2,['item' => function($index, $label, $name, $checked, $value) {
																				return getTemplate($index, $label, $name, $checked, $value);}])?>
    
	</div>
	</div>
	
	
	<div class="row">
	<div class="col-lg-6 col-md-6">
	<h4><b>8(d).Baby`s DOB</b></h4>
		<?= $form->field($model, 'dob_baby')->widget(DatePicker::className(),['type' => DatePicker::TYPE_COMPONENT_APPEND,'removeButton' => false,'pluginOptions'=>['endDate' => '+0d','format' => 'dd-M-yyyy','orientation' => 'left']])->label(false); ?>	
	</div>
	</div>
	
	<div class="row">
	<div class="col-lg-8 col-md-8">
	<h4><b>8(e).Baby`s Sex</b></h4>
    
	<?= $form->field($model, 'sex_baby')->radioList([1 => 'Male',2 => 'Female'],['item' => function($index, $label, $name, $checked, $value) {
																				return getTemplate($index, $label, $name, $checked, $value);}])?>
	</div>
	</div>
</div>
	<div id="scaleflag" class="hidden">
    <?= $form->field($model, 'score')->input('number',['readonly'=>true])->label(false);?>
	</div>
	
	</div>
    
	</div>
    </div>


<?php $this->registerJs('
	$("#"+formObj.formName).one("change",function()
	{
		autoSaveObj = formObj;
	});
')?>
<?php $this->registerJs('
	
	$(".persistant_pain").click(function()
	{
		 if($(this).val() == 1){
		 	$("#maternal-persistant_pain_dur").val("");
		 	$("#maternal-persistant_pain_site").val("");
			 $("#sev").show();
		 }
		 else{
		 	$("#maternal-persistant_pain_dur").val("-10");
		 	$("#maternal-persistant_pain_site").val("-10");
			 $("#sev").hide();
		 }
	});
	
	$(".pregnant_aftr_firstborn").click(function()
	{
		 if($(this).val() == 0){
		 	$("#maternal-dob_baby").val("-10");
		 	$("#maternal-status_presentpregnancy").val("-10");
		 	$("#maternal-last_mensural_date-kvdate").val("-10");
		 	$("#maternal-last_mensural_date").val("-10");
			 $("#8b").hide();
		 }
		 else{
		 	$("#maternal-dob_baby").val("");
		 	$("#maternal-status_presentpregnancy").val("");
		 	$("#maternal-last_mensural_date-kvdate").val("");
		 	$("#maternal-last_mensural_date").val("");
		 	$("#maternal-sex_baby").prop("checked", false);
			$("#8b").show();
		 }
	});
')?>

<?php if($model->not_applicable == 'YES'){
	$this->registerJs('
	setNotApplicableOnLoad("#withBoxShadow","#maternal-not_applicable");
	', \yii\web\View::POS_READY);

}
	$this->registerJs('

	$("#maternal-not_applicable").change(function()
		{	
			var value = $(this).val();
			if(value=="YES")
			{
			//var value = $(document).val();
	$(document).find(":input:not(#psychosocialriskfactors-not_applicable)").val("-10");
	$(document).find("input[type=\'radio\']").prop("checked",false);
	$(document).find("input[type=\'checkbox\']").prop("checked",false);
	$(this).val(value);
	autoSave(formObj);
	$(document).find(":input:not(#psychosocialriskfactors-not_applicable)").attr("disabled",true);
	$(document).attr("disabled",false);
	 location.reload();
			}
			else
			{
			//var value = $(document).val();
				
	$(document).find(":input:not(#psychosocialriskfactors-not_applicable)").val("");
	$(document).find("input[type=\'radio\']").prop("checked",false);
	$(document).find("input[type=\'checkbox\']").prop("checked",false);
	$(document).find(":input:not(#psychosocialriskfactors-not_applicable)").attr("disabled",false);
	$(this).val(value);
	autoSave(formObj);
	$(document).attr("disabled",false);
	 location.reload();
					}
		});


		$("input[name=\"Maternal[physical_health_problems]\"]").change(function(){

			if(($(this).val() == 0 ))
			{
				$("#maternal-physical_health_problems_det").val("-10");
			}
			else
			{
				$("#maternal-physical_health_problems_det").val("");
			}
			});

			$("input[name=\"Maternal[emotional_problem]\"]").change(function(){

			if(($(this).val() == 0 ))
			{
				$("#maternal-emotional_problem_det").val("-10");
			}
			else
			{
				$("#maternal-emotional_problem_det").val("");
			}
			});


			$("input[name=\"Maternal[medicines_taken]\"]").change(function(){

			if(($(this).val() == 0 ))
			{
				$("#maternal-medicines_taken_det").val("-10");
			}
			else
			{
				$("#maternal-medicines_taken_det").val("");
			}
			});





		
			
', \yii\web\View::POS_READY);
?>
<script type="text/javascript">
		var formObj = {'url' : "maternal/ajax",'formName' : "formMaternal","main":true};
		var calc = function () {
        var totalValue = 0;
        var root= 'maternal';
		var id = '#'+root+'-';
		var clas = '.field-'+root+'-';
        var arr = ['physical_health_problems','emotional_problem','medicines_taken','sleep','hours_of_sleep',
		'persistant_pain','pregnant_aftr_firstborn','status_presentpregnancy','sleepin_pills','depression_pills','anxiety_pills'];
        
        for (var i in arr) {
            var val = $(clas + arr[i] + ' input[type=radio]:checked').val();
			if (parseInt(val)) {
                totalValue += parseInt(val);
            }
        }
        $(id+'score').val(totalValue)
				autoSaveObj = formObj;

    }
</script>

