<?php

$this->registerJsFile('@web/jsControllers/dataConversionController.js',['depends' => [\app\assets\AppAsset::className()]]);
?>

<div class="col-md-12 col-xs-12 col-sm-12 col-lg-12" ng-controller = "dataConversionController" ng-init="Index()">
    <div class="row">
    	<div class="col-sm-2">
    		Table Names : 
    	</div>
    	<!--<div class="col-sm-10">
           
             <div class="row minMarginTop">
                <div class="col-sm-2">
                    Mother ID : 
                </div>
                <div class="col-sm-10">
                    <input type="text" name="mother_id" ng-model="mother_id" /><br>
                </div>
            </div>
       -->
         <input type="text" name="tableNames" ng-model="tableNames">
            <div class="row minMarginTop">
                <div class="col-sm-10 col-sm-offset-2">
                    <button class="btn btn-primary" ng-click="Retrieve()">Retrieve</button>
                </div>
            </div>
        </div>
         <div class="col-sm-10">
             <div ng-repeat="t in tables">
                  <label><input type="checkbox" ng-model="usertables.selected[t.id]" ng-true-value="'{{t.name}}'" ng-false-value="''">{{t.name}}</label>
             </div>
        </div>
      <div class="col-sm-10">
            <label>Select table to choose column:</label><br><br>
            <div class="col-md-5">
                <select class="form-control" name="displaycol" ng-model="selectedtable" ng-options = " val for (key, val) in usertables.selected">
                        <option  value = " ">{{val}}  </option>
			    </select>
            </div>
           <button class="btn btn-default" ng-click="submitt()">Submit</button>
           <div ng-repeat="t in columnList">
               {{t}}
            </div>
	       <div id="dvjson"></div>

       </div>
    </div>
</div>

<style type="text/css">
	
	.minMarginTop{
		margin-top: 10px;
	}
</style>