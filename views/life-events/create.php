<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LifeEvents */

$this->title = 'Life Events';
$this->params['breadcrumbs'][] = ['label' => 'Life Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="life-events-create">

    <h1 align="center"> ಜೀವನದಲ್ಲಿ ನಡೆದ ಘಟನೆಗಳ ಪಟ್ಟಿ  </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
