﻿<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$keyPair = [-15 => '-15: Child Not Co-Operative For Assessment',
          -14 => '-14: Not Available For Assessment',
          -11 => '-11: Inadequate Information',
          -10 => '-10: Not Applicable',
          -9 => '-9: Missing',
          -8 => '-8: Refused',
          -7 => '-7: Partner Accompanied Missing',
          -6 => '-6: Family Accompanied Missing',
          -5 => '-5: Missing:Not Asked',
          -3 => '-3: No Such Relationship/No Partner'];
/* @var $this yii\web\View */
/* @var $model app\models\LifeEvents */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile('@web/js/scales.js',['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div  class="life-events-form">

    <?php $form = ActiveForm::begin(['id' => 'formLifeevents']); ?>
    
    <div id="withBoxShadow">
    <p><label>ಕಳೆದ ಒಂದು ವರ್ಷದಲ್ಲಿ ಈ ಕೆಳಗಿನವುಗಳಲ್ಲಿ ಯಾವುದಾದರೂ ನಿಮ್ಮ ಜೀವನದಲ್ಲಿ ನಡೆದಿದಿಯೇ ?</label></p>
    <div class="col-lg-2 pull-right">
    <label>Number of Yes</label>
    <?= $form->field($model, 'noofyes')->input('number',['readonly'=>true])->label(false);?>
    </div>
        <b>1.ಮನೋಸಾಮಾಜಿಕ </b>
    
    
        <table border="1" class="responsive-scales">
        
        <thead>
          <tr>
            <th></th>
            <th>ಇಲ್ಲ </th>
            <th>ಹೌದು</th>
            <th>Other Answers</th>  
          </tr> 
        </thead>
        <tbody>
        
        <tr>
        <td>1)ಸಂಗಾತಿಯ /ಗಂಡನ ಸಾವು </td>
        
        <td><?= $form->field($model, 'death_spouse')->radio(['label' => '0', 'value' => 0])?></td>
        <td><?= $form->field($model, 'death_spouse')->radio(['label' => '100', 'value' => 1])?></td>
        <td>
            <?= $form->field($model, 'death_spouse')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
        </tr>
    
        <tr>
        <td>2)ವಿಚ್ಚೇದನ  </td>
        
        <td><?= $form->field($model, 'divorce')->radio(['label' => '0', 'value' => 0])?></td>
        <td><?= $form->field($model, 'divorce')->radio(['label' => '073', 'value' => 1])?></td>
        <td>
            <?= $form->field($model, 'divorce')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
        </tr>

        <tr>
        <td>3)ವೈವಾಹಿಕ ಜೀವನದಿಂದ ಬೇರೆಯಾಗಿರುವುದು </td>
        
        <td><?= $form->field($model, 'marital_separation')->radio(['label' => '0', 'value' => 0])?></td>
        <td><?= $form->field($model, 'marital_separation')->radio(['label' => '065', 'value' =>1])?></td>
        <td>
            <?= $form->field($model, 'marital_separation')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
        </tr>
        
        <tr>
        <td>4)ಹತ್ತಿರದ ಕುಟುಂಬದ ಸದಸ್ಯನ ಸಾವು</td>
        
        <td><?= $form->field($model, 'death_family_member')->radio(['label' => '0', 'value' => 0])?></td>
        <td><?= $form->field($model, 'death_family_member')->radio(['label' => '063', 'value' => 1])?></td>
        
        <td>
            <?= $form->field($model, 'death_family_member')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
        </tr>
    
        <tr>
        <td>5) ವೈಯುಕ್ತಿಕವಾಗಿ ಗಾಯವಾಗಿರುವುದು /ಅಸ್ವಸ್ಥರಾಗಿರುವುದು </td>
        
        <td><?= $form->field($model, 'personal_injury')->radio(['label' => '0', 'value' => 0])?></td>
        <td><?= $form->field($model, 'personal_injury')->radio(['label' => '053', 'value' => 1])?></td>
        <td>
            <?= $form->field($model, 'personal_injury')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
        </tr>

        <tr>
        <td>6)ವಿವಾಹ</td>
        
        <td><?= $form->field($model, 'marriage')->radio(['label' => '0', 'value' => 0])?></td>
        <td><?= $form->field($model, 'marriage')->radio(['label' => '050', 'value' => 1])?></td>
        <td>
            <?= $form->field($model, 'marriage')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
        </tr>
        
        <tr>
        <td>7)ವೈವಾಹಿಕ  ಸಾಮರಸ್ಯ  (ವೈವಾಹಿಕ ಜೀವನದಲ್ಲಿ ಬೇರೆಯಾಗಿ ಮತ್ತೆ ರಾಜಿಯಾಗಿರುವುದು )</td>
        
        <td><?= $form->field($model, 'marital_reconcilation')->radio(['label' => '0', 'value' => 0])?></td>
        <td><?= $form->field($model, 'marital_reconcilation')->radio(['label' => '045', 'value' => 1])?></td>
        <td>
            <?= $form->field($model, 'marital_reconcilation')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
        </tr>
        
        
        <tr>
        <td>8) ಕುಟುಂಬ ಸದಸ್ಯರ  ಆರೋಗ್ಯದಲ್ಲಿ  ಬದಲಾವಣೆ  </td>
        
        <td><?= $form->field($model, 'health_family_member')->radio(['label' => '0', 'value' => 0])?></td>
        <td><?= $form->field($model, 'health_family_member')->radio(['label' => '044', 'value' => 1])?></td>
        <td>
            <?= $form->field($model, 'health_family_member')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
        </tr>

        <tr>
        <td>9)ಹಣಕಾಸಿನ ಸ್ಥಿತಿಯಲ್ಲಿ ಬದಲಾವಣೆ</td>
        
        <td><?= $form->field($model, 'change_financial_status')->radio(['label' => '0', 'value' => 0])?></td>
        <td><?= $form->field($model, 'change_financial_status')->radio(['label' => '038', 'value' => 1])?></td>
        <td>
            <?= $form->field($model, 'change_financial_status')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
        </tr>
        
        
        
        <tr>
        <td>10)ಸಂಗಾತಿ /ಪತಿಯೊಂದಿಗೆ ಜಗಳ ಮಾಡುವ ಪ್ರಮಾಣದಲ್ಲಿ ಅತಿ ದೊಡ್ಡ ಬದಲಾವಣೆ </td>
        
        <td><?= $form->field($model, 'change_argument_spouse')->radio(['label' => '0', 'value' => 0])?></td>
        <td><?= $form->field($model, 'change_argument_spouse')->radio(['label' => '035', 'value' => 1])?></td>
        <td>
            <?= $form->field($model, 'change_argument_spouse')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
        </tr>
        
        <tr>
        <td>11)ಹತ್ತಿರದ ಸ್ನೇಹಿತರ ಸಾವು</td>
        
        <td><?= $form->field($model, 'death_close_friend')->radio(['label' => '0', 'value' => 0])?></td>
        <td><?= $form->field($model, 'death_close_friend')->radio(['label' =>'037', 'value' => 1])?></td>
        <td>
            <?= $form->field($model, 'death_close_friend')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
        </tr>
        
        <tr>
        <td>12)ಅತಿಯಾದ /ದೊಡ್ಡದಾದ ಸಾಲ /ಅಡಮಾನವಿಟ್ಟಿರುವುದು</td>
        
        <td><?= $form->field($model, 'large_loan')->radio(['label' => '0', 'value' => 0])?></td>
        <td><?= $form->field($model, 'large_loan')->radio(['label' => '031', 'value' => 1])?></td>
        <td>
        <?= $form->field($model, 'large_loan')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
        </tr>

        <tr>
        <td>13) ಅತ್ತೆ -ಮಾವ  (ಗಂಡನ ಮನೆಯವರೊಂದಿಗೆ  ತೊಂದರೆ)</td>
        
        <td><?= $form->field($model, 'trouble_in_laws')->radio(['label' => '0', 'value' => 0])?></td>
        <td><?= $form->field($model, 'trouble_in_laws')->radio(['label' => '029', 'value' => 1])?></td>
        <td>
            <?= $form->field($model, 'trouble_in_laws')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
        </tr>

        <tr>
        <td>14)ಸಂಗಾತಿಯು ಕೆಲಸಕ್ಕೆ ಹೋಗಲು ಪ್ರಾರಂಭ ಮಾಡಿರುವುದು ಅಥವಾ ನಿಲ್ಲಿಸಿರಿವುದು</td>
        
        <td><?= $form->field($model, 'spouse_work')->radio(['label' => '0', 'value' => 0])?></td>
        <td><?= $form->field($model, 'spouse_work')->radio(['label' => '028', 'value' => 1])?></td>
        <td>
            <?= $form->field($model, 'spouse_work')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
        </tr>
        
        <tr>
        <td>15)ಜೀವನ ಶೈಲಿಯಲ್ಲಿ ಬದಲಾವಣೆ </td>
        
        <td><?= $form->field($model, 'change_living_conditions')->radio(['label' => '0', 'value' => 0])?></td>
        <td><?= $form->field($model, 'change_living_conditions')->radio(['label' => '025', 'value' => 1])?></td>
        <td>
            <?= $form->field($model, 'change_living_conditions')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
        </tr>

        <tr>
        <td>16)ಕೆಲಸ ಮಾಡುವ ಸಮಯದಲ್ಲಿ ಬದಲಾವಣೆ ಅಥವ  ಪರಿಸ್ಥಿತಿಯಲ್ಲಿ ಬದಲಾವಣೆ  </td>
        
        <td><?= $form->field($model, 'change_working_hours')->radio(['label' => '0', 'value' => 0])?></td>
        <td><?= $form->field($model, 'change_working_hours')->radio(['label' => '020', 'value' => 1])?></td>
        <td>
            <?= $form->field($model, 'change_working_hours')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
        </tr>

        <tr>
        <td>17)ವಾಸವಿರುವ ಮನೆಯಲ್ಲಿ ಬದಲಾವಣೆ </td>
        
        <td><?= $form->field($model, 'change_residence')->radio(['label' => '0', 'value' => 0])?></td>
        <td><?= $form->field($model, 'change_residence')->radio(['label' => '020', 'value' => 1])?></td>
        <td>
        <?= $form->field($model, 'change_residence')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
        </tr>
        </tbody>
        </table><br>
      
        
        
        <div id = "lescale">
        <table border="1" class="responsive-scales">
        <thead>
          <tr>
            <th>ಪ್ರಮುಖ ನಕಾರಾತ್ಮಕ ಜೀವನದ ಘಟನೆಗಳು </th>
            <th>ಇಲ್ಲ </th>
            <th>ಹೌದು </th>
            <th>Others Answers</th>
          </tr> 
        </thead>
        <tbody>
        
        <tr>
        <td>1)ಕುಟುಂಬದ ಸದಸ್ಯರಲ್ಲಿ ಅತಿ ದೊಡ್ಡ ಕಾಯಿಲೆ /ಆರೋಗ್ಯದ ಸಮಸ್ಯೆ </td>
        <?php for($i = 0;$i < 2;$i++):?>
        <td>
        <?= $form->field($model, 'major_health_family_member')->radio(['label' => $i, 'value' => $i])?>
                </td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'major_health_family_member')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
        </tr>
        
        <tr>
        <td>2)ಹಣಕಾಸಿನ ಪರಿಸ್ಥಿತಿ ಕ್ಷೀಣಿಸಿರುವುದು / ಹದಗೆಟ್ಟಿರುವುದು   </td>
        <?php for($i = 0;$i < 2;$i++):?>
        <td>
        <?= $form->field($model, 'deterioration_finance')->radio(['label' => $i, 'value' => $i])?>
                </td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'deterioration_finance')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
        </tr>
        
        <tr>
        <td>3)ಪೋಷಕರೊಂದಿಗೆ ಜಗಳ </td>
        <?php for($i = 0;$i < 2;$i++):?>
        <td>
        <?= $form->field($model, 'conflict_parents')->radio(['label' => $i, 'value' => $i])?>
                </td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'conflict_parents')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
        </tr>
        
        <tr>
        <td>4)ಗಂಡನೊಂದಿಗೆ ಹೆಚ್ಚಾದ ವಾದ /ಜಗಳ </td>
        <?php for($i = 0;$i < 2;$i++):?>
        <td>
        <?= $form->field($model, 'argument_spouse')->radio(['label' => $i, 'value' => $i])?>
                </td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'argument_spouse')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
        </tr>

        <tr>
        <td>5)ಜೀವನ ಶೈಲಿಯು ಕ್ಷೀಣಿಸಿರುವುದು / ಹದಗೆಟ್ಟಿರುವುದು  </td>
        <?php for($i = 0;$i < 2;$i++):?>
        <td>
        <?= $form->field($model, 'deterioration_living_conditions')->radio(['label' => $i, 'value' => $i])?>
                </td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'deterioration_living_conditions')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
        </tr>
        </tbody>
        </table>
		<br>
		<div class="col-lg-12">
		
		<div class="col-lg-6">
		<h4><b>2.ಹೆರಿಗೆಯ ಬಗ್ಗೆ  </b></h4>
		</div>
		
		</div>
		
		<div id="notapp">
		<table>
		<tbody>
		<tr>
		
		<td>
		<h4><b> Is This Applicable ?</b></h4><?php echo $form->field($model, 'not_app')->dropDownList(['' =>'','YES' => 'Applicable', 'NO' => 'Not Applicable'],['class' => 'form-control scaleApplicable'])->label(false); ?>
		</td>
		
		</tr>
		</tbody>	
		</table>
		</div>
		
		
		
		
		<div id ="delivery" style = "display: <?php echo($model->not_app == 'YES')?"display":"none"; ?> ">
        
        <table border="1" class="responsive-scales">
        <thead>
          <tr>
            <th>sl.no</th>
            <th>Statements</th>
            <th>ಇಲ್ಲ </th>
            <th>ಹೌದು </th>
            <th>Others Answers</th>
          </tr> 
        </thead>
        <tbody>
        <tr>
        <td>1</td>
        <td>ಗರ್ಭಪಾತ </td>
        <?php for($i = 0;$i < 2;$i++):?>
        <td>
        <?= $form->field($model, 'abortion')->radio(['label' => $i, 'value' => $i])?>
        </td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'abortion')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
        </tr>
        
        <tr>
        <td>2</td>
        <td>ಗರ್ಭಸ್ರಾವ /ಹುಟ್ಟುವ ಮೊದಲೇ ಮಗುವು ಮರಣಹೊಂದಿರುವುದು </td>
        <?php for($i = 0;$i < 2;$i++):?>
        <td>
        <?= $form->field($model, 'miscarriage')->radio(['label' => $i, 'value' => $i])?>
        </td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'miscarriage')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
        </tr>
        
        <tr>
        <td>3</td>
        <td>ಮಧುಮೇಹ /ಥೈರಾಯಿಡ್ /ಹೆಚ್ಚಿನ ಒತ್ತಡ </td>
        <?php for($i = 0;$i < 2;$i++):?>
        <td>
        <?= $form->field($model, 'diabetes')->radio(['label' => $i, 'value' => $i])?>
        </td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'diabetes')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
        </tr>
        
        <tr>
        <td>4</td>
        <td>ಗರ್ಭಿಣಿಯಾಗಿದ್ದಾಗ ಕೈ ,ಕಾಲುಗಳಲ್ಲಿ ಊತ (ಪ್ರಿ -  ಎಕ್ಲಾ ಮಿಷಿಯಾ)</td>
        <?php for($i = 0;$i < 2;$i++):?>
        <td>
        <?= $form->field($model, 'pre_eclamptic')->radio(['label' => $i, 'value' => $i])?>
        </td>
        <?php endfor;?>
        <td>
    <?= $form->field($model, 'pre_eclamptic')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
        </tr>
        
        <tr>
        <td>5</td>
        <td>ಗರ್ಭಿಣಿಯಾಗಿದ್ದಾಗ ಫಿಟ್ಸ್ ಬರುವುದು ಅಥವಾ ಪ್ರಜ್ಞೆ ತಪ್ಪುವುದು (ಎಕ್ಲಾ ಮಿಷಿಯಾ )</td>
        <?php for($i = 0;$i < 2;$i++):?>
        <td>
        <?= $form->field($model, 'eclampsia')->radio(['label' => $i, 'value' => $i])?>
        </td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'eclampsia')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
        
        </tr>
        
        <tr>
        <td>6</td>
        <td>ಆಕಸ್ಮಿಕವಾದ ಬರೆದುಕೊಟ್ಟಂತಹ ಔಷದಗಳನ್ನು ತೆಗೆದುಕೊಳ್ಳುವುದು </td>
        <?php for($i = 0;$i < 2;$i++):?>
        <td>
        <?= $form->field($model, 'medication_exposure')->radio(['label' => $i, 'value' => $i])?>
        </td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'medication_exposure')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
        </tr>
        
        <tr>
        <td>7</td>
        <td>ಶಿಶುವು ಮರಣ ಹೊಂದಿರುವುದು</td>
        <?php for($i = 0;$i < 2;$i++):?>
        <td>
        <?= $form->field($model, 'neonatal_death')->radio(['label' => $i, 'value' => $i])?>
        </td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'neonatal_death')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
        </tr>
        
        <!--<tr>
        <td>8</td>
        <td>Others Answers</td>
        <td colspan="2">< ?= $form->field($model, 'other_answeres_about_delivery')->textarea()->label(false); ?></td>
        </tr>-->
        
        </tbody>
        </table>
        
        
        
        </div>
		
		<div id="scaleflag" class="hidden">
        <?= $form->field($model, 'score')->input('number',['readonly'=>true])->label(false);?>
        </div>
        
    <?php ActiveForm::end(); ?>

</div>
<?php $this->registerJs('
    $("#"+formObj.formName).one("change",function()
    {
        autoSaveObj = formObj;
    });
',\yii\web\View::POS_READY)?>

<?php $this->registerJs('
	
	
	$("#lifeevents-not_app").change(function(){
		
		if($(this).val() == "YES")
		{
			setNotApplicableShow("#delivery","#lifeevents-not_app");
		}
		else
		{
			setNotApplicableHide("#delivery","#lifeevents-not_app");
		}
	});
	
',\yii\web\View::POS_READY)?>

<script type="text/javascript">
    var formObj = {url:"life-events/ajax",formName : "formLifeevents"};
        var calc = function () {
        var totalValue = 0;
        var noe = 0;
        var root= 'lifeevents';
        var id = '#'+root+'-';
        var clas = '.field-'+root+'-';
        var arr = ['death_spouse', 'divorce', 'marital_separation', 'death_family_member', 'personal_injury', 'marriage',
        'marital_reconcilation', 'health_family_member', 'change_financial_status', 'death_close_friend', 'change_argument_spouse', 
        'large_loan', 'trouble_in_laws', 'spouse_work', 'change_living_conditions', 'change_working_hours', 'change_residence', 
        'major_health_family_member', 'deterioration_finance', 'conflict_parents', 'argument_spouse', 'deterioration_living_conditions',
        'abortion','miscarriage','diabetes','pre_eclamptic','eclampsia','medication_exposure','neonatal_death'];
        
        for (var i in arr) {
            var val = $(clas + arr[i] + ' input[type=radio]:checked').val();
            if (parseInt(val) >0) {
                noe++;
                totalValue += parseInt(val);
            }
        }
        $(id+'score').val(totalValue);
        $(id+'noofyes').val(noe);
        autoSaveObj = formObj;
    }

</script>
<style>
#notapp table{
	height:95px;
}
</style>
