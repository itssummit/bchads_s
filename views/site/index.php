<?php
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\helpers\Html;

?>
<div class="site-index">
	
	<div>
		<?php 
				echo $this->render('/mother/index', [
						'dataProvider' => $dataProvider
    				]); 
		?>
	</div>
	
</div>