<button type = "button" class = "btn btn-success" ng-click = "startSynchronization()" ng-disabled = "sync.progress">Synchronize</button>
<div class="progress" ng-show = "sync.progress">
	<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
    	Synchronizing.Please Wait...
	</div>
</div>
<div ng-show = "sync.message">
	<h3>{{sync.message}}</h3>
</div>