<?php
use kartik\daterange\DateRangePicker;
use kartik\date\DatePicker;
use yii\web\View;
$this->registerJsFile('@web/jsControllers/reportController.js',['depends' => [\app\assets\AppAsset::className()]]);
$this->registerCss("
.section {
    height: 100%;
    overflow: auto;
}
.daterangepicker .calendar {
    max-width: 700px;
}
tr{
	height:50px;
}");
?>
<div ng-controller="ReportController">
	<div class="row">
		<!--<div class="col-lg-12 col-md-12">
			<div class="col-lg-4 col-md-4">
			<b>Mother Code:	<input ng-model="mc"></b>
			</div>
			<div class="col-lg-4 col-md-4">
			<b>Assessment Type:	<select ng-model="selectedType" ng-options="x.model for x in assesstypes"></select></b>
			</div>
			<div class = "col-md-1 col-lg-1">
	    	<button type = "button" class = "btn btn-success btn-sm" ng-click = "getIndReportGenerator()">Export</button>
			</div>
		</div>-->
	</div><br><br>
	
	
	<div class = "row">
	<div class = "col-lg-12 col-md-12">
		<!--<div class = "col-md-6 col-lg-6">
		< ?php 	
	      				echo '<div class="drp-container">';
	      				echo DateRangePicker::widget([
    							'name'=>'date_range_1',
	      						'value'=>date('d-M-Y').' to '.date('d-M-Y'),
								'convertFormat'=>true,
	      						'useWithAddon'=>true,
    							'presetDropdown'=>true,
    							'hideInput'=>true,
								'pluginOptions'=>
									[
										'locale'=>[
											'format'=>'d-M-Y',
											'separator'=>' to ',
										],
										'opens'=>'right'
									],
	      						'options' => 
	      							[
	      								'id' => 'daterangeforline'
	      							]
						]);		
	      				echo '</div>';
		?>
	    </div>-->
			<div class="col-lg-4 col-md-4 export">
			<b>Assessment Type:	<select ng-model="selectedType" ng-options="x for x in assesstypes"></select></b>
			</div>
	    <div class = "col-md-1 col-lg-1">
	    	<button type = "button" class = "btn btn-success" ng-click = "getReportGenerator()">Export</button>
	    </div>
	</div>	
	</div><br>
	
	
		
	
</div>
<style>
.export select{
	width:200px;
	height:35px;
}
</style>