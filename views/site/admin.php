<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="col-md-6 col-md-offset-4 col-lg-6 col-lg-offset-4">
	<div class="row">
		<div class="col-sm-6 col-md-3 col-lg-3 form-group">
			<?php echo Html::tag('div',Html::a('Users',Url::to(['/user-management/user/index']),['style' => 'text-decoration:none']),["class"=>"admintile tile2","align"=>"center",'onclick'=>"location.href='/bchads/web/index.php?r=user-management/user/index'"])?>
		  </div>
		 <div class="col-sm-6 col-md-3 col-lg-3 form-group">
		 	<?php echo Html::tag('div',Html::a('Roles',Url::to(['/user-management/role/index']),['style' => 'text-decoration:none']),["class"=>"admintile tile2","align"=>"center",'onclick'=>"location.href='/bchads/web/index.php?r=user-management/role/index'"])?>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12 col-md-6 col-lg-6 form-group">
			<?php echo Html::tag('div',Html::a('Change own password',Url::to(['/user-management/auth/change-own-password']),['style' => 'text-decoration:none']),["class"=>"admintile tile2","align"=>"center",'onclick'=>"location.href='/bchads/web/index.php?r=user-management/auth/change-own-password'"])?>
	   	</div>
	</div>
	<div class="row">
		<div class="col-sm-6 col-md-3 col-lg-3 form-group">
			<?php echo Html::tag('div',Html::a('Permission',Url::to(['/user-management/permission/index']),['style' => 'text-decoration:none']),["class"=>"admintile tile2","align"=>"center",'onclick'=>"location.href='/bchads/web/index.php?r=user-management/permission/index'"])?>
		</div>
		<div class="col-sm-6 col-md-3 col-lg-3 form-group">
			<?php echo Html::tag('div',Html::a('Permission Group',Url::to(['/user-management/auth-item-group/index']),['style' => 'text-decoration:none']),["class"=>"admintile tile2","align"=>"center",'onclick'=>"location.href='/bchads/web/index.php?r=user-management/auth-item-group/index'"])?>
		</div>
	</div>
    <div class="row">
        <div class="col-sm-6 col-md-3 col-lg-3 form-group">
			<?php echo Html::tag('div',Html::a('Back Up',Url::to(['/backup/index']),['style' => 'text-decoration:none']),["class"=>"admintile tile2","align"=>"center",'onclick'=>"location.href='/bchads/web/index.php?r=backup/index'"])?>
		</div> 
        <!--<div class="col-sm-6 col-md-3 col-lg-3 form-group">
			< ?php echo Html::tag('div',Html::a('Data Conversion',Url::to(['/dataconversion/index']),['style' => 'text-decoration:none']),["class"=>"admintile tile2","align"=>"center",'onclick'=>"location.href='/bchads/web/index.php?r=dataconversion/index'"])?>
		</div> -->
		<div class="col-sm-6 col-md-3 col-lg-3 form-group">
			<?php echo Html::tag('div',Html::a('Export',Url::to(['/site/export']),['style' => 'text-decoration:none']),["class"=>"admintile","align"=>"center",'onclick'=>"location.href='/bchads/web/index.php?r=site/export'"])?>
		</div>
	</div>
	<div class="row">	
		<div class="col-sm-12 col-md-6 col-lg-6 form-group">
			<?php echo Html::tag('div',Html::a('Synchronization',Url::to(['/site/sync']),['style' => 'text-decoration:none']),["class"=>"admintile","align"=>"center",'onclick'=>"location.href='/bchads/web/index.php?r=site/sync'"])?>
		</div>
    </div>
	
    
</div>