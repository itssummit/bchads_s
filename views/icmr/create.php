<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Icmr */

$this->title = 'Icmr';
$this->params['breadcrumbs'][] = ['label' => 'Icmrs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="icmr-create">
    <h1 align="center">ICMR Domestic Violence Assessment Questionnaire</h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
