﻿<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Icmr;
$listData =["yes"=> "yes","no"=>"no"];
/* @var $this yii\web\View */
/* @var $model app\models\Icmr */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile('@web/js/scalesome.js',['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<h5><b>ಕಳೆದ ಭೇಟಿಯಿಂದ  ನಿಮ್ಮ ವೈವಾಹಿಕ ಜೀವನದಲ್ಲಿ ಈ ಕೆಳಗಿನ ಪರಿಸ್ಥಿತಿಗಳಿಗೆ ಒಳಗಾಗಿದ್ದೀರಾ  ? (ಹೌದಾದಲ್ಲಿ , ಪ್ರತಿಕ್ರಿಯೆಗಳನ್ನು  ವಿವರವಾಗಿ ಪರೀಕ್ಷಿಸಿ )  </b>  </h5>
<div  class="icmr-form">

    <?php $form = ActiveForm::begin(['id' => 'formIcmr']); ?>
	
    <div id="withBoxShadow" >
	
        	
			<h1>ಮಾನಸಿಕ ಹಿಂಸಾಚಾರ </h1>
			<div class="row">
				<div class="col-lg-6">
				<b>1)ನಿಮ್ಮನ್ನು ನಿಂದನೆ ಪದಗಳಿಂದ ಅವಮಾನಪಡಿಸಿದ್ದರೆ?</b>
				<?= $form->field($model, 'bad_words')->radioList(array(1=>'ಹೌದು',0=>'ಇಲ್ಲ'),['itemOptions'=>['class' => "bad_words"]])->label(false); ?>
				</div>
			</div>
			<div id="wards1" style = "border: 1px solid black;display:<?php echo ($model->bad_words == 1)?"display":"none";?>">
				<div class="row">
					<div class="col-lg-6 indent">
						<label>a)ವಿವಾಹವಾದ ಎಷ್ಟು ತಿಂಗಳುಗಳಲ್ಲಿ ಇದು ಪ್ರಾರಂಭವಾಯಿತು</label>
						<?= $form->field($model, 'bad_words1')->textInput()->label(false) ?>
					</div>	
					<div class="col-lg-6">
						<label>b)ಎಷ್ಟು ದಿನಗಳವರೆಗೆ ಇದು ಮುಂದುವರಿಯಿತು </label>
						<?= $form->field($model, 'bad_words2')->textInput()->label(false) ?>
					</div>
				</div><hr>
					
				<div class="row">
					<div class="col-lg-6">
						<label>c)ಇದನ್ನು ಮಾಡಿದ್ದು ಯಾರು <br></label>
						<?= $form->field($model, 'bad_words3')->textInput()->label(false) ?>
						<?= $form->field($model, 'bad_words9')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>d)ಇದು ಈಗಲೂ ನಡೆಯುತ್ತಿದೆಯೇ ?</label>
						<?= $form->field($model, 'bad_words4')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>e)ಅದರ ಪುನರಾವರ್ತನೆ ಏನು ?</label>
					<?= $form->field($model, 'bad_words5')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>f)ಈ ರೀತಿಯ ವರ್ತನೆ  ಕಾರಣವೇನು ?</label>
						<?= $form->field($model, 'bad_words6')->textInput()->label(false) ?>
						<?= $form->field($model, 'bad_words10')->textInput()->label(false) ?>
						<?= $form->field($model, 'bad_words11')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
					<label>g)ಈ ರೀತಿಯ ವರ್ತನೆ  ನೀವು ಸಮರ್ಥಿಸುತ್ತಿದ್ದೀರಾ  ?</label>
					<?= $form->field($model, 'bad_words7')->radioList(['Yes'=>'Yes','No'=>'No'],['itemOptions'=>['class' => "bad_words7"]])->label(false); ?>
					</div>
					<div class="col-lg-6">
					<label>h)ಆಕೆ ಗರ್ಭಿಣಿಯಾಗಿದ್ದ ಸಂದರ್ಭದಲ್ಲಿ ಈ ವಿಷಯಗಳಿಗೆ ಒಳಗಾಗಿದ್ದನ್ನು ನೆನೆಪಿಸಬಹುದೇ? </label>
					<?= $form->field($model, 'bad_words8')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>
			</div>
			
			<div class="row">
				<div class="col-lg-6">
					<b>2)ನಿಮ್ಮನ್ನು ಹೆದರಿಸಿದ್ದರೇ? (ಕಲ್ಲು ,ಬೆಲ್ಟ್ ಕತ್ತಿ  ಇತ್ಯಾದಿಗಳಿಂದ) </b>
					<?= $form->field($model, 'threatening')->radioList(array(1=>'ಹೌದು',0=>'ಇಲ್ಲ'),['itemOptions'=>['class' => "threatening"]])->label(false); ?>	
				</div>
			</div>
			<div id="wards2" style = "border: 1px solid black;display:<?php echo ($model->threatening == 1)?"display":"none";?>">
				<div class="row">
					<div class="col-lg-6">
						<label>a)ವಿವಾಹವಾದ ಎಷ್ಟು ತಿಂಗಳುಗಳಲ್ಲಿ ಇದು ಪ್ರಾರಂಭವಾಯಿತು</label>
						<?= $form->field($model, 'threatening1')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>b)ಎಷ್ಟು ದಿನಗಳವರೆಗೆ ಇದು ಮುಂದುವರಿಯಿತು </label>
						<?= $form->field($model, 'threatening2')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>c)ಇದನ್ನು ಮಾಡಿದ್ದು ಯಾರು </label>
						<?= $form->field($model, 'threatening3')->textInput()->label(false) ?>
						<?= $form->field($model, 'threatening9')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>d)ಇದು ಈಗಲೂ ನಡೆಯುತ್ತಿದೆಯೇ ?</label>
						<?= $form->field($model, 'threatening4')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>	
				<div class="row">
					<div class="col-lg-6">
					<label>e)ಅದರ ಪುನರಾವರ್ತನೆ ಏನು ?</label>
					<?= $form->field($model, 'threatening5')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
					<label>f)ಈ ರೀತಿಯ ವರ್ತನೆ  ಕಾರಣವೇನು ?</label>
					<?= $form->field($model, 'threatening6')->textInput()->label(false) ?>
					<?= $form->field($model, 'threatening10')->textInput()->label(false) ?>
					<?= $form->field($model, 'threatening11')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
					<label>g)ಈ ರೀತಿಯ ವರ್ತನೆ  ನೀವು ಸಮರ್ಥಿಸುತ್ತಿದ್ದೀರಾ  ?</label>
					<?= $form->field($model, 'threatening7')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
					<div class="col-lg-6">
					<label>h)ಆಕೆ ಗರ್ಭಿಣಿಯಾಗಿದ್ದ ಸಂದರ್ಭದಲ್ಲಿ ಈ ವಿಷಯಗಳಿಗೆ ಒಳಗಾಗಿದ್ದನ್ನು ನೆನೆಪಿಸಬಹುದೇ? </label>
					<?= $form->field($model, 'threatening8')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>
			</div>
				
			
			<div class="row">
				<div class="col-lg-6">
					<b>3)ತವರುಮನೆಗೆ ಕಳುಹಿಸುತ್ತೇನೆಂದು ಹೆದರಿಸಿದ್ದರೇ?</b><br>
					<?= $form->field($model, 'threatening_sending_home')->radioList(array(1=>'ಹೌದು',0=>'ಇಲ್ಲ'),['itemOptions'=>['class' => "threatening_sending_home"]])->label(false); ?>	
				</div>
			</div>	
			<div id="wards3" style = "border: 1px solid black;display:<?php echo ($model->threatening_sending_home == 1)?"display":"none";?>">
				<div class="row">
					<div class="col-lg-6">
						<label>a) ವಿವಾಹವಾದ ಎಷ್ಟು ತಿಂಗಳುಗಳಲ್ಲಿ ಇದು ಪ್ರಾರಂಭವಾಯಿತು</label>
						<?= $form->field($model, 'threatening_sending_home1')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>b) ಎಷ್ಟು ದಿನಗಳವರೆಗೆ ಇದು ಮುಂದುವರಿಯಿತು </label>
						<?= $form->field($model, 'threatening_sending_home2')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
					<label>c) ಇದನ್ನು ಮಾಡಿದ್ದು ಯಾರು </label>
					<?= $form->field($model, 'threatening_sending_home3')->textInput()->label(false) ?>
					<?= $form->field($model, 'threatening_sending_home9')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
					<label>d) ಇದು ಈಗಲೂ ನಡೆಯುತ್ತಿದೆಯೇ ?</label>
					<?= $form->field($model, 'threatening_sending_home4')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>	
				<div class="row">
					<div class="col-lg-6">
					<label>e) ಅದರ ಪುನರಾವರ್ತನೆ ಏನು ?</label>
					<?= $form->field($model, 'threatening_sending_home5')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
					<label>f) ಈ ರೀತಿಯ ವರ್ತನೆ  ಕಾರಣವೇನು ?</label>
					<?= $form->field($model, 'threatening_sending_home6')->textInput()->label(false) ?>
					<?= $form->field($model, 'threatening_sending_home10')->textInput()->label(false) ?>
					<?= $form->field($model, 'threatening_sending_home11')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
					<label>g) ಈ ರೀತಿಯ ವರ್ತನೆ  ನೀವು ಸಮರ್ಥಿಸುತ್ತಿದ್ದೀರಾ  ?</label>
					<?= $form->field($model, 'threatening_sending_home7')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
					<div class="col-lg-6">
					<label>h) ಆಕೆ ಗರ್ಭಿಣಿಯಾಗಿದ್ದ ಸಂದರ್ಭದಲ್ಲಿ ಈ ವಿಷಯಗಳಿಗೆ ಒಳಗಾಗಿದ್ದನ್ನು ನೆನೆಪಿಸಬಹುದೇ? </label>
					<?= $form->field($model, 'threatening_sending_home8')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>
			</div>

			
			<div class="row">
				<div class="col-lg-6">
				<b>4)ತವರುಮನೆಗೆ ಕಳುಹಿಸಿದ್ದರೇ?</b>
				<?= $form->field($model, 'send_home')->radioList(array(1=>'ಹೌದು',0=>'ಇಲ್ಲ'),['itemOptions'=>['class' => "send_home"]])->label(false); ?>
				</div>
			</div>	
			
			<div id="wards4" style = "border: 1px solid black;display:<?php echo ($model->send_home == 1)?"display":"none";?>">
				<div class="row">
					<div class="col-lg-6">
					<label>a) ವಿವಾಹವಾದ ಎಷ್ಟು ತಿಂಗಳುಗಳಲ್ಲಿ ಇದು ಪ್ರಾರಂಭವಾಯಿತು</label>
					<?= $form->field($model, 'send_home1')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
					<label>b) ಎಷ್ಟು ದಿನಗಳವರೆಗೆ ಇದು ಮುಂದುವರಿಯಿತು </label>
					<?= $form->field($model, 'send_home2')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
					<label>c) ಇದನ್ನು ಮಾಡಿದ್ದು ಯಾರು </label>
					<?= $form->field($model, 'send_home3')->textInput()->label(false) ?>
					<?= $form->field($model, 'send_home9')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
					<label>d) ಇದು ಈಗಲೂ ನಡೆಯುತ್ತಿದೆಯೇ ?</label>
					<?= $form->field($model, 'send_home4')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>	
				<div class="row">
					<div class="col-lg-6">
					<label>e) ಅದರ ಪುನರಾವರ್ತನೆ ಏನು ?</label>
					<?= $form->field($model, 'send_home5')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
					<label>f) ಈ ರೀತಿಯ ವರ್ತನೆ  ಕಾರಣವೇನು ?</label>
					<?= $form->field($model, 'send_home6')->textInput()->label(false) ?>
					<?= $form->field($model, 'send_home10')->textInput()->label(false) ?>
					<?= $form->field($model, 'send_home11')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
					<label>g) ಈ ರೀತಿಯ ವರ್ತನೆ  ನೀವು ಸಮರ್ಥಿಸುತ್ತಿದ್ದೀರಾ  ?</label>
					<?= $form->field($model, 'send_home7')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
					<div class="col-lg-6">
					<label>h) ಆಕೆ ಗರ್ಭಿಣಿಯಾಗಿದ್ದ ಸಂದರ್ಭದಲ್ಲಿ ಈ ವಿಷಯಗಳಿಗೆ ಒಳಗಾಗಿದ್ದನ್ನು ನೆನೆಪಿಸಬಹುದೇ? </label>
					<?= $form->field($model, 'send_home8')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>
				
			</div>

			<div class="row">
				<div class="col-lg-6">
				<b>5)ಆರ್ಥಿಕ ತೊಂದರೆ</b>
				<?= $form->field($model, 'finance_prob')->radioList(array(1=>'ಹೌದು',0=>'ಇಲ್ಲ'),['itemOptions'=>['class' => "finance_prob"]])->label(false); ?>	
				</div>
			</div>	
			
			<div id="wards5" style = "border: 1px solid black;display:<?php echo ($model->finance_prob == 1)?"display":"none";?>">
				<div class="row">
					<div class="col-lg-6">
						<label>a) ವಿವಾಹವಾದ ಎಷ್ಟು ತಿಂಗಳುಗಳಲ್ಲಿ ಇದು ಪ್ರಾರಂಭವಾಯಿತು</label>
						<?= $form->field($model, 'finance_prob1')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>b) ಎಷ್ಟು ದಿನಗಳವರೆಗೆ ಇದು ಮುಂದುವರಿಯಿತು </label>
						<?= $form->field($model, 'finance_prob2')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>c) ಇದನ್ನು ಮಾಡಿದ್ದು ಯಾರು </label>
						<?= $form->field($model, 'finance_prob3')->textInput()->label(false) ?>
						<?= $form->field($model, 'finance_prob9')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>d) ಇದು ಈಗಲೂ ನಡೆಯುತ್ತಿದೆಯೇ ?</label>
						<?= $form->field($model, 'finance_prob4')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>	
				<div class="row">
					<div class="col-lg-6">
						<label>e) ಅದರ ಪುನರಾವರ್ತನೆ ಏನು ?</label>
						<?= $form->field($model, 'finance_prob5')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>f) ಈ ರೀತಿಯ ವರ್ತನೆ  ಕಾರಣವೇನು ?</label>
						<?= $form->field($model, 'finance_prob6')->textInput()->label(false) ?>
						<?= $form->field($model, 'finance_prob10')->textInput()->label(false) ?>
						<?= $form->field($model, 'finance_prob11')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>g) ಈ ರೀತಿಯ ವರ್ತನೆ  ನೀವು ಸಮರ್ಥಿಸುತ್ತಿದ್ದೀರಾ  ?</label>
						<?= $form->field($model, 'finance_prob7')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
					<div class="col-lg-6">
						<label>h) ಆಕೆ ಗರ್ಭಿಣಿಯಾಗಿದ್ದ ಸಂದರ್ಭದಲ್ಲಿ ಈ ವಿಷಯಗಳಿಗೆ ಒಳಗಾಗಿದ್ದನ್ನು ನೆನೆಪಿಸಬಹುದೇ? </label>
						<?= $form->field($model, 'finance_prob8')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>
				
			</div>
			
			<div class="row">
				<div class="col-lg-6">
				<b>6)ಹೆದರಿಸುವುದು/ಕ್ರೂರ   ನೋಟ </b>
				<?= $form->field($model, 'fear_look')->radioList(array(1=>'ಹೌದು',0=>'ಇಲ್ಲ'),['itemOptions'=>['class' => "fear_look"]])->label(false); ?>
				</div>
			</div>	
			
			<div id="wards6" style = "border: 1px solid black;display:<?php echo ($model->fear_look == 1)?"display":"none";?>">
				<div class="row">
					<div class="col-lg-6">
						<label>a) ವಿವಾಹವಾದ ಎಷ್ಟು ತಿಂಗಳುಗಳಲ್ಲಿ ಇದು ಪ್ರಾರಂಭವಾಯಿತು</label>
						<?= $form->field($model, 'fear_look1')->textInput()->label(false) ?>
					</div>	
					<div class="col-lg-6">
						<label>b) ಎಷ್ಟು ದಿನಗಳವರೆಗೆ ಇದು ಮುಂದುವರಿಯಿತು </label>
						<?= $form->field($model, 'fear_look2')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>c) ಇದನ್ನು ಮಾಡಿದ್ದು ಯಾರು </label>
						<?= $form->field($model, 'fear_look3')->textInput()->label(false) ?>
						<?= $form->field($model, 'fear_look9')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>d) ಇದು ಈಗಲೂ ನಡೆಯುತ್ತಿದೆಯೇ ?</label>
						<?= $form->field($model, 'fear_look4')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>	
				<div class="row">
					<div class="col-lg-6">
						<label>e) ಅದರ ಪುನರಾವರ್ತನೆ ಏನು ?</label>
						<?= $form->field($model, 'fear_look5')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>f) ಈ ರೀತಿಯ ವರ್ತನೆ  ಕಾರಣವೇನು ?</label>
						<?= $form->field($model, 'fear_look6')->textInput()->label(false) ?>
						<?= $form->field($model, 'fear_look10')->textInput()->label(false) ?>
						<?= $form->field($model, 'fear_look11')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>g) ಈ ರೀತಿಯ ವರ್ತನೆ  ನೀವು ಸಮರ್ಥಿಸುತ್ತಿದ್ದೀರಾ  ?</label>
						<?= $form->field($model, 'fear_look7')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
					<div class="col-lg-6">
						<label>h) ಆಕೆ ಗರ್ಭಿಣಿಯಾಗಿದ್ದ ಸಂದರ್ಭದಲ್ಲಿ ಈ ವಿಷಯಗಳಿಗೆ ಒಳಗಾಗಿದ್ದನ್ನು ನೆನೆಪಿಸಬಹುದೇ? </label>
						<?= $form->field($model, 'fear_look8')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>
				
			</div>
			
			<div class="row">
				<div class="col-lg-6">
				<b>7)ನಿಮಗೆ ಅವಿಧೇಯರಾಗಿರುವುದನ್ನು ಸಮರ್ಥಿಸಿದ್ದರೆ</b>
				<?= $form->field($model, 'avidheya')->radioList(array(1=>'ಹೌದು',0=>'ಇಲ್ಲ'),['itemOptions'=>['class' => "avidheya"]])->label(false); ?>	
				</div>
			</div>	
			
			<div id="wards7" style = "border: 1px solid black;display:<?php echo ($model->avidheya == 1)?"display":"none";?>">
				<div class="row">
					<div class="col-lg-6">
						<label>a) ವಿವಾಹವಾದ ಎಷ್ಟು ತಿಂಗಳುಗಳಲ್ಲಿ ಇದು ಪ್ರಾರಂಭವಾಯಿತು</label>
						<?= $form->field($model, 'avidheya1')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>b) ಎಷ್ಟು ದಿನಗಳವರೆಗೆ ಇದು ಮುಂದುವರಿಯಿತು </label>
						<?= $form->field($model, 'avidheya2')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>c) ಇದನ್ನು ಮಾಡಿದ್ದು ಯಾರು </label>
						<?= $form->field($model, 'avidheya3')->textInput()->label(false) ?>
						<?= $form->field($model, 'avidheya9')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>d) ಇದು ಈಗಲೂ ನಡೆಯುತ್ತಿದೆಯೇ ?</label>
						<?= $form->field($model, 'avidheya4')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>	
				<div class="row">
					<div class="col-lg-6">
						<label>e) ಅದರ ಪುನರಾವರ್ತನೆ ಏನು ?</label>
						<?= $form->field($model, 'avidheya5')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>f) ಈ ರೀತಿಯ ವರ್ತನೆ  ಕಾರಣವೇನು ?</label>
						<?= $form->field($model, 'avidheya6')->textInput()->label(false) ?>
						<?= $form->field($model, 'avidheya10')->textInput()->label(false) ?>
						<?= $form->field($model, 'avidheya11')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>g) ಈ ರೀತಿಯ ವರ್ತನೆ  ನೀವು ಸಮರ್ಥಿಸುತ್ತಿದ್ದೀರಾ  ?</label>
						<?= $form->field($model, 'avidheya7')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
					<div class="col-lg-6">
						<label>h) ಆಕೆ ಗರ್ಭಿಣಿಯಾಗಿದ್ದ ಸಂದರ್ಭದಲ್ಲಿ ಈ ವಿಷಯಗಳಿಗೆ ಒಳಗಾಗಿದ್ದನ್ನು ನೆನೆಪಿಸಬಹುದೇ? </label>
						<?= $form->field($model, 'avidheya8')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>
				
			</div>
			
			<div class="row">
				<div class="col-lg-6">
				<b>8)ಉದಾಸೀನತೆ ತೋರಿದರೆ</b>
				<?= $form->field($model, 'udasinathe')->radioList(array(1=>'ಹೌದು',0=>'ಇಲ್ಲ'),['itemOptions'=>['class' => "udasinathe"]])->label(false); ?>
				</div>
			</div>	
			
			<div id="wards8" style = "border: 1px solid black;display:<?php echo ($model->udasinathe == 1)?"display":"none";?>">
				<div class="row">
					<div class="col-lg-6">
						<label>a) ವಿವಾಹವಾದ ಎಷ್ಟು ತಿಂಗಳುಗಳಲ್ಲಿ ಇದು ಪ್ರಾರಂಭವಾಯಿತು</label>
						<?= $form->field($model, 'udasinathe1')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>b) ಎಷ್ಟು ದಿನಗಳವರೆಗೆ ಇದು ಮುಂದುವರಿಯಿತು </label>
						<?= $form->field($model, 'udasinathe2')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>c) ಇದನ್ನು ಮಾಡಿದ್ದು ಯಾರು </label>
						<?= $form->field($model, 'udasinathe3')->textInput()->label(false) ?>
						<?= $form->field($model, 'udasinathe9')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>d) ಇದು ಈಗಲೂ ನಡೆಯುತ್ತಿದೆಯೇ ?</label>
						<?= $form->field($model, 'udasinathe4')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>	
				<div class="row">
					<div class="col-lg-6">
						<label>e) ಅದರ ಪುನರಾವರ್ತನೆ ಏನು ?</label>
						<?= $form->field($model, 'udasinathe5')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>f) ಈ ರೀತಿಯ ವರ್ತನೆ  ಕಾರಣವೇನು ?</label>
						<?= $form->field($model, 'udasinathe6')->textInput()->label(false) ?>
						<?= $form->field($model, 'udasinathe10')->textInput()->label(false) ?>
						<?= $form->field($model, 'udasinathe11')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>g) ಈ ರೀತಿಯ ವರ್ತನೆ  ನೀವು ಸಮರ್ಥಿಸುತ್ತಿದ್ದೀರಾ  ?</label>
						<?= $form->field($model, 'udasinathe7')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
					<div class="col-lg-6">
						<label>h) ಆಕೆ ಗರ್ಭಿಣಿಯಾಗಿದ್ದ ಸಂದರ್ಭದಲ್ಲಿ ಈ ವಿಷಯಗಳಿಗೆ ಒಳಗಾಗಿದ್ದನ್ನು ನೆನೆಪಿಸಬಹುದೇ? </label>
						<?= $form->field($model, 'udasinathe8')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>
				
			</div>
	
			<div class="row">
				<div class="col-lg-6">
				<b>9)ಸಾಮಾಜಿಕ ಹಕ್ಕುಗಳ/ಸೌಕರ್ಯಗಳನ್ನು ಕಸಿದುಕೊಂಡಿದ್ದರು</b>
				<?= $form->field($model, 'social_rights')->radioList(array(1=>'ಹೌದು',0=>'ಇಲ್ಲ'),['itemOptions'=>['class' => "social_rights"]])->label(false); ?>	
				</div>
			</div>	
			
			<div id="wards9" style = "border: 1px solid black;display:<?php echo ($model->social_rights == 1)?"display":"none";?>">
				<div class="row">
					<div class="col-lg-6">
						<label>a) ವಿವಾಹವಾದ ಎಷ್ಟು ತಿಂಗಳುಗಳಲ್ಲಿ ಇದು ಪ್ರಾರಂಭವಾಯಿತು</label>
						<?= $form->field($model, 'social_rights1')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>b) ಎಷ್ಟು ದಿನಗಳವರೆಗೆ ಇದು ಮುಂದುವರಿಯಿತು </label>
						<?= $form->field($model, 'social_rights2')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>c) ಇದನ್ನು ಮಾಡಿದ್ದು ಯಾರು </label>
						<?= $form->field($model, 'social_rights3')->textInput()->label(false) ?>
						<?= $form->field($model, 'social_rights9')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>d) ಇದು ಈಗಲೂ ನಡೆಯುತ್ತಿದೆಯೇ ?</label>
						<?= $form->field($model, 'social_rights4')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>	
				<div class="row">
					<div class="col-lg-6">
						<label>e) ಅದರ ಪುನರಾವರ್ತನೆ ಏನು ?</label>
						<?= $form->field($model, 'social_rights5')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>f) ಈ ರೀತಿಯ ವರ್ತನೆ  ಕಾರಣವೇನು ?</label>
						<?= $form->field($model, 'social_rights6')->textInput()->label(false) ?>
						<?= $form->field($model, 'social_rights10')->textInput()->label(false) ?>
						<?= $form->field($model, 'social_rights11')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>g) ಈ ರೀತಿಯ ವರ್ತನೆ  ನೀವು ಸಮರ್ಥಿಸುತ್ತಿದ್ದೀರಾ  ?</label>
						<?= $form->field($model, 'social_rights7')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
					<div class="col-lg-6">
						<label>h) ಆಕೆ ಗರ್ಭಿಣಿಯಾಗಿದ್ದ ಸಂದರ್ಭದಲ್ಲಿ ಈ ವಿಷಯಗಳಿಗೆ ಒಳಗಾಗಿದ್ದನ್ನು ನೆನೆಪಿಸಬಹುದೇ? </label>
						<?= $form->field($model, 'social_rights8')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>
				
			</div>
			
			<div class="row">
				<div class="col-lg-6">
				<b>10)ನಿಮ್ಮನು ನಿರ್ಲಕ್ಷಿಸಿದ್ದಾರೆಯೇ   </b>
				<?= $form->field($model, 'nirlakshya')->radioList(array(1=>'ಹೌದು',0=>'ಇಲ್ಲ'),['itemOptions'=>['class' => "nirlakshya"]])->label(false); ?>	
				</div>
			</div>	
			
			<div id="wards10" style = "border: 1px solid black;display:<?php echo ($model->nirlakshya == 1)?"display":"none";?>">
				<div class="row">
					<div class="col-lg-6">
						<label>a) ವಿವಾಹವಾದ ಎಷ್ಟು ತಿಂಗಳುಗಳಲ್ಲಿ ಇದು ಪ್ರಾರಂಭವಾಯಿತು</label>
						<?= $form->field($model, 'nirlakshya1')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>b) ಎಷ್ಟು ದಿನಗಳವರೆಗೆ ಇದು ಮುಂದುವರಿಯಿತು </label>
						<?= $form->field($model, 'nirlakshya2')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>c) ಇದನ್ನು ಮಾಡಿದ್ದು ಯಾರು </label>
						<?= $form->field($model, 'nirlakshya3')->textInput()->label(false) ?>
						<?= $form->field($model, 'nirlakshya9')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>d) ಇದು ಈಗಲೂ ನಡೆಯುತ್ತಿದೆಯೇ ?</label>
						<?= $form->field($model, 'nirlakshya4')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>	
				<div class="row">
					<div class="col-lg-6">
						<label>e) ಅದರ ಪುನರಾವರ್ತನೆ ಏನು ?</label>
						<?= $form->field($model, 'nirlakshya5')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>f) ಈ ರೀತಿಯ ವರ್ತನೆ  ಕಾರಣವೇನು ?</label>
						<?= $form->field($model, 'nirlakshya6')->textInput()->label(false) ?>
						<?= $form->field($model, 'nirlakshya10')->textInput()->label(false) ?>
						<?= $form->field($model, 'nirlakshya11')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>g) ಈ ರೀತಿಯ ವರ್ತನೆ  ನೀವು ಸಮರ್ಥಿಸುತ್ತಿದ್ದೀರಾ  ?</label>
						<?= $form->field($model, 'nirlakshya7')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
					<div class="col-lg-6">
						<label>h) ಆಕೆ ಗರ್ಭಿಣಿಯಾಗಿದ್ದ ಸಂದರ್ಭದಲ್ಲಿ ಈ ವಿಷಯಗಳಿಗೆ ಒಳಗಾಗಿದ್ದನ್ನು ನೆನೆಪಿಸಬಹುದೇ? </label>
						<?= $form->field($model, 'nirlakshya8')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>
				
			</div>

			<div class="row">
				<div class="col-lg-6">
				<b>11)ಮೂಲ ವೈಯಕ್ತಿಕ ಅವಶ್ಯಕತೆಗಳನ್ನು ನಿರಾಕರಿಸಿದ್ದರೆ </b>
				<?= $form->field($model, 'personal_needs')->radioList(array(1=>'ಹೌದು',0=>'ಇಲ್ಲ'),['itemOptions'=>['class' => "personal_needs"]])->label(false); ?>	
				</div>
			</div>	
			
			<div id="wards11" style = "border: 1px solid black;display:<?php echo ($model->personal_needs == 1)?"display":"none";?>">
				<div class="row">
					<div class="col-lg-6">
						<label>a) ವಿವಾಹವಾದ ಎಷ್ಟು ತಿಂಗಳುಗಳಲ್ಲಿ ಇದು ಪ್ರಾರಂಭವಾಯಿತು</label>
						<?= $form->field($model, 'personal_needs1')->textInput()->label(false) ?>
					</div>	
					<div class="col-lg-6">
						<label>b) ಎಷ್ಟು ದಿನಗಳವರೆಗೆ ಇದು ಮುಂದುವರಿಯಿತು </label>
						<?= $form->field($model, 'personal_needs2')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>c) ಇದನ್ನು ಮಾಡಿದ್ದು ಯಾರು </label>
						<?= $form->field($model, 'personal_needs3')->textInput()->label(false) ?>
						<?= $form->field($model, 'personal_needs9')->textInput()->label(false) ?>
					</div>	
					<div class="col-lg-6">
						<label>d) ಇದು ಈಗಲೂ ನಡೆಯುತ್ತಿದೆಯೇ ?</label>
						<?= $form->field($model, 'personal_needs4')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>	
				<div class="row">
					<div class="col-lg-6">
						<label>e) ಅದರ ಪುನರಾವರ್ತನೆ ಏನು ?</label>
						<?= $form->field($model, 'personal_needs5')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>f) ಈ ರೀತಿಯ ವರ್ತನೆ  ಕಾರಣವೇನು ?</label>
						<?= $form->field($model, 'personal_needs6')->textInput()->label(false) ?>
						<?= $form->field($model, 'personal_needs10')->textInput()->label(false) ?>
						<?= $form->field($model, 'personal_needs11')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>g) ಈ ರೀತಿಯ ವರ್ತನೆ  ನೀವು ಸಮರ್ಥಿಸುತ್ತಿದ್ದೀರಾ  ?</label>
						<?= $form->field($model, 'personal_needs7')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
					<div class="col-lg-6">
						<label>h) ಆಕೆ ಗರ್ಭಿಣಿಯಾಗಿದ್ದ ಸಂದರ್ಭದಲ್ಲಿ ಈ ವಿಷಯಗಳಿಗೆ ಒಳಗಾಗಿದ್ದನ್ನು ನೆನೆಪಿಸಬಹುದೇ? </label>
						<?= $form->field($model, 'personal_needs8')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>
				
			</div>
			
			<div class="row">
				<div class="col-lg-6">
				<b>12)ತೀರ್ಮಾನ ಕೈಗೊಳ್ಳುವಲ್ಲಿ ಅವಕಾಶ ನೀಡುತ್ತಿರ ಲಿಲ್ಲ </b>
				<?= $form->field($model, 'decision_taking')->radioList(array(1=>'ಹೌದು',0=>'ಇಲ್ಲ'),['itemOptions'=>['class' => "decision_taking"]])->label(false); ?>	
				</div>
			</div>	
			
			<div id="wards12" style = "border: 1px solid black;display:<?php echo ($model->decision_taking == 1)?"display":"none";?>">
				<div class="row">
					<div class="col-lg-6">
						<label>a) ವಿವಾಹವಾದ ಎಷ್ಟು ತಿಂಗಳುಗಳಲ್ಲಿ ಇದು ಪ್ರಾರಂಭವಾಯಿತು</label>
						<?= $form->field($model, 'decision_taking1')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>b) ಎಷ್ಟು ದಿನಗಳವರೆಗೆ ಇದು ಮುಂದುವರಿಯಿತು </label>
						<?= $form->field($model, 'decision_taking2')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>c) ಇದನ್ನು ಮಾಡಿದ್ದು ಯಾರು </label>
						<?= $form->field($model, 'decision_taking3')->textInput()->label(false) ?>
						<?= $form->field($model, 'decision_taking9')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>d) ಇದು ಈಗಲೂ ನಡೆಯುತ್ತಿದೆಯೇ ?</label>
						<?= $form->field($model, 'decision_taking4')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>	
				<div class="row">
					<div class="col-lg-6">
						<label>e) ಅದರ ಪುನರಾವರ್ತನೆ ಏನು ?</label>
						<?= $form->field($model, 'decision_taking5')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>f) ಈ ರೀತಿಯ ವರ್ತನೆ  ಕಾರಣವೇನು ?</label>
						<?= $form->field($model, 'decision_taking6')->textInput()->label(false) ?>
						<?= $form->field($model, 'decision_taking10')->textInput()->label(false) ?>
						<?= $form->field($model, 'decision_taking11')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>g) ಈ ರೀತಿಯ ವರ್ತನೆ  ನೀವು ಸಮರ್ಥಿಸುತ್ತಿದ್ದೀರಾ  ?</label>
						<?= $form->field($model, 'decision_taking7')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
					<div class="col-lg-6">
						<label>h) ಆಕೆ ಗರ್ಭಿಣಿಯಾಗಿದ್ದ ಸಂದರ್ಭದಲ್ಲಿ ಈ ವಿಷಯಗಳಿಗೆ ಒಳಗಾಗಿದ್ದನ್ನು ನೆನೆಪಿಸಬಹುದೇ? </label>
						<?= $form->field($model, 'decision_taking8')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>
				
			</div>
			
			<div class="row">
				<div class="col-lg-6">
				<b>13)ತಿರುಗಾಡಲು ನಿರ್ಬಂಧ  ವಿಧಿಸಲಾಗಿತ್ತು </b>
				<?= $form->field($model, 'restrict')->radioList(array(1=>'ಹೌದು',0=>'ಇಲ್ಲ'),['itemOptions'=>['class' => "restrict"]])->label(false); ?>	
				</div>
			</div>	
			
			<div id="wards13" style = "border: 1px solid black;display:<?php echo ($model->restrict == 1)?"display":"none";?>">
				<div class="row">
					<div class="col-lg-6">
						<label>a) ವಿವಾಹವಾದ ಎಷ್ಟು ತಿಂಗಳುಗಳಲ್ಲಿ ಇದು ಪ್ರಾರಂಭವಾಯಿತು</label>
						<?= $form->field($model, 'restrict1')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>b) ಎಷ್ಟು ದಿನಗಳವರೆಗೆ ಇದು ಮುಂದುವರಿಯಿತು </label>
						<?= $form->field($model, 'restrict2')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>c) ಇದನ್ನು ಮಾಡಿದ್ದು ಯಾರು </label>
						<?= $form->field($model, 'restrict3')->textInput()->label(false) ?>
						<?= $form->field($model, 'restrict9')->textInput()->label(false) ?>
					</div>	
					<div class="col-lg-6">
						<label>d) ಇದು ಈಗಲೂ ನಡೆಯುತ್ತಿದೆಯೇ ?</label>
						<?= $form->field($model, 'restrict4')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>	
				<div class="row">
					<div class="col-lg-6">
						<label>e) ಅದರ ಪುನರಾವರ್ತನೆ ಏನು ?</label>
						<?= $form->field($model, 'restrict5')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>f) ಈ ರೀತಿಯ ವರ್ತನೆ  ಕಾರಣವೇನು ?</label>
						<?= $form->field($model, 'restrict6')->textInput()->label(false) ?>
						<?= $form->field($model, 'restrict10')->textInput()->label(false) ?>
						<?= $form->field($model, 'restrict11')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>g) ಈ ರೀತಿಯ ವರ್ತನೆ  ನೀವು ಸಮರ್ಥಿಸುತ್ತಿದ್ದೀರಾ  ?</label>
						<?= $form->field($model, 'restrict7')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
					<div class="col-lg-6">
						<label>h) ಆಕೆ ಗರ್ಭಿಣಿಯಾಗಿದ್ದ ಸಂದರ್ಭದಲ್ಲಿ ಈ ವಿಷಯಗಳಿಗೆ ಒಳಗಾಗಿದ್ದನ್ನು ನೆನೆಪಿಸಬಹುದೇ? </label>
						<?= $form->field($model, 'restrict8')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>
				
			</div>

			<h1>ದೈಹಿಕ ಹಿಂಸಾಚಾರ </h1>
			
			<div class="row">
				<div class="col-lg-8">
				<b>1)ಹೊಡೆಯುವುದು ಮತ್ತು ಬಡಿಯುವುದು (ದೈಹಿಕ ನೂಕುವುದು, ಎಳೆಯುವುದು,
				ಕೆನ್ನೆಗೆ ಹೊಡೆಯುವುದು, ತಿವಿಯುವುದು, ಒದೆಯುವುದು)</b>
				<?= $form->field($model, 'harm')->radioList(array(1=>'ಹೌದು',0=>'ಇಲ್ಲ'),['itemOptions'=>['class' => "harm"]])->label(false); ?>
				</div>
			</div>	
		
			<div id="wards14" style = "border: 1px solid black;display:<?php echo ($model->harm == 1)?"display":"none";?>">
				<div class="row">
					<div class="col-lg-6">
						<label>a) ವಿವಾಹವಾದ ಎಷ್ಟು ತಿಂಗಳುಗಳಲ್ಲಿ ಇದು ಪ್ರಾರಂಭವಾಯಿತು</label>
						<?= $form->field($model, 'harm1')->textInput()->label(false) ?>
					</div>	
					<div class="col-lg-6">
						<label>b) ಎಷ್ಟು ದಿನಗಳವರೆಗೆ ಇದು ಮುಂದುವರಿಯಿತು </label>
						<?= $form->field($model, 'harm2')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>c) ಇದನ್ನು ಮಾಡಿದ್ದು ಯಾರು </label>
						<?= $form->field($model, 'harm3')->textInput()->label(false) ?>
						<?= $form->field($model, 'harm10')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>d) ಇದು ಈಗಲೂ ನಡೆಯುತ್ತಿದೆಯೇ ?</label>
						<?= $form->field($model, 'harm4')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>	
				<div class="row">
					<div class="col-lg-6">
						<label>e) ಅದರ ಪುನರಾವರ್ತನೆ ಏನು ?</label>
						<?= $form->field($model, 'harm5')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>f) ಈ ರೀತಿಯ ವರ್ತನೆ  ಕಾರಣವೇನು ?</label>
						<?= $form->field($model, 'harm6')->textInput()->label(false) ?>
						<?= $form->field($model, 'harm11')->textInput()->label(false) ?>
						<?= $form->field($model, 'harm12')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>g) ಈ ರೀತಿಯ ವರ್ತನೆ  ನೀವು ಸಮರ್ಥಿಸುತ್ತಿದ್ದೀರಾ  ?</label>
						<?= $form->field($model, 'harm7')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
					<div class="col-lg-6">
						<label>h) ಆಕೆ ಗರ್ಭಿಣಿಯಾಗಿದ್ದ ಸಂದರ್ಭದಲ್ಲಿ ಈ ವಿಷಯಗಳಿಗೆ ಒಳಗಾಗಿದ್ದನ್ನು ನೆನೆಪಿಸಬಹುದೇ? </label>
						<?= $form->field($model, 'harm8')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>i) ಹಿಂಸೆಯ ತೀಕ್ಷ್ಣತೆ </label>
						<?= $form->field($model, 'harm9')->textInput()->label(false) ?>
					</div>
				</div><hr>
				
			</div>
			
			<div class="row">
				<div class="col-lg-8">
				<b>2)ನಿಮ್ಮನು  ಸುಡುವುದು/ ಬರೆಹಾಕುವುದು</b>
				<?= $form->field($model, 'harm_fire')->radioList(array(1=>'ಹೌದು',0=>'ಇಲ್ಲ'),['itemOptions'=>['class' => "harm_fire"]])->label(false); ?>
				</div>
			</div>	
		
			<div id="wards15" style = "border: 1px solid black;display:<?php echo ($model->harm_fire == 1)?"display":"none";?>">
				<div class="row">
					<div class="col-lg-6">
						<label>a) ವಿವಾಹವಾದ ಎಷ್ಟು ತಿಂಗಳುಗಳಲ್ಲಿ ಇದು ಪ್ರಾರಂಭವಾಯಿತು</label>
						<?= $form->field($model, 'harm_fire1')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>b) ಎಷ್ಟು ದಿನಗಳವರೆಗೆ ಇದು ಮುಂದುವರಿಯಿತು </label>
						<?= $form->field($model, 'harm_fire2')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>c) ಇದನ್ನು ಮಾಡಿದ್ದು ಯಾರು </label>
						<?= $form->field($model, 'harm_fire3')->textInput()->label(false) ?>
						<?= $form->field($model, 'harm_fire10')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>d) ಇದು ಈಗಲೂ ನಡೆಯುತ್ತಿದೆಯೇ ?</label>
						<?= $form->field($model, 'harm_fire4')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>	
				<div class="row">
					<div class="col-lg-6">
						<label>e) ಅದರ ಪುನರಾವರ್ತನೆ ಏನು ?</label>
						<?= $form->field($model, 'harm_fire5')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>f) ಈ ರೀತಿಯ ವರ್ತನೆ  ಕಾರಣವೇನು ?</label>
						<?= $form->field($model, 'harm_fire6')->textInput()->label(false) ?>
						<?= $form->field($model, 'harm_fire11')->textInput()->label(false) ?>
						<?= $form->field($model, 'harm_fire12')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>g) ಈ ರೀತಿಯ ವರ್ತನೆ  ನೀವು ಸಮರ್ಥಿಸುತ್ತಿದ್ದೀರಾ  ?</label>
						<?= $form->field($model, 'harm_fire7')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
					<div class="col-lg-6">
						<label>h) ಆಕೆ ಗರ್ಭಿಣಿಯಾಗಿದ್ದ ಸಂದರ್ಭದಲ್ಲಿ ಈ ವಿಷಯಗಳಿಗೆ ಒಳಗಾಗಿದ್ದನ್ನು ನೆನೆಪಿಸಬಹುದೇ? </label>
						<?= $form->field($model, 'harm_fire8')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>i) ಹಿಂಸೆಯ ತೀಕ್ಷ್ಣತೆ </label>
						<?= $form->field($model, 'harm_fire9')->textInput()->label(false) ?>
					</div>
				</div><hr>
				
			</div>
		
			<h1>ಲೈಂಗಿಕ  ಹಿಂಸಾಚಾರ </h1>
			
			<div class="row">
				<div class="col-lg-8">
				<b>1)ಬಲವಂತ ಲೈಂಗಿಕ ಕ್ರಿಯೆ </b>
				<?= $form->field($model, 'forced')->radioList(array(1=>'ಹೌದು',0=>'ಇಲ್ಲ'),['itemOptions'=>['class' => "forced"]])->label(false); ?>
				</div>
			</div>	
		
			<div id="wards16" style = "border: 1px solid black;display:<?php echo ($model->forced == 1)?"display":"none";?>">
				<div class="row">
					<div class="col-lg-6">
						<label>a) ವಿವಾಹವಾದ ಎಷ್ಟು ತಿಂಗಳುಗಳಲ್ಲಿ ಇದು ಪ್ರಾರಂಭವಾಯಿತು</label>
						<?= $form->field($model, 'forced1')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>b) ಎಷ್ಟು ದಿನಗಳವರೆಗೆ ಇದು ಮುಂದುವರಿಯಿತು </label>
						<?= $form->field($model, 'forced2')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>c) ಇದನ್ನು ಮಾಡಿದ್ದು ಯಾರು </label>
						<?= $form->field($model, 'forced3')->textInput()->label(false) ?>
						<?= $form->field($model, 'forced9')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>d) ಇದು ಈಗಲೂ ನಡೆಯುತ್ತಿದೆಯೇ ?</label>
						<?= $form->field($model, 'forced4')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>	
				<div class="row">
					<div class="col-lg-6">
						<label>e) ಅದರ ಪುನರಾವರ್ತನೆ ಏನು ?</label>
						<?= $form->field($model, 'forced5')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>f) ಈ ರೀತಿಯ ವರ್ತನೆ  ಕಾರಣವೇನು ?</label>
						<?= $form->field($model, 'forced6')->textInput()->label(false) ?>
						<?= $form->field($model, 'forced10')->textInput()->label(false) ?>
						<?= $form->field($model, 'forced11')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>g) ಈ ರೀತಿಯ ವರ್ತನೆ  ನೀವು ಸಮರ್ಥಿಸುತ್ತಿದ್ದೀರಾ  ?</label>
						<?= $form->field($model, 'forced7')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
					<div class="col-lg-6">
						<label>h) ಆಕೆ ಗರ್ಭಿಣಿಯಾಗಿದ್ದ ಸಂದರ್ಭದಲ್ಲಿ ಈ ವಿಷಯಗಳಿಗೆ ಒಳಗಾಗಿದ್ದನ್ನು ನೆನೆಪಿಸಬಹುದೇ? </label>
						<?= $form->field($model, 'forced8')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>
				
			</div>
			
			<div class="row">
				<div class="col-lg-8">
				<b>2)ಲೈಂಗಿಕ ನಿರಾಕರಣೆ </b>
				<?= $form->field($model, 'avoid')->radioList(array(1=>'ಹೌದು',0=>'ಇಲ್ಲ'),['itemOptions'=>['class' => "avoid"]])->label(false); ?>
				</div>
			</div>	
		
			<div id="wards17" style = "border: 1px solid black;display:<?php echo ($model->avoid == 1)?"display":"none";?>">
				<div class="row">
					<div class="col-lg-6">
						<label>a) ವಿವಾಹವಾದ ಎಷ್ಟು ತಿಂಗಳುಗಳಲ್ಲಿ ಇದು ಪ್ರಾರಂಭವಾಯಿತು</label>
						<?= $form->field($model, 'avoid1')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>b) ಎಷ್ಟು ದಿನಗಳವರೆಗೆ ಇದು ಮುಂದುವರಿಯಿತು </label>
						<?= $form->field($model, 'avoid2')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>c) ಇದನ್ನು ಮಾಡಿದ್ದು ಯಾರು </label>
						<?= $form->field($model, 'avoid3')->textInput()->label(false) ?>
						<?= $form->field($model, 'avoid9')->textInput()->label(false) ?>
					</div>	
					<div class="col-lg-6">
						<label>d) ಇದು ಈಗಲೂ ನಡೆಯುತ್ತಿದೆಯೇ ?</label>
						<?= $form->field($model, 'avoid4')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>	
				<div class="row">
					<div class="col-lg-6">
						<label>e) ಅದರ ಪುನರಾವರ್ತನೆ ಏನು ?</label>
						<?= $form->field($model, 'avoid5')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>f) ಈ ರೀತಿಯ ವರ್ತನೆ  ಕಾರಣವೇನು ?</label>
						<?= $form->field($model, 'avoid6')->textInput()->label(false) ?>
						<?= $form->field($model, 'avoid10')->textInput()->label(false) ?>
						<?= $form->field($model, 'avoid11')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>g) ಈ ರೀತಿಯ ವರ್ತನೆ  ನೀವು ಸಮರ್ಥಿಸುತ್ತಿದ್ದೀರಾ  ?</label>
						<?= $form->field($model, 'avoid7')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
					<div class="col-lg-6">
						<label>h) ಆಕೆ ಗರ್ಭಿಣಿಯಾಗಿದ್ದ ಸಂದರ್ಭದಲ್ಲಿ ಈ ವಿಷಯಗಳಿಗೆ ಒಳಗಾಗಿದ್ದನ್ನು ನೆನೆಪಿಸಬಹುದೇ? </label>
						<?= $form->field($model, 'avoid8')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>
				
			</div>
			
			<div class="row">
				<div class="col-lg-8">
				<b>3)ಲೈಂಗಿಕವಾಗಿ  ನೋಯಿಸುವುದು /ಗಾಯಗೊಳಿಸುವುದು </b>
				<?= $form->field($model, 'wound')->radioList(array(1=>'ಹೌದು',0=>'ಇಲ್ಲ'),['itemOptions'=>['class' => "wound"]])->label(false); ?>
				</div>
			</div>	
		
			<div id="wards19" style = "border: 1px solid black;display:<?php echo ($model->wound == 1)?"display":"none";?>">
				<div class="row">
					<div class="col-lg-6">
						<label>a) ವಿವಾಹವಾದ ಎಷ್ಟು ತಿಂಗಳುಗಳಲ್ಲಿ ಇದು ಪ್ರಾರಂಭವಾಯಿತು</label>
						<?= $form->field($model, 'wound1')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>b) ಎಷ್ಟು ದಿನಗಳವರೆಗೆ ಇದು ಮುಂದುವರಿಯಿತು </label>
						<?= $form->field($model, 'wound2')->textInput()->label(false) ?>
					</div>
				</div><hr>
					
				<div class="row">
					<div class="col-lg-6">
						<label>c) ಇದನ್ನು ಮಾಡಿದ್ದು ಯಾರು </label>
						<?= $form->field($model, 'wound3')->textInput()->label(false) ?>
						<?= $form->field($model, 'wound9')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>d) ಇದು ಈಗಲೂ ನಡೆಯುತ್ತಿದೆಯೇ ?</label>
						<?= $form->field($model, 'wound4')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>e) ಅದರ ಪುನರಾವರ್ತನೆ ಏನು ?</label>
						<?= $form->field($model, 'wound5')->textInput()->label(false) ?>
					</div>
					<div class="col-lg-6">
						<label>f) ಈ ರೀತಿಯ ವರ್ತನೆ  ಕಾರಣವೇನು ?</label>
						<?= $form->field($model, 'wound6')->textInput()->label(false) ?>
						<?= $form->field($model, 'wound10')->textInput()->label(false) ?>
						<?= $form->field($model, 'wound11')->textInput()->label(false) ?>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-lg-6">
						<label>g) ಈ ರೀತಿಯ ವರ್ತನೆ  ನೀವು ಸಮರ್ಥಿಸುತ್ತಿದ್ದೀರಾ  ?</label>
						<?= $form->field($model, 'wound7')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
					<div class="col-lg-6">
						<label>h) ಆಕೆ ಗರ್ಭಿಣಿಯಾಗಿದ್ದ ಸಂದರ್ಭದಲ್ಲಿ ಈ ವಿಷಯಗಳಿಗೆ ಒಳಗಾಗಿದ್ದನ್ನು ನೆನೆಪಿಸಬಹುದೇ? </label>
						<?= $form->field($model, 'wound8')->radioList(['Yes'=>'Yes','No'=>'No'])->label(false); ?>
					</div>
				</div><hr>
				
			</div>
			
            <div class="hidden" >
            <?= $form->field($model, 'score')->input('number',['readonly'=>true])->label(false);?>
			</div>
			
	</div>
    <?php ActiveForm::end(); ?>


<?php $this->registerJs('
	$("#"+formObj.formName).one("change",function()
	{
		autoSaveObj = formObj;
	});
')?>
<?php $this->registerJs('
	
		$(".bad_words").click(function()
		{
			
			if($(this).val() == 1)
			{	 
				$("#icmr-bad_words1").val("");
				$("#icmr-bad_words2").val("");
				$("#icmr-bad_words3").val("");
				$("#icmr-bad_words4").val("");
				$("#icmr-bad_words7").attr("checked", false);
				$("#icmr-bad_words5").val("");
				$("#icmr-bad_words6").val("");
				//$("#icmr-bad_words7").val("");
				$("#icmr-bad_words8").val("");
				$("#wards1").show();
			}
			else
			{	
				$("#icmr-bad_words1").val("-10");
				$("#icmr-bad_words2").val("-10");
				$("#icmr-bad_words3").val("-10");
				$("#icmr-bad_words4").val("-10");
				$("#icmr-bad_words5").val("-10");
				$("#icmr-bad_words6").val("-10");
				$(".bad_words7").val("-10");
				$("#icmr-bad_words8").val("-10");
				$("#wards1").hide();
			}
		});
		
		$(".threatening").click(function()
		{	
			if($(this).val() == 1)
			{	
				$("#icmr-threatening1").val("");
				$("#icmr-threatening2").val("");
				$("#icmr-threatening3").val("");
				$("#icmr-threatening4").val("");
				$("#icmr-threatening5").val("");
				$("#icmr-threatening6").val("");
				$("#icmr-threatening7").val("");
				$("#icmr-threatening8").val("");
				$("#wards2").show();
			}
			else
			{	
				$("#icmr-threatening1").val("-10");
				$("#icmr-threatening2").val("-10");
				$("#icmr-threatening3").val("-10");
				$("#icmr-threatening4").val("-10");
				$("#icmr-threatening5").val("-10");
				$("#icmr-threatening6").val("-10");
				$("#icmr-threatening7").val("-10");
				$("#icmr-threatening8").val("-10");
				$("#wards2").hide();
			}
		});
		
		$(".threatening_sending_home").click(function()
		{
			
			if($(this).val() == 1)
			{
				$("#icmr-threatening_sending_home1").val("");
				$("#icmr-threatening_sending_home2").val("");
				$("#icmr-threatening_sending_home3").val("");
				$("#icmr-threatening_sending_home4").val("");
				$("#icmr-threatening_sending_home5").val("");
				$("#icmr-threatening_sending_home6").val("");
				$("#icmr-threatening_sending_home7").val("");
				$("#icmr-threatening_sending_home8").val("");
				$("#wards3").show();
			}
			else
			{
				$("#icmr-threatening_sending_home1").val("-10");
				$("#icmr-threatening_sending_home2").val("-10");
				$("#icmr-threatening_sending_home3").val("-10");
				$("#icmr-threatening_sending_home4").val("-10");
				$("#icmr-threatening_sending_home5").val("-10");
				$("#icmr-threatening_sending_home6").val("-10");
				$("#icmr-threatening_sending_home7").val("-10");
				$("#icmr-threatening_sending_home8").val("-10");
				$("#wards3").hide();
			}
		});
		
		$(".send_home").click(function()
		{	
			if($(this).val() == 1)
			{
				$("#icmr-send_home1").val("");
				$("#icmr-send_home2").val("");
				$("#icmr-send_home3").val("");
				$("#icmr-send_home4").val("");
				$("#icmr-send_home5").val("");
				$("#icmr-send_home6").val("");
				$("#icmr-send_home7").val("");
				$("#icmr-send_home8").val("");
				$("#wards4").show();
			}
			else
			{
				$("#icmr-send_home1").val("-10");
				$("#icmr-send_home2").val("-10");
				$("#icmr-send_home3").val("-10");
				$("#icmr-send_home4").val("-10");
				$("#icmr-send_home5").val("-10");
				$("#icmr-send_home6").val("-10");
				$("#icmr-send_home7").val("-10");
				$("#icmr-send_home8").val("-10");
				$("#wards4").hide();
			}
		});
		
		$(".finance_prob").click(function()
		{	
			if($(this).val() == 1)
			{
				$("#icmr-finance_prob1").val("");
				$("#icmr-finance_prob2").val("");
				$("#icmr-finance_prob3").val("");
				$("#icmr-finance_prob4").val("");
				$("#icmr-finance_prob5").val("");
				$("#icmr-finance_prob6").val("");
				$("#icmr-finance_prob7").val("");
				$("#icmr-finance_prob8").val("");
				$("#wards5").show();
			}
			else
			{
				$("#icmr-finance_prob1").val("-10");
				$("#icmr-finance_prob2").val("-10");
				$("#icmr-finance_prob3").val("-10");
				$("#icmr-finance_prob4").val("-10");
				$("#icmr-finance_prob5").val("-10");
				$("#icmr-finance_prob6").val("-10");
				$("#icmr-finance_prob7").val("-10");
				$("#icmr-finance_prob8").val("-10");
				$("#wards5").hide();
			}
		});
		
		$(".fear_look").click(function()
		{	
			if($(this).val() == 1)
			{
				$("#icmr-fear_look1").val("");
				$("#icmr-fear_look2").val("");
				$("#icmr-fear_look3").val("");
				$("#icmr-fear_look4").val("");
				$("#icmr-fear_look5").val("");
				$("#icmr-fear_look6").val("");
				$("#icmr-fear_look7").val("");
				$("#icmr-fear_look8").val("");
				$("#wards6").show();
			}
			else
			{
				$("#icmr-fear_look1").val("-10");
				$("#icmr-fear_look2").val("-10");
				$("#icmr-fear_look3").val("-10");
				$("#icmr-fear_look4").val("-10");
				$("#icmr-fear_look5").val("-10");
				$("#icmr-fear_look6").val("-10");
				$("#icmr-fear_look7").val("-10");
				$("#icmr-fear_look8").val("-10");
				$("#wards6").hide();
			}
		});
		
		$(".avidheya").click(function()
		{	
			if($(this).val() == 1)
			{
				$("#icmr-avidheya1").val("");
				$("#icmr-avidheya2").val("");
				$("#icmr-avidheya3").val("");
				$("#icmr-avidheya4").val("");
				$("#icmr-avidheya5").val("");
				$("#icmr-avidheya6").val("");
				$("#icmr-avidheya7").val("");
				$("#icmr-avidheya8").val("");
				$("#wards7").show();
			}
			else
			{
				$("#icmr-avidheya1").val("-10");
				$("#icmr-avidheya2").val("-10");
				$("#icmr-avidheya3").val("-10");
				$("#icmr-avidheya4").val("-10");
				$("#icmr-avidheya5").val("-10");
				$("#icmr-avidheya6").val("-10");
				$("#icmr-avidheya7").val("-10");
				$("#icmr-avidheya8").val("-10");
				$("#wards7").hide();
			}
		});
		
		$(".udasinathe").click(function()
		{	
			if($(this).val() == 1)
			{
				$("#icmr-udasinathe1").val("");
				$("#icmr-udasinathe2").val("");
				$("#icmr-udasinathe3").val("");
				$("#icmr-udasinathe4").val("");
				$("#icmr-udasinathe5").val("");
				$("#icmr-udasinathe6").val("");
				$("#icmr-udasinathe7").val("");
				$("#icmr-udasinathe8").val("");
				$("#wards8").show();
			}
			else
			{
				$("#icmr-udasinathe1").val("-10");
				$("#icmr-udasinathe2").val("-10");
				$("#icmr-udasinathe3").val("-10");
				$("#icmr-udasinathe4").val("-10");
				$("#icmr-udasinathe5").val("-10");
				$("#icmr-udasinathe6").val("-10");
				$("#icmr-udasinathe7").val("-10");
				$("#icmr-udasinathe8").val("-10");
				$("#wards8").hide();
			}
		});
		
		$(".social_rights").click(function()
		{	
			if($(this).val() == 1)
			{
				$("#icmr-social_rights1").val("");
				$("#icmr-social_rights2").val("");
				$("#icmr-social_rights3").val("");
				$("#icmr-social_rights4").val("");
				$("#icmr-social_rights5").val("");
				$("#icmr-social_rights6").val("");
				$("#icmr-social_rights7").val("");
				$("#icmr-social_rights8").val("");
				$("#wards9").show();
			}
			else
			{
				$("#icmr-social_rights1").val("-10");
				$("#icmr-social_rights2").val("-10");
				$("#icmr-social_rights3").val("-10");
				$("#icmr-social_rights4").val("-10");
				$("#icmr-social_rights5").val("-10");
				$("#icmr-social_rights6").val("-10");
				$("#icmr-social_rights7").val("-10");
				$("#icmr-social_rights8").val("-10");
				$("#wards9").hide();
			}
		});
		
		$(".nirlakshya").click(function()
		{	
			if($(this).val() == 1)
			{
				$("#icmr-nirlakshya1").val("");
				$("#icmr-nirlakshya2").val("");
				$("#icmr-nirlakshya3").val("");
				$("#icmr-nirlakshya4").val("");
				$("#icmr-nirlakshya5").val("");
				$("#icmr-nirlakshya6").val("");
				$("#icmr-nirlakshya7").val("");
				$("#icmr-nirlakshya8").val("");
				$("#wards10").show();
			}
			else
			{
				$("#icmr-nirlakshya1").val("-10");
				$("#icmr-nirlakshya2").val("-10");
				$("#icmr-nirlakshya3").val("-10");
				$("#icmr-nirlakshya4").val("-10");
				$("#icmr-nirlakshya5").val("-10");
				$("#icmr-nirlakshya6").val("-10");
				$("#icmr-nirlakshya7").val("-10");
				$("#icmr-nirlakshya8").val("-10");
				$("#wards10").hide();
			}
		});
		
		$(".personal_needs").click(function()
		{	
			if($(this).val() == 1)
			{
				$("#icmr-personal_needs1").val("");
				$("#icmr-personal_needs2").val("");
				$("#icmr-personal_needs3").val("");
				$("#icmr-personal_needs4").val("");
				$("#icmr-personal_needs5").val("");
				$("#icmr-personal_needs6").val("");
				$("#icmr-personal_needs7").val("");
				$("#icmr-personal_needs8").val("");
				$("#wards11").show();
			}
			else
			{
				$("#icmr-personal_needs1").val("-10");
				$("#icmr-personal_needs2").val("-10");
				$("#icmr-personal_needs3").val("-10");
				$("#icmr-personal_needs4").val("-10");
				$("#icmr-personal_needs5").val("-10");
				$("#icmr-personal_needs6").val("-10");
				$("#icmr-personal_needs7").val("-10");
				$("#icmr-personal_needs8").val("-10");
				$("#wards11").hide();
			}
		});
		
		$(".decision_taking").click(function()
		{	
			if($(this).val() == 1)
			{
				$("#icmr-decision_taking1").val("");
				$("#icmr-decision_taking2").val("");
				$("#icmr-decision_taking3").val("");
				$("#icmr-decision_taking4").val("");
				$("#icmr-decision_taking5").val("");
				$("#icmr-decision_taking6").val("");
				$("#icmr-decision_taking7").val("");
				$("#icmr-decision_taking8").val("");
				$("#wards12").show();
			}
			else
			{
				$("#icmr-decision_taking1").val("-10");
				$("#icmr-decision_taking2").val("-10");
				$("#icmr-decision_taking3").val("-10");
				$("#icmr-decision_taking4").val("-10");
				$("#icmr-decision_taking5").val("-10");
				$("#icmr-decision_taking6").val("-10");
				$("#icmr-decision_taking7").val("-10");
				$("#icmr-decision_taking8").val("-10");
				$("#icmr-decision_taking10").val("-10");
				$("#icmr-decision_taking11").val("-10");
				$("#wards12").hide();
			}
		});
		
		$(".restrict").click(function()
		{	
			if($(this).val() == 1)
			{
				$("#icmr-restrict1").val("");
				$("#icmr-restrict2").val("");
				$("#icmr-restrict3").val("");
				$("#icmr-restrict4").val("");
				$("#icmr-restrict5").val("");
				$("#icmr-restrict6").val("");
				$("#icmr-restrict7").val("");
				$("#icmr-restrict8").val("");
				$("#wards13").show();
			}
			else
			{
				$("#icmr-restrict1").val("-10");
				$("#icmr-restrict2").val("-10");
				$("#icmr-restrict3").val("-10");
				$("#icmr-restrict4").val("-10");
				$("#icmr-restrict5").val("-10");
				$("#icmr-restrict6").val("-10");
				$("#icmr-restrict7").val("-10");
				$("#icmr-restrict8").val("-10");
				$("#wards13").hide();
			}
		});
		
		$(".harm").click(function()
		{	
			if($(this).val() == 1)
			{
				$("#icmr-harm1").val("");
				$("#icmr-harm2").val("");
				$("#icmr-harm3").val("");
				$("#icmr-harm4").val("");
				$("#icmr-harm5").val("");
				$("#icmr-harm6").val("");
				$("#icmr-harm7").val("");
				$("#icmr-harm8").val("");
				$("#icmr-harm9").val("");
				$("#wards14").show();
			}
			else
			{
				$("#icmr-harm1").val("-10");
				$("#icmr-harm2").val("-10");
				$("#icmr-harm3").val("-10");
				$("#icmr-harm4").val("-10");
				$("#icmr-harm5").val("-10");
				$("#icmr-harm6").val("-10");
				$("#icmr-harm7").val("-10");
				$("#icmr-harm8").val("-10");
				$("#icmr-harm9").val("-10");
				$("#wards14").hide();
			}
		});
		
		$(".harm_fire").click(function()
		{
			if($(this).val() == 1)
			{
				$("#icmr-harm_fire1").val("");
				$("#icmr-harm_fire2").val("");
				$("#icmr-harm_fire3").val("");
				$("#icmr-harm_fire4").val("");
				$("#icmr-harm_fire5").val("");
				$("#icmr-harm_fire6").val("");
				$("#icmr-harm_fire7").val("");
				$("#icmr-harm_fire8").val("");
				$("#icmr-harm_fire9").val("");
				$("#wards15").show();
			}
			else
			{
				$("#icmr-harm_fire1").val("-10");
				$("#icmr-harm_fire2").val("-10");
				$("#icmr-harm_fire3").val("-10");
				$("#icmr-harm_fire4").val("-10");
				$("#icmr-harm_fire5").val("-10");
				$("#icmr-harm_fire6").val("-10");
				$("#icmr-harm_fire7").val("-10");
				$("#icmr-harm_fire8").val("-10");
				$("#icmr-harm_fire9").val("-10");
				$("#wards15").hide();
			}
		});
		
		$(".forced").click(function()
		{	
			if($(this).val() == 1)
			{
				$("#icmr-forced1").val("");
				$("#icmr-forced2").val("");
				$("#icmr-forced3").val("");
				$("#icmr-forced4").val("");
				$("#icmr-forced5").val("");
				$("#icmr-forced6").val("");
				$("#icmr-forced7").val("");
				$("#icmr-forced8").val("");
				$("#wards16").show();
			}
			else
			{
				$("#icmr-forced1").val("-10");
				$("#icmr-forced2").val("-10");
				$("#icmr-forced3").val("-10");
				$("#icmr-forced4").val("-10");
				$("#icmr-forced5").val("-10");
				$("#icmr-forced6").val("-10");
				$("#icmr-forced7").val("-10");
				$("#icmr-forced8").val("-10");
				$("#wards16").hide();
			}
		});
		
		$(".avoid").click(function()
		{	
			if($(this).val() == 1)
			{
				$("#icmr-avoid1").val("");
				$("#icmr-avoid2").val("");
				$("#icmr-avoid3").val("");
				$("#icmr-avoid4").val("");
				$("#icmr-avoid5").val("");
				$("#icmr-avoid6").val("");
				$("#icmr-avoid7").val("");
				$("#icmr-avoid8").val("");
				$("#wards17").show();
			}
			else
			{
				$("#icmr-avoid1").val("-10");
				$("#icmr-avoid2").val("-10");
				$("#icmr-avoid3").val("-10");
				$("#icmr-avoid4").val("-10");
				$("#icmr-avoid5").val("-10");
				$("#icmr-avoid6").val("-10");
				$("#icmr-avoid7").val("-10");
				$("#icmr-avoid8").val("-10");
				$("#wards17").hide();
			}
		});
		
		$(".wound").click(function()
		{	
			if($(this).val() == 1)
			{
				$("#icmr-wound1").val("");
				$("#icmr-wound2").val("");
				$("#icmr-wound3").val("");
				$("#icmr-wound4").val("");
				$("#icmr-wound5").val("");
				$("#icmr-wound6").val("");
				$("#icmr-wound7").val("");
				$("#icmr-wound8").val("");
				$("#wards19").show();
			}
			else
			{
				$("#icmr-wound1").val("-10");
				$("#icmr-wound2").val("-10");
				$("#icmr-wound3").val("-10");
				$("#icmr-wound4").val("-10");
				$("#icmr-wound5").val("-10");
				$("#icmr-wound6").val("-10");
				$("#icmr-wound7").val("-10");
				$("#icmr-wound8").val("-10");
				$("#wards19").hide();
			}
		});

			
	
', \yii\web\View::POS_READY);
?>


<?php 
if(!$model->id)
{
	$this->registerJs('
		$("#"+formObj.formName).find("input[type=\'text\']").val("-10")
		$("#"+formObj.formName).find("checkbox").val("-10")
		$("#"+formObj.formName).find("textarea").val("-10")
	',\yii\web\View::POS_READY);
}
?>

<script type="text/javascript">
	var formObj = {'url' : "icmr/ajax",'formName' : "formIcmr"};
    var calc = function () {
        var totalValue = 0;
		var root = 'icmr';
        var id = '#'+root+'-';
		var clas = '.field-'+root+'-';
        var arr = ['bad_words', 'threatening', 'threatening_sending_home',
		'send_home', 'finance_prob', 'fear_look', 'avidheya', 'udasinathe', 'social_rights',
		'nirlakshya', 'personal_needs', 'decision_taking', 'restrict', 'harm', 'harm_fire', 'forced', 'avoid', 'wound']

        for (var i in arr) {
            var val = $(clas + arr[i] + ' input[type=radio]:checked').val();
			if (parseInt(val)) {
                totalValue += parseInt(val);
            }
        }
		
        $(id+'score').val(totalValue);
		autoSaveObj = formObj;
    }
	
	
</script>

