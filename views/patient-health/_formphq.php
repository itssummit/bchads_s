﻿<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\PatientHealth;
$keyPair = [-15 => 'Child Not Co-Operative For Assessment',
		  -14 => 'Not Available For Assessment',
		  -11 => 'Inadequate Information',
		  -10 => 'Not Applicable',
		  -9 => 'Missing',
		  -8 => 'Refused',
		  -7 => 'Partner Accompanied Missing',
		  -6 => 'Family Accompanied Missing',
		  -5 => 'Missing:Not Asked',
		  -3 => 'No Such Relationship/No Partner'];
/* @var $this yii\web\View */
/* @var $model app\models\PatientHealth */
/* @var $form yii\widgets\ActiveForm */
?>
		
<div class="patient-health-form">

    <?php $form = ActiveForm::begin(['id' => 'formPatienthealth']); ?>

    <div id="withBoxShadow" class="col-xs-12 col-md-12 col-lg-12 col-sm-12">
	<h4><b>ಈ ಪ್ರಶ್ನಾವಳಿಗಳು ನಿಮಗೆ ಉತ್ತಮ ಆರೋಗ್ಯ ಆರೈಕೆ ನೀಡಲು ಸಾಧ್ಯವಾಗುವ ಒಂದು ಮುಖ್ಯ ಭಾಗವಾಗಿದೆ . ನಿಮ್ಮ ಉತ್ತರವು ನಿಮಗಿರಬಹುದಾದಂತಹ ತೊಂದರೆ ಗಳನ್ನು ಅರ್ಥಮಾಡಿಕೊಳ್ಳಲು ಸಹಾಯವಾಗುತ್ತದೆ . ಪ್ರಶ್ನೆಗಳನ್ನು ಬಿಟ್ಟು ಬಿಡಿ ಎಂದು ಹೇಳದೆ ಹೊರತು ದಯವಿಟ್ಟು ನಿಮ್ಮ ಉತ್ತಮ ಸಾಮರ್ಥ್ಯದಿಂದ ಎಲ್ಲ ಪ್ರಶ್ನೆಗಳಿಗೂ ಉತ್ತರಿಸಿ   </b></h4>
	<div id="idPhscale" style="overflow-x:auto;">
	<table border="1" class="responsive-scales">
		<thead>
                <tr>
					<th>1.</th>
				    <th>  ಕಳೆದ 2 ವಾರಗಳಲ್ಲಿ , ಈ ಕೆಳಗಿನ ಯಾವುದಾದರೂ ಸಮಸ್ಯೆಗಳಿಂದ ಎಷ್ಟು  ಚಿಂತಿತರಾಗಿದ್ದೀರಾ? </th>
                    <th>ಇಲ್ಲವೇ ಇಲ್ಲ </th>
                    <th>ಹಲವಾರು ದಿನಗಳು  </th>
                    <th>ಅರ್ಧಕ್ಕಿಂತ ಹೆಚ್ಚು ದಿನಗಳು </th>
                    <th>ಹೆಚ್ಚಿನಾಂಶ ಪ್ರತಿದಿನ </th>
					<th>Other Answers</th>
                </tr> 
        </thead>
	<tbody>		
		<tr>
			<td>a.</td>
			<td> ಕೆಲಸಗಳನ್ನು ಮಾಡುವುದರಲ್ಲಿ ಕಡಿಮೆ ಆಸಕ್ತಿ ಅಥವಾ ಆನಂದ </td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'little_interest_pleasure')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			
			<td>
				<?= $form->field($model, 'little_interest_pleasure')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
			</td>
			
		</tr>
		<tr>
			<td>b.</td>
			<td> ಬೇಸರ,ಖಿನ್ನತೆ,ಹತಾಶೆ </td>
			<?php for($i = 0;$i < 4;$i++):?>
				<td><?= $form->field($model, 'down_depressed_hopeless')->radio(['label' => $i, 'value' => $i,])?>
			</td>
			<?php endfor;?>
			
			<td>
				<?= $form->field($model, 'down_depressed_hopeless')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
			</td>
			
		</tr>
		<tr>
			<td>c.</td>
			<td> ನಿದ್ರೆ ಬರುವುದು ಅಥವಾ ನಿದ್ರೆಯಲ್ಲಿರುವುದಕ್ಕೆ ತೊಂದರೆ ಅಥವಾ ಅತಿಯಾಗಿ ನಿದ್ದೆ ಮಾಡುವುದು</td>
			<?php for($i = 0;$i < 4;$i++):?>
				<td><?= $form->field($model, 'falling_sleep')->radio(['label' => $i, 'value' => $i,])?>
			</td>
			<?php endfor;?>
			
			<td>
				<?= $form->field($model, 'falling_sleep')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
			</td>
			
		</tr>
		
		<tr>
			<td>d.</td>
			<td> ಆಯಾಸಗೊಳ್ಳುವುದು ಅಥವಾ ಚೈತನ್ಯ ಇಲ್ಲದಿರುವುದು   </td>
			<?php for($i = 0;$i < 4;$i++):?>
				<td><?= $form->field($model, 'tired')->radio(['label' => $i, 'value' => $i,])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'tired')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
			</td>
		</tr>
		<tr>
			<td>e.</td>
			<td> ಕಡಿಮೆ ಹಸಿವು ಅಥವಾ ಅತಿಯಾಗಿ ತಿನ್ನುವುದು</td>
			<?php for($i = 0;$i < 4;$i++):?>
				<td><?= $form->field($model, 'appetite_overeating')->radio(['label' => $i, 'value' => $i,])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'appetite_overeating')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
			</td>
		</tr>
		<tr>
			<td>f.</td>
			<td>  ನಿಮ್ಮ ಬಗ್ಗೆ ನಿಮಗೇ ಕೆಟ್ಟ ಭಾವನೆ -ಅಥವಾ ನೀವು ನಿಮ್ಮ ಹಾಗೂ   ಕುಟುಂಬದವರ ನಿರೀಕ್ಷೆ ಗಿಂತ ಕೆಳ ಮಟ್ಟದಲ್ಲಿ ಇದ್ದೀರಿ ಎಂಬ ಭಾವನೆ </td>
			<?php for($i = 0;$i < 4;$i++):?>
				<td><?= $form->field($model, 'bad_yourself')->radio(['label' => $i, 'value' => $i,])?>
			</td>
			<?php endfor;?>
			
			<td>
				<?= $form->field($model, 'bad_yourself')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
			</td>
		</tr>
		<tr>
			<td>g.</td>
			<td> ಪತ್ರಿಕೆಯನ್ನು  ಓದಲು ಅಥವಾ ಟೆಲಿವಿಷನ್ ನೋಡುವುದು ಇತ್ಯಾದಿ ವಿಷಯಗಳಲ್ಲಿ ಗಮನ ಕೇಂದ್ರೀಕರಿಸಲು ತೊಂದರೆ </td>
			<?php for($i = 0;$i < 4;$i++):?>
				<td><?= $form->field($model, 'trouble_concentrating')->radio(['label' => $i, 'value' => $i,])?>
			</td>
			<?php endfor;?>
			
			<td>
				<?= $form->field($model, 'trouble_concentrating')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
			</td>
		</tr>
		<tr>
			<td>h.</td>
			<td> ಇತರರ ಗಮನಕ್ಕೆ ಬರದಿರುವಷ್ಟು ನಿಧಾನವಾಗಿ ನಡೆದಾಡುವುದು ಅಥವಾ ಮಾತನಾಡುವುದು ಅಥವಾ ತದ್ವಿರುದ್ದ ವಾಗಿ ಸಾಮಾನ್ಯಕ್ಕಿಂತ ಹೆಚ್ಚು ಅತ್ತಿಂದಿತ್ತ ಓಡಾಡುವಷ್ಟು ಚಡಪಡಿಕೆ ಅಥವಾ ಅಶಾಂತಿ </td>
			<?php for($i = 0;$i < 4;$i++):?>
				<td><?= $form->field($model, 'moving_speaking_slowly')->radio(['label' => $i, 'value' => $i,])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'moving_speaking_slowly')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
			</td>
		</tr>
		<tr>
			<td>i.</td>
			<td> ನೀವು ಸತ್ತರೆ ಚೆನ್ನಾಗಿರುತ್ತದೆ ಎಂಬ ಅಥವಾ ಯಾವುದಾದರು ರೀತಿಯಲ್ಲಿ ನಿಮ್ಮನ್ನು ಹಾನಿಪಡಿಸಿಕೊಳ್ಳುವ ಯೋಚನೆಗಳು  </td>
			<?php for($i = 0;$i < 4;$i++):?>
				<td><?= $form->field($model, 'thought_hurting_yourself')->radio(['label' => $i, 'value' => $i,])?>
			</td>
			<?php endfor;?>
			
			<td>
				<?= $form->field($model, 'thought_hurting_yourself')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
			</td>
		</tr>
	</tbody>
	</table>
	</div><br>
	
	<div id="idPhscale" style="overflow-x:auto;">
	
		
			<div>
			<b>2)</b>
			<b> ಆತಂಕದ ಪ್ರಶ್ನೆ ಗಳು </b><br>
			<b>a.</b>
			<b> ಕಳೆದ ೪ ವಾರಗಳಲ್ಲಿ ,ನಿಮಗೆ ಆತಂಕದ ಆಘಾತವಾಗಿದಿಯೇ ? ಹಠಾತ್ತಾದ ಭಯ ಅಥವಾ  ಗಾಬರಿ ಅನಿಸಿದಂತೆ </b>
			<?= $form->field($model, 'anxiety_attack')->radioList(array(1=>'ಹೌದು',0=>'ಇಲ್ಲ'),['onclick' => 'calc()'])->label(false); ?>
            </div>     
				

	<table border = "1" class="responsive-scales" id = "wards" style = "display:<?php echo ($model->anxiety_attack == 1)?"display":"none";?>">
	<tbody>
			<tr>
				<th></th>
				<th></th>
				<th>ಇಲ್ಲ</th>
				<th>ಹೌದು</th>
				<th>Others Answers</th>
			</tr>
			<tr>
				<td>b.</td>
				<td>  ಇದು ಮೊದಲು  ಯಾವಾಗಲಾದರೂ ಆಗಿದೆಯೇ ?</td>
				<?php for($i = 0;$i < 2;$i++):?>
				<td><?= $form->field($model, 'happen_before')->radio(['label' => $i, 'value' => $i,])?>
				</td>
				<?php endfor;?>
				
				<td>
					<?= $form->field($model, 'happen_before')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
				</td>
				
			</tr>
			<tr>
				<td>c.</td>
				<td> ಈ ರೀತಿಯ ಕೆಲವು  ಆಘಾತಗಳು  ಹಠಾತ್ತನೇ  ಬರುತ್ತದೆಯೇ -ನೀವು ನಿರೀಕ್ಷಿಸದ ಸಂದರ್ಭಗಳಲ್ಲಿ ತಳಮಳ ಅಥವಾ ಅಹಿತಕರವಾಗುವುದು ?</td>
				<?php for($i = 0;$i < 2;$i++):?>
				<td><?= $form->field($model, 'attack_suddenly')->radio(['label' => $i, 'value' => $i,])?>
				</td>
				<?php endfor;?>
				
				<td>
					<?= $form->field($model, 'attack_suddenly')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
				</td>
				
			</tr>
			<tr>
				<td>d.</td>
				<td> ಈ ರೀತಿಯ ಆಘಾತಗಳಿಂದ ಬಹಳಷ್ಟು ತೊಂದರೆಯಾಗಿದಿಯೇ  ಅಥವಾ ನೀವು  ಮತ್ತೊಂದು ಆಘಾತವಾಗುವುದರ ಬಗ್ಗೆ  ಚಿಂತಿಸುತ್ತೀರಾ ? </td>
				<?php for($i = 0;$i < 2;$i++):?>
				<td><?= $form->field($model, 'attack_bother')->radio(['label' => $i, 'value' => $i,])?>
				</td>
				<?php endfor;?>
				
				<td>
					<?= $form->field($model, 'attack_bother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
				</td>
				
			</tr>
            
			<tr>
			<th>3</th>
			<th>  ನಿಮ್ಮ ಕಳೆದ ಕೆಟ್ಟ  ಆತಂಕದ ಆಘಾತದ  ಬಗ್ಗೆ ಯೋಚಿಸಿದಾಗ  </th>
			<th>ಇಲ್ಲ</th>
			<th>ಹೌದು </th>
			<th>Other Answers</th>
			</tr>
		
	
			<tr>
				<td>a.</td>
				<td> ಉಸಿರಾಟದ ತೊಂದರೆ ಆಯಿತೇ ? </td>
				<?php for($i = 0;$i < 2;$i++):?>
				<td><?= $form->field($model, 'short_breath')->radio(['label' => $i, 'value' => $i,])?>
				</td>
				<?php endfor;?>
				
				<td>
					<?= $form->field($model, 'short_breath')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
				</td>
				
			</tr>
	
			<tr>
				<td>b.</td>
				<td> ನಿಮ್ಮ ಹೃದಯ ವೇಗವಾಗಿ ಅಥವಾ ಬಿಟ್ಟು ಬಿಟ್ಟು ಬಡಿಯಿತೇ </td>
				<?php for($i = 0;$i < 2;$i++):?>
				<td><?= $form->field($model, 'heart_race')->radio(['label' => $i, 'value' => $i,])?>
				</td>
				<?php endfor;?>
				
				<td>
					<?= $form->field($model, 'heart_race')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
				</td>
				
			</tr>
			<tr>
				<td>c.</td>
				<td> ನಿಮಗೆ ಎದೆಯಲ್ಲಿ ನೋವು ಅಥವಾ ಒತ್ತಡವಾಯಿತೇ ?</td>
				<?php for($i = 0;$i < 2;$i++):?>
				<td><?= $form->field($model, 'chest_pain')->radio(['label' => $i, 'value' => $i,])?>
				</td>
				<?php endfor;?>
				
				<td>
					<?= $form->field($model, 'chest_pain')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
				</td>
				
			</tr>
			<tr>
				<td>d.</td>
				<td> ನೀವು ಬೆವರಿದಿರಾ ?</td>
				<?php for($i = 0;$i < 2;$i++):?>
				<td><?= $form->field($model, 'sweat')->radio(['label' => $i, 'value' => $i,])?>
				</td>
				<?php endfor;?>
				
				<td>
					<?= $form->field($model, 'sweat')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
				</td>
				
			</tr>
			<tr>
				<td>e.</td>
				<td> ನಿಮಗೆ ಉಸಿರು ಕಟ್ಟಿದಂತೆ ಆಯಿತೇ ?</td>
				<?php for($i = 0;$i < 2;$i++):?>
				<td><?= $form->field($model, 'choking')->radio(['label' => $i, 'value' => $i,])?>
				</td>
				<?php endfor;?>
				
				<td>
					<?= $form->field($model, 'choking')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
				</td>
				
			</tr>
			<tr>
				<td>f.</td>
				<td> ನಿಮಗೆ ಬಿಸಿಯಾದಂತೆ ಅಥವಾ ನಡುಕವಾಯಿತೇ ?</td>
				<?php for($i = 0;$i < 2;$i++):?>
				<td><?= $form->field($model, 'hot_flashes_chills')->radio(['label' => $i, 'value' => $i,])?>
				</td>
				<?php endfor;?>
				
				<td>
					<?= $form->field($model, 'hot_flashes_chills')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
				</td>
				
			</tr>
			<tr>
				<td>g.</td>
				<td> ನಿಮಗೆ ವಾಕರಿಕೆ ಅಥವಾ ಹೊಟ್ಟೆ ಕೆಟ್ಟಂತೆ ಅಥವಾ ಅತಿಸಾರವುಗುವಂತೆ ಅನಿಸಿತೇ ?</td>
				<?php for($i = 0;$i < 2;$i++):?>
				<td><?= $form->field($model, 'nausea')->radio(['label' => $i, 'value' => $i,])?>
				</td>
				<?php endfor;?>
				
				<td>
					<?= $form->field($model, 'nausea')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
				</td>
				
			</tr>
            <tr>
				<td>h.</td>
				<td> ನಿಮಗೆ ತಲೆಸುತ್ತು ಭದ್ರವಾಗಿ  ನಿಲ್ಲಲಾಗದಂತೆ ಅಥವಾ ಮೂರ್ಛೆ ಹೋದಂತೆ ಅನಿಸಿತೇ ?</td>
				<?php for($i = 0;$i < 2;$i++):?>
				<td><?= $form->field($model, 'dizzy_unsteady')->radio(['label' => $i, 'value' => $i,])?>
				</td>
				<?php endfor;?>
				
				<td>
					<?= $form->field($model, 'dizzy_unsteady')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
				</td>
				
				
			</tr>
			<tr>
				<td>i.</td>
				<td> ನಿಮಗೆ ನಿಮ್ಮ ದೇಹದ ಭಾಗಗಳಲ್ಲಿ ಜೋಮು ಅಥವಾ ಮರಕಟ್ಟಿದಂತೆ ಆಯಿತೇ ?</td>
				<?php for($i = 0;$i < 2;$i++):?>
				<td><?= $form->field($model, 'tingling_numbness')->radio(['label' => $i, 'value' => $i,])?>
				</td>
				<?php endfor;?>
				
				<td>
					<?= $form->field($model, 'tingling_numbness')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
				</td>
				
			</tr>
			<tr>
				<td>j.</td>
				<td> ನೀವು ನಡುಕ ಅಥವಾ ಕಂಪಿಸಿದಿರಾ ?</td>
				<?php for($i = 0;$i < 2;$i++):?>
				<td><?= $form->field($model, 'tremble')->radio(['label' => $i, 'value' => $i,])?>
				</td>
				<?php endfor;?>
				
				<td>
					<?= $form->field($model, 'tremble')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
				</td>
				
			</tr>
			<tr>
				<td>k.</td>
				<td> ನೀವು ಸಾಯುತ್ತೀರಾ ಎಂದು ಭಯ ಪಟ್ಟೀರಾ ?</td>
				<?php for($i = 0;$i < 2;$i++):?>
				<td><?= $form->field($model, 'afraid_dieing')->radio(['label' => $i, 'value' => $i,])?>
				</td>
				<?php endfor;?>
				
				<td>
					<?= $form->field($model, 'afraid_dieing')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
				</td>
				
			</tr>
	</tbody>	
	</table>
	</div><br>
	<div id="idPhscale" style="overflow-x:auto;">
	<table border="1" class="responsive-scales">
		<thead>
			<tr>
			<th>4.</th>
			<th>ಕಳೆದ 4ವಾರಗಳಲ್ಲಿ, ಈ ಕೆಳಗಿನ ಸಮಸ್ಯೆಗಳಿಂದ ಎಷ್ಟು ಚಿಂತಿತರಾಗಿದ್ದೀರಾ ?</th>
			<th>ಇಲ್ಲವೇ ಇಲ್ಲ </th>
			<th>ಬಹಳಷ್ಟು ದಿನಗಳು </th>
			<th> ಅರ್ಧಕ್ಕಿಂತ ಹೆಚ್ಚು ದಿನಗಳು </th>
			<th>Other Answers</th>
			</tr>
		</thead>
	<tbody>
			<tr>
				<td>a.</td>
				<td> ತಳಮಳ, ಆತಂಕ, ತುದಿಗಾಲಲ್ಲಿ  ನಿಂತಂತೆ ಅಥವಾ ಬೇರೆ ಬೇರೆ ವಿಷಯಗಳ ಬಗ್ಗೆ ಅತಿಯಾಗಿ ಚಿಂತಿಸುವುದು <br>
				<b>ನೀವು ಇಲ್ಲವೇ ಇಲ್ಲ ಎಂದು ಗುರುತಿಸಿದಲ್ಲಿ ೬ ನೇ ಪ್ರಶ್ನೆಗೆ ಹೋಗಿ </b></td>
				<?php for($i = 0;$i < 3;$i++):?>
				<td><?= $form->field($model, 'nervous')->radio(['label' => $i, 'value' => $i,])?>
				</td>
				<?php endfor;?>
				
				<td>
					<?= $form->field($model, 'nervous')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
				</td>
				
			</tr>
			<tr>
				<td>b.</td>
				<td> ಒಂದು ಕಡೆ  ಕುಳಿತುಕೊಳ್ಳಲು  ಆಗದಷ್ಟು  ನೋವುಗಳು , ಚಡಪಡಿಸುವುದು</td>
				<?php for($i = 0;$i < 3;$i++):?>
				<td><?= $form->field($model, 'restless')->radio(['label' => $i, 'value' => $i,])?>
				</td>
				<?php endfor;?>
				
				<td>
					<?= $form->field($model, 'restless')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
				</td>
				
			</tr>
			<tr>
				<td>c.</td>
				<td> ಸುಲಭವಾಗಿ ಸುಸ್ತಾಗುವುದು</td>
				<?php for($i = 0;$i < 3;$i++):?>
				<td><?= $form->field($model, 'tired_easily')->radio(['label' => $i, 'value' => $i,])?>
				</td>
				<?php endfor;?>
				
				<td>
					<?= $form->field($model, 'tired_easily')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
				</td>
				
			</tr>
			<tr>
				<td>d.</td>
				<td> ಮಾಂಸ ಖಂಡಗಳಲ್ಲಿ  ಒತ್ತಡ, ನೋವುಗಳು ಅಥವಾ ಬಿಗಿತ</td>
				<?php for($i = 0;$i < 3;$i++):?>
				<td><?= $form->field($model, 'muscle_tension')->radio(['label' => $i, 'value' => $i,])?>
				</td>
				<?php endfor;?>
				
				<td>
					<?= $form->field($model, 'muscle_tension')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
				</td>
				
			</tr>
			
			<tr>
				<td>e.</td>
				<td> ನಿದ್ರೆ ಬರಲು ಅಥವಾ ನಿದ್ರಿಸುತ್ತಿರಲು ತೊಂದರೆ </td>
				<?php for($i = 0;$i < 3;$i++):?>
				<td><?= $form->field($model, 'trouble_falling_staying_asleep')->radio(['label' => $i, 'value' => $i,])?>
				</td>
				<?php endfor;?>
				<td>
					<?= $form->field($model, 'trouble_falling_staying_asleep')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
				</td>
				
			</tr>
			
			<tr>
				<td>f.</td>
				<td> ಪತ್ರಿಕೆಯನ್ನು  ಓದಲು ಅಥವಾ ಟೆಲಿವಿಷನ್ ನೋಡುವುದು ಇತ್ಯಾದಿ ವಿಷಯಗಳಲ್ಲಿ ಗಮನ ಕೇಂದ್ರೀಕರಿಸಲು ತೊಂದರೆ </td>
				<?php for($i = 0;$i < 3;$i++):?>
				<td><?= $form->field($model, 'troble_concentrating')->radio(['label' => $i, 'value' => $i,])?>
				</td>
				<?php endfor;?>
				<td>
					<?= $form->field($model, 'troble_concentrating')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
				</td>
				
				
			</tr>
			<tr>
				<td>g.</td>
				<td> ಸುಲಭವಾಗಿ ಕೋಪಗೊಳ್ಳುವುದು ಅಥವಾ ಕಿರಿಕಿರಿಯಾಗುವುದು</td>
				<?php for($i = 0;$i < 3;$i++):?>
				<td><?= $form->field($model, 'easily_annoyed')->radio(['label' => $i,'id' => $i, 'value' => $i,])?>
				</td>
				<?php endfor;?>
				<td>
					<?= $form->field($model, 'easily_annoyed')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
				</td>
			</tr>
			
	</tbody>
	</table>
	<br>
	</div>
	<div id="idPhscale1">
		<div class="col-lg-12">
			<div class="col-lg-8">
			<b>5. ಈ ಪ್ರಶ್ನಾವಳಿಯಲ್ಲಿ ಯಾವುದೇ ತೊಂದರೆಯನ್ನು ಗುರುತಿಸಿದ್ದಲ್ಲಿ, ಅದು ಕೆಲಸದಲ್ಲಿ ಮನೆ ನೋಡಿಕೊಳ್ಳುವುದರಲ್ಲಿ ಅಥವಾ ಇತರರೊಂದಿಗೆ ಬೆರೆಯುವುದರಲ್ಲಿ ನಿಮಗೆ ಎಷ್ಟು ಕಷ್ಟವಾಯಿತು ?</b>
			</div>
			<div class="col-lg-4">
				<?= $form->field($model, 'difficulty_problem')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
			</div>
			
			
		</div>	
			<?= $form->field($model, 'difficulty_problem')->radio(['label'=>'ಕಷ್ಟವಾಗಲೇ ಇಲ್ಲ','value'=>'1','onclick'=>'calc()'])?>
			<?= $form->field($model, 'difficulty_problem')->radio(['label'=>' ಸ್ವಲ್ಪ ಕಷ್ಟ' ,'value'=>'2','onclick'=>'calc()'])?>
			<?= $form->field($model, 'difficulty_problem')->radio(['label'=>'ಹೆಚ್ಚು ಕಷ್ಟ','value'=>'3','onclick'=>'calc()'])?>
			<?= $form->field($model, 'difficulty_problem')->radio(['label'=>'ಅತಿ  ಹೆಚ್ಚು ಕಷ್ಟ','value'=>'4','onclick'=>'calc()'])?>
			
			
			
			
			
			
			<div>
			<label>Score</label>
			<?= $form->field($model, 'score')->input('number',['readonly'=>true])->label(false);?>
			</div>
	</div>
	</div>
	<?php ActiveForm::end(); ?>
</div>


<?php $this->registerJs('
	$(document).on("click", "td", function(e) {
		var selector = $(this).find("input:radio");
		var val = selector.val();
		if(val)
		{
			var rSelector = selector.attr("name");
			$("input[name=\'"+rSelector+"\'][value=\'"+val+"\']").prop("checked",true);
			var optSelector = $(this).parent().find("select").attr("id");
			$("#"+optSelector).val("");
			calc();
		}
	});
', \yii\web\View::POS_END);
?>

<?php $this->registerJs('
	
	$("#patienthealth-anxiety_attack").click(function()
		{
			
			if($(this).val() == 1)
			{
				location.reload();
				$("#wards").show();
			}
			else
			{
				location.reload();
				$("#patienthealth-anxiety_attack").val();
				$("#wards").hide();
				
			}
		});
		
	$("select").change(function(){
		var attrName = $(this).attr("name");
		$("input[name=\'"+attrName+"\']:radio").prop("checked",false);
		autoSaveForm({"url" : "patient-health/ajax","formName" : "formPatienthealth"});
	})	
	
', \yii\web\View::POS_READY);
?>
<script type="text/javascript">
		var calc = function () {
        var totalValue = 0;
        var root= 'patienthealth';
		var id = '#'+root+'-';
		var clas = '.field-'+root+'-';
        var arr = ['little_interest_pleasure','down_depressed_hopeless','falling_sleep','tired','appetite_overeating','bad_yourself',
		'trouble_concentrating','moving_speaking_slowly','thought_hurting_yourself','anxiety_attack','happen_before','attack_suddenly',
		'attack_bother','short_breath','heart_race','chest_pain','sweat','choking','hot_flashes_chills','nausea','dizzy_unsteady',
		'tingling_numbness','tremble','afraid_dieing','nervous','restless','tired_easily','muscle_tension','trouble_falling_staying_asleep',
		'troble_concentrating','easily_annoyed','difficulty_problem'];
		
		for (var i in arr) {
            var val = $(clas + arr[i] + ' input[type=radio]:checked').val();
			if (parseInt(val) >= 0) {
                totalValue += parseInt(val);
            }
        }
		
        $(id+'score').val(totalValue);
		autoSave({'url' : "patient-health/ajax",'formName' : "formPatienthealth"});
    }
</script>
<style>
#idPhscale input[type="number"]
{
    float: left;
    background-color: lightgray;
    border-color: white;
    font-size: 24px;
}
#idPhscale label {
    margin-left: 80px;
    margin-right: 80px;
}
#idPhscale input[type='radio']:after {
        width: 15px;
        height: 15px;
        border-radius: 15px;
        top: -2px;
        left: -1px;
        position: relative;
        background-color: #d1d3d1;
        content: '';
        display: inline-block;
        visibility: visible;
        border: 2px solid white;
    }
#idPhscale input[type='radio']:checked:after {
        width: 15px;
        height: 15px;
        border-radius: 15px;
        top: -2px;
        left: -1px;
        position: relative;
        background-color: #ffa500;
        content: '';
        display: inline-block;
        visibility: visible;
        border: 2px solid white;
		
    }	
	
#idPhscale1 input[type='radio']:after {
        width: 15px;
        height: 15px;
        border-radius: 15px;
        top: -2px;
		background-color: #d1d3d1;
        content: '';
        display: inline-block;
        visibility: visible;
        border: 2px solid white;
    }
#idPhscale1 input[type='radio']:checked:after {
        width: 15px;
        height: 15px;
        border-radius: 15px;
        top: -2px;
        left: -1px;
        position: relative;
        background-color: #ffa500;
        content: '';
        display: inline-block;
        visibility: visible;
        border: 2px solid white;
		
    }		
</style>

