<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PatientHealth */

$this->title = 'Patient Health';
$this->params['breadcrumbs'][] = ['label' => 'Patient Healths', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patient-health-create">

    <h1 align="center">ರೋಗಿಯ ಆರೋಗ್ಯ ಪ್ರಶ್ನಾವಳಿ </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
