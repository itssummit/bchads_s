﻿<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\PatientHealth;
$keyPair = [-15 => '-15: Child Not Co-Operative For Assessment',
          -14 => '-14: Not Available For Assessment',
          -11 => '-11: Inadequate Information',
          -10 => '-10: Not Applicable',
          -9 => '-9: Missing',
          -8 => '-8: Refused',
          -7 => '-7: Partner Accompanied Missing',
          -6 => '-6: Family Accompanied Missing',
          -5 => '-5: Missing:Not Asked',
          -3 => '-3: No Such Relationship/No Partner'];
/* @var $this yii\web\View */
/* @var $model app\models\PatientHealth */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile('@web/js/scales.js',['depends' => [\yii\web\JqueryAsset::className()]]);
?>
        
<div class="patient-health-form">

    <?php $form = ActiveForm::begin(['id' => 'formPatienthealth']); ?>

    <div id="withBoxShadow" class="col-xs-12 col-md-12 col-lg-12 col-sm-12">
    <h4><b>ಈ ಪ್ರಶ್ನಾವಳಿಗಳು ನಿಮಗೆ ಉತ್ತಮ ಆರೋಗ್ಯ ಆರೈಕೆ ನೀಡಲು ಸಾಧ್ಯವಾಗುವ ಒಂದು ಮುಖ್ಯ ಭಾಗವಾಗಿದೆ . ನಿಮ್ಮ ಉತ್ತರವು ನಿಮಗಿರಬಹುದಾದಂತಹ ತೊಂದರೆ ಗಳನ್ನು ಅರ್ಥಮಾಡಿಕೊಳ್ಳಲು ಸಹಾಯವಾಗುತ್ತದೆ . ಪ್ರಶ್ನೆಗಳನ್ನು ಬಿಟ್ಟು ಬಿಡಿ ಎಂದು ಹೇಳದೆ ಹೊರತು ದಯವಿಟ್ಟು ನಿಮ್ಮ ಉತ್ತಮ ಸಾಮರ್ಥ್ಯದಿಂದ ಎಲ್ಲ ಪ್ರಶ್ನೆಗಳಿಗೂ ಉತ್ತರಿಸಿ   </b></h4>
    
    <table border="1" class="responsive-scales">
        <thead>
                <tr>
                    <th>1.</th>
                    <th>  ಕಳೆದ 2 ವಾರಗಳಲ್ಲಿ , ಈ ಕೆಳಗಿನ ಯಾವುದಾದರೂ ಸಮಸ್ಯೆಗಳಿಂದ ಎಷ್ಟು  ಚಿಂತಿತರಾಗಿದ್ದೀರಾ? </th>
                    <th>ಇಲ್ಲವೇ ಇಲ್ಲ </th>
                    <th>ಹಲವಾರು ದಿನಗಳು  </th>
                    <th>ಅರ್ಧಕ್ಕಿಂತ ಹೆಚ್ಚು ದಿನಗಳು </th>
                    <th>ಹೆಚ್ಚಿನಾಂಶ ಪ್ರತಿದಿನ </th>
                    <th>Other Answers</th>
                </tr> 
        </thead>
    <tbody>     
        <tr>
            <td>a.</td>
            <td> ಕೆಲಸಗಳನ್ನು ಮಾಡುವುದರಲ್ಲಿ ಕಡಿಮೆ ಆಸಕ್ತಿ ಅಥವಾ ಆನಂದ </td>
            <?php for($i = 0;$i < 4;$i++):?>
            <td><?= $form->field($model, 'little_interest_pleasure')->radio(['label' => $i, 'value' => $i])?>
            </td>
            <?php endfor;?>
            
            <td>
                <?= $form->field($model, 'little_interest_pleasure')->dropDownList($keyPair,['prompt' => ''])->label(false)?>
            </td>
            
        </tr>
        <tr>
            <td>b.</td>
            <td> ಬೇಸರ,ಖಿನ್ನತೆ,ಹತಾಶೆ </td>
            <?php for($i = 0;$i < 4;$i++):?>
                <td><?= $form->field($model, 'down_depressed_hopeless')->radio(['label' => $i, 'value' => $i,])?>
            </td>
            <?php endfor;?>
            
            <td>
                <?= $form->field($model, 'down_depressed_hopeless')->dropDownList($keyPair,['prompt' => ''])->label(false)?>
            </td>
            
        </tr>
        <tr>
            <td>c.</td>
            <td> ನಿದ್ರೆ ಬರುವುದು ಅಥವಾ ನಿದ್ರೆಯಲ್ಲಿರುವುದಕ್ಕೆ ತೊಂದರೆ ಅಥವಾ ಅತಿಯಾಗಿ ನಿದ್ದೆ ಮಾಡುವುದು</td>
            <?php for($i = 0;$i < 4;$i++):?>
                <td><?= $form->field($model, 'falling_sleep')->radio(['label' => $i, 'value' => $i,])?>
            </td>
            <?php endfor;?>
            
            <td>
                <?= $form->field($model, 'falling_sleep')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
            </td>
            
        </tr>
        
        <tr>
            <td>d.</td>
            <td> ಆಯಾಸಗೊಳ್ಳುವುದು ಅಥವಾ ಚೈತನ್ಯ ಇಲ್ಲದಿರುವುದು   </td>
            <?php for($i = 0;$i < 4;$i++):?>
                <td><?= $form->field($model, 'tired')->radio(['label' => $i, 'value' => $i,])?>
            </td>
            <?php endfor;?>
            <td>
                <?= $form->field($model, 'tired')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
            </td>
        </tr>
        <tr>
            <td>e.</td>
            <td> ಕಡಿಮೆ ಹಸಿವು ಅಥವಾ ಅತಿಯಾಗಿ ತಿನ್ನುವುದು</td>
            <?php for($i = 0;$i < 4;$i++):?>
                <td><?= $form->field($model, 'appetite_overeating')->radio(['label' => $i, 'value' => $i,])?>
            </td>
            <?php endfor;?>
            <td>
                <?= $form->field($model, 'appetite_overeating')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
            </td>
        </tr>
        <tr>
            <td>f.</td>
            <td>  ನಿಮ್ಮ ಬಗ್ಗೆ ನಿಮಗೇ ಕೆಟ್ಟ ಭಾವನೆ -ಅಥವಾ ನೀವು ನಿಮ್ಮ ಹಾಗೂ   ಕುಟುಂಬದವರ ನಿರೀಕ್ಷೆ ಗಿಂತ ಕೆಳ ಮಟ್ಟದಲ್ಲಿ ಇದ್ದೀರಿ ಎಂಬ ಭಾವನೆ </td>
            <?php for($i = 0;$i < 4;$i++):?>
                <td><?= $form->field($model, 'bad_yourself')->radio(['label' => $i, 'value' => $i,])?>
            </td>
            <?php endfor;?>
            
            <td>
                <?= $form->field($model, 'bad_yourself')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
            </td>
        </tr>
        <tr>
            <td>g.</td>
            <td> ಪತ್ರಿಕೆಯನ್ನು  ಓದಲು ಅಥವಾ ಟೆಲಿವಿಷನ್ ನೋಡುವುದು ಇತ್ಯಾದಿ ವಿಷಯಗಳಲ್ಲಿ ಗಮನ ಕೇಂದ್ರೀಕರಿಸಲು ತೊಂದರೆ </td>
            <?php for($i = 0;$i < 4;$i++):?>
                <td><?= $form->field($model, 'trouble_concentrating')->radio(['label' => $i, 'value' => $i,])?>
            </td>
            <?php endfor;?>
            
            <td>
                <?= $form->field($model, 'trouble_concentrating')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
            </td>
        </tr>
        <tr>
            <td>h.</td>
            <td> ಇತರರ ಗಮನಕ್ಕೆ ಬರದಿರುವಷ್ಟು ನಿಧಾನವಾಗಿ ನಡೆದಾಡುವುದು ಅಥವಾ ಮಾತನಾಡುವುದು ಅಥವಾ ತದ್ವಿರುದ್ದ ವಾಗಿ ಸಾಮಾನ್ಯಕ್ಕಿಂತ ಹೆಚ್ಚು ಅತ್ತಿಂದಿತ್ತ ಓಡಾಡುವಷ್ಟು ಚಡಪಡಿಕೆ ಅಥವಾ ಅಶಾಂತಿ </td>
            <?php for($i = 0;$i < 4;$i++):?>
                <td><?= $form->field($model, 'moving_speaking_slowly')->radio(['label' => $i, 'value' => $i,])?>
            </td>
            <?php endfor;?>
            <td>
                <?= $form->field($model, 'moving_speaking_slowly')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
            </td>
        </tr>
        <tr>
            <td>i.</td>
            <td> ನೀವು ಸತ್ತರೆ ಚೆನ್ನಾಗಿರುತ್ತದೆ ಎಂಬ ಅಥವಾ ಯಾವುದಾದರು ರೀತಿಯಲ್ಲಿ ನಿಮ್ಮನ್ನು ಹಾನಿಪಡಿಸಿಕೊಳ್ಳುವ ಯೋಚನೆಗಳು  </td>
            <?php for($i = 0;$i < 4;$i++):?>
                <td><?= $form->field($model, 'thought_hurting_yourself')->radio(['label' => $i, 'value' => $i,])?>
            </td>
            <?php endfor;?>
            
            <td>
                <?= $form->field($model, 'thought_hurting_yourself')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
            </td>
        </tr>
    </tbody>
    </table>

            <div class="col-lg-12">
            <b>2)</b>
            <b> ಆತಂಕದ ಪ್ರಶ್ನೆ ಗಳು </b><br>
            <b>a.</b>
            <b> ಕಳೆದ ೪ ವಾರಗಳಲ್ಲಿ ,ನಿಮಗೆ ಆತಂಕದ ಆಘಾತವಾಗಿದಿಯೇ ? ಹಠಾತ್ತಾದ ಭಯ ಅಥವಾ  ಗಾಬರಿ ಅನಿಸಿದಂತೆ </b>
            <?= $form->field($model, 'anxiety_attack')->radioList(array(1=>'ಹೌದು',0=>'ಇಲ್ಲ'),['itemOptions'=>['class' => "anxiety_attack"]])->label(false); ?>
            </div>     
                

    <table border = "1" class="responsive-scales" id = "wards" style = "display:<?php echo ($model->anxiety_attack == 1)?"display":"none";?>">
    <tbody>
            <tr>
                <th></th>
                <th></th>
                <th>ಇಲ್ಲ</th>
                <th>ಹೌದು</th>
                <th>Others Answers</th>
            </tr>
            <tr>
                <td>b.</td>
                <td>  ಇದು ಮೊದಲು  ಯಾವಾಗಲಾದರೂ ಆಗಿದೆಯೇ ?</td>
                <?php for($i = 0;$i < 2;$i++):?>
                <td><?= $form->field($model, 'happen_before')->radio(['label' => $i, 'value' => $i,'class'=>'happen_before'])?>
                </td>
                <?php endfor;?>
                
                <td>
                    <?= $form->field($model, 'happen_before')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
                </td>
                
            </tr>
            <tr>
                <td>c.</td>
                <td> ಈ ರೀತಿಯ ಕೆಲವು  ಆಘಾತಗಳು  ಹಠಾತ್ತನೇ  ಬರುತ್ತದೆಯೇ -ನೀವು ನಿರೀಕ್ಷಿಸದ ಸಂದರ್ಭಗಳಲ್ಲಿ ತಳಮಳ ಅಥವಾ ಅಹಿತಕರವಾಗುವುದು ?</td>
                <?php for($i = 0;$i < 2;$i++):?>
                <td><?= $form->field($model, 'attack_suddenly')->radio(['label' => $i, 'value' => $i,'class'=>'attack_suddenly'])?>
                </td>
                <?php endfor;?>
                
                <td>
                    <?= $form->field($model, 'attack_suddenly')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
                </td>
                
            </tr>
            <tr>
                <td>d.</td>
                <td> ಈ ರೀತಿಯ ಆಘಾತಗಳಿಂದ ಬಹಳಷ್ಟು ತೊಂದರೆಯಾಗಿದಿಯೇ  ಅಥವಾ ನೀವು  ಮತ್ತೊಂದು ಆಘಾತವಾಗುವುದರ ಬಗ್ಗೆ  ಚಿಂತಿಸುತ್ತೀರಾ ? </td>
                <?php for($i = 0;$i < 2;$i++):?>
                <td><?= $form->field($model, 'attack_bother')->radio(['label' => $i, 'value' => $i,'class'=>'attack_bother'])?>
                </td>
                <?php endfor;?>
                
                <td>
                    <?= $form->field($model, 'attack_bother')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
                </td>
                
            </tr>
            
            <tr>
            <th>3</th>
            <th>  ನಿಮ್ಮ ಕಳೆದ ಕೆಟ್ಟ  ಆತಂಕದ ಆಘಾತದ  ಬಗ್ಗೆ ಯೋಚಿಸಿದಾಗ  </th>
            <th>ಇಲ್ಲ</th>
            <th>ಹೌದು </th>
            <th>Other Answers</th>
            </tr>
        
    
            <tr>
                <td>a.</td>
                <td> ಉಸಿರಾಟದ ತೊಂದರೆ ಆಯಿತೇ ? </td>
                <?php for($i = 0;$i < 2;$i++):?>
                <td><?= $form->field($model, 'short_breath')->radio(['label' => $i, 'value' => $i,'class'=>'short_breath'])?>
                </td>
                <?php endfor;?>
                
                <td>
                    <?= $form->field($model, 'short_breath')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
                </td>
                
            </tr>
    
            <tr>
                <td>b.</td>
                <td> ನಿಮ್ಮ ಹೃದಯ ವೇಗವಾಗಿ ಅಥವಾ ಬಿಟ್ಟು ಬಿಟ್ಟು ಬಡಿಯಿತೇ </td>
                <?php for($i = 0;$i < 2;$i++):?>
                <td><?= $form->field($model, 'heart_race')->radio(['label' => $i, 'value' => $i,'class'=>'heart_race'])?>
                </td>
                <?php endfor;?>
                
                <td>
                    <?= $form->field($model, 'heart_race')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
                </td>
                
            </tr>
            <tr>
                <td>c.</td>
                <td> ನಿಮಗೆ ಎದೆಯಲ್ಲಿ ನೋವು ಅಥವಾ ಒತ್ತಡವಾಯಿತೇ ?</td>
                <?php for($i = 0;$i < 2;$i++):?>
                <td><?= $form->field($model, 'chest_pain')->radio(['label' => $i, 'value' => $i,'class' => 'chest_pain'])?>
                </td>
                <?php endfor;?>
                
                <td>
                    <?= $form->field($model, 'chest_pain')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
                </td>
                
            </tr>
            <tr>
                <td>d.</td>
                <td> ನೀವು ಬೆವರಿದಿರಾ ?</td>
                <?php for($i = 0;$i < 2;$i++):?>
                <td><?= $form->field($model, 'sweat')->radio(['label' => $i, 'value' => $i,'class' => 'sweat'])?>
                </td>
                <?php endfor;?>
                
                <td>
                    <?= $form->field($model, 'sweat')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
                </td>
                
            </tr>
            <tr>
                <td>e.</td>
                <td> ನಿಮಗೆ ಉಸಿರು ಕಟ್ಟಿದಂತೆ ಆಯಿತೇ ?</td>
                <?php for($i = 0;$i < 2;$i++):?>
                <td><?= $form->field($model, 'choking')->radio(['label' => $i, 'value' => $i,'class' => 'choking'])?>
                </td>
                <?php endfor;?>
                
                <td>
                    <?= $form->field($model, 'choking')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
                </td>
                
            </tr>
            <tr>
                <td>f.</td>
                <td> ನಿಮಗೆ ಬಿಸಿಯಾದಂತೆ ಅಥವಾ ನಡುಕವಾಯಿತೇ ?</td>
                <?php for($i = 0;$i < 2;$i++):?>
                <td><?= $form->field($model, 'hot_flashes_chills')->radio(['label' => $i, 'value' => $i,'class' => 'hot_flashes_chills'])?>
                </td>
                <?php endfor;?>
                
                <td>
                    <?= $form->field($model, 'hot_flashes_chills')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
                </td>
                
            </tr>
            <tr>
                <td>g.</td>
                <td> ನಿಮಗೆ ವಾಕರಿಕೆ ಅಥವಾ ಹೊಟ್ಟೆ ಕೆಟ್ಟಂತೆ ಅಥವಾ ಅತಿಸಾರವುಗುವಂತೆ ಅನಿಸಿತೇ ?</td>
                <?php for($i = 0;$i < 2;$i++):?>
                <td><?= $form->field($model, 'nausea')->radio(['label' => $i, 'value' => $i,'class' => 'nausea'])?>
                </td>
                <?php endfor;?>
                
                <td>
                    <?= $form->field($model, 'nausea')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
                </td>
                
            </tr>
            <tr>
                <td>h.</td>
                <td> ನಿಮಗೆ ತಲೆಸುತ್ತು ಭದ್ರವಾಗಿ  ನಿಲ್ಲಲಾಗದಂತೆ ಅಥವಾ ಮೂರ್ಛೆ ಹೋದಂತೆ ಅನಿಸಿತೇ ?</td>
                <?php for($i = 0;$i < 2;$i++):?>
                <td><?= $form->field($model, 'dizzy_unsteady')->radio(['label' => $i, 'value' => $i,'class' => 'dizzy_unsteady'])?>
                </td>
                <?php endfor;?>
                
                <td>
                    <?= $form->field($model, 'dizzy_unsteady')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
                </td>
                
                
            </tr>
            <tr>
                <td>i.</td>
                <td> ನಿಮಗೆ ನಿಮ್ಮ ದೇಹದ ಭಾಗಗಳಲ್ಲಿ ಜೋಮು ಅಥವಾ ಮರಕಟ್ಟಿದಂತೆ ಆಯಿತೇ ?</td>
                <?php for($i = 0;$i < 2;$i++):?>
                <td><?= $form->field($model, 'tingling_numbness')->radio(['label' => $i, 'value' => $i,'class' => 'tingling_numbness'])?>
                </td>
                <?php endfor;?>
                
                <td>
                    <?= $form->field($model, 'tingling_numbness')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
                </td>
                
            </tr>
            <tr>
                <td>j.</td>
                <td> ನೀವು ನಡುಕ ಅಥವಾ ಕಂಪಿಸಿದಿರಾ ?</td>
                <?php for($i = 0;$i < 2;$i++):?>
                <td><?= $form->field($model, 'tremble')->radio(['label' => $i, 'value' => $i,'class' => 'tremble'])?>
                </td>
                <?php endfor;?>
                
                <td>
                    <?= $form->field($model, 'tremble')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
                </td>
                
            </tr>
            <tr>
                <td>k.</td>
                <td> ನೀವು ಸಾಯುತ್ತೀರಾ ಎಂದು ಭಯ ಪಟ್ಟೀರಾ ?</td>
                <?php for($i = 0;$i < 2;$i++):?>
                <td><?= $form->field($model, 'afraid_dieing')->radio(['label' => $i, 'value' => $i,'class' => 'afraid_dieing'])?>
                </td>
                <?php endfor;?>
                
                <td>
                    <?= $form->field($model, 'afraid_dieing')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
                </td>
                
            </tr>
    </tbody>    
    </table>
    <br>
	
    <div class="col-lg-12">
            <b>4a)</b>
            <b> ಕಳೆದ ೪ ವಾರಗಳಲ್ಲಿ , ತಳಮಳ, ಆತಂಕ, ತುದಿಗಾಲಲ್ಲಿ  ನಿಂತಂತೆ ಅಥವಾ ಬೇರೆ ಬೇರೆ ವಿಷಯಗಳ ಬಗ್ಗೆ ಅತಿಯಾಗಿ ಚಿಂತಿತರಾಗಿದ್ದೀರಾ ? </b>
            <?= $form->field($model, 'nervous')->radioList(array(0=>'ಇಲ್ಲವೇ ಇಲ್ಲ',1=>'ಬಹಳಷ್ಟು ದಿನಗಳು ',2=>' ಅರ್ಧಕ್ಕಿಂತ ಹೆಚ್ಚು ದಿನಗಳು '),['itemOptions'=>['class' => "nervous"]])->label(false); ?>
    </div>
	<div id="fourb" class="col-lg-12" style = "display:<?php echo ($model->nervous >= 1)?"display":"none";?>">
    <table border="1" class="responsive-scales">
        <thead>
            <tr>
            <th>ಕ್ರ .ಸಂ </th>
            <th>ಕಳೆದ 4ವಾರಗಳಲ್ಲಿ, ಈ ಕೆಳಗಿನ ಸಮಸ್ಯೆಗಳಿಂದ ಎಷ್ಟು ಚಿಂತಿತರಾಗಿದ್ದೀರಾ ?</th>
            <th>ಇಲ್ಲವೇ ಇಲ್ಲ </th>
            <th>ಬಹಳಷ್ಟು ದಿನಗಳು </th>
            <th> ಅರ್ಧಕ್ಕಿಂತ ಹೆಚ್ಚು ದಿನಗಳು </th>
            <th>Other Answers</th>
            </tr>
        </thead>
    
            <!--<tr>
                <td>a.</td>
                <td> ತಳಮಳ, ಆತಂಕ, ತುದಿಗಾಲಲ್ಲಿ  ನಿಂತಂತೆ ಅಥವಾ ಬೇರೆ ಬೇರೆ ವಿಷಯಗಳ ಬಗ್ಗೆ ಅತಿಯಾಗಿ ಚಿಂತಿಸುವುದು <br>
                <b>ನೀವು ಇಲ್ಲವೇ ಇಲ್ಲ ಎಂದು ಗುರುತಿಸಿದಲ್ಲಿ 5 ನೇ ಪ್ರಶ್ನೆಗೆ ಹೋಗಿ </b></td>
                < ?php for($i = 0;$i < 3;$i++):?>
                <td>< ?= $form->field($model, 'nervous')->radio(['label' => $i, 'value' => $i,])?>
                </td>
                < ?php endfor;?>
                
                <td>
                    < ?= $form->field($model, 'nervous')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
                </td>
                
            </tr>-->
            
	
	
				
                
        


			<tr>
                <td>4b.</td>
                <td>ಒಂದು ಕಡೆ  ಕುಳಿತುಕೊಳ್ಳಲು  ಆಗದಷ್ಟು  ನೋವುಗಳು , ಚಡಪಡಿಸುವುದು</td>
                <?php for($i = 0;$i < 3;$i++):?>
                <td><?= $form->field($model, 'restless')->radio(['label' => $i, 'value' => $i,'class'=>'restless'])?>
                </td>
                <?php endfor;?>
                
                <td>
                    <?= $form->field($model, 'restless')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
                </td>
                
            </tr>


		
            <tr>
                <td>4c.</td>
                <td> ಸುಲಭವಾಗಿ ಸುಸ್ತಾಗುವುದು</td>
                <?php for($i = 0;$i < 3;$i++):?>
                <td><?= $form->field($model, 'tired_easily')->radio(['label' => $i, 'value' => $i,'class'=>'restless'])?>
                </td>
                <?php endfor;?>
                
                <td>
                    <?= $form->field($model, 'tired_easily')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
                </td>
                
            </tr>
            <tr>
                <td>4d.</td>
                <td> ಮಾಂಸ ಖಂಡಗಳಲ್ಲಿ  ಒತ್ತಡ, ನೋವುಗಳು ಅಥವಾ ಬಿಗಿತ</td>
                <?php for($i = 0;$i < 3;$i++):?>
                <td><?= $form->field($model, 'muscle_tension')->radio(['label' => $i, 'value' => $i,'class'=>'restless'])?>
                </td>
                <?php endfor;?>
                
                <td>
                    <?= $form->field($model, 'muscle_tension')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
                </td>
                
            </tr>
            
            <tr>
                <td>4e.</td>
                <td> ನಿದ್ರೆ ಬರಲು ಅಥವಾ ನಿದ್ರಿಸುತ್ತಿರಲು ತೊಂದರೆ </td>
                <?php for($i = 0;$i < 3;$i++):?>
                <td><?= $form->field($model, 'trouble_falling_staying_asleep')->radio(['label' => $i, 'value' => $i,'class'=>'restless'])?>
                </td>
                <?php endfor;?>
                <td>
                    <?= $form->field($model, 'trouble_falling_staying_asleep')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
                </td>
                
            </tr>
            
            <tr>
                <td>4f.</td>
                <td> ಪತ್ರಿಕೆಯನ್ನು  ಓದಲು ಅಥವಾ ಟೆಲಿವಿಷನ್ ನೋಡುವುದು ಇತ್ಯಾದಿ ವಿಷಯಗಳಲ್ಲಿ ಗಮನ ಕೇಂದ್ರೀಕರಿಸಲು ತೊಂದರೆ </td>
                <?php for($i = 0;$i < 3;$i++):?>
                <td><?= $form->field($model, 'troble_concentrating')->radio(['label' => $i, 'value' => $i,'class'=>'restless'])?>
                </td>
                <?php endfor;?>
                <td>
                    <?= $form->field($model, 'troble_concentrating')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
                </td>
                
                
            </tr>
            <tr>
                <td>4g.</td>
                <td> ಸುಲಭವಾಗಿ ಕೋಪಗೊಳ್ಳುವುದು ಅಥವಾ ಕಿರಿಕಿರಿಯಾಗುವುದು</td>
                <?php for($i = 0;$i < 3;$i++):?>
                <td><?= $form->field($model, 'easily_annoyed')->radio(['label' => $i,'id' => $i, 'value' => $i,'class'=>'restless'])?>
                </td>
                <?php endfor;?>
                <td>
                    <?= $form->field($model, 'easily_annoyed')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
                </td>
            </tr>
			
		</tbody>
		</table>
		</div><br><br>
		
		<div id ="fifth">
			<table border="1">
				<tbody>
				<tr>
				
					<td>
					<b>5. ಈ ಪ್ರಶ್ನಾವಳಿಯಲ್ಲಿ ಯಾವುದೇ ತೊಂದರೆಯನ್ನು ಗುರುತಿಸಿದ್ದಲ್ಲಿ, ಅದು ಕೆಲಸದಲ್ಲಿ ಮನೆ ನೋಡಿಕೊಳ್ಳುವುದರಲ್ಲಿ ಅಥವಾ ಇತರರೊಂದಿಗೆ ಬೆರೆಯುವುದರಲ್ಲಿ ನಿಮಗೆ ಎಷ್ಟು ಕಷ್ಟವಾಯಿತು ?</b>
					</td>
					<td><?= $form->field($model, 'difficulty_problem')->radio(['label' => 'ಕಷ್ಟವಾಗಲೇ ಇಲ್ಲ','value' => 0,])->label(false);?>
					</td>
					<td><?= $form->field($model, 'difficulty_problem')->radio(['label' => ' ಸ್ವಲ್ಪ ಕಷ್ಟ','value' => 1,])->label(false);?>
					</td>
					<td><?= $form->field($model, 'difficulty_problem')->radio(['label' => 'ಹೆಚ್ಚು ಕಷ್ಟ','value' => 2,])->label(false);?>
					</td>
					<td>
						<?= $form->field($model, 'difficulty_problem')->dropDownList($keyPair,['prompt' => '  '])->label(false)?>
					</td>
				</tr>
				</tbody>
			</table>
		</div>
            
     
            <div class="hidden">
            <label>Score</label>
            <?= $form->field($model, 'score')->input('number',['readonly'=>true])->label(false);?>
            </div>
    
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php $this->registerJs('
	$("#"+formObj.formName).one("change",function()
	{
		autoSaveObj = formObj;
	});
',\yii\web\View::POS_READY)?>
<?php $this->registerJs('
	
	$(".nervous").click(function()
        {
            if($(this).val() >= 1)
			{
                $(".restless").attr("checked",false);
                $("select[name=\'PatientHealth[restless]\']").val("");
                
                $(".tired_easily").attr("checked",false);
                $("select[name=\'PatientHealth[tired_easily]\']").val("");
                
                $(".muscle_tension").attr("checked",false);
                $("select[name=\'PatientHealth[muscle_tension]\']").val("");
                
                $(".trouble_falling_staying_asleep").attr("checked",false);
                $("select[name=\'PatientHealth[trouble_falling_staying_asleep]\']").val("");
                
                $(".troble_concentrating").attr("checked",false);
                $("select[name=\'PatientHealth[troble_concentrating]\']").val("");
                
                $(".easily_annoyed").attr("checked",false);
                $("select[name=\'PatientHealth[easily_annoyed]\']").val("");
				$("#fourb").show();
			}
			else
			{
				$(".restless").attr("checked",false);
				$("select[name=\'PatientHealth[restless]\']").val("-10");
				
                $(".tired_easily").attr("checked",false);
				$("select[name=\'PatientHealth[tired_easily]\']").val("-10");
				
				$(".muscle_tension").attr("checked",false);
				$("select[name=\'PatientHealth[muscle_tension]\']").val("-10");
				
				$(".trouble_falling_staying_asleep").attr("checked",false);
				$("select[name=\'PatientHealth[trouble_falling_staying_asleep]\']").val("-10");
				
				$(".troble_concentrating").attr("checked",false);
				$("select[name=\'PatientHealth[troble_concentrating]\']").val("-10");
				
				$(".easily_annoyed").attr("checked",false);
				$("select[name=\'PatientHealth[easily_annoyed]\']").val("-10");
				calc();
				$("#fourb").hide();
			}
        });
	
	
    
    $(".anxiety_attack").click(function()
        {
            
            if($(this).val() == 1)
            {
                 $(".happen_before").attr("checked",false);
                $("select[name=\'PatientHealth[happen_before]\']").val("");
                
                $(".attack_suddenly").attr("checked",false);
                $("select[name=\'PatientHealth[attack_suddenly]\']").val("");
                
                $(".attack_bother").attr("checked",false);
                $("select[name=\'PatientHealth[attack_bother]\']").val("");
                
                $(".short_breath").attr("checked",false);
                $("select[name=\'PatientHealth[short_breath]\']").val("");
                
                $(".heart_race").attr("checked",false);
                $("select[name=\'PatientHealth[heart_race]\']").val("");
                
                $(".chest_pain").attr("checked",false);
                $("select[name=\'PatientHealth[chest_pain]\']").val("");
                
                $(".sweat").attr("checked",false);
                $("select[name=\'PatientHealth[sweat]\']").val("");
                
                $(".choking").attr("checked",false);
                $("select[name=\'PatientHealth[choking]\']").val("");
                
                $(".hot_flashes_chills").attr("checked",false);
                $("select[name=\'PatientHealth[hot_flashes_chills]\']").val("");
                
                $(".nausea").attr("checked",false);
                $("select[name=\'PatientHealth[nausea]\']").val("");
                
                $(".dizzy_unsteady").attr("checked",false);
                $("select[name=\'PatientHealth[dizzy_unsteady]\']").val("");
                
                $(".tingling_numbness").attr("checked",false);
                $("select[name=\'PatientHealth[tingling_numbness]\']").val("");
                
                $(".tremble").attr("checked",false);
                $("select[name=\'PatientHealth[tremble]\']").val("");
                
                $(".afraid_dieing").attr("checked",false);
                $("select[name=\'PatientHealth[afraid_dieing]\']").val("");
                
                
                $("#wards").show();
            }
            else
            {
                $(".happen_before").attr("checked",false);
				$("select[name=\'PatientHealth[happen_before]\']").val("-10");
				
                $(".attack_suddenly").attr("checked",false);
				$("select[name=\'PatientHealth[attack_suddenly]\']").val("-10");
				
				$(".attack_bother").attr("checked",false);
				$("select[name=\'PatientHealth[attack_bother]\']").val("-10");
				
				$(".short_breath").attr("checked",false);
				$("select[name=\'PatientHealth[short_breath]\']").val("-10");
				
				$(".heart_race").attr("checked",false);
				$("select[name=\'PatientHealth[heart_race]\']").val("-10");
				
				$(".chest_pain").attr("checked",false);
				$("select[name=\'PatientHealth[chest_pain]\']").val("-10");
				
				$(".sweat").attr("checked",false);
				$("select[name=\'PatientHealth[sweat]\']").val("-10");
				
				$(".choking").attr("checked",false);
				$("select[name=\'PatientHealth[choking]\']").val("-10");
				
				$(".hot_flashes_chills").attr("checked",false);
				$("select[name=\'PatientHealth[hot_flashes_chills]\']").val("-10");
				
				$(".nausea").attr("checked",false);
				$("select[name=\'PatientHealth[nausea]\']").val("-10");
				
				$(".dizzy_unsteady").attr("checked",false);
				$("select[name=\'PatientHealth[dizzy_unsteady]\']").val("-10");
				
				$(".tingling_numbness").attr("checked",false);
				$("select[name=\'PatientHealth[tingling_numbness]\']").val("-10");
				
				$(".tremble").attr("checked",false);
				$("select[name=\'PatientHealth[tremble]\']").val("-10");
				
				$(".afraid_dieing").attr("checked",false);
				$("select[name=\'PatientHealth[afraid_dieing]\']").val("-10");
				
				calc();
                $("#wards").hide();
                
            }
        });
        
   
', \yii\web\View::POS_READY);
?>
<script type="text/javascript">
		var formObj = {url : "patient-health/ajax",formName : "formPatienthealth"};
        var calc = function () {
        var totalValue = 0;
        var root= 'patienthealth';
        var id = '#'+root+'-';
        var clas = '.field-'+root+'-';
        var arr = ['little_interest_pleasure','down_depressed_hopeless','falling_sleep','tired','appetite_overeating','bad_yourself','trouble_concentrating','moving_speaking_slowly','thought_hurting_yourself','anxiety_attack','happen_before','attack_suddenly','attack_bother','short_breath','heart_race','chest_pain','sweat','choking','hot_flashes_chills','nausea','dizzy_unsteady','tingling_numbness','tremble','afraid_dieing','nervous','restless','tired_easily','muscle_tension','trouble_falling_staying_asleep','troble_concentrating','easily_annoyed','difficulty_problem'];
        
        for (var i in arr) {
            var val = $(clas + arr[i] + ' input[type=radio]:checked').val();
            if (parseInt(val) >= 0) {
                totalValue += parseInt(val);
            }
        }
        
        $(id+'score').val(totalValue);
        autoSaveObj = formObj;
    }
</script>
<style>
#foura table{
	height:200px;
}
#fourb table{
	height:500px;
}

</style>
