<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$keyPair = [-15 => '-15: Child Not Co-Operative For Assessment',
		  -14 => '-14: Not Available For Assessment',
		  -11 => '-11: Inadequate Information',
		  -10 => '-10: Not Applicable',
		  -9 => '-9: Missing',
		  -8 => '-8: Refused',
		  -7 => '-7: Partner Accompanied Missing',
		  -6 => '-6: Family Accompanied Missing',
		  -5 => '-5: Missing:Not Asked',
		  -3 => '-3: No Such Relationship/No Partner'];
/* @var $this yii\web\View */
/* @var $model app\models\Breastfeeding */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile('@web/js/scales.js',['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div   class="breastfeeding-form">

    <?php $form = ActiveForm::begin(['id'=>'formBreastfeeding']); ?>
	<div id="idbfscale">
	<div  id="withBoxShadow" class="col-xs-12 col-md-12 col-lg- col-sm-12">
        <table border="1" class="responsive-scales">
            <thead>
                <tr>
				    <th></th>
                    <th> ವಿಶ್ವಾಸ ಇಲ್ಲವೇ ಇಲ್ಲ </th>
                    <th> ಅಷ್ಟಾಗಿ ವಿಶ್ವಾಸವಿಲ್ಲ </th>
                    <th> ಕೆಲವೊಮ್ಮೆ ವಿಶ್ವಾಸವಿದೆ  </th>
                    <th> ವಿಶ್ವಾಸದಿಂದ್ದಿನಿ</th>
                    <th>ಹೆಚ್ಚು ವಿಶ್ವಾಸದಿಂದ್ದಿನಿ  </th>
                    <th>Other Answers</th>
                </tr> 
            </thead>
            <tbody>
			<tr>
                    <td>1) ನನ್ನ ಮಗುವಿಗೆ ಯಾವಾಗಲೂ ಸಾಕಾಗುವಷ್ಟು ಹಾಲು ಸಿಗುತ್ತಿದೆ ಎಂದು ನಾನು ಖಚಿತಪಡಿಸಬಹುದು.</td>
					<?php for($i = 1;$i < 6;$i++):?>
					<td>
                    <?= $form->field($model, 'determine_enough_milk')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td>
						 <?= $form->field($model, 'determine_enough_milk')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

					</td>
			</tr>

    
			<tr>
                    <td>2) ನನಗಿರುವ ಬೇರೆ ಸವಾಲುಗಳಂತೆಯೇ ನಾನು ಯಾವಾಗಲು ಯಶಸ್ವಿಯಾಗಿ ಎದೆ ಹಾಲು ಕುಡಿಸುವುದನ್ನು ನಿಭಾಯಿಸಬಲ್ಲೆ  </td>
                    <?php for($i = 1;$i < 6;$i++):?>
					<td><?= $form->field($model, 'successfully_cope')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td>
					 <?= $form->field($model, 'successfully_cope')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

					</td>
			</tr>
			
			<tr>
                    <td>3) ನಾನು ಬೇರೆ ಯಾವುದೇ ಆಹಾರವನ್ನು ಬಳಸದೇ ನನ್ನ ಮಗುವಿಗೆ ಯಾವಾಗಲೂ ಎದೆ ಹಾಲು ಕುಡಿಸಬಲ್ಲೆ</td>
                    <?php for($i = 1;$i < 6;$i++):?>
					<td><?= $form->field($model, 'without_formula')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td>
					 <?= $form->field($model, 'without_formula')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

					</td>
			</tr>
			
			<tr >
                    <td>4) ನನ್ನ ಮಗುವಿಗೆ ಪೂರ್ಣವಾಗಿ ಹಾಲುಣಿಸುವಾಗ ನನ್ನ ಮಗುವು ಮೊಲೆಯ ತೊಟ್ಟನ್ನು ಸರಿಯಾದ ರೀತಿಯಲ್ಲಿ  ಹಿಡಿದುಕೊಂಡಿದೆಯೇ  ಎಂದು ಖಚಿತಪಡಿಸಬಲ್ಲೆ </td>
                    <?php for($i = 1;$i < 6;$i++):?>
					<td><?= $form->field($model, 'lached_properly')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td>
					 <?= $form->field($model, 'lached_properly')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

					</td>
			</tr>
			<tr >
                    <td>5)ನಾನು ಯಾವಾಗಲೂ ಎದೆ ಹಾಲುಣಿಸುವ ಸಂದರ್ಭವನ್ನು ನನಗೆ ತೃಪ್ತಿಯಾಗುವಂತೆ ನಿಭಾಯಿಸ ಬಲ್ಲೆ</td>
                    <?php for($i = 1;$i < 6;$i++):?>
					<td><?= $form->field($model, 'manage_situation')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td>
					 <?= $form->field($model, 'manage_situation')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

					</td>
			</tr >
			
			<tr >
                    <td>6) ನನ್ನ ಮಗುವ್ರ ಆಳುತ್ತಿದ್ದರೂ ಸಹ ನಾನು ಎದೆ ಹಾಲುಣಿಸುವುದನ್ನು ನಿಭಾಯಿಸ ಬಲ್ಲೆ </td>
                    <?php for($i = 1;$i < 6;$i++):?>
					<td><?= $form->field($model, 'manage_crying')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td>
						 <?= $form->field($model, 'manage_crying')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

					</td>
			</tr>

			<tr >
                    <td>7) ನಾನು ನನ್ನ ಮಗುವಿಗೆ ಯಾವಾಗಲೂ ಎದೆ ಹಾಲು ಕುಡಿಸುತ್ತಿರಲು ಇಷ್ಟಪಡುತ್ತೇನೆ</td>
                    <?php for($i = 1;$i < 6;$i++):?>
					<td><?= $form->field($model, 'wanting_breastfeed')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td>
						 <?= $form->field($model, 'wanting_breastfeed')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

					</td>
			</tr>

			<tr >
                    <td>8) ನನ್ನ ಕುಟು೦ಬದ ಸದಸ್ಯರು ಇದ್ದಾಗಲೂ ಸಹ ನಾನು ಆರಾಮವಾಗಿ ನನ್ನ ಮಗುವಿಗೆ ಯಾವಾಗಲೂ ಎದೆ ಹಾಲು ಕುಡಿಸಬಲ್ಲೆ</td>
                    <?php for($i = 1;$i < 6;$i++):?>
					<td><?= $form->field($model, 'comfortable_family_members')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td>
						 <?= $form->field($model, 'comfortable_family_members')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

					</td>
			</tr>

			<tr >
                    <td>9)ನಾನು ಯಾವಾಗಲೂ ಎದೆ ಹಾಲು ಕುಡಿಸುವ ಅನುಭವದಿಂದ ತೃಪ್ತಿಪಡಬಲ್ಲೆ</td>
                    <?php for($i = 1;$i < 6;$i++):?>
					<td><?= $form->field($model, 'satisfied_experience')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td>
						 <?= $form->field($model, 'satisfied_experience')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

					</td>
			</tr>
			
			<tr>
                    <td>10) ಎದೆ ಹಾಲು ಕುಡಿಸುವುದಕ್ಕೆ ಸಾಕಷ್ಟು  ಸಮಯ ಬೇಕೆ೦ಬುದನ್ನು ತಿಳಿದಿದ್ದರೂ, ಅದನ್ನು ಯಾವಾಗಲೂ ನಿರ್ವಹಿಸ ಬಲ್ಲೆ </td>
                    <?php for($i = 1;$i < 6;$i++):?>
					<td><?= $form->field($model, 'deal_time_consuming')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td>
 <?= $form->field($model, 'deal_time_consuming')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

					</td>
			</tr>

			<tr >
                    <td>11) ನಾನು ಮಗುವಿಗೆ ಯಾವಾಗಲು ಒಂದು ಮೊಲೆಯ ಹಾಲನ್ನು ಪೂರ್ತಿಯಾಗಿ ಕುಡಿಸಿದ ನಂತರವೇ ಇನ್ನೊಂದು ಮೊಲೆ ಹಾಲು ಕುಡಿಸಬಲ್ಲೆ </td>
                    <?php for($i = 1;$i < 6;$i++):?>
					<td><?= $form->field($model, 'finish_1breast')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td>
						 <?= $form->field($model, 'finish_1breast')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

					</td>
			</tr>

			<tr >
                    <td>12) ಪ್ರತಿ ಬಾರಿಯೂ ನಾನು ಮಗುವಿಗೆ ಹಾಲುಣಿಸುವುದನ್ನು ಮುಂದುವರಿಸಬಲ್ಲೆ</td>
                    <?php for($i = 1;$i < 6;$i++):?>
					<td><?= $form->field($model, 'continue_feeding')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td>
						 <?= $form->field($model, 'continue_feeding')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

					</td>
			</tr>

			<tr >
                    <td>13) ಮಗುವಿನ ಎದೆ ಹಾಲು ಕುಡಿಯುವ ಬಯಕೆಯನ್ನು ನಾನು ಯಾವಾಗಲೂ ಸಮರ್ಥವಾಗಿ ನಿಭಾಯಿಸಬಲ್ಲೆ</td>
                    <?php for($i = 1;$i < 6;$i++):?>
					<td><?= $form->field($model, 'keepup_baby_demands')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td>
						 <?= $form->field($model, 'keepup_baby_demands')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

					</td>
			</tr>

			<tr >
                    <td>14)ನನ್ನ ಮಗುವು ಎದೆ ಹಾಲು ಸಂಪೂರ್ಣವಾಗಿ ಕುಡಿದಿದೆ ಎಂದು ನಾನು ಯಾವಾಗಲು ಹೇಳಬಲ್ಲೆ </td>
                    <?php for($i = 1;$i < 6;$i++):?>
					<td><?= $form->field($model, 'tell_finished')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td>
						 <?= $form->field($model, 'tell_finished')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

					</td>
			</tr>		

			<tr>
                <td></td>
                <td id="scaleflag" colspan="5" class="hidden"><?= $form->field($model, 'score')->input('number',['readonly'=>true])->label(false);?></td>
            </tr>
			
			
			</tbody>
			</table>
	</div>
	</div>
	<?php ActiveForm::end(); ?>
</div>

<?php $this->registerJs('
	$("#"+formObj.formName).one("change",function()
	{
		autoSaveObj = formObj;
	});
',\yii\web\View::POS_READY)?>

<script type="text/javascript">
	formObj = {'url': "breastfeeding/ajax",'formName' : "formBreastfeeding"};
     var calc = function () {
        var totalValue = 0;
		var root = 'breastfeeding';
        var id = '#'+root+'-';
		var clas = '.field-'+root+'-';
        var arr = ['determine_enough_milk','successfully_cope','without_formula','lached_properly','manage_situation',
		'manage_crying','wanting_breastfeed','comfortable_family_members','satisfied_experience','deal_time_consuming',
		'finish_1breast','continue_feeding','keepup_baby_demands','tell_finished'];
		
		for (var i in arr) {
            var val = $(clas + arr[i] + ' input[type=radio]:checked').val();
			if (parseInt(val)>=0) {
                totalValue += parseInt(val);
            }
        }
		
        $(id+'score').val(totalValue);
		autoSaveObj = formObj;
    }
</script>
