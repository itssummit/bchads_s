<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Breastfeeding */

$this->title = 'Create Breastfeeding';
$this->params['breadcrumbs'][] = ['label' => 'Breastfeedings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="breastfeeding-create">

    <h1 align="center">ಎದೆ ಹಾಲುಣಿಸುವ ಸ್ವಸಾಮರ್ಥ್ಯದ ಪ್ರಶ್ನಾವಳಿ </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
