<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Parental Bonding Firsts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parental-bonding-first-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Parental Bonding First', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'talks_friendly_mother',
            'dint_help_needed_mother',
            'allowed_loved_work_mother',
            'emotional_mother',
            // 'understands_pain_mother',
            // 'loving_mother',
            // 'allowed_own_decisions_mother',
            // 'hates_growing_mother',
            // 'stops_all_work_mother',
            // 'dint_allow_alone_mother',
            // 'happy_to_talk_mother',
            // 'laughs_mother',
            // 'treat_baby_mother',
            // 'didnt_understand_needs_mother',
            // 'decides_own_mother',
            // 'dont_need_feel_mother',
            // 'helps_in_sad_mother',
            // 'didnt_talk_lot_mother',
            // 'feels_dependent_mother',
            // 'didnt_lookafter_mother',
            // 'give_freedom_mother',
            // 'allow_outside_mother',
            // 'saves_more_mother',
            // 'praises_mother',
            // 'allow_own_style_mother',
            // 'talks_friendly_father',
            // 'dint_help_needed_father',
            // 'allowed_loved_work_father',
            // 'emotional_father',
            // 'understands_pain_father',
            // 'loving_father',
            // 'allowed_own_decisions_father',
            // 'hates_growing_father',
            // 'stops_all_work_father',
            // 'dint_allow_alone_father',
            // 'happy_to_talk_father',
            // 'laughs_father',
            // 'treat_baby_father',
            // 'didnt_understand_needs_father',
            // 'decides_own_father',
            // 'dont_need_feel_father',
            // 'helps_in_sad_father',
            // 'didnt_talk_lot_father',
            // 'feels_dependent_father',
            // 'didnt_lookafter_father',
            // 'give_freedom_father',
            // 'allow_outside_father',
            // 'saves_more_father',
            // 'praises_father',
            // 'allow_own_style_father',
            // 'respondent_id',
            // 'assessment_id',
            // 'created_dtm',
            // 'last_updated_dtm',
            // 'updated_by',
            // 'score',
            // 'scale_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
