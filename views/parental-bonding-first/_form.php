﻿<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$keyPair = [-15 => '-15: Child Not Co-Operative For Assessment',
          -14 => '-14: Not Available For Assessment',
          -11 => '-11: Inadequate Information',
          -10 => '-10: Not Applicable',
          -9 => '-9: Missing',
          -8 => '-8: Refused',
          -7 => '-7: Partner Accompanied Missing',
          -6 => '-6: Family Accompanied Missing',
          -5 => '-5: Missing:Not Asked',
          -3 => '-3: No Such Relationship/No Partner'];
/* @var $this yii\web\View */
/* @var $model app\models\ParentalBondingFirst */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile('@web/js/scales.js',['depends' => [\yii\web\JqueryAsset::className()]]);
?>



    <?php $form = ActiveForm::begin(['id' => 'formPbif']); ?>
    
	<h4>Is This Scale Applicable ?</h4>
			<div class="col-md-3"><?= $form->field($model, 'not_applicable')->dropDownList(['' =>'','YES' => 'Not Applicable', 'NO' => 'Applicable'])->label(false); ?></td></tr>
		</div>
		<br/><br/>
	<div id="withBoxShadow" class="parental-bonding-first-form">
    
        <caption><b>ಈ ಪ್ರಶ್ನಾವಳಿಯು ಪೋಷಕರ ವಿವಿಧ ರೀತಿಯ ನಡವಳಿಕೆ ಹಾಗೂ ವರ್ತನೆಯ ಪಟ್ಟಿ . ನೀವು 16 ವಯಸ್ಸಿನವರಿದ್ದಾಗ ನಿಮಗೆ ನೆನೆಪಿದ್ದಂತೆ ನಿಮ್ಮ ತಾಯಿಯು ನಿಮ್ಮೊಂದಿಗೆ ಹೇಗಿದ್ದಾರೆಂದು ತಿಳಿಸಲು ಪ್ರತಿ ಪ್ರಶ್ನೆಯ ಪಕ್ಕದಲ್ಲಿರುವ ಅತೀ ಹೆಚ್ಚು ಸೂಕ್ತವಾದ ಆಯ್ಕೆಗಳಿಗೆ ಗುರುತು ಹಾಕಿ </b></caption>
        <table border="1" class="responsive-scales">
            <thead>
                <tr>
                    <th>ಕ್ರ . ಸಂ </th>
                    <th>ಹೇಳಿಕೆಗಳು </th>
                    <th>ತುಂಬಾ  ಆಥರ </th>
                    <th>ಸ್ವಲ್ಪ ಮಟ್ಟಿಗೆ ಆಥರ </th>
                    <th>ಸ್ವಲ್ಪ ಮಟ್ಟಿಗೆ ಆಥರವಿಲ್ಲ</th>
                    <th>ತುಂಬಾ ಆಥಿರವಿಲ್ಲ </th>
                    
                    <th>Other Answers</th>
                </tr> 
            </thead>
            
            <tbody>
                    <tr>
                        <td>1</td>
                        <th>ನನ್ನ ಜೊತೆ ಮಮತೆಯಿಂದ ಹಾಗೂ ಸ್ನೇಹಮಯವಾಗಿ ಮಾತನಾಡಿದರು.</th>
                        <?php for($i = 1;$i < 5;$i++):?>
                        <td><?= $form->field($model, 'talks_friendly_mother')->radio(['label' => $i, 'value' => $i])?></td>
                        <?php endfor;?>
                        <td>
                        <?= $form->field($model, 'talks_friendly_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
                        </td>
                    </tr>
                    
                    
                    <tr>
                        <td>2</td>
                        <th>ನನಗೆ ಬೇಕಾದಷ್ಟುಅಗತ್ಯವಿದ್ದಷ್ಟು ಸಹಾಯ ಮಾಡಲಿಲ್ಲ.</th>
                        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'dint_help_needed_mother')->radio(['label' => $i, 'value' => $i])?></td>
                        <?php endfor;?>
                        <td>
                        <?= $form->field($model, 'dint_help_needed_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
                        </td>
                    </tr>
            
            
                    <tr>
                        <td>3</td>
                        <th>ನನಗೆ ಇಷ್ಟವಾಗಿರುವಂತಹ ಕೆಲಸಗಳನ್ನು ಮಾಡಲು ಬಿಟ್ಟಿದ್ದರು. </th>
                        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'allowed_loved_work_mother')->radio(['label' => $i, 'value' => $i])?></td>
                        <?php endfor;?>
                        <td>
                    <?= $form->field($model, 'allowed_loved_work_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
                        </td>
                    </tr>
                    
                    
                    
                    <tr>
        <td>4</td>
        <th>ನನ್ನೊಂದಿಗೆ ಭಾವನಾತ್ಮಕವಾಗಿ ಸ್ಥಬ್ಧವಾಗಿದ್ದರು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'emotional_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'emotional_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>5</td>
        <th>ನನ್ನ ನೋವು ಮತ್ತು ಚಿಂತೆಗಳನ್ನು ಅರ್ಥ ಮಾಡಿಕೊಂಡವರಂತೆ ಕಾಣುತ್ತಿದ್ದರು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'understands_pain_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'understands_pain_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    
    <tr>
        <td>6</td>
        <th>ನನ್ನೊಂದಿಗೆ  ಪ್ರೀತಿಯಿಂದಿದ್ದರು  </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'loving_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'loving_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    
    <tr>
        <td>7</td>
        <th> ನಾನು ಸ್ವಂತ ನಿರ್ಧಾರಗಳನ್ನು ತೆಗೆದುಕೊಳ್ಳುವುದನ್ನು ಇಷ್ಟಪಡುತ್ತಿದ್ದರು.</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'allowed_own_decisions_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'allowed_own_decisions_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>8</td>
        <th> ನಾನು ಬೆಳೆಯುವುದು ಇಷ್ಟವಾಗುತ್ತಿರಲಿಲ್ಲ. </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'hates_growing_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'hates_growing_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>9</td>
        <th>ನಾನು ಮಾಡುತ್ತಿದ್ದ ಪ್ರತಿಯೊಂದನ್ನು ನಿಯಂತ್ರಿಸುತ್ತಿದ್ದರು. </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'stops_all_work_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'stops_all_work_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>10</td>
        <th>ನನ್ನ ಏಕಾಂತತೆಯನ್ನು ಆಕ್ರಮಿಸುತ್ತಿದ್ದರು.</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'dint_allow_alone_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'dint_allow_alone_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>11</td>
        <th>ನನ್ನೊಂದಿಗೆ ವಿಷಯಗಳನ್ನು ಮಾತನಾಡುತ್ತ ಆನಂದಿಸುತ್ತಿದ್ದರು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'happy_to_talk_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'happy_to_talk_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>12</td>
        <th>ಆಗಾಗ ನನ್ನೊಂದಿಗೆ ನಗುತ್ತಿದ್ದರು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'laughs_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'laughs_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>13</td>
        <th>ನನ್ನನ್ನ ಮಗುವಿನಂತೆ ಆರೈಕೆಮಾಡುತ್ತಿದ್ದರು. </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'treat_baby_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'treat_baby_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>14</td>
        <th>ನನ್ನ ಆಗತ್ಯತೆ ಅಥವಾ ಅವಶ್ಯಕತೆಗಳ ಬಗ್ಗೆ ಅರ್ಥವಾದಂತೆ ಕಾಣುತ್ತಿರಲಿಲ್ಲ</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'didnt_understand_needs_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'didnt_understand_needs_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>15</td>
        <th>ನನಗೆ ಬೇಕಾದ್ದನ್ನು ನಾನೇ ನಿರ್ಧರಿಸುತ್ತಿದ್ದೆ.</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'decides_own_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'decides_own_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>16</td>
        <th>ನಾನು ಬೇಕಾಗಿರಲಿಲ್ಲ ಎಂಬ ಭಾವನೆ ಬರುವಂತೆ ಮಾಡುತ್ತಿದ್ದರು</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'dont_need_feel_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'dont_need_feel_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>17</td>
        <th>ಬೇಸರವಾಗಿದ್ದಾಗ ನಾನು ಸಮಾಧಾನವಾಗುವಂತೆ ಮಾಡುತ್ತಿದ್ದರು.</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'helps_in_sad_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'helps_in_sad_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>18</td>
        <th>ನನ್ನೊಂದಿಗೆ ಅತೀ ಹೆಚ್ಚಾಗಿ ಮಾತಾನಾಡುತ್ತಿರಲಿಲ್ಲ.</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'didnt_talk_lot_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'didnt_talk_lot_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>19</td>
        <th>ನಾನು ಅವನ/ಳನ್ನು ಅವಲಂಬಿಸಿದ್ದೇನೆಎಂಬ ಭಾವನೆ ಬರುವಂತೆ ಪ್ರಯತ್ನಿಸುತ್ತಿದ್ದರು (ಉದಾ .ತಲೆ ಬಾಚುವುದು ,ಎಣ್ಣೆ ಹಾಕುವುದು ,ಅಂಗಡಿಗೆ ಹೋಗುವುದು ,ರಸ್ತೆ ದಾಟುವುದು )</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'feels_dependent_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'feels_dependent_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>20</td>
        <th>ಅವನು/ಳು ನನ್ನೊಂದಿಗೆ ಇಲ್ಲದಿದ್ದರೆ ನನ್ನನ್ನು ನಾನು   ನೋಡಿಕೊಳ್ಳಲು ಸಾಧ್ಯವಿಲ್ಲ ಎಂದೆನಿಸುತ್ತಿತ್ತು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'didnt_lookafter_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'didnt_lookafter_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>21</td>
        <th>ನನಗೆ ಬೇಕಾದಷ್ಟು ಸ್ವಾತಂತ್ರ್ಯ  ಕೊಟ್ಟಿದ್ದರು</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'give_freedom_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'give_freedom_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>22</td>
        <th>ನನಗೆ ಬೇಕಾದಾಗ ಹೊರಗೆಹೋಗಲು ಹಲವು ಬಾರಿ ಬಿಡುತ್ತಿದ್ದರು</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'allow_outside_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'allow_outside_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>23</td>
        <th>ನನ್ನನ್ನ ಅತೀಹೆಚ್ಚಾಗಿ/ಅಗತ್ಯಕ್ಕಿಂತ ಹೆಚ್ಚಾಗಿ ರಕ್ಷಿಸುತ್ತಿದ್ದರು</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'saves_more_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'saves_more_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>24</td>
        <th>ನನ್ನನ್ನು ಹೊಗಳುತ್ತಿರಲಿಲ್ಲ</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'praises_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'praises_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    
    <tr>
        <td>25</td>
        <th>ನನಗೆ ಇಷ್ಟವಾಗುವ ಹಾಗೆ ಅಲಂಕಾರ ಮಾಡಲು ಬಿಡುತ್ತಿದ್ದರು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'allow_own_style_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'allow_own_style_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    </tbody>
    </table>
    
    
    
    <caption><b>ಈ ಪ್ರಶ್ನಾವಳಿಯು ಪೋಷಕರ ವಿವಿಧ ರೀತಿಯ ನಡವಳಿಕೆ ಹಾಗೂ ವರ್ತನೆಯ ಪಟ್ಟಿ . ನೀವು 16 ವಯಸ್ಸಿನವರಿದ್ದಾಗ ನಿಮಗೆ ನೆನೆಪಿದ್ದಂತೆ ನಿಮ್ಮ ತಂದೆಯು  ನಿಮ್ಮೊಂದಿಗೆ ಹೇಗಿದ್ದಾರೆಂದು ತಿಳಿಸಲು ಪ್ರತಿ ಪ್ರಶ್ನೆಯ ಪಕ್ಕದಲ್ಲಿರುವ ಅತೀ ಹೆಚ್ಚು ಸೂಕ್ತವಾದ ಆಯ್ಕೆಗಳಿಗೆ ಗುರುತು ಹಾಕಿ </b></caption>
        <table border="1" class="responsive-scales">
            <thead>
                <tr>
                    <th>ಕ್ರ . ಸಂ </th>
                    <th>ಹೇಳಿಕೆಗಳು </th>
                    <th>ತುಂಬಾ  ಆಥರ </th>
                    <th>ಸ್ವಲ್ಪ ಮಟ್ಟಿಗೆ ಆಥರ </th>
                    <th>ಸ್ವಲ್ಪ ಮಟ್ಟಿಗೆ ಆಥರವಿಲ್ಲ</th>
                    <th>ತುಂಬಾ ಆಥಿರವಿಲ್ಲ </th>
                    
                    <th>Other Answers</th>
                </tr> 
            </thead>
            
            <tbody>
                    <tr>
                        <td>1</td>
                        <th>ನನ್ನ ಜೊತೆ ಮಮತೆಯಿಂದ ಹಾಗೂ ಸ್ನೇಹಮಯವಾಗಿ ಮಾತನಾಡಿದರು.</th>
                        <?php for($i = 1;$i < 5;$i++):?>
                        <td><?= $form->field($model, 'talks_friendly_father')->radio(['label' => $i, 'value' => $i])?></td>
                        <?php endfor;?>
                        <td>
                        <?= $form->field($model, 'talks_friendly_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
                        </td>
                    </tr>
                    
                    
                    <tr>
                        <td>2</td>
                        <th>ನನಗೆ ಬೇಕಾದಷ್ಟ/ಅಗತ್ಯವಿದ್ದಷ್ಟು ಸಹಾಯ ಮಾಡಲಿಲ್ಲ.</th>
                        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'dint_help_needed_father')->radio(['label' => $i, 'value' => $i])?></td>
                        <?php endfor;?>
                        <td>
                        <?= $form->field($model, 'dint_help_needed_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
                        </td>
                    </tr>
            
            
                    <tr>
                        <td>3</td>
                        <th>ನನಗೆ ಇಷ್ಟವಾಗಿರುವಂತಹ ಕೆಲಸಗಳನ್ನು ಮಾಡಲು ಬಿಟ್ಟಿದ್ದರು. </th>
                        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'allowed_loved_work_father')->radio(['label' => $i, 'value' => $i])?></td>
                        <?php endfor;?>
                        <td>
                        <?= $form->field($model, 'allowed_loved_work_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
                        </td>
                    </tr>
                    
                    
                    
                    <tr>
        <td>4</td>
        <th>ನನ್ನೊಂದಿಗೆ ಭಾವನಾತ್ಮಕವಾಗಿ ಸ್ಥಬ್ಧವಾಗಿದ್ದರು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'emotional_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'emotional_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>5</td>
        <th>ನನ್ನ ನೋವು ಮತ್ತು ಚಿಂತೆಗಳನ್ನು ಅರ್ಥ ಮಾಡಿಕೊಂಡವರಂತೆ ಕಾಣುತ್ತಿದ್ದರು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'understands_pain_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'understands_pain_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    
    <tr>
        <td>6</td>
        <th>ನನ್ನೊಂದಿಗೆ  ಪ್ರೀತಿಯಿಂದಿದ್ದರು  </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'loving_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'loving_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    
    <tr>
        <td>7</td>
        <th> ನಾನು ಸ್ವಂತ ನಿರ್ಧಾರಗಳನ್ನು ತೆಗೆದುಕೊಳ್ಳುವುದನ್ನು ಇಷ್ಟಪಡುತ್ತಿದ್ದರು.</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'allowed_own_decisions_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'allowed_own_decisions_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>8</td>
        <th> ನಾನು ಬೆಳೆಯುವುದು ಇಷ್ಟವಾಗುತ್ತಿರಲಿಲ್ಲ. </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'hates_growing_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'hates_growing_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>9</td>
        <th>ನಾನು ಮಾಡುತ್ತಿದ್ದ ಪ್ರತಿಯೊಂದನ್ನು ನಿಯಂತ್ರಿಸುತ್ತಿದ್ದರು. </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'stops_all_work_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'stops_all_work_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>10</td>
        <th>ನನ್ನ ಏಕಾಂತತೆಯನ್ನು ಆಕ್ರಮಿಸುತ್ತಿದ್ದರು.</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'dint_allow_alone_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'dint_allow_alone_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>11</td>
        <th>ನನ್ನೊಂದಿಗೆ ವಿಷಯಗಳನ್ನು ಮಾತನಾಡುತ್ತ ಆನಂದಿಸುತ್ತಿದ್ದರು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'happy_to_talk_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'happy_to_talk_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>12</td>
        <th>ಆಗಾಗ ನನ್ನೊಂದಿಗೆ ನಗುತ್ತಿದ್ದರು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'laughs_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'laughs_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>13</td>
        <th>ನನ್ನನ್ನ ಮಗುವಿನಂತೆ ಆರೈಕೆಮಾಡುತ್ತಿದ್ದರು. </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'treat_baby_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'treat_baby_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>14</td>
        <th>ನನ್ನ ಆಗತ್ಯತೆ ಅಥವಾ ಅವಶ್ಯಕತೆಗಳ ಬಗ್ಗೆ ಅರ್ಥವಾದಂತೆ ಕಾಣುತ್ತಿರಲಿಲ್ಲ</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'didnt_understand_needs_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'didnt_understand_needs_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>15</td>
        <th>ನನಗೆ ಬೇಕಾದ್ದನ್ನು ನಾನೇ ನಿರ್ಧರಿಸುತ್ತಿದ್ದೆ.</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'decides_own_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'decides_own_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>16</td>
        <th>ನಾನು ಬೇಕಾಗಿರಲಿಲ್ಲ ಎಂಬ ಭಾವನೆ ಬರುವಂತೆ ಮಾಡುತ್ತಿದ್ದರು</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'dont_need_feel_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'dont_need_feel_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>17</td>
        <th>ಬೇಸರವಾಗಿದ್ದಾಗ ನಾನು ಸಮಾಧಾನವಾಗುವಂತೆ ಮಾಡುತ್ತಿದ್ದರು.</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'helps_in_sad_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'helps_in_sad_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>18</td>
        <th>ನನ್ನೊಂದಿಗೆ ಅತೀ ಹೆಚ್ಚಾಗಿ ಮಾತಾನಾಡುತ್ತಿರಲಿಲ್ಲ.</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'didnt_talk_lot_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'didnt_talk_lot_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>19</td>
        <th>ನಾನು ಅವನ/ಳನ್ನು ಅವಲಂಬಿಸಿದ್ದೇನೆಎಂಬ ಭಾವನೆ ಬರುವಂತೆ ಪ್ರಯತ್ನಿಸುತ್ತಿದ್ದರು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'feels_dependent_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'feels_dependent_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>20</td>
        <th>ಅವನು/ಳು ನನ್ನೊಂದಿಗೆ ಇಲ್ಲದಿದ್ದರೆ ನನ್ನನ್ನು ನಾನು   ನೋಡಿಕೊಳ್ಳಲು ಸಾಧ್ಯವಿಲ್ಲ ಎಂದೆನಿಸುತ್ತಿತ್ತು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'didnt_lookafter_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'didnt_lookafter_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>21</td>
        <th>ನನಗೆ ಬೇಕಾದಷ್ಟು ಸ್ವಾತಂತ್ರ್ಯ  ಕೊಟ್ಟಿದ್ದರು</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'give_freedom_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'give_freedom_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>22</td>
        <th>ನನಗೆ ಬೇಕಾದಾಗ ಹೊರಗೆಹೋಗಲು ಹಲವು ಬಾರಿ ಬಿಡುತ್ತಿದ್ದರು</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'allow_outside_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'allow_outside_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>23</td>
        <th>ನನ್ನನ್ನ ಅತೀಹೆಚ್ಚಾಗಿ/ಅಗತ್ಯಕ್ಕಿಂತ ಹೆಚ್ಚಾಗಿ ರಕ್ಷಿಸುತ್ತಿದ್ದರು</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'saves_more_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'saves_more_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>24</td>
        <th>ನನ್ನನ್ನು ಹೊಗಳುತ್ತಿರಲಿಲ್ಲ</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'praises_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'praises_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    
    <tr>
        <td>25</td>
        <th>ನನಗೆ ಇಷ್ಟವಾಗುವ ಹಾಗೆ ಅಲಂಕಾರ ಮಾಡಲು ಬಿಡುತ್ತಿದ್ದರು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'allow_own_style_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'allow_own_style_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>

    
        
    </tbody>
    </table>
    
    <div class="hidden">
    <?= $form->field($model, 'score')->input('number',['readonly'=>true])->label(false);?>
    </div>

    

    <?php ActiveForm::end(); ?>

</div>
<?php $this->registerJs('
    $("#"+formObj.formName).one("change",function()
    {
        autoSaveObj = formObj;
    });
',\yii\web\View::POS_READY)?>


<?php if($model->not_applicable == 'YES'){
	$this->registerJs('
	setNotApplicableOnLoad("#withBoxShadow","#parentalbondingfirst-not_applicable");
	', \yii\web\View::POS_READY);

}
	$this->registerJs('

	$("#parentalbondingfirst-not_applicable").change(function()
		{	
			var value = $(this).val();
			if(value=="YES")
			{	
				setNotApplicableYes("#withBoxShadow","#parentalbondingfirst-not_applicable");
			}
			else
			{
				setNotApplicableNo("#withBoxShadow","#parentalbondingfirst-not_applicable");
			}
		});
			
', \yii\web\View::POS_READY);
?>

<script type="text/javascript">
    var formObj = {url:"parental-bonding-first/ajax",formName : "formPbif"};
        var calc = function () {
        var totalValue = 0;
        var root= 'parentalbondingfirst';
        var id = '#'+root+'-';
        var clas = '.field-'+root+'-';
        var arr = ['talks_friendly_mother', 'dint_help_needed_mother', 'allowed_loved_work_mother', 'emotional_mother', 'understands_pain_mother',
        'loving_mother', 'allowed_own_decisions_mother', 'hates_growing_mother', 'stops_all_work_mother', 'dint_allow_alone_mother', 
        'happy_to_talk_mother', 'laughs_mother', 'treat_baby_mother', 'didnt_understand_needs_mother', 'decides_own_mother', 
        'dont_need_feel_mother', 'helps_in_sad_mother', 'didnt_talk_lot_mother', 'feels_dependent_mother', 'didnt_lookafter_mother',
        'give_freedom_mother', 'allow_outside_mother', 'saves_more_mother', 'praises_mother', 'allow_own_style_mother', 
        'talks_friendly_father', 'dint_help_needed_father', 'allowed_loved_work_father', 'emotional_father', 'understands_pain_father',
        'loving_father', 'allowed_own_decisions_father', 'hates_growing_father', 'stops_all_work_father', 'dint_allow_alone_father', 
        'happy_to_talk_father', 'laughs_father', 'treat_baby_father', 'didnt_understand_needs_father', 'decides_own_father', 
        'dont_need_feel_father', 'helps_in_sad_father', 'didnt_talk_lot_father', 'feels_dependent_father', 'didnt_lookafter_father', 
        'give_freedom_father', 'allow_outside_father', 'saves_more_father', 'praises_father','allow_own_style_father'];
        
        for (var i in arr) {
            var val = $(clas + arr[i] + ' input[type=radio]:checked').val();
            if (parseInt(val)) {
                totalValue += parseInt(val);
            }
        }
        

        $(id+'score').val(totalValue);
        autoSaveObj = formObj;
        
    }

</script>

<style>
.na{
	height:90px !important;
}
</style>