<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BabyLikesDislikes */

/*$this->title = 'Create Baby Likes Dislikes';
$this->params['breadcrumbs'][] = ['label' => 'Baby Likes Dislikes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/
?>
<div class="baby-likes-dislikes-create">

    <h1 align="center">ಮಗುವಿನ ವರ್ತನೆಯ ಪ್ರಶ್ನಾವಳಿ </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
