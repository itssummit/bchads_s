<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$keyPair = [-15 => '-15: Child Not Co-Operative For Assessment',
		  -14 => '-14: Not Available For Assessment',
		  -11 => '-11: Inadequate Information',
		  -10 => '-10: Not Applicable',
		  -9 => '-9: Missing',
		  -8 => '-8: Refused',
		  -7 => '-7: Partner Accompanied Missing',
		  -6 => '-6: Family Accompanied Missing',
		  -5 => '-5: Missing:Not Asked',
		  -3 => '-3: No Such Relationship/No Partner'];
/* @var $this yii\web\View */
/* @var $model app\models\BabyLikesDislikes */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile('@web/js/scales.js',['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<p>
ಮಗುವಿನ ವರ್ತೆನೆಯ  ಪ್ರಶ್ನಾವಳಿ 
ಈಗ ನಾನು ನಿಮ್ಮ ಮಗುವಿನ ಇಷ್ಟ ಮತ್ತು ಇಷ್ಟವಾಗದಂತಹಗಳನ್ನು ಕಲಿಯಲು ಇಚ್ಛಿಸುತ್ತೇನೆ . ಎಲ್ಲಾ ಮಕ್ಕಳು ವಿಭಿನ್ನರಾಗಿರುತ್ತಾರೆ 
ಆದ್ದರಿಂದ ನಿಮ್ಮದೇ ಆದ ಮಗುವಿನ ಬಗ್ಗೆ ಇಲ್ಲಿಯವರೆಗಿನ ನಿಮ್ಮ ಅನುಭವಗಳನ್ನು ಇನ್ನು ಸ್ವಲ್ಪ ಹೆಚ್ಚಾಗಿ ನಿಮ್ಮಿಂದ ನಾವು ಕಲಿಯಲು ಇಚ್ಛಿಸುತ್ತೇವೆ .
 ಸೂಚನೆಗಳು :
ಈ ಕೆಳಗೆ ಪ್ರತಿಯೊಂದು ಮಕ್ಕಳ ವರ್ತನೆಯನ್ನು  ನೀಡಲಾಗಿದ್ದು ,ಅವುಗಳನ್ನು ಓದುವ ಮೂಲಕ ದಯವಿಟ್ಟು ನಿಮ್ಮ ಮಗುವು ಕಳೆದ ವಾರದಲ್ಲಿ (ಕಳೆದ ೭ ದಿನಗಳಲ್ಲಿ ) ಆಗಾಗ್ಗೆ ಎಷ್ಟು ಬಾರಿ ಮಾಡಿದೆ ಎಂದು ಹೇಳಿಕೆಗಳ ಬಲ ಭಾಗದ ಸಾಲಿನಲ್ಲಿರುವ ಒಂದು ಸಂಖ್ಯೆಯನ್ನು ಸೂಚಿಸುವ ಮೂಲಕ ನಮಗೆ ತಿಳಿಸಿ . 
ಕಳೆದ ಒಂದು ವಾರದಲ್ಲಿ ನಿಮ್ಮ ಮಗುವನ್ನು ನೀವು ಈ ಪರಿಸ್ಥಿತಿಯಲ್ಲಿ    ನೋಡದಿದ್ದರೆ "ಇದು ಅನ್ವಯಿಸುವುದಿಲ್ಲ "(X ) ಎಂಬ ಸಾಲಿಗೆ ಗುರುತು ಹಾಕಿ . ಉದಾಹರಣೆಗೆ , ಮಗುವು ಊಟಕ್ಕಾಗಿ ಅಥವಾ ಹಾಲಿಗಾಗಿ ಕಾಯುತ್ತಿತ್ತು ಎಂಬ ಸನ್ನಿವೇಶವನ್ನು ನಿಮ್ಮ ಮಗುವು <b>ಕಳೆದ ಒಂದು ವಾರದಲ್ಲಿ </b>ಆ ರೀತಿ ಕಾಯುತ್ತಿರಲಿಲ್ಲ ಅಂದರೆ ,ಸಾಲಿನ (x ) ಗೆ ಗುರುತು ಹಾಕಿ . "ಇದು ಅನ್ವಯವಾಗುವುದಿಲ್ಲ "  ಎಂಬುದಕ್ಕಿಂತ "ಇಲ್ಲವೇ ಇಲ್ಲ" (೧) ಎಂಬ ಆಯ್ಕೆಯು ಬೇರೆಯಾಗಿದೆ . 
ಮಗುವು  ಈ ರೀತಿಯ ಸನ್ನಿವೇಶದಲ್ಲಿ ಇದ್ದದ್ದನ್ನು ನೀವು ನೋಡಿದ್ದೀರಿ ಆದರೆ ಕಳೆದ ವಾರದಲ್ಲಿ ನಿಮ್ಮ ಮಗುವು ಈ ವರ್ತನೆಯನ್ನು 
ಮಾಡದಿದ್ದರೆ ಇಲ್ಲವೇ ಇಲ್ಲ ಎಂಬ ಆಯ್ಕೆಗೆ ಗುರುತು ಹಾಕಿ . ಉದಾಹರಣೆಗೆ ,ಮಗುವು ಊಟಕ್ಕಾಗಿ ಅಥವಾ ಹಾಲಿಗಾಗಿ ಕೇವಲ ಒಮ್ಮೆ ಮಾತ್ರ 
ಕಾದಿದ್ದು ಆದರೆ ಕಾಯುತ್ತಿರುವಾಗ ಜೋರಾಗಿ ಅತ್ತಿಲ್ಲದಿದ್ದರೆ  ಸಾಲಿನ (೧) ಆಯ್ಕೆಯನ್ನು ಗುರುತು  ಹಾಕಿ .
</p>
<div class="baby-likes-dislikes-form" >

    <?php $form = ActiveForm::begin(['id' => 'formBabylikesdislikes']); ?>

    <div id="idbldscale" >
		<div id="withBoxShadow" style="overflow-x: scroll;">
		<table border="1" class="responsive-scales" >
				<thead>
					<tr>
						<th></th>
						<th>ಇಲ್ಲವೇ ಇಲ್ಲ</th>
						<th>ಅತಿ ವಿರಳವಾಗಿ</th>
						<th>ಅರ್ಧ ಸಮಯಕ್ಕಿಂತ ಕಡಿಮೆ</th>
						<th>ಅರ್ಧ ಸಮಯದಷ್ಟು</th>
						<th>ಅರ್ಧ ಸಮಯಕ್ಕಿಂತ ಹೆಚ್ಚು</th>
						<th>ಹೆಚ್ಚಾಗಿ ಯಾವಾಗಲು</th>
						<th>ಯಾವಾಗಲು</th>
						<th>ಇದು ಅನ್ವಯವಾಗುವುದಿಲ್ಲ</th>
						<th>Other Answers</th>
					</tr> 
				</thead>
			<tbody>
                
					<tr>
						<td><b>ರಾತ್ರಿ ಮಲಗುವ ಮೊದಲು ನಿಮ್ಮ ಮಗುವು ಹೀಗೆ ಎಷ್ಟು ಬಾರಿ ಮಾಡಿತ್ತು</b><br>
						1. ಗಲಾಟೆ  ಮಾಡಿಲ್ಲ ಆಥವಾ ಅತ್ತಿಲ್ಲ</td>
						<?php for($i = 1;$i < 8;$i++):?>
						<td><?= $form->field($model, 'fussing_crying')->radio(['label' => $i, 'value' => $i])?>
						</td>
						<?php endfor;?>
						<td><?= $form->field($model, 'fussing_crying')->radio(['label' => 'X', 'value' => 0])?></td>
						<td>
						<?= $form->field($model, 'fussing_crying')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
						</td>
					</tr>
				
					<tr>
						<td><b>ನಿದ್ರೆ ಮಾಡುವಾಗ ಎಷ್ಟು ಬಾರಿ ಹೀಗೆ ಮಾಡಿತು?  </b><br>
						  2. ಹಾಸಿಗೆ ಮೇಲೆ ಹೊರಾಳುಡುತ್ತಿತ್ತು?</td>
						<?php for($i = 1;$i < 8;$i++):?>			
						<td><?= $form->field($model, 'toss_cot')->radio(['label' => $i, 'value' => $i])?>
						</td>
						<?php endfor;?>
						<td><?= $form->field($model, 'toss_cot')->radio(['label' => 'X', 'value' => 0])?>
						</td>
						<td>
						<?= $form->field($model, 'toss_cot')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
						</td>
					</tr>
				
                 <tr>
                    <td><b>ನಿದ್ರೆ ಮಾಡಿದ ನಂತರ   ಎಷ್ಟು ಬಾರಿ ಹೀಗೆ ಮಾಡಿತ್ತು</b><br>
					3.ತಕ್ಷಣವೇ ಅತ್ತಿದ್ದೂ ಆಥವಾ ಕಿರಿಕಿರಿ ಮಾಡಿದ್ದು ? </td>
					<?php for($i = 1;$i < 8;$i++):?>
                    
                    <td> <?= $form->field($model, 'fuss_immediately')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'fuss_immediately')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'fuss_immediately')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
					</td>
                </tr>
				
                <tr>
                    <td><b>ನಿದ್ರೆ ಮಾಡಿದ ನಂತರ   ಎಷ್ಟು ಬಾರಿ ಹೀಗೆ ಮಾಡಿತ್ತು</b> </br>4.ಹಾಸಿಗೆಯ ಮೇಲೆ ಸುಮ್ಮನೆ ಆಟವಾಡುತ್ತಿರುವುದು?</td>
					<?php for($i = 1;$i < 8;$i++):?>
                    
                    <td> <?= $form->field($model, 'play_quitely')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'play_quitely')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'play_quitely')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

					</td>
                </tr>
				
                <tr>
                    <td><b>ನಿದ್ರೆ ಮಾಡಿದ ನಂತರ   ಎಷ್ಟು ಬಾರಿ ಹೀಗೆ ಮಾಡಿತ್ತು</b> </br>5.ಕೆಲವು ನಿಮಿಷದಲ್ಲಿ ಯಾರಾದರೂ ಬರಲಿಲ್ಲ ಎಂದರೆ ಅತ್ತಿರುವುದು?</td>
					<?php for($i = 1;$i < 8;$i++):?>
                    
                    <td> <?= $form->field($model, 'cry_someone_didnt_come')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'cry_someone_didnt_come')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'cry_someone_didnt_come')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

					</td>
                </tr>
				
                <tr>
                    <td><b>ನಿಮ್ಮ ಮಗುವು ಹೀಗೆ ಎಷ್ಟು ಬಾರಿ ಮಾಡಿತ್ತು </b><br>
					6.ಹಾಸಿಗೆಯ ಮೇಲೆ  ಬಿಟ್ಟು ಹೋದಾಗ ಕೋಪಗೊಂಡಿರುವುದು?</td>
					
                    <?php for($i = 1;$i < 8;$i++):?>
                    <td> <?= $form->field($model, 'angry')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'angry')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'angry')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

					</td>
                </tr>
				
                <tr>
                    <td><b>ನಿಮ್ಮ ಮಗುವು ಹೀಗೆ ಎಷ್ಟು ಬಾರಿ ಮಾಡಿತ್ತು </b><br>
					7.ನಿಮ್ಮ ಮಗುವನ್ನು ಹಾಸಿಗೆಯ ಮೇಲೆ ಬಿಟ್ಟಾಗ ತೃಪ್ತನಾದನೇ </td>
					<?php for($i = 1;$i < 8;$i++):?>
                   
                    <td> <?= $form->field($model, 'contented_corner')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'contented_corner')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'contented_corner')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

					</td>
                </tr>
				
                <tr>
                    <td><b>ನಿಮ್ಮ ಮಗುವು ಹೀಗೆ ಎಷ್ಟು ಬಾರಿ ಮಾಡಿತ್ತು </b><br>
					8.ಯಾವಾಗಲೂ ನಿದ್ರೆಗೆ ಹೋಗುವ ಸಮಯಕ್ಕೆ ಮೊದಲು ಅತ್ತಿದ್ದು  ಅಥವಾ ಗಲಾಟೆಮಾಡಿದ್ದು?</td>
					<?php for($i = 1;$i < 8;$i++):?>
                   
                    <td> <?= $form->field($model, 'cry_before_nap')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'cry_before_nap')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'cry_before_nap')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

					</td>
                </tr>
				
                <tr>
                    <td><b>ತಿನ್ನಿಸುವಾಗ ನಿಮ್ಮ ಮಗುವು ಹೀಗೆ ಎಷ್ಟು ಬಾರಿ ಹೀಗೆ  ಮಾಡಿತ್ತು </b><br>
					9.ಸುಮ್ಮನೆ ಮಲಗಿರುವುದು ಅಥವಾ ಕುಳಿತಿರುವುದು?</td>
					
                    <?php for($i = 1;$i < 8;$i++):?>
                    <td> <?= $form->field($model, 'lie_sit_quietly')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'lie_sit_quietly')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'lie_sit_quietly')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

					</td>
                </tr>
				
                <tr>
                    <td><b>ತಿನ್ನಿಸುವಾಗ ನಿಮ್ಮ ಮಗುವು ಹೀಗೆ ಎಷ್ಟು ಬಾರಿ ಹೀಗೆ  ಮಾಡಿತ್ತು </b><br>
					10.ಒದೆಯುವುದು ಅಥವಾ ತೆವಳುವುದು?</td>
					
                    <?php for($i = 1;$i < 8;$i++):?>
                    <td> <?= $form->field($model, 'squirm')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'squirm')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'squirm')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

					</td>
                </tr>
				
                <tr>
                    <td><b>ಮುಖ ತೊಳಿಸಿದಾಗ  ನಿಮ್ಮ ಮಗುವು ಹೀಗೆ ಎಷ್ಟು ಬಾರಿ ಹೀಗೆ  ಮಾಡಿತ್ತು <b><br>
					11.ಗದ್ದಲ ಮಾಡಿರುವುದು ಅಥವಾ ಅತ್ತಿರುವುದು?</td>
					
                    <?php for($i = 1;$i < 8;$i++):?>
                    <td> <?= $form->field($model, 'fuss_facewash')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'fuss_facewash')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'fuss_facewash')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

					</td>
                </tr>
                <tr>
                    <td><b>ತಲೆ ಸ್ನಾನ ಮಾಡಿಸುವಾಗ ಎಷ್ಟು ಬಾರಿ ಹೀಗೆ ಮಾಡಿತ್ತು:<b><br>
					12.ಗದ್ದಲ ಮಾಡಿರುವುದು ಅಥವಾ ಅತ್ತಿರುವುದು?</td>
					
                    <?php for($i = 1;$i < 8;$i++):?>
                    <td> <?= $form->field($model, 'fuss_hairwash')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'fuss_hairwash')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'fuss_hairwash')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

					</td>
                </tr>
				
                <tr>
                    <td><b>ನಿಮ್ಮ ಮಗುವು ಯಾವುದಾದರು ವಸ್ತುವಿನೊಂದಿಗೆ ಆಟವಾಡುವಾಗ ಅದನ್ನು ಮಗುವಿನಿಂದ ತೆಗೆದುಕೊಳ್ಳ ಬೇಕಾದಾಗ ನಿಮ್ಮ ಮಗು ಎಷ್ಟು  ಬಾರಿ ಹೀಗೆ ಮಾಡಿತು:</b><br/>
					13.  ಕ್ಷಣಕ್ಕೆ ಮಗು ಅತ್ತಿದ್ದು  ಅಥವಾ ಬೇಜಾರು ತೋರಿಸಿದ್ದು  </td>
					
                    <?php for($i = 1;$i < 8;$i++):?>
                    <td> <?= $form->field($model, 'cry_removed_playrhing')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'cry_removed_playrhing')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'cry_removed_playrhing')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

					</td>
                </tr>
				
                <tr>
                    <td><b>ನಿಮ್ಮ ಮಗುವು ಯಾವುದಾದರು ವಸ್ತುವಿನೊಂದಿಗೆ ಆಟವಾಡುವಾಗ ಅದನ್ನು ಮಗುವಿನಿಂದ ತೆಗೆದುಕೊಳ್ಳ ಬೇಕಾದಾಗ ನಿಮ್ಮ ಮಗು ಎಷ್ಟು  ಬಾರಿ ಹೀಗೆ ಮಾಡಿತು:</b><br/>
					14.ಅದರ ಬಗ್ಗೆ ಚಿಂತೆ ಮಾಡಲಿಲ್ಲ  </td>
					
                    <?php for($i = 1;$i < 8;$i++):?>
                    <td> <?= $form->field($model, 'not_bothered_removed_plaything')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'not_bothered_removed_plaything')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'not_bothered_removed_plaything')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

					</td>
                </tr>
				
                <tr>
                    <td><b>ನಿಮ್ಮ ಮಗು ಕಳೆದ ವಾರದಲ್ಲಿ ಎಷ್ಟು  ಬಾರಿ ಹೀಗೆ ಮಾಡಿತು:</b><br>
					15.ಸೀಮಿತವಾದ ಸ್ಥಳದಲ್ಲಿ ಇರಿಸಿದ್ದಕ್ಕೆ ಪ್ರತಿಭಟಿಸಿರುವುದು / ಮುನಿಸಿಕೊಂಡಿರುವುದು ? ಉದಾ : ಕುರ್ಚಿಯಲ್ಲಿ ಕೂಡಿಸಿದಾಗ ,ಕಾರ್ ಸೀಟ್  </td>
					
                    <?php for($i = 1;$i < 8;$i++):?>
                    <td> <?= $form->field($model, 'protest')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'protest')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'protest')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

					</td>
                </tr>
				
                <tr>
                    <td><b>ಅವನನ್ನು /ಅವಳನ್ನು ಅಂಗಾತ ಮಲಗಿಸಿದಾಗ ಎಷ್ಟು ಬಾರಿ  ಹೇಗೆ ಮಾಡಿತ್ತು ? </b><br>
					16.ಗದ್ದಲ ಮಾಡಿರುವುದು ಅಥವಾ  ವಿರೋಧ ವ್ಯಕ್ತ ಪಡಿಸಿರಿವುದು  </td>
					
                    <?php for($i = 1;$i < 8;$i++):?>
                    <td> <?= $form->field($model, 'fuss_back')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'fuss_back')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'fuss_back')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

					</td>
                </tr>
				
                <tr>
                    <td><b>ನಿಮ್ಮ ಮಗುವಿಗೆ ಯಾವುದಾದರೂ   ಬೇಕಾದಾಗ  ಎಷ್ಟು ಬಾರಿ ಹೀಗೆ ಮಾಡಿತು:</b><br>
					17.ಅವನಿಗೆ ಬೇಕಾಗಿದ್ದು ಸಿಗದಿದ್ದಾಗ ಅಸಮಾಧಾನ ವ್ಯಕ್ತ ಪಡಿಸಿರುವುದು </td>
					
                    <?php for($i = 1;$i < 8;$i++):?>
                    <td> <?= $form->field($model, 'upset_wanted_something')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'upset_wanted_something')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'upset_wanted_something')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

					</td>
                </tr>
				
                <tr>
                    <td>18.ಅವನಿಗೆ ಬೇಕಾಗಿದ್ದು ಸಿಗದಿದ್ದಾಗ  ಅತಿಯಾದ ಹಠ ಮಾಡಿರುವುದು (ಅಳುವುದು ,ಕಿರುಚುವುದು ,ಮುಖವನ್ನು ಕೆಂಪಗೆ ಮಾಡಿಕೊಂಡಿರುವುದು ಇತ್ಯಾದಿ )</td>
					
                    <?php for($i = 1;$i < 8;$i++):?>
                    <td> <?= $form->field($model, 'tantrums_wanted_something')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'tantrums_wanted_something')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'tantrums_wanted_something')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
					</td>
                </tr>
				
                <tr>
                    <td> <b>ಕುರ್ಚಿಯಲ್ಲಿ ಅಥವಾ  ಜೋಲಿ ಯಲ್ಲಿ ಕೂಡಿಸಿದಾಗ ಎಷ್ಟು ಬಾರಿ ಹೀಗೆ ಮಾಡಿತು:</b><br/>
					19.ಮೊದಲಿಗೆ ಅಸಮಾಧಾನ ವ್ಯಕ್ತ ಪಡಿಸಿ ನಂತರ  ಸುಮ್ಮನಾಗಿರುವುದು </td>
					
                    <?php for($i = 1;$i < 8;$i++):?>
                    <td> <?= $form->field($model, 'distress_infant_seat')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'distress_infant_seat')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					
					<?= $form->field($model, 'distress_infant_seat')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				</td>
                </tr>
				
                <tr><td><b>ಕಳೆದ ಒಂದು ವಾರದಲ್ಲಿ ಎಷ್ಟು ಬಾರಿ ಅವನು /ಳು  ಹೀಗೆ ಮಾಡಿತ್ತು ?
20.ಪೋಷಕರ ವೇಷ ಭೂಷಣದಲ್ಲಿ  ಬದಲಾವಣೆಯಾದಾಗ ಅತ್ತಿರುವುದು ಅಥವಾ ಅಸಮಾಧಾನ ವ್ಯಕ್ತಪಡಿಸಿರಿವುದು </b>

                  </td>
					
                    <?php for($i = 1;$i < 8;$i++):?>
                    <td> <?= $form->field($model, 'cry_parents_chane_appearence')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'cry_parents_chane_appearence')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'cry_parents_chane_appearence')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
					</td>
                </tr>
				
                <tr>
                    <td>21.ಧೈಹಿಕ ವಾಗಿ ತಕ್ಷಣ ಬದಲಾವಣೆ ಆದಾಗ ಹೆದರಿರುವುದು (ಉದಾ :ತಕ್ಷಣ ಸರಿದಾಗ ) (.. startle at a sudden change in body position for example, when moved suddenly)?</td>
					
                    <?php for($i = 1;$i < 8;$i++):?>
                    <td> <?= $form->field($model, 'startle_sudden_change')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'startle_sudden_change')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'startle_sudden_change')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
					</td>
                </tr>
				
                <tr><td><b>ಕಳೆದ ಒಂದು ವಾರದಲ್ಲಿ ಎಷ್ಟು ಬಾರಿ ಅವನು /ಳು  ಹೀಗೆ ಮಾಡಿತ್ತು ?</b><br/>
						22.ಜೋರಾದ ಶಬ್ದ ಅಥವಾ ಹಠಾತ್ ಗದ್ದಲ ದಿಂದ ಹೆದರಿರುವುದು </td>
					<?php for($i = 1;$i < 8;$i++):?>
                    <td><?= $form->field($model, 'startle_noise')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'startle_noise')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'startle_noise')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
					</td>
                </tr>
				
                <tr><td><b>ಕಳೆದ ಎರಡು ವಾರದಿಂದ  ಅಪರಿಚಿತ ವ್ಯಕ್ತಿಯನ್ನು  ಪರಿಚಯಿಸಿದಾಗ ,ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ ಮಗುವು  ಹೀಗೆ ಮಾಡಿತ್ತು : </b><br/>
				
						23.ಪೋಷಕರನ್ನು ಅಂಟಿ ಕೊಂಡಿರುವುದು </td>
					
                    <?php for($i = 1;$i < 8;$i++):?>
                    <td> <?= $form->field($model, 'cling_parent_unfamiliar_person')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'cling_parent_unfamiliar_person')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'cling_parent_unfamiliar_person')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
					</td>
                </tr>
				
                <tr>
                    <td>24.ಅಪರಿಚಿತ ವ್ಯಕ್ತಿ ಬಳಿ ಹೋಗಲು ನಿರಾಕರಿಸಿರುವುದು </td>
                    <?php for($i = 1;$i < 8;$i++):?>
					<td><?= $form->field($model, 'refuse_unfamiliar_person')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'refuse_unfamiliar_person')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'refuse_unfamiliar_person')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
					</td>
                </tr>
				
                <tr>
                    <td>25.ದೊಡ್ಡವರು /ವಯಸ್ಕರಿಂದ  ದೂರ ಸರಿಯುವುದು  </td>
					<?php for($i = 1;$i < 8;$i++):?>
                    <td><?= $form->field($model, 'hang_back_unfamiliar_person')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'hang_back_unfamiliar_person')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'hang_back_unfamiliar_person')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
					</td>
                </tr>
                
				<tr>
                    <td>26.ಅಪರಿಚಿತ  ವ್ಯಕ್ತಿ ಯೊಂದಿಗೆ ಹೊಂದಿಕೊಳ್ಳದಿರುವುದು </td>
					<?php for($i = 1;$i < 8;$i++):?>
                    <td>
                    <?= $form->field($model, 'never_warmup_unfamiliar_person')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'never_warmup_unfamiliar_person')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'never_warmup_unfamiliar_person')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
					</td>
                </tr>
                <tr>
                    <td>27. ಪೋಷಕರನ್ನು ಅಂಟಿ ಕೊಂಡಿರುವುದು</td>
                    <?php for($i = 1;$i < 8;$i++):?>
                    <td><?= $form->field($model, 'cling_several_unfamiliar_persons')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'cling_several_unfamiliar_persons')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'cling_several_unfamiliar_persons')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
					</td>
                </tr>
                <tr>
                    <td>28.ಅತ್ತಿರುವುದು</td>
                    <?php for($i = 1;$i < 8;$i++):?>
                    <td><?= $form->field($model, 'cry_unfamiliar_adults')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'cry_unfamiliar_adults')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'cry_unfamiliar_adults')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
					</td>
                </tr>
                <tr>
                    <td>29.೧೦ ನಿಮಿಷದ ತನಕ ಅಥವಾ ಜಾಸ್ತಿ ಸಮಯ ಬೇಜಾರಾಗಿರುವುದು </td>
                    <?php for($i = 1;$i < 8;$i++):?>
                    <td><?= $form->field($model, 'upset_morethan_10min')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'upset_morethan_10min')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'upset_morethan_10min')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
					</td>
                </tr>
                <tr>
                    <td>30.ಮೊದಲು ಕೆಲವು ನಿಮಿಷಗಳು ಬೇಜಾರಾಗಿರುವುದು </td>
                    <?php for($i = 1;$i < 8;$i++):?>
                    <td> <?= $form->field($model, 'distress_new_place')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'distress_new_place')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'distress_new_place')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
					</td>
                </tr>
                <tr>
                    <td>31)೧೦ ನಿಮಿಷಗಳ ತನಕ ಅಥವಾ ಜಾಸ್ತಿ ಸಮಯ  ಬೇಜಾರಾಗಿರುವುದು </td>
                    <?php for($i = 1;$i < 8;$i++):?>
                    <td> <?= $form->field($model, 'upset_new_place')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'upset_new_place')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'upset_new_place')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
					</td>
                </tr>
                <tr>
                    <td>32.ಅವನನ್ನು ಎತ್ತಿ ಕೊಳ್ಳಲು ಬಿಟ್ಟಿರುವುದು </td>
                    <?php for($i = 1;$i < 8;$i++):?>
                    <td> <?= $form->field($model, 'allow_unfamiliar_person_pickup')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'allow_unfamiliar_person_pickup')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'allow_unfamiliar_person_pickup')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
					</td>
                </tr>
                <tr>
                    <td><b>ನೀವು ಮತ್ತು ಮಗುವು ಹೊರಗಡೆ ಹೋದಾಗ ,ಅಪರಿಚಿತರು ಮಾತನಾಡಿಸಿದಾಗ ಮಗು ಎಷ್ಟು ಬಾರಿ ಹೀಗೆ ಮಾಡಿತು  </b><br>
					33.ಬೇಜಾರಾಗಿರುವುದನ್ನು ತೋರಿಸಿದ್ದು </td>
                    <?php for($i = 1;$i < 8;$i++):?>
                    <td> <?= $form->field($model, 'distress_outside')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'distress_outside')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'distress_outside')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
					</td>
                </tr>
                <tr>
                    <td>34.ಅತ್ತಿರುವುದು</td>
                    <?php for($i = 1;$i < 8;$i++):?>
                    <td><?= $form->field($model, 'cry_taken_outside')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'cry_taken_outside')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'cry_taken_outside')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
					</td>
                </tr>
                <tr>
                    <td><b>ಅಪರಿಚಿತ ವ್ಯಕ್ತಿಯು ಮನೆಗೆ ಬಂದಾಗ ಮಗುವು ಎಷ್ಟು ಬಾರಿ ಹೀಗೆ ಮಾಡಿತ್ತು  <b><br>
					35.ಅವನನ್ನು ಎತ್ತಿಕೊಳ್ಳಲು ಹೋದಾಗ ಅತ್ತಿರುವುದು</td>
                    <?php for($i = 1;$i < 8;$i++):?>
                    <td> <?= $form->field($model, 'cry_unfamilar_person_pickup')->radio(['label' => $i, 'value' => $i])?>
					</td>
					<?php endfor;?>
					<td><?= $form->field($model, 'cry_unfamilar_person_pickup')->radio(['label' => 'X', 'value' => 0])?>
					</td>
					<td>
					<?= $form->field($model, 'cry_unfamilar_person_pickup')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
					</td>
                </tr>
                <tr>
                
					<td id="scaleflag" class="hidden"><?= $form->field($model, 'score')->input('number',['readonly'=>true])->label(false);?></td>
				</tr>
                
            </tbody>
        </table>

    </div>
	</div>
	<?php ActiveForm::end(); ?>
</div>

<?php $this->registerJs('
	$("#"+formObj.formName).one("change",function()
	{
		autoSaveObj = formObj;
	});
',\yii\web\View::POS_READY)?>
<script type="text/javascript">

		var formObj = {url : "baby-likes-dislikes/ajax",formName : "formBabylikesdislikes"};
    
		var calc = function () {
        var totalValue = 0;
        var root= 'babylikesdislikes';
		var id = '#'+root+'-';
		var clas = '.field-'+root+'-';
        var arr = ['fussing_crying', 'toss_cot','fuss_immediately','play_quitely','cry_someone_didnt_come','angry','contented_corner','cry_before_nap','lie_sit_quietly','squirm','fuss_facewash','fuss_hairwash','cry_removed_playrhing','not_bothered_removed_plaything','protest','fuss_back','upset_wanted_something','tantrums_wanted_something','distress_infant_seat','cry_parents_chane_appearence',
		'startle_sudden_change','startle_noise','cling_parent_unfamiliar_person','refuse_unfamiliar_person','hang_back_unfamiliar_person','never_warmup_unfamiliar_person','cling_several_unfamiliar_persons','cry_unfamiliar_adults','upset_morethan_10min','distress_new_place','upset_new_place','allow_unfamiliar_person_pickup','distress_outside','cry_taken_outside','cry_unfamilar_person_pickup'];
        
        for (var i in arr) {
            var val = $(clas + arr[i] + ' input[type=radio]:checked').val();
			if (parseInt(val)>=0) {
                totalValue += parseInt(val);
            }
        }
        $(id+'score').val(totalValue);
		autoSaveObj = formObj;
    }

</script>
