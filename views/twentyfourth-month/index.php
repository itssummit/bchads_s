<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Assessments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="assessment-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Assessment', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'mother_id',
            'created_dtm',
            'last_updated_dtm',
            'updated_by',
            // 'visit_date',
            // 'assessment_type',
            // 'respondent',
            // 'respondent_id',
            // 'relation_type',
            // 'relationship_id',
            // 'assessment_date',
            // 'place_of_assesment',
            // 'assessment_by',
            // 'caregiver',
            // 'relation_with_child',
            // 'assessment_date_ac',
            // 'place_of_assessment_ac',
            // 'assessment_by_ac',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
