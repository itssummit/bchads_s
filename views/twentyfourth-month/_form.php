<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Assessment */
/* @var $form yii\widgets\ActiveForm */ 
if($model->assessment_date)
{
	$model->assessment_date = date('d-M-Y',strtotime($model->assessment_date));
}
if($model->assessment_date_ac)
{
	$model->assessment_date_ac = date('d-M-Y',strtotime($model->assessment_date_ac));
}
$caregiver =[1=>'Father',
            2=>'Maternal Grandmother',
            3=>'Maternal Grandfather',
            4=>'Paternal Grandmother',
            5=>'Paternal Grandfather',
            6=>'Aunt',
            7=>'Uncle',
            8=>'Other'];	

?>

<div class="assessment-form">

    <?php $form = ActiveForm::begin(['id'=>'twentyfourthAssessmentForm']); ?>

   
	
	<div class="row">
		<div class="col-lg-6" style="display:none;">
			<?= $form->field($model, 'assessment_type')->textInput(['maxlength' => true]) ?>
		</div>
	
		<div class="col-lg-6">
			<?= $form->field($model, 'assessment_date')->widget(DatePicker::className(),['type' => DatePicker::TYPE_COMPONENT_APPEND,'removeButton' => false,'pluginOptions'=>['endDate' => '+0d','format' => 'dd-M-yyyy','orientation' => 'left'], 'options'=>['placeHolder' => 'Assessment date','id' => '24m']]); ?>	           
		</div>
		<div class="col-lg-6">
			<?= $form->field($model, 'place_of_assesment')->dropDownList([''=>'','Home' => 'Home','NIMHANS' => 'NIMHANS' ,'NCWB'=>'NCWB','Anganavaadi'=>'Anganavaadi' ]); ?>
		</div>
	</div>		
	<div class="row">
		
		<div class="col-lg-6">
			<?= $form->field($model, 'assessment_by')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			
			<?= $form->field($model, 'is_there_acg')->dropDownList([''=>'','1' => 'YES','0' => 'NO']) ?>
		</div>
	</div>
<div id ="depend3" style = "display: <?php echo($model->is_there_acg >= '1')?"display":"none"; ?> ">	
	<div class="row">
		<!--<div class="col-lg-6">
			< ?= $form->field($model, 'caregiver')->textInput(['maxlength' => true]) ?>
		</div>-->
		<div class="col-lg-6">
			<?= $form->field($model, 'relation_with_child')->dropDownList($caregiver,['prompt' => 'Select']) ?>
		</div>
        <div  class="col-lg-6" id="other_24" style = "display:<?php echo ($model->relation_with_child == 8)?"display":"none";?>">
            <?= $form->field($model, 'specify_other')->textInput(['maxlength' => true])?>
        </div>
	</div>
	<div class="row">
		<div class="col-lg-6">
		<label>Assessment Date(CareGiver)</label>
		<?= $form->field($model, 'assessment_date_ac')->widget(DatePicker::className(),['type' => DatePicker::TYPE_COMPONENT_APPEND,'removeButton' => false,'pluginOptions'=>['endDate' => '+0d','format' => 'dd-M-yyyy','orientation' => 'left'], 'options'=>['placeHolder' => 'Assessment date (ACG)','id' => '24ac']])->label(false); ?>
			
		</div>
		<div class="col-lg-6">
		<label>Place Of Assessment(CareGiver)</label>
			<?= $form->field($model, 'place_of_assessment_ac')->dropDownList([''=>'','Home' => 'Home','NIMHANS' => 'NIMHANS' ,'NCWB'=>'NCWB','Anganavaadi'=>'Anganavaadi' ])->label(false); ?>
		</div>
	</div>	
	<div class="row">
		<div class="col-lg-6">
		<label>Assessment By(CareGiver)</label>
			<?= $form->field($model, 'assessment_by_ac')->textInput(['maxlength' => true])->label(false); ?>
		</div>
	</div>
</div>
    <?php ActiveForm::end(); ?>
<button type = "button" class = "btn btn-success" id = "saveTwentyfourth">Save</button>
<?= Html::a('Scales form', ['assessment/scaleform','aid' => $model->id], ['class'=>'btn btn-warning','id' => 'id24th','style' => ($model->id)?'display:initial':'display:none']); ?>
</div>
<?php $this->registerJs("
	
	
	$('#saveTwentyfourth').click(function(){
		autoSaveForm({url : 'twentyfourth-month/ajax&id=".$model->id."&motherId=".$model->mother_id."',formName : 'twentyfourthAssessmentForm'},'id24th');
	});
	
	$('#twentyfourthAssessmentForm #assessment-is_there_acg').change(function(){
		if($(this).val() == '1')
		{
			
			$('#depend3').show();
		}
		else
		{
			$('#depend3').hide();
		}
	});

     $('#twentyfourthAssessmentForm #assessment-relation_with_child').change(function () {
        if ($(this).val() == 8) {
            $('#other_24').show();
        }
        else{
             $('#other_24').hide();
            }
        });
		
",\yii\web\View::POS_READY)?>