<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$keyPair = [-15 => '-15: Child Not Co-Operative For Assessment',
		  -14 => '-14: Not Available For Assessment',
		  -11 => '-11: Inadequate Information',
		  -10 => '-10: Not Applicable',
		  -9 => '-9: Missing',
		  -8 => '-8: Refused',
		  -7 => '-7: Partner Accompanied Missing',
		  -6 => '-6: Family Accompanied Missing',
		  -5 => '-5: Missing:Not Asked',
		  -3 => '-3: No Such Relationship/No Partner'];
/* @var $this yii\web\View */
/* @var $model app\models\WooleyDepression */
/* @var $form yii\widgets\ActiveForm */
/*
*/
$this->registerJsFile('@web/js/wscalesome.js',['depends' => [\yii\web\JqueryAsset::className()]]);
?>
		
<div  class="wooley-depression-form">

    <?php $form = ActiveForm::begin(['id'=>'formwooleydepression']); ?>
	<div id="withBoxShadow" class="col-xs-12 col-md-12 col-lg-12 col-sm-12">
	

	<div class="col-lg-12">
		
		<div class="col-lg-8">
			<label>1) ಕಳೆದ ತಿಂಗಳಲ್ಲಿ, ನೀವು ಬೇಜಾರಾಗಿರುವುದು, ಖಿನ್ನರಾಗಿರುವುದು ಅಥವಾ ಹತಾಶರಾಗಿ ಇವುಗಳಿಂದ ನೀವು ಪದೇ ಪದೇ ತೊಂದರೆಗೆ ಒಳಪಟ್ಟಿದ್ದೀರಾ?</label>
				<?= $form->field($model, 'depressed_pastmonth')->radioList([1=>'ಹೌದು ',0=>'ಇಲ್ಲ '],['itemOptions'=>['class'=>'radio']])->label(false);?>
		</div>
	
		<div class="col-lg-4">	
			<?= $form->field($model, 'depressed_pastmonth')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
		</div>
		
	</div>		
	
	<div class="col-lg-12">
	
		<div class="col-lg-8">
			<label>2)ಕಳೆದ  ತಿಂಗಳಲ್ಲಿ ,  ನೀವು   ಏನನ್ನಾದರೂ  ಮಾಡುವಾಗ ಕಡಿಮೆ ಆಸಕ್ತಿ ಅಥವಾ ಆನಂದದಾಯಕವಾಗಿದ್ದು  ಇದರಿಂದ ನೀವು ತೊಂದರೆಗೆ ಒಳಪಟ್ಟಿದ್ದೀರಾ?</label>
				<?= $form->field($model, 'little_interest_pleasure_pastmonth')->radioList([1=>'ಹೌದು ',0=>'ಇಲ್ಲ '],['itemOptions'=>['class'=>'radio1']])->label(false);?>
		</div>
		
		<div class="col-lg-4">	
		<?= $form->field($model, 'little_interest_pleasure_pastmonth')->dropDownList($keyPair,['prompt' => ''])->label(false)?>
		</div>
	</div>
	
	
	<div class="col-lg-12" id = "wynh">
		<div class="col-lg-8">
			<h4><label>ಈ ಮೇಲಿನ ಯಾವುದಾದರೊಂದು ಪ್ರಶ್ನೆಗೆ ಮಹಿಳೆಯು ಹೌದು ಎಂದು ಉತ್ತರಿಸಿದ್ದಲ್ಲಿ ಇದನ್ನು ಕೇಳಿ.</label></h4><br>
			<label>3) ನಿಮಗೇನಾದರೂ ಸಹಾಯದ ಅವಶ್ಯಕತೆ ಅಥವಾ ಅಗತ್ಯತೆ ಇದೆ ಎಂದು ನಿಮಗನಿಸುತ್ತಿದೆಯೇ?</label>
			<?= $form->field($model, 'women_yes_needhelp')->textInput() ?><p>
		</div>
	</div>
	
	
	<div id="scaleflag" class="hidden">
    <?= $form->field($model, 'score')->input('number',['readonly'=>true])->label(false);?>
	</div>
	
	
</div>	
    <?php ActiveForm::end(); ?>

</div>
<?php $this->registerJs('
	checkWYNH(0);
	$("#"+formObj.formName).one("change",function()
	{
		autoSave(formObj);
	});
',\yii\web\View::POS_READY);?>
<script type="text/javascript">
	var formObj = {'url' : "wooley-depression/ajax",'formName' : "formwooleydepression"};
    var calc = function () 
	{
        var totalValue = 0;
		var root = 'wooleydepression';
        var id = '#'+root+'-';
		var clas = '.field-'+root+'-';
        var arr = ['depressed_pastmonth', 'little_interest_pleasure_pastmonth'];
		
		for (var i in arr) {
            var val = $(clas + arr[i] + ' input[type=radio]:checked').val();
			if (parseInt(val) >= 0) {
                totalValue += parseInt(val);
            }
        }
		
        $(id+'score').val(totalValue);
		autoSaveObj = formObj;
    }
	var checkWYNH = function(trig){
		var val1 = parseInt($('input[name="WooleyDepression[depressed_pastmonth]"]:checked').val());
		var val2 = parseInt($('input[name="WooleyDepression[little_interest_pleasure_pastmonth]"]:checked').val());
		if(val1 == 1  || val2 == 1)
		{
			if(trig)
			{
				$("#wooleydepression-women_yes_needhelp").val("")
			}
			$('#wynh').show();
		}
		else
		{
			$('#wynh').hide();
			$("#wooleydepression-women_yes_needhelp").val("-10")
		}
	}
</script>
