<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\WooleyDepression */

$this->title = 'Wooley Depression';
$this->params['breadcrumbs'][] = ['label' => 'Wooley Depressions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wooley-depression-create">

    <h1 align = "center">ಖಿನ್ನತೆಯನ್ನು ಪತ್ತೆಹಚ್ಚುವ ಪ್ರಶ್ನಾವಳಿ </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
