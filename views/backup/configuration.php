<?php
use yii\helpers\Html;
use app\models\Proposal;
$this->registerJsFile('@web/jsControllers/BackupController.js',['depends' => [\app\assets\AppAsset::className()]]);
\app\assets\AppAsset::register($this);

?>
<style type="text/css">
h3{
    text-decoration: underline;
    text-align: center;
}
.alert{
  padding: 10px;
  display: none;
}
</style>
<div ng-cloak class="ng-cloak withBoxShadow" ng-controller="BackupController" ng-init="Configuration()">
<br>
<h3>Backup Configuration Details</h3>

<br />
<br />

	<div class="row">
		<div class="alert alert-success fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> Backup Configuration successfully saved </b>
		</div>
		<div class="alert alert-danger fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Failure!</strong> Not able to save backup configrations please try again !
		</div>
	</div>

  <form class="form-horizontal" role="form">
	  <div class="form-group">
	    <label class="control-label col-sm-3" for="full_backup"><span class="mandatory-field">*&nbsp;</span>Full Backup Path :</label>
	    <div class="col-sm-9">
	      <input class="form-control" ng-model="BackupConfig.full_backup" name="full_backup" placeholder="destination path for full backup"/><br>
	    <span>Ex : <b>D:/db/full/backup</b></span>
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="control-label col-sm-3" for="incremental_backup"><span class="mandatory-field">*&nbsp;</span>Incremental Backup Path :</label>
	    <div class="col-sm-9">
	      <input class="form-control" ng-model="BackupConfig.incremental_backup" name="incremental_backup" placeholder="destination path for incremental backup"/><br>
	    <span>Ex : <b>D:/db/incremental/backup</b></span>
	    </div>
	  </div>
	  <div class="form-group"> 
	    <div class="col-sm-offset-10 col-sm-1">
	      <button type="submit" ng-click="Back()" class="btn btn-default">Back</button>
	    </div>
	    <div class="col-sm-1">
	      <button type="submit" ng-click="Savedata()" class="btn btn-default">Save</button>
	    </div>
	  </div>
  </form>
</div>