<?php
use yii\helpers\Html;
use app\models\Proposal;
$this->registerJsFile('@web/jsControllers/BackupController.js',['depends' => [\app\assets\AppAsset::className()]]);
\app\assets\AppAsset::register($this);

?>
<style type="text/css">
h3{
    text-decoration: underline;
    text-align: center;
}
th,td{
  padding: 40px;
  margin: 40px;
  font-size: 14px;
}
.alert{
  padding: 10px;
  display: none;
}
</style>
<div ng-cloak class="ng-cloak" ng-controller = "BackupController" ng-init="Index()">
<br>

<h3>Backup History Details</h3>
<br />
<br />

<div class="row">
  <div class="alert alert-success fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Success!</strong> Backup successfully created and placed at <b>{{newLocaltion}}</b>
  </div>
  <div class="alert alert-danger fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Failure!</strong> Not able to take backup please try again !
  </div>
</div>

<div class="withBoxShadow">
      <div class="childWithBoxShadow">
<div class="row">
  <div class="col-sm-4" style="padding-left: 20px;">
      
          <div class="input-group stylish-input-group">
              <input type="text" class="form-control"  ng-model="searchName" placeholder="Search" >
              <span class="input-group-addon">
                  <button type="submit">
                      <span class="glyphicon glyphicon-search"></span>
                  </button>  
              </span>
         
      </div>
  </div>
    <a href="<?php echo Yii::$app->homeUrl; ?>?r=backup/configuration"><button style="float: left;width: initial;" class="btn btn-default">Add Backup Configurations</button></a>
  <div style="float:right;" class="input-group">
    <select ng-init="BackUpType = 'full'" ng-model="BackUpType" class="form-control" style="float: left;width: initial;">
      <option ng-repeat="item in BackUpTypes">{{item}}</option>
    </select>
    <button style="float: right;width: initial;" class="btn btn-default" ng-click="TakeBackUp(BackUpType)" >Take Backup Now</button>
  </div>
</div>
<br /><br />  

<div class="row table-responsive">                 
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>#</th>
        <th>Type</th>
        <th>Filename</th>
        <th>Location</th>
        <th>Duration</th>
        <th>Result</th>
        <th>Taken&nbsp;By</th>
        <th>Timestamp</th>
        <th>Created on</th>
        <th>Updated on</th>
      </tr>
    </thead>
    <tbody>
       <!--<tr ng-repeat="backup in AllBackups | filter : searchName" >--> 
      <tr dir-paginate="backup in AllBackups | filter : searchName | itemsPerPage: 5">
        <td>{{$index+1}}</td>
        <td>{{backup.type}}</td>
        <td>{{backup.filename}}</td>
        <td>{{backup.location}}</td>
        <td>{{backup.duration}}</td>
        <td>{{backup.result}}</td>
        <td>{{backup.backup_by}}</td>
        <td>{{backup.timestamp}}</td>
        <td>{{backup.created_date}}</td>
        <td>{{backup.last_updated_date}}</td>
      </tr>
      <tr ng-show="ListError">
        <td colspan="10" style="text-align:center;"> 
                <small class="error">----  No backups available   ----</small>
        </td>
      </tr>
    </tbody>
  </table>
</div>
<div class="row pull-right"> 
     <dir-pagination-controls   max-size="10"  direction-links="true" boundary-links="true"></dir-pagination-controls>
  
</div>

</div>
 </div>
        </div>




