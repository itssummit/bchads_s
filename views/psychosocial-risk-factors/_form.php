<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
$assessmentId = Yii::$app->session->get('assessmentId');
$this->registerJsFile('@web/js/scalepsycho.js',['depends' => [\yii\web\JqueryAsset::className()]]);
/* @var $this yii\web\View     checkboxList(['0' => 'financial', '1' => 'work/responsibilities', '2' => 'others']);*/
/* @var $model app\models\PsychosocialRiskFactors */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
#psychosocialriskfactors-not_applicable{
	width:250px !important;
}
#psychosocialriskfactors-health_problem_of_familymember{
	width:500px !important;
}
#psychosocialriskfactors-gender_of_the_infant{
	width:500px !important;
}
.indent{
	margin-left:80px;
}
.idform:hover{     
    border: 1px ridge #31a4c4 ;
    box-shadow:2px 0px 3px 3px #b6dbf7 inset;
    border-top: 0px;
}
</style>


 <div id="fullform">
<div class="psychosocial-risk-factors-form">

    <?php $form = ActiveForm::begin(['id' => 'formPsychosocialriskfactor']); ?>
<div id="withBoxShadow">
	
	
	<div class="row">
		<div class="col-lg-6">
			<h4><b>Is This Scale Applicable ?</b></h4>
		<?= $form->field($model, 'not_applicable')->dropDownList([''=>'','YES' => 'Not Applicable', 'NO' => 'Applicable'])->label(false); ?>
		</div>
	</div>
		
	<div class="row">
		<div class="col-lg-6">
			<?= $form->field($model, 'stress_issues')->checkbox(array('label'=>'1) Stress Related Issues','class' => 'stress'));?>
			
		</div>
	</div>
	<div class="row" id="rrr" style = "display:<?php echo ($model->stress_issues == '1')?"display":"none";?>">
		<div class="col-lg-2">
			<?= $form->field($model, 'financial')->checkbox(array('label'=>'a.Financial'));?>
		</div>
		<div class="col-lg-2">
			<?= $form->field($model, 'work')->checkbox(array('label'=>'b.Work'));?>
		</div>
		<div class="col-lg-2" >
			<?= $form->field($model, 'others')->checkbox(array('label'=>'c.Others','class' => 'others'));?>
		</div>
		<div class="col-lg-4"  id="oth" style = "display:<?php echo ($model->others == '1')?"display":"none";?>" >
			<label>Specify</label>
			<?= $form->field($model, 'others_desc')->textInput()->label(false);?>
		</div>
	</div>
	
	
	
	
	<div class="row">
		<div class="col-lg-6">
				<?= $form->field($model, 'lack_care')->checkbox(['label'=>'2) Lack of support/Care','class' => 'lack_care']);?>
		</div>
	</div>
	
	<div class="row" id="rr1" style = "display:<?php echo ($model->lack_care == '1')?"display":"none";?>" >
		<div class="col-lg-4">
			<?= $form->field($model, 'spouse')->checkbox(array('label'=>'a.Spouse'));?>
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'in_laws')->checkbox(array('label'=>'b.In laws'));?>
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'parents')->checkbox(array('label'=>'c.Parents/Siblings'));?>
		</div>
		
	</div>
	
	
	<div class="row">
		<div class="col-lg-6">
				<?= $form->field($model, 'violence_experience')->checkbox(['label'=>'3) Experience of violence','class' => 'violence_experience']);?>
		</div>
	</div>
	
	<div class="row"  id="rr2" style = "display:<?php echo ($model->lack_care == '1')?"display":"none";?>" >
		<div class="col-lg-4">
			<?= $form->field($model, 'psychological')->checkbox(array('label'=>'a.Psychological'));?>
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'physical')->checkbox(array('label'=>'b.Physical'));?>
		</div>
		<div class="col-lg-4">
			<?= $form->field($model, 'sexual')->checkbox(array('label'=>'c.Sexual'));?>
		</div>
		
	</div>
	
	
	
	

	<div class="row">
		<div class="col-lg-12">
				<?= $form->field($model, 'anxiety_issues')->checkbox(['label'=>'4) anxiety issues (fear about pregnancy,fear about childbirth,General worry/fear)']);?>
				
		</div>
	</div>


	<div class="row">
		<div class="col-lg-12">
				<?= $form->field($model, 'mood_issues')->checkbox(['label'=>'5) mood issues (feeling low,loss of interest,crying spells)']);?>
				
		</div>
	</div>


		<div class="row">
		<div class="col-lg-12">
				<?= $form->field($model, 'suicidal_ideation')->checkbox(['label'=>'6) suicidal_ideation (lifetime attempt,ideation during past current pregnancy,Postpartum)']);?>
				
		</div>
	</div>

		<div class="row">
		<div class="col-lg-12">
				<?= $form->field($model, 'poor_pregnancy')->checkbox(['label'=>'7)Past history of poor pregnancy outcome : (neonatal death,still birth,any congenital anomalies)']);?>
				
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-12">
				<?= $form->field($model, 'traumatic_events')->checkbox(['label'=>'8)Major traumatic life events : (death of close one,major loss)']);?>
				
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-12">
				<?= $form->field($model, 'substance_use_spouse')->checkbox(['label'=>'9) Substance use/Habits of spouse : (if Yes)']);?>
				
		</div>
	</div>

	<div style="display: none;">
		<?= $form->field($model, 'valid')->checkbox();?>
	</div>

	
	
	<div class="row">
		<div class="col-lg-8">
			<h5><b>10)Major health problems of close family members :</b></h5>
			<?= $form->field($model, 'health_problem_of_familymember')->radioList(array(1=>'yes',0=>'no'),['itemOptions'=>['class' => "healthproblem"]])->label(false); ?>
		</div>
		<div class="col-lg-4" id="rel" style = "display:<?php echo ($model->health_problem_of_familymember == '1')?"display":"none";?>">
			<h5><b>Relation</b></h5>
			<?= $form->field($model, 'relation')->textInput()->label(false) ?>
		</div>
	</div>
	
	<b>Delivery and postpartum</b><br>
	<div class="row">
		<div class="col-lg-6">
			<h5><b>11.a)Complications related to labour :</b></h5>
			<?= $form->field($model, 'complications_related_to_labour_delivery_childbirth')->radioList(['1' => 'Yes',
															'0'=>'No'])->label(false);?>
		</div>
	</div>
	
	
	<div class="row">
		<div class="col-lg-6">
			<h5><b>11.b)Complications related to delivery :</b></h5>
			<?= $form->field($model, 'complications_related_to_labour_delivery_childbirth1')->radioList(['1' => 'Yes',
															'0'=>'No'])->label(false);?>
		</div>
	</div>
	
		
	<div class="row">
		<div class="col-lg-8">
			<h5><b>11.c)Complications related to childbirth :</b></h5>
			<?= $form->field($model, 'complications_related_to_labour_delivery_childbirth2')->radioList(['1' => 'Yes',
															'0'=>'No'])->label(false);?>
		</div>
	</div>
		
	<div class="row">
		<div class="col-lg-8">
			<h5><b>11.d)Obstetric trauma :</b></h5>
			<?= $form->field($model, 'obstetric_trauma')->radioList(['1' => 'Yes',
															'0'=>'No'])->label(false);?>
		</div>
	</div>
		
	<b>Poor OBG outcome</b>	
	<div class="row">
		<div class="col-lg-8">
			<h5><b>c1) :</b></h5>
			<?= $form->field($model, 'poor_obg_outcome')->radioList(['1' => 'Yes',
															'0'=>'No'])->label(false);?>
		</div>
	</div>
		
	<div class="row">
		<div class="col-lg-8">
			<h5><b>c2) :</b></h5>
			<?= $form->field($model, 'poor_obg_outcome1')->radioList(['1' => 'Yes',
															'0'=>'No']);?>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-8">
			<h5><b>c3) :</b></h5>
			<?= $form->field($model, 'poor_obg_outcome2')->radioList(['1' => 'Yes',
															'0'=>'No'])->label(false);?>
		</div>
	</div>
		
		
		
		<div class="row">
		<div class="col-lg-8">
		<label>d)Gender :(Infant) :</label>
		<?= $form->field($model, 'gender_of_the_infant')->radioList(['1' => 'Yes',
															'2'=>'No'])->label(false);?>
		</div>
		</div>
		
		<div class="row">
		<div class="col-lg-8">
		<label>e)Delivery and postpartum :</label>
		<?= $form->field($model, 'delivery_and_postpartum')->textInput()->label(false);?>
		</div>
		</div>
		
	
	
	
	</div>
	<?php if(!$model->id):?>

 </div>">
 <?php endif;?> 

    <?php ActiveForm::end(); ?>
<div class="text-center">

<?php echo Html::a('Scales Menu',Url::toRoute(['assessment/scaleform','aid' => $assessmentId]),['class' => 'btn btn-primary'])."&nbsp;";?>
</div>
</div>
<?php $this->registerJs('
	$("#"+formObj.formName).one("change",function()
	{
		autoSaveObj = formObj;
		
	});
')?>
<?php if($model->not_applicable == 'YES'){
	$this->registerJs('
	setNotApplicableOnLoad("#withBoxShadow","#psychosocialriskfactors-not_applicable");
	', \yii\web\View::POS_READY);

}
	$this->registerJs('

	$("#psychosocialriskfactors-not_applicable").change(function()
		{	
			var value = $(this).val();
			if(value=="YES")
			{
			//var value = $(document).val();
	$(document).find(":input:not(#psychosocialriskfactors-not_applicable)").val("-10");
	$(document).find("input[type=\'radio\']").prop("checked",false);
	$(document).find("input[type=\'checkbox\']").prop("checked",false);
	$(this).val(value);
	autoSave(formObj);
	$(document).find(":input:not(#psychosocialriskfactors-not_applicable)").attr("disabled",true);
	$(document).attr("disabled",false);
	 location.reload();
			}
			else
			{
			//var value = $(document).val();
				
	$(document).find(":input:not(#psychosocialriskfactors-not_applicable)").val("");
	$(document).find("input[type=\'radio\']").prop("checked",false);
	$(document).find("input[type=\'checkbox\']").prop("checked",false);
	$(document).find(":input:not(#psychosocialriskfactors-not_applicable)").attr("disabled",false);
	$(this).val(value);
	autoSave(formObj);
	$(document).attr("disabled",false);
	 location.reload();
					}
		});
		
		$(".healthproblem").click(function()
		{	
			var value = $(this).val();
			if(value=="1")
			{	
				$("#rel").val("bvfvfdvfd");
				$("#rel").show();
	autoSave(formObj);
			}
			else
			{
				$("#rel").hide();
	autoSave(formObj);
			}
		});



$(".stress").click(function()
		{	
  if (this.checked) {
            $("#rrr").val("0");
				$("#rrr").show();
	autoSave(formObj);
		}
		else
			{
				$("#rrr").hide();
	autoSave(formObj);
			}

		});


$(".lack_care").click(function()
		{	
  if (this.checked) {
            $("#rr1").val("0");
				$("#rr1").show();
	autoSave(formObj);
		}
		else
			{
				$("#rr1").hide();
	autoSave(formObj);
			}

		});


$(".violence_experience").click(function()
		{	
  if (this.checked) {
            $("#rr2").val("0");
				$("#rr2").show();
	autoSave(formObj);
		}
		else
			{
				$("#rr2").hide();
	autoSave(formObj);
			}

		});


$(".others").click(function()
		{	
  if (this.checked) {
            $("#psychosocialriskfactors-others_desc").val("");
				$("#oth").show();
	autoSave(formObj);
		}
		else
			{
				$("#psychosocialriskfactors-others_desc").val("-10");
				$("#oth").hide();
	autoSave(formObj);
			}

		});
		
		
		
		
', \yii\web\View::POS_READY);
?>

<script type="text/javascript">
	var formObj = {'url' : "psychosocial-risk-factors/ajax",'formName' : "formPsychosocialriskfactor","main":true};
</script>
