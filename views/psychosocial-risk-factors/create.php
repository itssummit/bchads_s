<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PsychosocialRiskFactors */

$this->title = 'Psychosocial Risk Factors';
$this->params['breadcrumbs'][] = ['label' => 'Psychosocial Risk Factors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="psychosocial-risk-factors-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
