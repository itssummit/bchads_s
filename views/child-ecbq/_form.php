<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Assessment;
$keyPair = [-15 => '-15: Child Not Co-Operative For Assessment',
		  -14 => '-14: Not Available For Assessment',
		  -11 => '-11: Inadequate Information',
		  -10 => '-10: Not Applicable',
		  -9 => '-9: Missing',
		  -8 => '-8: Refused',
		  -7 => '-7: Partner Accompanied Missing',
		  -6 => '-6: Family Accompanied Missing',
		  -5 => '-5: Missing:Not Asked',
		  -3 => '-3: No Such Relationship/No Partner'];
/* @var $this yii\web\View */
/* @var $model app\models\ChildEcbq */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile('@web/js/scales.js',['depends' => [\yii\web\JqueryAsset::className()]]);


$respondentId = Yii::$app->session->get('respondentId');
$motherId = Yii::$app->session->get('motherId');

        $assessmentId = Yii::$app->session->get('assessmentId');
$pModel = Assessment::findOne(['mother_id' => $motherId , 'id' => $assessmentId]);
?>

<div class="child-ecbq-form">

    <?php $form = ActiveForm::begin(['id' => 'formChildEcbq']); ?>
	<table class="na">
		<thead>
			<tr><th>Is This Scale Applicable ?</th></tr>
			<tr><td><?= $form->field($model, 'not_applicable')->dropDownList(['' =>'','YES' => 'Not Applicable', 'NO' => 'Applicable'],['class' => 'form-control scaleApplicable'])->label(false); ?></td></tr>
		</thead>
	</table>
	
	<div id="idcescale">
	
	<div id="withBoxShadow">
	
	
	
	
	<p><b>ಈ ಕೆಳಕಂಡ ನಿಮ್ಮ ಮಗುವಿನ ನಡವಳಿಕೆಯ ಪ್ರತಿ ವಿವರಣೆಗಳನ್ನು ನೀವು ಓದುತ್ತಿರಬೇಕಾದರೆ, ನಿಮ್ಮ ಮಗುವು ಕಳೆದ ಎರಡು ವಾರದಲ್ಲಿ ಎಷ್ಟು ಬಾರಿ ಹೀಗೆ  ಮಾಡಿದ್ದಾನೆ /ಳೆ ಎಂಬುದನ್ನು ಒಂದು ಅಂಕಿಯ ಸುತ್ತ ವೃತ್ತವನ್ನು ಎಳೆಯುವುದರ ಮೂಲಕ ತಿಳಿಸಿ.
	ಈ ಅಂಕಿಗಳು ನೀವು ಕಳೆದ ಎರಡು ವಾರದಲ್ಲಿ ಎಷ್ಟು ಬಾರಿ ಈ ನಡವಳಿಕೆಗಳನ್ನು ಗಮನಿಸಿದ್ದೀರಾ ಎಂದು ತಿಳಿಸುತ್ತದೆ. </b></p>
	
	
	<p><b>ನಿಮ್ಮ ಮಗುವನ್ನು ಕೆಳಕಂಡ ಸಂದರ್ಭಗಳಲ್ಲಿ ಕಳೆದ ಎರಡು ವಾರಗಳಲ್ಲಿ ಗಮನಿಸದೇ ಇದ್ದಾಗ “ಅನ್ವಯವಾಗುವುದಿಲ್ಲ” ಆಯ್ಕೆಯನ್ನು 
	ಉಪಯೋಗಿಸಲಾಗುವುದು. ಉದಾಹರಣೆಗೆ: ಒಂದು ಸಂದರ್ಭವು ನಿಮ್ಮ ಮಗುವು ವೈದ್ಯರ ಬಳಿಗೆ ಹೋಗುವುದರ ಬಗ್ಗೆ ಇದ್ದಲ್ಲಿ ಮತ್ತು ನಿಮ್ಮ ಮಗುವು ಕಳೆದ ಒಂದು ವಾರದಲ್ಲಿ ವೈದ್ಯರ ಬಳಿ ಹೋಗಿಲ್ಲ ಎಂದಾದಲ್ಲಿ, “ಅನ್ವಯವಾಗುವುದಿಲ್ಲ' ಎನ್ನುವುದಕ್ಕೆ ವೃತ್ತವನ್ನು ಎಳೆಯಿರಿ.
	“ಅನ್ವಯವಾಗುವುದಿಲ್ಲ” ಎನ್ನುವುದು ಯಾವಾಗಲೂ ಇಲ್ಲ” ಎನ್ನುವುದಕ್ಕಿಂತ ಭಿನ್ನವಾಗಿದೆ. ನೀವು ನಿಮ್ಮ ಮಗುವನ್ನು ಕಳೆದ ಎರಡು ವಾರಗಳಲ್ಲಿ ಕೆಳಕಂಡ ಸಂದರ್ಭದಲ್ಲಿ ನೋಡಿರುತ್ತೀರಿ ಆದರೆ ಮಗುವಿನಲ್ಲಿ ಹೇಳಿರುವ/ಉಲ್ಲೇಖಿಸಿರುವ ನಡತೆಯನ್ನು ಗಮನಿಸದೆ ಇದ್ದಾಗ 
	“ಯಾವಾಗಲೂ ಇಲ್ಲ” ಎನ್ನುವುದಕ್ಕೆ ವೃತ್ತವನ್ನು ಎಳೆಯಬೇಕು. ದಯವಿಟ್ಟು ಪ್ರತಿ ಪ್ರಶ್ನೆಗೆ ಉತ್ತರಿಸುವಾಗ ಅಂಕಿಗೆ ಅಥವಾ ಅನ್ವಯವಾಗುವುದಿಲ್ಲ ಎನ್ನುವುದಕ್ಕೆ ವೃತ್ತವನ್ನು ಎಳೆಯುವಾಗ ಖಾತ್ರಿಪಡಿಸಿಕೊಳ್ಳಿ.</b></p>
        <table border="1" class="responsive-scales">
            <thead>
                <tr>
					<th></th>
                    
						<th>ಯಾವಾಗಲೂ ಇಲ್ಲ </th>
						<th>ತುಂಬ ಅಪರೂಪ </th>
						<th>ಅರ್ಧಕ್ಕಿಂತ ಕಡಿಮೆ </th>
						<th>ಅರ್ಧದಷ್ಟು </th>
						<th>ಅರ್ಧಕ್ಕಿಂತ ಜಾಸ್ತಿ </th>
						<th>ಹೆಚ್ಚಾಗಿ ಯಾವಾಗಲೂ </th>
						<th>ಯಾವಾಗಲೂ </th>
						<th>Other Answers</th>
                </tr> 
            </thead>
		<tbody>
			<tr>
			<td><b>ಸಾರ್ವಜನಿಕ ಪ್ರದೇಶದಲ್ಲಿ (ಉದಾ . ಅಂಗಡಿಯಲ್ಲಿ ) ಅಪರಿಚಿತರು ಸಮೀಪಿಸಿದಾಗ ,ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ ಮಗುವು </b><br>
			1)ಪೋಷಕರನ್ನು ಅಂಟಿಕೊಂಡಿದ್ದಾನೆಯೇ ?</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'publicplace_unfamiliarperson')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'publicplace_unfamiliarperson')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			
			</tr>
    
			<tr>
			<td>ಒಂದು ಕೆಲಸವನ್ನು (ಉದಾ . ಕಟ್ಟುವುದು ,ಬಿಡಿಸುವುದು ,ಧರಿಸುವುದು ) ಪೂರ್ತಿಗೊಳಿಸಲು ಕಷ್ಟವಾದರೆ ,ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ ಮಗುವು <br>
			2)ಬೇಗನೆ ಕಿರಿಕಿರಿಯಾಗಿದ್ದಾನೆ </td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'work_incomplete')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'work_incomplete')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>ನಿಮ್ಮ ಮನೆಗೆ ಪರಿಚಿತ ಮಗುವು ಬಂದಾಗ  ,ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ ಮಗುವು<br>
			3)ಮಗುವಿನ ಜೊತೆ ಬೆರೆತಿದ್ದಾನೆಯೇ ?</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'familiar_baby')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'familiar_baby')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>ವಿವಿಧ ಚಟುವಟಿಕೆಗಳನ್ನು ಕೊಟ್ಟಾಗ ,ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ ಮಗುವು<br>
			4)ಏನು ಮಾಡಬೇಕೆಂದು ಕೂಡಲೇ ನಿರ್ಧರಿಸಿ ಅದನ್ನೇ  ಮಾಡಿದ್ದಾನೆಯೇ? </td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'other_activities')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'other_activities')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>ನಿಮ್ಮ ಮತ್ತು ನಿಮ್ಮ ಮಗುವಿನ ಜೊತೆಗಿನ ಶಾಂತವಾದ ದಿನ  ಅಥವಾ ಸಂಜೆ ಸಮಯದಲ್ಲಿ ,ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ ಮಗುವು<br>
			5)ಮೆಲುವಾಗಿ ಹಾಡಿದಾಗ ಖುಷಿಪಟ್ಟಿದ್ದಾನೆಯೇ ?</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'happy_sing')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'happy_sing')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>ಹೊರಗಡೆ ಆಡುತ್ತಿರುವಾಗ ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ ಮಗುವು <br>
			6)ತನಗೆ ಈ ಆಟ ಆ ಆಟ  ಆಡಬೇಕೆಂದು ಉತ್ಸಾಹದಿಂದ ಹೋಗಿದ್ದಾನೆಯೇ ?</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'play_outside')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'play_outside')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>ಅವನ/ಳ ಮೆಚ್ಚಿನ ಆಟಿಕೆಗಳಲ್ಲಿ ತೊಡಗಿಸಿದಾಗ, ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮಮಗುವು <br>
			7)೧೦ ನಿಮಿಷಕ್ಕಿಂತ ಜಾಸ್ತಿ ಹೊತ್ತು ಆಟವಾಡಿದ್ದಾನೆಯೇ ?</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'toys1')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'toys1')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>ಅವನ/ಳ ಮೆಚ್ಚಿನ ಆಟಿಕೆಗಳಲ್ಲಿ ತೊಡಗಿಸಿದಾಗ, ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮಮಗುವು <br>
			8)ನಿಮ್ಮ ಪ್ರಶ್ನೆಗಳಿಗೆ ಅಥವಾ ಹೇಳಿಕೆಗಳಿಗೆ ಪ್ರತಿಕ್ರಿಯಿಸುತ್ತಾ ಆಟವಾಡುವುದನ್ನು ಮುಂದುವರಿಸಿದ್ದಾನೆಯೇ?</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'toys2')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'toys2')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>9)ಇಷ್ಟವಾಗಿರುವ ದೊಡ್ಡವರು ಬರುತ್ತಾರೆ ಎಂದಾಗ , ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ ಮಗುವು<br>
			ತು೦ಬಾ ಉತ್ಸುಕವಾಗಿದ್ದಾನೆಯೇ?</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'like_elders')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'like_elders')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>ಸುಮ್ಮನೆ ಕುಳಿತು ಮಾಡುವ ಚಟುವಟಿಕೆಗಳನ್ನು ಮಾಡುವಾಗ, ಉದಾ: ಕಥೆ ಪುಸ್ತಕವನ್ನು ಓದುವುದು, ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ ಮಗುವು <br>
			10)ಅವನ/ಳ ಕೂದಲು ಅಥವಾ ಬಟ್ಟೆಯನ್ನು  (ಚಡಪಡಿಕೆಯಿಂದ ) ಎಳೆದಿದ್ದಾನೆಯೇ?</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'simple_activity')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'simple_activity')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</th>
			</td>

			<tr>
			<td>ಮನೆ  ಒಳಗಡೆ ಆಡುವಾಗ, ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮಮಗುವು <br>
			11)ಒರಟು ಅಥವಾ ರೌಡಿ ಆಡಲು ಇಷ್ಟ ಪಡುತ್ತಾನೆಯೇ ?
			</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'play_inside')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'play_inside')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>ಮೆತ್ತಗೆ ತೂಗಿದಾಗ ಅಥವಾ ತಬ್ಬಿಕೊಂಡಾಗ, ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ ಮಗುವು<br>
			12)ಬಿಡಿಸಿಕೊಳ್ಳಲು  ಕಾತುರವಾಗಿದ್ದಾನೆಯೇ ?</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'hug')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'hug')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>ಹೊಸ ಚಟುವಟಿಕೆಯನ್ನು ಮಾಡಬೇಕಾದಾಗ, ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ ಮಗುವು<br>
			13)ಕೂಡಲೇ ಅದನ್ನು  ಮಾಡಿದ್ದಾನೆಯೇ ?</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'new_activity')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'new_activity')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>ಏಕಾಗ್ರತೆ ಬೇಕಾಗಿರುವ ಚಟುವಟಿಕೆಗಳಲ್ಲಿ ತೊಡಗಿರುವಾಗ, ಉದಾ: ಬ್ಲಾಕ್  ಗಳನ್ನು  ಕಟ್ಟುವುದು, ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ  ಮಗುವು<br>
			14)ಚಟುವಟಿಕೆಯಿಂದ ಕೂಡಲೇ ಆಸಕ್ತಿ ಕಳೆದುಕೊಂಡಿದ್ದಾನೆಯೇ?</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'concentrate')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'concentrate')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>
			
			
			
			
			
			
			
			
			
			<tr>
			<td><b>ದಿನನಿತ್ಯದ ಚಟುವಟಿಕೆಗಳಲ್ಲಿ , ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ ಮಗುವು </b><br>
			15)ನೀವು ಅವನ /ಳನ್ನು  ಕರೆದ ಕೂಡಲೇ ನಿಮಗೆ  ಗಮನ ಕೊಟ್ಟಿದ್ದಾನೆಯೇ ?</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'attention')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'attention')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>
			
			
			<tr>
			<td><b>ದಿನನಿತ್ಯದ ಚಟುವಟಿಕೆಗಳಲ್ಲಿ , ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ ಮಗುವು </b><br>
			16)ಅವನ /ಳ  ಬಟ್ಟೆಯ ದಾರದಿಂದ /ಕುಣಿಕೆಯಿಂದ ಕಿರಿಕಿರಿಯಾದಂತೆ ಕಂಡಿದ್ದಾನೆಯೇ ?</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'kirikiri_clothes')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'kirikiri_clothes')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>
			
			
			
			<tr>
			<td><b>ದಿನನಿತ್ಯದ ಚಟುವಟಿಕೆಗಳಲ್ಲಿ , ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ ಮಗುವು </b><br>
			17)ಗದ್ದಲದ ವಾತಾವರಣದಲ್ಲಿನ ಶಬ್ದಗಳಿಂದ ಕಿರಿಕಿರಿಯಾಗಿದ್ದಾನೆಯೇ ?</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'kirikiri_sounds')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'kirikiri_sounds')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>
			
			
			<tr>
			<td><b>ದಿನನಿತ್ಯದ ಚಟುವಟಿಕೆಗಳಲ್ಲಿ , ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ ಮಗುವು </b><br>
			18)ಸಂಜೆಯಾದರೂ ಸಂಪೂರ್ಣ ಚೈತನ್ಯದಿಂದ ಇದ್ದಂತೆ ಕಂಡಿದ್ದಾನೆಯೇ ?</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'active_sunset')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'active_sunset')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>
			
			
			

			<tr>
			<td>ಸಾರ್ವಜನಿಕ ಪ್ರದೇಶದಲ್ಲಿದ್ದಾಗ, ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ ಮಗುವು <br>
			19)ಜೋರಾಗಿ ಶಬ್ದ  ಮಾಡುವ ವಾಹನಗಳಿಂದ ಭಯಪಟ್ಟಿದ್ದಾನೆಯೇ?</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'public_place')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'public_place')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>ಬೇರೆ ಮಕ್ಕಳೊಂದಿಗೆ ಹೊರಗಡೆ ಆಡುತ್ತಿರುವಾಗ, ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ ಮಗು<br>
			20)ತುಂಬಾ ಚೂಟಿಯಾಗಿರುವ ಮಕ್ಕಳಲ್ಲೊಬ್ಬ ಅನ್ನಿಸಿದೆಯೇ?</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'play_out')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'play_out')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>ಬೇಡ ಎಂದಾಗ, ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ ಮಗುವು<br>
			21)ಮಾಡಬೇಡ ಎಂದಿರುವ ಚಟುವಟಿಕೆಗಳನ್ನು ಮಾಡುವುದನ್ನು ನಿಲ್ಲಿಸಿದ್ದಾನೆಯೇ?</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'stop_work')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'stop_work')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>ಬೇಡ ಎಂದಾಗ, ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ ಮಗುವು<br>
			22)ಬೇಜಾರಿನಿಂದ ಅತ್ತಿದ್ದಾನೆಯೇ?</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'sad_cry')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<th>
			<?= $form->field($model, 'sad_cry')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</th>
			</tr>

			<tr>
			<td>ಉತ್ತೇಜನಕಾರಿ ಚಟುವಟಿಕೆ ಅಥವಾ ಘಟನೆಯ ನಂತರ, ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ ಮಗು<br>
			23)ಬೇಜಾರಾಗಿರುವಂತೆ ಕಂಡಿದ್ದಾನೆಯೇ ?</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'sad_look')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'sad_look')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td> ಮನೆ ಒಳಗಡೆ ಆಟವಾಡುವಾಗ, ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ ಮಗುವು<br>
			24)ಮನೆ  ತುಂಬ  ಓಡಾಡಿದ್ದಾನೆಯೇ ?</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'walk_in_home')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'walk_in_home')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>ಉತ್ಸಾಹಕಾರಿ ಘಟನೆಯ ಮೊದಲು ಉದಾ: ಹೊಸ ಆಟಿಕೆಯನ್ನು ತೆಗೆದುಕೊಳ್ಳುವುದು, ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ ಮಗು<br>
			25)ಅದನ್ನು ತೆಗೆದುಕೊಳ್ಳಲು  ಉತ್ಸಾಹಕನಾಗಿದ್ದಾನೆಯೇ ?</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'new_toys')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'new_toys')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>ಅವನು/ಳು ಏನನ್ನಾದರೂ   ಕೇಳಿದಾಗ ಮತ್ತು ನೀವು  ಅದಕ್ಕೆ  ಇಲ್ಲ  ಎಂದಾಗ, ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ ಮಗುವು<br>
			26)ರಂಪಾಟ ಮಾಡಿದ್ದಾನೆಯೇ?</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'sad_on_no')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'sad_on_no')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>ಇಷ್ಟವಾದ ವಸ್ತುವನ್ನು ತೆಗೆದುಕೊಳ್ಳಲು ಕಾಯಲು ಹೇಳಿದಾಗ  (ಉದಾ: ಐಸ್ ಕ್ರೀಮ್), ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ ಮಗುವು <br>
			27)ತಾಳ್ಮೆ ಯಿಂದ  ಕಾದಿದ್ದಾನೆಯೇ ?</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'patience')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'patience')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>ನಿಧಾನಕ್ಕೆ ತೂಗಿದಾಗ, ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ ಮಗುವು <br>
			28)ನಕ್ಕಿದ್ದಾನೆಯೇ  ?</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'slow_smile')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'slow_smile')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>ನಿಮ್ಮ  ತೊಡೆಯ ಮೇಲೆ ಕೂಡಿಸಿಕೊಂಡಾಗ ,ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ ಮಗುವು <br>
			29)ನಿಮಗೆ ಒರಗಿಕೊಂಡಿದ್ದಾನೆಯೇ?</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'lap_sleep')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'lap_sleep')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>ಪರಿಚಿತ ವ್ಯಕ್ತಿಯು ಮನೆಗೆ ಬಂದಾಗ, ಉದಾ: ನೆಂಟರು ಅಥವಾ ಸ್ನೇಹಿತರು, ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ ಮಗುವು <br>
			30)ಆ ವ್ಯಕ್ತಿಯೊಂದಿಗೆ ಮಾತನಾಡಲು  ಬಯಸಿದ್ದಾನೆಯೇ  ? </td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'talks_familiar')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'talks_familiar')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>ಏನನ್ನಾದರೂ  ಮಾಡಲು ಹೇಳಿದಾಗ, ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ  ಮಗನಿಗೆ  <br>
			31)ಮುರಿಯಬಹುದಾದ ವಸ್ತುವಿನೊಂದಿಗೆ ಜಾಗ್ರತೆಯಿಂದ ಇರಲು ಸಾಧ್ಯವಾಯಿತೇ?</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'breakable_things')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'breakable_things')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>ಹೊಸ ಸ್ಥಳಕ್ಕೆ ಭೇಟಿ ಕೊಟ್ಟಾಗ , ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ ಮಗುವು<br>
			32)ಒಳಗೆ ಹೋಗಲು ಇಷ್ಟಪಡಲಿಲ್ಲ ?</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'dint_go_inside')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'dint_go_inside')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>ಅವನಿ/ಳಿಗೆ ಅಸಮಾಧಾನವಾದಾಗ, ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ  ಮಗುವು<br>
			33)ಸಮಾಧಾನ ಮಾಡುತ್ತಿದ್ದರೂ, 3 ನಿಮಿಷಕ್ಕಿಂತಲೂ ಜಾಸ್ತಿ  ಅತ್ತಿದ್ದಾನೆಯೇ ?</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'cry_morethan_3min')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'cry_morethan_3min')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>ಅವನಿ/ಳಿಗೆ ಅಸಮಾಧಾನವಾದಾಗ, ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ  ಮಗುವು<br>
			34)ಬೇಗನೆ ಸಮಾಧಾನ ಮಾಡಲು ಸಾಧ್ಯವಾಯಿತೇ?</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'fast_samadhana')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'fast_samadhana')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>ನಿಮಗೆ ಬಿಡುವಿಲ್ಲದಿದ್ದಾಗ  ಎಷ್ಟು ಬಾರಿ ನಿಮ್ಮ ಮಗುವು <br>
			35)ನೀವು ಹೇಳಿದಾಗ ಬೇರೆ ಚಟುವಟಿಕೆಯನ್ನು  ಹುಡುಕಿಕೊಂಡು ಮಾಡಿದ್ದಾನೆಯೇ?!</td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'looks_other_activity')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'looks_other_activity')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>ಪರಿಚಿತ ವ್ಯಕ್ತಿಗಳ ಅಥವಾ ಮಕ್ಕಳ ಗು೦ಪಿನೊಂದಿಗೆ ಇರುವಾಗ, ಎಷ್ಟು ಬಾರಿ  ನಿಮ್ಮ ಮಗುವು <br>
			36)ಬೇರೆ ಬೇರೆ ಜನರೊಂದಿಗೆ ಆಡುತ್ತಾ ಖುಷಿಯಾಗಿದ್ದಿರುವುದು ? </td>
			<?php for($i = 1;$i < 8;$i++):?>
			<td><?= $form->field($model, 'plays_withdiff_people')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			
			<td>
			<?= $form->field($model, 'plays_withdiff_people')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>
			
			<tr>
			<td></td>
			<td id="scaleflag" colspan="8" class="hidden"><?= $form->field($model, 'score')->input('number',['readonly'=>true])->label(false);?></td>
			</tr>
		</tbody>
	</table>
	</div>
	</div>	
	<?php ActiveForm::end(); ?>

</div>

<?php $this->registerJs('
	$("#"+formObj.formName).one("change",function()
	{
		autoSaveFormObj = formObj;
	});
',\yii\web\View::POS_READY)?>
<?php if($model->not_applicable == 'YES'){
	$this->registerJs('
	setNotApplicableOnLoad("#withBoxShadow","#sharedcaregiving-not_applicable");
	', \yii\web\View::POS_READY);

}


	$this->registerJs('

	$("#childecbq-not_applicable").change(function()
		{	
			var value = $(this).val();
			if(value=="YES")
			{	
				setNotApplicableYes("#withBoxShadow","#childecbq-not_applicable");
			}
			else
			{
				setNotApplicableNo("#withBoxShadow","#childecbq-not_applicable");
			}
		});
			
', \yii\web\View::POS_READY);
?>

<?php if($pModel->is_there_acg == 0 && $pModel->is_there_acg != null && $respondentId == 3){
	$this->registerJs('
	$(":input").val("-3");
		autoSaveFormObj = formObj;
		console.log("There is no ACG to enter");
	', \yii\web\View::POS_READY);

}?>
<script type="text/javascript">
	formObj = {'url' : "child-ecbq/ajax",'formName' : "formChildEcbq"};
		var calc = function () {
        var totalValue = 0;
        var root= 'childecbq';
		var id = '#'+root+'-';
		var clas = '.field-'+root+'-';
        var arr = ['publicplace_unfamiliarperson', 'work_incomplete', 'familiar_baby', 'other_activities', 
			'happy_sing', 'play_outside', 'toys1', 'toys2', 'like_elders', 'simple_activity', 'play_inside', 'hug', 'new_activity', 
			'concentrate', 'public_place', 'play_out', 'stop_work', 'sad_cry', 'sad_look', 'walk_in_home', 'new_toys', 'sad_on_no', 
			'patience', 'slow_smile', 'lap_sleep', 'talks_familiar', 'breakable_things', 'dint_go_inside', 'cry_morethan_3min', 
			'fast_samadhana', 'looks_other_activity', 'plays_withdiff_people','attention','kirikiri_clothes','kirikiri_sounds','active_sunset'];
        
        for (var i in arr) {
            var val = $(clas + arr[i] + ' input[type=radio]:checked').val();
			console.log($(clas + arr[i] + ' input[type=radio]:checked').val());
			if (parseInt(val)>=0) {
                totalValue += parseInt(val);
            }
        }
		

        $(id+'score').val(totalValue)
		autoSaveObj = formObj;
    }

</script>
<style>
.na{
	height:90px !important;
}
</style>
