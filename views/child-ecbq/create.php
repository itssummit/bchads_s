<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ChildEcbq */

$this->title = 'ChildEcbq';
$this->params['breadcrumbs'][] = ['label' => 'Child Ecbqs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="child-ecbq-create">

    <h1>ಪೂರ್ವ ಬಾಲ್ಯಾವಧಿಯ ವರ್ತನಾ ಪ್ರಶ್ನಾವಳಿ </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
