﻿<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Assessment;
$keyPair = [-15 => '-15: Child Not Co-Operative For Assessment',
		  -14 => '-14: Not Available For Assessment',
		  -11 => '-11: Inadequate Information',
		  -10 => '-10: Not Applicable',
		  -9 => '-9: Missing',
		  -8 => '-8: Refused',
		  -7 => '-7: Partner Accompanied Missing',
		  -6 => '-6: Family Accompanied Missing',
		  -5 => '-5: Missing:Not Asked',
		  -3 => '-3: No Such Relationship/No Partner'];
$carer = ['Myself'=>'Myself',
			'Husband'=>'Husband',
			'Maternal Grandmother'=>'Maternal Grandmother',
			'Maternal Grandfather'=>'Maternal Grandfather',
			'Paternal Grandfather'=>'Paternal Grandfather',
			'Paternal Grandmother'=>'Paternal Grandmother',
			'Siblings Of The Child'=>'Siblings Of The Child'];		  
/* @var $this yii\web\View */
/* @var $model app\models\SharedCaregiving */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile('@web/js/scalesshare.js',['depends' => [\yii\web\JqueryAsset::className()]]);

$motherId = Yii::$app->session->get('motherId');

        $assessmentId = Yii::$app->session->get('assessmentId');
?>
<style>
#sharedcaregiving-not_applicable,#sharedcaregiving-care_myself ,#sharedcaregiving-care_husband,#sharedcaregiving-care_maternal_grandmother{
	width:200px !important;
}
</style>
<?php 
$pModel = Assessment::findOne(['mother_id' => $motherId , 'id' => $assessmentId]);
	if( $pModel->is_there_acg == 0 && $pModel->is_there_acg != null)
	{
		//echo "<script type='text/javascript'> alert('$pModel->is_there_acg')</script>";
		 $model->songs_acg='-3';
		  $model->played_acg='-3';
		  $model->taken_out_acg='-3';
		  $model->introduced_newthings_acg='-3';
		  $model->told_not_todo_something_acg='-3';
		  $model->cleaned_acg='-3';
		  $model->rocked_acg='-3';
		  $model->carried_acg='-3';
		  $model->scolded_acg='-3';
		  $model->wasnt_too_hot_acg='-3';
		  $model->care_sick_acg='-3';
		  $model->taken_harm_acg='-3';
		  $model->fed_acg='-3';
		  $model->kissed_acg='-3';
		  $model->cooed_acg='-3';
		  $model->put_sleep_acg='-3';
		  $model->bathed_acg='-3';
		  $model->didnt_eat_harmful_acg='-3';
		  $model->soothed_acg='-3';
		  $model->massaged_acg='-3';
		  $model->showed_what_todo_acg='-3';
		  $model->thoogiddene_acg='-3';
		  $model->cuddled_crying_acg='-3';
		  $model->dressed_acg='-3';
		  $model->told_off_acg='-3';
		  $model->comfortable_acg='-3';
		  $model->slept_beside_acg='-3';
		  $model->introduced_new_people_acg='-3';
		  $model->scolded_crying_acg='-3';
		  $model->cradled_crying_acg='-3';
		  $model->made_warm_acg='-3';
		  $model->gone_wokeup_acg='-3';
		  $model->prepared_formula_milk_acg='-3';
		  $model->talked_acg='-3';
		  $model->cuddled_acg='-3';
		  $model->sung_acg='-3';
		  $model->taken_nurse_acg='-3';
		  $model->talked_sternly_acg='-3';
		  $model->held_acg='-3';
		  $model->shared_smiles_acg='-3';
		  $model->distracted_crying_acg='-3';
		  $model->showed_rightthing_acg='-3';
		  $model->taken_visit_family_acg='-3';
		  $model->cradled_arms_acg='-3';
		  $model->fed_crying_acg='-3';
		  $model->prayed_acg='-3';
		  $model->invovled_family_rituals_acg='-3';

		  $model->songs_acg_lastweek='-3';
		  $model->played_acg_lastweek='-3';
		  $model->taken_out_acg_lastweek='-3';
		  $model->introduced_newthings_acg_lastweek='-3';
		  $model->told_not_todo_something_acg_lastweek='-3';
		  $model->cleaned_acg_lastweek='-3';
		  $model->rocked_acg_lastweek='-3';
		  $model->carried_acg_lastweek='-3';
		  $model->scolded_acg_lastweek='-3';
		  $model->wasnt_too_hot_acg_lastweek='-3';
		  $model->care_sick_acg_lastweek='-3';
		  $model->taken_harm_acg_lastweek='-3';
		  $model->fed_acg_lastweek='-3';
		  $model->kissed_acg_lastweek='-3';
		  $model->cooed_acg_lastweek='-3';
		  $model->put_sleep_acg_lastweek='-3';
		  $model->bathed_acg_lastweek='-3';
		  $model->didnt_eat_harmful_acg_lastweek='-3';
		  $model->soothed_acg_lastweek='-3';
		  $model->massaged_acg_lastweek='-3';
		  $model->showed_what_todo_acg_lastweek='-3';
		  $model->thoogiddene_acg_lastweek='-3';
		  $model->cuddled_crying_acg_lastweek='-3';
		  $model->dressed_acg_lastweek='-3';
		  $model->told_off_acg_lastweek='-3';
		  $model->comfortable_acg_lastweek='-3';
		  $model->slept_beside_acg_lastweek='-3';
		  $model->introduced_new_people_acg_lastweek='-3';
		  $model->scolded_crying_acg_lastweek='-3';
		  $model->cradled_crying_acg_lastweek='-3';
		  $model->made_warm_acg_lastweek='-3';
		  $model->gone_wokeup_acg_lastweek='-3';
		  $model->prepared_formula_milk_acg_lastweek='-3';
		  $model->talked_acg_lastweek='-3';
		  $model->cuddled_acg_lastweek='-3';
		  $model->sung_acg_lastweek='-3';
		  $model->taken_nurse_acg_lastweek='-3';
		  $model->talked_sternly_acg_lastweek='-3';
		  $model->held_acg_lastweek='-3';
		  $model->shared_smiles_acg_lastweek='-3';
		  $model->distracted_crying_acg_lastweek='-3';
		  $model->showed_rightthing_acg_lastweek='-3';
		  $model->taken_visit_family_acg_lastweek='-3';
		  $model->cradled_arms_acg_lastweek='-3';
		  $model->fed_crying_acg_lastweek='-3';
		  $model->prayed_acg_lastweek='-3';
		   $model->invovled_family_rituals_acg_lastweek='-3';
		   $model->other_activity_acg='-3';
		   $model->any_others='-3';

	}
	/*else
	{
		//echo "<script type='text/javascript'> alert('$pModel->is_there_acg')</script>";
		  $model->songs_acg='':$model->songs_acg;
		  $model->played_acg='':$model->played_acg;
		  $model->taken_out_acg='':$model->taken_out_acg;
		  $model->introduced_newthings_acg='':$model->introduced_newthings_acg;
		  $model->told_not_todo_something_acg='':$model->told_not_todo_something_acg;
		  $model->cleaned_acg='':$model->cleaned_acg;
		  $model->rocked_acg='':$model->rocked_acg;
		  $model->carried_acg='':$model->carried_acg;
		  $model->scolded_acg='':$model->scolded_acg;
		  $model->wasnt_too_hot_acg='':$model->wasnt_too_hot_acg;
		  $model->care_sick_acg='':$model->care_sick_acg;
		  $model->taken_harm_acg='':$model->taken_harm_acg;
		  $model->fed_acg='':$model->fed_acg;
		  $model->kissed_acg='':$model->kissed_acg;
		  $model->cooed_acg='':$model->cooed_acg;
		  $model->put_sleep_acg='':$model->put_sleep_acg;
		  $model->bathed_acg='':$model->bathed_acg;
		  $model->didnt_eat_harmful_acg='':$model->didnt_eat_harmful_acg;
		  $model->soothed_acg='':$model->soothed_acg;
		  $model->massaged_acg='':$model->massaged_acg;
		  $model->showed_what_todo_acg='':$model->showed_what_todo_acg;
		  $model->thoogiddene_acg='':$model->thoogiddene_acg;
		  $model->cuddled_crying_acg='':$model->cuddled_crying_acg;
		  $model->dressed_acg='':$model->dressed_acg;
		  $model->told_off_acg='':$model->told_off_acg;
		  $model->comfortable_acg='':$model->comfortable_acg;
		  $model->slept_beside_acg='':$model->slept_beside_acg;
		  $model->introduced_new_people_acg='':$model->introduced_new_people_acg;
		  $model->scolded_crying_acg='':$model->scolded_crying_acg;
		  $model->cradled_crying_acg='':$model->cradled_crying_acg;
		  $model->made_warm_acg='':$model->made_warm_acg;
		  $model->gone_wokeup_acg='':$model->gone_wokeup_acg;
		  $model->prepared_formula_milk_acg='':$model->prepared_formula_milk_acg;
		  $model->talked_acg='':$model->talked_acg;
		  $model->cuddled_acg='':$model->cuddled_acg;
		  $model->sung_acg='':$model->sung_acg;
		  $model->taken_nurse_acg='':$model->taken_nurse_acg;
		  $model->talked_sternly_acg='':$model->talked_sternly_acg;
		  $model->held_acg='':$model->held_acg;
		  $model->shared_smiles_acg='':$model->shared_smiles_acg;
		  $model->distracted_crying_acg='':$model->distracted_crying_acg;
		  $model->showed_rightthing_acg='':$model->showed_rightthing_acg;
		  $model->taken_visit_family_acg='':$model->taken_visit_family_acg;
		  $model->cradled_arms_acg='':$model->cradled_arms_acg;
		  $model->fed_crying_acg='':$model->fed_crying_acg;
		  $model->prayed_acg='':$model->prayed_acg;
		  $model->invovled_family_rituals_acg='':$model->invovled_family_rituals_acg;

		  $model->songs_acg_lastweek='':$model->songs_acg_lastweek;
		  $model->played_acg_lastweek='':$model->played_acg_lastweek;
		  $model->taken_out_acg_lastweek='':$model->taken_out_acg_lastweek;
		  $model->introduced_newthings_acg_lastweek='':$model->introduced_newthings_acg_lastweek;
		  $model->told_not_todo_something_acg_lastweek='':$model->told_not_todo_something_acg_lastweek;
		  $model->cleaned_acg_lastweek='':$model->cleaned_acg_lastweek;
		  $model->rocked_acg_lastweek='':$model->rocked_acg_lastweek;
		  $model->carried_acg_lastweek='':$model->carried_acg_lastweek;
		  $model->scolded_acg_lastweek='':$model->scolded_acg_lastweek;
		  $model->wasnt_too_hot_acg_lastweek='':$model->wasnt_too_hot_acg_lastweek;
		  $model->care_sick_acg_lastweek='':$model->care_sick_acg_lastweek;
		  $model->taken_harm_acg_lastweek='':$model->taken_harm_acg_lastweek;
		  $model->fed_acg_lastweek='':$model->fed_acg_lastweek;
		  $model->kissed_acg_lastweek='':$model->kissed_acg_lastweek;
		  $model->cooed_acg_lastweek='':$model->cooed_acg_lastweek;
		  $model->put_sleep_acg_lastweek='':$model->put_sleep_acg_lastweek;
		  $model->bathed_acg_lastweek='':$model->bathed_acg_lastweek;
		  $model->didnt_eat_harmful_acg_lastweek='':$model->didnt_eat_harmful_acg_lastweek;
		  $model->soothed_acg_lastweek='':$model->soothed_acg_lastweek;
		  $model->massaged_acg_lastweek='':$model->massaged_acg_lastweek;
		  $model->showed_what_todo_acg_lastweek='':$model->showed_what_todo_acg_lastweek;
		  $model->thoogiddene_acg_lastweek='':$model->thoogiddene_acg_lastweek;
		  $model->cuddled_crying_acg_lastweek='':$model->cuddled_crying_acg_lastweek;
		  $model->dressed_acg_lastweek='':$model->dressed_acg_lastweek;
		  $model->told_off_acg_lastweek='':$model->told_off_acg_lastweek;
		  $model->comfortable_acg_lastweek='':$model->comfortable_acg_lastweek;
		  $model->slept_beside_acg_lastweek='':$model->slept_beside_acg_lastweek;
		  $model->introduced_new_people_acg_lastweek='':$model->introduced_new_people_acg_lastweek;
		  $model->scolded_crying_acg_lastweek='':$model->scolded_crying_acg_lastweek;
		  $model->cradled_crying_acg_lastweek='':$model->cradled_crying_acg_lastweek;
		  $model->made_warm_acg_lastweek='':$model->made_warm_acg_lastweek;
		  $model->gone_wokeup_acg_lastweek='':$model->gone_wokeup_acg_lastweek;
		  $model->prepared_formula_milk_acg_lastweek='':$model->prepared_formula_milk_acg_lastweek;
		  $model->talked_acg_lastweek='':$model->talked_acg_lastweek;
		  $model->cuddled_acg_lastweek='':$model->cuddled_acg_lastweek;
		  $model->sung_acg_lastweek='':$model->sung_acg_lastweek;
		  $model->taken_nurse_acg_lastweek='':$model->taken_nurse_acg_lastweek;
		  $model->talked_sternly_acg_lastweek='':$model->talked_sternly_acg_lastweek;
		  $model->held_acg_lastweek='':$model->held_acg_lastweek;
		  $model->shared_smiles_acg_lastweek='':$model->shared_smiles_acg_lastweek;
		  $model->distracted_crying_acg_lastweek='':$model->distracted_crying_acg_lastweek;
		  $model->showed_rightthing_acg_lastweek='':$model->showed_rightthing_acg_lastweek;
		  $model->taken_visit_family_acg_lastweek='':$model->taken_visit_family_acg_lastweek;
		  $model->cradled_arms_acg_lastweek='':$model->cradled_arms_acg_lastweek;
		  $model->fed_crying_acg_lastweek='':$model->fed_crying_acg_lastweek;
		  $model->prayed_acg_lastweek='':$model->prayed_acg_lastweek;
		   $model->invovled_family_rituals_acg_lastweek='':$model->invovled_family_rituals_acg_lastweek;

	}*/
	?>
<div class="shared-caregiving-form">
    <?php $form = ActiveForm::begin(['id' => 'formSharedcaregiving']); ?>
	<div id="withBoxShadow" class="col-lg-12 col-md-12 col-lg-12 col-xs-12">
	<div class="row">
	<div id="notapp" class="col-lg-4 pull-right">
	<table>
	<tbody>
		<tr>
		<td><h4><b>Is This Scale Applicable ?</b></h4><?php echo $form->field($model, 'not_applicable')->dropDownList(['' =>'','YES' => 'Not Applicable', 'NO' => 'Applicable'])->label(false); ?>
		</td>
		</tr>
	</tbody>	
	</table>	
	</div>
	</div>
	<h4><b>Number of family members living in the home:</b></h4>
	<div class="row">
		
		<div class="col-xs-6 col-md-6 col-lg-6 col-sm-6">
		
		<?= $form->field($model, 'number_adults')->textInput() ?>
		</div>
		<div class="col-xs-6 col-md-6 col-lg-6 col-sm-6">
		<?= $form->field($model, 'number_infants')->textInput() ?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6 col-md-6 col-lg-6 col-sm-6">
		<?= $form->field($model, 'number_children_preschool')->textInput() ?>
		</div>
		<div class="col-xs-6 col-md-6 col-lg-6 col-sm-6">
		<?= $form->field($model, 'number_children')->textInput() ?>
		</div>
	</div>	
	<div class="row">	
		<div class="col-xs-6 col-md-6 col-lg-6 col-sm-6">
		<?= $form->field($model, 'number_adolescents')->textInput() ?>
		</div>
	</div>
   
   
	<h4>Type of family</h4>
    <?= $form->field($model, 'family_type')->radioList([0=>'Nuclear Family',1=>'Joint Family',3=>'Extended Family'],['itemOptions'=>['onChange' => "calc()"]])->label(false);?>

	

	<h4>current stay of mother and infant:</h4>
    <?= $form->field($model, 'current_stay')->radioList([0=>'Parent`s Home' ,1=>'Husband`s Home',3=>'Others'],['itemOptions'=>['onChange' => "calc()"]])->label(false);?>
	
	<div id="wards" style = "display:<?php echo ($model->current_stay == 3)?"display":"none";?>">
	<label>specify if Others</label>
	<?= $form->field($model, 'current_stay_others')->textInput()->label(false); ?>
	</div>
	<div id = "scsclae">
	<caption><b>In our culture ,many persons/relative/friends are involved in the care of the child .We want to know who are the main carers for this child .Rank them in the order of contribution to the care of the this child .First three people in the order of maximum(1) to the minimum(3).</b></caption>
    <table border='1' class="responsive-sclaes" width="100%;">
	<thead>
			<tr>
				<th>First Carer</th>
				<th>Second Carer</th>
				<th>Third Carer</th>
			</tr>
    </thead>
	<tbody>
	
			<tr>
				<td><?= $form->field($model, 'care_myself')->dropDownList($carer,['prompt' => 'Select'])->label(false)?></td>
				<td><?= $form->field($model, 'care_husband')->dropDownList($carer,['prompt' => 'Select'])->label(false)?></td>
				<td><?= $form->field($model, 'care_maternal_grandmother')->dropDownList($carer,['prompt' => 'Select'])->label(false)?></td>
			</tr>
	
	</tbody>
	</table>
	</div>
	<div id ="sharetable">
    
			<br>
			<label><b>Any other: (Please specify and the relationship with the mother)</b></label>
			<?= $form->field($model, 'any_others')->textInput()->label(false); ?>
			<br>
			<div class="container-fluid">
	<div class="row">
<div class="col-sm-4 col-md-4 col-lg-4">
	<button id="yes" class="btn btn-primary">Make all YES</button>
</div>
<div class="col-sm-4 col-md-4 col-lg-4" style = "display: <?php echo($pModel->is_there_acg ==0 && $pModel->is_there_acg != null)?"none":"display"; ?> ">
	<button id="yes1" class="btn btn-primary">Make all YES</button>
</div>
<div class="col-sm-4 col-md-4 col-lg-4" style = "display: <?php echo($pModel->is_there_acg == 0 && $pModel->is_there_acg != null)?"none":"display"; ?> ">
	<button id="yes2" class="btn btn-primary">Make all YES</button>
</div>
</div>
</div>

<table border='1' class="responsive-scales">
			<thead>
                <tr>
                    <th></th>
                    <th>ಕಳೆದ ಒಂದು ವಾರದಲ್ಲಿ ನಾನು </th>
					<th>Other Answers</th>
					
                    <th>ಕಳೆದ ಒಂದು ವಾರದಲ್ಲಿ ACG:</th>
					<th>Other Answers</th>
					
					<th>ACG:ಕಳೆದ ಒಂದು ವಾರದಲ್ಲಿ ನಾನು </th>
					<th>Other Answers</th>
                </tr> 
			</thead>
			<tbody>
				<td>1)ಅವನಿಗೆ ಹಾಡು ಹಾಡಿದ್ದೇನೆ </td>
				
				<td>
				<?= $form->field($model, 'songs')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No','id' => 'c']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'songs')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'songs_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'songs_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'songs_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				<?= $form->field($model, 'songs_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				</td>
				
				</tr>
			
				<tr>
				<td>2)ಅವನೊಂದಿಗೆ ಆಟ ಆಡಿದ್ದೇನೆ </td>
				
				<td>
				<?= $form->field($model, 'played')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'played')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'played_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'played_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'played_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				<?= $form->field($model, 'played_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				</td>
				
				
				
				</tr>
				
				<tr>
				<td>3)ಅವನನ್ನು ಹೊರಗಡೆ ಕರೆದುಕೊಂಡು ಹೋಗಿದ್ದೇನೆ</td>
				<td>
				<?= $form->field($model, 'taken_out')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'taken_out')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'taken_out_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'taken_out_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'taken_out_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'taken_out_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>4)ಅವನನ್ನ ಹೊಸ ವಸ್ತುಗಳಿಗೆ ಪರಿಚಯಿಸಿದ್ದೇನೆ.</td>
				
				<td>
				<?= $form->field($model, 'introduced_newthings')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'introduced_newthings')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'introduced_newthings_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'introduced_newthings_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'introduced_newthings_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				
				
				
				<td>
				<?= $form->field($model, 'introduced_newthings_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>5)ಅವನಿಗೆ ಕೆಲವೊಂದು ವಿಷಯಗಳನ್ನು ಮಾಡಬಾರದು ಎಂದು ಕಲಿಸಿದ್ದೇನೆ.</td>
				<td>
				<?= $form->field($model, 'told_not_todo_something')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'told_not_todo_something')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'told_not_todo_something_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'told_not_todo_something_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'told_not_todo_something_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				
				
				<td>
			<?= $form->field($model, 'told_not_todo_something_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>6)ಅವನನ್ನ ಸ್ವಚ್ಛಗೊಳಿಸಿದ್ದೇನೆ ಆಥವಾ ಅವನ ಡಯಪರ್ ಅನ್ನು ಬದಲಾಯಿಸಿದ್ದೇನೆ</td>
				<td>
				<?= $form->field($model, 'cleaned')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'cleaned')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'cleaned_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'cleaned_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'cleaned_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				
				<td>
				<?= $form->field($model, 'cleaned_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>7)ಅವನು ಅಳುವಾಗ ಅವನನ್ನು  ತೂಗಿದ್ದೇನೆ</td>
				<td>
				<?= $form->field($model, 'rocked')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'rocked')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'rocked_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'rocked_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'rocked_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				
				
				
				
				<td>
				<?= $form->field($model, 'rocked_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>8)ಅವನನ್ನು ಎತ್ತಿಕೊಂಡಿದ್ದೇನೆ </td>
				<td>
				<?= $form->field($model, 'carried')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'carried')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'carried_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'carried_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'carried_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				
				
				
				<td>
				<?= $form->field($model, 'carried_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>9)ಅವನಿಗೆ ಬೈದಿದ್ದೇನೆ</td>
				<td>
				<?= $form->field($model, 'scolded')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'scolded')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'scolded_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'scolded_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'scolded_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				
				
				
				<td>
				<?= $form->field($model, 'scolded_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>10)ಅವನಿಗೆ ತುಂಬಾ  ಬೆಚ್ಚಗಾಗಿಲ್ಲ  ಎಂದು ನೋಡಿಕೊಂಡಿದ್ದೇನೆ.</td>
				<td>
				<?= $form->field($model, 'wasnt_too_hot')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'wasnt_too_hot')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'wasnt_too_hot_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'wasnt_too_hot_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'wasnt_too_hot_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				
				
				
				<td>
				<?= $form->field($model, 'wasnt_too_hot_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>11)ಅವನಿಗೆ ಹುಷಾರಿಲ್ಲದಿದ್ದಾಗ ಅವನನ್ನು ನೋಡಿಕೊಂಡಿದ್ದೇನೆ  </td>
				<td>
				<?= $form->field($model, 'care_sick')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'care_sick')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'care_sick_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'care_sick_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'care_sick_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				
				<td>
				<?= $form->field($model, 'care_sick_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>12)ಅವನಿಗೆ ಹಾನಿ ಮಾಡಬಹುದಾದ ವಸ್ತುವಿನಿಂದ ಅವನನ್ನು ದೂರ ಇಟ್ಟಿದ್ದೇನೆ. </td>
				<td>
				<?= $form->field($model, 'taken_harm')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'taken_harm')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'taken_harm_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'taken_harm_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'taken_harm_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				<?= $form->field($model, 'taken_harm_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>13)ಅವನಿಗೆ ಊಟ ಮಾಡಿಸಿದ್ದೇನೆ</td>
				<td>
				<?= $form->field($model, 'fed')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'fed')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'fed_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'fed_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'fed_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				<?= $form->field($model, 'fed_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>14)ಅವನಿಗೆ ಮುತ್ತು  ಕೊಟ್ಟಿದ್ದೇನೆ</td>
				<td>
				<?= $form->field($model, 'kissed')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'kissed')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'kissed_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'kissed_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'kissed_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				<?= $form->field($model, 'kissed_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>15) ಅವನಿಗೆ ಗುಣಿಗುಣಿಸಿದ್ದೇನೆ .</td>
				<td>
				<?= $form->field($model, 'cooed')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'cooed')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'cooed_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'cooed_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'cooed_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				<?= $form->field($model, 'cooed_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>16)ಅವನಿಗೆ ನಿದ್ದೆ ಮಾಡಿಸಿದ್ದೇನೆ. </td>
				<td>
				<?= $form->field($model, 'put_sleep')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'put_sleep')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'put_sleep_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'put_sleep_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'put_sleep_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				<?= $form->field($model, 'put_sleep_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>17) ಅವನಿಗೆ ಸ್ನಾನ ಮಾಡಿಸಿದ್ದೇನೆ.</td>
				<td>
				<?= $form->field($model, 'bathed')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'bathed')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'bathed_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'bathed_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'bathed_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				<?= $form->field($model, 'bathed_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>18)ಅವನು ಹಾನಿ ಮಾಡುವಂತದ್ದನ್ನು  ತಿಂದಿಲ್ಲ   ಎಂದು ನೋಡಿಕೊಂಡಿದ್ದೇನೆ</td>
				<td>
				<?= $form->field($model, 'didnt_eat_harmful')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'didnt_eat_harmful')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'didnt_eat_harmful_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'didnt_eat_harmful_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'didnt_eat_harmful_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				<?= $form->field($model, 'didnt_eat_harmful_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>19)ಅವನು ಅಳುವಾಗ ಅವನನ್ನು ಸಮಾಧಾನ ಪಡಿಸಿದ್ದೇನೆ </td>
				<td>
				<?= $form->field($model, 'soothed')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'soothed')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'soothed_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'soothed_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'soothed_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				<?= $form->field($model, 'soothed_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>20)ಅವನಿಗೆ ಮಾಲಿಶ್ ಮಾಡಿದ್ದೇನೆ</td>
				<td>
				<?= $form->field($model, 'massaged')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'massaged')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'massaged_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'massaged_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'massaged_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				<?= $form->field($model, 'massaged_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>21)ಅವನಿಗೆ ಏನು ಮಾಡಬೇಕೆಂದು ತೋರಿಸಿದ್ದೇನೆ </td>
				<td>
				<?= $form->field($model, 'showed_what_todo')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'showed_what_todo')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'showed_what_todo_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'showed_what_todo_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'showed_what_todo_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				<?= $form->field($model, 'showed_what_todo_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				<tr>
				<td>22)ಅವನನ್ನು ತೂಗಿದ್ದೇನೆ </td>
				<td>
				<?= $form->field($model, 'thoogiddene')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'thoogiddene')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'thoogiddene_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'thoogiddene_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'thoogiddene_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				<?= $form->field($model, 'thoogiddene_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>23)ಅವನ್ನು ಅಳುವಾಗ ಅವನನ್ನು ಅಪ್ಪಿಕೊಂಡು ಮುದ್ದಾಡಿದ್ದೇನೆ </td>
				<td>
				<?= $form->field($model, 'cuddled_crying')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'cuddled_crying')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'cuddled_crying_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'cuddled_crying_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'cuddled_crying_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				<?= $form->field($model, 'cuddled_crying_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>24)ಅವನಿಗೆ ಬಟ್ಟೆ ತೊಡಿಸಿದ್ದೇನೆ.</td>
				<td>
				<?= $form->field($model, 'dressed')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'dressed')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'dressed_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'dressed_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'dressed_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				<?= $form->field($model, 'dressed_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>25)ಅವನು ತಪ್ಪು ಕೆಲಸಗಳನ್ನು ಮಾಡಿದಾಗ ಬೈದಿದ್ದೇನೆ </td>
				<td>
				<?= $form->field($model, 'told_off')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'told_off')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'told_off_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'told_off_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'told_off_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				<?= $form->field($model, 'told_off_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>26) ಅವನಿಗೆ ಆರಾಮವಾಗುವಂತೆ ಮಾಡಿದ್ದೇನೆ</td>
				<td>
				<?= $form->field($model, 'comfortable')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'comfortable')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'comfortable_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'comfortable_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'comfortable_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				<?= $form->field($model, 'comfortable_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>27) ಅವನ ಪಕ್ಕ/ಹತ್ತಿರದಲ್ಲಿ ಮಲಗಿದ್ದೇನೆ</td>
				<td>
				<?= $form->field($model, 'slept_beside')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'slept_beside')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'slept_beside_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'slept_beside_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'slept_beside_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				<?= $form->field($model, 'slept_beside_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>28)ಅವನನ್ನು ಹೊಸ ಜನರಿಗೆ ಪರಿಚಯಿಸಿದ್ದೇನೆ </td>
				<td>
				<?= $form->field($model, 'introduced_new_people')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'introduced_new_people')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'introduced_new_people_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'introduced_new_people_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'introduced_new_people_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				<?= $form->field($model, 'introduced_new_people_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>29)ಅವನು ಅತ್ತಾಗ ಅವನಿಗೆ ಬೈದಿದ್ದೇನೆ.</td>
				<td>
				<?= $form->field($model, 'scolded_crying')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'scolded_crying')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'scolded_crying_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'scolded_crying_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'scolded_crying_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				<?= $form->field($model, 'scolded_crying_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>30)ಅವನು ಅತ್ತಾಗ ಅವನನ್ನು  ನನ್ನ ಕೈಯ್ಯಲ್ಲಿ ತೂಗಾಡಿದ್ದೇನೆ</td>
				<td>
				<?= $form->field($model, 'cradled_crying')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'cradled_crying')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'cradled_crying_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'cradled_crying_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'cradled_crying_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				<?= $form->field($model, 'cradled_crying_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>31)ಅವನಿಗೆ ಸಾಕಷ್ಟು ಬೆಚ್ಚಗೆ ಇದೆ ಎಂದು ಖಚಿತಪಡಿಸಿಕೊಂಡಿದ್ದೇನೆ</td>
				<td>
				<?= $form->field($model, 'made_warm')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'made_warm')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'made_warm_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'made_warm_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'made_warm_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				<?= $form->field($model, 'made_warm_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>32)ಅವನಿಗೆ ಎಚ್ಚರವಾದಾಗ  ಅವನ ಹತ್ತಿರ ಹೋಗಿದ್ದೇನೆ</td>
				<td>
				<?= $form->field($model, 'gone_wokeup')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'gone_wokeup')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'gone_wokeup_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'gone_wokeup_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'gone_wokeup_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'gone_wokeup_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>33)ಅವನಿಗೆ  ಬಾಟಲ್ /ಮೇಲಿನ ಹಾಲನ್ನು ತಯಾರಿಸಿದ್ದೇನೆ.</td>
				<td>
				<?= $form->field($model, 'prepared_formula_milk')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'prepared_formula_milk')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'prepared_formula_milk_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'prepared_formula_milk_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'prepared_formula_milk_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				
				<td>
				<?= $form->field($model, 'prepared_formula_milk_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>34)ಅವನ ಜೊತೆ ಮಾತನಾಡಿದ್ದೇನೆ</td>
				<td>
				<?= $form->field($model, 'talked')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'talked')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'talked_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'talked_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'talked_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'talked_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>35)ಅವನನ್ನು ಮುದ್ದಾಡಿದ್ದೇನೆ </td>
				<td>
				<?= $form->field($model, 'cuddled')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'cuddled')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'cuddled_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'cuddled_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'cuddled_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				
				<td>
				<?= $form->field($model, 'cuddled_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>36)ಅವನು ಅಳುತ್ತಿದಾಗ ಅವನಿಗೆ ಹಾಡು ಹೇಳಿದ್ದೇನೆ</td>
				<td>
				<?= $form->field($model, 'sung')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'sung')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'sung_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'sung_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'sung_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				
				<td>
				<?= $form->field($model, 'sung_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				
				
				<tr>
				<td>37)ಅವನನ್ನು ದಾದಿ /ವೈದ್ಯರ ಹತ್ತಿರ ಕರೆದುಕೊಂಡು ಹೋಗಿದ್ದೇನೆ </td>
				<td>
				<?= $form->field($model, 'taken_nurse')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'taken_nurse')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'taken_nurse_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'taken_nurse_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'taken_nurse_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'taken_nurse_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>38)ಅವನ ಹತ್ತಿರ ಕಟ್ಟು ನಿಟ್ಟಾಗಿ ಮಾತನಾಡಿದ್ದೇನೆ </td>
				<td>
				<?= $form->field($model, 'talked_sternly')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'talked_sternly')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'talked_sternly_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'talked_sternly_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'talked_sternly_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'talked_sternly_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>39)ಅವನನ್ನು ಹಿಡಿದು ಕೊಂಡಿದ್ದೇನೆ </td>
				<td>
				<?= $form->field($model, 'held')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'held')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'held_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'held_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'held_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'held_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>40)ಅವನ ಜೊತೆ ನಕ್ಕಿದ್ದೇನೆ </td>
				<td>
				<?= $form->field($model, 'shared_smiles')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'shared_smiles')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'shared_smiles_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'shared_smiles_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'shared_smiles_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'shared_smiles_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>41)ಅವನು ಅಳುವಾಗ ಅವನ ಗಮನ ಬೆರೆಡೆ ತಿರುಗಿಸಿದ್ದೇನೆ </td>
				<td>
				<?= $form->field($model, 'distracted_crying')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'distracted_crying')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'distracted_crying_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'distracted_crying_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'distracted_crying_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'distracted_crying_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>42)ಅವನಿಗೆ ಸರಿಯಾದ ವಿಷಯ ಮಾಡಲು ತೋರಿಸಿದ್ದೇನೆ ಉದಾ :ಚಪ್ಪಲ್ ಹಾಕುವುದು ,ಬಟ್ಟೆ ಹಾಕುವುದು  ಇತ್ಯಾದಿ . </td>
				<td>
				<?= $form->field($model, 'showed_rightthing')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'showed_rightthing')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'showed_rightthing_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'showed_rightthing_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'showed_rightthing_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'showed_rightthing_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>43)ಅವನನ್ನು ನೆಂಟರನ್ನು /ಸಂಭಂದಿಕರನ್ನು ಭೇಟಿ ಮಾಡಲು ಕರೆದುಕೊಂಡು ಹೋಗಿದ್ದೇನೆ</td>
				
				<td>
				<?= $form->field($model, 'taken_visit_family')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'taken_visit_family')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'taken_visit_family_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'taken_visit_family_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'taken_visit_family_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				
				
				
				<td>
				<?= $form->field($model, 'taken_visit_family_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>44)ಅವನನ್ನ ನನ್ನ ಕೈಯಲ್ಲಿ   ತೂಗಾಡಿದ್ದೇನೆ</td>
				<td>
				<?= $form->field($model, 'cradled_arms')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'cradled_arms')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'cradled_arms_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'cradled_arms_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'cradled_arms_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'cradled_arms_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>45)ಅವನು ಅಳುವನ್ನು ನಿಲ್ಲಿಸಲು ಅವನಿಗೆ ಊಟ ಮಾಡಿಸಿದ್ದೇನೆ /ಹಾಲು ಕುಡಿಸಿದ್ದೇನೆ </td>
				<td>
				<?= $form->field($model, 'fed_crying')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'fed_crying')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'fed_crying_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'fed_crying_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'fed_crying_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'fed_crying_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>46)ಅವನ ಜೊತೆ ಪ್ರಾರ್ಥಿಸಿದ್ದೇನೆ</td>
				<td>
				<?= $form->field($model, 'prayed')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'prayed')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'prayed_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'prayed_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'prayed_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'prayed_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				<tr>
				<td>47)ಅವನನ್ನ ಕುಟುಂಬದ ವಿವಿಧ ಆಚರಣೆಗಳಲ್ಲಿ ಭಾಗಿ ಮಾಡಿಸಿದ್ದೇನೆ (ಮದುವೆ, ಪೂಜೆ, ಇತರೆ ಸಮಾರಂಭ)</td>
				<td>
				<?= $form->field($model, 'invovled_family_rituals')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'invovled_family_rituals')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'invovled_family_rituals_acg')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				<td>
				
				<?= $form->field($model, 'invovled_family_rituals_acg')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
				
				</td>
				<td>
				<?= $form->field($model, 'invovled_family_rituals_acg_lastweek')->checkbox(['class' => 'toggle-switch-req','dataOn' => 'Yes','dataOff' => 'No']); ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'invovled_family_rituals_acg_lastweek')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 

				</td>
				</tr>
				
				

				<tr class="hidden">
				<td><label>Score</label></td>
             	<td id="scaleflag" colspan="4"><?= $form->field($model, 'score')->input('number',['readonly'=>true])->label(false);?></td>
				</tr>
				
				</tbody>
			</table>
				<label></b>ನೀವು ಮಗುವಿನೊಂದಿಗೆ ಮಾಡುವ ಇತರೆ ಚಟುವಟಿಕೆಗಳನ್ನು ತಿಳಿಸಿ    (ತಾಯಿ ) </b></label>
				<?= $form->field($model, 'other_activity')->textInput()->label(false); ?>
				<label></b>ನೀವು ಮಗುವಿನೊಂದಿಗೆ ಮಾಡುವ ಇತರೆ ಚಟುವಟಿಕೆಗಳನ್ನು ತಿಳಿಸಿ (ACG)</b></label>
				<?= $form->field($model, 'other_activity_acg')->textInput()->label(false); ?>
    </div>
     <?php ActiveForm::end(); ?>
</div>
</div>
<?php $this->registerJs('
	$("#"+formObj.formName).change(function()
	{
		autoSave(formObj);
	});

')?>
<?php $this->registerJs('
	$("#yes").click(function()
		{
		 $("#c").prop("checked", true);
		  $("#sharedcaregiving-played").prop("checked", true);
		   $("#sharedcaregiving-taken_out").prop("checked", true);
		   $("#sharedcaregiving-introduced_newthings").prop("checked", true);
		   $("#sharedcaregiving-told_not_todo_something").prop("checked", true);
		   $("#sharedcaregiving-cleaned").prop("checked", true);
		    $("#sharedcaregiving-rocked").prop("checked", true);
		    $("#sharedcaregiving-carried").prop("checked", true);
		     $("#sharedcaregiving-scolded").prop("checked", true);
		     $("#sharedcaregiving-wasnt_too_hot").prop("checked", true);
		     $("#sharedcaregiving-care_sick").prop("checked", true);
		      $("#sharedcaregiving-taken_harm").prop("checked", true);
		      $("#sharedcaregiving-fed").prop("checked", true);
		      $("#sharedcaregiving-kissed").prop("checked", true);
		      $("#sharedcaregiving-cooed").prop("checked", true);
		      $("#sharedcaregiving-put_sleep").prop("checked", true);
		      $("#sharedcaregiving-bathed").prop("checked", true);
		      $("#sharedcaregiving-didnt_eat_harmful").prop("checked", true);
		      $("#sharedcaregiving-soothed").prop("checked", true);
		      $("#sharedcaregiving-massaged").prop("checked", true);
		      $("#sharedcaregiving-showed_what_todo").prop("checked", true);
		      $("#sharedcaregiving-thoogiddene").prop("checked", true);
		       $("#sharedcaregiving-cuddled_crying").prop("checked", true);
		       $("#sharedcaregiving-dressed").prop("checked", true);
		       $("#sharedcaregiving-told_off").prop("checked", true);
		        $("#sharedcaregiving-comfortable").prop("checked", true);
		        $("#sharedcaregiving-slept_beside").prop("checked", true);
		         $("#sharedcaregiving-introduced_new_people").prop("checked", true);
		         $("#sharedcaregiving-scolded_crying").prop("checked", true);
		        $("#sharedcaregiving-made_warm").prop("checked", true);
		         $("#sharedcaregiving-gone_wokeup").prop("checked", true);
		         $("#sharedcaregiving-prepared_formula_milk").prop("checked", true);
		        $("#sharedcaregiving-talked").prop("checked", true);
		         $("#sharedcaregiving-cuddled").prop("checked", true);
		         $("#sharedcaregiving-sung").prop("checked", true);
		        $("#sharedcaregiving-taken_nurse").prop("checked", true);
		         $("#sharedcaregiving-talked_sternly").prop("checked", true);
		         $("#sharedcaregiving-held").prop("checked", true);
		        $("#sharedcaregiving-shared_smiles").prop("checked", true);
		         $("#sharedcaregiving-distracted_crying").prop("checked", true);
		           $("#sharedcaregiving-showed_rightthing").prop("checked", true);
		         $("#sharedcaregiving-taken_visit_family").prop("checked", true);
		         $("#sharedcaregiving-cradled_arms").prop("checked", true);
		        $("#sharedcaregiving-fed_crying").prop("checked", true);
		         $("#sharedcaregiving-prayed").prop("checked", true);
		           $("#sharedcaregiving-invovled_family_rituals").prop("checked", true);
		         $("#sharedcaregiving-cradled_crying").prop("checked", true);
			 autoSaveObj = formObj;
		});

		$("#yes1").click(function()
		{
			 $("#sharedcaregiving-songs_acg").prop("checked", true);
		     $("#sharedcaregiving-played_acg").prop("checked", true);
		     $("#sharedcaregiving-taken_out_acg").prop("checked", true);
			 $("#sharedcaregiving-introduced_newthings_acg").prop("checked", true);
		     $("#sharedcaregiving-told_not_todo_something_acg").prop("checked", true);
		     $("#sharedcaregiving-cleaned_acg").prop("checked", true);
			 $("#sharedcaregiving-rocked_acg").prop("checked", true);
		     $("#sharedcaregiving-carried_acg").prop("checked", true);
		     $("#sharedcaregiving-scolded_acg").prop("checked", true);
			 $("#sharedcaregiving-wasnt_too_hot_acg").prop("checked", true);
		     $("#sharedcaregiving-care_sick_acg").prop("checked", true);
		     $("#sharedcaregiving-taken_harm_acg").prop("checked", true);
			 $("#sharedcaregiving-fed_acg").prop("checked", true);
		     $("#sharedcaregiving-kissed_acg").prop("checked", true);
		     $("#sharedcaregiving-cooed_acg").prop("checked", true);
			 $("#sharedcaregiving-put_sleep_acg").prop("checked", true);
		     $("#sharedcaregiving-bathed_acg").prop("checked", true);
		     $("#sharedcaregiving-didnt_eat_harmful_acg").prop("checked", true);
			 $("#sharedcaregiving-soothed_acg").prop("checked", true);
		     $("#sharedcaregiving-massaged_acg").prop("checked", true);
		     $("#sharedcaregiving-showed_what_todo_acg").prop("checked", true);
			 $("#sharedcaregiving-thoogiddene_acg").prop("checked", true);
		     $("#sharedcaregiving-cuddled_crying_acg").prop("checked", true);
		     $("#sharedcaregiving-dressed_acg").prop("checked", true);
			 $("#sharedcaregiving-told_off_acg").prop("checked", true);
			 $("#sharedcaregiving-comfortable_acg").prop("checked", true);
		     $("#sharedcaregiving-slept_beside_acg").prop("checked", true);
		     $("#sharedcaregiving-introduced_new_people_acg").prop("checked", true);
			 $("#sharedcaregiving-scolded_crying_acg").prop("checked", true);
		     $("#sharedcaregiving-cradled_crying_acg").prop("checked", true);
		     $("#sharedcaregiving-made_warm_acg").prop("checked", true);
			 $("#sharedcaregiving-gone_wokeup_acg").prop("checked", true);
		     $("#sharedcaregiving-prepared_formula_milk_acg").prop("checked", true);
		     $("#sharedcaregiving-talked_acg").prop("checked", true);
			 $("#sharedcaregiving-cuddled_acg").prop("checked", true);
		     $("#sharedcaregiving-sung_acg").prop("checked", true);
		     $("#sharedcaregiving-taken_nurse_acg").prop("checked", true);
			 $("#sharedcaregiving-talked_sternly_acg").prop("checked", true);
		     $("#sharedcaregiving-held_acg").prop("checked", true);
		     $("#sharedcaregiving-shared_smiles_acg").prop("checked", true);
			 $("#sharedcaregiving-distracted_crying_acg").prop("checked", true);
		     $("#sharedcaregiving-showed_rightthing_acg").prop("checked", true);
		     $("#sharedcaregiving-taken_visit_family_acg").prop("checked", true);
			 $("#sharedcaregiving-cradled_arms_acg").prop("checked", true);
		     $("#sharedcaregiving-fed_crying_acg").prop("checked", true);
		     $("#sharedcaregiving-prayed_acg").prop("checked", true);
			 $("#sharedcaregiving-invovled_family_rituals_acg").prop("checked", true);
			 	 autoSaveObj = formObj;
		});





		$("#yes2").click(function()
		{
		     $("#sharedcaregiving-songs_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-played_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-taken_out_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-introduced_newthings_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-told_not_todo_something_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-told_not_todo_something_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-cleaned_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-rocked_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-carried_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-scolded_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-wasnt_too_hot_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-care_sick_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-care_sick_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-taken_harm_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-fed_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-kissed_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-cooed_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-put_sleep_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-bathed_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-didnt_eat_harmful_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-soothed_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-massaged_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-showed_what_todo_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-thoogiddene_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-cuddled_crying_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-dressed_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-told_off_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-comfortable_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-slept_beside_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-introduced_new_people_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-scolded_crying_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-cradled_crying_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-made_warm_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-gone_wokeup_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-prepared_formula_milk_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-talked_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-cuddled_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-sung_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-taken_nurse_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-taken_nurse_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-held_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-shared_smiles_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-distracted_crying_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-showed_rightthing_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-taken_visit_family_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-cradled_arms_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-fed_crying_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-prayed_acg_lastweek").prop("checked", true); 
		     $("#sharedcaregiving-invovled_family_rituals_acg_lastweek").prop("checked", true);
		     $("#sharedcaregiving-talked_sternly_acg_lastweek").prop("checked", true);
			 autoSaveObj = formObj;


		});
')?>

<?php if($model->not_applicable == 'YES'){
	$this->registerJs('
	setNotApplicableOnLoad("#withBoxShadow","#sharedcaregiving-not_applicable");
	', \yii\web\View::POS_READY);

}
	$this->registerJs('

	$("#sharedcaregiving-not_applicable").change(function()
		{	
			var value = $(this).val();
			if(value=="YES")
			{	
				setNotApplicableYes("#withBoxShadow","#sharedcaregiving-not_applicable");
			}
			else
			{
				setNotApplicableNo("#withBoxShadow","#sharedcaregiving-not_applicable");
				location.reload();
			}
		});
		
	
', \yii\web\View::POS_READY);
?>

<script type="text/javascript">
    var formObj = {url:"shared-caregiving/ajax",formName : "formSharedcaregiving"};
	var calc = function () {
        var totalValue = 0;
		var root = 'sharedcaregiving';
        var id = '#'+root+'-';
		var clas = '.field-'+root+'-';
        var arr = ['songs','played', 'taken_out', 'introduced_newthings', 'told_not_todo_something', 'cleaned', 'rocked', 
		'carried', 'scolded', 'wasnt_too_hot', 'care_sick', 'taken_harm', 'fed', 'kissed', 'cooed', 'put_sleep', 'bathed', 
		'didnt_eat_harmful', 'soothed', 'massaged', 'showed_what_todo', 'cuddled_crying', 'dressed', 'told_off', 'comfortable', 
		'slept_beside', 'introduced_new_people', 'scolded_crying', 'cradled_crying', 'made_warm', 'gone_wokeup', 
		'prepared_formula_milk', 'talked', 'sung', 'cuddled', 'taken_nurse', 'talked_sternly', 'held', 'shared_smiles', 
		'distracted_crying', 'showed_rightthing', 'taken_visit_family', 'cradled_arms', 'fed_crying', 'prayed', 
		'invovled_family_rituals','thoogiddene']
        for (var i in arr) {
            var val = $(clas + arr[i] + ' input[type=radio]:checked').val();
			if (parseInt(val)>=0) {
                totalValue += parseInt(val);
            }
        }
        $(id+'score').val(totalValue);
		autoSaveObj = formObj;
    }
</script>

<style>
#notapp table{
	height:95px;
}
#sharetable table{
	height:600px;
}
</style>