<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SharedCaregiving */

$this->title = 'Shared Caregiving';
$this->params['breadcrumbs'][] = ['label' => 'Shared Caregivings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shared-caregiving-create">

    <h1 align="center">Shared Care Giving Checklist</h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
