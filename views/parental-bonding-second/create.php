<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ParentalBondingSecond */

$this->title = 'Parental Bonding Second';
$this->params['breadcrumbs'][] = ['label' => 'Parental Bonding Seconds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parental-bonding-second-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
