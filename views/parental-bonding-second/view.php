<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ParentalBondingSecond */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Parental Bonding Seconds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parental-bonding-second-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'less_helps_mother',
            'less_understands',
            'less_praise_mother',
            'more_secure_mother',
            'allow_decisions_mother',
            'allowstyle_mother',
            'less_books_mother',
            'less_stydy_mother',
            'family_burden_mother',
            'show_olavu_mother',
            'show_nirlakshya_mother',
            'less_helpings_mother',
            'less_undestnds_mother',
            'less_praises_mother',
            'high_safes_mother',
            'allows_decionss_mother',
            'allow_style_as_boys_mother',
            'less_resources_mother',
            'less_oportunity_study_mother',
            'boon_mother',
            'jokes_onme_mother',
            'respects_me_mother',
            'less_helps_father',
            'less_understands_father',
            'less_praise_father',
            'more_secure_father',
            'allow_decisions_father',
            'allowstyle_father',
            'less_books_father',
            'less_stydy_father',
            'family_burden_father',
            'show_olavu_father',
            'show_nirlakshya_father',
            'less_helpings_father',
            'less_undestnds_father',
            'less_praises_father',
            'high_safes_father',
            'allows_decionss_father',
            'allow_style_as_boys_father',
            'less_resources_father',
            'less_oportunity_study_father',
            'boon_father',
            'jokes_onme_father',
            'respects_me_father',
            'respondent_id',
            'assessment_id',
            'created_dtm',
            'last_updated_dtm',
            'updated_by',
            'score',
            'scale_id',
        ],
    ]) ?>

</div>
