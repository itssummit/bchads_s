﻿<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$keyPair = [-15 => '-15: Child Not Co-Operative For Assessment',
          -14 => '-14: Not Available For Assessment',
          -11 => '-11: Inadequate Information',
          -10 => '-10: Not Applicable',
          -9 => '-9: Missing',
          -8 => '-8: Refused',
          -7 => '-7: Partner Accompanied Missing',
          -6 => '-6: Family Accompanied Missing',
          -5 => '-5: Missing:Not Asked',
          -3 => '-3: No Such Relationship/No Partner'];
/* @var $this yii\web\View */
/* @var $model app\models\ParentalBondingSecond */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile('@web/js/scales.js',['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<style>
#parentalbondingsecond-not_applicable ,#parentalbondingsecond-any_siblings,#parentalbondingsecond-any_sisters{
	width:200px !important;
}
</style>
<div id="withBoxShadow" class="parental-bonding-second-form">

    <?php $form = ActiveForm::begin(['id' => 'formPbis']); ?>
	
	<table class="na">
		<thead>
			<tr><th>Is This Scale Applicable ?</th></tr>
			<tr><td><?= $form->field($model, 'not_applicable')->dropDownList(['' =>'','YES' => 'Not Applicable', 'NO' => 'Applicable'])->label(false); ?></td></tr>
		</thead>
	</table>
	
        <caption>ಈ ಕೆಳಗಿನ ಭಾಗದಲ್ಲಿ ಇರುವ ಪ್ರಶ್ನಾವಳಿಗಳಲ್ಲಿ "ಕುಟುಂಬದಲ್ಲಿರುವ ಇತರ ಹುಡುಗಿಯರು " ಎಂದರೆ "ಸಹೋದರಿಯರು " ಮತ್ತು ಒಂದು ವೇಳೆ ಇಲ್ಲದೆ ಇದ್ದಲ್ಲಿ "ಸೋದರ ಸೋದರಿಯರು " ಎಂದರ್ಥ ."ಕುಟುಂಬದಲ್ಲಿರುವ  ಇತರ ಹುಡುಗರು " ಎಂದರೆ "ಸಹೋದರರು " ಮತ್ತು  ಒಂದು  ವೇಳೆ ಇಲ್ಲದೆ ಇದ್ದಲ್ಲಿ "ಸೋದರ ಸಹೋದರರು " ಎಂದರ್ಥ 
				ಹಾಗಾದರೆ ನಮಗೆ ಅರ್ಥವಾಯಿತು ನೀವು ಯಾರ ಬಗ್ಗೆ ಉತ್ತರಿಸುತ್ತಿದ್ದೀರಿ ಎಂದು .ಮೊದಲು ನೀವು ನಿಮ್ಮ ಜೊತೆ ಯಾರು ಇದ್ದರು ಎಂದು ತಿಳಿಸಿ .ನಿಮಗೆ ಅನ್ವಯಿಸುವುದನ್ನು ಆಯ್ಕೆ ಮಾಡಿ  </caption>
		<br>
		<table class="na">
		<thead>
			<tr>
			<th>Any Brothers? </th>
			<th>Any Sisters?</th>
			</tr>
			<tr>
			<td><?= $form->field($model, 'any_siblings')->dropDownList(['' =>'','YES' => 'YES', 'NO' => 'NO'])->label(false); ?></td>
			<td><?= $form->field($model, 'any_sisters')->dropDownList(['' =>'','YES' => 'YES', 'NO' => 'NO'])->label(false); ?></td>			</tr>
			
		</thead>
		</table>
		
		
		
		<div id="small">
			<table border="1" class="responsive-scales" style = "display:<?php echo ($model->any_siblings == 'YES')?"display":"none";?>">
			<tbody>
			<tr>
			<th>1. No of Brothers</th>
			<td><?= $form->field($model, 'no_of_brothers')->textInput()->label(false); ?></td>
			</tr>
			<tr>
			<th>2. No of Cousin Brothers</th>
			<td><?= $form->field($model, 'no_of_cousines_brothers')->textInput()->label(false); ?></td>
			</tr>
			</tbody>
			</table>
		</div>
		
		<div id="small2">
			<table border="1" class="responsive-scales" style = "display:<?php echo ($model->any_sisters == 'YES')?"display":"none";?>">
			<tbody>
			<tr>
			<th>3. No of Sisters</th>
			<td><?= $form->field($model, 'no_of_sisters')->textInput()->label(false); ?></td>
			</tr>
			<tr>
			<th>4. No of Cousin Sisters</th>
			<td><?= $form->field($model, 'no_of_cousins')->textInput()->label(false); ?></td>
			</tr>
			</tbody>
			</table>
		</div>
        <table border="1" class="responsive-scales">
            <thead>
                <tr>
                    <th>ಕ್ರ . ಸಂ </th>
                    <th>ಹೇಳಿಕೆಗಳು </th>
                    <th>ತುಂಬಾ  ಆಥರ </th>
                    <th>ಸ್ವಲ್ಪ ಮಟ್ಟಿಗೆ ಆಥರ </th>
                    <th>ಸ್ವಲ್ಪ ಮಟ್ಟಿಗೆ ಆಥರವಿಲ್ಲ</th>
                    <th>ತುಂಬಾ ಆಥಿರವಿಲ್ಲ </th>
                    
                    <th>Other Answers</th>
                </tr> 
            </thead>
            
            <tbody>
            
            
            
            
            
                <tr>
        <td>26</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ ಇತರ ಹುಡುಗರಿಗಿಂತ ನನಗೆ ಕಡಿಮೆ ಸಹಾಯ ಮಾಡಿದರು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'less_helps_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'less_helps_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>27</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ ಹುಡುಗಿಯರಿಗಿಂತ ನನ್ನ ನೋವು ಮತ್ತು ಚಿಂತೆಗಳನ್ನು ಕಡಿಮೆ ಅರ್ಥ ಮಾಡಿಕೊಂಡಂತೆ ಕಾಣುತಿತ್ತು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'less_understands_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'less_understands_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>28</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ ಹುಡುಗರಿಗಿಂತ ನನ್ನನ್ನು ಕಡಿಮೆ ಹೊಗಳುತ್ತಿದ್ದರು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'less_praise_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'less_praise_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>29</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ ಇತರೆ ಹುಡುಗಿಯರಿಗಿಂತ  ಅಗತ್ಯಕ್ಕಿಂತ ಹೆಚ್ಚಾಗಿ  ಮಾಡುತ್ತಿದ್ದರು . </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'more_secure_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'more_secure_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>30</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ ಇತರ ಹುಡುಗರಿಗಿಂತ ,ನನ್ನ ನಿರ್ಧಾರಗಳನ್ನು ನಾನೇ ತೆಗೆದುಕೊಳ್ಳಲು ಕಡಿಮೆ ಬಿಡುತ್ತಿದ್ದರು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'allow_decisions_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'allow_decisions_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>31</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ ಇತರ  ಹುಡುಗಿಯರಿಗಿಂತ ,ನನಗೆ ಇಷ್ಟವಾಗುವ  ಹಾಗೆ ಅಲಂಕಾರ ಮಾಡಿಕೊಳ್ಳಲು   ಕಡಿಮೆ    ಬಿಡುತ್ತಿದ್ದರು .</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'allowstyle_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'allowstyle_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>32</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ  ಇತರ   ಹುಡುಗರಿಗೆ   ಹೋಲಿಸಿದರೆ,  ಬೇಕಾಗುವ  ಅವಶ್ಯಕತೆ /ಸೌಲಭ್ಯಗಳನ್ನು ಕಡಿಮೆ ಒದಗಿಸುತ್ತಿದ್ದರು (ಪುಸ್ತಕಗಳು,ಬಟ್ಟೆ,ಆಹಾರ ).</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'less_books_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'less_books_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>33</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ  ಇತರ ಹುಡುಗಿಯರಿಗೆ  ಹೋಲಿಸಿದರೆ, ನನಗೆ ಓದಲು ಕಡಿಮೆ ಅವಕಾಶ ಕೊಡುತ್ತಿದ್ದರು .  </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'less_stydy_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'less_stydy_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>34</td>
        <th> ನಾನು ಕುಟುಂಬಕ್ಕೆ ಹೊರೆಯಾಗಿದ್ದೇನೆಂದು ಹೇಳುತ್ತಿದ್ದರು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'family_burden_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'family_burden_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>35</td>
        <th>ನಾನು ಹುಡುಗಿ ಆಗಿದ್ದ ಕಾರಣಕ್ಕಾಗಿ ನನ್ನ   ಪರವಾಗಿದ್ದರು /ಒಲವು ತೋರಿಸುತ್ತಿದ್ದರು. </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'show_olavu_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'show_olavu_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>36</td>
        <th>ನಾನು ಹುಡುಗಿ ಆಗಿದ್ದ ಕಾರಣಕ್ಕಾಗಿ  ನನಗೆ ಸಂಬಂಧಿಸಿದ  ವಿಷಯಗಲ್ಲಿಯೂ ನನ್ನ ಅಭಿಪ್ರಾಯ  /  ಇಲ್ಲದಿರುವಿಕೆಗಳನ್ನೂ ನಿರ್ಲಕ್ಷ್ಯ ಮಾಡುತ್ತಿದ್ದರು .</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'show_nirlakshya_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'show_nirlakshya_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>37</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ  ಇತರ  ಹುಡುಗಿಯರಿಗಿಂತ  ನನಗೆ ಕಡಿಮೆ  ಸಹಾಯ ಮಾಡಿದರು .  </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'less_helpings_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'less_helpings_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>38</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ  ಹುಡುಗರಿಗಿಂತ ನನ್ನ ನೋವು ಮತ್ತು ಚಿಂತೆಗಳನ್ನು ಕಡಿಮೆ ಅರ್ಥ ಅರ್ಥ ಮಾಡಿಕೊಂಡಂತೆ ಕಾಣುತ್ತಿತ್ತು .</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'less_undestnds_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'less_undestnds_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>39</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ   ಹುಡುಗಿಯರಿಗಿಂತ  ನನ್ನನ್ನು ಕಡಿಮೆ ಹೊಗಳುತ್ತಿದ್ದರು . </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'less_praises_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'less_praises_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>40</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ  ಇತರ  ಹುಡುಗರಿಗಿಂತ ನನ್ನನ್ನು ಅಗತ್ಯಕ್ಕಿಂತ ಹೆಚ್ಚಾಗಿ ರಕ್ಷಣೆ ಮಾಡುತ್ತಿದ್ದರು .</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'high_safes_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'high_safes_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>41</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ   ಇತರ  ಹುಡುಗಿಯರಿಗಿಂತ, ನನ್ನ ನಿರ್ಧಾರಗಳನ್ನು  ನಾನೇ  ತೆಗೆದುಕೊಳ್ಳಲು  ಕಡಿಮೆ ಬಿಡುತ್ತಿದ್ದರು .</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'allows_decionss_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'allows_decionss_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>42</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ  ಇತರ  ಹುಡುಗರಿಗಿಂತ  , ನನಗೆ ಇಷ್ಟವಾದ ಹಾಗೆ ಅಲಂಕಾರ ಮಾಡಲು  ನನಗೆ ಕಡಿಮೆ ಬಿಡುತ್ತಿದ್ದರು . </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'allow_style_as_boys_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'allow_style_as_boys_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>43</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ   ಇತರೆ ಹುಡುಗಿಯರಿಗೆ ಹೋಲಿಸಿದರೆ , ನನಗೆ ಬೇಕಾಗುವ ಸಂಪನ್ಮೂಲಗಳಗನ್ನು  ಕಡಿಮೆ  ಒದಗಿಸುತ್ತಿದ್ದರು .   </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'less_resources_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'less_resources_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>44</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ   ಇತರ  ಹುಡುಗರಿಗೆ ಹೋಲಿಸಿದರೆ ,ನನಗೆ  ಓದಲು ಕಡಿಮೆ ಅವಕಾಶವನ್ನು ಕೊಡಿತ್ತಿದ್ದರು . </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'less_oportunity_study_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'less_oportunity_study_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    
    <tr>
        <td>44</td>
        <th>ನಾನು ಕುಟುಂಬಕ್ಕೆ ಒಂದು ವರದಾನವೆಂದು ಹೇಳುತ್ತಿದ್ದರು . </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'boon_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'boon_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>46</td>
        <th>ನಾನು ಹುಡಿಗಿಯಾಗಿದ್ದ ಕಾರಣ ನನ್ನನ್ನು ಅಪಹಾಸ್ಯ ಮಾಡುವುದು ಮತ್ತು ಟೀಕಿಸಿವುದು ಮಾಡುತ್ತಿದ್ದರು . </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'jokes_onme_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'jokes_onme_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>47</td>
        <th>ನಾನು ಹುಡಿಗಿಯಾಗಿದ್ದ ಕಾರಣ , ನನಗೆ ಸಂಬಂಧಿಸಿದ ವಿಷಯಗಲ್ಲಿಯೂ  ನನ್ನ ಅಭಿಪ್ರಾಯ /ಇಷ್ಟ ಮತ್ತು ಇಷ್ಟದಿಲ್ಲದಿರುವಿಕೆಯನ್ನು ಗೌರವಿಸುತ್ತಿದ್ದರು . </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'respects_me_mother')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'respects_me_mother')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    </tbody>
    </table>




<caption>ಈ ಕೆಳಗಿನ ಭಾಗದಲ್ಲಿ ಇರುವ ಪ್ರಶ್ನಾವಳಿಗಳಲ್ಲಿ "ಕುಟುಂಬದಲ್ಲಿರುವ ಇತರ ಹುಡುಗಿಯರು " ಎಂದರೆ "ಸಹೋದರಿಯರು " ಮತ್ತು ಒಂದು ವೇಳೆ ಇಲ್ಲದೆ ಇದ್ದಲ್ಲಿ "ಸೋದರ ಸೋದರಿಯರು " ಎಂದರ್ಥ ."ಕುಟುಂಬದಲ್ಲಿರುವ  ಇತರ ಹುಡುಗರು " ಎಂದರೆ "ಸಹೋದರರು " ಮತ್ತು  ಒಂದು  ವೇಳೆ ಇಲ್ಲದೆ ಇದ್ದಲ್ಲಿ "ಸೋದರ ಸಹೋದರರು " ಎಂದರ್ಥ 
ಹಾಗಾದರೆ ನಮಗೆ ಅರ್ಥವಾಯಿತು ನೀವು ಯಾರ ಬಗ್ಗೆ ಉತ್ತರಿಸುತ್ತಿದ್ದೀರಿ ಎಂದು .ಮೊದಲು ನೀವು ನಿಮ್ಮ ಜೊತೆ ಯಾರು ಇದ್ದರು ಎಂದು ತಿಳಿಸಿ .ನಿಮಗೆ ಅನ್ವಯಿಸುವುದನ್ನು ಆಯ್ಕೆ ಮಾಡಿ  </caption>
<table border="1" class="responsive-scales">
            <thead>
                <tr>
                    <th>ಕ್ರ . ಸಂ </th>
                    <th>ಹೇಳಿಕೆಗಳು </th>
                    <th>ತುಂಬಾ  ಆಥರ </th>
                    <th>ಸ್ವಲ್ಪ ಮಟ್ಟಿಗೆ ಆಥರ </th>
                    <th>ಸ್ವಲ್ಪ ಮಟ್ಟಿಗೆ ಆಥರವಿಲ್ಲ</th>
                    <th>ತುಂಬಾ ಆಥಿರವಿಲ್ಲ </th>
                    
                    <th>Other Answers</th>
                </tr> 
            </thead>
            
            <tbody>
            
            
            
            
            
                <tr>
        <td>26</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ ಇತರ ಹುಡುಗರಿಗಿಂತ ನನಗೆ ಕಡಿಮೆ ಸಹಾಯ ಮಾಡಿದರು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'less_helps_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'less_helps_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>27</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ ಹುಡುಗಿಯರಿಗಿಂತ ನನ್ನ ನೋವು ಮತ್ತು ಚಿಂತೆಗಳನ್ನು ಕಡಿಮೆ ಅರ್ಥ ಮಾಡಿಕೊಂಡಂತೆ ಕಾಣುತಿತ್ತು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'less_understands_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'less_understands_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>28</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ ಹುಡುಗರಿಗಿಂತ ನನ್ನನ್ನು ಕಡಿಮೆ ಹೊಗಳುತ್ತಿದ್ದರು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'less_praise_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'less_praise_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>29</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ ಇತರೆ ಹುಡುಗಿಯರಿಗಿಂತ  ಅಗತ್ಯಕ್ಕಿಂತ ಹೆಚ್ಚಾಗಿ  ಮಾಡುತ್ತಿದ್ದರು . </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'more_secure_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'more_secure_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>30</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ ಇತರ ಹುಡುಗರಿಗಿಂತ ,ನನ್ನ ನಿರ್ಧಾರಗಳನ್ನು ನಾನೇ ತೆಗೆದುಕೊಳ್ಳಲು ಕಡಿಮೆ ಬಿಡುತ್ತಿದ್ದರು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'allow_decisions_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'allow_decisions_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>31</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ ಇತರ  ಹುಡುಗಿಯರಿಗಿಂತ ,ನನಗೆ ಇಷ್ಟವಾಗುವ  ಹಾಗೆ ಅಲಂಕಾರ ಮಾಡಿಕೊಳ್ಳಲು   ಕಡಿಮೆ    ಬಿಡುತ್ತಿದ್ದರು .</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'allowstyle_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'allowstyle_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>32</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ  ಇತರ   ಹುಡುಗರಿಗೆ   ಹೋಲಿಸಿದರೆ,  ಬೇಕಾಗುವ  ಅವಶ್ಯಕತೆ /ಸೌಲಭ್ಯಗಳನ್ನು ಕಡಿಮೆ ಒದಗಿಸುತ್ತಿದ್ದರು (ಪುಸ್ತಕಗಳು,ಬಟ್ಟೆ,ಆಹಾರ ).</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'less_books_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'less_books_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>33</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ  ಇತರ ಹುಡುಗಿಯರಿಗೆ  ಹೋಲಿಸಿದರೆ, ನನಗೆ ಓದಲು ಕಡಿಮೆ ಅವಕಾಶ ಕೊಡುತ್ತಿದ್ದರು .  </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'less_stydy_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'less_stydy_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>34</td>
        <th> ನಾನು ಕುಟುಂಬಕ್ಕೆ ಹೊರೆಯಾಗಿದ್ದೇನೆಂದು ಹೇಳುತ್ತಿದ್ದರು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'family_burden_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'family_burden_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>35</td>
        <th>ನಾನು ಹುಡುಗಿ ಆಗಿದ್ದ ಕಾರಣಕ್ಕಾಗಿ ನನ್ನ   ಪರವಾಗಿದ್ದರು /ಒಲವು ತೋರಿಸುತ್ತಿದ್ದರು. </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'show_olavu_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'show_olavu_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>36</td>
        <th>ನಾನು ಹುಡುಗಿ ಆಗಿದ್ದ ಕಾರಣಕ್ಕಾಗಿ  ನನಗೆ ಸಂಬಂಧಿಸಿದ  ವಿಷಯಗಲ್ಲಿಯೂ ನನ್ನ ಅಭಿಪ್ರಾಯ  /  ಇಲ್ಲದಿರುವಿಕೆಗಳನ್ನೂ ನಿರ್ಲಕ್ಷ್ಯ ಮಾಡುತ್ತಿದ್ದರು .</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'show_nirlakshya_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'show_nirlakshya_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>37</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ  ಇತರ  ಹುಡುಗಿಯರಿಗಿಂತ  ನನಗೆ ಕಡಿಮೆ  ಸಹಾಯ ಮಾಡಿದರು .  </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'less_helpings_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'less_helpings_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>38</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ  ಹುಡುಗರಿಗಿಂತ ನನ್ನ ನೋವು ಮತ್ತು ಚಿಂತೆಗಳನ್ನು ಕಡಿಮೆ ಅರ್ಥ ಅರ್ಥ ಮಾಡಿಕೊಂಡಂತೆ ಕಾಣುತ್ತಿತ್ತು .</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'less_undestnds_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'less_undestnds_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>39</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ   ಹುಡುಗಿಯರಿಗಿಂತ  ನನ್ನನ್ನು ಕಡಿಮೆ ಹೊಗಳುತ್ತಿದ್ದರು . </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'less_praises_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'less_praises_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>40</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ  ಇತರ  ಹುಡುಗರಿಗಿಂತ ನನ್ನನ್ನು ಅಗತ್ಯಕ್ಕಿಂತ ಹೆಚ್ಚಾಗಿ ರಕ್ಷಣೆ ಮಾಡುತ್ತಿದ್ದರು .</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'high_safes_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'high_safes_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>41</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ   ಇತರ  ಹುಡುಗಿಯರಿಗಿಂತ, ನನ್ನ ನಿರ್ಧಾರಗಳನ್ನು  ನಾನೇ  ತೆಗೆದುಕೊಳ್ಳಲು  ಕಡಿಮೆ ಬಿಡುತ್ತಿದ್ದರು .</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'allows_decionss_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'allow_decisions_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>42</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ  ಇತರ  ಹುಡುಗರಿಗಿಂತ  , ನನಗೆ ಇಷ್ಟವಾದ ಹಾಗೆ ಅಲಂಕಾರ ಮಾಡಲು  ನನಗೆ ಕಡಿಮೆ ಬಿಡುತ್ತಿದ್ದರು . </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'allow_style_as_boys_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'allow_style_as_boys_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>43</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ   ಇತರೆ ಹುಡುಗಿಯರಿಗೆ ಹೋಲಿಸಿದರೆ , ನನಗೆ ಬೇಕಾಗುವ ಸಂಪನ್ಮೂಲಗಳಗನ್ನು  ಕಡಿಮೆ  ಒದಗಿಸುತ್ತಿದ್ದರು .   </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'less_resources_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'less_resources_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>44</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ   ಇತರ  ಹುಡುಗರಿಗೆ ಹೋಲಿಸಿದರೆ ,ನನಗೆ  ಓದಲು ಕಡಿಮೆ ಅವಕಾಶವನ್ನು ಕೊಡಿತ್ತಿದ್ದರು . </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'less_oportunity_study_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'less_oportunity_study_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    
    <tr>
        <td>45</td>
        <th>ನಾನು ಕುಟುಂಬಕ್ಕೆ ಒಂದು ವರದಾನವೆಂದು ಹೇಳುತ್ತಿದ್ದರು . </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'boon_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'boon_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>46</td>
        <th>ನಾನು ಹುಡಿಗಿಯಾಗಿದ್ದ ಕಾರಣ ನನ್ನನ್ನು ಅಪಹಾಸ್ಯ ಮಾಡುವುದು ಮತ್ತು ಟೀಕಿಸಿವುದು ಮಾಡುತ್ತಿದ್ದರು . </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'jokes_onme_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
    <?= $form->field($model, 'jokes_onme_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    
    <tr>
        <td>47</td>
        <th>ನಾನು ಹುಡಿಗಿಯಾಗಿದ್ದ ಕಾರಣ , ನನಗೆ ಸಂಬಂಧಿಸಿದ ವಿಷಯಗಲ್ಲಿಯೂ  ನನ್ನ ಅಭಿಪ್ರಾಯ /ಇಷ್ಟ ಮತ್ತು ಇಷ್ಟದಿಲ್ಲದಿರುವಿಕೆಯನ್ನು ಗೌರವಿಸುತ್ತಿದ್ದರು . </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'respects_me_father')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'respects_me_father')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
        </td>
    </tr>
    </tbody>
    </table>
    
    <div class="hidden">

    <?= $form->field($model, 'score')->input('number',['readonly'=>true])->label(false);?>
     </div>
   

    
    <?php ActiveForm::end(); ?>

</div>
<?php $this->registerJs('
    $("#"+formObj.formName).one("change",function()
    {
        autoSaveObj = formObj;
    });
',\yii\web\View::POS_READY)?>

<?php if($model->not_applicable == 'YES'){
	$this->registerJs('
	setNotApplicableOnLoad("#withBoxShadow","#parentalbondingsecond-not_applicable");
	', \yii\web\View::POS_READY);

}
	$this->registerJs('

	$("#parentalbondingsecond-not_applicable").change(function()
		{	
			var value = $(this).val();
			if(value=="YES")
			{	
				setNotApplicableYes("#withBoxShadow","#parentalbondingsecond-not_applicable");
			}
			else
			{
				setNotApplicableNo("#withBoxShadow","#parentalbondingsecond-not_applicable");
			}
		});
		
		
		
		$("#parentalbondingsecond-any_siblings").change(function()
		{	
            console.log("this is Siblings");
			var value = $(this).val();
			
			if(value == "YES")
			{	
				
				$("#small").show();
                location.reload();
			}
			else
			{
				$("#small").hide();
			}
		});
		
		$("#parentalbondingsecond-any_sisters").change(function()
		{	
			var value = $(this).val();
			
			if(value == "YES")
			{	
				$("#small2").show();
                location.reload();
			}
			else
			{
				$("#small2").hide();
			}
		});
			
', \yii\web\View::POS_READY);
?>

<script type="text/javascript">
    var formObj = {url:"parental-bonding-second/ajax",formName : "formPbis"};
        var calc = function () {
        var totalValue = 0;
        var root= 'parentalbondingsecond';
        var id = '#'+root+'-';
        var clas = '.field-'+root+'-';
        var arr = ['less_helps_mother', 'less_understands_mother', 'less_praise_mother', 'more_secure_mother', 'allow_decisions_mother', 'allowstyle_mother', 'less_books_mother', 
        'less_stydy_mother', 'family_burden_mother', 'show_olavu_mother', 'show_nirlakshya_mother', 'less_helpings_mother', 'less_undestnds_mother', 'less_praises_mother', 
        'high_safes_mother', 'allows_decionss_mother', 'allow_style_as_boys_mother', 'less_resources_mother', 'less_oportunity_study_mother', 'boon_mother', 'jokes_onme_mother', 
        'respects_me_mother', 'less_helps_father', 'less_understands_father', 'less_praise_father', 'more_secure_father', 'allow_decisions_father', 'allowstyle_father', 
        'less_books_father', 'less_stydy_father', 'family_burden_father', 'show_olavu_father', 'show_nirlakshya_father', 'less_helpings_father', 'less_undestnds_father', 
        'less_praises_father', 'high_safes_father', 'allows_decionss_father', 'allow_style_as_boys_father', 'less_resources_father', 'less_oportunity_study_father', 
        'boon_father', 'jokes_onme_father', 'respects_me_father'];
        
        for (var i in arr) {
            var val = $(clas + arr[i] + ' input[type=radio]:checked').val();
            if (parseInt(val)) {
                totalValue += parseInt(val);
            }
        }
        

        $(id+'score').val(totalValue);
        autoSaveObj = formObj;
        
    }

</script>
<style>
#small table{
	height:175px;
}
#small2 table{
	height:175px;
}
.na{
	height:90px !important;
}
</style>
