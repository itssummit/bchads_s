<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ParentalBondingSecond */

$this->title = 'Update Parental Bonding Second: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Parental Bonding Seconds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="parental-bonding-second-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
