﻿<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$keyPair = [-15 => '-15: Child Not Co-Operative For Assessment',
          -14 => '-14: Not Available For Assessment',
          -11 => '-11: Inadequate Information',
          -10 => '-10: Not Applicable',
          -9 => '-9: Missing',
          -8 => '-8: Refused',
          -7 => '-7: Partner Accompanied Missing',
          -6 => '-6: Family Accompanied Missing',
          -5 => '-5: Missing:Not Asked',
          -3 => '-3: No Such Relationship/No Partner'];
/* @var $this yii\web\View */
/* @var $model app\models\Multidimentional */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile('@web/js/scales.js',['depends' => [\yii\web\JqueryAsset::className()]]);
?>


<div  class="multidimentional-form">

    <?php $form = ActiveForm::begin(['id' => 'formMultidimentional']); ?>
    <div id="withBoxShadow" class="col-xs-12 col-md-12 col-lg-12 col-sm-12">
    <div id="idMdscale">
    <caption><b>ಸೂಚನೆಗಳು : ಈ ಕೆಳಗಿನ ಹೇಳಿಕೆಗಳ ಬಗ್ಗೆ ನಿಮ್ಮ ಅನಿಸಿಕೆ ಏನು ಎಂಬುದನ್ನು ತಿಳಿಯುವ ಆಸಕ್ತಿಯನ್ನು ಹೊಂದಿದ್ದೇವೆ . ಪ್ರತಿಯೊಂದು ಹೇಳಿಕೆಯನ್ನು ಎಚ್ಚರಿಕೆಯಿಂದ ಓದಿ . ಪ್ರತಿಯೊಂದು ಹೇಳಿಕೆಯ ಬಗ್ಗೆ ನಿಮ್ಮ ಅನಿಸಿಕೆ ಏನು ಎಂಬುದನ್ನು ಸೂಚಿಸಿ    </b></caption>
        <h4><b>ಕಳೆದ ಭೇಟಿಯಿಂದ </b></h4>
        <table border="1" class="responsive-scales">
            <thead>
                <tr>
                    <th>ಕ್ರ .ಸಂ) ಹೇಳಿಕೆಗಳು </th>
                    <th>ಅತೀ ಹೆಚ್ಚಾಗಿ ಒಪ್ಪುವುದಿಲ್ಲ </th>
                    <th>ಹೆಚ್ಚಾಗಿ ಒಪ್ಪುವುದಿಲ್ಲ </th>
                    <th> ಸಾಧಾರಣವಾಗಿ ಒಪ್ಪುವುದಿಲ್ಲ </th>
                    <th> ಒಪ್ಪಬಹುದು ಅಥವಾ ಒಪ್ಪದೆಯೂ ಇರಬಹುದು</th>
                    <th> ಸಾಧಾರಣವಾಗಿ ಒಪ್ಪುತ್ತೇನೆ </th>
                    <th>ಹೆಚ್ಚಾಗಿ ಒಪ್ಪುತ್ತೇನೆ </th>
                    <th> ಅತೀ ಹೆಚ್ಚಾಗಿ ಒಪ್ಪುತ್ತೇನೆ </th>
                    <th>Other Answers</th>
                </tr> 
            </thead>
            <tbody> 
            <tr>
                <td>1) ನನಗೆ ಅಗತ್ಯವಿದ್ದಾಗ   ಒಬ್ಬ ವಿಶೇಷ  ವ್ಯಕ್ತಿಯು  ನನ್ನೊಂದಿಗಿದ್ದಾರೆ  </td>
                <?php for($i = 1;$i < 8;$i++):?>
                    <td>
                        <?= $form->field($model,'special_person_need')->radio(['label' => $i, 'value' => $i])?>
                    </td>
                <?php endfor;?>
            <td>
                <?= $form->field($model, 'special_person_need')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
            </td>
            </tr>
    
            <tr>
                <td>2) ನನ್ನ ಸಂತೋಷ ಮತ್ತು  ದುಃಖಗಳನ್ನು ಹಂಚಿಕೊಳ್ಳಲು ನನಗೊಬ್ಬ ವಿಶೇಷ ವ್ಯಕ್ತಿ ಇದ್ದಾರೆ.</td>
                
                <?php for($i = 1;$i < 8;$i++):?>
                <td><?= $form->field($model, 'special_person_share')->radio(['label' => $i, 'value' => $i])?>
                    </td>
                <?php endfor;?>
                <td>
            <?= $form->field($model, 'special_person_share')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
                    
                </td>
            </tr>
    
            <tr>
                <td>3) ನನ್ನ ಕುಟು೦ಬದವರು ಖಂಡಿತವಾಗಿಯೂ ನನಗೆ ಸಹಾಯ ಮಾಡಲು ಪ್ರಯತ್ನಿಸುತ್ತಾರೆ. </td>
                <?php for($i = 1;$i < 8;$i++):?>
                <td><?= $form->field($model, 'family_help')->radio(['label' => $i, 'value' => $i])?>
                    </td>
                <?php endfor;?>
                <td>
            <?= $form->field($model, 'family_help')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
                
                </td>
            </tr>
    
            <tr>
                <td>4) ನನ್ನ ಕುಟುಂಬದವರಿಂದ ಬೇಕಾಗಿರುವಂತಹ ಭಾವನಾತ್ಮಕ ಸಹಾಯ ಮತು ಬೆಂಬಲವು ನನಗೆ ದೊರಕುತ್ತಿದೆ.</td>
                <?php for($i = 1;$i < 8;$i++):?>
                <td ><?= $form->field($model, 'emotional_help_family')->radio(['label' => $i, 'value' => $i])?>
                    </td>
                <?php endfor;?>
                <td>
                <?= $form->field($model, 'emotional_help_family')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
                </td>
            </tr>
    
            <tr>
                <td>5) ನಾನು ನಿಜವಾಗಿಯೂ ಆರಾಮವಾಗಿರಲು ಸಾಧ್ಯವಾಗುವಂತೆ ಮಾಡುವ ಒಬ್ಬ ವಿಶೇಷ ವ್ಯಕ್ತಿಯನ್ನು ಹೊಂದಿದ್ದೇನೆ.</td>
                <?php for($i = 1;$i < 8;$i++):?>
                <td ><?= $form->field($model, 'special_person_comfort')->radio(['label' => $i, 'value' => $i])?>
                    </td>
                <?php endfor;?>
                <td>
                <?= $form->field($model, 'special_person_comfort')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>       
                </td>
            </tr>
    
            <tr>
                <td>6) ನನ್ನ ಸ್ನೇಹಿತರು ನಿಜವಾಗಲೂ ನನಗೆ ಸಹಾಯ ಮಾಡಲು ಪ್ರಯತ್ನಿಸುತ್ತಾರೆ.</td>
                <?php for($i = 1;$i < 8;$i++):?>
                <td ><?= $form->field($model, 'friend_help')->radio(['label' => $i, 'value' => $i])?>
                    </td>
                <?php endfor;?>
                <td>
                <?= $form->field($model, 'friend_help')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
                </td>
            </tr>
    
            <tr>
                <td>7) ಯಾವುದಾದರೂ ವಿಷಯ ಸರಿಯಿಲ್ಲ ಎನಿಸಿದಲ್ಲಿ ನಾನು ನನ್ನ ಸ್ನೇಹಿತರ ಸಲಹೆ/ಸಹಾಯವನ್ನು ತೆಗೆದುಕೊಳ್ಳಬಹುದು.</td>
                <?php for($i = 1;$i < 8;$i++):?>
                <td ><?= $form->field($model, 'friends_things_go_wrong')->radio(['label' => $i, 'value' => $i])?>
                    </td>
                <?php endfor;?>
                <td>
                <?= $form->field($model, 'friends_things_go_wrong')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
                </td>
            </tr>
    
            <tr>
                <td>8) ನನ್ನ ಕುಟುಂಬದವರೊಂದಿಗೆ ನನ್ನ ಸಮಸ್ಯೆಗಳ ಬಗ್ಗೆ ಮಾತನಾಡಬಹುದು  .</td>
                <?php for($i = 1;$i < 8;$i++):?>
                <td ><?= $form->field($model, 'problems_family_talk')->radio(['label' => $i, 'value' => $i])?>
                    </td>
                <?php endfor;?>
                <td>
                <?= $form->field($model, 'problems_family_talk')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
                </td>
            </tr>
    
            <tr>
                <td>9) ನನ್ನ ಸಂತೋಷ ಮತ್ತು  ದುಃಖಗಳನ್ನು ಹಂಚಿಕೊಳ್ಳಲು ನಾನು ಸ್ನೇಹಿತರನ್ನು ಹೊಂದಿದ್ದೇನೆ.</td>
                <?php for($i = 1;$i < 8;$i++):?>
                <td ><?= $form->field($model, 'friends_share')->radio(['label' => $i, 'value' => $i])?>
                    </td>
                <?php endfor;?>
                <td>
                <?= $form->field($model, 'friends_share')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>    
                </td>
            </tr>
    
            <tr>
                <td>10) ನನ್ನ ಭಾವನೆಗಳ ಬಗ್ಗೆ ಕಾಳಜಿವಹಿಸುವಂತಹ ವಿಶೇಷವಾದ ವ್ಯಕ್ತಿಯು ನನ್ನ ಜೀವನದಲ್ಲಿ  ಇದ್ದಾರೆ </td>
                <?php for($i = 1;$i < 8;$i++):?>
                <td ><?= $form->field($model, 'special_person_feelings')->radio(['label' => $i, 'value' => $i])?>
                    </td>
                <?php endfor;?>
                <td>
                <?= $form->field($model, 'special_person_feelings')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>  
                </td>
            </tr>
    
            <tr>
                <td>11) ನಾನು ನಿರ್ಧಾರಗಳನ್ನು ತೆಗೆದುಕೊಳ್ಳಲು ನನ್ನ ಕುಟು೦ಬದವರು ನನಗೆ ಸಹಾಯ ಮಾಡಲು ಒಪ್ಪುತ್ತಾರೆ .</td>
                <?php for($i = 1;$i < 8;$i++):?>
                <td ><?= $form->field($model, 'family_help_decision')->radio(['label' => $i, 'value' => $i])?>
                    </td>
                <?php endfor;?>
                <td>
                <?= $form->field($model, 'family_help_decision')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
                </td>
            </tr>
    
            <tr>
                <td>12) ನನ್ನ ಸ್ನೇಹಿತರೊಂದಿಗೆ ನನ್ನ ಸಮಸ್ಯೆಗಳ ಬಗ್ಗೆ ಮಾತನಾಡಬಹುದು.</td>
                <?php for($i = 1;$i < 8;$i++):?>
                <td ><?= $form->field($model, 'talk_problems_friends')->radio(['label' => $i, 'value' => $i])?>
                    </td>
                <?php endfor;?>
                <td>
                <?= $form->field($model, 'talk_problems_friends')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
                </td>
            </tr>
    
            <tr>
               
                <td id="scaleflag" colspan="7" class="hidden"> <?= $form->field($model, 'score')->input('number',['readonly'=>true])->label(false);?></td>
            </tr>
    
    
            </tbody>
    </table>
    </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>  
<?php $this->registerJs('
    $("#"+formObj.formName).one("change",function()
    {
        autoSaveObj = formObj;
    });
',\yii\web\View::POS_READY)?>
<script type="text/javascript">
    var formObj = {url:"multidimentional/ajax",formName : "formMultidimentional"};
    var calc = function () {
        var totalValue = 0;
        var root = 'multidimentional';
        var id = '#'+root+'-';
        var clas = '.field-'+root+'-';
        var arr = ['special_person_need', 'special_person_share', 'family_help', 'emotional_help_family', 'special_person_comfort', 'friend_help', 'friends_things_go_wrong', 'problems_family_talk', 'friends_share', 'special_person_feelings', 'family_help_decision', 'talk_problems_friends'];

        for (var i in arr) {
            var val = $(clas + arr[i] + ' input[type=radio]:checked').val();
            if (parseInt(val) >= 0) {
                totalValue += parseInt(val);
            }
        }
        
        $(id+'score').val(totalValue);
        autoSaveObj = formObj;
    }
</script>

    
    
