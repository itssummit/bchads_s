<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Multidimentional */

$this->title = 'Multidimentional';
$this->params['breadcrumbs'][] = ['label' => 'Multidimentionals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="multidimentional-create">

    <h1 align="center">Multidimensional Scale of Perceived Social Support</h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
