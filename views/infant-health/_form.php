<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\InfantHealth */
/* @var $form yii\widgets\ActiveForm *//**/
if($model->dod)
{
	$model->dod = date('d-M-Y',strtotime($model->dod));
}
$values = [1 => 1,2 => 2,3 => 3,4 => 4,5 => 5,6 => 6,7 => 7,8 => 8,9 => 9,10 => 10,11 => 11,12 => 12,13 => 13,14 => 14,15 => 15,16 => 16,17 => 17,18 => 18,19 => 19,20 => 20,21 =>21,22 => 22,23 => 23,24 => 24,25 => 25,26 => 26,27 => 27,28 => 28,29 => 29,30 => 30,31=>31,32=>32,33=>33,34=>34,35=>35];
$val= [1=>1,2=>2,3=>3];
$model->hospitalization  = ($model->hospitalization)?explode(',',$model->hospitalization):'';
$model->nicu_care  = ($model->nicu_care)?explode(',',$model->nicu_care):'';
$model->neonatal_jaundice  = ($model->neonatal_jaundice)?explode(',',$model->neonatal_jaundice):'';
$model->icu_care  = ($model->icu_care)?explode(',',$model->icu_care):'';
$model->fevers  = ($model->fevers)?explode(',',$model->fevers):'';
$model->exanthaemotous_fever  = ($model->exanthaemotous_fever)?explode(',',$model->exanthaemotous_fever):'';
$model->seizures  = ($model->seizures)?explode(',',$model->seizures):'';
$model->asthma  = ($model->asthma)?explode(',',$model->asthma):'';
$model->respiratory_illness  = ($model->respiratory_illness)?explode(',',$model->respiratory_illness):'';
$model->diarrhoea  = ($model->diarrhoea)?explode(',',$model->diarrhoea):'';
$model->other_details  = ($model->other_details)?explode(',',$model->other_details):'';
$this->registerJsFile('@web/js/scaleinfant.js',['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<?php $form = ActiveForm::begin(['id' => 'formInfanthealth','fieldConfig' => ['template' => '{input}','options' => ['tag' => 'span']]]); ?>

 <div id="fullform">
	<div  class="table-responsive">
	<div style="margin-left:20px;">
		<div class="row">
		<div class="col-lg-4">
		<label>Date Of Delivery</label>
		<?= $form->field($model, 'dod')->widget(DatePicker::className(),['type' => DatePicker::TYPE_COMPONENT_APPEND,'removeButton' => false,'pluginOptions'=>['endDate' => '+0d','format' => 'dd-M-yyyy','orientation' => 'left']])->label(false); ?>	
		</div>
		</div><br>
		<div class="row">
		<div class="col-lg-4">
		<label>Age Of Infant(In Months)</label>
		<?= $form->field($model, 'ageofinfant')->textInput(['type' =>'number','max'=>'36'])->label(false) ?>
		</div>
		</div>
	</div><br>
	<div style="margin-left:20px;">
	<caption>1.Physical Health</caption>
	</div>
	
		<table class="table table-bordered">
			<thead>
				<tr>
				<th>Months</th>
				<?php for($i=1;$i<36;$i++):?>
				<th><?php echo $i?></th>
				<?php endfor;?>
			
				</tr>
			</thead>
			<tbody>
			<tr>
				<td><label>1) Hospitalization</label></td>
				<?= $form->field($model, 'hospitalization')->checkboxList($values,
				['item'=> function($index,$label,$name,$checked,$value){
						return "<td>".Html::checkbox($name,$checked,['value' => $value,'label'=>$label])."</td>";
					}
				]); ?>
			</tr>
			<tr>	
				<td><label>2) NICU care</label></td>
				<?= $form->field($model, 'nicu_care')->checkboxList($val,
				['item'=> function($index,$label,$name,$checked,$value){
						return "<td>".Html::checkbox($name,$checked,['value' => $value,'label'=>$label])."</td>";
					}
				]); ?>
				
			</tr>	
			<tr>
				<td><label>3) Neonatal Jaundice</label></td>
				<?= $form->field($model, 'neonatal_jaundice')->checkboxList($val,
				['item'=> function($index,$label,$name,$checked,$value){
						return "<td>".Html::checkbox($name,$checked,['value' => $value,'label'=>$label])."</td>";
					}
				]); ?>
			</tr>
			<tr>
				<td><label>4) ICU care</label></td>
				<?= $form->field($model, 'icu_care')->checkboxList($values,
				['item'=> function($index,$label,$name,$checked,$value){
						return "<td>".Html::checkbox($name,$checked,['value' => $value,'label'=>$label])."</td>";
					}
				]); ?>
			</tr>
			<tr>		
				<td><label>5) Fevers</label></td>
				<?= $form->field($model, 'fevers')->checkboxList($values,
				['item'=> function($index,$label,$name,$checked,$value){
						return "<td>".Html::checkbox($name,$checked,['value' => $value,'label'=>$label])."</td>";
					}
				]); ?>
			</tr>
			<tr>		
				<td><label>6) Exanthemotous Fevers</label></td>
				<?= $form->field($model, 'exanthaemotous_fever')->checkboxList($values,
				['item'=> function($index,$label,$name,$checked,$value){
						return "<td>".Html::checkbox($name,$checked,['value' => $value,'label'=>$label])."</td>";
					}
				]); ?>
			</tr>
			<tr>
				<td><label>7) Seizures</label></td>
				<?= $form->field($model, 'seizures')->checkboxList($values,
				['item'=> function($index,$label,$name,$checked,$value){
						return "<td>".Html::checkbox($name,$checked,['value' => $value,'label'=>$label])."</td>";
					}
				]); ?>
			</tr>
			<tr>
				<td><label>8) Asthma</label></td>
				<?= $form->field($model, 'asthma')->checkboxList($values,
				['item'=> function($index,$label,$name,$checked,$value){
						return "<td>".Html::checkbox($name,$checked,['value' => $value,'label'=>$label])."</td>";
					}
				]); ?>
			</tr>	
			<tr>
				<td><label>9) Other respiratory Illness</label></td>
				<?= $form->field($model, 'respiratory_illness')->checkboxList($values,
				['item'=> function($index,$label,$name,$checked,$value){
						return "<td>".Html::checkbox($name,$checked,['value' => $value,'label'=>$label])."</td>";
					}
				]); ?>
			</tr>
			<tr>
				<td><label>10) Diarrhoea</label></td>
				<?= $form->field($model, 'diarrhoea')->checkboxList($values,
				['item'=> function($index,$label,$name,$checked,$value){
						return "<td>".Html::checkbox($name,$checked,['value' => $value,'label'=>$label])."</td>";
					}
				]); ?>
			</tr>
			
			<!--<tr>
				<td><label>No Of visits to pediatrician</label></td>
				< ?= $form->field($model, 'pediatrician_visits')->checkboxList($values,
				['item'=> function($index,$label,$name,$checked,$value){
						return "<td>".Html::checkbox($name,$checked,['value' => $value,'label'=>$label])."</td>";
					}
				]); ?>
			</tr>-->
				
						
			</tbody>
		</table><br>
	</div>
		<div id="withBoxShadow" class="table-responsive">
		<table border="2" color="black;">
		<tbody>
		
		<tr>
		<th>11) Pedeatrician Visits</th>
		<th>How Many Times Pedeatrician Visited ?</th>
		<th>Pedeatrician Visits</th>
		<th>How Many Times Pedeatrician Visited ?</th>
		</tr>
		
		<tr>
		<th>1 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit1')->textInput(["placeholder"=>"1 month"]); ?>
		</div>
		</td> 
		<th>19 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit19')->textInput(["placeholder"=>"19 month"]); ?>
		</div>
		</td>
		</tr>
		
		<tr>
		<th>2 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit2')->textInput(["placeholder"=>"2 month"]); ?>
		</div>
		</td> 
		<th>20 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit20')->textInput(["placeholder"=>"20 month"]); ?>
		</div>
		</td>
		</tr>
		
		<tr>
		<th>3 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit3')->textInput(["placeholder"=>"3 month"]); ?>
		</div>
		</td> 
		<th>21 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit21')->textInput(["placeholder"=>"21 month"]); ?>
		</div>
		</td>
		</tr>
		
		<tr>
		<th>4 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit4')->textInput(["placeholder"=>"4 month"]); ?>
		</div>
		</td> 
		<th>22 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit22')->textInput(["placeholder"=>"22 month"]); ?>
		</div>
		</td>
		</tr>
		<tr style="display: none;">
			<td><?= $form->field($model, 'valid')->checkbox(); ?></td>
		</tr>
		
		<tr>
		<th>5 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit5')->textInput(["placeholder"=>"5 month"]); ?>
		</div>
		</td> 
		<th>23 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit23')->textInput(["placeholder"=>"23 month"]); ?>
		</div>
		</td>
		</tr>
		
		<tr>
		<th>6 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit6')->textInput(["placeholder"=>"6 month"]); ?>
		</div>
		</td> 
		<th>24 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit24')->textInput(["placeholder"=>"24 month"]); ?>
		</div>
		</td>
		</tr>
		
		<tr>
		<th>7 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit7')->textInput(["placeholder"=>"7 month"]); ?>
		</div>
		</td> 
		<th>25 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit25')->textInput(["placeholder"=>"25 month"]); ?>
		</div>
		</td>
		</tr>
		
		<tr>
		<th>8 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit8')->textInput(["placeholder"=>"8 month"]); ?>
		</div>
		</td> 
		<th>26 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit26')->textInput(["placeholder"=>"26 month"]); ?>
		</div>
		</td>
		</tr>
		
		<tr>
		<th>9 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit9')->textInput(["placeholder"=>"9 month"]); ?>
		</div>
		</td> 
		<th>27 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit27')->textInput(["placeholder"=>"27 month"]); ?>
		</div>
		</td>
		</tr>
		
		<tr>
		<th>10 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit10')->textInput(["placeholder"=>"10 month"]); ?>
		</div>
		</td> 
		<th>28 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit28')->textInput(["placeholder"=>"28 month"]); ?>
		</div>
		</td>
		</tr>
		
		<tr>
		<th>11 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit11')->textInput(["placeholder"=>"11 month"]); ?>
		</div>
		</td> 
		<th>29 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit29')->textInput(["placeholder"=>"29 month"]); ?>
		</div>
		</td>
		</tr>
		
		<tr>
		<th>12 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit12')->textInput(["placeholder"=>"12 month"]); ?>
		</div>
		</td> 
		<th>30 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit30')->textInput(["placeholder"=>"30 month"]); ?>
		</div>
		</td>
		</tr>
		
		<tr>
		<th>13 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit13')->textInput(["placeholder"=>"13 month"]); ?>
		</div>
		</td> 
		<th>31 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit31')->textInput(["placeholder"=>"31 month"]); ?>
		</div>
		</td>
		</tr>
		
		<tr>
		<th>14 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit14')->textInput(["placeholder"=>"14 month"]); ?>
		</div>
		</td> 
		<th>32 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit32')->textInput(["placeholder"=>"32 month"]); ?>
		</div>
		</td>
		</tr>
		
		<tr>
		<th>15 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit15')->textInput(["placeholder"=>"15 month"]); ?>
		</div>
		</td> 
		<th>33 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit33')->textInput(["placeholder"=>"33 month"]); ?>
		</div>
		</td>
		</tr>
		
		<tr>
		<th>16 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit16')->textInput(["placeholder"=>"16 month"]); ?>
		</div>
		</td> 
		<th>34 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit34')->textInput(["placeholder"=>"34 month"]); ?>
		</div>
		</td>
		</tr>
		
		<tr>
		<th>17 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit17')->textInput(["placeholder"=>"17 month"]); ?>
		</div>
		</td> 
		<th>35 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit35')->textInput(["placeholder"=>"35 month"]); ?>
		</div>
		</td>
		</tr>
		
		<tr>
		<th>18 Month</th>
		<td>
		<div class="col-lg-6">
		<?= $form->field($model, 'doc_visit18')->textInput(["placeholder"=>"18 month","class"=>"18"]); ?>
		</div>
		</td> 
		<td></td>
		<td></td>
		</tr>
		</tbody>
		</table>
		<button id="resettozero" type="button">Make Rest as 0</button>
	<div class="row" style="margin:20px;">
	<div class="col-lg-12 col-md-12">
	<label>Others Details</label>
	<?= $form->field($model, 'others')->textarea(['rows' => '5'])->label(false) ?>
	
	</div>
	</div>
	
	
	
	<div class="indent">
	<caption><b>12) Other Details</b><caption>
	<table style="height:100px;width:100%;">
			
		<tr>
				
				<?= $form->field($model, 'other_details')->checkboxList($values,
				['item'=> function($index,$label,$name,$checked,$value){
						return "<td>".Html::checkbox($name,$checked,['value' => $value,'label'=>$label])."</td>";
					}
				]); ?>
		</tr>	
	</table>	
	</div>
	
	
	<div class="col-lg-12">
	<b>2.However careful a person is, most children have accidents at some time or other ,please tell us on the set of questions below about the times your child has had an accident , at what age and if there was any treatment given
	</b><br>
	</div>
	<div class="col-lg-12">
	<table border="1">
	<tbody>
	<tr>
	<th>Has your child ever ...</th>
	<th>How many times ? (please tick a box)</th>
	<th>How old was he/she in months ?</th>
	<th>what did the person with your child do about the accidents ?(please tick a box)</th>
	</tr>
	<tr>
	<th>a)Been burnt or scalded</th>
	<td>
	<?= $form->field($model, 'how_many_times_burnt')->radio(['label' => 'Once', 'value' => 0])->label(false);?><br>
	<?= $form->field($model, 'how_many_times_burnt')->radio(['label' => 'Twice', 'value' => 1])->label(false);?><br>
	<?= $form->field($model, 'how_many_times_burnt')->radio(['label' => '3 or more', 'value' => 2])->label(false);?><br>
	<?= $form->field($model, 'how_many_times_burnt')->radio(['label' => 'Never', 'value' => 3])->label(false);?><br>
	</td>
	<td>
	<?= $form->field($model, 'how_old_was_she_while_burnt')->textInput(['placeholder'=>'Age at first time'])->label(false) ?>
	<?= $form->field($model, 'how_old_was_she_while_burnt2')->textInput(['placeholder'=>'Age at second time'])->label(false) ?>
	<?= $form->field($model, 'how_old_was_she_while_burnt3')->textInput(['placeholder'=>'Age at third time'])->label(false) ?>
	</td>
	<td>
	<?= $form->field($model, 'what_did_person_with_child_do_about_burnt')->radio(['label' => 'Nothing', 'value' => 0])->label(false);?><br>
	<?= $form->field($model, 'what_did_person_with_child_do_about_burnt')->radio(['label' => 'Treated child themselves', 'value' => 1])->label(false);?><br>
	<?= $form->field($model, 'what_did_person_with_child_do_about_burnt')->radio(['label' => 'Took child to GP', 'value' => 2])->label(false);?><br>
	<?= $form->field($model, 'what_did_person_with_child_do_about_burnt')->radio(['label' => 'Took child to hospital', 'value' => 3])->label(false);?><br>
	<?= $form->field($model, 'what_did_person_with_child_do_about_burnt')->radio(['label' => 'Other', 'value' => 4])->label(false);?><br>
	<?= $form->field($model, 'what_did_person_with_child_do_about_burnt')->radio(['label' => 'NA', 'value' => -10])->label(false);?><br>
	<?= $form->field($model, 'what_did_person_with_child_do_about_burnt_others')->textInput()->label(false) ?>
	</td>
	</tr>
	
	<tr>
	<th>b)Been dropped or had a bad fall</th>
	<td>
	<?= $form->field($model, 'how_many_times_dropped')->radio(['label' => 'Once', 'value' => 0])->label(false);?><br>
	<?= $form->field($model, 'how_many_times_dropped')->radio(['label' => 'Twice', 'value' => 1])->label(false);?><br>
	<?= $form->field($model, 'how_many_times_dropped')->radio(['label' => '3 or more', 'value' => 2])->label(false);?><br>
	<?= $form->field($model, 'how_many_times_dropped')->radio(['label' => 'Never', 'value' => 3])->label(false);?><br>
	</td>
	<td>
	<?= $form->field($model, 'how_old_was_she_while_dropped')->textInput(['placeholder'=>'Age at first time'])->label(false) ?>
	<?= $form->field($model, 'how_old_was_she_while_dropped2')->textInput(['placeholder'=>'Age at second time'])->label(false) ?>
	<?= $form->field($model, 'how_old_was_she_while_dropped3')->textInput(['placeholder'=>'Age at third time'])->label(false) ?>
	</td>
	<td>
	<?= $form->field($model, 'what_did_person_with_child_do_about_dropp')->radio(['label' => 'Nothing', 'value' => 0])->label(false);?><br>
	<?= $form->field($model, 'what_did_person_with_child_do_about_dropp')->radio(['label' => 'Treated child themselves', 'value' => 1])->label(false);?><br>
	<?= $form->field($model, 'what_did_person_with_child_do_about_dropp')->radio(['label' => 'Took child to GP', 'value' => 2])->label(false);?><br>
	<?= $form->field($model, 'what_did_person_with_child_do_about_dropp')->radio(['label' => 'Took child to hospital', 'value' => 3])->label(false);?><br>
	<?= $form->field($model, 'what_did_person_with_child_do_about_dropp')->radio(['label' => 'Other', 'value' => 4])->label(false);?><br>
	<?= $form->field($model, 'what_did_person_with_child_do_about_dropp')->radio(['label' => 'NA', 'value' => -10])->label(false);?><br>
	<?= $form->field($model, 'what_did_person_with_child_do_about_dropp_others')->textInput()->label(false) ?>
	</td>
	</tr>
	
	
	
	
	<tr>
	<th>c)Swallowed anything she or he shouldn`t (e.g ,pills ,buttons,disinfectants)</th>
	<td>
	<?= $form->field($model, 'how_many_times_swallowed')->radio(['label' => 'Once', 'value' => 0])->label(false);?><br>
	<?= $form->field($model, 'how_many_times_swallowed')->radio(['label' => 'Twice', 'value' => 1])->label(false);?><br>
	<?= $form->field($model, 'how_many_times_swallowed')->radio(['label' => '3 or more', 'value' => 2])->label(false);?><br>
	<?= $form->field($model, 'how_many_times_swallowed')->radio(['label' => 'Never', 'value' => 3])->label(false);?><br>
	</td>
	<td>
	<?= $form->field($model, 'how_old_was_she_while_swallowed')->textInput(['placeholder'=>'Age at first time'])->label(false) ?>
	<?= $form->field($model, 'how_old_was_she_while_swallowed2')->textInput(['placeholder'=>'Age at second time'])->label(false) ?>
	<?= $form->field($model, 'how_old_was_she_while_swallowed3')->textInput(['placeholder'=>'Age at third time'])->label(false) ?>
	</td>
	<td>
	<?= $form->field($model, 'what_did_person_with_child_do_about_swallow')->radio(['label' => 'Nothing', 'value' => 0])->label(false);?><br>
	<?= $form->field($model, 'what_did_person_with_child_do_about_swallow')->radio(['label' => 'Treated child themselves', 'value' => 1])->label(false);?><br>
	<?= $form->field($model, 'what_did_person_with_child_do_about_swallow')->radio(['label' => 'Took child to GP', 'value' => 2])->label(false);?><br>
	<?= $form->field($model, 'what_did_person_with_child_do_about_swallow')->radio(['label' => 'Took child to hospital', 'value' => 3])->label(false);?><br>
	<?= $form->field($model, 'what_did_person_with_child_do_about_swallow')->radio(['label' => 'Other', 'value' => 4])->label(false);?><br>
	<?= $form->field($model, 'what_did_person_with_child_do_about_swallow')->radio(['label' => 'NA', 'value' => -10])->label(false);?><br>
	<?= $form->field($model, 'what_did_person_with_child_do_about_swallow_others')->textInput()->label(false) ?>
	</td>
	</tr>
	
	<tr>
	<th>d)Had any other accidents or injuries ?</th>
	<td>
	<?= $form->field($model, 'how_many_times_others')->radio(['label' => 'Once', 'value' => 0])->label(false);?><br>
	<?= $form->field($model, 'how_many_times_others')->radio(['label' => 'Twice', 'value' => 1])->label(false);?><br>
	<?= $form->field($model, 'how_many_times_others')->radio(['label' => '3 or more', 'value' => 2])->label(false);?><br>
	<?= $form->field($model, 'how_many_times_others')->radio(['label' => 'Never', 'value' => 3])->label(false);?><br>
	</td>
	<td>
	<?= $form->field($model, 'how_old_was_she_while_other')->textInput(['placeholder'=>'Age at first time'])->label(false) ?>
	<?= $form->field($model, 'how_old_was_she_while_other2')->textInput(['placeholder'=>'Age at second time'])->label(false) ?>
	<?= $form->field($model, 'how_old_was_she_while_other3')->textInput(['placeholder'=>'Age at third time'])->label(false) ?>
	</td>
	<td>
	<?= $form->field($model, 'what_did_person_with_child_do_about_other')->radio(['label' => 'Nothing', 'value' => 0])->label(false);?><br>
	<?= $form->field($model, 'what_did_person_with_child_do_about_other')->radio(['label' => 'Treated child themselves', 'value' => 1])->label(false);?><br>
	<?= $form->field($model, 'what_did_person_with_child_do_about_other')->radio(['label' => 'Took child to GP', 'value' => 2])->label(false);?><br>
	<?= $form->field($model, 'what_did_person_with_child_do_about_other')->radio(['label' => 'Took child to hospital', 'value' => 3])->label(false);?><br>
	<?= $form->field($model, 'what_did_person_with_child_do_about_other')->radio(['label' => 'Other', 'value' => 4])->label(false);?><br>
	<?= $form->field($model, 'what_did_person_with_child_do_about_other')->radio(['label' => 'NA', 'value' => -10])->label(false);?><br>
	<?= $form->field($model, 'what_did_person_with_child_do_about_other_others')->textInput()->label(false) ?>
	</td>
	</tr>
	
	
	
	
	
	</tbody>
	</table>
	
	</div>
	<div class="row" style="margin:20px;">
	<div class="col-lg-12 col-md-12">
	<label>Others Details</label>
	<?= $form->field($model, 'others_notes')->textarea(['rows' => '5'])->label(false) ?>
	
	</div>
	</div>

	<div class="col-lg-12" id ="ihscale">
	<b>3.please tell us how often during the day your child is in room or enclosed place where people are smoking?</b>
	<table border="1" class="bordered table">
	<tbody>
	<tr>
	<td></td>
	<th>Not at all</th>
	<th>less than 1 hour</th>
	<th>1 or 2 hours</th>
	<th>3,4,or 5 hours</th>
	<th>More than 5</th>
	<th>All the time</th>
	</tr>
	<tr>
	<th>a)On weekdays ?</th>
	<?php for($i = 0;$i < 6;$i++):?>
        <td><?= $form->field($model, 'how_often_on_weekdays')->radio(['label' => $i, 'value' => $i])?></td>
    <?php endfor;?>
	
	
	
	</tr>
	<tr>
	<th>a)At weekends ?</th>
	<?php for($i = 0;$i < 6;$i++):?>
        <td><?= $form->field($model, 'how_often_at_weekdays')->radio(['label' => $i, 'value' => $i])?></td>
    <?php endfor;?>
	</tr>
	</tbody>
	</table>
	
	</div>
	<div class="col-lg-12" id = "ihimm">
	<b>4)Immunization</b>
	<table border="1">
	<tbody>
	<tr>
	<th>Time</th>
	<th>Vaccination</th>
	<td></td>
	</tr>
	<tr>
	<th>1.5 months</th>
	<th>BCG,DPT,Polio</th>
	<td><b>
	<?= $form->field($model, 't1_5m')->dropDownList(['YES'=>'YES','NO'=>'NO'],['prompt' => '  '])->label(false)?></b></td>
	</tr>
	<tr>
	<th>2.5 months</th>
	<th>DPT-2,Polio-2</th>
	<td><b>
	<?= $form->field($model, 't2_5m')->dropDownList(['YES'=>'YES','NO'=>'NO'],['prompt' => '  '])->label(false)?>
	</b></td>
	</tr>
	<tr>
	<th>3.5 months</th>
	<th>DPT-3,Polio-3</th>
	<td><b>
	<?= $form->field($model, 't3_5m')->dropDownList(['YES'=>'YES','NO'=>'NO'],['prompt' => '  '])->label(false)?>
	</b></td>
	</tr>
	<tr>
	<th>9 months</th>
	<th>Measles</th>
	<td><b>
	<?= $form->field($model, 't9m')->dropDownList(['YES'=>'YES','NO'=>'NO'],['prompt' => '  '])->label(false)?>
	</b></td>
	</tr>
	<tr>
	<th>16 -24 months</th>
	<th>DPT-booster,Polio-booster</th>
	<td><b>
	<?= $form->field($model, 't16_24m')->dropDownList(['YES'=>'YES','NO'=>'NO'],['prompt' => '  '])->label(false)?>
	</b></td>
	</tr>
	</tbody>
	</table>
	</div>
</div>
	
	
	</div>
<?php $this->registerJs('
	$("#"+formObj.formName).one("change",function()
	{
		autoSaveObj = formObj;
	});

	$("input[name=\"InfantHealth[how_many_times_burnt]\"]").change(function(){
		console.log($(this).val())

			if(($(this).val() == 3 ))
			{
				$("#infanthealth-how_old_was_she_while_burnt").val("-10");
				$("#infanthealth-how_old_was_she_while_burnt2").val("-10");
				$("#infanthealth-how_old_was_she_while_burnt3").val("-10");
				$("#infanthealth-what_did_person_with_child_do_about_burnt_others").val("-10");
				$("input[name=\"InfantHealth[what_did_person_with_child_do_about_burnt]\"]").val("-10");
			}
			else
			{
				$("#infanthealth-how_old_was_she_while_burnt").val("");
				$("#infanthealth-how_old_was_she_while_burnt2").val("");
				$("#infanthealth-how_old_was_she_while_burnt3").val("")
				$("#infanthealth-what_did_person_with_child_do_about_burnt_others").val("");
				$("input[name=\"InfantHealth[what_did_person_with_child_do_about_burnt]\"]").val("");
			}
			});


			$("input[name=\"InfantHealth[how_many_times_dropped]\"]").change(function(){
		console.log($(this).val())

			if(($(this).val() == 3 ))
			{
				$("#infanthealth-how_old_was_she_while_dropped").val("-10");
				$("#infanthealth-how_old_was_she_while_dropped2").val("-10");
				$("#infanthealth-how_old_was_she_while_dropped3").val("-10");
				$("#infanthealth-what_did_person_with_child_do_about_dropp_others").val("-10");
				$("input[name=\"InfantHealth[what_did_person_with_child_do_about_dropp]\"]").val("-10");

			}
			else
			{
				$("#infanthealth-how_old_was_she_while_dropped").val("");
				$("#infanthealth-how_old_was_she_while_dropped2").val("");
				$("#infanthealth-how_old_was_she_while_dropped3").val("");
				$("#infanthealth-what_did_person_with_child_do_about_dropp_others").val("");
				$("input[name=\"InfantHealth[what_did_person_with_child_do_about_dropp]\"]").val("");
			}
			});


			$("input[name=\"InfantHealth[how_many_times_swallowed]\"]").change(function(){
		console.log($(this).val())

			if(($(this).val() == 3 ))
			{
				$("#infanthealth-how_old_was_she_while_swallowed").val("-10");
				$("#infanthealth-how_old_was_she_while_swallowed2").val("-10");
				$("#infanthealth-how_old_was_she_while_swallowed3").val("-10");
				$("#infanthealth-what_did_person_with_child_do_about_swallow_others").val("-10");
				$("input[name=\"InfantHealth[what_did_person_with_child_do_about_swallow]\"]").val("-10");
			}
			else
			{
				$("#infanthealth-how_old_was_she_while_swallowed").val("");
				$("#infanthealth-how_old_was_she_while_swallowed2").val("");
				$("#infanthealth-how_old_was_she_while_swallowed3").val("");
				$("#infanthealth-what_did_person_with_child_do_about_swallow_others").val("");
				$("input[name=\"InfantHealth[what_did_person_with_child_do_about_swallow]\"]").val("");
			}
			});


			$("input[name=\"InfantHealth[how_many_times_others]\"]").change(function(){
		console.log($(this).val())

			if(($(this).val() == 3 ))
			{
				$("#infanthealth-how_old_was_she_while_other").val("-10");
				$("#infanthealth-how_old_was_she_while_other2").val("-10");
				$("#infanthealth-how_old_was_she_while_other3").val("-10");
				$("#infanthealth-what_did_person_with_child_do_about_other_others").val("-10");
				$("input[name=\"InfantHealth[what_did_person_with_child_do_about_other]\"]").val("-10");
			}
			else
			{
				$("#infanthealth-how_old_was_she_while_other").val("");
				$("#infanthealth-how_old_was_she_while_other2").val("");
				$("#infanthealth-how_old_was_she_while_other3").val("");
				$("#infanthealth-what_did_person_with_child_do_about_other_others").val("");
				$("input[name=\"InfantHealth[what_did_person_with_child_do_about_other]\"]").val("");
			}
			});
	')?>


<?php $this->registerJs('
    $("#resettozero").click( function()
    {
    	for(var i = 1;i <= 35;i++)
    	{
    		var val = $("#infanthealth-doc_visit"+i).val(); 
    		if((val == "") || (val == "-10"))
    		{
    			$("#infanthealth-doc_visit"+i).val("0");		
    		}	
    	}
    	autoSave(formObj);
    });
')?>
<?php 
if(!$model->id)
{
	$this->registerJs('
		$("#"+formObj.formName).find("input[type=\'text\']").val("")
		$("#"+formObj.formName).find("select").val("")
		$("#"+formObj.formName).find("textarea").val("")
	',\yii\web\View::POS_READY);
}
?>
<script type="text/javascript">
		var formObj = {'url' : "infant-health/ajax",'formName' : "formInfanthealth","main":true};
		var setValueAsZero = function(){
			$("#"+formObj.formName).find("input[type=\'text\']").val("0")
			$("#"+formObj.formName).find("select").val("0")
			$("#"+formObj.formName).find("textarea").val("0")
			if(!$("input[name=\"InfantHealth[hospitalization][]\"]").is(":checked"))
			{
				$("input[name=\"InfantHealth[hospitalization][]\"]").attr("checked",false)
			}
			if(!$("input[name=\"InfantHealth[nicu_care][]\"]").is(":checked"))
			{
				$("input[name=\"InfantHealth[nicu_care][]\"]").attr("checked",false)
			}
			autoSaveObj = formObj;
		}
</script>
<style>
.oth{
	height:20px;
}
.indent{
	margin-left:20px;
}
</style>


