<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\InfantHealth */

$this->title = ' Infant Health Checklist and Immunization Checks';
$this->params['breadcrumbs'][] = ['label' => 'Infant Healths', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="infant-health-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
