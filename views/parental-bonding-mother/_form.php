﻿<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$keyPair = [-15 => '-15: Child Not Co-Operative For Assessment',
          -14 => '-14: Not Available For Assessment',
          -11 => '-11: Inadequate Information',
          -10 => '-10: Not Applicable',
          -9 => '-9: Missing',
          -8 => '-8: Refused',
          -7 => '-7: Partner Accompanied Missing',
          -6 => '-6: Family Accompanied Missing',
          -5 => '-5: Missing:Not Asked',
          -3 => '-3: No Such Relationship/No Partner'];
/* @var $this yii\web\View */
/* @var $model app\models\ParentalBondingFather */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile('@web/js/scales.js',['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="parental-bonding-mother-form">
    <?php $form = ActiveForm::begin(['id' => 'formPbim']); ?>
    <div id="withBoxShadow" class="col-xs-12 col-md-12 col-lg-12 col-sm-12">
    <div id="idpbfscale">
    <table border="1" class="responsive-scales">
            <thead>
                <tr>
                    <th>ಕ್ರ . ಸಂ </th>
                    <th>ಹೇಳಿಕೆಗಳು </th>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>4</th>
                    
                    <th>Other Answers</th>
                </tr> 
            </thead>
    <tbody>
    <tr>
        <td>1</td>
        <th>ನನ್ನ ಜೊತೆ ಮಮತೆಯಿಂದ ಹಾಗೂ ಸ್ನೇಹಮಯವಾಗಿ ಮಾತನಾಡಿದರು.</th>
        <?php for($i = 1;$i < 5;$i++):?>
        <td><?= $form->field($model, 'talks_friendly')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'talks_friendly')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>2</td>
        <th>ನನಗೆ ಬೇಕಾದಷ್ಟ/ಅಗತ್ಯವಿದ್ದಷ್ಟು ಸಹಾಯ ಮಾಡಲಿಲ್ಲ.</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'dint_help_needed')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'dint_help_needed')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>3</td>
        <th>ನನಗೆ ಇಷ್ಟವಾಗಿರುವಂತಹ ಕೆಲಸಗಳನ್ನು ಮಾಡಲು ಬಿಟ್ಟಿದ್ದರು. </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'allowed_loved_work')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'allowed_loved_work')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>4</td>
        <th>ನನ್ನೊಂದಿಗೆ ವಾತ್ಸಲ್ಯದಿಂದಿರಲಿಲ್ಲ </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'understands_pain')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'understands_pain')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>4</td>
        <th>ನನ್ನ ನೋವು ಮತ್ತು ಚಿಂತೆಗಳನ್ನು ಅರ್ಥ ಮಾಡಿಕೊಂಡವರಂತೆ ಕಾಣುತ್ತಿದ್ದರು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'loving')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'loving')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>6</td>
        <th> ನಾನು ಸ್ವಂತ ನಿರ್ಧಾರಗಳನ್ನು ತೆಗೆದುಕೊಳ್ಳುವುದನ್ನು ಇಷ್ಟಪಡುತ್ತಿದ್ದರು.</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'allowed_own_decisions')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'allowed_own_decisions')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>7</td>
        <th> ನಾನು ಬೆಳೆಯುವುದು ಇಷ್ಟವಾಗುತ್ತಿರಲಿಲ್ಲ. </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'hates_growing')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'hates_growing')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>8</td>
        <th>ನಾನು ಮಾಡುತ್ತಿದ್ದ ಪ್ರತಿಯೊಂದನ್ನು ನಿಯಂತ್ರಿಸುತ್ತಿದ್ದರು. </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'stops_all_work')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'stops_all_work')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>9</td>
        <th>ನನ್ನ ಏಕಾಂತತೆಯನ್ನು ಆಕ್ರಮಿಸುತ್ತಿದ್ದರು.</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'dint_allow_alone')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'dint_allow_alone')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>10</td>
        <th>ನನ್ನೊಂದಿಗೆ ವಿಷಯಗಳನ್ನು ಮಾತನಾಡುತ್ತ ಆನಂದಿಸುತ್ತಿದ್ದರು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'happy_to_talk')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'happy_to_talk')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>11</td>
        <th>ಆಗಾಗ ನನ್ನೊಂದಿಗೆ ನಗುತ್ತಿದ್ದರು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'laughs')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'laughs')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>12</td>
        <th>ನನ್ನನ್ನ ಮಗುವಿನಂತೆ ಆರೈಕೆಮಾಡುತ್ತಿದ್ದರು. </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'treat_baby')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'treat_baby')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>13</td>
        <th>ನನ್ನ ಆಗತ್ಯತೆ ಅಥವಾ ಅವಶ್ಯಕತೆಗಳ ಬಗ್ಗೆ ಅರ್ಥವಾದಂತೆ ಕಾಣುತ್ತಿರಲಿಲ್ಲ</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'didnt_understand_needs')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'didnt_understand_needs')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>14</td>
        <th>ನನಗೆ ಬೇಕಾದ್ದನ್ನು ನಾನೇ ನಿರ್ಧರಿಸುತ್ತಿದ್ದೆ.</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'decides_own')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'decides_own')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>14</td>
        <th>ನಾನು ಬೇಕಾಗಿರಲಿಲ್ಲ ಎಂಬ ಭಾವನೆ ಬರುವಂತೆ ಮಾಡುತ್ತಿದ್ದರು</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'dont_need_feel')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'dont_need_feel')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>16</td>
        <th>ಬೇಸರವಾಗಿದ್ದಾಗ ನಾನು ಸಮಾಧಾನವಾಗುವಂತೆ ಮಾಡುತ್ತಿದ್ದರು.</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'helps_in_sad')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'helps_in_sad')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>17</td>
        <th>ನನ್ನೊಂದಿಗೆ ಅತೀ ಹೆಚ್ಚಾಗಿ ಮಾತಾನಾಡುತ್ತಿರಲಿಲ್ಲ.</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'didnt_talk_lot')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'didnt_talk_lot')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>18</td>
        <th>ನಾನು ಅವನ/ಳನ್ನು ಅವಲಂಬಿಸಿದ್ದೇನೆಎಂಬ ಭಾವನೆ ಬರುವಂತೆ ಪ್ರಯತ್ನಿಸುತ್ತಿದ್ದರು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'feels_dependent')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'feels_dependent')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>19</td>
        <th>ಅವನು/ಳು ನನ್ನೊಂದಿಗೆ ಇಲ್ಲದಿದ್ದರೆ ನನ್ನನ್ನು ನಾನು   ನೋಡಿಕೊಳ್ಳಲು ಸಾಧ್ಯವಿಲ್ಲ ಎಂದೆನಿಸುತ್ತಿತ್ತು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'didnt_lookafter_me')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'didnt_lookafter_me')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>20</td>
        <th>ನನಗೆ ಬೇಕಾದಷ್ಟು ಸ್ವಾತಂತ್ರ್ಯ  ಕೊಟ್ಟಿದ್ದರು</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'give_freedom')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'give_freedom')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>21</td>
        <th>ನನಗೆ ಬೇಕಾದಾಗ ಹೊರಗೆಹೋಗಲು ಹಲವು ಬಾರಿ ಬಿಡುತ್ತಿದ್ದರು</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'allow_outside')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'allow_outside')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>22</td>
        <th>ನನ್ನನ್ನ ಅತೀಹೆಚ್ಚಾಗಿ/ಅಗತ್ಯಕ್ಕಿಂತ ಹೆಚ್ಚಾಗಿ ರಕ್ಷಿಸುತ್ತಿದ್ದರು</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'saves_more')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'saves_more')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>23</td>
        <th>ನನ್ನನ್ನು ಹೊಗಳುತ್ತಿರಲಿಲ್ಲ</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'praises')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'praises')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    
    <tr>
        <td>26</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ ಇತರ ಹುಡುಗರಿಗಿಂತ ನನಗೆ ಕಡಿಮೆ ಸಹಾಯ ಮಾಡಿದರು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'less_helps')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'less_helps')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>27</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ ಹುಡುಗಿಯರಿಗಿಂತ ನನ್ನ ನೋವು ಮತ್ತು ಚಿಂತೆಗಳನ್ನು ಕಡಿಮೆ ಅರ್ಥ ಮಾಡಿಕೊಂಡಂತೆ ಕಾಣುತಿತ್ತು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'less_understands')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'less_understands')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>28</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ ಹುಡುಗರಿಗಿಂತ ನನ್ನನ್ನು ಕಡಿಮೆ ಹೊಗಳುತ್ತಿದ್ದರು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'less_praise')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'less_praise')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>29</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ ಇತರೆ ಹುಡುಗಿಯರಿಗಿಂತ  ಅಗತ್ಯಕ್ಕಿಂತ ಹೆಚ್ಚಾಗಿ  ಮಾಡುತ್ತಿದ್ದರು . </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'more_secure')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'more_secure')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>30</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ ಇತರ ಹುಡುಗರಿಗಿಂತ ,ನನ್ನ ನಿರ್ಧಾರಗಳನ್ನು ನಾನೇ ತೆಗೆದುಕೊಳ್ಳಲು ಕಡಿಮೆ ಬಿಡುತ್ತಿದ್ದರು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'allow_decisions')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'allow_decisions')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>31</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ ಇತರ  ಹುಡುಗಿಯರಿಗಿಂತ ,ನನಗೆ ಇಷ್ಟವಾಗುವ  ಹಾಗೆ ಅಲಂಕಾರ ಮಾಡಿಕೊಳ್ಳಲು   ಕಡಿಮೆ    ಬಿಡುತ್ತಿದ್ದರು .</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'allowstyle')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'allowstyle')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>32</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ  ಇತರ   ಹುಡುಗರಿಗೆ   ಹೋಲಿಸಿದರೆ,  ಬೇಕಾಗುವ  ಅವಶ್ಯಕತೆ /ಸೌಲಭ್ಯಗಳನ್ನು ಕಡಿಮೆ ಒದಗಿಸುತ್ತಿದ್ದರು (ಪುಸ್ತಕಗಳು,ಬಟ್ಟೆ,ಆಹಾರ ).</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'less_books')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'less_books')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>33</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ  ಇತರ ಹುಡುಗಿಯರಿಗೆ  ಹೋಲಿಸಿದರೆ, ನನಗೆ ಓದಲು ಕಡಿಮೆ ಅವಕಾಶ ಕೊಡುತ್ತಿದ್ದರು .  </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'less_stydy')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'less_stydy')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>34</td>
        <th> ನಾನು ಕುಟುಂಬಕ್ಕೆ ಹೊರೆಯಾಗಿದ್ದೇನೆಂದು ಹೇಳುತ್ತಿದ್ದರು </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'family_burden')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
    <?= $form->field($model, 'family_burden')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>34</td>
        <th>ನಾನು ಹುಡುಗಿ ಆಗಿದ್ದ ಕಾರಣಕ್ಕಾಗಿ ನನ್ನ   ಪರವಾಗಿದ್ದರು /ಒಲವು ತೋರಿಸುತ್ತಿದ್ದರು. </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'show_olavu')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'show_olavu')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>36</td>
        <th>ನಾನು ಹುಡುಗಿ ಆಗಿದ್ದ ಕಾರಣಕ್ಕಾಗಿ  ನನಗೆ ಸಂಬಂಧಿಸಿದ  ವಿಷಯಗಲ್ಲಿಯೂ ನನ್ನ ಅಭಿಪ್ರಾಯ  /  ಇಲ್ಲದಿರುವಿಕೆಗಳನ್ನೂ ನಿರ್ಲಕ್ಷ್ಯ ಮಾಡುತ್ತಿದ್ದರು .</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'show_nirlakshya')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'show_nirlakshya')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>37</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ  ಇತರ  ಹುಡುಗಿಯರಿಗಿಂತ  ನನಗೆ ಕಡಿಮೆ  ಸಹಾಯ ಮಾಡಿದರು .  </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'less_helpings')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
    <?= $form->field($model, 'less_helpings')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>38</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ  ಹುಡುಗರಿಗಿಂತ ನನ್ನ ನೋವು ಮತ್ತು ಚಿಂತೆಗಳನ್ನು ಕಡಿಮೆ ಅರ್ಥ ಅರ್ಥ ಮಾಡಿಕೊಂಡಂತೆ ಕಾಣುತ್ತಿತ್ತು .</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'less_undestnds')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'less_undestnds')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>39</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ   ಹುಡುಗಿಯರಿಗಿಂತ  ನನ್ನನ್ನು ಕಡಿಮೆ ಹೊಗಳುತ್ತಿದ್ದರು . </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'less_praises')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
    <?= $form->field($model, 'less_praises')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>40</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ  ಇತರ  ಹುಡುಗರಿಗಿಂತ ನನ್ನನ್ನು ಅಗತ್ಯಕ್ಕಿಂತ ಹೆಚ್ಚಾಗಿ ರಕ್ಷಣೆ ಮಾಡುತ್ತಿದ್ದರು .</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'high_safes')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'high_safes')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>41</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ   ಇತರ  ಹುಡುಗಿಯರಿಗಿಂತ, ನನ್ನ ನಿರ್ಧಾರಗಳನ್ನು  ನಾನೇ  ತೆಗೆದುಕೊಳ್ಳಲು  ಕಡಿಮೆ ಬಿಡುತ್ತಿದ್ದರು .</th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'allows_decionss')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'allows_decionss')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>42</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ  ಇತರ  ಹುಡುಗರಿಗಿಂತ  , ನನಗೆ ಇಷ್ಟವಾದ ಹಾಗೆ ಅಲಂಕಾರ ಮಾಡಲು  ನನಗೆ ಕಡಿಮೆ ಬಿಡುತ್ತಿದ್ದರು . </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'allow_style_as_boys')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'allow_style_as_boys')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>43</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ   ಇತರೆ ಹುಡುಗಿಯರಿಗೆ ಹೋಲಿಸಿದರೆ , ನನಗೆ ಬೇಕಾಗುವ ಸಂಪನ್ಮೂಲಗಳಗನ್ನು  ಕಡಿಮೆ  ಒದಗಿಸುತ್ತಿದ್ದರು .   </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'less_resources')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'less_resources')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>44</td>
        <th>ಕುಟುಂಬದಲ್ಲಿರುವ   ಇತರ  ಹುಡುಗರಿಗೆ ಹೋಲಿಸಿದರೆ ,ನನಗೆ ನನಗೆ ಓದಲು ಕಡಿಮೆ ಅವಕಾಶವನ್ನು ಕೊಡಿತ್ತಿದ್ದರು . </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'less_oportunity_study')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
    <?= $form->field($model, 'less_oportunity_study')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    
    <tr>
        <td>44</td>
        <th>ನಾನು ಕುಟುಂಬಕ್ಕೆ ಒಂದು ವರದಾನವೆಂದು ಹೇಳುತ್ತಿದ್ದರು . </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'boon')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
        <?= $form->field($model, 'boon')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>46</td>
        <th>ನಾನು ಹುಡಿಗಿಯಾಗಿದ್ದ ಕಾರಣ ನನ್ನನ್ನು ಅಪಹಾಸ್ಯ ಮಾಡುವುದು ಮತ್ತು ಟೀಕಿಸಿವುದು ಮಾಡುತ್ತಿದ್ದರು . </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'jokes_onme')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
    <?= $form->field($model, 'jokes_onme')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    <tr>
        <td>47</td>
        <th>ನಾನು ಹುಡಿಗಿಯಾಗಿದ್ದ ಕಾರಣ , ನನಗೆ ಸಂಬಂಧಿಸಿದ ವಿಷಯಗಲ್ಲಿಯೂ  ನನ್ನ ಅಭಿಪ್ರಾಯ /ಇಷ್ಟ ಮತ್ತು ಇಷ್ಟದಿಲ್ಲದಿರುವಿಕೆಯನ್ನು ಗೌರವಿಸುತ್ತಿದ್ದರು . </th>
        <?php for($i = 1;$i < 5;$i++):?><td><?= $form->field($model, 'respects_me')->radio(['label' => $i, 'value' => $i])?></td>
        <?php endfor;?>
        <td>
    <?= $form->field($model, 'respects_me')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
        </td>
    </tr>
    
    
    
    </tbody>
    </table>
    </div>
    <div id="scaleflag" class="hidden">
    <?= $form->field($model, 'score')->input('number',['readonly'=>true])->label(false);?>
    </div>

    

    <?php ActiveForm::end(); ?>

</div>
</div>
<?php $this->registerJs('
    $("#"+formObj.formName).one("change",function()
    {
        autoSaveObj = formObj;
    });
',\yii\web\View::POS_READY)?>


<script type="text/javascript">
    var formObj = {url:"parental-bonding-mother/ajax",formName : "formPbim"};
	var calc = function () {
		var totalValue = 0;
		var root= 'parentalbondingmother';
		var id = '#'+root+'-';
		var clas = '.field-'+root+'-';
		var arr = ['talks_friendly', 'dint_help_needed', 'allowed_loved_work', 'understands_pain', 'loving', 
		'allowed_own_decisions', 'hates_growing', 'stops_all_work', 'dint_allow_alone', 'happy_to_talk', 'laughs',
		'treat_baby', 'didnt_understand_needs', 'decides_own', 'dont_need_feel', 'helps_in_sad', 'didnt_talk_lot', 
		'feels_dependent', ,'less_helps','didnt_lookafter_me', 'give_freedom', 'allow_outside', 'saves_more', 'praises','allow_style','less_praise','more_secure','allow_decisions','allowstyle',
		'less_books','less_stydy','family_burden','show_olavu','show_nirlakshya','less_helpings','less_undestnds','less_praises','high_safes','allows_decionss','allow_style_as_boys',
		'less_resources','less_oportunity_study','boon','jokes_onme','respects_me','less_understands'];
		
		for (var i in arr) {
			var val = $(clas + arr[i] + ' input[type=radio]:checked').val();
			if (parseInt(val)) {
				totalValue += parseInt(val);
			}
		}
		$(id+'score').val(totalValue);
		autoSaveObj=(formObj);
    }
</script>

