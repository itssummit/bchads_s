<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ParentalBondingMother */

$this->title = 'Parental Bonding Mother';
$this->params['breadcrumbs'][] = ['label' => 'Parental Bonding Mothers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parental-bonding-mother-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
