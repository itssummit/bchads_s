<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ConflictTacticsScale */

$this->title = 'conflict-tactics-scale';
$this->params['breadcrumbs'][] = ['label' => 'Conflict Tactics Scales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="conflict-tactics-scale-create">

    <h1>ಮನೋವೈಜ್ಞಾನಿಕ  ನಿಂದನಾತ್ಮಕ /ಕಿರುಕುಳಾತ್ಮಕ ಮಾಪನ</h1>
	<div id = "btnGroup">
		<button type = "button" id = "btnone" class = "btn btn-primary" onclick = "loadIt('one')">Form1</button>
		<button type = "button" id = "btntwo" class = "btn btn-success" onclick = "loadIt('two')">Form2</button>
		<button type = "button" id = "btnthree" class = "btn btn-success" onclick = "loadIt('three')">Form3</button>
		<button type = "button" id = "btnfour" class = "btn btn-success" onclick = "loadIt('four')">Form4</button>
		<button type = "button" id = "btnfive" class = "btn btn-success" onclick = "loadIt('five')">Form5</button>
	</div>
    <div id = "loadData"></div>
	
	
	<?php echo $this->registerJs('
		loadIt("one");
	');
	?>
	<script>
		var loadIt = function(id){
			$('#btnGroup .btn').each(function(){
				$(this).removeClass();
				if($(this).attr('id') == 'btn'+id)
				{	
					$('#btn'+id).addClass('btn btn-primary');
				}
				else
				{
					$(this).addClass('btn btn-success');
				}
			})
			if(autoSaveObj.url)
			{
				autoSave(autoSaveObj);
				autoSaveObj = {url : null};
			}
			if(autoSaveFormObj.url){
				autoSaveForm(autoSaveFormObj,null);
				autoSaveFormObj = {url : null};
			}
			$("#loadData").load("/bchads/web/index.php?r=conflict-tactics-scale/form"+id);
		}
	</script>
</div>
