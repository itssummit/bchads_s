<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$keyPair = [-15 => '-15: Child Not Co-Operative For Assessment',
		  -14 => '-14: Not Available For Assessment',
		  -11 => '-11: Inadequate Information',
		  -10 => '-10: Not Applicable',
		  -9 => '-9: Missing',
		  -8 => '-8: Refused',
		  -7 => '-7: Partner Accompanied Missing',
		  -6 => '-6: Family Accompanied Missing',
		  -5 => '-5: Missing:Not Asked',
		  -3 => '-3: No Such Relationship/No Partner'];
/* @var $this yii\web\View */
/* @var $model app\models\ConflictTacticsScale */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile('@web/js/scales.js',['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="conflict-tactics-scale-form">

	<?php $form = ActiveForm::begin(['id'=>'formConflicttacticsscale']); ?>
	<div id="withBoxShadow" class="col-xs-12 col-md-12 col-lg-12 col-sm-12">
	<div class="col-lg-4 pull-right">
		<h4><b>Is This Scale Applicable ?</b></h4>
			<?php echo $form->field($model, 'not_applicable')->dropDownList(['' =>'','YES' => 'Not Applicable', 'NO' => 'Applicable'],['class' => 'form-control scaleApplicable'])->label(false); ?>
	</div>		
	<table border="1" class="responsive-scales">
        <thead>
            <tr>
			<th>ಕ್ರ .ಸಂ </th>
			<th>ಹೇಳಿಕೆಗಳು </th>
			<th>ಹೌದು </th>
			<th>ಇಲ್ಲ  </th>
			<th>Other Answers</th>
            </tr>
        </thead>
		<tbody>
		
				<tr>
				<td>1</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಗೆ ಕಷ್ಟಕರ ಸಮಯಗಳಲ್ಲೂ ಬೆಂಬಲವಾಗಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'supporting_your_partner_at_difficult_times')->radio(['label' => $i, 'value' => $i])?> </td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'supporting_your_partner_at_difficult_times')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				
				<tr>
				<td>2</td>
				<td> ನಿಮ್ಮ ಅನಿಸಿಕೆಗಳನ್ನು ಹೇಳದೆಯೇ ನಿಮ್ಮಲ್ಲಿಯೇ ಇಟ್ಟುಕೊಂಡಿದ್ದೀರಾ  </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'keeping_your_opinions_with_yourself')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'keeping_your_opinions_with_yourself')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>3</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಯ ಮೇಲಿನ ಕೋಪದಿಂದ ಮನೆಯಲ್ಲಿನ ವಸ್ತುಗಳನ್ನು ಅಥವಾ ಮನೆಯ ಕೆಲವು ಭಾಗವನ್ನು  ಹಾನಿಮಾಡಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'damaged_things_because_of_angry_on_your_wife')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'damaged_things_because_of_angry_on_your_wife')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>4</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಯೊಂದಿಗೆ ವಾದಮಾಡಿ ಮುಗಿದ ನಂತರ ಅವರನ್ನು ಸಮಾಧಾನಪಡಿಸಲು ಹೆಚ್ಚಿನ ಪ್ರೀತಿಯನ್ನು ನೀಡಿದ್ದೀರಾ  </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'after_arguing_with_your_partner_did_u_shown_more_love')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'after_arguing_with_your_partner_did_u_shown_more_love')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>5</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಗೆ ಸಂಬಂಧಪಟ್ಟಂತಹ ಮುಖ್ಯವಾದ ವಸ್ತುವನ್ನು ಉದ್ದೇಶಪೂರ್ವಕವಾಗಿಯೇ ಕೊಟ್ಟು ಬಿಡುವುದು   ಅಥವಾ ಬಚ್ಚಿಟ್ಟಿದ್ದೀರಾ   </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'knowingly_hiding_ur_partners_loved_things')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'knowingly_hiding_ur_partners_loved_things')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>6</td>
				<td> ನೀವು ಅಂದುಕೊಂಡಾಗ ಅಡುಗೆ ಏನಾದರೂ ಆಗದಿದ್ದರೆ, ಮನೆಯ ಕೆಲಸ ಅಥವಾ ಮನೆಯ ದುರಸ್ತಿ ಕೆಲಸಗಳು  ನಡೆಯದಿದ್ದರೆ ನನಗೆ ಬೇಜಾರಾಗಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'did_u_felt_sad_when_things_are_undone')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'did_u_felt_sad_when_things_are_undone')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>7</td>
				<td> ಎಲ್ಲ ದೃಷ್ಟಿಕೋನಗಳಲ್ಲೂ ಮಾತನಾಡಲು ನೀವಾಗಿಯೇ ಚರ್ಚೆಯನ್ನು ಪ್ರಾರಂಭಮಾಡಿರುವುದು </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'started_meeting_to_talk_in_all_angles')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'started_meeting_to_talk_in_all_angles')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>8</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಯ ಬಟ್ಟೆಗಳನ್ನು ವಾಹನಗಳನ್ನು ಅಥವಾ ಇತರೆ ವೈಯಕ್ತಿಕ ಆಸ್ತಿಗಳನ್ನು ಉದ್ದೇಶ ಪೂರ್ವಕವಾಗಿ  ಹಾಳುಮಾಡಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'intentionally_destroying_partners_personal_things')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'intentionally_destroying_partners_personal_things')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>9</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಯನ್ನು ಇತರರ ಮುಂದೆ ಅವಮಾನಪಡಿಸಿದ್ದೀರಾ ಅಥವಾ ಹೀಯಾಳಿಸಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'disrespecting_ur_partner_infront_of_others')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'disrespecting_ur_partner_infront_of_others')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>10</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಯು ಹೇಳುತ್ತಿರುವುದನ್ನು ಗಮನವಿಟ್ಟು   ಕೇಳಿದ್ದೀರಾ   </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'listened_to_ur_partners_words_with_concentration')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'listened_to_ur_partners_words_with_concentration')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>11</td>
				<td> ನಿಮ್ಮ  ಸಂಗಾತಿಯನ್ನು ಮನೆಯ ಹೊರಗಡೆಯಿಂದ ಕೂಡಿಹಾಕಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'locked_ur_partner_from_outside_home')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'locked_ur_partner_from_outside_home')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>12</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಗೆ ಅವನು/ಳು ಕೆಲಸ ಮಾಡಲು ಅಥವಾ ವಿದ್ಯಾಭ್ಯಾಸ ಮಾಡಲು ಸಾಧ್ಯವಾಗುವುದಿಲ್ಲವೆಂದು  ಹೇಳಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'said_to_ur_partner_like_u_r_unable_to_study_or_work')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'said_to_ur_partner_like_u_r_unable_to_study_or_work')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>13</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಗೆ ಸ್ಥಾನವನ್ನು ಸ್ಪಷ್ಟವಾಗಿ ತಿಳಿಸಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'specified_ur_partners_position')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'specified_ur_partners_position')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>14</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಯನ್ನು ಕುಟುಂಬದವರು ಅಥವಾ ಸ್ನೇಹಿತರೊಂದಿಗೆ ಮಾತನಾಡುವುದನ್ನು ಅಥವಾ ಬೇಟೆಮಾಡಲು  ನಿಲ್ಲಿಸಲು  ಪ್ರಯತ್ನಿಸಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'stopping_ur_partner_from_talking_with_relatives')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'stopping_ur_partner_from_talking_with_relatives')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>15</td>
				<td> ಭಿನ್ನಾಭಿಪ್ರಾಯಗಳನ್ನು ಹೇಗೆ ನಿಭಾಯಿಸುವುದು ಎಂಬುವುದರ ಬಗ್ಗೆ ನೀವ್ರ ಸುಲಭವಾಗಿ ಹೊಂದಿಕೊಳ್ಳುತ್ತಿದ್ದೀರಾ  </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'adjusting_with_different_opinions')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'adjusting_with_different_opinions')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>16</td>
				<td> ಅರ್ಥವಾಗಿದೆಯೇ ಎಂದು ತಿಳಿದುಕೊಳ್ಳಲುಮತ್ತೆ ಮತ್ತೆ ಅದನ್ನೇ ಹೇಳುತ್ತಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'frequently_asking_did_u_understood_or_not')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'frequently_asking_did_u_understood_or_not')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>17</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಯು  ಅವರ ಫೋನನ್ನು   ಅಥವಾ ವಾಹನಗಳನ್ನು ಬಳಸದಂತೆ  ನಿರ್ಬಂಧಿಸಿದ್ದೀರಾ</td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'restritcting_ur_partner_by_using_of_mobiles_or_vehicle')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'restritcting_ur_partner_by_using_of_mobiles_or_vehicle')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>18</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಯೊಂದಿಗಿನ ಸಂಬಂಧಗಳನ್ನು ಕಳೆದುಕೊಳ್ಳುತ್ತೇನೆಂದು ಬೆದರಿಕೆಯನ್ನು ಹಾಕಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'threatning_ur_partner_for_disconnection_of_relationship')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'threatning_ur_partner_for_disconnection_of_relationship')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>19</td>
				<td>ದೈಹಿಕ ಕೆಲಸಗಳನ್ನು ಅಥವಾ ಚಟುವಟಿಕೆಗಳನ್ನು ಮಾಡುವುದರ ಮೂಲಕ ಸಮಾಧಾನಮಾಡಿಕೊಂಡಿದ್ದೀರಾ   </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'by_doing_physical_work_did_u_reliefed')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'by_doing_physical_work_did_u_reliefed')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>20</td>
				<td> ಕುಟುಂಬದವರನ್ನು ಸ್ನೇಹಿತರನ್ನು ಅಥವಾ ಮಕ್ಕಳನ್ನು ನಿಮ್ಮ ಸಂಗಾತಿಗೆ ವಿರುದ್ಧವಾಗಿರುವಂತೆ ಮಾಡಲು  ಪ್ರಯತ್ನಿಸಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'tried_ur_relatives_against_ur_partner')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'tried_ur_relatives_against_ur_partner')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>21</td>
				<td> ಸಮಸ್ಯೆಗಳನ್ನು ಪರಿಹರಿಸುವಾಗ ನಿಮ್ಮ ಸಂಗಾತಿಗೆ ಸಹಾಯಕವಾಗುವಂತಹ ಸಲಹೆಗಳನ್ನು ನೀಡಿದ್ದೀರಾ  </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'while_partner_solving_their_problem_did_u_suggested_ideas')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'while_partner_solving_their_problem_did_u_suggested_ideas')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>22</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಗೆ ಆದೇಶಗಳನ್ನು ಅಥವಾ ಆಜ್ಞೆಗಳನ್ನು ಮಾಡಿದ್ದೀರಾ  </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'ordered_or_promised_ur_partner')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'ordered_or_promised_ur_partner')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>23</td>
				<td> ಇಬ್ಬರು ಒಟ್ಟಿಗೆ ಸಮಸ್ಯೆಗಳನ್ನು ಪರಿಹಾರ ಮಾಡಲು ಹೊಸ ದಾರಿಗಳನ್ನು ಹುಡುಕಲು ಪ್ರಯತ್ನಿಸಿದ್ದೀರಾ  </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'tried_to_solve_problems_together')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'tried_to_solve_problems_together')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>24</td>
				<td> ನಿಮ್ಮ ತಪ್ಪುಗಳನ್ನು ಒಪ್ಪಿಕೊಂಡಿರುವುದು ಅಥವಾ ಆ ಸಮಸ್ಯೆಯ ಜವಾಬ್ದಾರಿಯನ್ನು ನೀವೇ ಹೊತ್ತುಕೊಂಡಿದ್ದೀರಾ  </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'accepted_ur_faults')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'accepted_ur_faults')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>25</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಯನ್ನು  ಭಯಪಡಿಸಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'feared_ur_partner')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'feared_ur_partner')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>26</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿ ಅವನು/ಳು ಮೂರ್ಖರೆಂಬಂತೆ ನಡೆಸಿಕೊಂಡಿದ್ದೀರಾ  </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'treated_ur_partner_as_a_fool')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'treated_ur_partner_as_a_fool')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>27</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಗೆ ಸಹಾಯವಾಗುವಂತಹ ಉಪಾಯಗಳನ್ನು ಹೇಳಿದ್ದೀರಾ  </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'given_useful_ideas_to_ur_partner')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'given_useful_ideas_to_ur_partner')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>28</td>
				<td> ನೀವು ಮೊದಲೇ ಯೋಚಿಸಿಕೊಂಡಿದ್ದು ನಿಮ್ಮ ಸಂಗಾತಿಗೆ ಸೇಡಿನಿಂದ ಮಾತನಾಡಿದ್ದೀರಾ  </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'done_preplanned_work_with_angry')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'done_preplanned_work_with_angry')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>29</td>
				<td> ನಿಮ್ಮಿಬ್ಬರ ನಡುವಿನ  ಭಿನ್ನಾಭಿಪ್ರಾಯಗಳನ್ನು ಸರಿಪಡಿಸಲು ಯಾರಾದರೊಬ್ಬರನ್ನು ಕರೆದುಕೊಂಡು ಬಂದಿರುವುದು  ಅಥವಾ ಪ್ರಯತ್ನ ಮಾಡಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'tried_to_join_others_to_solve_ur_problems')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'tried_to_join_others_to_solve_ur_problems')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>30</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಯನ್ನು ಅವಮಾನಪಡಿಸಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'disrespected_ur_partner')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'disrespected_ur_partner')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>31</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಗೆ ನೀವು ಹೇಳಿರುವಂತಹ ಮಾತಿನಿಂದ ಅಥವಾ ನೀವು ನಡೆದುಕೊಂಡಿರುವ ರೀತಿಯಿಂದ ನಿಮಗೆ  ಪಶ್ಚಾತಾಪವಾಗಿರುವುದನ್ನು ವ್ಯಕ್ತಪಡಿಸಿದ್ದೀರಾ  </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'self_realised_what_u_said_to_ur_partner')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'self_realised_what_u_said_to_ur_partner')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>32</td>
				<td> ಕೋಪದಲ್ಲಿರುವಾಗ ನಿಮ್ಮ ಸಂಗಾತಿಗೆ ಹೊಡೆಯುವ ಅಥವಾ ಯಾವುದಾದರೂ ವಸ್ತುವನ್ನು ಎಸೆಯುತ್ತೇನೆಂದು  ಅವನಿಗೆ /ಅವಳಿಗೆ  ಹೆದರಿಸಿದ್ದೀರಾ  </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'feared_ur_partner_to_throw_things_while_angry')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'feared_ur_partner_to_throw_things_while_angry')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>33</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಗೆ  ಅವನು /ಳು  ಅಂದವಾಗಿಲ್ಲ/ಆಕರ್ಷಿತವಾಗಿಲ್ಲ ಎಂದು ಹೇಳಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'said_u_r_not_beautiful_to_partner')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'said_u_r_not_beautiful_to_partner')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>34</td>
				<td> ಸಮಸ್ಯೆಗಳಿಗೆ ಹೊಂದಾಣಿಕೆ ಮಾಡಿಕೊಳ್ಳಬೇಕಾದಾಗ ಒಪ್ಪಲು ಸಾಧ್ಯವಾಗಿದೀಯೇ  </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'agreed_urself_for_adjustment')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'agreed_urself_for_adjustment')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>35</td>
				<td> ಮಾದಕ ವಸ್ತು  ಅಥವಾ ಮದ್ಯಪಾನ ಮಾಡಿದ ನಂತರ ಬೈದಿರುವುದು ಅಥವಾ ನಿಂದನೆ ಮಾಡಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'after_smoking_did_u_scolded_ur_partner')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'after_smoking_did_u_scolded_ur_partner')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>36</td>
				<td> ಯಾವುದಾಕಾದರೂ ಒಪ್ಪದಿದ್ದಾಗ ಒದ್ದಿರುವುದು ಅಥವಾ ಹೊಡೆದಿರುವುದು, ನೆಲಕ್ಕೆ ಹಾಕಿ ತುಳಿದಿರುವುದು , ಏನನ್ನಾದರೂ ಎಸೆದಿರುವುದು </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'if_ur_partner_disagreed_u_then_did_u_thrown_any_objects')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'if_ur_partner_disagreed_u_then_did_u_thrown_any_objects')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>37</td>
				<td> ವಾದ ಮಾಡಿದ ನಂತರ ಕ್ಷಮೆಯನ್ನು ಕೇಳಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'asked_sorry_after_arguing')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'asked_sorry_after_arguing')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>38</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಯು ಒಪ್ಪದಿದ್ದರು ವಾದವನ್ನು ಮುಗಿಸಬೇಕೆಂದು ನೀವು ಒಪ್ಪಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'forcibly_closing_the_argument')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'forcibly_closing_the_argument')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>39</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಗೆ ಬೇಕಾಗಿರುವುದನ್ನೇ ನೀವೂ ಒಪ್ಪಿರುವುದು </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'agreed_to_ur_partner_wanted_things')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'agreed_to_ur_partner_wanted_things')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td colspan="5"><label><b>ಕಳೆದ ವರ್ಷದಲ್ಲಿ ನಿಮ್ಮ ಸಂಗಾತಿಯು ಯಾವಾಗಲಾದರೂ </b></label></td>
				</tr>
				
				<tr>
				<th>ಕ್ರ .ಸಂ </th>
				<th>ಹೇಳಿಕೆಗಳು </th>
				<th>ಹೌದು </th>
				<th>ಇಲ್ಲ </th>
				<th>Other Answers</th>
				</tr>
				
				<tr>
				<td>1</td>
				<td> ನಿಮಗೆ ಕಷ್ಟಕರ ಸಮಯಗಳಲ್ಲೂ ಬೆಂಬಲವಾಗಿದ್ದಾರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'supporting_your_partner_at_difficult_times1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'supporting_your_partner_at_difficult_times1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>2</td>
				<td> ಅವರ ಅನಿಸಿಕೆಗಳನ್ನು ಹೇಳದೆಯೇ ಅವರಲ್ಲಿಯೇ ಇಟ್ಟುಕೊಂಡಿದ್ದಾರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'keeping_your_opinions_with_yourself1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'keeping_your_opinions_with_yourself1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>3</td>
				<td> ನಿಮ್ಮ ಮೇಲಿನ ಕೋಪದಿಂದ ಮನೆಯಲ್ಲಿನ ವಸ್ತುಗಳನ್ನು ಅಥವಾ ಮನೆಯ ಕೆಲವು ಭಾಗವನ್ನು ಹಾನಿಮಾಡಿದ್ದಾರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'damaged_things_because_of_angry_on_your_wife1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'damaged_things_because_of_angry_on_your_wife1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>4</td>
				<td> ನಿಮ್ಮೊಂದಿಗೆ ವಾದಮಾಡಿ ಮುಗಿದ ನಂತರ ನಿಮ್ಮನ್ನು ಸಮಾಧಾನಪಡಿಸಲು ಹೆಚ್ಚಿನ ಪ್ರೀತಿಯನ್ನು ನೀಡಿದ್ದಾರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'after_arguing_with_your_partner_did_u_shown_more_love1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'after_arguing_with_your_partner_did_u_shown_more_love1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>5</td>
				<td> ನಿಮಗೆ ಸಂಬಂಧಪಟ್ಟಂತಹ ಮುಖ್ಯವಾದ ವಸ್ತುವನ್ನು ಉದ್ದೇಶಪೂರ್ವಕವಾಗಿಯೇ ಕೊಟ್ಟು ಬಿಡುವುದು ಅಥವಾ  ಬಚ್ಚಿಟ್ಟಿದ್ದಾರಾ  </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'knowingly_hiding_ur_partners_loved_things1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'knowingly_hiding_ur_partners_loved_things1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>6</td>
				<td> ಅವರು ಅಂದುಕೊಂಡಾಗ ಅಡುಗೆ ಏನಾದರೂ ಆಗದಿದ್ದರೆ, ಮನೆಯ ಕೆಲಸ ಅಥವಾ ಮನೆಯ ದುರಸ್ತಿ ಕೆಲಸಗಳು  ನಡೆಯದಿದ್ದರೆ ಅವರು ಬೇಜಾರಾಗಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'did_u_felt_sad_when_things_are_undone1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'did_u_felt_sad_when_things_are_undone1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>7</td>
				<td> ಎಲ್ಲ ದೃಷ್ಟಿಕೋನಗಳಲ್ಲೂ ಮಾತನಾಡಲು ನೀವಾಗಿಯೇ ಚರ್ಚೆಯನ್ನು ಪ್ರಾರಂಭಮಾಡಿರುವುದು </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'started_meeting_to_talk_in_all_angles1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'started_meeting_to_talk_in_all_angles1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>

				<tr>
				<td>8</td>
				<td> ನಿಮ್ಮ ಬಟ್ಟೆಗಳನ್ನು ವಾಹನಗಳನ್ನು ಅಥವಾ ಇತರೆ ವೈಯಕ್ತಿಕ ಆಸ್ತಿಗಳನ್ನು ಉದ್ದೇಶ ಪೂರ್ವಕವಾಗಿ  ಹಾಳುಮಾಡಿದ್ದಾರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'intentionally_destroying_partners_personal_things1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'intentionally_destroying_partners_personal_things1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>9</td>
				<td> ನಿಮ್ಮನ್ನು ಇತರರ ಮುಂದೆ ಅವಮಾನಪಡಿಸಿದ್ದಾರಾ ಅಥವಾ ಹೀಯಾಳಿಸಿದ್ದಾರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'disrespecting_ur_partner_infront_of_others1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'disrespecting_ur_partner_infront_of_others1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>10</td>
				<td> ನೀವು ಹೇಳುತ್ತಿರುವುದನ್ನು ಗಮನವಿಟ್ಟ ಕೇಳಿದ್ದಾರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'listened_to_ur_partners_words_with_concentration1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'listened_to_ur_partners_words_with_concentration1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>11</td>
				<td> ನಿಮ್ಮನ್ನು ಮನೆಯ ಹೊರಗಡೆಯಿಂದ ಕೂಡಿಹಾಕಿದ್ದಾರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'locked_ur_partner_from_outside_home1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'locked_ur_partner_from_outside_home1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>12</td>
				<td> ನಿಮಗೆ ಕೆಲಸ ಮಾಡಲು ಅಥವಾ ವಿಧ್ಯಾಭ್ಯಾಸ  ಮಾಡಲು ಸಾಧ್ಯವಾಗುವುದಿಲ್ಲವೆಂದು ಹೇಳಿದ್ದಾರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'said_to_ur_partner_like_u_r_unable_to_study_or_work1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'said_to_ur_partner_like_u_r_unable_to_study_or_work1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>13</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಯ ಸ್ಥಾನವನ್ನು ಸ್ಪಷ್ಟವಾಗಿ  ತಿಳಿಸಿದ್ದಾರಾ  </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'specified_ur_partners_position1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'specified_ur_partners_position1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>14</td>
				<td>  ನಿಮ್ಮನ್ನು  ಕುಟುಂಬದವರು ಅಥವಾ ಸ್ನೇಹಿತರೊಂದಿಗೆ ಮಾತನಾಡುವುದನ್ನು ಅಥವಾ ಬೇಟಿಮಾಡಲು  ಪ್ರಯತ್ನಿಸಿದ್ದಾರ   </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'stopping_ur_partner_from_talking_with_relatives1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'stopping_ur_partner_from_talking_with_relatives1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>15</td>
				<td> ಭಿನ್ನಾಭಿಪ್ರೌಯಗಳನ್ನು ಹೇಗೆ ನಿಭಾಯಿಸುವುದು ಎಂಬುವುದರ ಬಗ್ಗೆ ಅವರು ಸುಲಭವಾಗಿ ಹೊಂದಿಕೊಳ್ಳುತ್ತಿದ್ದಾರಾ  </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'adjusting_with_different_opinions1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'adjusting_with_different_opinions1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>16</td>
				<td> ಅರ್ಥವಾಗಿದೆಯೇ ಎಂದು ತಿಳಿದುಕೊಳ್ಳಲು ಪದೇ ಪದೇ ಅದನ್ನೇ ಹೇಳುತ್ತಿದ್ದಾರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'frequently_asking_did_u_understood_or_not1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'frequently_asking_did_u_understood_or_not1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>17</td>
				<td> ನೀವು ನಿಮ್ಮ ಫೋನನ್ನು  ಅಥವಾ ವಾಹನಗಳನ್ನು ಬಳಸದಂತೆ ನಿರ್ಬಂಧಿಸಿದ್ದಾರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'restritcting_ur_partner_by_using_of_mobiles_or_vehicle1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'restritcting_ur_partner_by_using_of_mobiles_or_vehicle1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>18</td>
				<td> ನಿಮ್ಮೊಂದಿಗಿನ ಸಂಬಂಧಗಳನ್ನು ಕಳೆದುಕೊಳ್ಳುತ್ತೇನೆಂದು ಬೆದರಿಕೆಯನ್ನು ಹಾಕಿದ್ದಾರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'threatning_ur_partner_for_disconnection_of_relationship1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'threatning_ur_partner_for_disconnection_of_relationship1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>19</td>
				<td> ದೈಹಿಕ ಕೆಲಸಗಳನ್ನು ಅಥವಾ ಚಟುವಟಿಕೆಗಳನ್ನು ಮಾಡುವುದರ ಮೂಲಕ ಸಮಾಧಾನ  ಮಾಡಿಕೊಂಡಿದ್ದಾರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'by_doing_physical_work_did_u_reliefed1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'by_doing_physical_work_did_u_reliefed1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>20</td>
				<td> ಕುಟುಂಬದವರನ್ನು ಸ್ನೇಹಿತರನ್ನ ಆಥವಾ ಮಕ್ಕಳನ್ನು ನಿಮಗೆ ವಿರುದ್ಧವಾಗಿರುವಂತೆ ಮಾಡಲು ಪ್ರಯತ್ನಿಸಿದ್ದಾರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'tried_ur_relatives_against_ur_partner1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'tried_ur_relatives_against_ur_partner1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>21</td>
				<td> ಸಮಸ್ಯೆಗಳನ್ನು ಪರಿಹರಿಸುವಾಗ ನಿಮಗೆ ಸಹಾಯಕವಾಗುವಂತಹ ಸಲಹೆಗಳನ್ನು ನೀಡಿದ್ದಾರಾ  </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'while_partner_solving_their_problem_did_u_suggested_ideas1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'while_partner_solving_their_problem_did_u_suggested_ideas1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>22</td>
				<td> ನಿಮಗೆ ಆದೇಶಗಳನ್ನು ಅಥವಾ ಆಜ್ಞೆಗಳನ್ನು ಮಾಡಿದ್ದಾರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'ordered_or_promised_ur_partner1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'ordered_or_promised_ur_partner1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>23</td>
				<td> ಇಬ್ಬರು ಒಟ್ಟಿಗೆ ಸಮಸ್ಯೆಗಳನ್ನು ಪರಿಹಾರ ಮಾಡಲು ಹೊಸ ದಾರಿಗಳನ್ನು ಹುಡುಕಲು ಪ್ರಯತ್ನಿಸಿದ್ದಾರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'tried_to_solve_problems_together1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'tried_to_solve_problems_together1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>24</td>
				<td> ಅವರ ತಪ್ಪುಗಳನ್ನು ಒಪ್ಪಿಕೊಂಡಿರುವುದು ಅಥವಾ ಆ ಸಮಸ್ಯೆಯ ಜವಾಬ್ದಾರಿಯನ್ನು ಅವರೇ ಹೊತ್ತುಕೊಂಡಿದ್ದಾರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'accepted_ur_faults1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'accepted_ur_faults1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>25</td>
				<td> ನಿಮ್ಮನ್ನು ಭಯಪಡಿಸಿದ್ದಾರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'feared_ur_partner1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'feared_ur_partner1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>26</td>
				<td> ನಿಮ್ಮನ್ನು ಮೂರ್ಖರೆಂಬಂತೆ ನಡೆಸಿಕೊಂಡಿದ್ದಾರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'treated_ur_partner_as_a_fool1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'treated_ur_partner_as_a_fool1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>27</td>
				<td> ನಿಮಗೆ ಸಹಾಯವಾಗುವಂತಹ ಉಪಾಯಗಳನ್ನು ಹೇಳಿದ್ದಾರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'given_useful_ideas_to_ur_partner1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'given_useful_ideas_to_ur_partner1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>28</td>
				<td> ಅವರು ಮೊದಲೇ ಯೋಜಿಸಿಕೊಂಡಿದ್ದು ನಿಮಗೆ ಸೇಡಿನಿಂದ ಮಾತನಾಡಿದ್ದಾರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'done_preplanned_work_with_angry1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'done_preplanned_work_with_angry1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>29</td>
				<td> ನಿಮ್ಮಿಬ್ಬರ ನಡುವಿನ ಬಿನ್ನಾಬಿಪ್ರಾಯ ಗಳನ್ನು ಸರಿಪಡಿಸಲು ಯಾರಾದರೊಬ್ಬರನ್ನು ಕರೆದುಕೊಂಡು ಬಂದಿರುವುದು  ಅಥವಾ ಪ್ರಯತ್ನ ಮಾಡಿದ್ದಾರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'tried_to_join_others_to_solve_ur_problems1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'tried_to_join_others_to_solve_ur_problems1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>30</td>
				<td> ನಿಮ್ಮನ್ನು ಅವಮಾನಪಡಿಸಿದ್ದಾರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'disrespected_ur_partner1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'disrespected_ur_partner1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>31</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಗೆ ನೀವು ಹೇಳಿರುವಂತಹ ಮಾತಿನಿಂದ ಅಥವಾ ನೀವು ನಡೆದುಕೊಂಡಿರುವ ರೀತಿಯಿಂದ ನಿಮಗೆ  ಪಶ್ಚಾತಾಪವಾಗಿರುವುದನ್ನು ವ್ಯಕ್ತಪಡಿಸಿದ್ದಾರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'self_realised_what_u_said_to_ur_partner1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'self_realised_what_u_said_to_ur_partner1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>32</td>
				<td> ಕೋಪದಲ್ಲಿರುವಾಗ ನಿಮಗೆ ಹೊಡೆಯುವ ಅಥವಾ ಯಾವುದಾದರೂ ವಸ್ತುವನ್ನು ಎಸೆಯುತ್ತೇನೆಂದು ಹೆದರಿಸಿದ್ದಾರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'feared_ur_partner_to_throw_things_while_angry1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'feared_ur_partner_to_throw_things_while_angry1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>33</td>
				<td> ನಿಮಗೆ ಅಂದವಾಗಿಲ್ಲ/ಆಕರ್ಷಿತವಾಗಿಲ್ಲ ಎಂದು ಹೇಳಿದ್ದಾರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'said_u_r_not_beautiful_to_partner1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'said_u_r_not_beautiful_to_partner1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>34</td>
				<td> ಸಮಸ್ಯೆಗಳಿಗೆ ಹೊಂದಾಣಿಕೆ ಮಾಡಿಕೊಳ್ಳಬೇಕಾದಾಗ ಒಪ್ಪಲು ಸಾಧ್ಯವಾಗಿದೆಯೇ  </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'agreed_urself_for_adjustment1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'agreed_urself_for_adjustment1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>35</td>
				<td> ಮಾದಕ ವಸ್ತು ಅಥವಾ ಮದ್ಯಪಾನ ಮಾಡಿದ ನಂತರ ಬೈದಿರುವುದು ಅಥವಾ ನಿಂದನೆ ಮಾಡಿದ್ದಾರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'after_smoking_did_u_scolded_ur_partner1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'after_smoking_did_u_scolded_ur_partner1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>36</td>
				<td> ಯಾವುದಕ್ಕಾದರೂ  ಒಪ್ಪದಿದ್ದಾಗ ಒದ್ದಿರುವುದು ಅಥವಾ ಹೊಡೆದಿರುವುದು, ನೆಲಕ್ಕೆ ಹಾಕಿ ತುಳಿದಿರುವುದು , ಏನನ್ನಾದರೂ ಎಸೆದಿರುವುದು </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'if_ur_partner_disagreed_u_then_did_u_thrown_any_objects1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'if_ur_partner_disagreed_u_then_did_u_thrown_any_objects1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>37</td>
				<td> ವಾದ ಮಾಡಿದ ನಂತರ ಕ್ಷಮೆಯನ್ನು ಕೇಳಿದ್ದಾರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'asked_sorry_after_arguing1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'asked_sorry_after_arguing1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>38</td>
				<td> ನೀವು ಒಪ್ಪದಿದ್ದರು ವಾದವನ್ನು ಮುಗಿಸಬೇಕೆಂದು ಅವರು ಒಪ್ಪಿದ್ದಾರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'forcibly_closing_the_argument1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'forcibly_closing_the_argument1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				<td>39</td>
				<td> ನಿಮಗೆ ಬೇಕಾಗಿರುವುದನ್ನೇ ಅವರೂ ಒಪ್ಪಿರುವುದು </td>
				<?php for($i = 1;$i >= 0;$i--):?><td><?= $form->field($model, 'agreed_to_ur_partner_wanted_things1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'agreed_to_ur_partner_wanted_things1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
	</tbody>	
	</table><br>
	
	<table border = "1" class="responsive-scales">	
			 <thead>
				<tr>
					<td colspan="3"></td>
					<td colspan="4">ಹೌದು ಎಂದಾದಲ್ಲಿ ಕೆಳಗಿನ ಪ್ರಶ್ನೆಗಳಿಗೆ ಉತ್ತರಿಸಿ </td>
	<button type="button" id="na1" class="btn btn-primary">Not applicable</button>

				</tr>
                <tr>
					
                    <th> ನೀವು  	 ಗರ್ಭಿಣಿಯಾಗಿದ್ದಾಗ  ಒಮ್ಮೆ  ಅಥವಾ   ಹೆಚ್ಚಿನ  ಬಾರಿ   ನೀವೇನಾದರೂ;
							(ನಿಮಗೆ ಒಪ್ಪುವುದನ್ನು ಗುರುತು ಹಾಕಿ )</th>
                    
                    <th>ಹೌದು </th>
					<th>ಇಲ್ಲ  </th>
					<th>ಮೊದಲ ಮೂರು ತಿಂಗಳ  1-12 ವಾರ </th>
					<th>ಎರಡನೆಯ ಮೂರು ತಿಂಗಳ 12-24 ವಾರಗಳು </th>
					<th>ಮೂರನೆಯ ಮೂರು ತಿಂಗಳು 24-  ಇಲ್ಲಿಯವರೆಗೂ </th>
					<th>Other Answers</th>
				</tr> 
            </thead>
		<tbody>
				<tr>
				
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಯ ಕೈಯನ್ನು ತಿರುಗಿಸಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'rotated_ur_partner_hand')->radio(['label' => $i, 'value' => $i,'class'=>'aaa'])?></td>
				<?php endfor;?>
				
				<td>
				<?= $form->field($model, 'rotated_ur_partner_hand_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				<td>
				<?= $form->field($model, 'rotated_ur_partner_hand_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'rotated_ur_partner_hand_desc3')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
	
				<td>
				<?= $form->field($model, 'rotated_ur_partner_hand')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				</tr>
	
				
				<tr>
				
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಯನ್ನು ಹಿಡಿದು ತಳ್ಳಿರುವುದು ಅಥವಾ ದೂಡಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'grabed_and_pushed_ur_husband')->radio(['label' => $i, 'value' => $i,'class'=>'aaa'])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'grabed_and_pushed_ur_husband_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'grabed_and_pushed_ur_husband_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'grabed_and_pushed_ur_husband_desc3')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'grabed_and_pushed_ur_husband')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಯ ಕಪಾಳಕ್ಕೆ ಹೊಡೆದಿರುವುದು </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'slapped_ur_husband')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'slapped_ur_husband_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'slapped_ur_husband_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'slapped_ur_husband_desc3')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'slapped_ur_husband')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಗೆ ಲೈಂಗಿಕ  ಕ್ರಿಯೆಗೆ ಬಲವಂತ ಮಾಡಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'forced_ur_partner_for_sex')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'forced_ur_partner_for_sex_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'forced_ur_partner_for_sex_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'forced_ur_partner_for_sex_desc3')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'forced_ur_partner_for_sex')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಯನ್ನು ಗಟ್ಟಿಯಾಗಿ ಹಿಡಿದು ಅಲುಗಾಡಿಸಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'shaked_ur_partner_by_holding_tight')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'shaked_ur_partner_by_holding_tight_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'shaked_ur_partner_by_holding_tight_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'shaked_ur_partner_by_holding_tight_desc3')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'shaked_ur_partner_by_holding_tight')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಯನ್ನು ಎಸೆದಿದ್ದೀರಾ   ಅಥವಾ ಎಸೆಯಲು  ಪ್ರಯತ್ನಿಸಿದ್ದೀರಾ   </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'tried_to_throw_ur_husband')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'tried_to_throw_ur_husband_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_throw_ur_husband_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_throw_ur_husband_desc3')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_throw_ur_husband')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಯ ಮೇಲೆ ವಸ್ತುಗಳನ್ನು ಎಸೆದಿದ್ದೀರಾ  </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'tried_to_throw_things_on_ur_partner')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'tried_to_throw_things_on_ur_partner_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_throw_things_on_ur_partner_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_throw_things_on_ur_partner_desc3')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_throw_things_on_ur_partner')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಯ ಕತ್ತು ಹಿಸುಕಿದ್ದೀರಾ ಅಥವಾ ಉಸಿರುಗಟ್ಟಿಸಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'tried_to_hold_ur_partners_neck')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'tried_to_hold_ur_partners_neck_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_hold_ur_partners_neck_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_hold_ur_partners_neck_desc3')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_hold_ur_partners_neck')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				
				<td> ಒದ್ದಿದ್ದಾರೆ, ಕಚ್ಚಿರುವುದು ಅಥವಾ ಮುಷ್ಠಿಯಿಂದ ಹೊಡೆದಿರುವುದು </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'tried_to_hit_kick_bite')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'tried_to_hit_kick_bite_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_hit_kick_bite_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_hit_kick_bite_desc3')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_hit_kick_bite')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				
				<td> ಯಾವುದಾದರೊಂದು ವಸ್ತುವಿನಿಂದ ಹೊಡೆದಿರುವುದು ಅಥವಾ ಹೊಡೆಯಲು ಪ್ರಯತ್ನಿಸಿದ್ದೀರಾ  </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'tried_to_hit_or_hitted')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'tried_to_hit_or_hitted_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_hit_or_hitted_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_hit_or_hitted_desc3')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_hit_or_hitted')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಯನ್ನು ಹೊಡೆದಿರುವುದು  </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'hitted_ur_partner')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'hitted_ur_partner_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'hitted_ur_partner_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'hitted_ur_partner_desc3')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'hitted_ur_partner')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				
				<td>  ನಿಮ್ಮ ಸಂಗಾತಿಯನ್ನು ಗನ್ ಅಥವಾ ಚಾಕುವಿನಿಂದ ಹೆದರಿಸಿದ್ದೀರಾ  </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'feared_ur_partner_by_gun_knife')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'feared_ur_partner_by_gun_knife_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'feared_ur_partner_by_gun_knife_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'feared_ur_partner_by_gun_knife_desc3')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'feared_ur_partner_by_gun_knife')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				
				<td> ಚಾಕು, ಗನ್ ಅಥವಾ ಇತರೆ ಆಯುಧಗಳನ್ನು ಉಪಯೋಗಿಸಿದ್ದರು </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'used_gun_knife')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'used_gun_knife_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'used_gun_knife_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'used_gun_knife_desc3')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'used_gun_knife')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
		</tbody>
		</table><br>
		
		
		
		
		
			
		<table border="1" class="responsive-scales">	
			<thead>
				<tr>
					<th colspan="4"></th>
					<th colspan="3">ಹೌದು ಎಂದಾದಲ್ಲಿ ಕೆಳಗಿನ ಪ್ರಶ್ನೆಗಳಿಗೆ ಉತ್ತರಿಸಿ </th>
					<button type="button" class="btn btn-primary" id="na2">Not applicable</button>
				</tr>
                <tr>
					
                    <th>  ನೀವು ಗರ್ಭಿಣಿಯಾಗಿದ್ದಾಗ ಒಮ್ಮೆ ಅಥವಾ ಹೆಚ್ಚಿನ ಬಾರಿ ನಿಮ್ಮ ಸಂಗಾತಿ ಏನಾದಾರೂ;<br>
					ನಿಮಗೆ ಒಪ್ಪುವುದನ್ನು ಗುರುತು ಹಾಕಿ</th>
                    <th>ಹೌದು </th>
                    <th>ಇಲ್ಲ  </th>
					<th>ಮೊದಲ ಮೂರು ತಿಂಗಳ  1-12 ವಾರ </th>
					<th>ಎರಡನೆಯ ಮೂರು ತಿಂಗಳ 12-24 ವಾರಗಳು </th>
					<th>ಮೂರನೆಯ ಮೂರು ತಿಂಗಳು 24-  ಇಲ್ಲಿಯವರೆಗೂ </th>
					<th>Other Answers</th>
				</tr>
            </thead>
		<tbody>	
				
				<tr>
				
				<td> ನಿಮ್ಮ  ಕೈಯನ್ನು ತಿರುಗಿಸಿದ್ದಾರ </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'rotated_ur_partner_hand1')->radio(['label' => $i, 'value' => $i])->label(false);?>
					
				</td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'rotated_ur_partner_hand1_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'rotated_ur_partner_hand1_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'rotated_ur_partner_hand1_desc3')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'rotated_ur_partner_hand1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				
				<td> ನಿಮ್ಮನ್ನು ಹಿಡಿದು ತಳ್ಳಿರುವುದು ಅಥವಾ ದೂಡಿರುವುದು </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'grabed_and_pushed_ur_husband1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'grabed_and_pushed_ur_husband1_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'grabed_and_pushed_ur_husband1_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'grabed_and_pushed_ur_husband1_desc3')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'grabed_and_pushed_ur_husband1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				
				<td> ನಿಮ್ಮ  ಕಪಾಳಕ್ಕೆ ಹೊಡೆದಿರುವುದು </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'slapped_ur_husband1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'slapped_ur_husband1_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'slapped_ur_husband1_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'slapped_ur_husband1_desc3')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'slapped_ur_husband1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				
				<td> ನಿಮ್ಮನ್ನು  ಲೈಂಗಿಕ ಕ್ರಿಯೆಗೆ ಬಲವಂತ ಮಾಡಿರುವುದು </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'forced_ur_partner_for_sex1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'forced_ur_partner_for_sex1_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'forced_ur_partner_for_sex1_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'forced_ur_partner_for_sex1_desc3')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'forced_ur_partner_for_sex1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				</tr>
				
				<tr>
				
				<td> ನಿಮ್ಮನ್ನು  ಗಟ್ಟಿಯಾಗಿ ಹಿಡಿದು ಅಲುಗಾಡಿಸಿರುವುದು </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'shaked_ur_partner_by_holding_tight1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				<td>
				<?= $form->field($model, 'shaked_ur_partner_by_holding_tight1_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'shaked_ur_partner_by_holding_tight1_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'shaked_ur_partner_by_holding_tight1_desc3')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				<td>
				<?= $form->field($model, 'shaked_ur_partner_by_holding_tight1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				
				</tr>
				
				<tr>
				
				<td>  ನಿಮ್ಮನ್ನು  ಎಸೆದಿರುವುದು   ಅಥವಾ ಎಸೆಯಲು  ಪ್ರಯತ್ನಿಸಿರುವುದು </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'tried_to_throw_ur_husband1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				
				<td>
				<?= $form->field($model, 'tried_to_throw_ur_husband1_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_throw_ur_husband1_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_throw_ur_husband1_desc3')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				<td>
				<?= $form->field($model, 'tried_to_throw_ur_husband1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				
				
				
				</tr>
				
				<tr>
				
				<td> ನಿಮ್ಮ  ಮೇಲೆ ವಸ್ತುಗಳನ್ನು ಎಸೆದಿರುವುದು  </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'tried_to_throw_things_on_ur_partner1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				<td>
				<?= $form->field($model, 'tried_to_throw_things_on_ur_partner1_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_throw_things_on_ur_partner1_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_throw_things_on_ur_partner1_desc3')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'tried_to_throw_things_on_ur_partner1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				
				
				</tr>
				
				<tr>
				
				<td> ನಿಮ್ಮ  ಕತ್ತನ್ನು ಹಿಸುಕಿರುವುದು ಅಥವಾ ಉಸಿರುಗಟ್ಟಿಸಿರುವುದು </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'tried_to_hold_ur_partners_neck1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				
				<td>
				<?= $form->field($model, 'tried_to_hold_ur_partners_neck1_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_hold_ur_partners_neck1_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_hold_ur_partners_neck1_desc3')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'tried_to_hold_ur_partners_neck1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				
				
				</tr>
				
				<tr>
				
				<td> ಒದ್ದಿರುವುದು, ಕಚ್ಚಿರುವುದು ಅಥವಾ ಮುಷ್ಠಿಯಿಂದ ಹೊಡೆದಿರುವುದು </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'tried_to_hit_kick_bite1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				
				<td>
				<?= $form->field($model, 'tried_to_hit_kick_bite1_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_hit_kick_bite1_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_hit_kick_bite1_desc3')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				
				
				<td>
				<?= $form->field($model, 'tried_to_hit_kick_bite1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				</tr>
				
				<tr>
				
				<td> ಯಾವುದಾದರೊಂದು ವಸ್ತುವಿನಿಂದ ಹೊಡೆದಿರುವುದು ಅಥವಾ ಹೊಡೆಯಲು ಪ್ರಯತ್ನಿಸಿರುವುದು  </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'tried_to_hit_or_hitted1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				<td>
				<?= $form->field($model, 'tried_to_hit_or_hitted1_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_hit_or_hitted1_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_hit_or_hitted1_desc3')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'tried_to_hit_or_hitted1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				
				
				</tr>
				
				<tr>
				
				<td> ನಿಮ್ಮನ್ನು  ಹೊಡೆದಿರುವುದು  </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'hitted_ur_partner1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				<td>
				<?= $form->field($model, 'hitted_ur_partner1_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'hitted_ur_partner1_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'hitted_ur_partner1_desc3')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'hitted_ur_partner1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				
				</tr>
				
				<tr>
				
				<td> ನಿಮ್ಮನ್ನು  ಗನ್ ಅಥವಾ ಚಾಕುವಿನಿಂದ ಹೆದರಿಸಿರುವುದು </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'feared_ur_partner_by_gun_knife1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				<td>
				<?= $form->field($model, 'feared_ur_partner_by_gun_knife1_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'feared_ur_partner_by_gun_knife1_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'feared_ur_partner_by_gun_knife1_desc3')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
	
				<td>
				<?= $form->field($model, 'feared_ur_partner_by_gun_knife1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				
				</tr>
				
				<tr>
				
				<td> ಚಾಕು, ಗನ್ ಅಥವಾ ಇತರೆ ಆಯುಧಗಳನ್ನು ಉಪಯೋಗಿಸಿರುವುದು </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'used_gun_knife1')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				<td>
				<?= $form->field($model, 'used_gun_knife1_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'used_gun_knife1_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'used_gun_knife1_desc3')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				
				
				<td>
				<?= $form->field($model, 'used_gun_knife1')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				
				</tr>

		</tbody>
		</table><br>
		
		<table border = "1" class="responsive-scales">	
			<thead>
				<tr>
					<th colspan="4"></th>
					<th colspan="2">ಹೌದು ಎಂದಾದಲ್ಲಿ ಕೆಳಗಿನ ಪ್ರಶ್ನೆಗಳಿಗೆ ಉತ್ತರಿಸಿ </th>
					<button type="button" class="btn btn-primary" id="na3">Not applicable</button>
				</tr>
                <tr>
					<th>sl.no</th>
                    <th > ಒಮ್ಮೆ ಅಥವಾ ಹೆಚ್ಚಿನ ಬಾರಿ ಈ ಸಮಯದಲ್ಲಿ  ಈ ಕೆಳಗಿನವುಗಳನ್ನು  ನೀವೇನಾದರೂ;<br>
					ನಿಮಗೆ ಒಪ್ಪುವುದನ್ನು ಗುರುತು ಹಾಕಿ</td>
                    <th>ಹೌದು </th>
					<th>ಇಲ್ಲ  </th>
					<th>ಮಗು ಹುಟ್ಟಿದ  6 ತಿಂಗಳಿನಿಂದ </th>
					<th>6 ತಿಂಗಳಿನಿಂದ ಇಲ್ಲಿಯವರೆಗೆ </th>
					<th>Other Answers</th>
				</tr> 
            </thead>
		<tbody>					
				<tr>
				<td>1</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಯ ಕೈಯನ್ನು ತಿರುಗಿಸಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'rotated_ur_partner_hand12')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				<td>
				<?= $form->field($model, 'rotated_ur_partner_hand12_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'rotated_ur_partner_hand12_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
	
				<td>
				<?= $form->field($model, 'rotated_ur_partner_hand12')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				</tr>
				
				<tr>
				<td>2</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಯನ್ನು ಹಿಡಿದು ತಳ್ಳಿರುವುದು ಅಥವಾ ದೂಡಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'grabed_and_pushed_ur_husband12')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				<td>
				<?= $form->field($model, 'grabed_and_pushed_ur_husband12_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'grabed_and_pushed_ur_husband12_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
			
				<td>
				<?= $form->field($model, 'grabed_and_pushed_ur_husband12')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				</tr>
				
				<tr>
				<td>3</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಯ ಕಪಾಳಕ್ಕೆ ಹೊಡೆದಿರುವುದು </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'slapped_ur_husband12')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				<td>
				<?= $form->field($model, 'slapped_ur_husband12_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'slapped_ur_husband12_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'slapped_ur_husband12')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				
				
				</tr>
				
				<tr>
				<td>4</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಗೆ ಲೈಂಗಿಕ  ಕ್ರಿಯೆಗೆ ಬಲವಂತ ಮಾಡಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'forced_ur_partner_for_sex12')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				<td>
				<?= $form->field($model, 'forced_ur_partner_for_sex12_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'forced_ur_partner_for_sex12_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'forced_ur_partner_for_sex12')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				
				</tr>
				
				<tr>
				<td>5</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಯನ್ನು ಗಟ್ಟಿಯಾಗಿ ಹಿಡಿದು ಅಲುಗಾಡಿಸಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'shaked_ur_partner_by_holding_tight12')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				<td>
				<?= $form->field($model, 'shaked_ur_partner_by_holding_tight12_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'shaked_ur_partner_by_holding_tight12_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				<td>
				<?= $form->field($model, 'shaked_ur_partner_by_holding_tight12')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				
				</tr>
				
				<tr>
				<td>6</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಯನ್ನು ಎಸೆದಿದ್ದೀರಾ   ಅಥವಾ ಎಸೆಯಲು  ಪ್ರಯತ್ನಿಸಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'tried_to_throw_ur_husband12')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				<td>
				<?= $form->field($model, 'tried_to_throw_ur_husband12_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_throw_ur_husband12_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				<td>
				<?= $form->field($model, 'tried_to_throw_ur_husband12')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				
				
				</tr>
				
				<tr>
				<td>7</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಯ ಮೇಲೆ ವಸ್ತುಗಳನ್ನು ಎಸೆದಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'tried_to_throw_things_on_ur_partner12')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				<td>
				<?= $form->field($model, 'tried_to_throw_things_on_ur_partner12_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_throw_things_on_ur_partner12_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				<td>
				<?= $form->field($model, 'tried_to_throw_things_on_ur_partner12')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				</tr>
				
				<tr>
				<td>8</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಯ ಕತ್ತು ಹಿಸುಕಿದ್ದೀರಾ ಅಥವಾ ಉಸಿರುಗಟ್ಟಿಸಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'tried_to_hold_ur_partners_neck12')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				
				<td>
				<?= $form->field($model, 'tried_to_hold_ur_partners_neck12_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_hold_ur_partners_neck12_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'tried_to_hold_ur_partners_neck12')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				
				
				</tr>
				
				<tr>
				<td>9</td>
				<td>  ಒದ್ದಿದ್ದಾರೆ, ಕಚ್ಚಿರುವುದು ಅಥವಾ ಮುಷ್ಠಿಯಿಂದ ಹೊಡೆದಿರುವುದು </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'tried_to_hit_kick_bite12')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				
				<td>
				<?= $form->field($model, 'tried_to_hit_kick_bite12_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_hit_kick_bite12_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'tried_to_hit_kick_bite12')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				</tr>
				
				<tr>
				<td>10</td>
				<td> ಯಾವುದಾದರೊಂದು ವಸ್ತುವಿನಿಂದ ಹೊಡೆದಿರುವುದು ಅಥವಾ ಹೊಡೆಯಲು ಪ್ರಯತ್ನಿಸಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'tried_to_hit_or_hitted12')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				<td>
				<?= $form->field($model, 'tried_to_hit_or_hitted12_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_hit_or_hitted12_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'tried_to_hit_or_hitted12')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				</tr>
				
				<tr>
				<td>11</td>
				<td> ನಿಮ್ಮ ಸಂಗಾತಿಯನ್ನು ಹೊಡೆದಿರುವುದು </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'hitted_ur_partner12')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				<td>
				<?= $form->field($model, 'hitted_ur_partner12_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'hitted_ur_partner12_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'hitted_ur_partner12')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				</tr>
				
				<tr>
				<td>12</td>
				<td>  ನಿಮ್ಮ ಸಂಗಾತಿಯನ್ನು ಗನ್ ಅಥವಾ ಚಾಕುವಿನಿಂದ ಹೆದರಿಸಿದ್ದೀರಾ </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'feared_ur_partner_by_gun_knife12')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				<td>
				<?= $form->field($model, 'feared_ur_partner_by_gun_knife12_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'feared_ur_partner_by_gun_knife12_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				
				
				<td>
				<?= $form->field($model, 'feared_ur_partner_by_gun_knife12')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				</tr>
				
				<tr>
				<td>13</td>
				<td>  ಚಾಕು, ಗನ್ ಅಥವಾ ಇತರೆ ಆಯುಧಗಳನ್ನು ಉಪಯೋಗಿಸಿದ್ದರು </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'used_gun_knife12')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				<td>
				<?= $form->field($model, 'used_gun_knife12_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'used_gun_knife12_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'used_gun_knife12')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				</tr>
				
		</tbody>
		</table><br>
		
		
		<table border="1" class="responsive-scales" width="100%;">	
			<thead>
				<tr>
					<th colspan="4"></th>
					<th colspan="3">ಹೌದು ಎಂದಾದಲ್ಲಿ ಕೆಳಗಿನ ಪ್ರಶ್ನೆಗಳಿಗೆ ಉತ್ತರಿಸಿ </th>
					<button type="button" class="btn btn-primary" id="na4">Not applicable</button>
				</tr>
                <tr>
					<th>sl.no</th>
                    <th >  ಒಮ್ಮೆ ಅಥವಾ ಹೆಚ್ಚಿನ ಬಾರಿ  ಈ ಸಮಯದಲ್ಲಿ  ಈ ಕೆಳಗಿನವುಗಳನ್ನು ನಿಮ್ಮ ಸಂಗಾತಿ ಏನಾದಾರೂ;<br>
					ನಿಮಗೆ ಒಪ್ಪುವುದನ್ನು ಗುರುತು ಹಾಕಿ</td>
                    <th>ಹೌದು </th>
					<th>ಇಲ್ಲ  </th>
                    <th>ಮಗು ಹುಟ್ಟಿದ  6 ತಿಂಗಳಿನಿಂದ </th>
					<th>6 ತಿಂಗಳಿನಿಂದ ಇಲ್ಲಿಯವರೆಗೆ </th>
					<th>Other Answers</th>
				 </tr> 
            </thead>
		<tbody>				

				<tr>
				<td>1</td>
				<td> ನಿಮ್ಮ  ಕೈಯನ್ನು ತಿರುಗಿಸಿದ್ದಾರ  </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'rotated_ur_partner_hand123')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				<td>
				<?= $form->field($model, 'rotated_ur_partner_hand123_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'rotated_ur_partner_hand123_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'rotated_ur_partner_hand123')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				</tr>
				
				<tr>
				<td>2</td>
				<td>  ನಿಮ್ಮನ್ನು ಹಿಡಿದು ತಳ್ಳಿರುವುದು ಅಥವಾ ದೂಡಿರುವುದು </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'grabed_and_pushed_ur_husband123')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				<td>
				<?= $form->field($model, 'grabed_and_pushed_ur_husband123_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'grabed_and_pushed_ur_husband123_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'grabed_and_pushed_ur_husband123')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				</tr>
				
				<tr>
				<td>3</td>
				<td> ನಿಮ್ಮ  ಕಪಾಳಕ್ಕೆ ಹೊಡೆದಿರುವುದು </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'slapped_ur_husband123')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				<td>
				<?= $form->field($model, 'slapped_ur_husband123_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'slapped_ur_husband123_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'slapped_ur_husband123')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				</tr>
				
				<tr>
				<td>4</td>
				<td>  ನಿಮ್ಮನ್ನು  ಲೈಂಗಿಕ ಕ್ರಿಯೆಗೆ ಬಲವಂತ ಮಾಡಿರುವುದು </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'forced_ur_partner_for_sex123')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				<td>
				<?= $form->field($model, 'forced_ur_partner_for_sex123_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'forced_ur_partner_for_sex123_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'forced_ur_partner_for_sex123')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				</tr>
				
				<tr>
				<td>5</td>
				<td> ನಿಮ್ಮನ್ನು  ಗಟ್ಟಿಯಾಗಿ ಹಿಡಿದು ಅಲುಗಾಡಿಸಿರುವುದು </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'shaked_ur_partner_by_holding_tight123')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				
				<td>
				<?= $form->field($model, 'shaked_ur_partner_by_holding_tight123_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'shaked_ur_partner_by_holding_tight123_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'shaked_ur_partner_by_holding_tight123')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				</tr>
				
				<tr>
				<td>6</td>
				<td> ನಿಮ್ಮನ್ನು  ಎಸೆದಿರುವುದು   ಅಥವಾ ಎಸೆಯಲು  ಪ್ರಯತ್ನಿಸಿರುವುದು </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'tried_to_throw_ur_husband123')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				<td>
				<?= $form->field($model, 'tried_to_throw_ur_husband123_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_throw_ur_husband123_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				
				
				<td>
				<?= $form->field($model, 'tried_to_throw_ur_husband123')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				</tr>
				
				<tr>
				<td>7</td>
				<td> ನಿಮ್ಮ  ಮೇಲೆ ವಸ್ತುಗಳನ್ನು ಎಸೆದಿರುವುದು </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'tried_to_throw_things_on_ur_partner123')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				<td>
				<?= $form->field($model, 'tried_to_throw_things_on_ur_partner123_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_throw_things_on_ur_partner123_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'tried_to_throw_things_on_ur_partner123')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				</tr>
				
				<tr>
				<td>8</td>
				<td> ನಿಮ್ಮ  ಕತ್ತನ್ನು ಹಿಸುಕಿರುವುದು ಅಥವಾ ಉಸಿರುಗಟ್ಟಿಸಿರುವುದು </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'tried_to_hold_ur_partners_neck123')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				<td>
				<?= $form->field($model, 'tried_to_hold_ur_partners_neck123_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_hold_ur_partners_neck123_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'tried_to_hold_ur_partners_neck123')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				</tr>
				
				<tr>
				<td>9</td>
				<td> ಒದ್ದಿರುವುದು, ಕಚ್ಚಿರುವುದು ಅಥವಾ ಮುಷ್ಠಿಯಿಂದ ಹೊಡೆದಿರುವುದು </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'tried_to_hit_kick_bite123')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				
				<td>
				<?= $form->field($model, 'tried_to_hit_kick_bite123_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_hit_kick_bite123_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				
				
				<td>
				<?= $form->field($model, 'tried_to_hit_kick_bite123')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				</tr>
				
				<tr>
				<td>10</td>
				<td> ಯಾವುದಾದರೊಂದು ವಸ್ತುವಿನಿಂದ ಹೊಡೆದಿರುವುದು ಅಥವಾ ಹೊಡೆಯಲು ಪ್ರಯತ್ನಿಸಿರುವುದು </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'tried_to_hit_or_hitted123')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				<td>
				<?= $form->field($model, 'tried_to_hit_or_hitted123_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'tried_to_hit_or_hitted123_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				
				
				<td>
				<?= $form->field($model, 'tried_to_hit_or_hitted123')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				</tr>
				
				<tr>
				<td>11</td>
				<td> ನಿಮ್ಮನ್ನು  ಹೊಡೆದಿರುವುದು  </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'hitted_ur_partner123')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				
				<td>
				<?= $form->field($model, 'hitted_ur_partner123_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'hitted_ur_partner123_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'hitted_ur_partner123')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				</tr>
				
				<tr>
				<td>12</td>
				<td> ನಿಮ್ಮನ್ನು  ಗನ್ ಅಥವಾ ಚಾಕುವಿನಿಂದ ಹೆದರಿಸಿರುವುದು </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'feared_ur_partner_by_gun_knife123')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				
				<td>
				<?= $form->field($model, 'feared_ur_partner_by_gun_knife123_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'feared_ur_partner_by_gun_knife123_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				
				<td>
				<?= $form->field($model, 'feared_ur_partner_by_gun_knife123')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				</tr>
				
				<tr>
				<td>13</td>
				<td> ಚಾಕು, ಗನ್ ಅಥವಾ ಇತರೆ ಆಯುಧಗಳನ್ನು ಉಪಯೋಗಿಸಿರುವುದು </td>
				<?php for($i = 1;$i >= 0;$i--):?>
				<td><?= $form->field($model, 'used_gun_knife123')->radio(['label' => $i, 'value' => $i])?></td>
				<?php endfor;?>
				
				
				<td>
				<?= $form->field($model, 'used_gun_knife123_desc1')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				<td>
				<?= $form->field($model, 'used_gun_knife123_desc2')->dropDownList([''=>'','1'=>'Yes','0'=>'No','-10'=>'NA'])->label(false) ?>
				</td>
				
				
				
				<td>
				<?= $form->field($model, 'used_gun_knife123')->dropDownList($keyPair,['prompt' => ' '])->label(false)?>
				</td>
				
				
				</tr>
		</tbody>
		</table>
		

	<div id="scaleflag" class="hidden">
	<?= $form->field($model, 'score')->input('number',['readonly'=>true])->label(false);?>
	</div>
	</div>
    <?php ActiveForm::end(); ?>
</div>	
<?php $this->registerJs('
	$("#"+formObj.formName).one("change",function()
	{
		autoSaveFormObj = formObj;
	});

	$("#conflicttacticsscale-supporting_your_partner_at_difficult_times").click(function(){
		//alert("calling");
		});
')?>

<?php if($model->not_applicable == 'YES'){
	$this->registerJs('
	setNotApplicableOnLoad("#withBoxShadow","#conflicttacticsscale-not_applicable");
	', \yii\web\View::POS_READY);

}
	$this->registerJs('

	$("#conflicttacticsscale-not_applicable").change(function()
		{	
		    var value = $(this).val();
			if(value=="YES")
			{	
				setNotApplicableYes("#withBoxShadow","#conflicttacticsscale-not_applicable");
			}
			else
			{
				setNotApplicableNo("#withBoxShadow","#conflicttacticsscale-not_applicable");
			}
		});
	
	$("input[name=\"ConflictTacticsScale[rotated_ur_partner_hand1]\"]").change(function(){
		console.log($(this).val())
		//$("input[name=\"ConflictTacticsScale[rotated_ur_partner_hand1]\"]").attr("checked", false);
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-rotated_ur_partner_hand1_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-rotated_ur_partner_hand1_desc"+i).val("")
			}
		}
		});


		$("input[name=\"ConflictTacticsScale[rotated_ur_partner_hand]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-rotated_ur_partner_hand_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-rotated_ur_partner_hand_desc"+i).val("")
			}
		}
		});

		$("input[name=\"ConflictTacticsScale[grabed_and_pushed_ur_husband]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-grabed_and_pushed_ur_husband_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-grabed_and_pushed_ur_husband_desc"+i).val("")
			}
		}
		});

		$("input[name=\"ConflictTacticsScale[slapped_ur_husband]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-slapped_ur_husband_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-slapped_ur_husband_desc"+i).val("")
			}
		}
		});

		$("input[name=\"ConflictTacticsScale[forced_ur_partner_for_sex]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-forced_ur_partner_for_sex_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-forced_ur_partner_for_sex_desc"+i).val("")
			}
		}
		});

		$("input[name=\"ConflictTacticsScale[shaked_ur_partner_by_holding_tight]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-shaked_ur_partner_by_holding_tight_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-shaked_ur_partner_by_holding_tight_desc"+i).val("")
			}
		}
		});

		$("input[name=\"ConflictTacticsScale[tried_to_throw_ur_husband]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-tried_to_throw_ur_husband_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-tried_to_throw_ur_husband_desc"+i).val("")
			}
		}
		});

		$("input[name=\"ConflictTacticsScale[tried_to_throw_things_on_ur_partner]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-tried_to_throw_things_on_ur_partner_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-tried_to_throw_things_on_ur_partner_desc"+i).val("")
			}
		}
		});

		$("input[name=\"ConflictTacticsScale[tried_to_hold_ur_partners_neck]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-tried_to_hold_ur_partners_neck_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-tried_to_hold_ur_partners_neck_desc"+i).val("")
			}
		}
		});

		$("input[name=\"ConflictTacticsScale[tried_to_hit_kick_bite]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-tried_to_hit_kick_bite_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-tried_to_hit_kick_bite_desc"+i).val("")
			}
		}
		});

		$("input[name=\"ConflictTacticsScale[tried_to_hit_or_hitted]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-tried_to_hit_or_hitted_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-tried_to_hit_or_hitted_desc"+i).val("")
			}
		}
		});


		$("input[name=\"ConflictTacticsScale[hitted_ur_partner]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-hitted_ur_partner_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-hitted_ur_partner_desc"+i).val("")
			}
		}
		});



		$("input[name=\"ConflictTacticsScale[shaked_ur_partner_by_holding_tight12]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-shaked_ur_partner_by_holding_tight12_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-shaked_ur_partner_by_holding_tight12_desc"+i).val("")
			}
		}
		});


		$("input[name=\"ConflictTacticsScale[tried_to_throw_ur_husband12]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-tried_to_throw_ur_husband12_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-tried_to_throw_ur_husband12_desc"+i).val("")
			}
		}
		});


		$("input[name=\"ConflictTacticsScale[tried_to_throw_things_on_ur_partner12]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-tried_to_throw_things_on_ur_partner12_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-tried_to_throw_things_on_ur_partner12_desc"+i).val("")
			}
		}
		});


		$("input[name=\"ConflictTacticsScale[tried_to_hold_ur_partners_neck12]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-tried_to_hold_ur_partners_neck12_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-tried_to_hold_ur_partners_neck12_desc"+i).val("")
			}
		}
		});


		$("input[name=\"ConflictTacticsScale[slapped_ur_husband1]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-slapped_ur_husband1_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-slapped_ur_husband1_desc"+i).val("")
			}
		}
		});


		$("input[name=\"ConflictTacticsScale[forced_ur_partner_for_sex1]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-forced_ur_partner_for_sex1_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-forced_ur_partner_for_sex1_desc"+i).val("")
			}
		}
		});


		$("input[name=\"ConflictTacticsScale[shaked_ur_partner_by_holding_tight1]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-shaked_ur_partner_by_holding_tight1_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-shaked_ur_partner_by_holding_tight1_desc"+i).val("")
			}
		}
		});


		$("input[name=\"ConflictTacticsScale[tried_to_throw_ur_husband1]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-tried_to_throw_ur_husband1_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-tried_to_throw_ur_husband1_desc"+i).val("")
			}
		}
		});


		$("input[name=\"ConflictTacticsScale[tried_to_throw_things_on_ur_partner1]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-tried_to_throw_things_on_ur_partner1_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-tried_to_throw_things_on_ur_partner1_desc"+i).val("")
			}
		}
		});


		$("input[name=\"ConflictTacticsScale[tried_to_hold_ur_partners_neck1]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-tried_to_hold_ur_partners_neck1_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-tried_to_hold_ur_partners_neck1_desc"+i).val("")
			}
		}
		});


		$("input[name=\"ConflictTacticsScale[tried_to_hit_kick_bite1]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-tried_to_hit_kick_bite1_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-tried_to_hit_kick_bite1_desc"+i).val("")
			}
		}
		});

		$("input[name=\"ConflictTacticsScale[tried_to_hit_or_hitted1]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-tried_to_hit_or_hitted1_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-tried_to_hit_or_hitted1_desc"+i).val("")
			}
		}
		});


		$("input[name=\"ConflictTacticsScale[hitted_ur_partner1]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-hitted_ur_partner1_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-hitted_ur_partner1_desc"+i).val("")
			}
		}
		});


		$("input[name=\"ConflictTacticsScale[feared_ur_partner_by_gun_knife1]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-feared_ur_partner_by_gun_knife1_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-feared_ur_partner_by_gun_knife1_desc"+i).val("")
			}
		}
		});



		$("input[name=\"ConflictTacticsScale[used_gun_knife1]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-used_gun_knife1_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-used_gun_knife1_desc"+i).val("")
			}
		}
		});


		$("input[name=\"ConflictTacticsScale[rotated_ur_partner_hand12]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-rotated_ur_partner_hand12_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-rotated_ur_partner_hand12_desc"+i).val("")
			}
		}
		});

		$("input[name=\"ConflictTacticsScale[tried_to_hit_kick_bite12]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-tried_to_hit_kick_bite12_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-tried_to_hit_kick_bite12_desc"+i).val("")
			}
		}
		});


		$("input[name=\"ConflictTacticsScale[slapped_ur_husband12]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-slapped_ur_husband12_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-slapped_ur_husband12_desc"+i).val("")
			}
		}
		});


		$("input[name=\"ConflictTacticsScale[forced_ur_partner_for_sex12]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-forced_ur_partner_for_sex12_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-forced_ur_partner_for_sex12_desc"+i).val("")
			}
		}
		});

		$("input[name=\"ConflictTacticsScale[tried_to_hit_or_hitted12]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-tried_to_hit_or_hitted12_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-tried_to_hit_or_hitted12_desc"+i).val("")
			}
		}
		});


		$("input[name=\"ConflictTacticsScale[hitted_ur_partner12]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-hitted_ur_partner12_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-hitted_ur_partner12_desc"+i).val("")
			}
		}
		});

		$("input[name=\"ConflictTacticsScale[feared_ur_partner_by_gun_knife12]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-feared_ur_partner_by_gun_knife12_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-feared_ur_partner_by_gun_knife12_desc"+i).val("")
			}
		}
		});


		$("input[name=\"ConflictTacticsScale[used_gun_knife12]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-used_gun_knife12_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-used_gun_knife12_desc"+i).val("")
			}
		}
		});


		$("input[name=\"ConflictTacticsScale[rotated_ur_partner_hand123]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-rotated_ur_partner_hand123_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-rotated_ur_partner_hand123_desc"+i).val("")
			}
		}
		});

		$("input[name=\"ConflictTacticsScale[slapped_ur_husband123]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-slapped_ur_husband123_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-slapped_ur_husband123_desc"+i).val("")
			}
		}
		});


		$("input[name=\"ConflictTacticsScale[forced_ur_partner_for_sex123]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-forced_ur_partner_for_sex123_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-forced_ur_partner_for_sex123_desc"+i).val("")
			}
		}
		});

		$("input[name=\"ConflictTacticsScale[shaked_ur_partner_by_holding_tight123]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-shaked_ur_partner_by_holding_tight123_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-shaked_ur_partner_by_holding_tight123_desc"+i).val("")
			}
		}
		});


		$("input[name=\"ConflictTacticsScale[tried_to_throw_ur_husband123]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-tried_to_throw_ur_husband123_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-tried_to_throw_ur_husband123_desc"+i).val("")
			}
		}
		});


		$("input[name=\"ConflictTacticsScale[tried_to_throw_things_on_ur_partner123]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-tried_to_throw_things_on_ur_partner123_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-tried_to_throw_things_on_ur_partner123_desc"+i).val("")
			}
		}
		});

		$("input[name=\"ConflictTacticsScale[tried_to_hold_ur_partners_neck123]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-tried_to_hold_ur_partners_neck123_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-tried_to_hold_ur_partners_neck123_desc"+i).val("")
			}
		}
		});


		$("input[name=\"ConflictTacticsScale[tried_to_hit_kick_bite123]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-tried_to_hit_kick_bite123_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-tried_to_hit_kick_bite123_desc"+i).val("")
			}
		}
		});

		$("input[name=\"ConflictTacticsScale[tried_to_hit_or_hitted123]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-tried_to_hit_or_hitted123_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-tried_to_hit_or_hitted123_desc"+i).val("")
			}
		}
		});


		$("input[name=\"ConflictTacticsScale[hitted_ur_partner123]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-hitted_ur_partner123_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-hitted_ur_partner123_desc"+i).val("")
			}
		}
		});


		$("input[name=\"ConflictTacticsScale[feared_ur_partner_by_gun_knife123]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-feared_ur_partner_by_gun_knife123_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-feared_ur_partner_by_gun_knife123_desc"+i).val("")
			}
		}
		});

		$("input[name=\"ConflictTacticsScale[used_gun_knife123]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-used_gun_knife123_desc"+i).val("-10")
			}
		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-used_gun_knife123_desc"+i).val("")
			}
		}
		});

		$("input[name=\"ConflictTacticsScale[grabed_and_pushed_ur_husband1]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-grabed_and_pushed_ur_husband1_desc"+i).val("-10")
			}

		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-grabed_and_pushed_ur_husband1_desc"+i).val("")
			}
		}
		});

		$("input[name=\"ConflictTacticsScale[grabed_and_pushed_ur_husband123]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-grabed_and_pushed_ur_husband123_desc"+i).val("-10")
			}

		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-grabed_and_pushed_ur_husband123_desc"+i).val("")
			}
		}
		});


		$("input[name=\"ConflictTacticsScale[grabed_and_pushed_ur_husband12]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-grabed_and_pushed_ur_husband12_desc"+i).val("-10")
			}

		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 2 ; i++)
			{
			$("#conflicttacticsscale-grabed_and_pushed_ur_husband12_desc"+i).val("")
			}
		}
		});

		$("input[name=\"ConflictTacticsScale[feared_ur_partner_by_gun_knife]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-feared_ur_partner_by_gun_knife_desc"+i).val("-10")
			}

		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-feared_ur_partner_by_gun_knife_desc"+i).val("")
			}
		}
		});

		$("input[name=\"ConflictTacticsScale[used_gun_knife]\"]").change(function(){
		console.log($(this).val())
		if(!parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-used_gun_knife_desc"+i).val("-10")
			}

		}

		if(parseInt($(this).val()))
		{
			for(var i=1 ; i <= 3 ; i++)
			{
			$("#conflicttacticsscale-used_gun_knife_desc"+i).val("")
			}
		}
		});

		$("#na1").click(function(){
			//alert("hii");
			for(i=1;i<=3;i++)
			{
			$("#conflicttacticsscale-rotated_ur_partner_hand_desc"+i).val("-10");
			$("#conflicttacticsscale-grabed_and_pushed_ur_husband_desc"+i).val("-10");
			$("#conflicttacticsscale-slapped_ur_husband_desc"+i).val("-10");
			$("#conflicttacticsscale-forced_ur_partner_for_sex_desc"+i).val("-10");
			$("#conflicttacticsscale-shaked_ur_partner_by_holding_tight_desc"+i).val("-10");
			$("#conflicttacticsscale-tried_to_throw_ur_husband_desc"+i).val("-10");
			$("#conflicttacticsscale-tried_to_throw_things_on_ur_partner_desc"+i).val("-10");
			$("#conflicttacticsscale-tried_to_hold_ur_partners_neck_desc"+i).val("-10");
			$("#conflicttacticsscale-tried_to_hit_kick_bite_desc"+i).val("-10");
			$("#conflicttacticsscale-tried_to_hit_or_hitted_desc"+i).val("-10");
			$("#conflicttacticsscale-hitted_ur_partner_desc"+i).val("-10");
			$("#conflicttacticsscale-feared_ur_partner_by_gun_knife_desc"+i).val("-10");
			$("#conflicttacticsscale-used_gun_knife_desc"+i).val("-10");
		}
                $("select[name=\'ConflictTacticsScale[rotated_ur_partner_hand]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[grabed_and_pushed_ur_husband]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[slapped_ur_husband]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[forced_ur_partner_for_sex]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[shaked_ur_partner_by_holding_tight]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[tried_to_throw_ur_husband]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[tried_to_throw_things_on_ur_partner]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[tried_to_hold_ur_partners_neck]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[tried_to_hit_kick_bite]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[tried_to_hit_or_hitted]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[hitted_ur_partner]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[feared_ur_partner_by_gun_knife]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[used_gun_knife]\']").val("-10");
			autoSaveFormObj = formObj;
			 

			});


			$("#na2").click(function(){
			//alert("hii");
			for(i=1;i<=3;i++)
			{
			$("#conflicttacticsscale-rotated_ur_partner_hand1_desc"+i).val("-10");
			$("#conflicttacticsscale-grabed_and_pushed_ur_husband1_desc"+i).val("-10");
			$("#conflicttacticsscale-slapped_ur_husband1_desc"+i).val("-10");
			$("#conflicttacticsscale-forced_ur_partner_for_sex1_desc"+i).val("-10");
			$("#conflicttacticsscale-shaked_ur_partner_by_holding_tight1_desc"+i).val("-10");
			$("#conflicttacticsscale-tried_to_throw_ur_husband1_desc"+i).val("-10");
			$("#conflicttacticsscale-tried_to_throw_things_on_ur_partner1_desc"+i).val("-10");
			$("#conflicttacticsscale-tried_to_hold_ur_partners_neck1_desc"+i).val("-10");
			$("#conflicttacticsscale-tried_to_hit_kick_bite1_desc"+i).val("-10");
			$("#conflicttacticsscale-tried_to_hit_or_hitted1_desc"+i).val("-10");
			$("#conflicttacticsscale-hitted_ur_partner1_desc"+i).val("-10");
			$("#conflicttacticsscale-feared_ur_partner_by_gun_knife1_desc"+i).val("-10");
			$("#conflicttacticsscale-used_gun_knife1_desc"+i).val("-10");

                $("select[name=\'ConflictTacticsScale[rotated_ur_partner_hand1]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[grabed_and_pushed_ur_husband1]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[slapped_ur_husband1]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[forced_ur_partner_for_sex1]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[shaked_ur_partner_by_holding_tight1]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[tried_to_throw_ur_husband1]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[tried_to_throw_things_on_ur_partner1]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[tried_to_hold_ur_partners_neck1]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[tried_to_hit_kick_bite1]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[tried_to_hit_or_hitted1]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[hitted_ur_partner1]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[feared_ur_partner_by_gun_knife1]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[used_gun_knife1]\']").val("-10");
			autoSaveFormObj = formObj;
			
		    }

			});

	$("#na3").click(function(){
			//alert("hii");
			for(i=1;i<=2;i++)
			{
			$("#conflicttacticsscale-rotated_ur_partner_hand12_desc"+i).val("-10");
			$("#conflicttacticsscale-grabed_and_pushed_ur_husband12_desc"+i).val("-10");
			$("#conflicttacticsscale-slapped_ur_husband12_desc"+i).val("-10");
			$("#conflicttacticsscale-forced_ur_partner_for_sex12_desc"+i).val("-10");
			$("#conflicttacticsscale-shaked_ur_partner_by_holding_tight12_desc"+i).val("-10");
			$("#conflicttacticsscale-tried_to_throw_ur_husband12_desc"+i).val("-10");
			$("#conflicttacticsscale-tried_to_throw_things_on_ur_partner12_desc"+i).val("-10");
			$("#conflicttacticsscale-tried_to_hold_ur_partners_neck12_desc"+i).val("-10");
			$("#conflicttacticsscale-tried_to_hit_kick_bite12_desc"+i).val("-10");
			$("#conflicttacticsscale-tried_to_hit_or_hitted12_desc"+i).val("-10");
			$("#conflicttacticsscale-hitted_ur_partner12_desc"+i).val("-10");
			$("#conflicttacticsscale-feared_ur_partner_by_gun_knife12_desc"+i).val("-10");
			$("#conflicttacticsscale-used_gun_knife12_desc"+i).val("-10");
		


			 $("select[name=\'ConflictTacticsScale[rotated_ur_partner_hand12]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[grabed_and_pushed_ur_husband12]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[slapped_ur_husband12]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[forced_ur_partner_for_sex12]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[shaked_ur_partner_by_holding_tight12]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[tried_to_throw_ur_husband12]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[tried_to_throw_things_on_ur_partner12]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[tried_to_hold_ur_partners_neck12]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[tried_to_hit_kick_bite12]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[tried_to_hit_or_hitted12]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[hitted_ur_partner12]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[feared_ur_partner_by_gun_knife12]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[used_gun_knife12]\']").val("-10");

				autoSaveFormObj = formObj;


		    }

			});

			$("#na4").click(function(){
			//alert("hii");
			for(i=1;i<=2;i++)
			{
			$("#conflicttacticsscale-rotated_ur_partner_hand123_desc"+i).val("-10");
			$("#conflicttacticsscale-grabed_and_pushed_ur_husband123_desc"+i).val("-10");
			$("#conflicttacticsscale-slapped_ur_husband123_desc"+i).val("-10");
			$("#conflicttacticsscale-forced_ur_partner_for_sex123_desc"+i).val("-10");
			$("#conflicttacticsscale-shaked_ur_partner_by_holding_tight123_desc"+i).val("-10");
			$("#conflicttacticsscale-tried_to_throw_ur_husband123_desc"+i).val("-10");
			$("#conflicttacticsscale-tried_to_throw_things_on_ur_partner123_desc"+i).val("-10");
			$("#conflicttacticsscale-tried_to_hold_ur_partners_neck123_desc"+i).val("-10");
			$("#conflicttacticsscale-tried_to_hit_kick_bite123_desc"+i).val("-10");
			$("#conflicttacticsscale-tried_to_hit_or_hitted123_desc"+i).val("-10");
			$("#conflicttacticsscale-hitted_ur_partner123_desc"+i).val("-10");
			$("#conflicttacticsscale-feared_ur_partner_by_gun_knife123_desc"+i).val("-10");
			$("#conflicttacticsscale-used_gun_knife123_desc"+i).val("-10");

			 $("select[name=\'ConflictTacticsScale[rotated_ur_partner_hand123]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[grabed_and_pushed_ur_husband123]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[slapped_ur_husband123]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[forced_ur_partner_for_sex123]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[shaked_ur_partner_by_holding_tight123]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[tried_to_throw_ur_husband123]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[tried_to_throw_things_on_ur_partner123]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[tried_to_hold_ur_partners_neck123]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[tried_to_hit_kick_bite123]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[tried_to_hit_or_hitted123]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[hitted_ur_partner123]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[feared_ur_partner_by_gun_knife123]\']").val("-10");
                $("select[name=\'ConflictTacticsScale[used_gun_knife123]\']").val("-10");


				autoSaveFormObj = formObj;


		    }

			});
			$("#test1").click(function(){
				//alert("dd");
				$(".aaa").attr("checked",false);
				$(".aaa").val("-10");
                $("select[name=\'ConflictTacticsScale[rotated_ur_partner_hand]\']").val("-10");
				});

', \yii\web\View::POS_READY);
?>
<script>
var formObj = {url : "conflict-tactics-scale/ajax",formName : "formConflicttacticsscale"};

var calc = function () {
	var totalValue = 0;
	var root = 'conflicttacticsscale';
	var id = '#'+root+'-';
	var clas = '.field-'+root+'-';
	var arr = ['supporting_your_partner_at_difficult_times', 'keeping_your_opinions_with_yourself', 'damaged_things_because_of_angry_on_your_wife', 'after_arguing_with_your_partner_did_u_shown_more_love', 'knowingly_hiding_ur_partners_loved_things', 'did_u_felt_sad_when_things_are_undone', 'started_meeting_to_talk_in_all_angles', 'intentionally_destroying_partners_personal_things', 'disrespecting_ur_partner_infront_of_others', 'listened_to_ur_partners_words_with_concentration', 'locked_ur_partner_from_outside_home', 'said_to_ur_partner_like_u_r_unable_to_study_or_work', 'specified_ur_partners_position', 'stopping_ur_partner_from_talking_with_relatives', 'adjusting_with_different_opinions', 'frequently_asking_did_u_understood_or_not', 'restritcting_ur_partner_by_using_of_mobiles_or_vehicle', 'threatning_ur_partner_for_disconnection_of_relationship', 'by_doing_physical_work_did_u_reliefed', 'tried_ur_relatives_against_ur_partner', 'while_partner_solving_their_problem_did_u_suggested_ideas', 'ordered_or_promised_ur_partner', 'tried_to_solve_problems_together', 'accepted_ur_faults', 'feared_ur_partner', 'treated_ur_partner_as_a_fool', 'given_useful_ideas_to_ur_partner', 'done_preplanned_work_with_angry', 'tried_to_join_others_to_solve_ur_problems', 'disrespected_ur_partner', 'self_realised_what_u_said_to_ur_partner', 'feared_ur_partner_to_throw_things_while_angry', 'said_u_r_not_beautiful_to_partner', 'agreed_urself_for_adjustment', 'after_smoking_did_u_scolded_ur_partner', 'if_ur_partner_disagreed_u_then_did_u_thrown_any_objects', 'asked_sorry_after_arguing', 'forcibly_closing_the_argument', 'agreed_to_ur_partner_wanted_things', 'supporting_your_partner_at_difficult_times1', 'keeping_your_opinions_with_yourself1', 'damaged_things_because_of_angry_on_your_wife1', 'after_arguing_with_your_partner_did_u_shown_more_love1', 'knowingly_hiding_ur_partners_loved_things1', 'did_u_felt_sad_when_things_are_undone1', 'intentionally_destroying_partners_personal_things1', 'disrespecting_ur_partner_infront_of_others1', 'listened_to_ur_partners_words_with_concentration1', 'locked_ur_partner_from_outside_home1', 'said_to_ur_partner_like_u_r_unable_to_study_or_work1', 'specified_ur_partners_position1', 'stopping_ur_partner_from_talking_with_relatives1', 'adjusting_with_different_opinions1', 'frequently_asking_did_u_understood_or_not1', 'restritcting_ur_partner_by_using_of_mobiles_or_vehicle1', 'threatning_ur_partner_for_disconnection_of_relationship1', 'by_doing_physical_work_did_u_reliefed1', 'tried_ur_relatives_against_ur_partner1', 'while_partner_solving_their_problem_did_u_suggested_ideas1', 'ordered_or_promised_ur_partner1', 'tried_to_solve_problems_together1', 'accepted_ur_faults1', 'feared_ur_partner1', 'treated_ur_partner_as_a_fool1', 'given_useful_ideas_to_ur_partner1', 'done_preplanned_work_with_angry1', 'tried_to_join_others_to_solve_ur_problems1', 'disrespected_ur_partner1', 'self_realised_what_u_said_to_ur_partner1', 'feared_ur_partner_to_throw_things_while_angry1', 'said_u_r_not_beautiful_to_partner1', 'agreed_urself_for_adjustment1', 'after_smoking_did_u_scolded_ur_partner1', 'if_ur_partner_disagreed_u_then_did_u_thrown_any_objects1', 'asked_sorry_after_arguing1', 'forcibly_closing_the_argument1', 'agreed_to_ur_partner_wanted_things1', 'rotated_ur_partner_hand', 'grabed_and_pushed_ur_husband', 'slapped_ur_husband', 'forced_ur_partner_for_sex', 'shaked_ur_partner_by_holding_tight', 'tried_to_throw_ur_husband', 'tried_to_throw_things_on_ur_partner', 'tried_to_hold_ur_partners_neck', 'tried_to_hit_kick_bite', 'tried_to_hit_or_hitted', 'hitted_ur_partner', 'feared_ur_partner_by_gun_knife', 'used_gun_knife', 'rotated_ur_partner_hand1', 'grabed_and_pushed_ur_husband1', 'slapped_ur_husband1', 'forced_ur_partner_for_sex1', 'shaked_ur_partner_by_holding_tight1', 'tried_to_throw_ur_husband1', 'tried_to_throw_things_on_ur_partner1', 'tried_to_hold_ur_partners_neck1', 'tried_to_hit_kick_bite1', 'tried_to_hit_or_hitted1', 'hitted_ur_partner1', 'feared_ur_partner_by_gun_knife1', 'used_gun_knife1', 'rotated_ur_partner_hand12', 'grabed_and_pushed_ur_husband12', 'slapped_ur_husband12', 'forced_ur_partner_for_sex12', 'shaked_ur_partner_by_holding_tight12', 'tried_to_throw_ur_husband12', 'tried_to_throw_things_on_ur_partner12', 'tried_to_hold_ur_partners_neck12', 'tried_to_hit_kick_bite12', 'tried_to_hit_or_hitted12', 'hitted_ur_partner12', 'feared_ur_partner_by_gun_knife12', 'used_gun_knife12', 'rotated_ur_partner_hand123', 'grabed_and_pushed_ur_husband123', 'slapped_ur_husband123', 'forced_ur_partner_for_sex123', 'shaked_ur_partner_by_holding_tight123', 'tried_to_throw_ur_husband123', 'tried_to_throw_things_on_ur_partner123', 'tried_to_hold_ur_partners_neck123', 'tried_to_hit_kick_bite123', 'tried_to_hit_or_hitted123', 'hitted_ur_partner123', 'feared_ur_partner_by_gun_knife123', 'used_gun_knife123'];
	
	for (var i in arr) {
		var val = $(clas + arr[i] + ' input[type=radio]:checked').val();
		if (parseInt(val)>=0) {
			totalValue += parseInt(val);
		}
	}
	
	$(id+'score').val(totalValue);
	autoSaveObj = formObj;	
}
var changeThis = function(value,ids){
	value = parseInt(value);
	for(var i=0;i < ids.length;i++)
	{
		console.log(ids[i])
		if(value){

		}
		else
		{

		}
	}
	console.log(value,ids)
}
</script>
<style>
#notapp table{
	height:95px;
}
</style>