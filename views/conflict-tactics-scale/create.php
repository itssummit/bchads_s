<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ConflictTacticsScale */

$this->title = 'conflict-tactics-scale';
$this->params['breadcrumbs'][] = ['label' => 'Conflict Tactics Scales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="conflict-tactics-scale-create">

    <h1>ಮನೋವೈಜ್ಞಾನಿಕ  ನಿಂದನಾತ್ಮಕ /ಕಿರುಕುಳಾತ್ಮಕ ಮಾಪನ</h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
