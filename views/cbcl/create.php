<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Cbcl */

$this->title = ' Preschool Cbcl Checklist';
$this->params['breadcrumbs'][] = ['label' => 'Cbcls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cbcl-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
