<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Assessment;
$keyPair = [-15 => '-15: Child Not Co-Operative For Assessment',
		  -14 => '-14: Not Available For Assessment',
		  -11 => '-11: Inadequate Information',
		  -10 => '-10: Not Applicable',
		  -9 => '-9: Missing',
		  -8 => '-8: Refused',
		  -7 => '-7: Partner Accompanied Missing',
		  -6 => '-6: Family Accompanied Missing',
		  -5 => '-5: Missing:Not Asked',
		  -3 => '-3: No Such Relationship/No Partner'];
/* @var $this yii\web\View */
/* @var $model app\models\Cbcl */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile('@web/js/scales.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$respondentId = Yii::$app->session->get('respondentId');

$motherId = Yii::$app->session->get('motherId');

        $assessmentId = Yii::$app->session->get('assessmentId');
$pModel = Assessment::findOne(['mother_id' => $motherId , 'id' => $assessmentId]);

?>
	
<div id="withBoxShadow" class="cbcl-form">

    <?php $form = ActiveForm::begin(['id'=>'formCbcl']);?>
	
	<table class="na">
		<thead>
			<tr><th>Is This Scale Applicable ?</th></tr>
			<tr><td><?= $form->field($model, 'not_applicable')->dropDownList(['' =>'','YES' => 'Not Appplicable', 'NO' => 'Appplicable'],['class' => 'form-control scaleApplicable'])->label(false); ?></td></tr>
		</thead>
	</table>
	
	<caption><b>ದಯವಿಟ್ಟು ನಿಮ್ಮ ದೃಷ್ಟಿಕೋನದಲ್ಲಿ ನಿಮ್ಮ ಮಗುವಿನ ವರ್ತನೆಯನ್ನು ತೋರಿಸುವ/ಬಿತ್ತರಿಸುವ ಪಟ್ಟಿಯನ್ನು ಪೂರ್ತಿಮಾಡಿ ಅದು ಇತರೆ ಜನರು ಒಪ್ಪದಿರಬಹುದು .ಹಿಂದೆ ಇರುವ ಖಾಲಿ ಜಾಗದಲ್ಲಿ ಇನ್ನೂ ಹೆಚ್ಚಿನ ಪ್ರತಿಕ್ರಿಯೆಗಳನ್ನು ಬರೆಯಬಹುದು .ಎಲ್ಲಾ ಅಂಶಗಳಿಗೂ ಉತ್ತರಿಸಿದ್ದೀರಿ ಎಂದು ಖಚಿತ ಪಡಿಸಿಕೊಳ್ಳಿ 
 <br>ಈ ಕೆಳಗೆ ಮಕ್ಕಳನ್ನು ವರ್ಣಿಸುವ ಹೇಳಿಕೆಗಳ ಪಟ್ಟಿಯನ್ನು ನೀಡಲಾಗಿದೆ .ಪ್ರತಿಯೊಂದು  ಹೇಳಿಕೆಗೂ ಪ್ರಸ್ತುತ ಅಥವಾ ತಿಂಗಳೊಳಗೆ ಹೇಳಿಕೆಯು ತುಂಬ ನಿಜವಾಗಿದ್ದರೆ ಅಥವಾ ಆಗ್ಗಾಗ್ಗೆ ನಿಜವಾಗಿದ್ದರೆ ೨ ಅನ್ನು ಗುರುತು ಹಾಕಿ  .ಯಾವಾಗಲಾದರೂ ಒಮ್ಮೆ ನಿಜವಾಗಿದ್ದರೆ  ೧ ಅನ್ನು ಗುರುತು ಹಾಕಿ . ನಿಮ್ಮ ಮಗುವಿಗೆ ಸಂಬಂಧಪಟ್ಟಂತಹ ಹೇಳಿಕೆಯ ಬಗ್ಗೆ ನಿಮಗೆ ಖಚಿತವಿಲ್ಲದಿದ್ದರೆ  ೦ ಆನು ಗುರುತು ಹಾಕಿ ನಿಮ್ಮ ಮಗುವಿಗೆ ಅನ್ವಯವಾಗದಿದ್ದರೂ ಸಹ ನಿಮಗೆ ಸಾಧ್ಯವಾದಷ್ಟು ಎಲ್ಲ ಹೇಳಿಕೆಗಳಿಗೆ ಉತ್ತರಿಸಿ  </b></caption>
	<table border="1" id="idcbscale">
            <thead>
                <tr>
                    <th></th>
                    <th>ನಿಜವಲ್ಲ </th>
                    <th>ಯಾವಾಗಲಾದರೂ   ಒಮ್ಮೆ ನಿಜ </th>
                    <th>ಹೆಚ್ಚಿನ ಬಾರಿ  ನಿಜ ಅಥವಾ ಆಗಾಗ್ಗೆ ನಿಜ </th>
                    <th>Other Answers</th>
                </tr> 
            </thead>
		<tbody>

			<tr>
			<td> 1)ನೋವು (ವೈದ್ಯಕೀಯಕ್ಕೆ ಸಂಬಂಧಪಡದ ಹಾಗೆ: ಹೊಟ್ಟೆ ನೋವು ಅಥವಾ ತಲೆ ನೋವುಗಳಲ್ಲದೆ)</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'pain')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td><?= $form->field($model, 'pain')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>2) ಅವರ ವಯಸ್ಸಿಗಿಂತ ಅತಿಚಿಕ್ಕವನಾಗೆ ನಡೆದುಕೊಳ್ಳತ್ತಾನಾ/ಳಾ</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'behave_childish')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'behave_childish')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>3)ಹೊಸದನ್ನು ಅಥವಾ ಹೊಸದಾಗಿ ಏನನ್ನಾದರೂ  ಮಾಡಲು ಹೆದರುತ್ತಾನೆ/ಳೆ</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'fear_for_doing_new_things')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'fear_for_doing_new_things')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>4) ಇತರರನ್ನು ಕಣ್ಣಲ್ಲಿ ಕಣ್ಣನಿಟ್ಟು   ನೋಡುವುದರಿಂದ ದೂರವಿರುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'far_eye_contact')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'far_eye_contact')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>			
			
			<tr>
			<td>5) ಅವನು/ಳು ಗಮನಹರಿಸುವುದಿಲ್ಲ. ಹೆಚ್ಚಿನ ಸಮಯದವರೆಗೂ ಒಂದೇ ಕಡೆ ಗಮನವನ್ನು ಕೇಂದ್ರೀಕರಿಸುವುದಿಲ್ಲ    </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'less_concentration')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'less_concentration')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>
			
			<tr>
			<td>6) ಅವನು/ಳು ಸುಮ್ಮನೆ ಕುಳಿತುಕೊಳ್ಳುವುದಿಲ್ಲ,  ಸಮಾಧಾನದಿಂದಿರುವುದಿಲ್ಲ  ಅಥವಾ ಹೆಚ್ಚು ಕ್ರೀಯಾಶೀಲರಾಗಿರುತ್ತಾನಾ/ಳಾ </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'patience_or_active')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'patience_or_active')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>
			
			
			<tr>
			<td>7)ವಸ್ತುಗಳನ್ನು ಅದು ಇದ್ದ ಜಾಗದಿಂದ ಮತ್ತೊಂದು ಕಡೆಗೆ ಬದಲಾಯಿಸಿದರೆ ಸಹಿಸಿಕೊಳ್ಳುವುದಿಲ್ಲ </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'impatience_for_displacing_object')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'impatience_for_displacing_object')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>
			
			
			<tr>
			<td>8)ಕಾಯದಿರುವುದು ಎಲ್ಲವೂ ಈಗಲೇ ಬೇಕು ಎನ್ನುತ್ತಾಳೆ/ನೆ  </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'no_waiting_habit')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'no_waiting_habit')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>
			
			<tr>
			<td>9) ತಿನ್ನಬಾರದಂತಹುಗಳನ್ನು ಅಗಿಯುತ್ತಾನೆ. </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'chewing_non_eatable_things')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'chewing_non_eatable_things')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>10) ದೊಡ್ಡವರನ್ನು ಅವಲಂಬಿಸಿವುದು ಅಥವಾ ತುಂಬಾ ಅವಲಂಬಿತವಾಗಿರುವುದು </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'depending_on_elders')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'depending_on_elders')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>11) ಪದೇ ಪದೇ ಸಹಾಯವನ್ನು ಕೇಳುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'frequently_asks_help')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'frequently_asks_help')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>12) ಮಲ ಬಧ್ಧತೆ  (ಮಲ ವಿಸರ್ಜನೆ ಮಾಡಲು ಆಗುವುದಿಲ್ಲ) ಅಥವಾ ಹೋಗುವುದಿಲ್ಲ (ಚೆನ್ನಾಗಿದ್ದಾಗ) </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'dont_poop')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'dont_poop')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>13) ತು೦ಬ ಅಳುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'cries_more')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'cries_more')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>14) ಪ್ರಾಣಿಗಳಿಗೆ  ಕ್ರೂರ ವಾಗಿರುತ್ತಾನೆ/ಳೆ. </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'cruel_for_animals')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'cruel_for_animals')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>15) ಉದ್ಧಟತನ/ ಮೊಂಡುತನ  ತೋರುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'perseverance')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'perseverance')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>16) ಅವನಿಗೆ/ಳಿಗೆ ಬೇಡಿಕೆಗಳು ಅವಾಗಿಂದಾಗೆ/ತಕ್ಷಣವಾಗಿ ಪೂರೈಸಬೇಕು </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'finish_demands_suddenly')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'finish_demands_suddenly')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>17)ಅವನ/ಅವಳದೇ    ಆದ ವಸ್ತುಗಳನ್ನು ನಾಶ ಮಾಡುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'destroys_his_or_her_belongings')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'destroys_his_or_her_belongings')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>18) ಅವನ/ಅವಳ ಕುಟುಂಬದವರ ಅಥವಾ ಇತರ ಮಕ್ಕಳ ವಸ್ತುಗಳನ್ನು ನಾಶ ಮಾಡುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'destroys_family_or_other_children_things')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'destroys_family_or_other_children_things')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>19) ಅತಿಸಾರ ಅಥವಾ ಭೇದಿ (ಚೆನ್ನಾಗಿದ್ದಾಗ) </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'loose_motion_or_Diarrhea')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'loose_motion_or_Diarrhea')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>20) ಅವಿಧೇಯ/ಹೇಳದ ಮಾತನ್ನು ಕೇಳುವುದಿಲ್ಲ </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'doesnt_work_what_people_say')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'doesnt_work_what_people_say')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>21)ದಿನಚರಿಯಲ್ಲಿ ಯಾವುದೇ ಬದಲಾವಣೆಗಳಾದರೂ ಸಹಿಸಿಕೊಳ್ಳುವುದಿಲ್ಲ/ಬೇಜಾರು ಮಾಡಿಕೊಳ್ಳುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'irritation_for_change_of_regular_daily_plans')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'irritation_for_change_of_regular_daily_plans')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>22) ಒಬ್ಬನೆ/ಳೆ ಮಲಗಲು ಇಷ್ಟಪಡುವುದಿಲ್ಲ </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'doesnt_like_to_sleep_alone')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'doesnt_like_to_sleep_alone')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>23) ಅವನ/ಅವಳೊಂದಿಗೆ ಜನರು ಮಾತನಾಡಿಸಿದರೆ ಪ್ರತಿಕ್ರಿಯಸುವುದಿಲ್ಲ/ಉತ್ತರಿಸುವುದಿಲ್ಲ </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'doesnt_respond_to_people')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'doesnt_respond_to_people')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>24) ಚೆನ್ನಾಗಿ ಊಟ ಮಾಡುವುದಿಲ್ಲ (ವಿವರಿಸಿ) 		
			<?= $form->field($model, 'doesnt_eats_well_explain_det')->textInput()->label(false);?>
			</td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'doesnt_eats_well_explain')->radio(['label' => $i, 'value' => $i,'class' =>'t2'])?>
			
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'doesnt_eats_well_explain')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
		
			<tr>
			<td>25) ಇತರೆ ಮಕ್ಕಳೊಂದಿಗೆ ಬೆರೆಯುವುದಿಲ್ಲ </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'doesnt_be_with_other_children')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<th>
				<?= $form->field($model, 'doesnt_be_with_other_children')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</th></tr>
			
			<tr>
			<td>26) ದೊಡ್ಡವರ ತರಹ ಗಂಭೀರವಾಗಿರುತ್ತಾನೆ/ಳೆ, ಮೋಜು-ಮಸ್ತಿ ಮಾಡುವುದಿಲ್ಲ </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'behaves_like_elders_or_doesnt_enjoy')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'behaves_like_elders_or_doesnt_enjoy')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>27) ತಪ್ಪಾಗಿ ನಡೆದುಕೊಂಡ ನಂತರವೂ ಪಶ್ಚಾತಾಪ ಪಡುವುದಿಲ್ಲ </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'doesnt_feels_bad_after_mischievous_things')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'doesnt_feels_bad_after_mischievous_things')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>28) ಮನೆಯಿಂದ ಹೊರಗಡೆ ಹೋಗಲು ಇಷ್ಟಪಡುವುದಿಲ್ಲ </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'doesnt_like_to_go_outside')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'doesnt_like_to_go_outside')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>29)ಸುಲಭವಾಗಿ ನಿರಾಶಾನಾಗುತ್ತಾನೆ/ಳೆ  </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'easily_depressed')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'easily_depressed')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>30) ಸುಲಭವಾಗಿ ಹೊಟ್ಟೆಕಿಚ್ಚು  ಪಡುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'easily_feels_jealous')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'easily_feels_jealous')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>31) ಊಟ-ತಿಂಡಿಯಲ್ಲದಂತಹುಗಳನ್ನು ತಿನುತ್ತಾನೆ/ಳೆ ಅಥವಾ ಕುಡಿಯುತ್ತಾನೆ/ಳೆ, – ಸಿಹಿತಿಂಡಿಗಳನ್ನು  ಸೇರಿಸಬೇಡಿ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'eat_or_drinks_non_eatable_food')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'eat_or_drinks_non_eatable_food')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>32) ಕೆಲವೊಂದು ಪ್ರಾಣಿಗಳಿಗೆ, ಸನ್ನಿವೇಶಗಳಿಗೆ ಅಥವಾ ಸ್ಥಳಗಳನ್ನು ಕಂಡರೆ ಭಯಪಡುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'fear_for_certain_animals_at_certain_place_or_situation')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<th>
				<?= $form->field($model, 'fear_for_certain_animals_at_certain_place_or_situation')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</th></tr>
			
			<tr>
			<td>33)  ಮನಸ್ಸಿಗೆ ಬೇಗನೆ ನೋವುಮಾಡಿಕೊಳ್ಳುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'suddenly_hurts_his_or_her_feelings')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'suddenly_hurts_his_or_her_feelings')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>34) ಹೆಚ್ಚಾಗಿ ನೋವುಂಟುಮಾಡಿಕೊಳ್ಳುವುದು, ಅಪಘಾತಕ್ಕೆ ತುತ್ತಾಗುವುದು </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'will_have_more_pain_or_accidented')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'will_have_more_pain_or_accidented')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>35) ಹಲವು ಜಗಳಗಳಲ್ಲಿ ತೊಡಗುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'involves_in_many_fights')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'involves_in_many_fights')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>36) ಎಲ್ಲದಕ್ಕೂ ತಲೆಹಾಕುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'interfere_in_every_matter')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'interfere_in_every_matter')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>37) ತಂದೆ ತಾಯಿಯಿಂದ ದೂರವಾದಾಗ ಹೆಚ್ಚಾಗಿ ಬೇಜಾರುಗುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'feels_bad_when_he_or_she_is_away_from_their_parents')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'feels_bad_when_he_or_she_is_away_from_their_parents')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>38) ನಿದ್ರೆ ಮಾಡುವುದಕ್ಕೆ ತೊಂದರೆಯಾಗುತ್ತದೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'disturbance_for_sleeping')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'disturbance_for_sleeping')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>39) ತಲೆ ನೋವುವುದು, (ವೈದ್ಯಕೀಯ ಕಾರಣಗಳಿಲ್ಲದೆ) </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'headache_without_medical_reasons')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'headache_without_medical_reasons')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>40) ಇತರರನ್ನ ಹೊಡೆಯುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'hits_others')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'hits_others')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>41) ಅವನು / ಅವಳು  ಉಸಿರುಗಟ್ಟುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'holds_breath')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'holds_breath')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>42) ಉದ್ದೇಶಪೂರ್ವಕವಲ್ಲದೆ ಪ್ರಾಣಿಗಳನ್ನು ಅಥವಾ ಜನರಿಗೆ ತೊಂದರೆ ಮಾಡುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'disturbing_animals_or_people_without_intention')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'disturbing_animals_or_people_without_intention')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>43) ಸೂಕ್ತ ಕಾರಣವಿಲ್ಲದೆ ಬೇಜಾರಾಗಿರುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'depressed_without_reason')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'depressed_without_reason')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>44) ಕೋಪವಾಗಿರುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'angry')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'angry')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>45) ಅನಾರೋಗ್ಯದಿಂದಿರುತ್ತಾನೆ/ಳೆ. (ವೈದ್ಯಕೀಯ ಕಾರಣವಿಲ್ಲದಿರುವುದು </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'vomitting_sensation_or_unhealthy_without_medical_reason')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'vomitting_sensation_or_unhealthy_without_medical_reason')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>46) ನಿಶಕ್ತಿ(ಹೆದರಿದಂತಿರುವುದು) ದೇಹದ ಭಾಗಗಳು ಅದರುವುದು ಅಥವಾ ಸೆಳೆಯುವುದು (ವಿವರಿಸಿ): 

			<?= $form->field($model, 'energyless_or_shewering_of_body_parts_det')->textInput()->label(false);?>
			</td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'energyless_or_shewering_of_body_parts')->radio(['label' => $i, 'value' => $i,'class'=>'t3'])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'energyless_or_shewering_of_body_parts')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
		
			<tr>
			<td>47) ನಿಶಕ್ತಿ(ಹೆದರಿದಂತಿರುವುದು) ,ಪುಕ್ಕಲು ಸ್ವಭಾವದವನು/ಳು </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'energyless_or_feared_person')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'energyless_or_feared_person')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>			
			<tr>
			<td>48) ಕೆಟ್ಟ ಕನಸು/ದುಃಸ್ವಪ್ನ ಬೀಳುವುದು </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'bad_dreams')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'bad_dreams')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>49) ಅತಿಯಾಗಿ ತಿನ್ನುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'eats_more')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'eats_more')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>50) ಅತಿಯಾಗಿ ಆಯಾಸವಾಗಿರುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'more_tired')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'more_tired')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>51) ಸೂಕ್ತ ಕಾರಣವಿಲ್ಲದೆ ಅಘಾತಪಡುತ್ತಾನೆ/ಳೆ./ತೀವ್ರವಾಗಿ ಭಯಪಡುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'fears_without_reason')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'fears_without_reason')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>52) ಮಲ ವಿಸರ್ಜನೆ ಮಾಡುವಾಗ ನೋವಾಗುವುದು(ಅಳುತ್ತಾನೆ/ಳೆ) (ವೈದ್ಯಕೀಯ ಕಾರಣವಿಲ್ಲದೆ) </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'pain_while_pooping_without_medical_reason')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'pain_while_pooping_without_medical_reason')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>53) ಇದ್ದಕ್ಕಿದಂತೆ ಜನರನ್ನು ದೈಹಿಕವಾಗಿ ಹಲ್ಲೆ ಮಾಡುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'suddenly_attacks_people_physically')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'suddenly_attacks_people_physically')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>54) ಮೂಗು, ಚರ್ಮ ಅಥವಾ ದೇಹದ ಇತರ ಭಾಗಗಳನ್ನು ಕೀಳುತ್ತಾನೆ/ಳೆ.. (ವಿವರಿಸಿ) 
			<?= $form->field($model, 'tears_skin_or_nose_or_other_body_parts_det')->textInput()->label(false);?>
			</td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'tears_skin_or_nose_or_other_body_parts')->radio(['label' => $i, 'value' => $i,'class'=>'t4'])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'tears_skin_or_nose_or_other_body_parts')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>55) ತಮ್ಮ ಲೈಂಗಿಕ ಭಾಗಗಳೊಂದಿಗೆ ಅತಿಯಾಗಿ ಆಟವಾಡುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'plays_more_with_sex_organs')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'plays_more_with_sex_organs')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>56) ಸಹಕರಿಸುವುದಿಲ್ಲ ಅಥವಾ ಯಾವ ಕೆಲಸವನ್ನು ನಾಜೂಕಾಗಿ ಮಾಡುವುದಿಲ್ಲ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'doesnt_co_operate_or_doesnt_finishes_work_neatly')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'doesnt_co_operate_or_doesnt_finishes_work_neatly')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>57) ಕಣ್ಣಿನಲ್ಲಿ ತೊಂದರೆ (ವೈದ್ಯಕೀಯ ಕಾರಣವಿಲ್ಲದೆ) (ವಿವರಿಸಿ) 

			<?= $form->field($model, 'eye_problem_without_medical_reason_det')->textInput()->label(false);?>
			</td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'eye_problem_without_medical_reason')->radio(['label' => $i, 'value' => $i,'class' => 't5'])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'eye_problem_without_medical_reason')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>58) ಶಿಕ್ಷಿಸುವುದರಿಂದ ಕೂಡ ಅವನ/ಳ ವರ್ತನೆಯಲ್ಲಿ ಬದಲಾವಣೆಯಾಗುವುದಿಲ್ಲ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'doesnt_changes_behaviour_even_after_punishment')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'doesnt_changes_behaviour_even_after_punishment')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>59) ಒಂದು ಚಟುವಟಿಕೆಯಿಂದ ಇನ್ನೊಂದು ಚಟುವಟಿಕೆಗೆ ತಕ್ಷಣವೇ ಬದಲಾಗುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'from_one_activity_to_another_changes_immediately')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'from_one_activity_to_another_changes_immediately')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>60) ತುರಿಕೆ ಅಥವಾ ಇತರೆ ಚರ್ಮದ ತೊಂದರೆಯಾಗಿರುವುದು (ವೈದ್ಯಕೀಯ ಕಾರಣವಿಲ್ಲದೆ) </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'itchyness_or_skin_problems_without_medical_reason')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'itchyness_or_skin_problems_without_medical_reason')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>61) ತಿನ್ನಲು ನಿರಾಕರಿಸುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'disagrees_food')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'disagrees_food')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>62) ಹೆಚ್ಚು ಚಟುವಟಿಕೆಯಿಂದಿರುವ ಕ್ರೀಡೆಗಳನ್ನು ಆಡಲು ನಿರಾಕರಿಸುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'disagrress_playing_more_active_games')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'disagrress_playing_more_active_games')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>63) ಪದೇ ಪದೇ ತಲೆ ಅಥವಾ ದೇಹವನ್ನು ಅಲ್ಲಾಡಿಸುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'frequently_shakes_his_head_or_body_parts')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'frequently_shakes_his_head_or_body_parts')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>64) ರಾತ್ರಿಯಲ್ಲಿ  ಮಲುಗಲು ಹೋಗಲು ನಿರಾಕರಿಸುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'disagrees_to_sleep_at_night')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'disagrees_to_sleep_at_night')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>65) ಟಾಯ್ಲೆಟ್  ತರಬೇತಿ ನಿರಾಕರಿಸುತ್ತಾನೆ (ಮಲ - ಮೂತ್ರ ವಿಸರ್ಜನೆಯ ತರಬೇತಿ) (ವಿವರಿಸಿ) 

			<?= $form->field($model, 'disagrees_toilet_coaching_det')->textInput()->label(false);?>
			</td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'disagrees_toilet_coaching')->radio(['label' => $i, 'value' => $i,'class' => 't6'])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'disagrees_toilet_coaching')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>66) ಅತಿಯಾಗಿ ಕಿರುಚುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'screeches_more')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'screeches_more')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>67) ಪ್ರೀತಿ-ವಾತ್ಸಲ್ಯಕ್ಕೆ ಪ್ರತಿಕ್ರಿಯಸದಿರುವಂತೆ ಕಾಣುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'looks_like_they_doesnt_respond_for_love_and_affection')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'looks_like_they_doesnt_respond_for_love_and_affection')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>68) ಸುಲಭವಾಗಿ ಸಂಕೋಚಪಟ್ಟಿಕೊಳ್ಳುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'feels_shy_easily')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'feels_shy_easily')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>69) ಸ್ವಾರ್ಥಿ ಅಥವಾ ಹಂಚಿಕೊಳ್ಳುವುದಿಲ್ಲ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'selfish_or_doesnt_share')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'selfish_or_doesnt_share')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>70) ಜನರಿಗೆ ಅಷ್ಟಾಗಿ ಪ್ರೀತಿಯನ್ನು ತೋರುವುದಿಲ್ಲ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'doesnt_show_love_for_people')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'doesnt_show_love_for_people')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>71) ಅವನ/ಳ ಸುತ್ತಲಿನ ವಿಷಯಗಳ ಬಗ್ಗೆ ಕಡಿಮೆ ಆಸಕ್ತಿ ತೋರುತ್ತಾನೆ/ಳೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'shows_little_interest_with_surrounding_things')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'shows_little_interest_with_surrounding_things')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>72) ನೋವಾಗುತ್ತಾದೆ ಎಂದು ಗೊತ್ತಿದ್ದರೂ ಭಯಪಡುವುದಿಲ್ಲ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'doesnt_fear_if_he_knows_it_will_pain_also')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'doesnt_fear_if_he_knows_it_will_pain_also')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>73) ಅತಿಯಾಗಿ ನಾಚಿಕೆಪಡುತ್ತಾನೆ ಅಥವಾ ಅಂಜುತ್ತಾನೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'feels_more_shy')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'feels_more_shy')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>74) ದಿನದಲ್ಲಿ ಅಥವಾ ರಾತ್ರಿಯಲಿ ಹೆಚ್ಚಿನ ಮಕ್ಕಳಿಗಿಂತ ಕಡಿಮೆ ನಿದ್ರೆ ಮಾಡುವುದು (ವಿವರಿಸಿ)  

			<?= $form->field($model, 'sleeps_less_in_day_night_when_compared_to_normal_children_det')->textInput()->label(false);?>
			</td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'sleeps_less_in_day_night_when_compared_to_normal_children')->radio(['label' => $i, 'value' => $i,'class' => 't7'])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'sleeps_less_in_day_night_when_compared_to_normal_children')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>75) ಮಲ ಅಥವಾ ಮೂತ್ರದೊಂದಿಗೆ ಆಟವಾಡುತ್ತಾನೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'plays_with_recess_or_poop')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'plays_with_recess_or_poop')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>76) ಮಾತನಾಡಲು ತೊಂದರೆ (ವಿವರಿಸಿ)  

			<?= $form->field($model, 'problem_in_speaking_det')->textInput()->label(false);?>
			</td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'problem_in_speaking')->radio(['label' => $i, 'value' => $i,'class' => 't8'])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'problem_in_speaking')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
		
			<tr>
			<td>77) ಒಂದೆ ಕಡೆ ದಿಟ್ಟಿಸಿ ನೋಡುವುದು ಅಥವಾ ಯಾವುದೋ ಯೋಚನೆಯಲ್ಲಿ ತೊಡಗಿಕೊಂಡಿರುತ್ತಾನೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'staring_continuosly_at_only_side_or_thinking_something_else')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'staring_continuosly_at_only_side_or_thinking_something_else')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>78) ಹೊಟ್ಟೆ ನೋವು ಅಥವಾ ಹಿಡಿದಂತಾಗುತ್ತದೆ ( ವೈದ್ಯಕೀಯ ಕಾರಣಗಳಿಲ್ಲದೆ) </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'stomach_ache_without_medical_reason')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'stomach_ache_without_medical_reason')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>79) ಮನಸ್ಸಿನ ಭಾವನೆಗಳು ಬೇಜಾರು ಮತ್ತು ಉತ್ಸಾಹದ ನಡುವೆ ಬದಲಾಗುತ್ತದೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'change_in_feelings')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'change_in_feelings')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>80) ವಿಚಿತ್ರವಾದ ವರ್ತನೆ (ವಿವರಿಸಿ) 

			<?= $form->field($model, 'different_behaviour_det')->textInput()->label(false);?>
			</td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'different_behaviour')->radio(['label' => $i, 'value' => $i,'class' => 't9'])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'different_behaviour')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td> 81) ಹಠಮಾಡುತ್ತಾನೆ, ಕಿರಿಕಿರಿಯಾಗುವುದು ಅಥವಾ ಸಿಟ್ಟು ಮಾಡಿಕೊಳ್ಳುತ್ತಾನೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'irritating_or_getting_angry')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'irritating_or_getting_angry')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>82) ಭಾವನೆಗಳಲ್ಲಿ ಅಥವಾ ಮನಸ್ಥಿತಿಯಲ್ಲಿ  ಇದ್ದಕ್ಕಿದ್ದಂತೆ  ಬದಲಾಗುತ್ತದೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'sudden_difference_in_behaviour')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'sudden_difference_in_behaviour')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>83) ಅತಿಯಾಗಿ ಮುನಿಸಿಕೊಳ್ಳುತ್ತಾನೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'gets_more_angry')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'gets_more_angry')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>84) ನಿದ್ರೆಯಲ್ಲಿ ಮಾತನಾಡುತ್ತಾನೆ ಅಥವಾ ಅಳುತ್ತಾನೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'speaks_or_cry_while_sleeping')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'speaks_or_cry_while_sleeping')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>85) ರಂಪ ಮಾಡುತ್ತಾನೆ ಅಥವಾ ರಚ್ಚೆ ಹಿಡಿಯುತ್ತಾನೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'fusses_more')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'fusses_more')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>86) ಸ್ವಚ್ಛತೆಯ ಬಗ್ಗೆ ಅತಿಯಾಗಿ ನಿಗವಹಿಸುತ್ತಾನೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'cares_about_neatness')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'cares_about_neatness')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>87) ಅತಿಯಾಗಿ ಭಯಪಡುವುದು ಅಥವಾ ಆಂತಕಪಡುತ್ತಾನೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'fears_more')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'fears_more')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>88) ಅಸಹಕಾರದಿಂದಿರುತ್ತಾನೆ/ ಹೇಳಿದ್ದನು ಕೇಳುವುದಿಲ್ಲ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'feels_helpless_or_doesnt_listen_to_elders_words')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'feels_helpless_or_doesnt_listen_to_elders_words')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>89) ಕಡಿಮೆ ಚಟುವಟಿಕೆ ಮಾಡುತ್ತಾನೆ, ಅತಿ ನಿಧಾನವಾಗಿ ಚಲನವಲನ ಮಾಡುತ್ತಾನೆ ಅಥವಾ ಮಂಕಾಗಿರುತ್ತಾನೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'does_less_activities_or_walks_slowly')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'does_less_activities_or_walks_slowly')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>90) ಸಂತೋಷವಾಗಿಲ್ಲದಿರುವುದು, ದುಃಖ ಅಥವಾ ಖಿನ್ನರಾಗಿರುತ್ತಾನೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'not_happy_or_being_sad')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'not_happy_or_being_sad')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>91) ಮಾಮೂಲಿಗಿಂತ ಜಾಸ್ತಿಯಾಗಿ ಜೋರಾದ ಧ್ವನಿಯಲ್ಲಿ ಮಾತನಾಡುತ್ತಾನೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'speaks_at_high_voice')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'speaks_at_high_voice')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>92) ಹೊಸ ಜನ ಅಥವಾ ಸನ್ನಿವೇಶದಿಂದ ಬೇಜಾರು ಪಟ್ಟುಕೊಳ್ಳುತ್ತಾನೆ (ವಿವರಿಸಿ)
			<?= $form->field($model, 'feels_sad_with_new_people_or_situation_det')->textInput()->label(false);?>
			</td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'feels_sad_with_new_people_or_situation')->radio(['label' => $i, 'value' => $i,'class' => 't10'])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'feels_sad_with_new_people_or_situation')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>93)  ವಾಂತಿ (ವೈದ್ಯಕೀಯ ಕಾರಣಗಳಿಲ್ಲದೆ) </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'vomit')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'vomit')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>94) ರಾತ್ರಿಯಲ್ಲಿ ಪದೇ ಪದೇ ಎದ್ದೇಳುತ್ತಾನೆ  </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'frequently_wakesup_at_night')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'frequently_wakesup_at_night')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>95) ಬಿಟ್ಟು/ ಕೈಬಿಟ್ಟು ಹೋಗುತ್ತಾನೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'walks_without_holding_hand')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'walks_without_holding_hand')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>96) ಅತಿಯಾಗಿ ಅವನನ್ನೇ ಗಮನಿಸಬೇಕು ಎಂದು ಬಯಸುತ್ತಾನೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'wants_to_look_after_him_for_most_of_the_time')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'wants_to_look_after_him_for_most_of_the_time')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>97) ಗೋಳಾಡುತ್ತಿರುತ್ತಾನೆ  </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'murmuring')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'murmuring')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>98) ಹಿಂಜರಿಯುತ್ತಾನೆ, ಇತರರೊಂದಿಗೆ ಬೆರೆಯುವುದಿಲ್ಲ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'feels_shy_or_doesnt_be_with_others')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'feels_shy_or_doesnt_be_with_others')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>99) ಚಿಂತೆ ಮಾಡುತ್ತಾನೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'thinks')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'thinks')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>100) ಈ ಮೇಲೆ ಪಟ್ಟಿ ಮಾಡದಂತಹ ಬೇರೆ ಯಾವುದಾದರೂ ತೊಂದರೆಗಳಲ್ಲಿ ನಿಮ್ಮ ಮಗುವಿನಲ್ಲಿದ್ದರೆ  ದಯವಿಟ್ಟು  ತಿಳಿಸಿ . 

			<?= $form->field($model, 'list_other_problems_if_he_or_she_has_det')->textInput()->label(false);?>
			</td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'list_other_problems_if_he_or_she_has')->radio(['label' => $i, 'value' => $i,'class' => 't11'])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'list_other_problems_if_he_or_she_has')->dropDownList($keyPair,['prompt' => 'Select','class' => 't1'])->label(false)?> 
</td></tr>
			
			<tr>
			<th>ಬೇರೆ ಮಕ್ಕಳೊಂದಿಗೆ ನಿಮ್ಮ ಮಗು  ಹೇಗೆ ಇರುತ್ತಾನೆ ಎಂದು ತಿಳಿಸಿ. </td>
			</tr>
			
			<tr>
			<td>101)ಇತರೆ ಮಕ್ಕಳನ್ನು ಕಚ್ಚುತ್ತಾನೆ</td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'bites_other_children')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
			<?= $form->field($model, 'bites_other_children')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>102) ಇತರೆ ಮಕ್ಕಳನ್ನು ಒದೆಯುತ್ತಾನೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'kicks_other_children')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'kicks_other_children')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>103) ಇತರೆ ಮಕ್ಕಳಿಗೆ ಸಹಾಯ ಮಾಡುತ್ತಾನೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'helps_other_children')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'helps_other_children')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>104) ಇತರೆ ಮಕ್ಕಳನ್ನು ಹೊಡೆಯುತ್ತಾನೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'beats_other_children')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'beats_other_children')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>105) ಇತರೆ ಮಕ್ಕಳೊಂದಿಗೆ ಹಂಚಿಕೊಳ್ಳುತ್ತಾನೆ </td>
			<?php for($i = 0;$i < 3;$i++):?><td><?= $form->field($model, 'share_with_other_children')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'share_with_other_children')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td>106) ಇತರೆ ಮಕ್ಕಳನ್ನು ನೋಡಿ ನಗುತ್ತಾನೆ </td>
			<?php for($i = 0;$i < 3;$i++):?>
			<td><?= $form->field($model, 'laughs_at_other_children')->radio(['label' => $i, 'value' => $i])?>
			</td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'laughs_at_other_children')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
</td></tr>
			
			<tr>
			<td colspan="2">107)ನಿಮ್ಮ ಮಗುವು ಯಾವುದಾದರು ತೊಂದರೆ ಅಥವಾ ವಿಶೇಷಚೇತನವಾಗಿರುವುದು (ದೈಹಿಕವಾಗಿ ಅಥವಾ ಮಾನಸಿಕವಾಗಿ )<br>
			ಹೌದು ಎಂದಾದಲ್ಲಿ 
			<?= $form->field($model, 'visheshachethana_det')->textInput()->label(false);?>
			</td>
			
			<td><?= $form->field($model, 'visheshachethana')->radio(['label' => 'ಇಲ್ಲ ', 'value' => 0])?></td>
			<td><?= $form->field($model, 'visheshachethana')->radio(['label' => 'ಹೌದು ', 'value' => 1])?></td>
			</tr>
			<tr>
			<td>
			</td>
			
			</tr>
			
			<tr>
			<td colspan="5">108)ನಿಮ್ಮ ಮಗುವಿನ ಬಗ್ಗೆ ನಿಮಗಿರುವ ಅತಿಯಾದ ಖಾಳಜಿ ಏನು <br>
			<?= $form->field($model, 'athiyada_kalaji')->textInput()->label(false);?>
			</td>
			</tr>
			
			<tr>
                <td></td>
                <td id="scaleflag" colspan="4" class="hidden"><?= $form->field($model, 'score')->input('number',['readonly'=>true])->label(false);?></td>
            </tr>
			
		</tbody>
		
	</table>
	
	
    

    <?php ActiveForm::end(); ?>
</div>

<?php $this->registerJs('
	$("#"+formObj.formName).one("change",function()
	{
		autoSaveObj = formObj;
	});
',\yii\web\View::POS_READY)?>

<?php if($model->not_applicable == 'YES'){
	$this->registerJs('
	setNotApplicableOnLoad("#withBoxShadow","#cbcl-not_applicable");
	', \yii\web\View::POS_READY);

}
	$this->registerJs('

	$("#cbcl-not_applicable").change(function()
		{	
			var value = $(this).val();
			if(value=="YES")
			{	
				setNotApplicableYes("#withBoxShadow","#cbcl-not_applicable");
			}
			else
			{
				setNotApplicableNo("#withBoxShadow","#cbcl-not_applicable");
			}
		});

		$("#cbcl-visheshachethana").change(function(){
			$("#cbcl-visheshachethana_det").val("-10");
			});


			$(".t2").change(function(){
				console.log("calling");
				if($(this).val() == 1 || $(this).val() == 2){
					console.log("entered in if loop");
					$("#cbcl-doesnt_eats_well_explain_det").val("");
				}
				else if($(this).val() == 0)
				{
					$("#cbcl-doesnt_eats_well_explain_det").val("-10");
				}
				});

				$(".t3").change(function(){
				console.log("calling");
				if($(this).val() == 1 || $(this).val() == 2){
					console.log("entered in if loop");
					$("#cbcl-energyless_or_shewering_of_body_parts_det").val("");
				}
				else if($(this).val() == 0)
				{
					$("#cbcl-energyless_or_shewering_of_body_parts_det").val("-10");
				}
				});
			
			$(".t4").change(function(){
				console.log("calling");
				if($(this).val() == 1 || $(this).val() == 2){
					console.log("entered in if loop");
					$("#cbcl-tears_skin_or_nose_or_other_body_parts_det").val("");
				}
				else if($(this).val() == 0)
				{
					$("#cbcl-tears_skin_or_nose_or_other_body_parts_det").val("-10");
				}
				});

				$(".t5").change(function(){
				console.log("calling");
				if($(this).val() == 1 || $(this).val() == 2){
					console.log("entered in if loop");
					$("#cbcl-eye_problem_without_medical_reason_det").val("");
				}
				else if($(this).val() == 0)
				{
					$("#cbcl-eye_problem_without_medical_reason_det").val("-10");
				}
				});

				$(".t6").change(function(){
				console.log("calling");
				if($(this).val() == 1 || $(this).val() == 2){
					console.log("entered in if loop");
					$("#cbcl-disagrees_toilet_coaching_det").val("");
				}
				else if($(this).val() == 0)
				{
					$("#cbcl-disagrees_toilet_coaching_det").val("-10");
				}
				});

				$(".t7").change(function(){
				console.log("calling");
				if($(this).val() == 1 || $(this).val() == 2){
					console.log("entered in if loop");
					$("#cbcl-sleeps_less_in_day_night_when_compared_to_normal_children_det").val("");
				}
				else if($(this).val() == 0)
				{
					$("#cbcl-sleeps_less_in_day_night_when_compared_to_normal_children_det").val("-10");
				}
				});

				$(".t8").change(function(){
				console.log("calling");
				if($(this).val() == 1 || $(this).val() == 2){
					console.log("entered in if loop");
					$("#cbcl-problem_in_speaking_det").val("");
				}
				else if($(this).val() == 0)
				{
					$("#cbcl-problem_in_speaking_det").val("-10");
				}
				});

				$(".t9").change(function(){
				console.log("calling");
				if($(this).val() == 1 || $(this).val() == 2){
					console.log("entered in if loop");
					$("#cbcl-different_behaviour_det").val("");
				}
				else if($(this).val() == 0)
				{
					$("#cbcl-different_behaviour_det").val("-10");
				}
				});

				$(".t10").change(function(){
				console.log("calling");
				if($(this).val() == 1 || $(this).val() == 2){
					console.log("entered in if loop");
					$("#cbcl-feels_sad_with_new_people_or_situation_det").val("");
				}
				else if($(this).val() == 0)
				{
					$("#cbcl-feels_sad_with_new_people_or_situation_det").val("-10");
				}
				});

				$(".t11").change(function(){
				console.log("calling");
				if($(this).val() == 1 || $(this).val() == 2){
					console.log("entered in if loop");
					$("#cbcl-list_other_problems_if_he_or_she_has_det").val("");
				}
				else if($(this).val() == 0)
				{
					$("#cbcl-list_other_problems_if_he_or_she_has_det").val("-10");
				}
				});
', \yii\web\View::POS_READY);
?>

<?php if($pModel->is_there_acg == 0 && $pModel->is_there_acg != null && $respondentId == 3){
	$this->registerJs('
	$(":input").val("-3");
		autoSaveFormObj = formObj;
		console.log("There is no ACG to enter");
	', \yii\web\View::POS_READY);

}?>

<script type="text/javascript">
	formObj = {'url' : "cbcl/ajax",'formName' : "formCbcl"};
    var calc = function () {
        var totalValue = 0;
		var root = 'cbcl';
        var id = '#'+root+'-';
		var clas = '.field-'+root+'-';
        var arr = ['pain', 'behave_childish', 'fear_for_doing_new_things', 'far_eye_contact', 
		'less_concentration', 'patience_or_active', 'impatience_for_displacing_object', 'no_waiting_habit', 
		'chewing_non_eatable_things', 'depending_on_elders', 'frequently_asks_help', 'dont_poop', 'cries_more', 
		'cruel_for_animals', 'perseverance', 'finish_demands_suddenly', 'destroys_his_or_her_belongings', 
		'destroys_family_or_other_children_things', 'loose_motion_or_Diarrhea', 'doesnt_work_what_people_say', 
		'irritation_for_change_of_regular_daily_plans', 'doesnt_like_to_sleep_alone', 'doesnt_respond_to_people',
		'doesnt_be_with_other_children', 'behaves_like_elders_or_doesnt_enjoy', 'doesnt_feels_bad_after_mischievous_things',
		'doesnt_like_to_go_outside', 'easily_depressed', 'easily_feels_jealous', 'eat_or_drinks_non_eatable_food', 
		'fear_for_certain_animals_at_certain_place_or_situation', 'suddenly_hurts_his_or_her_feelings',
		'will_have_more_pain_or_accidented', 'involves_in_many_fights', 'interfere_in_every_matter', 
		'feels_bad_when_he_or_she_is_away_from_their_parents', 'disturbance_for_sleeping', 
		'headache_without_medical_reasons', 'hits_others', 'holds_breath', 'disturbing_animals_or_people_without_intention', 
		'depressed_without_reason', 'angry', 'vomitting_sensation_or_unhealthy_without_medical_reason',
		'energyless_or_shewering_of_body_parts','energyless_or_feared_person', 'bad_dreams', 'eats_more', 
		'more_tired', 'fears_without_reason', 'pain_while_pooping_without_medical_reason', 'suddenly_attacks_people_physically',
		'tears_skin_or_nose_or_other_body_parts', 'plays_more_with_sex_organs', 'doesnt_co_operate_or_doesnt_finishes_work_neatly',
		'eye_problem_without_medical_reason', 'doesnt_changes_behaviour_even_after_punishment', 
		'from_one_activity_to_another_changes_immediately', 'itchyness_or_skin_problems_without_medical_reason',
		'disagrees_food', 'disagrress_playing_more_active_games', 'frequently_shakes_his_head_or_body_parts', 
		'disagrees_to_sleep_at_night', 'disagrees_toilet_coaching', 'screeches_more',
		'looks_like_they_doesnt_respond_for_love_and_affection', 'feels_shy_easily', 'selfish_or_doesnt_share', 
		'doesnt_show_love_for_people', 'shows_little_interest_with_surrounding_things', 
		'doesnt_fear_if_he_knows_it_will_pain_also', 'feels_more_shy', 'sleeps_less_in_day_night_when_compared_to_normal_children',
		'plays_with_recess_or_poop', 'problem_in_speaking', 'staring_continuosly_at_only_side_or_thinking_something_else',
		'stomach_ache_without_medical_reason', 'change_in_feelings', 'irritating_or_getting_angry', 
		'sudden_difference_in_behaviour', 'gets_more_angry', 'speaks_or_cry_while_sleeping', 'fusses_more', 
		'cares_about_neatness', 'fears_more', 'feels_helpless_or_doesnt_listen_to_elders_words', 
		'does_less_activities_or_walks_slowly', 'not_happy_or_being_sad', 'speaks_at_high_voice', 'vomit', 
		'frequently_wakesup_at_night', 'walks_without_holding_hand', 'wants_to_look_after_him_for_most_of_the_time',
		'murmuring', 'feels_shy_or_doesnt_be_with_others', 'thinks', 'list_other_problems_if_he_or_she_has', 'bites_other_children', 
		'kicks_other_children', 'helps_other_children', 'beats_other_children', 'share_with_other_children', 'laughs_at_other_children'];

        for (var i in arr) {
            var val = $(clas + arr[i] + ' input[type=radio]:checked').val();
			if (parseInt(val)>=0) {
                totalValue += parseInt(val);
            }
        }
		
        $(id+'score').val(totalValue);
		autoSaveObj = formObj;
    }
</script>

<style>
.na{
	height:90px !important;
}
</style>