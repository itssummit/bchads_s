<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\Url;
use yii\web\View;
$this->registerJsFile('@web/jsControllers/motherController.js',['depends' => [\app\assets\AppAsset::className()]]);
if($model->assessment_date)
{
	$model->assessment_date = date('d-M-Y',strtotime($model->assessment_date));
}
if($model->assessment_date_ac)
{
	$model->assessment_date_ac = date('d-M-Y',strtotime($model->assessment_date_ac));
}
$caregiver =[1=>'Father',
            2=>'Maternal Grandmother',
            3=>'Maternal Grandfather',
            4=>'Paternal Grandmother',
            5=>'Paternal Grandfather',
            6=>'Aunt',
            7=>'Uncle',
            8=>'Other'];	
/* @var $this yii\web\View */
/* @var $model app\models\Assessment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="assessment-form">

    <?php $form = ActiveForm::begin(['id'=>'twelvethAssessmentForm']); ?>

    
	
	<div class="row">
		<div class="col-lg-6" style="display:none;">
			<?= $form->field($model, 'assessment_type')->textInput(['maxlength' => true]) ?>
		</div>
	
		<div class="col-lg-6">
		<?= $form->field($model, 'assessment_date')->widget(DatePicker::className(),['type' => DatePicker::TYPE_COMPONENT_APPEND,'removeButton' => false,'pluginOptions'=>['endDate' => '+0d','format' => 'dd-M-yyyy','orientation' => 'left'], 'options'=>['placeHolder' => 'Assessment date','id' => '12m']]); ?>
		</div>
		<div class="col-lg-6">
			<?= $form->field($model, 'place_of_assesment')->dropDownList([''=>'','Home' => 'Home','NIMHANS' => 'NIMHANS' ,'NCWB'=>'NCWB','Anganavaadi'=>'Anganavaadi' ]); ?>
		</div>
	</div>		
	<div class="row">
		
		<div class="col-lg-6">
			<?= $form->field($model, 'assessment_by')->textInput(['maxlength' => true]) ?>
		</div>
		
		
	</div>
	<div class="row">
		<div class="col-lg-6">
			
			<?= $form->field($model, 'is_there_acg')->dropDownList([''=>'','1' => 'YES','0' => 'NO']) ?>
		</div>
	</div>
<div id ="depend2" style = "display: <?php echo($model->is_there_acg == '1')?"display":"none"; ?> ">	
	<div class="row">
		<!--<div class="col-lg-6">
			< ?=  $form->field($model, 'caregiver')->textInput(['maxlength' => true]) ?>
		</div>-->
		<div class="col-lg-6">
			<?= $form->field($model, 'relation_with_child')->dropDownList($caregiver,['prompt' => 'Select']) ?>
		</div>
         <div  class="col-lg-6" id="other_12" style = "display:<?php echo ($model->relation_with_child == 8)?"display":"none";?>">
            <?= $form->field($model, 'specify_other')->textInput(['maxlength' => true])?>
        </div>
	</div>
	<div class="row">
		<div class="col-lg-6">
		<label>Assessment Date(CareGiver)</label>
		<?= $form->field($model, 'assessment_date_ac')->widget(DatePicker::className(),['type' => DatePicker::TYPE_COMPONENT_APPEND,'removeButton' => false,'pluginOptions'=>['endDate' => '+0d','format' => 'dd-M-yyyy','orientation' => 'left'], 'options'=>['placeHolder' => 'Assessment date(ACG)','id' => '12m1']])->label(false); ?>
		</div>
		<div class="col-lg-6">
		<label>Place Of Assessment(CareGiver)</label>
			<?= $form->field($model, 'place_of_assessment_ac')->dropDownList([''=>'','Home' => 'Home','NIMHANS' => 'NIMHANS' ,'NCWB'=>'NCWB','Anganavaadi'=>'Anganavaadi' ])->label(false); ?>
		</div>
	</div>	
	<div class="row">
		<div class="col-lg-6">
			<label>Assessment By(CareGiver)</label>
			<?= $form->field($model, 'assessment_by_ac')->textInput(['maxlength' => true])->label(false); ?>
		</div>
	</div>
</div>	
	
    <?php ActiveForm::end(); ?>
<button type = "button" class = "btn btn-success pull-left" id = "saveTwelveth">Save</button>&nbsp;

<?= Html::a('Scales form', ['assessment/scaleform','aid' => $model->id], ['class'=>'btn btn-warning','id' => 'id12th','style' => ($model->id)?'display:initial':'display:none']); ?>
</div>
<?php $this->registerJs("
		
	
	$('#saveTwelveth').click(function(){
		autoSaveForm({url : 'twelveth-month/ajax&id=".$model->id."&motherId=".$model->mother_id."',formName : 'twelvethAssessmentForm'},'id12th');
	});
	
	$('#twelvethAssessmentForm #assessment-is_there_acg').change(function(){
		if($(this).val() == '1')
		{
			
			$('#depend2').show();
		}
		else
		{
			$('#depend2').hide();
		}
	});
      $('#twelvethAssessmentForm #assessment-relation_with_child').change(function () {
        if ($(this).val() == 8) {
            $('#other_12').show();
        }
        else{
             $('#other_12').hide();
            }
        });
		
",\yii\web\View::POS_READY)?>