﻿<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$keyPair = [-15 => '-15: Child Not Co-Operative For Assessment',
          -14 => '-14: Not Available For Assessment',
          -11 => '-11: Inadequate Information',
          -10 => '-10: Not Applicable',
          -9 => '-9: Missing',
          -8 => '-8: Refused',
          -7 => '-7: Partner Accompanied Missing',
          -6 => '-6: Family Accompanied Missing',
          -5 => '-5: Missing:Not Asked',
          -3 => '-3: No Such Relationship/No Partner'];

/* @var $this yii\web\View */
/* @var $model app\models\PsychosocialAssessment */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile('@web/js/scales.js',['depends' => [\yii\web\JqueryAsset::className()]]);

?>
<b>

<h4> 
ಒತ್ತಡದ  ಮಾಪನ. </h4>
<h4>
ಪ್ರಸವ- ಪೂರ್ವ  ಮನೋಸಾಮಾಜಿಕ  ಪರಿಚಯ </h4>
<h4>
ಈ ಕೆಳಗಿನ ಅಂಶಗಳು  ಪ್ರಸ್ತುತ  ಎಷ್ಟರ ಮಟ್ಟಿಗೆ ಒತ್ತಡಕಾರಕಗಳಾಗಿವೆ  ಎಂದು ಮಹಿಳೆಯನ್ನು ಕೇಳಿ . </h4>
<h4>ಸೂಕ್ತ ಪ್ರತಿಕ್ರಿಯೆಗೆ ಅನುಗುಣವಾದ ಸಂಖ್ಯೆಗೆ ಸುತ್ತು ಹಾಕಿ. </h4>
</h3> </b>
<b>From past 1 month</b>
<div class="psychosocial-assessment-form">

    <?php $form = ActiveForm::begin(['id' => 'formPsychosocialassessment']); ?>

    <div id="withBoxShadow" class="col-xs-12 col-md-12 col-lg-12 col-sm-12">
    
        <table border="1" class="responsive-scales">
            <thead>
                <tr>
                    <th>ಕ್ರ .ಸಂ </th>
                    <th> ನಿಮಗೆ ಎಷ್ಟರ ಮಟ್ಟಿಗೆ ಪ್ರಸ್ತುತ  ಒತ್ತಡ  \  ತೊಂದರೆ ಅಥವಾ ಸಮಸ್ಯೆಗಳಿವೆ ? (ಆಯ್ಕೆಗಳನ್ನು ಓದಿ )</th>
                    <th> ಒತ್ತಡ ಇಲ್ಲ </th>
                    <th> ಕೆಲವು ಒತ್ತಡ</th>
                    <th>  ಸಾಧಾರಣವಾದ  ಒತ್ತಡ </th>
                    <th>  ತೀವ್ರ ಒತ್ತಡ</th>
                    <th>Other Answers</th>
                 </tr> 
            </thead>
            <tbody>
                <tr>
                <td>B18A.</td>
                <td> ಹಣಕಾಸಿನ ಚಿಂತೆಗಳು (ಉದಾ :ಆಹಾರ,ವಸತಿ , ಆರೋಗ್ಯದ ಆರೈಕೆ ;ವಾಹನ ಸೌಕರ್ಯ )</td>
                <?php for($i = 1;$i < 5;$i++):?>
                <td><?= $form->field($model, 'financial_worries')->radio(['label' => $i, 'value' => $i])?>
                </td>
                <?php endfor;?>
                <td>
                    <?= $form->field($model, 'financial_worries')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
                </td>
                </tr>
    
                <tr>
                <td>B18B.</td>
                <td> ಇತರೆ ಹಣಕಾಸಿನ ಚಿಂತೆಗಳು (ಉದಾ :  ಬಿಲ್ಲುಗಳು ಇತರೆ )</td>
                <?php for($i = 1;$i < 5;$i++):?>
                <td><?= $form->field($model, 'money_worries')->radio(['label' => $i, 'value' => $i])?>
                </td>
                <?php endfor;?>
                <td>
                    <?= $form->field($model, 'money_worries')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
                </td>
                </tr>
                
                <tr>
                <td>B18C.</td>
                <td> ಕುಟುಂಬಕ್ಕೆ ಸಂಭಂದ ಪಟ್ಟಂತಹ ಸಮಸ್ಯೆಗಳು (ಸಂಗಾತಿ ,ಮಕ್ಕಳು  ಇತರೆ ) </td>
                <?php for($i = 1;$i < 5;$i++):?>
                <td><?= $form->field($model, 'family_problems')->radio(['label' => $i, 'value' => $i])?>
                </td>
                <?php endfor;?>
                <td>
                    <?= $form->field($model, 'family_problems')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
                </td>
                </tr>

                <tr>
                <td>B18D.</td>
                <td> ಸ್ಥಳ ಬದಲಾವಣೆಯಾಗಿರುವುದು  ಇತ್ತೀಚೆಗೆ  ಇಲ್ಲವೇ ಮುಂದಿನ ದಿನಗಳಲ್ಲಿ(ಭವಿಷ್ಯತ್)  </td>
                <?php for($i = 1;$i < 5;$i++):?>
                <td><?= $form->field($model, 'move_recently_future')->radio(['label' => $i, 'value' => $i])?>
                </td>
                <?php endfor;?>
                <td>
                    <?= $form->field($model, 'move_recently_future')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
                </td>
                </tr>

                <tr>
                <td>B18E.</td>
                <td> ಇತ್ತೀಚಿಗೆ  ಪ್ರೀತಿಪಾತ್ರರಾದವರನ್ನು  ಕಳೆದುಕೊಂಡಿರುವುದು </td>
                <?php for($i = 1;$i < 5;$i++):?>
                <td><?= $form->field($model, 'loss_loved')->radio(['label' => $i, 'value' => $i])?>
                </td>
                <?php endfor;?>
                <td>
                    <?= $form->field($model, 'loss_loved')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
                </td>
                </tr>

                <tr>
                <td>B18F.</td>
                <td> ಮಗು ಆಗಿರುವುದರಿಂದ ತೊಂದರೆ </td>
                <?php for($i = 1;$i < 5;$i++):?>
                <td><?= $form->field($model, 'current_pregnancy')->radio(['label' => $i, 'value' => $i])?>
                </td>
                <?php endfor;?>
                <td>
                    <?= $form->field($model, 'current_pregnancy')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
                </td>
                </tr>
                
                <tr>
                <td>B18G.</td>
                <td> ಪ್ರಸ್ತುತ ಲೈಂಗಿಕ ಭಾವನಾತ್ಮಕ ಅಥವಾ ದೈಹಿಕವಾಗಿ ದೌರ್ಜನ್ಯವಾಗುತ್ತಿರುವುದು</td>
                <?php for($i = 1;$i < 5;$i++):?>
                <td><?= $form->field($model, 'current_abuse')->radio(['label' => $i, 'value' => $i])?>
                </td>
                <?php endfor;?>
                <td>
                    <?= $form->field($model, 'current_abuse')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
                </td>
                </tr>
                
                <tr>
                <td>B18H.</td>
                <td> ಮದ್ಯ  ದಿಂದ ಅಥವಾ ಮಾದಕ ವಸ್ತುಗಳಿಂದ ತೊಂದರೆಯಾಗಿರುವುದು</td>
                <?php for($i = 1;$i < 5;$i++):?>
                <td><?= $form->field($model, 'problem_alcohol')->radio(['label' => $i, 'value' => $i])?>
                </td>
                <?php endfor;?>
                <td>
                    <?= $form->field($model, 'problem_alcohol')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
                </td>
                </tr>
                
                <tr>
                <td>B18I.</td>
                <td> ಕೆಲಸದಲ್ಲಿ ತೊಂದರೆ (ಉದಾ : ಕೆಲಸದಿಂದ ವಜಾಗೊಳಿಸಿರುವುದು /ತೆಗೆದುಹಾಕಿರುವುದು  ಇತ್ಯಾದಿ )</td>
                <?php for($i = 1;$i < 5;$i++):?>
                <td><?= $form->field($model, 'problem_work')->radio(['label' => $i, 'value' => $i])?>
                </td>
                <?php endfor;?>
                <td>
                    <?= $form->field($model, 'problem_work')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
                </td>
                </tr>
                
                <tr>
                <td>B18J.</td>
                <td> ಸ್ನೇಹಿತರಿಗೆ ಸಂಬಂಧಿಸಿದಂತಹ ತೊಂದೆರೆಗಳು  </td>
                <?php for($i = 1;$i < 5;$i++):?>
                <td><?= $form->field($model, 'problem_friends')->radio(['label' => $i, 'value' => $i])?>
                </td>
                <?php endfor;?>
                <td>
                    <?= $form->field($model, 'problem_friends')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
                </td>
                </tr>
                
                <tr>
                <td>B18K.</td>
                <td> ಸಾಮಾನ್ಯವಾಗಿ  "ಹೆಚ್ಚು ಹೊರೆಯಾಗಿರುವ " ಅನುಭವ </td>
                <?php for($i = 1;$i < 5;$i++):?>
                <td><?= $form->field($model, 'overloaded')->radio(['label' => $i, 'value' => $i])?>
                </td>
                <?php endfor;?>
                <td>
                    <?= $form->field($model, 'overloaded')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?>
                </td>
                </tr>
                
                
                <tr class="hidden">
                <td id="scaleflag" colspan="7"> <?= $form->field($model, 'score')->input('number',['readonly'=>true])->label(false);?></td>
                </tr> 
    
    </tbody>
    </table>
    
    
    
   
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php $this->registerJs('
    $("#"+formObj.formName).one("change",function()
    {
        autoSaveObj = formObj;
    });
',\yii\web\View::POS_READY)?>
<script type="text/javascript">
    var formObj = {url:"psychosocial-assessment/ajax",formName : "formPsychosocialassessment"};
     var calc = function () {
        var totalValue = 0;
        var root = 'psychosocialassessment';
        var id = '#'+root+'-';
        var clas = '.field-'+root+'-';
        var arr = ['financial_worries','money_worries','family_problems','move_recently_future','loss_loved','current_pregnancy',
        'current_abuse','problem_alcohol','problem_work','problem_friends','overloaded'];

        for (var i in arr) {
            var val = $(clas + arr[i] + ' input[type=radio]:checked').val();
            if (parseInt(val)>=0) {
                totalValue += parseInt(val);
            }
        }

        $(id + 'score').val(totalValue);
        autoSaveObj = formObj;

    }
</script>
