<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PsychosocialAssessment */

$this->title = 'Psychosocial Assessment';
$this->params['breadcrumbs'][] = ['label' => 'Psychosocial Assessments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="psychosocial-assessment-create">
<center><b>one month</b></center>

    <h1 align="center">ಮನೋಸಾಮಾಜಿಕ ಮಾಪನ ಸಾಧನ </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
