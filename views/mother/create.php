<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Mother */

$this->title = 'Create Mother';
$this->params['breadcrumbs'][] = ['label' => 'Mothers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mother-create">

    <?= $this->render('_form',['id' => $id]) ?>
	

</div>
