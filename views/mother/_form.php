<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\web\View;
use app\models\Mother;

/* @var $this yii\web\View */
/* @var $model app\models\Mother */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile('@web/jsControllers/motherController.js',['depends' => [\app\assets\AppAsset::className()]]);
$this->registerJsFile('@web/jsControllers/accordion.js',['depends' => [\app\assets\AppAsset::className()]]);
?>
<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12"  style="background-color:lightblue;" id="navId">
	<ul class="nav nav-tabs">
	    <li class="active">
            <?php echo Html::a('<img src ="/bchads/web/img/demographic.png" height="35px" width="35px"> <span>DEMOGRAPHIC</span>','#demographic',['data-toggle' => "tab"])?>
	    </li>
	    <li>
	  	<?php echo Html::a('<img src ="/bchads/web/img/obstetric.png" height="35px" width="35px"> <span>OBSTETRIC</span>','#obstetric',['data-toggle' => "tab"])?>
	    </li>
	    <li>
	  	<?php echo Html::a('<img src ="/bchads/web/img/assess.png" height="35px" width="35px"> <span>ASSESSMENT</span>','#assessment',['data-toggle' => "tab"])?>
	    </li>
	</ul>
</div>
<div  ng-controller="MotherController" class="col-md-12 col-lg-12 colsm-12 col-xs-12" ng-init = "<?php if($id){echo "getData($id)";}?>">
    <div class="tab-content">
        <div id="demographic" class="tab-pane fade in active">
            <form name="mainForm"  ng-submit = "save(mainForm,null)">
                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" style="border: 10px ridge white">
                    <h3></h3>
					<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
					<div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
						<label>Mother Code<span class="red" style="color: #f00">*</span></label>
						<div class="form-group" show-errors>
		      	            <input type="text" class="form-control" name="mother_code" ng-model="$root.motherObj.mother_code" ng-disabled = "$root.motherObj.id" ng-change= "$root.checkMotherCode($root.motherObj.mother_code)" required />
		                    <span class="help-block"  ng-if="mainForm.mother_code.$error.required" ></span>
							<span class="help-block" style="color:#d9534f;" ng-show="$root.errorMotherCode" >Mother Code has already taken</span>
						</div>
                        
					</div>
					
					<div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
						<label>Gender(Baby)</label>
						<div class="form-group" show-errors>
		      	            <select class="form-control" name="'gender_baby" ng-model="$root.motherObj.gender_baby" >
							                    <option value="1">Male</option>
							                    <option value="2">Female</option>
                            </select>
						</div>
                    </div>
				<!--	<div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
						
						<label>Age(Baby)-In Months</label>
						<div class="form-group" show-errors>
						
		      	            <input type="text" class="form-control" name="age_baby" ng-model="$root.motherObj.age_baby">
							
						</div>
                        
					</div>-->
					
					</div>
                    <div class="row col-md-12 col-lg-12 col-xs-12 col-sm-12">
                        <div class = "pull-right">
        	                <button type = "submit" class = "btn btn-success doubleclick" id="save">Save</button>
                            <span ng-hide="$root.motherObj.id==null"><a class="btn btn-info btnNext">Next</a></span>
                        </div>
                    </div>
			    </div>
            </form>
		</div>
      
        <div id="obstetric" class="tab-pane fade">
            <form name="secondForm" novalidate  ng-submit = "save(secondForm,null)">
			    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" style="border: 10px ridge white" >
                    <!--<h3>OBSTETRIC</h3>-->
                    <div class="accordion">
                        <div class="accordion-section">
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                <a class="accordion-section-title" href="#accordion-1">A.PRESENT PREGNANCY DETAILS<span class="glyphicon glyphicon-sort pull-right"></span></a>
                                <div id="accordion-1" class="accordion-section-content" >
							     <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                <div class="col-md-3 col-lg-3 col-xs-6 col-sm-6">	
                                    
                                        <label>Date Of Registration</label>
										<?php echo DatePicker::widget(['name' => 'date_of_reg','removeButton' => false,'value'=>null,'options' => ['ng-model' => '$root.motherObj.dateofreg',"data-toggle" => "tooltip"],'pluginOptions'=>['endDate' => '+0d','format' => 'dd-M-yyyy','orientation' => 'left']]); ?>
								
                                    
                                </div>
                                <div class = "col-md-3 col-lg-3 col-xs-6 col-sm-6 col-md-offset-6 col-lg-offset-6">
                                    <label> Place Of Registration</label>
        		                    <div class="form-group" show-errors>
        			                    <input type="text" class="form-control" name="placeofreg" ng-model="$root.motherObj.placeofreg" />
        		                    </div>
                                </div>
                                </div>
                                 <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                <div class = "col-md-3 col-lg-3 col-xs-6 col-sm-6">
                                    
									<label> Current Visit</label>
									<?php echo DatePicker::widget(['name' => 'current_visit','removeButton' => false,'value'=>null,'options' => ['ng-model' => '$root.motherObj.current_visit',"data-toggle" => "tooltip"],'pluginOptions'=>['endDate' => '+0d','format' => 'dd-M-yyyy','orientation' => 'left']]); ?>
								   

									
                                </div>
                                <div class="col-md-3 col-lg-3 col-xs-6 col-sm-6 col-md-offset-6 col-lg-offset-6">	
                                    
                                        <label>Next Appointment (ANC)</label>
										<?php echo DatePicker::widget(['name' => 'next_appointment_ANC','removeButton' => false,'value'=>null,'options' => ['ng-model' => '$root.motherObj.next_appointment_ANC',"data-toggle" => "tooltip"],'pluginOptions'=>['startDate' => '+0d','format' => 'dd-M-yyyy','orientation' => 'left']]); ?>
								   
                                    
                                </div>
                                </div>
                                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                   <div class="col-md-3 col-lg-3 col-xs-6 col-sm-6">	
                                        
                                            <label>EDD</label>
											<?php echo DatePicker::widget(['name' => 'edd','removeButton' => false,'value'=>null,'options' => ['ng-model' => '$root.motherObj.edd',"data-toggle" => "tooltip"],'pluginOptions'=>['endDate' => '+0d','format' => 'dd-M-yyyy','orientation' => 'left']]); ?>
								   

                                    </div>
                                    <div class = "col-md-3 col-lg-3 col-xs-6 col-sm-6">
                                        <label>Gravida</label>
        		                        <div class="form-group" show-errors>
        			                        <input type="text" class="form-control" name="gravida" ng-model="$root.motherObj.gravida" />
        		                        </div>
                                    </div>
                                    <div class = "col-md-3 col-lg-3 col-xs-6 col-sm-6">
                                        <label>Parity</label>
        		                        <div class="form-group" show-errors>
        			                        <input type="text" class="form-control" name="parity" ng-model="$root.motherObj.parity" />
        		                        </div>
                                    </div>
                                    <div class="col-md-3 col-lg-3 col-xs-6 col-sm-6">	
                                        
                                            <label>LMP</label>
											<?php echo DatePicker::widget(['name' => 'lmp','removeButton'=>false,'value'=>null,'options' => ['ng-model' => '$root.motherObj.lmp',"data-toggle" => "tooltip"],'pluginOptions'=>['endDate' => '+0d','format' => 'dd-M-yyyy','orientation' => 'left']]); ?>
								   
                                        
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                    
                                    <div class = "col-md-2 col-lg-2 col-xs-2 col-sm-2">    
                                    <label>TRIMESTERS:</label>
									</div>
								</div>	
								<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                    <div class = "col-md-2 col-lg-2 col-xs-2 col-sm-2">
									<label></label><br>
											<div class="form-group" show-errors>
        			                        <input type="text" class="form-control" name="tri_t1" ng-model="$root.motherObj.tri_t1" placeholder="T1"/>
        		                        </div>
                                    </div>
                                    <div class = "col-md-2 col-lg-2 col-xs-2 col-sm-2">
									<label></label><br>
                                        <div class="form-group" show-errors>
        			                        <input type="text" class="form-control" name="tri_t2" ng-model="$root.motherObj.tri_t2" placeholder="T2"/>
        		                        </div>
                                    </div>
                                    <div class = "col-md-2 col-lg-2 col-xs-2 col-sm-2">
                                        <div class="form-group" show-errors>
										<label></label><br>
        			                        <input type="text" class="form-control" name="tri_t3" ng-model="$root.motherObj.tri_t3" placeholder="T3"/>
        		                        </div>
                                    </div>
                                    <div class = "col-md-2 col-lg-2 col-xs-2 col-sm-2">
                                            <label>Gestation</label><br>
        		                            <div class="form-group" show-errors>
        			                            <input type="text" class="form-control" name="gestation" ng-model="$root.motherObj.gestation" placeholder="Weeks"/>
        		                            </div>
                                    </div>
                                    <div class = "col-md-4 col-lg-4 col-xs-2 col-sm-2">
                                            <label>Paternal Age At Conception</label>
        		                            <div class="form-group" show-errors>
        			                            <input type="text" class="form-control" name="paternal_age_at_conception" ng-model="$root.motherObj.paternal_age_at_conception" placeholder="Years" />
        		                            </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                    <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
                                        <label>Medication Prior Pregnancy</label>
        		                        <div class="form-group" show-errors>
                     			            <label><input type="radio"  name="medication_prior_pregnancy" ng-model="$root.motherObj.medication_prior_pregnancy" value="1" />Yes</label>
                   				            <label><input type="radio"  name="medication_prior_pregnancy" ng-model="$root.motherObj.medication_prior_pregnancy" value="0" />No</label>
        					        </div>
                                    </div>
                                    <div class="col-md-8 col-lg-8 col-sm-8 col-xs-8" ng-if="$root.motherObj.medication_prior_pregnancy==1">
                                        <label>Medication</label>
        		                        <div class="form-group" show-errors>
        			                        <input type="textarea" class="form-control" name="medication" ng-model="$root.motherObj.medication" />
        		                        </div>
                                    </div>
								</div>	
								<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                    <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
                                        <label>Prescribed Medication During Pregnancy</label>
        		                        <div class="form-group" show-errors>
                     			            <label><input type="radio"  name="medication_prior_pregnancy" ng-model="$root.motherObj.prescribed_medication_during_pregnancy" value="1" />Yes</label>
                   				            <label><input type="radio"  name="medication_prior_pregnancy" ng-model="$root.motherObj.prescribed_medication_during_pregnancy" value="0" />No</label>
        					        </div>
                                    </div>
                                    <div class="col-md-8 col-lg-8 col-sm-8 col-xs-8" ng-if="$root.motherObj.prescribed_medication_during_pregnancy==1">
                                        <label>Medication During Pregnancy </label>
        		                        <div class="form-group">
                                            <select class="form-control" name="'medication_during_pregnancy" ng-model="$root.motherObj.medication_during_pregnancy" >
							                    <option value="Nutrients supplements">Nutrients supplements</option>
							                    <option value="Thyroid medication">Thyroid medication</option>
                                                <option value="Diabetic medication">Diabetic medication</option>
                                                <option value="Anti hypertensive">Anti hypertensive</option>
                                                <option value="Others">Others</option>
						                    </select>
                                        </div>
                                   </div>
                                </div>
                                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                  <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
                                        <label>Fertility Treatment Past</label>
        		                        <div class="form-group" show-errors>
                     			            <label><input type="radio"  name="fertility_treatment_past" ng-model="$root.motherObj.fertility_treatment_past" value="1" />Yes</label>
                   				            <label><input type="radio"  name="fertility_treatment_past" ng-model="$root.motherObj.fertility_treatment_past" value="0" />No</label>
        					            </div>
                                    </div>
                                    <div class="col-md-5 col-lg-5 col-sm-5 col-xs-5" ng-if="$root.motherObj.fertility_treatment_past==1">
                                        <label>Fertility Treatment </label>
        		                        <div class="form-group" show-errors>
        			                        <input type="textarea" class="form-control" name="fertility_treatment" ng-model="$root.motherObj.fertility_treatment" />
        		                        </div>
                                   </div>
                                    <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
                                        <label>Planned Pregnancy ?</label>
        		                        <div class="form-group" show-errors>
                     			            <label><input type="radio"  name="planned_pregnancy" ng-model="$root.motherObj.planned_pregnancy" value="1" />Yes</label>
                   				            <label><input type="radio"  name="planned_pregnancy" ng-model="$root.motherObj.planned_pregnancy" value="0" />No</label>
        					            </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                    <div class = "col-md-4 col-lg-4 col-xs-4 col-sm-4">
                                        <label> Self Reaction For Pregnancy</label>
        		                        <div class="form-group">
                                            <select class="form-control" name="self_reaction_for_pregnancy" ng-model="$root.motherObj.self_reaction_for_pregnancy" >
	        		        	                <option value="Wanted">Wanted</option>
								                <option value="Unwanted">Unwanted</option>
							                </select>
                                        </div>
                                    </div>
                                    <div class = "col-md-4 col-lg-4 col-xs-4 col-sm-4">
                                        <label> Husband Reaction For Pregnancy</label>
        		                        <div class="form-group">
                                            <select class="form-control" name="husband_reaction_for_pregnancy" ng-model="$root.motherObj.husband_reaction_for_pregnancy" >
	        		        	                <option value="Wanted">Wanted</option>
								                <option value="Unwanted">Unwanted</option>
							                </select>
                                        </div>
                                    </div>
                                    <div class = "col-md-4 col-lg-4 col-xs-4 col-sm-4">
                                        <label> Family Reaction For Pregnancy</label>
        		                        <div class="form-group">
                                            <select class="form-control" name="family_reaction_for_pregnancy" ng-model="$root.motherObj.family_reaction_for_pregnancy" >
	        		        	                <option value="Wanted">Wanted</option>
								                <option value="Unwanted">Unwanted</option>
							                </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                    <div class = "col-md-4 col-lg-4 col-xs-4 col-sm-4">
                                        <label> Physical Activity</label>
        		                        <div class="form-group">
                                            <select class="form-control" name="physical_activity" ng-model="$root.motherObj.physical_activity" >
	        		        	                <option value="Yes">Yes</option>
								                <option value="No">No</option>
							                </select>
                                        </div>
                                    </div>
                                    <div class = "col-md-4 col-lg-4 col-xs-4 col-sm-4">
                                        <label> Walking Duration</label>
        		                        <div class="form-group">
        			                        <input type="text" class="form-control"  name="walking_duration" ng-model="$root.motherObj.walking_duration" />
        		                        </div>
                                    </div>
                                   <div class = "col-md-4 col-lg-4 col-xs-4 col-sm-4">
                                        <label> Exercise Duration</label>
        		                        <div class="form-group">
        			                        <input type="text" class="form-control"  name="exercise_duration" ng-model="$root.motherObj.exercise_duration" />
        		                        </div>
                                    </div>
                                    
								</div>
								<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
									<div class = "col-md-4 col-lg-4 col-xs-4 col-sm-4">
                                        <label> Yoga Duration</label>
        		                        <div class="form-group" show-errors>
        			                        <input type="text" class="form-control"  name="yoga_duration" ng-model="$root.motherObj.yoga_duration" />
        		                        </div>
                                    </div>
                                    <div class = "col-md-4 col-lg-4 col-xs-4 col-sm-4">
                                        <label>Household Chores Duration</label>
        		                        <div class="form-group" show-errors>
        			                        <input type="text" class="form-control"  name="householdchores_duration" ng-model="$root.motherObj.householdchores_duration"/>
        		                        </div>
                                    </div>
                                    <div class = "col-md-4 col-lg-4 col-xs-4 col-sm-4">
                                        <label> Others Duration</label>
        		                        <div class="form-group" show-errors>
        			                        <input type="text" class="form-control"  name="others_duration" ng-model="$root.motherObj.others_duration" />
        		                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                    <div class = "col-md-3 col-lg-3 col-xs-6 col-sm-6">
        				                <label>Spouse Substance Use?</label>
        				                <div class="form-group" show-errors>
                    		                <label><input type="radio"  name="spouse_substance_use" ng-model="$root.motherObj.spouse_substance_use" value="1" />Yes</label>
        		    		                <label><input type="radio"  name="spouse_substance_use" ng-model="$root.motherObj.spouse_substance_use" value="0" />No</label>
                		                </div>
                                     </div>
                                    <div class = "col-md-3 col-lg-3 col-xs-6 col-sm-6" ng-if="$root.motherObj.spouse_substance_use==1">
        				                    <label> Substance Type</label>
        			                    <span class="form-group" show-errors ng-if="$root.motherObj.spouse_substance_use==1">
                                            <select class="form-control" name="substance_type" ng-model="$root.motherObj.substance_type" >
	        		        	                    <option value=" ">SELECT</option>
								                    <option value="Alcohol">Alcohol</option>
								                    <option value="Tobacco">Tobacco</option>
                                                    <option value="Others">Others</option>
							                    </select>
                                        </span>
                                        </div>
                                </div>
                               </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
					            <a class="accordion-section-title" href="#accordion-2">B.PAST OBG HISTORY<span class="glyphicon glyphicon-sort pull-right"></span></a>
                                <div id="accordion-2" class="accordion-section-content" >
                                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
						                <div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
						                <label>Delivery</label>
        		                                    <div class="form-group">
                                                        <select class="form-control" name="delivery" ng-model="$root.motherObj.delivery">
	        		        	                            <option value="Yes">Yes</option>
								                            <option value="No">No</option>
							                            </select>
                                                    </div>
						                </div>
						                <div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
						                <label>Year Of Delivery</label>
        		                                    <div class="form-group">
                                                     <input type="text" class="form-control"  name="yearof_delivery" ng-model="$root.motherObj.yearof_delivery" />   
                                                    </div>
						                </div>
						                <div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
						                <label>Gestation Age Of Delivery</label>
        		                                    <div class="form-group">
                                                     <input type="text" class="form-control"  name="gestation_age_of_delivery" ng-model="$root.motherObj.gestation_age_of_delivery" />   
                                                    </div>
						                </div>
						                </div>
						                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
						                <div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
						                <label>Mode Of Delivery</label>
									                <div class="form-group">
                                                        <select class="form-control" name="mode_of_delivery" ng-model="$root.motherObj.mode_of_delivery" >
	        		        	                            <option value="Vaginal">Vaginal</option>
								                            <option value="C - Section">C - Section</option>
											                <option value="Forceps">Forceps</option>
											                <option value="Vacuum">Vacuum</option>
							                            </select>
                                                    </div>
						                </div>
						                <div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
						                <label>Complications During Pregnancy</label>
									                <div class="form-group">
                                                        <select class="form-control" name="complications_during_pregnancy" ng-model="$root.motherObj.complications_during_pregnancy" >
	        		        	                            <option value="Yes">Yes</option>
								                            <option value="No">No</option>
											
							                            </select>
                                                    </div>
						                </div>
						                <div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
						                <label>Complications In Baby</label>
									                <div class="form-group">
                                                        <select class="form-control" name="complications_in_baby" ng-model="$root.motherObj.complications_in_baby" >
	        		        	                            <option value="NICU Care">NICU Care</option>
								                            <option value="Low birth weight">Low birth weight</option>
											                <option value="Still Birth">Still Birth</option>
											                <option value="Pre-term Delivery">Pre-term Delivery</option>
											                <option value="Neonatal Death">Neonatal Death</option>
											                <option value="Infant Death">Infant Death</option>
											                <option value="Others">Others</option>
											                <option value="none">none</option>
											
							                            </select>
                                                    </div>
						                </div>
						            </div>
                                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
						                     <div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
									            <label>Complications In Mother</label>
									            <div class="form-group">
                                                    <select class="form-control" name="complications_in_mother" ng-model="$root.motherObj.complications_in_mother">
	        		        	                        <option value="Anaemia">Anaemia</option>
								                        <option value="Thyroid">Thyroid</option>
											            <option value=" Diabetes "> Diabetes </option>
											            <option value="Hypertension">Hypertension</option>
											            <option value="Abruption of pregnancy">Abruption of pregnancy</option>
											            <option value="Blood Transfusion">Blood Transfusion</option>
											            <option value="Post-Partum Hemorrhage ">Post-Partum Hemorrhage </option>
											            <option value="Infections">Infections</option>
														<option value="Others ">Others </option>
														<option value="none">none</option>
							                        </select>
                                                </div>
						            </div>
						            <div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
						            <label>Risk Identified In Foetus</label>
									            <div class="form-group">
                                                    <select class="form-control" name="risk_identified_in_foetus_past" ng-model="$root.motherObj.risk_identified_in_foetus_past">
	        		        	                        <option value="Congenital abnormalities">Congenital abnormalities</option>
								                        <option value="Growth problems">Growth Problems</option>
											            <option value="Others">Others</option>
											            <option value="none">none</option>
											        </select>
                                                </div>
						            </div>
						            <div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
						            <label>Abortion/ Miscarriege</label>
									            <div class="form-group">
                                                  <select class="form-control" name="abortions_miscarrieges" ng-model="$root.motherObj.abortions_miscarrieges">
	        		        	                        <option value="Yes">Yes</option>
								                        <option value="No">No</option>
													</select>  
                                                </div>
						            </div>
						            </div>
						            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
						            <div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
						            <label>Medical / Surgical Termination Related Complications</label>
									            <div class="form-group">
                                                    <select class="form-control" name="medical_surgical_termination_related_complication" ng-model="$root.motherObj.medical_surgical_termination_related_complication">
	        		        	                        <option value="Yes">Yes</option>
								                        <option value="No">No</option>
													</select>
                                                </div>
						            </div>
						            <div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
						            <label>Contraception</label>
									            <div class="form-group">
                                                    <select class="form-control" name="contraception" ng-model="$root.motherObj.contraception">
	        		        	                        <option value="Yes">Yes</option>
								                        <option value="No">No</option>
											
											
							                        </select>
                                                </div>
						            </div>
						            <div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
									<label>Have you experienced (mood swings, crying spells, irritability, anger, restlessness, feeling sad etc.) in previous pregnancy/post-delivery?</label>
									            <div class="form-group">
                                                    <select class="form-control" name="experience_in_previouspregnancy_postdelivery" ng-model="$root.motherObj.experience_in_previouspregnancy_postdelivery">
	        		        	                        <option value="Yes">Yes</option>
								                        <option value="No">No</option>
											
											
							                        </select>
                                                </div>
						            </div>
						            </div>
								<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
						            <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3">
						            <label>Specify the Experience</label>
        		                                <div class="form-group">
                                                 <input type="text" class="form-control"  name="specify_experience" ng-model="$root.motherObj.specify_experience" />   
                                                </div>
						            </div>
						            <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3">
						            <label>Have you done a PAP test?</label>
									            <div class="form-group">
                                                    <select class="form-control" name="pap_test" ng-model="$root.motherObj.pap_test">
	        		        	                        <option value="Yes">Yes</option>
								                        <option value="No">No</option>
											
											
							                        </select>
                                                </div>
						            </div>
						            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
						            <label>Problems Related To Baby’s Ability To Suck</label>
									            <div class="form-group">
                                                    <select class="form-control" name="baby_ability_to_suck" ng-model="$root.motherObj.baby_ability_to_suck">
	        		        	                        <option value="Yes">Yes</option>
								                        <option value="No">No</option>
											
											
							                        </select>
                                                </div>
						            </div>
								</div>
						            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
						            <div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
						            <label>Problems Related To Production Of Breast Milk</label>
									            <div class="form-group">
                                                    <select class="form-control" name="production_of_breast_milk" ng-model="$root.motherObj.production_of_breast_milk">
	        		        	                        <option value="Yes">Yes</option>
								                        <option value="No">No</option>
											
											
							                        </select>
                                                </div>
						            </div>
						            </div>
						        </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
					            <a class="accordion-section-title" href="#accordion-3">C.FAMILY HISTORY<span class="glyphicon glyphicon-sort pull-right"></span></a>
                                <div id="accordion-3" class="accordion-section-content" >
                                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
						                <div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
						                <label>Congenital Anomalies (Mental/Physical Disability)</label>
									                <div class="form-group">
                                                        <select class="form-control" name="congenital_anomolies" ng-model="$root.motherObj.congenital_anomolies">
	        		        	                            <option value="Yes">Yes</option>
								                            <option value="No">No</option>
							                            </select>
                                                    </div>
						                </div>
						                <div class="col-md-2 col-lg-2 col-xs-2 col-sm-2">
						                <label>Special Children</label>
									                <div class="form-group"><br>
                                                        <select class="form-control" name="special_children" ng-model="$root.motherObj.special_children">
	        		        	                            <option value="Yes">Yes</option>
								                            <option value="No">No</option>
							                            </select>
                                                    </div>
						                </div>
						                <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3">
						                <label>Mental Illness (1<sup>o</sup> or 2<sup>o</sup> relatives)</label>
									                <div class="form-group">
                                                        <select class="form-control" name="mental_illness" ng-model="$root.motherObj.mental_illness">
	        		        	                            <option value="Yes">Yes</option>
								                            <option value="No">No</option>
							                            </select>
                                                    </div>
						                </div>
						                <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3">
						                <label>Postpartum Mental Health Issues</label>
									                <div class="form-group">
                                                        <select class="form-control" name="postpartum_mental_health_issues" ng-model="$root.motherObj.postpartum_mental_health_issues">
	        		        	                            <option value="Yes">Yes</option>
								                            <option value="No">No</option>
											
											
							                            </select>
                                                    </div>
						                </div>
						                </div>



			                </div>
					    </div>
                    </div>
                    </div>
                </div>
                <div class = "col-md-12 col-lg-12 col-xs-12 col-sm-12">
                    <div class="pull-right">
                         <a class="btn btn-info btnPrevious" >Previous</a>
        		        <button type = "submit" class = "btn btn-success">Save</button>
						
                        <a class="btn btn-info btnNext" >Next</a>
                    </div>
        	    </div>
            </form>
		</div>
       
        <div id="assessment" class="tab-pane fade">
			<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" style="border: 10px ridge white">
	    	    <div id = "loadAssessTypes">
					<h3>Please Create Mother</h3>
				</div>
                 <div class="pull-right">
                    <a class="btn btn-info btnPrevious" >Previous</a>
                </div>
			</div>
            
	  </div>
    </div>
</div>
<?php $this->registerJs("

$('.btnNext').click(function(){
  $('.nav-tabs > .active').next('li').find('a').trigger('click');
});

  $('.btnPrevious').click(function(){
  $('.nav-tabs > .active').prev('li').find('a').trigger('click');
});
$('.datepicker').datepicker({
    format: 'dd-mm-yyyy',
});

$('#demographic').submit(function(){
			$('.doubleclick').prop('disabled',true);
		});
		
$('#obstetric').submit(function(){
			$('.doubleclick').prop('disabled',true);
		});
	
",View::POS_READY)?>




	