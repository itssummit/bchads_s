<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MotherSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mother-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'first_name') ?>

    <?= $form->field($model, 'middle_name') ?>

    <?= $form->field($model, 'last_name') ?>

    <?= $form->field($model, 'dob') ?>

    <?php // echo $form->field($model, 'age') ?>

    <?php // echo $form->field($model, 'present_address') ?>

    <?php // echo $form->field($model, 'phone1') ?>

    <?php // echo $form->field($model, 'phone2') ?>

    <?php // echo $form->field($model, 'permanent_address') ?>

    <?php // echo $form->field($model, 'domicile') ?>

    <?php // echo $form->field($model, 'religion') ?>

    <?php // echo $form->field($model, 'educational_qualification') ?>

    <?php // echo $form->field($model, 'occupation_Self') ?>

    <?php // echo $form->field($model, 'occupation_Spouse') ?>

    <?php // echo $form->field($model, 'socio_economic_status') ?>

    <?php // echo $form->field($model, 'marital_status') ?>

    <?php // echo $form->field($model, 'nature_of_marriage') ?>

    <?php // echo $form->field($model, 'family_type') ?>

    <?php // echo $form->field($model, 'no_of_family_members') ?>

    <?php // echo $form->field($model, 'total_family_income') ?>

    <?php // echo $form->field($model, 'spouse_substance_use') ?>

    <?php // echo $form->field($model, 'substance_type') ?>

    <?php // echo $form->field($model, 'duration_of_residence') ?>

    <?php // echo $form->field($model, 'no_of_years_of_marriage_years') ?>

    <?php // echo $form->field($model, 'no_of_years_of_marriage_months') ?>

    <?php // echo $form->field($model, 'dateofreg') ?>

    <?php // echo $form->field($model, 'placeofreg') ?>

    <?php // echo $form->field($model, 'current_visit') ?>

    <?php // echo $form->field($model, 'next_appointment_ANC') ?>

    <?php // echo $form->field($model, 'gravida') ?>

    <?php // echo $form->field($model, 'parity') ?>

    <?php // echo $form->field($model, 'lmp') ?>

    <?php // echo $form->field($model, 'edd') ?>

    <?php // echo $form->field($model, 'tri_t1') ?>

    <?php // echo $form->field($model, 'tri_t2') ?>

    <?php // echo $form->field($model, 'tri_t3') ?>

    <?php // echo $form->field($model, 'gestation') ?>

    <?php // echo $form->field($model, 'paternal_age_at_conception') ?>

    <?php // echo $form->field($model, 'medication_prior_pregnancy') ?>

    <?php // echo $form->field($model, 'medication') ?>

    <?php // echo $form->field($model, 'prescribed_medication_during_pregnancy') ?>

    <?php // echo $form->field($model, 'medication_during_pregnancy') ?>

    <?php // echo $form->field($model, 'fertility_treatment_past') ?>

    <?php // echo $form->field($model, 'fertility_treatment') ?>

    <?php // echo $form->field($model, 'planned_pregnancy') ?>

    <?php // echo $form->field($model, 'self_reaction_for_pregnancy') ?>

    <?php // echo $form->field($model, 'husband_reaction_for_pregnancy') ?>

    <?php // echo $form->field($model, 'family_reaction_for_pregnancy') ?>

    <?php // echo $form->field($model, 'sleep_disturbance') ?>

    <?php // echo $form->field($model, 'physical_activity') ?>

    <?php // echo $form->field($model, 'walking_duration') ?>

    <?php // echo $form->field($model, 'exercise_duration') ?>

    <?php // echo $form->field($model, 'yoga_duration') ?>

    <?php // echo $form->field($model, 'householdchores_duration') ?>

    <?php // echo $form->field($model, 'others_activity') ?>

    <?php // echo $form->field($model, 'others_duration') ?>

    <?php // echo $form->field($model, 'risk_identified_for_pregnancy') ?>

    <?php // echo $form->field($model, 'risk_identified_in_foetus') ?>

    <?php // echo $form->field($model, 'delivery') ?>

    <?php // echo $form->field($model, 'yearof_delivery') ?>

    <?php // echo $form->field($model, 'gestation_age_of_delivery') ?>

    <?php // echo $form->field($model, 'mode_of_delivery') ?>

    <?php // echo $form->field($model, 'complications_during_pregnancy') ?>

    <?php // echo $form->field($model, 'complications_in_baby') ?>

    <?php // echo $form->field($model, 'complications_in_mother') ?>

    <?php // echo $form->field($model, 'risk_identified_in_foetus_past') ?>

    <?php // echo $form->field($model, 'abortions_miscarrieges') ?>

    <?php // echo $form->field($model, 'medical_surgical_termination_related_complication') ?>

    <?php // echo $form->field($model, 'contraception') ?>

    <?php // echo $form->field($model, 'experience_in_previouspregnancy_postdelivery') ?>

    <?php // echo $form->field($model, 'specify_experience') ?>

    <?php // echo $form->field($model, 'pap_test') ?>

    <?php // echo $form->field($model, 'lactation_related_problems') ?>

    <?php // echo $form->field($model, 'baby_ability_to_suck') ?>

    <?php // echo $form->field($model, 'production_of_breast_milk') ?>

    <?php // echo $form->field($model, 'congenital_anomolies') ?>

    <?php // echo $form->field($model, 'special_children') ?>

    <?php // echo $form->field($model, 'mental_illness') ?>

    <?php // echo $form->field($model, 'postpartum_mental_health_issues') ?>

    <?php // echo $form->field($model, 'postpartum_mental_health_deatails') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <?php // echo $form->field($model, 'last_updated_date') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
