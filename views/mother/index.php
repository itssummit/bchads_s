<?php
use yii\web\View;
//Yii::$app->cache->set('motherId',$this->id);
$this->registerJsFile('@web/jsControllers/motherController.js',['depends' => [\app\assets\AppAsset::className()]])

?>
<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 index" ng-controller = "MotherController">
    
	<h3 class="hcss text-center"> List of Mothers</h3>
    <div class="row" ng-init="getAllSubject()">
        <table class="table table-bordered" data-pagination="" data-num-pages="numPages()"  data-current-page="currentPage" data-max-size="maxSize"  
         data-boundary-links ="true">
            <thead>
                <tr>
                    <th>ID</th>
					<th>Mother Code</th>
                </tr>
            </thead>
            <tbody data-dir-paginate="s in subjectList | filter : search |itemsPerPage:10" ng-click = "updateDetails(s.id)">
                <tr>
                	<td>{{s.id}}</td>
                    <td>{{s.mother_code}}</td>
                </tr>
				
				<tr id="indexId" ng-if="s.expanded" style="border-collapse: collapse;">
					<td colspan="2"></td>
					<td><b>Member Code</b></td>
					<td><b>Relation Type</b></td>
					<td><b>Action</b></td>
				</tr>
                <tr id="indexId" ng-repeat="m in memberList" ng-if="s.expanded && (s.id==m.mother_id)"  style="border-collapse: collapse;">
                	<td colspan="2">
					    <div class="col-md-3 col-lg-2 col-sm-6 col-xs-12" ng-if = "m.relation_type == 'Infant' && m.gender =='male'"><i class="fa fa-child" style="font-size:25px;height:20px"></i></div>
						<div class="col-md-3 col-lg-2 col-sm-6 col-xs-12" ng-if = "m.relation_type == 'Infant' && m.gender =='Female'"><i class="fa fa-female" style="font-size:25px;height:20px"></i></div>
						<div class="col-md-3 col-lg-2 col-sm-6 col-xs-12" ng-if = "m.relation_type == 'Spouse'"><img src ="/bchads/web/img/spouse1.png" alt="spouse" height="25px" width="30px"></div>
						<div class="col-md-3 col-lg-2 col-sm-6 col-xs-12" ng-if = "m.relation_type == 'CareGiver'"><img src ="/bchads/web/img/caregiver.png" alt="caregiver" height="25px" width="30px"></div>
                    </td>
					<td>
						<div class="col-md-3 col-lg-2 col-sm-6 col-xs-12 col-md-offset-2 col-lg-offset-2">{{m.member_code}}</div>
					</td>
					<td>
                        <div class="col-md-3 col-lg-2 col-sm-6 col-xs-12">{{m.relation_type}} </div>
					</td>
					<td>
						<div class="col-md-3 col-lg-2 col-sm-6 col-xs-12"><button type = "button" class = "btn btn-link" ng-click = "updatemem(m.id);$event.stopPropagation();"><i class = "fa fa-pencil-square-o">Edit Member</i></button>
						</div>
					</td>
                </tr>
            </tbody>
        </table>
        <dir-pagination-controls   max-size="10"  direction-links="true" boundary-links="true"></dir-pagination-controls>
    </div>
</div>