<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Mother */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Mothers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mother-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'first_name',
            'middle_name',
            'last_name',
            'dob',
            'age',
            'present_address',
            'phone1',
            'phone2',
            'permanent_address',
            'domicile',
            'religion',
            'educational_qualification',
            'occupation_Self',
            'occupation_Spouse',
            'socio_economic_status',
            'marital_status',
            'nature_of_marriage',
            'family_type',
            'no_of_family_members',
            'total_family_income',
            'spouse_substance_use',
            'substance_type',
            'duration_of_residence',
            'no_of_years_of_marriage_years',
            'no_of_years_of_marriage_months',
            'dateofreg',
            'placeofreg',
            'current_visit',
            'next_appointment_ANC',
            'gravida',
            'parity',
            'lmp',
            'edd',
            'tri_t1',
            'tri_t2',
            'tri_t3',
            'gestation',
            'paternal_age_at_conception',
            'medication_prior_pregnancy',
            'medication',
            'prescribed_medication_during_pregnancy',
            'medication_during_pregnancy',
            'fertility_treatment_past',
            'fertility_treatment',
            'planned_pregnancy',
            'self_reaction_for_pregnancy',
            'husband_reaction_for_pregnancy',
            'family_reaction_for_pregnancy',
            'sleep_disturbance',
            'physical_activity',
            'walking_duration',
            'exercise_duration',
            'yoga_duration',
            'householdchores_duration',
            'others_activity',
            'others_duration',
            'risk_identified_for_pregnancy',
            'risk_identified_in_foetus',
            'delivery',
            'yearof_delivery',
            'gestation_age_of_delivery',
            'mode_of_delivery',
            'complications_during_pregnancy',
            'complications_in_baby',
            'complications_in_mother',
            'risk_identified_in_foetus_past',
            'abortions_miscarrieges',
            'medical_surgical_termination_related_complication',
            'contraception',
            'experience_in_previouspregnancy_postdelivery',
            'specify_experience',
            'pap_test',
            'lactation_related_problems',
            'baby_ability_to_suck',
            'production_of_breast_milk',
            'congenital_anomolies',
            'special_children',
            'mental_illness',
            'postpartum_mental_health_issues',
            'postpartum_mental_health_deatails',
            'created_date',
            'last_updated_date',
            'updated_by',
        ],
    ]) ?>

</div>
