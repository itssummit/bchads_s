<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Mother */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="navigationID" >
	<ul class="nav nav-tabs">
	    <li class="active">
            <?php echo Html::a('Demographic','#demographic',['data-toggle' => "tab"])?>
	  	    
	    </li>
	    <li>
	  	<?php echo Html::a('Obstetric','#obstetric',['data-toggle' => "tab"])?>
	    </li>
	    <li>
	  	<?php echo Html::a('Assessment','#assessment',['data-toggle' => "tab"])?>
	    </li>
	</ul>
</div>
<div ng-controller="MotherController" class="col-md-12 col-lg-12 colsm-12 col-xs-12">
    <div class="tab-content">
        <div id="demographic" class="tab-pane fade in active">
        <form name="mainForm"  ng-submit = "save(mainForm,null)">
            <div class="col-md-4 col-lg-4 col-xs-6 col-sm-6">
                <label>First Name<span class="red" style="color: #f00">*</span></label>
		        <div class="form-group" show-errors>
		      	    <input type="text" class="form-control" name="firstname"  ng-bind="firstname" ng-model="$root.motherObj.first_name" required />
		            <p class="help-block" ng-if="mainForm.first_name.$error.required" >First Name Required</p>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-xs-6 col-sm-6">
                <label>Middle Name</label>
		        <div class="form-group" show-errors>
		      	    <input type="text" class="form-control" name="middlename" ng-model="$root.motherObj.middle_name" />
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-xs-6 col-sm-6">
                <label>Last Name<span class="red" style="color: #f00">*</span></label>
		        <div class="form-group" show-errors>
		      	    <input type="text" class="form-control" name="lastname"  ng-bind="firstname" ng-model="$root.motherObj.last_name" required />
		            <p class="help-block" ng-if="mainForm.last_name.$error.required" >Last Name Required</p>
                </div>
            </div>
            <div class = "pull-right">
        	    <button type = "submit" class = "btn btn-success">Save</button>
        	    <button type = "button" class = "btn btn-info"  ng-click = "navigate(1)" >Next</button>
            </div>
        </form>
       </div>
    

	    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'middle_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dob')->textInput() ?>

    <?= $form->field($model, 'age')->textInput() ?>

    <?= $form->field($model, 'present_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone1')->textInput() ?>

    <?= $form->field($model, 'phone2')->textInput() ?>

    <?= $form->field($model, 'permanent_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'domicile')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'religion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'educational_qualification')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'occupation_Self')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'occupation_Spouse')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'socio_economic_status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'marital_status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nature_of_marriage')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'family_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_of_family_members')->textInput() ?>

    <?= $form->field($model, 'total_family_income')->textInput() ?>

    <?= $form->field($model, 'spouse_substance_use')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'substance_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'duration_of_residence')->textInput() ?>

    <?= $form->field($model, 'no_of_years_of_marriage_years')->textInput() ?>

    <?= $form->field($model, 'no_of_years_of_marriage_months')->textInput() ?>
        
    
    <div id="obstetric" class="tab-pane fade">
        
    <?= $form->field($model, 'dateofreg')->textInput() ?>

    <?= $form->field($model, 'placeofreg')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'current_visit')->textInput() ?>

    <?= $form->field($model, 'next_appointment_ANC')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gravida')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'parity')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lmp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'edd')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tri_t1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tri_t2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tri_t3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gestation')->textInput() ?>

    <?= $form->field($model, 'paternal_age_at_conception')->textInput() ?>

    <?= $form->field($model, 'medication_prior_pregnancy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'medication')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prescribed_medication_during_pregnancy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'medication_during_pregnancy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fertility_treatment_past')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fertility_treatment')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'planned_pregnancy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'self_reaction_for_pregnancy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'husband_reaction_for_pregnancy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'family_reaction_for_pregnancy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sleep_disturbance')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'physical_activity')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'walking_duration')->textInput() ?>

    <?= $form->field($model, 'exercise_duration')->textInput() ?>

    <?= $form->field($model, 'yoga_duration')->textInput() ?>

    <?= $form->field($model, 'householdchores_duration')->textInput() ?>

    <?= $form->field($model, 'others_activity')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'others_duration')->textInput() ?>

    <?= $form->field($model, 'risk_identified_for_pregnancy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'risk_identified_in_foetus')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'delivery')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'yearof_delivery')->textInput() ?>

    <?= $form->field($model, 'gestation_age_of_delivery')->textInput() ?>

    <?= $form->field($model, 'mode_of_delivery')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'complications_during_pregnancy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'complications_in_baby')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'complications_in_mother')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'risk_identified_in_foetus_past')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'abortions_miscarrieges')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'medical_surgical_termination_related_complication')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contraception')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'experience_in_previouspregnancy_postdelivery')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'specify_experience')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pap_test')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lactation_related_problems')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'baby_ability_to_suck')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'production_of_breast_milk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'congenital_anomolies')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'special_children')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mental_illness')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'postpartum_mental_health_issues')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'postpartum_mental_health_deatails')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_date')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_updated_date')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'updated_by')->textInput(['maxlength' => true]) ?>

   
            <?php ActiveForm::end(); ?>
	</div>
 
    <div id="assessment" class="tab-pane fade">
	    	<?php echo $this->render('/assessment/index') ?>
	  </div>
    </div>
</div>