<?php

use yii\helpers\Html;
use app\models\Mother;
/* @var $this yii\web\View */
/* @var $model app\models\Mother */

$model = Mother::findOne(['id' => $id]);
$this->title = 'Update Details';
?>
<?= $this->render('_form',['id' => $id]) ?>