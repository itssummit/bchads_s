<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Mother;
use kartik\date\DatePicker;
use yii\web\View;
$motherId= Yii::$app->cache->get('motherId');
$mCode=Mother::findOne(['id'=> $motherId])->mother_code;
/* @var $this yii\web\View */
/* @var $model app\models\Member */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="member-form">
 <?php $form = ActiveForm::begin(['id' => 'formMember']); ?>

    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
	<b>Mother Code:</b>	<?php echo  $mCode;?>
        <br><br>
        <div class="row">
            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
				<?= $form->field($model, 'member_code',['template' => "{label}<span style = 'color:red;font-size:15px'>*</span>{input}{hint}{error}"])->textInput(["disabled" => ($model->member_code)?true:false]); ?>
			</div>
            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                 <?= $form->field($model, 'relation_type',['template' => "{label}<span style = 'color:red;font-size:15px'>*</span>{input}{hint}{error}"])->dropDownList(['' =>'','Infant' => 'Infant','CareGiver' => 'CareGiver' ]); ?>
            </div>
			
         </div>
        <div class="row" id = "wards" style = "display:<?php echo ($model->relation_type == 'Infant')?"display":"none";?>" >
			<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12" >
                 <?= $form->field($model, 'gender')->dropDownList([''=>'','male' => 'Male','Female' => 'Female' ]); ?>
            </div>
			<!--<div class="col-md-4 col-lg-6 col-sm-6 col-xs-12">
                 < ?= $form->field($model, 'dob')->widget(DatePicker::className(),['type' => DatePicker::TYPE_COMPONENT_APPEND,'removeButton' => false,'pluginOptions'=>['endDate' => '+0d','format' => 'yyyy-mm-dd','orientation' => 'left']]); ?>	           
			</div>-->
			<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
			<label>Age (In months)</label>
                 <?= $form->field($model, 'age_months')->textInput(['maxlength' => true])->label(false); ?> 
            </div>
        </div>
		<div class="row" id = "wardss" style = "display:<?php echo ($model->relation_type == 'CareGiver')?"display":"none";?>">
			<label>CareGiver`s Relationship With Child</label>
            <?= $form->field($model, 'ag_relation')->textInput()->label(false); ?> 
        </div>
         <div class="row">
            
			<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12"><br><br>
                <?php $model->is_active = "1"; ?>
				<?= $form->field($model, "is_active")->checkbox(['value' => "1"]); ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php $this->registerJs('
		
		$("#member-relation_type").change(function()
		{
			
			if($(this).val()== "Infant")
			{
				$("#wards").show();
			}
			else
			{
				$("#member-relation_type").val();
				$("#wards").hide();
			}
		});
		$("#member-relation_type").change(function()
		{
			
			if($(this).val()== "CareGiver")
			{
				$("#wardss").show();
			}
			else
			{
				$("#member-relation_type").val();
				$("#wardss").hide();
			}
		});
		
',View::POS_READY);?>		