<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Sass */

$this->title = 'Sass';
$this->params['breadcrumbs'][] = ['label' => 'Sasses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sass-create">

    <h1 align = "center"> ದೈಹಿಕ ಲಕ್ಷಣಗಳನ್ನು ವಿಮರ್ಶಿಸುವ ಮಾಪನ    </h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
