﻿<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$keyPair = [-15 => '-15: Child Not Co-Operative For Assessment',
		  -14 => '-14: Not Available For Assessment',
		  -11 => '-11: Inadequate Information',
		  -10 => '-10: Not Applicable',
		  -9 => '-9: Missing',
		  -8 => '-8: Refused',
		  -7 => '-7: Partner Accompanied Missing',
		  -6 => '-6: Family Accompanied Missing',
		  -5 => '-5: Missing:Not Asked',
		  -3 => '-3: No Such Relationship/No Partner'];

/* @var $this yii\web\View */
/* @var $model app\models\Sass */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile('@web/js/scalesass.js',['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<center>
<h3> 15 days </h3>
</center>
<div class="sass-form">

    <?php $form = ActiveForm::begin(['id' => 'formSass']); ?>

	<div  id="withBoxShadow" class="col-xs-8 col-md-8 col-lg-8 col-sm-8">
	
	<table border="1" class="responsive-scales">
			<thead>
                <tr>
				    <th></th>
					<th>ಇಲ್ಲವೇ ಇಲ್ಲ </th>
					<th>ಸ್ವಲ್ಪ ಮಟ್ಟಿಗೆ </th>
					<th>ಸಾಧಾರಣ </th>
					<th>ತೀವ್ರ </th>
					<th>Other Answers</th>
                </tr> 
			</thead>
			<tbody>
			
			<tr>
			<td>
			<b>A.ನೋವಿಗೆ ಸಂಬಂಧಪಟ್ಟ  ಲಕ್ಷಣಗಳು </b><br>
			1.ತಲೆ ನೋವು </td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'head_ache')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'head_ache')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>
			<tr>
			<td>2.ಬೆನ್ನು ನೋವು </td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'back_pain')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'back_pain')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>
			<tr>
			<td>3.ಕೈ,ಕಾಲುಗಳಲ್ಲಿ ನೋವು </td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'hand_leg_pain')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'hand_leg_pain')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>
			<tr>
			<td>4.ಹೊಟ್ಟೆ ನೋವು</td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'stomach_pain')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'stomach_pain')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>
			<tr>
			<td>5.ಇಡೀ ದೇಹದಲ್ಲಿ ನೋವು</td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'body_pain')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'body_pain')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>
			
			
			
			<tr>
			<td>
			<b>B.ಸಂವೇದನಾ ದೈಹಿಕ ಲಕ್ಷಣಗಳು</b><br>
			6.ಜುಮ್ಮೆನಿಸುವುದು  , ಜಡತ್ವ/ಮರಗಟ್ಟುವಿಕೆ</td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'jadatva')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'jadatva')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>
			
			<tr>
			<td>7.ಬಿಸಿ ಅಥವಾ ತಣ್ಣಗಿನ  ಅನುಭವ</td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'cold_hot')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'cold_hot')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>8.ಜೋರಾದ ಎದೆ ಬಡಿತ</td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'high_heart_beat')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'high_heart_beat')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>9.ಹೊಟ್ಟೆ ಉಬ್ಬರಿಸಿದ ಅನುಭವ</td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'stomach_prob')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'stomach_prob')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>10.ಉರಿಯ ಅನುಭವ </td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'burn')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'burn')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>
			
			
			
			<tr>
			<td>
			<b>c.ನಿರ್ದಿಷ್ಟವಾಗಿರದ ದೈಹಿಕ ಲಕ್ಷಣಗಳು</b><br>
			11.ದೈಹಿಕವಾಗಿ ನಿಶ್ಯಕ್ತಿ </td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'physical_weakness')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'physical_weakness')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>12.ಮಾನಸಿಕ ನಿಶ್ಯಕ್ತಿ</td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'psychological_weakness')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'psychological_weakness')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>13.ತಲೆಸುತ್ತುವುದು , ಮೂರ್ಛೆಬೀಳುವುದು</td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'unconscious')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'unconscious')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>14.ನಡುಕ, ಅದಿರುವಿಕೆ </td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'shivering')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'shivering')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>15.ಸುಸ್ತು/ಆಯಾಸ ,ಆಲಸ್ಯ /ನಿರಾಸಕ್ತಿ </td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'tired')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'tired')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>
			
			
			
			<tr>
			<td>
			<b>d.ಜೈವಿಕವಾದ ದೈಹಿಕ ಲಕ್ಷಣಗಳು</b><br>
			16.ನಿದ್ರೆ ಇಲ್ಲದಿರುವುದು </td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'no_sleep')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'no_sleep')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>17.ಹಸಿವಿಲ್ಲದಿರುವುದು</td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'no_hungry')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'no_hungry')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>18.ಲೈಂಗಿಕ ಆಸಕ್ತಿ ಇಲ್ಲದಿರುವುದು </td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'no_interest')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'no_interest')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>19.ಮಲಬದ್ಧತೆ </td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'constipation')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'constipation')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>20.ಭೇದಿ /ಅತಿಸಾರ </td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'loose_motions')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'loose_motions')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>
			
			
			
			<tr>
			<th>
			<b>e.ನಿರ್ದಿಷ್ಟವಾದ ದೈಹಿಕ ಲಕ್ಷಣಗಳು </b><br>
			21)ಕಾಲುಗಳ ಚಡಪಡಿಕೆಯ ರೋಗ ಲಕ್ಷಣಗಳ ಸಮೂಹ </th>
			</tr>
			<!--< ?php for($i = 0;$i < 4;$i++):?>
			<td>< ?= $form->field($model, 'leg_unrest')->radio(['label' => $i, 'value' => $i])?></td>
			< ?php endfor;?>
			<td>
				<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModalLong21">
				<span class="glyphicon glyphicon-download"></span>
				</button>
				<div class="modal fade" id="exampleModalLong21" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header bg-primary">
								<h3 class="modal-title" id="exampleModalLongTitle">21) Other Answers
								<button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">&times;</button>
								</h3>
							</div>
						<div class="modal-body">
						<table class = "table table-bordered">
							<tbody>
							<thead>
							<tr>
							<th>Options</th>
							<th>Values</th>
							</tr>
							</thead>
							< ?php for($i = 1;$i < 10;$i++):?>
								<tr>
								<td>< ?= $form->field($model, 'leg_unrest')->radio(['label' => $label[$i],'value' => $value[$i]])?></td>
								<th>< ?php echo  $value[$i];?></th>
								</tr>
							< ?php endfor;?>
							</tbody>
						</table>	
						</div>
						</div>
					</div>
				</div>
			</td>
			</tr>-->
			<tr>
			<td>a.ಕಾಲುಗಳನ್ನು ಅಲ್ಲಾಡಿಸುತ್ತಿರುವ  ಬಯಕೆ </td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'leg_shake')->radio(['label' => $i, 'value' => $i,'onchange' => 'checkLegShake()'])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'leg_shake')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>b.ಜುಮ್ಮೆನಿಸುವುದು , ಜಡತ್ವ ,ಕಾಲುಗಳ ಮಾಂಸಖಂಡಗಳಲ್ಲಿ  ಹಿಡಿತ ಅಥವಾ ಕಾಲುಗಳಲ್ಲಿ ನೋವು </td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'legs_pain')->radio(['label' => $i, 'value' => $i,'onchange' => 'checkLegShake()'])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'legs_pain')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr id = "trLS1">
			<td>c.ನಿಷ್ಕ್ರಿಯೆಯಾದಾಗ   ರೋಗ ಲಕ್ಷಣಗಳು ಇನ್ನೂ  ಕೆಟ್ಟದಾಗಿರುತ್ತದೆ / ಹೆಚ್ಚುತ್ತದೆ  </td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'disease_symptoms')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'disease_symptoms')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr id = "trLS2">
			<td>d.ನಡೆದಾಡುವುದರಿಂದ ಪರಿಹಾರ / ನಿರಾಳ  </td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'walks_relief')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'walks_relief')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr id = "trLS3">
			<td>e.ರಾತ್ರಿಯಾಗುತ್ತಿದಂತೆ   ರೋಗ ಲಕ್ಷಣಗಳು ಹೆಚ್ಚುತ್ತವೆ </td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'night_symptoms_increases')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'night_symptoms_increases')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr id = "trLS4">
			<td>f.ಇದರಿಂದ  ನಿದ್ರೆಗೆ  ತೊಂದೆರೆಯಾಗುತ್ತಿದೆಯೇ  </td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'sleep_prob')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'sleep_prob')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>
			
			
			
			<tr>
			<td>
			<b>22.ಟಿನಿಟಸ್  /  ಕಿವಿಮೂರೆತ  </b><br>
			a.ಕಿವಿಯಲ್ಲಿ ರಿಂಗಿರಿಂಗ್, ಶಿಳ್ಳೆ ,ಝೇಂಕರಿಸುವಂತಹ ಒಂದು ರೀತಿಯ  ಶಬ್ದ </td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'ringing_sounds')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'ringing_sounds')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>
			
				
				
			<tr>
			<td>
			<b>F.ಇತರೆ ಲಕ್ಷಣಗಳು</b><br>
			23.ವಾಕರಿಕೆ/ವಾಂತಿ </td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'vometings')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'vometings')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>24.ಪದೇ ಪದೇ ಮೂತ್ರ ವಿಸರ್ಜಿಸುವುದು</td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'urine')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'urine')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>25.ಯೋನಿಯಲ್ಲಿ ನವೆ</td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'yoni')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'yoni')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>26.ಕೆಳಹೊಟ್ಟೆ /ಸೊಂಟ ನೋವು </td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'pelvic')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'pelvic')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>27.ಬಿಳಿಸೆರಗು ಹೋಗುವುದು </td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'biliseragu')->radio(['label' => $i, 'value' => $i])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'biliseragu')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>

			<tr>
			<td>28.ಇತರೆ ಯಾವುದಾದರು </td>
			<?php for($i = 0;$i < 4;$i++):?>
			<td><?= $form->field($model, 'any_other')->radio(['label' => $i, 'value' => $i,'class' => 's1'])?></td>
			<?php endfor;?>
			<td>
				<?= $form->field($model, 'any_other')->dropDownList($keyPair,['prompt' => 'Select'])->label(false)?> 
			</td>
			</tr>
			
			<tr  id="rrr" style = "display:<?php echo ($model->any_other >= '1')?"display":"none";?>">
			
			<td>29 .Others Symptoms</td>
			
			<td colspan="4"><?= $form->field($model, 'symptoms_others')->textInput()->label(false);?></td>
			
			<td>
				 
			</td>
			</tr>
			
			<tr>
            <td id="scaleflag" colspan="4" class="hidden"><?= $form->field($model, 'score')->input('number',['readonly'=>true])->label(false);?></td>
            </tr>
    </tbody>
    </table>
	
	</div>
    
    <?php ActiveForm::end(); ?>

</div>
<?php $this->registerJs('
	$("#"+formObj.formName).one("change",function()
	{
		autoSaveObj = formObj;
	});

	$(".s1").change(function(){
				console.log("calling");
				if($(this).val() == 1 || $(this).val() == 2 || $(this).val() == 3){
					console.log("entered in if loop");
					$("#sass-symptoms_others").val("");
					$("#rrr").show();
	autoSave(formObj);
				}
				else if($(this).val() == 0)
				{
					$("#sass-symptoms_others").val("-10");
					$("#rrr").hide();
	autoSave(formObj);
				}
				});
	checkLegShake();
',\yii\web\View::POS_READY)?>
<script type="text/javascript">
	formObj = {url:"sass/ajax",formName : "formSass"};
    var calc = function () {
        var totalValue = 0;
		var root = 'sass';
        var id = '#'+root+'-';
		var clas = '.field-'+root+'-';
        var arr = ['head_ache', 'back_pain', 'hand_leg_pain', 'stomach_pain', 'body_pain', 'jadatva', 'cold_hot', 'high_heart_beat', 
		'stomach_prob', 'burn', 'physical_weakness', 'psychological_weakness', 'unconscious', 'shivering', 'tired', 'no_sleep', 
		'no_hungry', 'no_interest', 'constipation', 'loose_motions', 'leg_shake', 'legs_pain', 'disease_symptoms', 'walks_relief', 
		'night_symptoms_increases', 'sleep_prob', 'ringing_sounds', 'vometings', 'urine', 'yoni', 'pelvic', 'biliseragu', 'any_other'];
            

        for (var i in arr) {
            var val = $(clas + arr[i] + ' input[type=radio]:checked').val();
            if (parseInt(val) >= 0) {
                totalValue += parseInt(val);
            }
        }

        $(id + 'score').val(totalValue);
		autoSaveObj = formObj;
	}
	
	var checkLegShake = function(){
		var val	=	['disease_symptoms','walks_relief','night_symptoms_increases','sleep_prob'];
		var val1 = parseInt($('input[name="Sass[leg_shake]"]:checked').val());
		var val2 = parseInt($('input[name="Sass[legs_pain]"]:checked').val());
		val1 = (isNaN(val1))?0:val1;
		val2 = (isNaN(val2))?0:val2;
		if((val1 <= 0) && (val2 <= 0))
		{
			hideDS();
		}	
		else
		{
			showDS();
		}
	}
	var showDS = function(){
		$("#trLS1").show();
		$("#trLS2").show();
		$("#trLS3").show();
		$("#trLS4").show();
	}
	
	var hideDS = function(){
		$("#trLS1").hide();
		$('input[name="Sass[disease_symptoms]"]').prop("checked",false);
		$('select[name="Sass[disease_symptoms]"]').val("-10")
		$("#trLS2").hide();
		$('input[name="Sass[walks_relief]"]').prop("checked",false);
		$("#trLS3").hide();
		$('input[name="Sass[night_symptoms_increases]"]').prop("checked",false);
		$("#trLS4").hide();
		$('input[name="Sass[sleep_prob]"]').prop("checked",false);
	}
</script>


