<?php

namespace app\controllers;

use Yii;
use app\models\Obstetric;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


/**
 * ObstetricController implements the CRUD actions for Obstetric model.
 */
class ObstetricController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Obstetric models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Obstetric::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Obstetric model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Obstetric model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
    	Yii::$app->session->set('patientId',null);
        $model = new Obstetric();

        if ($model->load(Yii::$app->request->post())) {
        	$model->dateofreg = ($model->dateofreg)?date('Y-m-d',strtotime($model->dateofreg)):null;
            
        	$model->created_dtm = date('Y-m-d H:i:s');
        	$model->save();
			
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }   
    }
	
	 public function actionValidation(){
    	
    	$model = new Obstetric();
    	if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())){
    		Yii::$app->response->format = 'json';
    		return ActiveForm::validate($model);
    	}
    }
    public function actionAjax()
    {
    	$model = new Obstetric();
    	$postForm = $_POST['Obstetric'];
    	$returnCreateValue = 'Obstetric Created';
    	$returnUpdateValue = 'Obstetric Updated';
    	$returnFalseValue = 'Obstetric Not Inserted';
    	$patientId = Yii::$app->cache->get('patientId');;
    	if(isset($postForm))
    	{
    		if($id)
    		{
    			$model = $this->findModel($id);
    			$returnValue = $returnUpdateValue;
    			$model->attributes = $postForm;
                 
                 $model->save(false);
    		}
    		else
    		{
    			$returnValue = $returnCreateValue;
    			$model->attributes = $postForm;
    			$model->created_dtm = date('Y-m-d H:i:s');
    		}
    		if($model->dateofreg)
    		{
    			$model->dateofreg = date('Y-m-d h:i:s',strtotime($model->dateofreg));
    		}
    		
    		$model->save();
    	}
    	return $returnValue;
    }
    /**
     * Updates an existing Obstetric model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
    	Yii::$app->session->set('patientId',$model->id);
    	if ($model->load(Yii::$app->request->post())) 
    	{
    		$model->dateofreg = ($model->dateofreg)?date('Y-m-d',strtotime($model->dateofreg)):null;
            $model->save();
    		return $this->render('update',['model' => $model]);
        } 
        else 
        {
            return $this->render('update', ['model' => $model]);
        }
    }

    /**
     * Deletes an existing Obstetric model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Obstetric model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Obstetric the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Obstetric::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
