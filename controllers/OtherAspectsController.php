<?php

namespace app\controllers;

use Yii;
use app\models\OtherAspects;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OtherAspectsController implements the CRUD actions for OtherAspects model.
 */
class OtherAspectsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OtherAspects models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => OtherAspects::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OtherAspects model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new OtherAspects model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
     public function actionCreate()
    {
       
        $respondentId = Yii::$app->session->get('respondentId');
        $assessmentId = Yii::$app->session->get('assessmentId');
    	$check = OtherAspects::findOne(['respondent_id' =>  $respondentId,'assessment_id' => $assessmentId]);
    	if(isset($check['id']))
    	{
    		$model = $this->findModel($check['id']);
    	}
    	else
    	{
    		$model = new OtherAspects();
    	}
    	return $this->render('create', ['model' => $model]);
    }
	
	public function actionAjax()
    {
    	$model = new OtherAspects();
    	$postForm = $_POST['OtherAspects'];
    	$returnCreateValue = 'OtherAspects Created';
    	$returnUpdateValue = 'OtherAspects Updated';
    	$returnFalseValue = 'OtherAspects Not Inserted';
        $assessmentId = Yii::$app->session->get('assessmentId');
        $respondentId = Yii::$app->session->get('respondentId');
    	if(isset($postForm))
    	{
    		$findModel = OtherAspects::findOne(['respondent_id' =>  $respondentId,'assessment_id' => $assessmentId]);
    		if(isset($findModel['id']))
    		{
    			$model = $this->findModel($findModel['id']);
    			$returnValue = $returnUpdateValue;
    			$model->attributes = $postForm;
                $model->save();
    		}
    		else
    		{
    			$returnValue = $returnCreateValue;
    			$model->attributes = $postForm;
                $model->scale_id=0;
                $model->save();
    		}
    		$model->respondent_id =  $respondentId;
            $model->assessment_id= $assessmentId;
    		$model->save();
            $scale = new \app\models\Scores;
            $scale->updateScales( $respondentId,$assessmentId,$model->scale_id,$model->score);
            
    			
    	}
    	return $returnValue;
    }


    /**
     * Updates an existing OtherAspects model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing OtherAspects model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the OtherAspects model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OtherAspects the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OtherAspects::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
