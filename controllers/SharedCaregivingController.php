<?php

namespace app\controllers;

use Yii;
use app\models\SharedCaregiving;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\classes\SessionHandler;
use app\models\Users;
use app\classes\RespondentType;
/**
 * SharedCaregivingController implements the CRUD actions for SharedCaregiving model.
 */
class SharedCaregivingController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SharedCaregiving models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => SharedCaregiving::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SharedCaregiving model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SharedCaregiving model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
     public function actionCreate()
    {
       
        $motherId = Yii::$app->session->get('motherId');
        $assessmentId = Yii::$app->session->get('assessmentId');
		//$respondentId = Yii::$app->session->get('respondentId');
        $resp = new RespondentType();
        $respondentId =$resp->getTypeId('m,ac');
    	$check = SharedCaregiving::findOne(['mother_id' =>  $motherId,'assessment_id' => $assessmentId,'respondent_id'=>$respondentId]);
    	if(isset($check['id']))
    	{
    		$model = $this->findModel($check['id']);
    	}
    	else
    	{
    		$model = new SharedCaregiving();
    	}
    	return $this->render('create', ['model' => $model]);
    }
	
	public function actionAjax()
    {
    	$model = new SharedCaregiving();
    	$postForm = $_POST['SharedCaregiving'];
    	$returnCreateValue = 'SharedCaregiving Created';
    	$returnUpdateValue = 'SharedCaregiving Updated';
    	$returnFalseValue = 'SharedCaregiving Not Inserted';
        $assessmentId = Yii::$app->session->get('assessmentId');
        $motherId = Yii::$app->session->get('motherId');
		$resp = new RespondentType();
		$respondentId =$resp->getTypeId('m,ac');
    	if(isset($postForm))
    	{
    		$findModel = SharedCaregiving::findOne(['mother_id' =>  $motherId,'assessment_id' => $assessmentId,'respondent_id'=>$respondentId]);
    		if(isset($findModel['id']))
    		{
    			$model = $this->findModel($findModel['id']);
				$returnValue = $returnUpdateValue;
    			$model->attributes = $postForm;
			
			}
    		else
    		{
    			$returnValue = $returnCreateValue;
    			$model->attributes = $postForm;
                $model->scale_id=21;
                $model->created_dtm= date('Y:m:d h:i:s');
    		}
    		$model->mother_id =  $motherId;
            $model->assessment_id= $assessmentId;
			$model->respondent_id= $respondentId;
			$model->last_updated_dtm= date('Y:m:d h:i:s');
			$model->updated_by=Users::findOne(['id' => Yii::$app->user->id])->username;
            $fields = ['songs','songs_acg','songs_acg_lastweek','played','played_acg','played_acg_lastweek','taken_out','taken_out_acg','taken_out_acg_lastweek','introduced_newthings','introduced_newthings_acg','introduced_newthings_acg_lastweek','told_not_todo_something','told_not_todo_something_acg','told_not_todo_something_acg_lastweek','cleaned','cleaned_acg','cleaned_acg_lastweek','rocked','rocked_acg','rocked_acg_lastweek','carried','carried_acg','carried_acg_lastweek','scolded','scolded_acg','scolded_acg_lastweek','wasnt_too_hot','wasnt_too_hot_acg','wasnt_too_hot_acg_lastweek','care_sick','care_sick_acg','care_sick_acg_lastweek','taken_harm','taken_harm_acg','taken_harm_acg_lastweek','fed','fed_acg','fed_acg_lastweek','kissed','kissed_acg','kissed_acg_lastweek','cooed','cooed_acg','cooed_acg_lastweek','put_sleep','put_sleep_acg','put_sleep_acg_lastweek','bathed','bathed_acg','bathed_acg_lastweek','didnt_eat_harmful','didnt_eat_harmful_acg','didnt_eat_harmful_acg_lastweek','soothed','soothed_acg','soothed_acg_lastweek','massaged','massaged_acg','massaged_acg_lastweek','showed_what_todo','showed_what_todo_acg','showed_what_todo_acg_lastweek','thoogiddene','thoogiddene_acg','thoogiddene_acg_lastweek','cuddled_crying','cuddled_crying_acg','cuddled_crying_acg_lastweek','dressed','dressed_acg','dressed_acg_lastweek','told_off','told_off_acg','told_off_acg_lastweek','comfortable','comfortable_acg','comfortable_acg_lastweek','slept_beside','slept_beside_acg','slept_beside_acg_lastweek','introduced_new_people','introduced_new_people_acg','introduced_new_people_acg_lastweek','scolded_crying','scolded_crying_acg','scolded_crying_acg_lastweek','cradled_crying','cradled_crying_acg','cradled_crying_acg_lastweek','made_warm','made_warm_acg','made_warm_acg_lastweek','gone_wokeup','gone_wokeup_acg','gone_wokeup_acg_lastweek','prepared_formula_milk','prepared_formula_milk_acg','prepared_formula_milk_acg_lastweek','talked','talked_acg','talked_acg_lastweek','cuddled','cuddled_acg','cuddled_acg_lastweek','sung','sung_acg','sung_acg_lastweek','taken_nurse','taken_nurse_acg','taken_nurse_acg_lastweek','talked_sternly','talked_sternly_acg','talked_sternly_acg_lastweek','held','held_acg','held_acg_lastweek','shared_smiles','shared_smiles_acg','shared_smiles_acg_lastweek','distracted_crying','distracted_crying_acg','distracted_crying_acg_lastweek','showed_rightthing','showed_rightthing_acg','showed_rightthing_acg_lastweek','taken_visit_family','taken_visit_family_acg','taken_visit_family_acg_lastweek','cradled_arms','cradled_arms_acg','cradled_arms_acg_lastweek','fed_crying','fed_crying_acg','fed_crying_acg_lastweek','prayed','prayed_acg','prayed_acg_lastweek','invovled_family_rituals','invovled_family_rituals_acg','invovled_family_rituals_acg_lastweek'];
            for($i = 0;$i < count($fields);$i++)
            {
                $model[$fields[$i]] = ($model[$fields[$i]] !== '')?$model[$fields[$i]]:0;
            }
    		$model->save(false);
            $scale = new \app\models\Scores;
            $scale->updateScales( $motherId,$assessmentId,$model->scale_id,$model->score);
            
    			
    	}
    	return $returnValue;
    }

    private function setZero($value){
        if($value == null)
        {
            $value = "0";
        }
        return $value;
    }


    /**
     * Updates an existing SharedCaregiving model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SharedCaregiving model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SharedCaregiving model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SharedCaregiving the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SharedCaregiving::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
