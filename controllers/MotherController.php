<?php

namespace app\controllers;

use Yii;
use app\models\Mother;
use app\models\MotherSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use app\models\User;
use app\models\Users;
use app\models\Assessment;
use app\classes\SessionHandler;
/**
 * MotherController implements the CRUD actions for Mother model.
 */
class MotherController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Mother models.
     * @return mixed
     */
    public function actionIndex()
    {   
        //$session = new SessionHandler();
    	//$session->clearSessions();
        $searchModel = new MotherSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Mother model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Mother model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $session = new SessionHandler();
    	$session->clearSessions();
        return $this->render('create',['id' => null]);
    }

    /**
     * Updates an existing Mother model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
    	Yii::$app->session->set('motherId',intval($id));
       	$model = $this->findModel($id);
		return $this->render('update',['id' => $id]);
	}
    

    /**
     * Deletes an existing Mother model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /* save mother details*/

     public function actionSave()
    {
    	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	$params = Json::decode(file_get_contents('php://input'),true);
		if(!isset($params['mother_code']))exit;
    	if(isset($params['id']))
    	{
    		$model = Mother::findOne(['id' => $params['id']]);
			$model->attributes = $params;
			$model->last_updated_dtm = date('Y-m-d h:i:s');
    	}
    	else
    	{
			$model = new Mother();
    		$model->attributes = $params;
			$model->created_dtm = date('Y-m-d h:i:s');
			$model->last_updated_dtm = date('Y-m-d h:i:s');
			
    	}
		$userId = Yii::$app->user->id;
		$username = Users::findOne(['id' => $userId])->username;
		$model->mother_code = strtoupper($model->mother_code);
		$model->delivery_date = ($model->delivery_date)?date('Y-m-d',strtotime($model->delivery_date)):null;
		$model->dateofreg = ($model->dateofreg)?date('Y-m-d',strtotime($model->dateofreg)):null;
		$model->current_visit = ($model->current_visit)?date('Y-m-d',strtotime($model->current_visit)):null;
		$model->next_appointment_ANC = ($model->next_appointment_ANC)?date('Y-m-d',strtotime($model->next_appointment_ANC)):null;
		$model->edd = ($model->edd)?date('Y-m-d',strtotime($model->edd)):null;
		$model->lmp = ($model->lmp)?date('Y-m-d',strtotime($model->lmp)):null;
		$model->assessment_date = ($model->assessment_date)?date('Y-m-d',strtotime($model->assessment_date)):null;
		$model->updated_by = $username;
    	if($model->save(false))
		{
        	Yii::$app->session->set('motherId',intval($model->id));
   			return $model;
		}
		else 
		{
			Yii::$app->session->set('motherId',null);
    		return "Validation Error";
		}
    }
	
	
	public function actionCheckmothercode($val)
	{
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$model = Mother::findOne(['mother_code' => $val]);
		if(isset($model['id']))
		{
			return true;
		}
		return false;
	}
    

	public function actionAllsubjects(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = Yii::$app->db->createCommand("SELECT m.*,m.first_name as Name1,m.last_name as Name2,DATE_FORMAT(m.last_updated_dtm,'%d-%b-%Y') as last_updated_dtm,DATE_FORMAT(m.delivery_date,'%d-%b-%Y') as deliverydate,DATE_FORMAT(m.created_dtm,'%d-%b-%Y') as createddate,m.mother_code as mothercode from mother as m order by m.id desc")->queryAll();
        return $model;
    }
     
	public function actionAllmemberlist(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = Yii::$app->db->createCommand("SELECT * from member")->queryAll();
        return $model;
    }
	
	public function actionGet($id)
    {
    	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	//$params = Json::decode(file_get_contents('php://input'),true);
        $session = new SessionHandler();
        $motherId = $session->getMotherId();
    	$model = Mother::findOne(['id' => $id]);
        $obj = 	[
	  				'model' => $model,
	  				'assessmentList' => Assessment::find()->where(['mother_id' => $id])->all(),
                    
                ];
        return $obj;
    }
    /**
     * Finds the Mother model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Mother the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Mother::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	public function actionCreatemothers(){
		
		for($i=901;$i<913;$i++){
			
				$model = new Mother();
				$model->mother_code = "C".$i;
				$model->created_dtm = date('Y-m-d h:i:s');
				$model->last_updated_dtm = date('Y-m-d h:i:s');
				$model->updated_by = "superadmin";
				$model->save(false);
			}
		
		
		
		
	}
	public function actionTest(){
		return "hii";
	}
}
