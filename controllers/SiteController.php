<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use app\classes\SessionHandler;
use app\models\Mother;
use app\classes\ReportProcessor;
use app\classes\SyncOperations;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
   /* public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }*/
    public function behaviors()
    {
	return [
        	'ghost-access'=> [
            	'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
        	],
	    ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    /**
     * Displays homepage.
     *
     * @return string
     */
  
     public function actionIndex()
    {
        $session = new SessionHandler();
    	$session->clearSessions();
        return $this->render('index',[
    								'dataProvider' => new ActiveDataProvider([
    													'query' => Mother::find()
    												])
    							]);
    }
    
    public function actionSync(){
        $session = new SessionHandler();
        $session->clearSessions();
        return $this->render('sync');
    }
    
    public function actionSyncprocess(){
        $sync = new SyncOperations();
        return $sync->process();
    }

    
    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }
	
	public function actionExport(){
	    $session = new SessionHandler();
	    $session->clearSessions();
    	return $this->render('export');
    }
	
	public function actionReportgenerator($assesstype){
    	$report = new ReportProcessor();
    	$data = $report->getReport($assesstype);
		$dt = date('Y:m:d h:i:s');
    	$file = "$dt.bchads.csv";
    	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    	header('Content-Disposition: attachment; filename='.$file);
    	$print = null;
    	for($i = 0;$i < count($data);$i++)
    	{
    		$values = implode(array_values($data[$i]),",");
    		if($i == 0)
    		{
    			$keys = implode(array_keys($data[$i]),",");
    			$print .= $keys."\n";
    		}
    		$print .= $values."\n";
    	}
		
    	echo $print;    
    }
	
    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
    	$session = new SessionHandler();
    	$session->clearSessions();
        Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    public function actionAdmin()
    {
        $session = new SessionHandler();
        $session->clearSessions();
        return $this->render('admin');
    }
}
