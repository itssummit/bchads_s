<?php

namespace app\controllers;

use Yii;
use app\models\ParentalBondingMother;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\classes\SessionHandler;
use app\models\Users;
/**
 * ParentalBondingMotherController implements the CRUD actions for ParentalBondingMother model.
 */
class ParentalBondingMotherController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ParentalBondingMother models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ParentalBondingMother::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ParentalBondingMother model.
     * @param integer $id
     * @param string $talks_friendly
     * @return mixed
     */
    public function actionView($id, $talks_friendly)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, $talks_friendly),
        ]);
    }

    /**
     * Creates a new ParentalBondingMother model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
     public function actionCreate()
    {
       $respondentId = Yii::$app->session->get('respondentId');
        $assessmentId = Yii::$app->session->get('assessmentId');
    	$check = ParentalBondingMother::findOne(['respondent_id' =>  $respondentId,'assessment_id' => $assessmentId]);
    	if(isset($check['id']))
    	{
    		$model = $this->findModel($check['id']);
    	}
    	else
    	{
    		$model = new ParentalBondingMother();
    	}
    	return $this->render('create', ['model' => $model]);
    }
	
	public function actionAjax()
    {
    	$model = new ParentalBondingMother();
    	$postForm = $_POST['ParentalBondingMother'];
    	$returnCreateValue = 'ParentalBondingMother Created';
    	$returnUpdateValue = 'ParentalBondingMother Updated';
    	$returnFalseValue = 'ParentalBondingMother Not Inserted';
        $assessmentId = Yii::$app->session->get('assessmentId');
        $respondentId = Yii::$app->session->get('respondentId');
    	if(isset($postForm))
    	{
    		$findModel = ParentalBondingMother::findOne(['respondent_id' =>  $respondentId,'assessment_id' => $assessmentId]);
    		if(isset($findModel['id']))
    		{
    			$model = $this->findModel($findModel['id']);
    			$returnValue = $returnUpdateValue;
    			$model->attributes = $postForm;
                
    		}
    		else
    		{
    			$returnValue = $returnCreateValue;
    			$model->attributes = $postForm;
                $model->scale_id=29;
                $model->created_dtm= date('Y:m:d h:i:s');
    		}
    		$model->respondent_id =  $respondentId;
            $model->assessment_id= $assessmentId;
			$model->last_updated_dtm= date('Y:m:d h:i:s');
            $model->updated_by=Users::findOne(['id' => Yii::$app->user->id])->username;
    		$model->save();
            $scale = new \app\models\Scores;
            $scale->updateScales( $respondentId,$assessmentId,$model->scale_id,$model->score);
            
    			
    	}
    	return $returnValue;
    }


    /**
     * Updates an existing ParentalBondingMother model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param string $talks_friendly
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ParentalBondingMother model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param string $talks_friendly
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ParentalBondingMother model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param string $talks_friendly
     * @return ParentalBondingMother the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ParentalBondingMother::findOne(['id' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
