<?php

namespace app\controllers;

use Yii;
use app\models\EdinburghPostnatalDepression;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\classes\SessionHandler;
use app\models\Users;
use app\classes\RespondentType;
/**
 * EdinburghPostnatalDepressionController implements the CRUD actions for EdinburghPostnatalDepression model.
 */
class EdinburghPostnatalDepressionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

	
	
    /**
     * Lists all EdinburghPostnatalDepression models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => EdinburghPostnatalDepression::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EdinburghPostnatalDepression model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new EdinburghPostnatalDepression model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $motherId = Yii::$app->session->get('motherId');
        $assessmentId = Yii::$app->session->get('assessmentId');
		//$respondentId = Yii::$app->session->get('respondentId');
        $resp = new RespondentType();
        $respondentId =$resp->getTypeId('m');
    	$check = EdinburghPostnatalDepression::findOne(['mother_id' => $motherId,'assessment_id' => $assessmentId,'respondent_id' =>$respondentId]);
    	if(isset($check['id']))
    	{
    		$model = $this->findModel($check['id']);
    	}
    	else
    	{
    		$model = new EdinburghPostnatalDepression();
    	}
    	return $this->render('create', ['model' => $model]);
    }

	
	public function actionAjax()
    {
    	$model = new EdinburghPostnatalDepression();
    	$postForm = $_POST['EdinburghPostnatalDepression'];
    	$returnCreateValue = 'EdinburghPostnatalDepression Created';
    	$returnUpdateValue = 'EdinburghPostnatalDepression Updated';
    	$returnFalseValue = 'EdinburghPostnatalDepression Not Inserted';
    	$motherId = Yii::$app->session->get('motherId');
        $assessmentId = Yii::$app->session->get('assessmentId');
        //$respondentId = Yii::$app->session->get('respondentId');
        $resp = new RespondentType();
        $respondentId =$resp->getTypeId('m');
    	if(isset($postForm))
    	{
    		$findModel = EdinburghPostnatalDepression::findOne(['mother_id' => $motherId,'assessment_id' => $assessmentId,'respondent_id' => $respondentId]);
    		if(isset($findModel['id']))
    		{
    			$model = $this->findModel($findModel['id']);
    			$returnValue = $returnUpdateValue;
    			$model->attributes = $postForm;
                
				
    		}
    		else
    		{
    			$returnValue = $returnCreateValue;
    			$model->attributes = $postForm;
                $model->scale_id=2;
				$model->created_dtm= date('Y:m:d h:i:s');
                
    		}
    		$model->mother_id = $motherId;
            $model->assessment_id= $assessmentId;
			$model->respondent_id= $respondentId;
			$model->last_updated_dtm= date('Y:m:d h:i:s');
            $model->updated_by=Users::findOne(['id' => Yii::$app->user->id])->username;
    		$model->save();
            $scale = new \app\models\Scores;
            $scale->updateScales($motherId,$assessmentId,$model->scale_id,$model->score);
            
    			
    	}
    	return $returnValue;
    }
    /**
     * Updates an existing EdinburghPostnatalDepression model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing EdinburghPostnatalDepression model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the EdinburghPostnatalDepression model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EdinburghPostnatalDepression the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EdinburghPostnatalDepression::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
