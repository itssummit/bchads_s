<?php

namespace app\controllers;

use Yii;
use app\models\ConflictTacticsScale;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Scores;
use app\classes\SessionHandler;
use app\models\Users;
use app\classes\RespondentType;


/**
 * ConflictTacticsScaleController implements the CRUD actions for ConflictTacticsScale model.
 */
class ConflictTacticsScaleController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ConflictTacticsScale models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ConflictTacticsScale::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ConflictTacticsScale model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ConflictTacticsScale model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
      // $respondentId = Yii::$app->session->get('respondentId');
         $resp = new RespondentType();
        $respondentId =$resp->getTypeId('m');
       $assessmentId = Yii::$app->session->get('assessmentId');
	   $motherId = Yii::$app->session->get('motherId');
       $check = ConflictTacticsScale::findOne(['respondent_id' => $respondentId,'assessment_id' => $assessmentId,'mother_id'=>$motherId]);
    	if(isset($check['id']))
    	{
    		$model = $this->findModel($check['id']);
    	}
    	else
    	{
    		$model = new ConflictTacticsScale();
    	}
    	return $this->render('create', ['model' => $model]);
    }
	
	public function actionAjax()
    {
    	$model = new ConflictTacticsScale();
    	$postForm = $_POST['ConflictTacticsScale'];
    	$returnCreateValue = 'ConflictTacticsScale Created';
    	$returnUpdateValue = 'ConflictTacticsScale Updated';
    	$returnFalseValue = 'ConflictTacticsScale Not Inserted';
        $assessmentId = Yii::$app->session->get('assessmentId');
        //$respondentId = Yii::$app->session->get('respondentId');
         $resp = new RespondentType();
        $respondentId =$resp->getTypeId('m');
		$motherId = Yii::$app->session->get('motherId');
    	if(isset($postForm))
    	{
    		$findModel = ConflictTacticsScale::findOne(['respondent_id' =>  $respondentId,'assessment_id' => $assessmentId,'mother_id'=>$motherId]);
    		if(isset($findModel['id']))
    		{
    			$model = $this->findModel($findModel['id']);
    			$returnValue = $returnUpdateValue;
    			$model->attributes = $postForm;
                
    		}
    		else
    		{
    			$returnValue = $returnCreateValue;
    			$model->attributes = $postForm;
                $model->scale_id=12;
                $model->created_dtm= date('Y:m:d h:i:s');
    		}
    		$model->respondent_id = $respondentId;
            $model->assessment_id= $assessmentId;
			$model->mother_id= $motherId;
			$model->last_updated_dtm= date('Y:m:d h:i:s');
            $model->updated_by=Users::findOne(['id' => Yii::$app->user->id])->username;
    		$model->save(false);
            $scale = new \app\models\Scores;
            $scale->updateScales($respondentId,$assessmentId,$model->scale_id,$model->score);
            
    			
    	}
    	return $returnValue;
    }

    /**
     * Updates an existing ConflictTacticsScale model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ConflictTacticsScale model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ConflictTacticsScale model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ConflictTacticsScale the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ConflictTacticsScale::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
