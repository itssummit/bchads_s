<?php

namespace app\controllers;

use Yii;
use app\models\InfantAnthropomatric;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\classes\SessionHandler;
use app\models\Users;
use app\classes\RespondentType;

/**
 * InfantAnthropomatricController implements the CRUD actions for InfantAnthropomatric model.
 */
class InfantAnthropomatricController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all InfantAnthropomatric models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => InfantAnthropomatric::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single InfantAnthropomatric model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new InfantAnthropomatric model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
     public function actionCreate()
    {
       
        //$respondentId = Yii::$app->session->get('respondentId');
        $resp = new RespondentType();
        $respondentId =$resp->getTypeId('m');
        $assessmentId = Yii::$app->session->get('assessmentId');
		$motherId = Yii::$app->session->get('motherId');
    	$check = InfantAnthropomatric::findOne(['respondent_id' => $respondentId,'assessment_id' => $assessmentId,'mother_id'=>$motherId]);
    	if(isset($check['id']))
    	{
    		$model = $this->findModel($check['id']);
    	}
    	else
    	{
    		$model = new InfantAnthropomatric();
    	}
    	return $this->render('create', ['model' => $model]);
    }
	
	public function actionAjax()
    {
    	$model = new InfantAnthropomatric();
    	$postForm = $_POST['InfantAnthropomatric'];
    	$returnCreateValue = 'InfantAnthropomatric Created';
    	$returnUpdateValue = 'InfantAnthropomatric Updated';
    	$returnFalseValue = 'InfantAnthropomatric Not Inserted';
        $assessmentId = Yii::$app->session->get('assessmentId');
        //$respondentId = Yii::$app->session->get('respondentId');
        $resp = new RespondentType();
        $respondentId =$resp->getTypeId('m');
		$motherId = Yii::$app->session->get('motherId');
    	if(isset($postForm))
    	{
    		$findModel = InfantAnthropomatric::findOne(['respondent_id' =>  $respondentId,'assessment_id' => $assessmentId,'mother_id'=>$motherId]);
    		if(isset($findModel['id']))
    		{
    			$model = $this->findModel($findModel['id']);
    			$returnValue = $returnUpdateValue;
    			$model->attributes = $postForm;
                
    		}
    		else
    		{
    			$returnValue = $returnCreateValue;
    			$model->attributes = $postForm;
                $model->scale_id=27;
                $model->created_dtm= date('Y:m:d h:i:s');
    		}
    		$model->respondent_id =  $respondentId;
            $model->assessment_id= $assessmentId;
			$model->mother_id= $motherId;
			$model->last_updated_dtm= date('Y:m:d h:i:s');
            $model->updated_by=Users::findOne(['id' => Yii::$app->user->id])->username;
    		$model->save();
            $scale = new \app\models\Scores;
            $scale->updateScales( $respondentId,$assessmentId,$model->scale_id,$model->score);
            
    			
    	}
    	return $returnValue;
    }

    
    /**
     * Updates an existing InfantAnthropomatric model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing InfantAnthropomatric model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the InfantAnthropomatric model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return InfantAnthropomatric the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = InfantAnthropomatric::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
