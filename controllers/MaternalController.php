<?php

namespace app\controllers;

use Yii;
use app\models\Maternal;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\classes\SessionHandler;
use app\models\Users;
use app\classes\RespondentType;

/**
 * MaternalController implements the CRUD actions for Maternal model.
 */
class MaternalController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Maternal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Maternal::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Maternal model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
	 public function multiSelect($model){
    	 
    	if(!empty($model))
    	{
    		if(is_array($model))
    		{
    			$tempArr = $model;
    			$key = array_search("multiselect-all",$tempArr);
    			if($key!==false){
    				array_shift($tempArr);
    			}
    			$model = implode(",",$tempArr);
    		}
    		else
    		{
    			return $model;
    		}
    	}
    	else
    	{
    		$model = null;
    	}
    	return $model;
    }

    /**
     * Creates a new Maternal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
     public function actionCreate()
    {
       
        $motherId = Yii::$app->session->get('motherId');
        $assessmentId = Yii::$app->session->get('assessmentId');
		//$respondentId = Yii::$app->session->get('respondentId');
        $resp = new RespondentType();
        $respondentId =$resp->getTypeId('m');
    	$check = Maternal::findOne(['mother_id' =>  $motherId,'assessment_id' => $assessmentId,'respondent_id'=>$respondentId]);
    	if(isset($check['id']))
    	{
    		$model = $this->findModel($check['id']);
			
    	}
    	else
    	{
    		$model = new Maternal();
			
    	}
    	return $this->render('create', ['model' => $model]);
    }
	
	public function actionAjax()
    {
    	$model = new Maternal();
    	$postForm = $_POST['Maternal'];
    	$returnCreateValue = 'Maternal Created';
    	$returnUpdateValue = 'Maternal Updated';
    	$returnFalseValue = 'Maternal Not Inserted';
        $assessmentId = Yii::$app->session->get('assessmentId');
        $motherId = Yii::$app->session->get('motherId');
		//$respondentId = Yii::$app->session->get('respondentId');
        $resp = new RespondentType();
        $respondentId =$resp->getTypeId('m');
    	if(isset($postForm))
    	{
    		$findModel = Maternal::findOne(['mother_id' =>  $motherId,'assessment_id' => $assessmentId]);
    		if(isset($findModel['id']))
    		{
    			$model = $this->findModel($findModel['id']);
    			$returnValue = $returnUpdateValue;
    			$model->attributes = $postForm;
                
    		}
    		else
    		{
    			$returnValue = $returnCreateValue;
    			$model->attributes = $postForm;
                $model->scale_id=18;
                $model->created_dtm= date('Y:m:d h:i:s');
    		}
    		$model->mother_id =  $motherId;
            $model->assessment_id= $assessmentId;
			$model->respondent_id= $respondentId;
			$model->last_updated_dtm= date('Y:m:d h:i:s');
            $model->updated_by=Users::findOne(['id' => Yii::$app->user->id])->username;
			$model->last_mensural_date = ($model->last_mensural_date)?date('Y-m-d',strtotime($model->last_mensural_date)):null;
			$model->dob_baby = ($model->dob_baby)?date('Y-m-d',strtotime($model->dob_baby)):null;
			$model->sleepin_pills = $this->multiSelect($model->sleepin_pills);
			$model->depression_pills = $this->multiSelect($model->depression_pills);
			$model->anxiety_pills = $this->multiSelect($model->anxiety_pills);
			$model->save(false);
            $scale = new \app\models\Scores;
            $scale->updateScales( $motherId,$assessmentId,$model->scale_id,$model->score);
            
    			
    	}
    	return $returnValue;
    }


    /**
     * Updates an existing Maternal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Maternal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Maternal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Maternal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Maternal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
