<?php

namespace app\controllers;

use Yii;
use app\models\ParentalBondingFather;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\classes\SessionHandler;
use app\models\Users;
/**
 * ParentalBondingFatherController implements the CRUD actions for ParentalBondingFather model.
 */
class ParentalBondingFatherController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ParentalBondingFather models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ParentalBondingFather::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ParentalBondingFather model.
     * @param integer $id
     * @param string $talks_friendly
     * @return mixed
     */
    public function actionView($id, $talks_friendly)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, $talks_friendly),
        ]);
    }

    /**
     * Creates a new ParentalBondingFather model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		$respondentId = Yii::$app->session->get('respondentId');
        $assessmentId = Yii::$app->session->get('assessmentId');
    	$check = ParentalBondingFather::findOne(['respondent_id' => $respondentId,'assessment_id' => $assessmentId]);
    	if(isset($check['id']))
    	{
    		$model = $this->findModel($check['id']);
    	}
    	else
    	{
    		$model = new ParentalBondingFather();
    	}
    	return $this->render('create', ['model' => $model]);
    }
	
	public function actionAjax()
    {
    	$model = new ParentalBondingFather();
    	$postForm = $_POST['ParentalBondingFather'];
    	$returnCreateValue = 'ParentalBondingFather Created';
    	$returnUpdateValue = 'ParentalBondingFather Updated';
    	$returnFalseValue = 'ParentalBondingFather Not Inserted';
    	$respondentId = Yii::$app->session->get('respondentId');
        $assessmentId = Yii::$app->session->get('assessmentId');
        
    	if(isset($postForm))
    	{
    		$findModel = ParentalBondingFather::findOne(['respondent_id' => $respondentId,'assessment_id' => $assessmentId]);
    		if(isset($findModel['id']))
    		{
    			$model = $this->findModel($findModel['id']);
    			$returnValue = $returnUpdateValue;
    			$model->attributes = $postForm;
                
    		}
    		else
    		{
    			$returnValue = $returnCreateValue;
    			$model->attributes = $postForm;
                $model->scale_id=28;
                $model->created_dtm= date('Y:m:d h:i:s');
    		}
    		$model->respondent_id = $respondentId;
            $model->assessment_id= $assessmentId;
			$model->last_updated_dtm= date('Y:m:d h:i:s');
            $model->updated_by=Users::findOne(['id' => Yii::$app->user->id])->username;
    		$model->save();
            $scale = new \app\models\Scores;
            $scale->updateScales($respondentId,$assessmentId,$model->scale_id,$model->score);
            
    			
    	}
    	return $returnValue;
    }


    /**
     * Updates an existing ParentalBondingFather model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param string $talks_friendly
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ParentalBondingFather model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param string $talks_friendly
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ParentalBondingFather model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param string $talks_friendly
     * @return ParentalBondingFather the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ParentalBondingFather::findOne(['id' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
