<?php

namespace app\controllers;

use Yii;
use app\models\Assessment;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\User;
use app\models\Users;

/**
 * TwentyFourthController implements the CRUD actions for Assessment model.
 */
class TwentyfourthMonthController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Assessment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Assessment::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Assessment model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Assessment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
         $model = new Assessment();
        if ($model->load(Yii::$app->request->post())) {
			$model->mother_id = $motherId;
			$model->assessment_type = 'm24';
			$model->save();
        }
		return $this->redirect(['/mother/update', 'id' => $motherId]);
    }

    /**
     * Updates an existing Assessment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
       $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
			$model->save();
        }
		return $this->redirect(['/mother/update', 'id' => $model->mother_id]);
    }

	public function actionAjax($id,$motherId)
    {
    	$model = new Assessment();
    	$postForm = $_POST['Assessment'];
    	$returnCreateValue = 'Assessment Created';
    	$returnUpdateValue = 'Assessment Updated';
    	$returnFalseValue = 'Assessment Not Inserted';
		$userId = Yii::$app->user->id;
		$username = Users::findOne(['id' => $userId])->username;
    	if(isset($postForm))
    	{
    
    		$findModel = Assessment::findOne(['id'=>$id]);
    		if(isset($findModel['id']))
    		{
    			$model = $this->findModel($findModel['id']);
				$model->last_updated_dtm = date('Y-m-d h:i:s');
    			$returnValue = $returnUpdateValue;
    			$model->attributes = $postForm;
    		}
    		else
    		{
    			$returnValue = $returnCreateValue;
    			$model->attributes = $postForm;
    			$aiStarts = Yii::$app->params['autoIncrementStarts'];
    			$aiEnds = Yii::$app->params['autoIncrementEnds'];
    			$getLastRow = Assessment::findBySql("Select * from assessment where id between $aiStarts and $aiEnds order by id desc limit 1")->one();
    			if($getLastRow['id'])
    			{
    			    $model->id = $getLastRow['id']+1;
    			}
    			else
    			{
    			    $model->id = $aiStarts;
    			}
				$model->mother_id = $motherId;
				$model->created_dtm = date('Y-m-d h:i:s');
				$model->last_updated_dtm = date('Y-m-d h:i:s');
				$model->assessment_type = 'm24';
    		}
			$model->assessment_date = ($model->assessment_date)?date('Y-m-d',strtotime($model->assessment_date)):null;
			$model->assessment_date_ac = ($model->assessment_date_ac)?date('Y-m-d',strtotime($model->assessment_date_ac)):null;
			$model->updated_by = $username;
			$model->save();
    	}
    	return $model->id;
    }
    /**
     * Deletes an existing Assessment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Assessment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Assessment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Assessment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
