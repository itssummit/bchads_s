<?php

namespace app\controllers;

use Yii;
use app\models\BabyLikesDislikes;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Scores;
use app\classes\SessionHandler;
use app\models\Users;
use app\classes\RespondentType;

/**
 * BabyLikesDislikesController implements the CRUD actions for BabyLikesDislikes model.
 */
class BabyLikesDislikesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BabyLikesDislikes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => BabyLikesDislikes::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BabyLikesDislikes model.
     * @param integer $id
     * @param integer $idbaby_likes_dislikes
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BabyLikesDislikes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $motherId = Yii::$app->session->get('motherId');
        $assessmentId = Yii::$app->session->get('assessmentId');
		//$respondentId = Yii::$app->session->get('respondentId');
        $resp = new RespondentType();
        $respondentId =$resp->getTypeId('m,ac');
        $check = BabyLikesDislikes::findOne(['mother_id' => $motherId,'assessment_id' => $assessmentId,'respondent_id'=>$respondentId]);
    	if(isset($check['id']))
    	{
    		$model = $this->findModel($check['id']);
    	}
    	else
    	{
    		$model = new BabyLikesDislikes();
    	}
    	return $this->render('create', ['model' => $model]);
    }
   public function actionAjax()
    {
    	$model = new BabyLikesDislikes();
    	$postForm = $_POST['BabyLikesDislikes'];
    	$returnCreateValue = 'BabyLikesDislikes Created';
    	$returnUpdateValue = 'BabyLikesDislikes Updated';
    	$returnFalseValue = 'BabyLikesDislikes Not Inserted';
        $assessmentId = Yii::$app->session->get('assessmentId');
        $motherId = Yii::$app->session->get('motherId');
		//$respondentId = Yii::$app->session->get('respondentId');
        $resp = new RespondentType();
        $respondentId =$resp->getTypeId('m,ac');
    	if(isset($postForm))
    	{
    		$findModel = BabyLikesDislikes::findOne(['mother_id' =>  $motherId,'assessment_id' => $assessmentId,'respondent_id'=>$respondentId]);
    		if(isset($findModel['id']))
    		{
    			$model = $this->findModel($findModel['id']);
    			$returnValue = $returnUpdateValue;
    			$model->attributes = $postForm;
                
    		}
    		else
    		{
    			$returnValue = $returnCreateValue;
    			$model->attributes = $postForm;
                $model->scale_id=15;
				$model->created_dtm= date('Y:m:d h:i:s');
                
    		}
    		$model->mother_id =  $motherId;
            $model->assessment_id= $assessmentId;
			$model->respondent_id= $respondentId;
			$model->last_updated_dtm= date('Y:m:d h:i:s');
            $model->updated_by=Users::findOne(['id' => Yii::$app->user->id])->username;
    		$model->save();
            $scale = new \app\models\Scores;
            $scale->updateScales( $motherId,$assessmentId,$model->scale_id,$model->score);
            
    			
    	}
    	return $returnValue;
    }


 public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
	
	public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    /**
     * Finds the BabyLikesDislikes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param integer $idbaby_likes_dislikes
     * @return BabyLikesDislikes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */


     
    protected function findModel($id)
    {
        if (($model = BabyLikesDislikes::findOne(['id' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
