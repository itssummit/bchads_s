<?php

namespace app\controllers;

use Yii;
use app\models\Sass;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\classes\SessionHandler;
use app\models\Users;
use app\classes\RespondentType;

/**
 * SassController implements the CRUD actions for Sass model.
 */
class SassController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sass models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Sass::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Sass model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Sass model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $motherId = Yii::$app->session->get('motherId');
        $assessmentId = Yii::$app->session->get('assessmentId');
		//$respondentId = Yii::$app->session->get('respondentId');
         $resp = new RespondentType();
        $respondentId =$resp->getTypeId('m');
    	$check = Sass::findOne(['mother_id' => $motherId,'assessment_id' => $assessmentId,'respondent_id' => $respondentId]);
    	if(isset($check['id']))
    	{
    		$model = $this->findModel($check['id']);
    	}
    	else
    	{
    		$model = new Sass();
    	}
    	return $this->render('create', ['model' => $model]);
    }
	
	public function actionAjax()
    {
        $model = new Sass();
        $postForm = $_POST['Sass'];
        $returnCreateValue = 'Sass Created';
        $returnUpdateValue = 'Sass Updated';
        $returnFalseValue = 'Sass Not Inserted';
        $motherId = Yii::$app->session->get('motherId');
        $assessmentId = Yii::$app->session->get('assessmentId');
       // $respondentId = Yii::$app->session->get('respondentId');
         $resp = new RespondentType();
        $respondentId =$resp->getTypeId('m');
        if(isset($postForm))
        {
            $findModel = Sass::findOne(['mother_id' => $motherId,'assessment_id' => $assessmentId,'respondent_id' => $respondentId]);
            if(isset($findModel['id']))
            {
                $model = $this->findModel($findModel['id']);
                $returnValue = $returnUpdateValue;
                $model->attributes = $postForm;
                
            }
            else
            {
                $returnValue = $returnCreateValue;
                $model->attributes = $postForm;
                $model->scale_id=5;
                $model->created_dtm= date('Y:m:d h:i:s');
                
            }
            $model->mother_id = $motherId;
            $model->assessment_id= $assessmentId;
            $model->respondent_id= $respondentId;
            $model->leg_shake = intval($model->leg_shake);
            $model->legs_pain = intval($model->legs_pain);
            if(($model->leg_shake <= 0) && ($model->legs_pain <= 0))
            {
                $model->disease_symptoms = -10;
                $model->walks_relief = -10;
                $model->night_symptoms_increases = -10;
                $model->sleep_prob = -10;
            }
            $model->last_updated_dtm= date('Y:m:d h:i:s');
            $model->updated_by=Users::findOne(['id' => Yii::$app->user->id])->username;
            $model->save(false);
            $scale = new \app\models\Scores;
            $scale->updateScales($motherId,$assessmentId,$model->scale_id,$model->score);
            
                
        }
        return $returnValue;
    }
    /**
     * Updates an existing Sass model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Sass model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Sass model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sass the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sass::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
