<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;
use yii\helpers\Json;
use app\classes\SessionHandler;
use app\models\Services;
use app\models\DbBackupConfig;
use app\models\DbBackupHistory;
/**
 * BackupConfigurationsController implements the CRUD actions for PatientVisit model.
 */
class BackupController extends Controller
{
	
	public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PatientVisit models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionConfiguration()
    {
        return $this->render('configuration');
    }

    public function actionGetbackuphistory(){
        $data = Yii::$app->db->createCommand( 'SELECT * FROM db_backup_history' )->queryAll();
        return json_encode($data);
    }

    public function actionTakebackup(){
        $post = file_get_contents("php://input");
        $data = json_decode($post, true);
        return Services::TriggerBackUp($data['type'],Yii::$app->user->username,Yii::getAlias('@webroot'));
    }

    public function actionGetbackupconfig(){
        $FinalData = [];
        $data = Yii::$app->db->createCommand( '
            SELECT destination_folder FROM db_backup_config
            WHERE type="full"
            ' )->queryOne();
        if(isset($data['destination_folder'])) $FinalData['full_backup'] = $data['destination_folder'];
        $data = Yii::$app->db->createCommand( '
            SELECT destination_folder FROM db_backup_config
            WHERE type="incremental"
            ' )->queryOne();
        if(isset($data['destination_folder'])) $FinalData['incremental_backup'] = $data['destination_folder'];
        return json_encode($FinalData);
    }
    public function actionSavebackupconfig(){
        $post = file_get_contents("php://input");
        $data = json_decode($post, true);
        $data = $data['data'];
        $full_backup = $data['full_backup'];
        $incremental_backup = $data['incremental_backup'];
        $full_status = false;
        $incremental_status = false;
        if(isset($full_backup) && $full_backup !="" && $full_backup != null) {
            $model = DbBackupConfig::findOne(['type'=>'full']);
            $model->destination_folder = $full_backup;
            $full_status = $model->update();
        }
        if(isset($incremental_backup) && $incremental_backup !="" && $incremental_backup != null) {
            $model = DbBackupConfig::findOne(['type'=>'incremental']);
            $model->destination_folder = $incremental_backup;
            $incremental_status = $model->update();
        }
        if($incremental_status || $full_status) return json_encode(array('status'=>'success'));
        else return json_encode(array('status'=>'failure'));
    }

}

