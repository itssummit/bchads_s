<?php

namespace app\controllers;

use Yii;
use app\models\Icmr;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\classes\SessionHandler;
use app\models\Users;
use app\classes\RespondentType;

/**
 * IcmrController implements the CRUD actions for Icmr model.
 */
class IcmrController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Icmr models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Icmr::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Icmr model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Icmr model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $motherId = Yii::$app->session->get('motherId');
        $assessmentId = Yii::$app->session->get('assessmentId');
		//$respondentId = Yii::$app->session->get('respondentId');
        $resp = new RespondentType();
        $respondentId =$resp->getTypeId('m');
    	$check = Icmr::findOne(['mother_id' => $motherId,'assessment_id' => $assessmentId,'respondent_id'=>$respondentId]);
    	if(isset($check['id']))
    	{
    		$model = $this->findModel($check['id']);
    	}
    	else
    	{
    		$model = new Icmr();
    	}
    	return $this->render('create', ['model' => $model]);
    }
	
	public function actionAjax()
    {
    	$model = new Icmr();
    	$postForm = $_POST['Icmr'];
    	$returnCreateValue = 'Icmr Created';
    	$returnUpdateValue = 'Icmr Updated';
    	$returnFalseValue = 'Icmr Not Inserted';
    	$motherId = Yii::$app->session->get('motherId');
        $assessmentId = Yii::$app->session->get('assessmentId');
        //$respondentId = Yii::$app->session->get('respondentId');
        $resp = new RespondentType();
        $respondentId =$resp->getTypeId('m');
    	if(isset($postForm))
    	{
    		$findModel = Icmr::findOne(['mother_id' => $motherId,'assessment_id' => $assessmentId,'respondent_id'=>$respondentId]);
    		if(isset($findModel['id']))
    		{
    			$model = $this->findModel($findModel['id']);
    			$returnValue = $returnUpdateValue;
    			$model->attributes = $postForm;
                
    		}
    		else
    		{
    			$returnValue = $returnCreateValue;
    			$model->attributes = $postForm;
                $model->scale_id=11;
				$model->created_dtm= date('Y:m:d h:i:s');
                
    		}
    		$model->mother_id = $motherId;
            $model->assessment_id= $assessmentId;
			$model->respondent_id= $respondentId;
			$model->last_updated_dtm= date('Y:m:d h:i:s');
            $model->updated_by=Users::findOne(['id' => Yii::$app->user->id])->username;
             $model->bad_words4 = $this->setMinusTen($model->bad_words4);
            $model->bad_words7 = $this->setMinusTen($model->bad_words7);
            $model->bad_words8 = $this->setMinusTen($model->bad_words8);
            $model->threatening4 = $this->setMinusTen($model->threatening4);
            $model->threatening7 = $this->setMinusTen($model->threatening7);
            $model->threatening8 = $this->setMinusTen($model->threatening8);
            $model->threatening_sending_home4 = $this->setMinusTen($model->threatening_sending_home4);
            $model->threatening_sending_home7 = $this->setMinusTen($model->threatening_sending_home7);
            $model->threatening_sending_home8 = $this->setMinusTen($model->threatening_sending_home8);
            $model->send_home4 = $this->setMinusTen($model->send_home4);
            $model->send_home7 = $this->setMinusTen($model->send_home7);
            $model->send_home8 = $this->setMinusTen($model->send_home8);
            $model->finance_prob4 = $this->setMinusTen($model->finance_prob4);
            $model->finance_prob7 = $this->setMinusTen($model->finance_prob7);
            $model->finance_prob8 = $this->setMinusTen($model->finance_prob8);
            $model->fear_look4 = $this->setMinusTen($model->fear_look4);
            $model->fear_look7 = $this->setMinusTen($model->fear_look7);
            $model->fear_look8 = $this->setMinusTen($model->fear_look8);
            $model->avidheya4 = $this->setMinusTen($model->avidheya4);
            $model->avidheya7 = $this->setMinusTen($model->avidheya7);
            $model->avidheya8 = $this->setMinusTen($model->avidheya8);
            $model->udasinathe4 = $this->setMinusTen($model->udasinathe4);
            $model->udasinathe7 = $this->setMinusTen($model->udasinathe7);
            $model->udasinathe8 = $this->setMinusTen($model->udasinathe8);
            $model->social_rights4 = $this->setMinusTen($model->social_rights4);
            $model->social_rights7 = $this->setMinusTen($model->social_rights7);
            $model->social_rights8 = $this->setMinusTen($model->social_rights8);
            $model->nirlakshya4 = $this->setMinusTen($model->nirlakshya4);
            $model->nirlakshya7 = $this->setMinusTen($model->nirlakshya7);
            $model->nirlakshya8 = $this->setMinusTen($model->nirlakshya8);
            $model->personal_needs4 = $this->setMinusTen($model->personal_needs4);
            $model->personal_needs7 = $this->setMinusTen($model->personal_needs7);
            $model->personal_needs8 = $this->setMinusTen($model->personal_needs8);
            $model->decision_taking4 = $this->setMinusTen($model->decision_taking4);
            $model->decision_taking7 = $this->setMinusTen($model->decision_taking7);
            $model->decision_taking8 = $this->setMinusTen($model->decision_taking8);
            $model->restrict4 = $this->setMinusTen($model->restrict4);
            $model->restrict7 = $this->setMinusTen($model->restrict7);
            $model->restrict8 = $this->setMinusTen($model->restrict8);
            $model->harm4 = $this->setMinusTen($model->harm4);
            $model->harm7 = $this->setMinusTen($model->harm7);
            $model->harm_fire4 = $this->setMinusTen($model->harm_fire4);
            $model->harm_fire7 = $this->setMinusTen($model->harm_fire7);
            $model->harm_fire8 = $this->setMinusTen($model->harm_fire8);
            $model->forced4 = $this->setMinusTen($model->forced4);
            $model->forced7 = $this->setMinusTen($model->forced7);
            $model->forced8 = $this->setMinusTen($model->forced8);
            $model->avoid4 = $this->setMinusTen($model->avoid4);
            $model->avoid7 = $this->setMinusTen($model->avoid7);
            $model->avoid8 = $this->setMinusTen($model->avoid8);
            $model->wound4 = $this->setMinusTen($model->wound4);
            $model->wound7 = $this->setMinusTen($model->wound7);
            $model->wound8 = $this->setMinusTen($model->wound8);
            $model->decision_taking11 = $this->setMinusTen($model->decision_taking11);

            $model->harm8 = $this->setMinusTen($model->harm8);
    		$model->save();
            $scale = new \app\models\Scores;
            $scale->updateScales($motherId,$assessmentId,$model->scale_id,$model->score);
            
    			
    	}
    	return $returnValue;
    }

     private function setMinusTen($value){
        if(!intval($value))
        {
            $value = "-10";
        }
        return $value;
    }


    /**
     * Updates an existing Icmr model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Icmr model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Icmr model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Icmr the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Icmr::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
