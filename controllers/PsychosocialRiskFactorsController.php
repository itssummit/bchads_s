<?php

namespace app\controllers;

use Yii;
use app\models\PsychosocialRiskFactors;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\classes\SessionHandler;
use app\models\Users;
use app\classes\RespondentType;

/**
 * PsychosocialRiskFactorsController implements the CRUD actions for PsychosocialRiskFactors model.
 */
class PsychosocialRiskFactorsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PsychosocialRiskFactors models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => PsychosocialRiskFactors::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PsychosocialRiskFactors model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
	public function multiSelect($model){
    	 
    	if(!empty($model))
    	{
    		if(is_array($model))
    		{
    			$tempArr = $model;
    			$key = array_search("multiselect-all",$tempArr);
    			if($key!==false){
    				array_shift($tempArr);
    			}
    			$model = implode(",",$tempArr);
    		}
    		else
    		{
    			return $model;
    		}
    	}
    	else
    	{
    		$model = "-10";
    	}
    	return $model;
    }

    /**
     * Creates a new PsychosocialRiskFactors model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
     public function actionCreate()
    {
       $motherId = Yii::$app->session->get('motherId');
        $assessmentId = Yii::$app->session->get('assessmentId');
		//$respondentId = Yii::$app->session->get('respondentId');
        $resp = new RespondentType();
        $respondentId =$resp->getTypeId('i');
    	$check = PsychosocialRiskFactors::findOne(['mother_id' => $motherId,'assessment_id' => $assessmentId,'respondent_id'=>$respondentId]);
    	if(isset($check['id']))
    	{
    		$model = $this->findModel($check['id']);
    	}
    	else
    	{
    		$model = new PsychosocialRiskFactors();
    	}
    	return $this->render('create', ['model' => $model]);
    }
	
	

    /**
     * Updates an existing PsychosocialRiskFactors model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
	
	public function actionAjax()
    {
    	$model = new PsychosocialRiskFactors();
    	$postForm = $_POST['PsychosocialRiskFactors'];
    	$returnCreateValue = 'PsychosocialRiskFactors Created';
    	$returnUpdateValue = 'PsychosocialRiskFactors Updated';
    	$returnFalseValue = 'PsychosocialRiskFactors Not Inserted';
    	$motherId = Yii::$app->session->get('motherId');
        $assessmentId = Yii::$app->session->get('assessmentId');
		//$respondentId = Yii::$app->session->get('respondentId');
        $resp = new RespondentType();
        $respondentId =$resp->getTypeId('i');
    	if(isset($postForm))
    	{
    		$findModel = PsychosocialRiskFactors::findOne(['mother_id' => $motherId,'assessment_id' => $assessmentId,'respondent_id'=>$respondentId]);
    		if(isset($findModel['id']))
    		{
    			$model = $this->findModel($findModel['id']);
    			$returnValue = $returnUpdateValue;
    			$model->attributes = $postForm;
    		}
    		else
    		{
    			$returnValue = $returnCreateValue;
    			$model->attributes = $postForm;
                $model->scale_id=23;
				$model->created_dtm= date('Y:m:d h:i:s');
    		}
    		$model->mother_id = $motherId;
            $model->assessment_id= $assessmentId;
			$model->respondent_id= $respondentId;
			$model->last_updated_dtm= date('Y:m:d h:i:s');
			$model->stress_issues = $this->multiSelect($model->stress_issues);
            $model->updated_by=Users::findOne(['id' => Yii::$app->user->id])->username;
            $model->lack_care = $this->setMinusTen($model->lack_care);
           // $model->work = $this->setMinusTen($model->work);
            $model->relation = $this->setMinusTen($model->relation);
           // $model->others = $this->setMinusTen($model->others);
            $model->others_desc = $this->setMinusTen($model->others_desc);
          //  $model->financial = $this->setMinusTen($model->financial);
           // $model->spouse = $this->setMinusTen($model->spouse);
           // $model->in_laws = $this->setMinusTen($model->in_laws);
           // $model->parents = $this->setMinusTen($model->parents);
            $model->violence_experience = $this->setMinusTen($model->violence_experience);
           // $model->psychological = $this->setMinusTen($model->psychological);
            //$model->physical = $this->setMinusTen($model->physical);
            //$model->sexual = $this->setMinusTen($model->sexual);
            $model->anxiety_issues = $this->setMinusTen($model->anxiety_issues);
            $model->mood_issues = $this->setMinusTen($model->mood_issues);
            $model->suicidal_ideation = $this->setMinusTen($model->suicidal_ideation);
            $model->delivery_and_postpartum = $this->setMinusTen($model->delivery_and_postpartum);
            $model->health_problem_of_familymember = $this->setMinusTen($model->health_problem_of_familymember);
            /*
             $model->complications_related_to_labour_delivery_childbirth = $this->setMinusTen($model->complications_related_to_labour_delivery_childbirth);
             $model->complications_related_to_labour_delivery_childbirth1 = $this->setMinusTen($model->complications_related_to_labour_delivery_childbirth1);
            $model->complications_related_to_labour_delivery_childbirth2 = $this->setMinusTen($model->complications_related_to_labour_delivery_childbirth2);

             $model->obstetric_trauma = $this->setMinusTen($model->obstetric_trauma);
            $model->poor_obg_outcome = $this->setMinusTen($model->poor_obg_outcome);
            $model->poor_obg_outcome1 = $this->setMinusTen($model->poor_obg_outcome1);
            $model->poor_obg_outcome2 = $this->setMinusTen($model->poor_obg_outcome2);

            $model->gender_of_the_infant = $this->setMinusTen($model->gender_of_the_infant);*/
            $model->poor_pregnancy = $this->setMinusTen($model->poor_pregnancy);
            $model->traumatic_events = $this->setMinusTen($model->traumatic_events);
           
            
           
            
           
            $model->substance_use_spouse = $this->setMinusTen($model->substance_use_spouse);
            $model->delivery_postpartum = $this->setMinusTen($model->delivery_postpartum);
            $model->fear_about_pregnancy = $this->setMinusTen($model->fear_about_pregnancy);
            $model->fear_about_childbirth = $this->setMinusTen($model->fear_about_childbirth);
            $model->general_worry = $this->setMinusTen($model->general_worry);
            $model->feeling_low = $this->setMinusTen($model->feeling_low);
            $model->loss_of_interest = $this->setMinusTen($model->loss_of_interest);
            $model->crying_spells = $this->setMinusTen($model->crying_spells);
            $model->lifetime_attempt = $this->setMinusTen($model->lifetime_attempt);
            $model->ideation_during_past_current_pregnancy = $this->setMinusTen($model->ideation_during_past_current_pregnancy);
            $model->postpartum = $this->setMinusTen($model->postpartum);
            $model->neonatal_death = $this->setMinusTen($model->neonatal_death);
            $model->still_birth = $this->setMinusTen($model->still_birth);
            $model->any_congenital_anomalies = $this->setMinusTen($model->any_congenital_anomalies);
            $model->death_of_close_one = $this->setMinusTen($model->death_of_close_one);
            $model->major_loss = $this->setMinusTen($model->major_loss);
    		$model->save();
            $scale = new \app\models\Scores;
            $scale->updateScales($motherId,$assessmentId,$model->scale_id,$model->score);
    	}
    	return $returnValue;
    }

    private function setMinusTen($value){
        if($value == null || $value == "0")
        {
            $value = "-10";
        }
        return $value;
    }


    /**
     * Deletes an existing PsychosocialRiskFactors model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PsychosocialRiskFactors model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PsychosocialRiskFactors the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PsychosocialRiskFactors::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
