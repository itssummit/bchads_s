<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use app\models\User;
use app\models\Users;
use app\models\Assessment;
use app\classes\SessionHandler;
/**
 * DatasyncController implements all offline data sync operations
 */
class DatasyncController extends Controller
{
    public $_ServerDatabase = 'server';
    public $_LocalDatabase = 'local';
    public $_UserId;
	
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $this->_UserId = Yii::$app->user->id;
        if(!isset($this->_UserId) || $this->_UserId == 0) header('Location: '.Yii::$app->homeUrl);
            
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionStart()
    {   
        $c_local = $this->localConnection();
        $c_server = $this->serverConnection();
        $c_local->open();
        $c_server->open();
		 

        //All assessment table operations
        $this->updateMotherTable($c_local, $c_server);// Offline allowed operations : update 

        //All table data sync
        $tablesToSync = [
            "member",
            "assessment",
            "edinburgh_postnatal_depression",
            "psychosocial_assessment",
            "psychosocial_risk_factors",
            "phychological_violence",
            "suicide_questions",
            "stai",
            "somatic_symptoms",
            "shared_caregiving",
            "sass",
            "ptsd_checklist",
            "pics",
            "scores",
            "patient_health",
            "parental_bonding_mother",
            "parental_bonding_father",
            "other_aspects",
            "multidimentional",
            "maternal",
            "life_events",
            "infant_immunization",
            "infant_health",
            "infant_anthropomatric",
            "icmr",
            "conflict_tactics_scale",
            "cbcl",
            "child_growth",
            "child_ecbq",
            "breastfeeding",
            "bitsea",
            "baby_likes_dislikes"
        ];

        foreach ($tablesToSync as $key => $table) {
            $this->syncTableDataByName($c_local, $c_server, $table);
        }
        
        //After all table updations then update server sync table
        $this->updateServerLastSyncDate($c_server);
		
		return true;
       
    }

    // Dynamic function for all table sync

    protected function syncTableDataByName($c_local, $c_server, $tableName){
        $lastSyncDate = $this->getLastSyncDateByTableName($c_local, $tableName);
        $localUpdatedRows = $c_local->createCommand("SELECT * FROM ".$tableName." where last_updated_dtm > '".$lastSyncDate."'")->queryAll();
        $fullSyncQuery = "";
        foreach($localUpdatedRows as $key => $row) {
            $find_server_row = $this->findServerRow($c_server, $tableName, $row);
            if(isset($find_server_row['id'])){
                $row['id'] = $find_server_row['id'];
                if($row['last_updated_dtm'] > $find_server_row['last_updated_dtm'])
                    $fullSyncQuery .= $this->prepareUpdateSqlQuery($tableName, $row);
            }else{
                $fullSyncQuery .= $this->prepareInsertSqlQuery($this->_ServerDatabase, $tableName, $row);
            }
        }

        $this->executeSqlQuery($c_server, $fullSyncQuery);  
        $this->cleanAndCloneDataFromServer($c_local, $c_server, $tableName);        
        $this->updateLastSyncDate($c_local, $tableName);
    }

    protected function updateMotherTable($c_local, $c_server){
        $lastSyncDate = $this->getLastSyncDateByTableName($c_local, 'mother');
        $localUpdatedMothers = $c_local->createCommand("SELECT * FROM mother where last_updated_dtm > '".$lastSyncDate."'")->queryAll();
        $fullSyncQuery = "";
        foreach($localUpdatedMothers as $key => $mother) {
            $findMother = $c_server->createCommand("SELECT * FROM mother where id = '".$mother['id']."'")->queryOne();
            if($mother['last_updated_dtm'] > $findMother['last_updated_dtm']){
                $fullSyncQuery .= $this->prepareUpdateSqlQuery('mother', $mother);
            }
        }

        $this->executeSqlQuery($c_server, $fullSyncQuery);   
        $this->cleanAndCloneDataFromServer($c_local, $c_server, 'mother');
        $this->updateLastSyncDate($c_local, 'mother'); 
    }

    // Common function across tables

    protected function findServerRow($c_server, $tableName, $row){        
        $query = "SELECT * FROM ".$tableName." where id = '".$row['id']."' and created_dtm='".$row['created_dtm']."'";
        return $c_server->createCommand($query)->queryOne();
    }   

    protected function cleanAndCloneDataFromServer($c_local, $c_server, $tableName){
        $fullSyncQuery = "TRUNCATE TABLE ".$tableName.";";
        $all_rows = $c_server->createCommand("SELECT * FROM ".$tableName)->queryAll();
        foreach($all_rows as $key => $row) {
            $fullSyncQuery .= $this->prepareInsertSqlQuery($this->_LocalDatabase, $tableName, $row);
        }
        $this->executeSqlQuery($c_local, $fullSyncQuery);
    }

    protected function prepareInsertSqlQuery($databaseType, $tableName, $dataToUpdate){
        if($databaseType == $this->_ServerDatabase) unset($dataToUpdate['id']);
        $keys = array_keys($dataToUpdate);
        $values = array_values($dataToUpdate);
        return "INSERT INTO ".$tableName." (".implode(", ",$keys).") VALUES ('".implode("', '",$values)."');";
    }
    
    protected function prepareUpdateSqlQuery($tableName, $dataToUpdate){
        $keyValues = [];
        foreach ($dataToUpdate as $key => $value) {
            if($key != 'id') $keyValues[] = $key." = '".$value."'";
        }
        return "UPDATE ".$tableName." SET ".implode(", ",$keyValues)." WHERE id = ".$dataToUpdate['id'].";";
    }

    protected function executeSqlQuery($dataBaseConnetion,$sqlQuery){
        return $dataBaseConnetion->createCommand($sqlQuery)->execute();
    }
    
    protected function getLastSyncDateByTableName($c_local, $tableName){
        $data = $c_local->createCommand("SELECT last_sync_date FROM offline_data_sync where table_name='".$tableName."'")->queryOne();
        return (isset($data['last_sync_date'])) ? $data['last_sync_date']:null;
    }

    protected function updateLastSyncDate($c_local, $tableName){
        $c_local->createCommand("UPDATE offline_data_sync SET updated_user_id='".$this->_UserId."', last_sync_date='".date("Y-m-d H:i:s")."' WHERE table_name = '".$tableName."'")->execute();
    }

    protected function updateServerLastSyncDate($c_local){
        $c_local->createCommand("INSERT INTO  offline_data_sync (updated_user_id,last_sync_date) VALUES ('".$this->_UserId."','".date("Y-m-d H:i:s")."')")->execute();
    }

    protected function localConnection(){
        return new \yii\db\Connection([
        'dsn' => 'mysql:host=localhost:3306;dbname=bchads',
        'username' => 'root',
        'password' => 'admin123',
        ]);
    }
    
    protected function serverConnection(){
        return new \yii\db\Connection([
        'dsn' => 'mysql:host=localhost:3306;dbname=bchads_server',
        'username' => 'root',
        'password' => 'admin123',
        ]);
    }

}
