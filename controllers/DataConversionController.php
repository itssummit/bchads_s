<?php


namespace app\controllers;

use Yii;
use app\models\Mother;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\classes\app\classes;
use yii\helpers\ArrayHelper; 
use app\classes\ExportData;

 class DataconversionController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

     
    public function actionIndex(){
        return $this->render('index');
    }


    public function actionRetrieveC(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $post = file_get_contents("php://input");
        $data = json_decode($post, true);
       
         $data = array(
             "tableNames"=>"mother,member"
         );
       // $ids = (isset($data["mother_id"]) && $data["mother_id"] != "")?array(0=>$data["mother_id"]):array();
        $MotherIds= Yii::$app->db->createCommand("SELECT * from mother")->queryAll();
        $ids= ArrayHelper::getColumn($MotherIds,'id');
        //$ids=array(0=>$MotherIds);
        
        $FormatedData = $this->prepareDataByTableNames($data["tableNames"],$ids);
        $AllRows = $this->convertDataToSingleRow($FormatedData);
        //print_r($this->convertDataToSingleRow($FormatedData));exit();
        return $this->convertDataToSingleRow($FormatedData);
       // return $this->AdjustKeysForAllRows($AllRows);
    }

    protected function prepareDataByTableNames($tableNames,$ids){
        $tables = explode(',', $tableNames);
         $select= " id as MotherId, 
                       first_name as Firstname,
                       last_name as Lastname,
                       age as AGE
                     ";
        $query = "SELECT * FROM mother";
        if(count($ids) > 0) $query .= " where id IN (".implode(',', $ids).")";
        $subjects = $this->executeQueryAll($query);
        $FormatedData = array();
        foreach ($subjects as $subjectkey => $subject) {
            $subjectData = array();
            foreach ($tables as $key => $tname) {
                switch ($tname) {
                    case 'mother':{
                        $subjectData["details"] = $this->executeQueryOne("SELECT $select FROM mother where id=".$subject['id']);
                        break;
                    }
                     case 'member':{
                        $subjectData["member"] = $this->queryByTableNameAndMotherId("member",$subject['id']);
                        break;
                    }
                    case 'assessment':{
                        $assessments = $this->queryBySimpleTableNameAndMotherId("assessment",$subject['id']);
                       foreach ($assessments as $key => $assessment) {
                            $subjectData["assessments"][$key] = $this->PrepareAssessmentData($assessment,$tables);
                        }
                        break;
                    }
                     default:
                        break;
                }
            }
            $FormatedData[] = $subjectData;
        }
        return $FormatedData;
    }

    protected function convertDataToSingleRow($allRowsFormatedData){
        $AllRows = array();
        foreach ($allRowsFormatedData as $subjectKey => $FormatedRowData) {
            $SingleRow = array();
            foreach ($FormatedRowData as $key1 => $value1) {
                switch ($key1) {
                    case 'details':{
                        $SingleRow = $this->BaseLevelDataConversion($SingleRow,$value1);
                        break;

                    }
                    case 'member':{
                        $SingleRow = $this->SingleLevelDataConversionWithoutKey($SingleRow,$value1);
                        break;
                    }
                    case 'assessments':{
                        foreach ($value1 as $key3 => $value3){
                            $key3++;
                            if(isset($value3["details"])) 
                                $SingleRow = $this->SingleLevelDataConversion($SingleRow,$value3["details"],$key3);
                            if(isset($value3["edinburgh_postnatal_depression"])) 
                                $SingleRow = $this->MultiLevelDataConversion($SingleRow,$value3["edinburgh_postnatal_depression"],$key3);
                            if(isset($value3["baby_likes_dislikes"])) 
                                $SingleRow = $this->MultiLevelDataConversion($SingleRow,$value3["baby_likes_dislikes"],$key3);
                            if(isset($value3["bitsea"])) 
                                $SingleRow = $this->MultiLevelDataConversion($SingleRow,$value3["bitsea"],$key3);
                            if(isset($value3["breastfeeding"])) 
                                $SingleRow = $this->MultiLevelDataConversion($SingleRow,$value3["breastfeeding"],$key3);
                            if(isset($value3["cbcl"])) 
                                $SingleRow = $this->MultiLevelDataConversion($SingleRow,$value3["cbcl;"],$key3);
                            if(isset($value3["child_ecbq"])) 
                                $SingleRow = $this->MultiLevelDataConversion($SingleRow,$value3["child_ecbq"],$key3);
                             if(isset($value3["child_growth"])) 
                                $SingleRow = $this->MultiLevelDataConversion($SingleRow,$value3["child_growth"],$key3);
                            if(isset($value3["conflict_tactics_scale"])) 
                                $SingleRow = $this->MultiLevelDataConversion($SingleRow,$value3["conflict_tactics_scale"],$key3);
                            if(isset($value3["icmr"])) 
                                $SingleRow = $this->MultiLevelDataConversion($SingleRow,$value3["icmr"],$key3);
                            if(isset($value3["infant_anthropomatric"])) 
                                $SingleRow = $this->MultiLevelDataConversion($SingleRow,$value3["infant_anthropomatric"],$key3);
                            if(isset($value3["infant_health"])) 
                                $SingleRow = $this->MultiLevelDataConversion($SingleRow,$value3["infant_health"],$key3);
                            if(isset($value3["infant_immunization"])) 
                                $SingleRow = $this->MultiLevelDataConversion($SingleRow,$value3["infant_immunization"],$key3);
                             if(isset($value3["life_events"])) 
                                $SingleRow = $this->MultiLevelDataConversion($SingleRow,$value3["life_events"],$key3);
                            if(isset($value3["maternal"])) 
                                $SingleRow = $this->MultiLevelDataConversion($SingleRow,$value3["maternal"],$key3);
                            if(isset($value3["multidimentionalother_aspects"])) 
                                $SingleRow = $this->MultiLevelDataConversion($SingleRow,$value3["multidimentionalother_aspects"],$key3);
                            if(isset($value3["other_aspects"])) 
                                $SingleRow = $this->MultiLevelDataConversion($SingleRow,$value3["other_aspects"],$key3);
                            if(isset($value3["parental_bonding_father"])) 
                                $SingleRow = $this->MultiLevelDataConversion($SingleRow,$value3["parental_bonding_father"],$key3);
                            if(isset($value3["parental_bonding_mother"])) 
                                $SingleRow = $this->MultiLevelDataConversion($SingleRow,$value3["parental_bonding_mother"],$key3);
                            if(isset($value3["patient_health"])) 
                                $SingleRow = $this->MultiLevelDataConversion($SingleRow,$value3["patient_health"],$key3);
                            if(isset($value3["phychological_violence"])) 
                                $SingleRow = $this->MultiLevelDataConversion($SingleRow,$value3["phychological_violence"],$key3);
                            if(isset($value3["pics"])) 
                                $SingleRow = $this->MultiLevelDataConversion($SingleRow,$value3["pics"],$key3);
                            if(isset($value3["psychosocial_assessment"])) 
                                $SingleRow = $this->MultiLevelDataConversion($SingleRow,$value3["psychosocial_assessment"],$key3);
                            if(isset($value3["psychosocial_risk_factors"])) 
                                $SingleRow = $this->MultiLevelDataConversion($SingleRow,$value3["psychosocial_risk_factors"],$key3);
                            if(isset($value3["ptsd_checklist"])) 
                                $SingleRow = $this->MultiLevelDataConversion($SingleRow,$value3["ptsd_checklist"],$key3);
                            if(isset($value3["sass"])) 
                                $SingleRow = $this->MultiLevelDataConversion($SingleRow,$value3["sass"],$key3);
                            if(isset($value3["shared_caregiving"])) 
                                $SingleRow = $this->MultiLevelDataConversion($SingleRow,$value3["shared_caregiving"],$key3);
                            if(isset($value3["somatic_symptoms"])) 
                                $SingleRow = $this->MultiLevelDataConversion($SingleRow,$value3["somatic_symptoms"],$key3);
                            if(isset($value3["stai"])) 
                                $SingleRow = $this->MultiLevelDataConversion($SingleRow,$value3["stai"],$key3);
                            if(isset($value3["suicide_questions"])) 
                                $SingleRow = $this->MultiLevelDataConversion($SingleRow,$value3["suicide_questions"],$key3);
                            if(isset($value3["wooley_depression"])) 
                                $SingleRow = $this->MultiLevelDataConversion($SingleRow,$value3["wooley_depression"],$key3);
                          
                        }
                        break;
                    }

                     default:
                        break;
                }
            }
            $AllRows[] = $SingleRow;
        }
        return $AllRows;
    }


     protected function PrepareAssessmentData($assessment,$tables){
        $finalData = array('details' => $assessment);
        $assessment_id = $assessment['id'];
        if(in_array("edinburgh_postnatal_depression", $tables))
            $finalData['edinburgh_postnatal_depression'] = $this->queryByTableNameAndAssessmentId('edinburgh_postnatal_depression',$assessment_id);
        if(in_array("baby_likes_dislikes", $tables))
            $finalData['baby_likes_dislikes'] = $this->queryByTableNameAndAssessmentId('baby_likes_dislikes',$assessment_id);
        if(in_array("bitsea", $tables))
            $finalData['bitsea'] = $this->queryByTableNameAndAssessmentId('bitsea',$assessment_id);
        if(in_array("breastfeeding", $tables))
            $finalData['breastfeeding'] = $this->queryByTableNameAndAssessmentId('breastfeeding',$assessment_id);
        if(in_array("cbcl", $tables))
            $finalData['cbcl'] = $this->queryByTableNameAndAssessmentId('cbcl',$assessment_id);
        if(in_array("child_ecbq", $tables))
            $finalData['child_ecbq'] = $this->queryByTableNameAndAssessmentId('child_ecbq',$assessment_id);
        if(in_array("child_growth", $tables))
            $finalData['child_growth'] = $this->queryByTableNameAndAssessmentId('child_growth',$assessment_id);
        if(in_array("conflict_tactics_scale", $tables))
            $finalData['conflict_tactics_scale'] = $this->queryByTableNameAndAssessmentId('conflict_tactics_scale',$assessment_id);
        if(in_array("child_growth", $tables))
            $finalData['child_growth'] = $this->queryByTableNameAndAssessmentId('child_growth',$assessment_id);
        if(in_array("icmr", $tables))
            $finalData['icmr'] = $this->queryByTableNameAndAssessmentId('icmr',$assessment_id);
        if(in_array("infant_anthropomatric", $tables))
            $finalData['infant_anthropomatric'] = $this->queryByTableNameAndAssessmentId('infant_anthropomatric',$assessment_id);
        if(in_array("infant_health", $tables))
            $finalData['infant_health'] = $this->queryByTableNameAndAssessmentId('infant_health',$assessment_id);
        if(in_array("infant_immunization", $tables))
            $finalData['infant_immunization'] = $this->queryByTableNameAndAssessmentId('infant_immunization',$assessment_id);
        if(in_array("life_events", $tables))
            $finalData['life_events'] = $this->queryByTableNameAndAssessmentId('life_events',$assessment_id);
        if(in_array("maternal", $tables))
            $finalData['maternal'] = $this->queryByTableNameAndAssessmentId('maternal',$assessment_id);
        if(in_array("multidimentional", $tables))
            $finalData['multidimentional'] = $this->queryByTableNameAndAssessmentId('multidimentional',$assessment_id);
        if(in_array("other_aspects", $tables))
            $finalData['other_aspects'] = $this->queryByTableNameAndAssessmentId('other_aspects',$assessment_id);
        if(in_array("parental_bonding_father", $tables))
            $finalData['parental_bonding_father'] = $this->queryByTableNameAndAssessmentId('parental_bonding_father',$assessment_id);
        if(in_array("parental_bonding_mother", $tables))
            $finalData['parental_bonding_mother'] = $this->queryByTableNameAndAssessmentId('parental_bonding_mother',$assessment_id);
        if(in_array("patient_health", $tables))
            $finalData['patient_health'] = $this->queryByTableNameAndAssessmentId('patient_health',$assessment_id);
        if(in_array("phychological_violence", $tables))
            $finalData['phychological_violence'] = $this->queryByTableNameAndAssessmentId('phychological_violence',$assessment_id);
        if(in_array("pics", $tables))
            $finalData['pics'] = $this->queryByTableNameAndAssessmentId('pics',$assessment_id);
        if(in_array("psychosocial_assessment", $tables))
            $finalData['psychosocial_assessment'] = $this->queryByTableNameAndAssessmentId('psychosocial_assessment',$assessment_id);
        if(in_array("psychosocial_risk_factors", $tables))
            $finalData['psychosocial_risk_factors'] = $this->queryByTableNameAndAssessmentId('psychosocial_risk_factors',$assessment_id);
        if(in_array("ptsd_checklist", $tables))
            $finalData['ptsd_checklist'] = $this->queryByTableNameAndAssessmentId('ptsd_checklist',$assessment_id);
        if(in_array("sass", $tables))
            $finalData['sass'] = $this->queryByTableNameAndAssessmentId('sass',$assessment_id);
        if(in_array("shared_caregiving", $tables))
            $finalData['shared_caregiving'] = $this->queryByTableNameAndAssessmentId('shared_caregiving',$assessment_id);
        if(in_array("somatic_symptoms", $tables))
            $finalData['somatic_symptoms'] = $this->queryByTableNameAndAssessmentId('somatic_symptoms',$assessment_id);
        if(in_array("stai", $tables))
            $finalData['stai'] = $this->queryByTableNameAndAssessmentId('stai',$assessment_id);
        if(in_array("suicide_questions", $tables))
            $finalData['suicide_questions'] = $this->queryByTableNameAndAssessmentId('suicide_questions',$assessment_id);
        if(in_array("wooley_depression", $tables))
            $finalData['wooley_depression'] = $this->queryByTableNameAndAssessmentId('wooley_depression',$assessment_id);

         return $finalData;
    }

     protected function AdjustKeysForAllRows($AllRows){

        $headers = array();
        foreach ($AllRows as $key => $row) {            
            $headers = array_unique(array_merge($headers,array_keys($row)), SORT_REGULAR);
        }

        foreach ($AllRows as $key1 => $row) {
            foreach ($headers as $key2 => $name) {
                if(!isset($row[$name])) $AllRows[$key1][$name] = "";
            }          
            ksort($AllRows[$key1]);
        }
        return $AllRows;
    }


     protected function BaseLevelDataConversion($SingleRow,$data){
        foreach ($data as $key2 => $value2){
           $SingleRow[$key2] = $value2;
        }
        return $SingleRow;
    }

    protected function SingleLevelDataConversion($SingleRow,$data,$key3){
        foreach ($data as $key4 => $value4) {
            $SingleRow[$key4."_".$key3] = $value4;
        }
        return $SingleRow;
    }

    protected function SingleLevelDataConversionWithoutKey($SingleRow,$data){
        foreach ($data as $key3 => $value3) {
            $key3++;
            foreach ($value3 as $key4 => $value4) {
                $SingleRow[$key4."_".$key3] = $value4;
            }
        }
        return $SingleRow;
    }
    protected function MultiLevelDataConversion($SingleRow,$data,$key3){
        foreach ($data as $key4 => $value4) {
            $key4++;
            foreach ($value4 as $key5 => $value5) {
                $SingleRow[$key5."_".$key3."_".$key4] = $value5;
            }
        }
        return $SingleRow;
    }

     protected function executeQueryAll($query){
        return Yii::$app->db->createCommand($query)->queryAll();
    }
     
      protected function executeQueryOne($query){
        return Yii::$app->db->createCommand($query)->queryOne();
    } 
    
    protected function queryByTableNameAndMotherId($table,$id){
        return $this->executeQueryAll("SELECT * FROM ".$table." where mother_id=".$id);
    } 
     protected function queryBySimpleTableNameAndMotherId($table,$id){
        return $this->executeQueryAll("SELECT * FROM ".$table." where mother_id=".$id);
    } 
    protected function queryByTableNameAndAssessmentId($table,$assessment_id){
        return $this->executeQueryAll("SELECT * FROM ".$table." where assessment_id=".$assessment_id);
    } 
    
    
   
        public function actionRetrieve(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model= new ExportData;
        $data= $model->get();
        return $data;
    
    }   
    
          
}
?>