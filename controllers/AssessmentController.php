<?php

namespace app\controllers;

use Yii;
use app\models\Assessment;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\classes\SessionHandler;
use yii\helpers\Json;
use app\models\Scale;
use app\models\Scores;
use app\models\Users;
use app\models\Member;
use app\models\Mother;
use app\models\Relationship;
use app\classes\AssessmentType;
use app\classes\RespondentType;
/**
 * AssessmentController implements the CRUD actions for Assessment model.
 */
class AssessmentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Assessment models.
     * @return mixed
     */
    public function actionIndex($motherId)
    {
		$sixthModel = Assessment::findOne(['mother_id' => $motherId,'assessment_type' => 'm6']);
		if(!isset($sixthModel->id))
		{
			$sixthModel = new Assessment();
			$sixthModel->assessment_type = 'm6';
			$sixthModel->mother_id = $motherId;
		}
		$twelvethModel = Assessment::findOne(['mother_id' => $motherId,'assessment_type' => 'm12']);
		if(!isset($twelvethModel->id))
		{
			$twelvethModel = new Assessment();
			$twelvethModel->assessment_type = 'm12';
			$twelvethModel->mother_id = $motherId;
		}
		$twentyfourthModel = Assessment::findOne(['mother_id' => $motherId,'assessment_type' => 'm24']);
		if(!isset($twentyfourthModel))
		{
			$twentyfourthModel = new Assessment();
			$twentyfourthModel->assessment_type = 'm24';
			$twentyfourthModel->mother_id = $motherId;
		}
		return $this->renderAjax('index',['sixthModel' => $sixthModel,'twelvethModel' => $twelvethModel,'twentyfourthModel' => $twentyfourthModel]);
    }

    /**
     * Displays a single Assessment model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
	


    /**
     * Creates a new Assessment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Assessment();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Assessment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Assessment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
    public function actionAddassessment($edit=null)
   	{
       	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
       	
       	$motherId = Yii::$app->session->get('motherId');
        if(!$motherId)
        {
            $session = new SessionHandler();
            $motherId = $session->getMotherId();
        }
       	$type = $params['type'];
		/*$assessment_date = $params['assessment_date'];
		$place_of_assesment = $params['place_of_assesment'];
		$assessment_by  = $params['assessment_by'];$params = Json::decode(file_get_contents("php://input"),true);
		$caregiver = $params['caregiver'];
		$relation_with_child = $params['relation_with_child'];
		$assessment_date_ac = $params['assessment_date_ac'];
		$place_of_assessment_ac = $params['place_of_assessment_ac'];
		$assessment_by_ac = $params['assessment_by_ac'];*/
		
		$model = Assessment::findOne(['mother_id' => $motherId ,'assessment_type' => $type]);
		if(isset($model['id']))
		{
			return false;
		}
		else
		{
					$assessmentModal = new Assessment();
					$assessmentModal->mother_id = $motherId;
					$assessmentModal->assessment_type = $type;
					$motherName=Mother::findOne(['id' => $motherId])->mother_code;
					$assessmentModal->respondent = $motherName;
					$assessmentModal->respondent_id = $motherName;
					//$assessmentModal->place_of_assesment = $place_of_assesment;
					//$assessmentModal->assessment_by = $assessment_by;
					//$assessmentModal->caregiver = $caregiver;
					//$assessmentModal->relation_with_child = $relation_with_child;
					
					$assessmentModal->place_of_assessment_ac = $place_of_assessment_ac;
					$assessmentModal->assessment_by_ac = $assessment_by_ac;
					/*$user=Users::findOne(['id' => Yii::$app->user->id])->username;
                    
                    $member=Member::findOne(['id' => $respondentId]);
					if ($respondent==$user){
						$assessmentModal->relation_type="Interviewer";
						$assessmentModal->relationship_id=2;
					}
					else if ($respondentId==$motherId && $respondent!=$user && $respondent==$motherName ){
						$assessmentModal->relation_type="Self";
						$assessmentModal->relationship_id=1;
					}
					
					else if($respondentId==$member['id']){
						$assessmentModal->relation_type=$member['relation_type'];
						$assessmentModal->relationship_id=3;
					}*/
					$assessmentModal->assessment_date = date('Y-m-d');
					$assessmentModal->assessment_date_ac = date('Y-m-d');
					$assessmentModal->visit_date = date('Y-m-d');
					$assessmentModal->created_dtm = date('Y-m-d H:i:s');
					$assessmentModal->last_updated_dtm = date('Y-m-d H:i:s');
					$assessmentModal->updated_by = Users::findOne(['id' => Yii::$app->user->id])->username;
					$assessmentModal->save(false);
					return $assessmentModal;
		}
		
	}
   public function actionAllassessment()
   {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $motherId = Yii::$app->session->get('motherId');
        if(!$motherId)
         {
             $session = new SessionHandler();
             $motherId = $session->getMotherId();
         }
       	     $model = Yii::$app->db->createCommand("Select a.*,DATE_FORMAT(a.visit_date,'%d-%b-%y') as visit_date FROM  assessment as a 
                                                      INNER JOIN mother as m on m.id=a.mother_id where m.id= $motherId 
                                                     order by a.id desc" )->queryAll();
            return $model;
         
    }
    public function actionAllrespondent(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
         $motherId = Yii::$app->session->get('motherId');
         if(!$motherId)
         {
             $session = new SessionHandler();
             $motherId = $session->getMotherId();
         }
         //$model = Yii::$app->db->createCommand("SELECT m.first_name, m.last_name,m.id from mother as m where m.id=$motherId UNION SELECT me.first_name,me.last_name,me.id from member as me where me.mother_id=$motherId ")->queryAll();
         $model = Yii::$app->db->createCommand("SELECT m.mother_code as code,m.id as id  from mother as m where m.id=$motherId
												UNION
												SELECT u.username as code ,u.id from user as u")->queryAll();
       
        return $model;
        
    }

     public function actionScaleform($aid)
    {
		 $motherId = Yii::$app->session->get('motherId');
        Yii::$app->session->set('displaySequence',null);
    	$model = Assessment::findOne(['id' => $aid]);
    	Yii::$app->session->set('assessmentId',intval($aid));
    	Yii::$app->session->set('motherId',$model['mother_id']);
        Yii::$app->session->set('assessmentType',$model['assessment_type']);
		Yii::$app->session->set('respondentId',null);
        Yii::$app->session->set('respondentType',null);
        //Yii::$app->session->set('relationshipId',$model['relationship_id']);
        //Yii::$app->session->set('navigationMenus',true);
        return $this->render('scaleform');
    }

    public function actionAllscales(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $params = Json::decode(file_get_contents("php://input"),true);
        $assessType = Yii::$app->session->get('assessmentType');
		$motherId = Yii::$app->session->get('motherId');
        $assessmentId = Yii::$app->session->get('assessmentId');
        $respType = new RespondentType();
        /**
		-----The below lines are commented as there is no need for displaying scales
		based on respondent and members. This can be uncommented when there is a change 
		in requirement to get these scales based on respondent and/or members---------
		*/
		
		//$respondentType = Yii::$app->session->get('respondentType');
        //$respondentId = Yii::$app->session->get('respondentId');
		//$relationshipId = Yii::$app->session->get('relationshipId');
		
		/*
		 $temp = ['motherId' => $motherId,'assessmentId' => $assessmentId,'assessmentType' => $assessType,
                 'respondentId' => $respondentId,'model' => null];
		
		*/
        
        $temp = ['motherId' => $motherId,'assessmentId' => $assessmentId,'assessmentType' => $assessType,
                 'model' => null];
        $arr = [];

        /*$assessmentType = new AssessmentType();
        $str = $assessmentType->getType($assessType,$relationshipId);
        if($str)*/
         if(true)
		{
            $temp['model'] = Yii::$app->db->createCommand("SELECT * from scale where table_name is not null and $assessType not in ('x') ")->queryAll();
            $modelScore = $temp['model'];
            for($i = 0;$i < count($modelScore);$i++)
            {
            	$tableName = str_replace("-","_",$modelScore[$i]['table_name']);
            	if($temp['model'][$i][$assessType] == 'm,ac')
                {
                	$scaleName = $temp['model'][$i]['scale_name'];
					$temp['model'][$i][$assessType] = 'm,ac';
					$temp['model'][$i]['respId'] = $respType->getTypeId($temp['model'][$i][$assessType]);
					$score = Yii::$app->db->createCommand($this->getQuery($tableName,$motherId,$assessmentId,$temp['model'][$i]['respId']))->queryOne();
					$temp['model'][$i]['value'] = $this->getScore($score);
                	array_push($temp['model'],$temp['model'][$i]);
                	$temp['model'][$i][$assessType] = 'm';
                	$temp['model'][$i]['respId'] = $respType->getTypeId($temp['model'][$i][$assessType]);
					if($modelScore[$i]['table_name'] == 'shared-caregiving')
					{
						$score = Yii::$app->db->createCommand($this->getQuery($tableName,$motherId,$assessmentId,$respType->getTypeId('m,ac')))->queryOne();
					}
                    else if($modelScore[$i]['table_name'] == 'child-ecbq')
                    {
                        $respondentId = Yii::$app->session->get('respondentId');
                        if(isset($respondentId)){
                            if($respondentId==$motherId)

                             $score = Yii::$app->db->createCommand($this->getQuery($tableName,$motherId,$assessmentId,$motherId))->queryOne(); 

                             else{
                                 $score = Yii::$app->db->createCommand($this->getQuery($tableName,$motherId,$assessmentId,$respondentId))->queryOne(); 
                             } 
                        }
                        else{
                            $score = Yii::$app->db->createCommand("SELECT * from child_ecbq  where assessment_id = $assessmentId and  mother_id = $motherId")->queryOne();
                        }
                        
                    } 
                    else if($modelScore[$i]['table_name'] == 'cbcl')
                    {
                        $respondentId = Yii::$app->session->get('respondentId');
                        if(isset($respondentId)){
                            if($respondentId==$motherId)

                             $score = Yii::$app->db->createCommand($this->getQuery($tableName,$motherId,$assessmentId,$motherId))->queryOne(); 

                             else{
                                 $score = Yii::$app->db->createCommand($this->getQuery($tableName,$motherId,$assessmentId,$respondentId))->queryOne(); 
                             } 
                        }
                        else{
                            $score = Yii::$app->db->createCommand("SELECT * from cbcl  where assessment_id = $assessmentId and  mother_id = $motherId")->queryOne();
                        }
                        
                    }

                      else if($modelScore[$i]['table_name'] == 'bitsea')
                    {
                        $respondentId = Yii::$app->session->get('respondentId');
                        if(isset($respondentId)){
                            if($respondentId==$motherId)

                             $score = Yii::$app->db->createCommand($this->getQuery($tableName,$motherId,$assessmentId,$motherId))->queryOne(); 

                             else{
                                 $score = Yii::$app->db->createCommand($this->getQuery($tableName,$motherId,$assessmentId,$respondentId))->queryOne(); 
                             } 
                        }
                        else{
                            $score = Yii::$app->db->createCommand("SELECT * from bitsea  where assessment_id = $assessmentId and  mother_id = $motherId")->queryOne();
                        }
                        
                    }
                    else if($modelScore[$i]['table_name'] == 'pics')
                    {
                        $score = Yii::$app->db->createCommand($this->getQuery($tableName,$motherId,$assessmentId,$respType->getTypeId('m,ac')))->queryOne();
                    }
                    else if($modelScore[$i]['table_name'] == 'baby-likes-dislikes')
                    {
                        $score = Yii::$app->db->createCommand($this->getQuery($tableName,$motherId,$assessmentId,$respType->getTypeId('m,ac')))->queryOne();
                    }
                    else if($modelScore[$i]['table_name'] == 'icmr')
                    {
                        $score = Yii::$app->db->createCommand($this->getQuery($tableName,$motherId,$assessmentId,$respType->getTypeId('m')))->queryOne();
                    }
					else
					{
						$score = Yii::$app->db->createCommand($this->getQuery($tableName,$motherId,$assessmentId,$temp['model'][$i]['respId']))->queryOne();
					}
                	$temp['model'][$i]['value'] = $this->getScore($score);
                }
                else 
                {
					$temp['model'][$i]['respId'] = $respType->getTypeId($temp['model'][$i][$assessType]);
                	$score = Yii::$app->db->createCommand($this->getQuery($tableName,$motherId,$assessmentId,$temp['model'][$i]['respId']))->queryOne();
                	$temp['model'][$i]['value'] = $this->getScore($score);
                }
            }
        }
        return $temp;
    }
    
    public function getQuery($tableName,$motherId,$assessmentId,$respondentId){
		return 'SELECT * from '.$tableName.' where mother_id = '.$motherId.' and assessment_id = '.$assessmentId.' and respondent_id = '.$respondentId;
    }

    public function getScore($model){
        $count1 = 0;
		$count = 0;
        $totalCount = 0;
        $score = 0;
        if(isset($model['id']))
        {
            unset($model['id']);
            unset($model['respondent_id']); 
            unset($model['assessment_id']); 
            unset($model['last_updated_dtm']);
			unset($model['mother_id']);
            $score = $model['score']; 
            unset($model['score']); 
            unset($model['updated_by']); 
            unset($model['created_dtm']); 
            unset($model['scale_id']);
            foreach($model as $key => $value)
            {
                $totalCount++; 
                if(($value != "") && ($value != null))
                {
                    $count++;
                }
            }
        }
        if($count)
        {
			$count1 = $count;
            $count = floor(($count * 100)/$totalCount);   
        }
        return ['totalcount' => $totalCount,'count1' => $count1,'count' => $count,'score' => $score];
    }

     public function actionScores(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        //$model=Score::findBySql("Select s.*,sc.scale_name FROM  scores as s INNER JOIN scale as sc on sc.id=s.scale_id where subject_id=1 and assessment_id=1 ")->all();
		//$respondentId = Yii::$app->session->get('respondentId');
		$motherId = Yii::$app->session->get('motherId');
        $assessmentId = Yii::$app->session->get('assessmentId');
        $model=Scores::findBySql("SELECT * from scores as s where s.respondent_id = $motherId and s.assessment_id= $assessmentId")->all();
        return $model;
    }
    /**
     * Finds the Assessment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Assessment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Assessment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	public function actionCreateassessments(){
		$id = 20000;
		for($i=897;$i<912;$i++){
			
				$model = new Assessment();
				$model->mother_id = $i;
				$model->created_dtm = date('Y-m-d h:i:s');
				$model->last_updated_dtm = date('Y-m-d h:i:s');
				$model->updated_by = "superadmin";
				$model->assessment_type = "m6";
				$model->assessment_date = date('Y-m-d h:i:s');
				$model->place_of_assesment = "NIMHANS";
				$model->assessment_by = "aakash";
				$model->is_there_acg = "NO";
				$model->save(false);
				
				$model = new Assessment();
				$model->mother_id = $i;
				$model->created_dtm = date('Y-m-d h:i:s');
				$model->last_updated_dtm = date('Y-m-d h:i:s');
				$model->updated_by = "superadmin";
				$model->assessment_type = "m12";
				$model->assessment_date = date('Y-m-d h:i:s');
				$model->place_of_assesment = "NIMHANS";
				$model->assessment_by = "aakash";
				$model->is_there_acg = "NO";
				$model->save(false);
				
				$model = new Assessment();
				$model->mother_id = $i;
				$model->created_dtm = date('Y-m-d h:i:s');
				$model->last_updated_dtm = date('Y-m-d h:i:s');
				$model->updated_by = "superadmin";
				$model->assessment_type = "m24";
				$model->assessment_date = date('Y-m-d h:i:s');
				$model->place_of_assesment = "NIMHANS";
				$model->assessment_by = "aakash";
				$model->is_there_acg = "NO";
				$model->save(false);
				
				
				
			}
	}
}
