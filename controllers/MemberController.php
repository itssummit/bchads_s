<?php

namespace app\controllers;

use Yii;
use app\models\Member;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Users;
/**
 * MemberController implements the CRUD actions for Member model.
 */
class MemberController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Member models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Member::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Member model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Member model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
     public function actionCreate()
    {
       
        $respondentId = Yii::$app->session->get('respondentId');
        $assessmentId = Yii::$app->session->get('assessmentId');
    	$check = Member::findOne(['respondent_id' =>  $respondentId,'assessment_id' => $assessmentId]);
    	if(isset($check['id']))
    	{
    		$model = $this->findModel($check['id']);
    	}
    	else
    	{
			$model = new Member();
    	}
		return $this->render('create', ['model' => $model]);
    }
	
	public function actionAjax()
    {
    	$model = new Member();
    	$postForm = $_POST['Member'];
    	$returnCreateValue = 'Member Created';
    	$returnUpdateValue = 'Member Updated';
    	$returnFalseValue = 'Member Not Inserted';
        $assessmentId = Yii::$app->session->get('assessmentId');
        $respondentId = Yii::$app->session->get('respondentId');
    	if(isset($postForm))
    	{
    		$findModel = Member::findOne(['respondent_id' =>  $respondentId,'assessment_id' => $assessmentId]);
    		if(isset($findModel['id']))
    		{
    			$model = $this->findModel($findModel['id']);
    			$returnValue = $returnUpdateValue;
    			$model->attributes = $postForm;
                $model->save();
    		}
    		else
    		{
    			$returnValue = $returnCreateValue;
    			$model->attributes = $postForm;
                $model->scale_id=0;
                $model->save();
    		}
            $model->member_code = strtoupper($model->member_code);
    		$model->respondent_id =  $respondentId;
            $model->assessment_id= $assessmentId;
			$model->save();
            $scale = new \app\models\Scores;
            $scale->updateScales( $respondentId,$assessmentId,$model->scale_id,$model->score);
            
    			
    	}
    	return $returnValue;
    }


    /**
     * Updates an existing Member model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/', 'id' => $model->id]);
        } else {
                return $this->render('update', [
				'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Member model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

      public function actionAddmember($motherId=null)
    {  
       
        $model = new Member();

        if ($model->load(Yii::$app->request->post())) {
            $model->created_dtm= date('Y-m-d H:i:s');
			$model->updated_by=Users::findOne(['id' => Yii::$app->user->id])->username;
            if($motherId){
                $model->mother_id=$motherId;
				 
        	}
			$model->dob = ($model->dob)?date('Y-m-d',strtotime($model->dob)):null;
            $model->save();
            return $this->redirect(['/site/index']);
        } 
        else {
            if($motherId){
                Yii::$app->cache->set('motherId',$motherId);
                return $this->renderAjax('_form', [
                'model' => $model,
                ]); 
                   
            }
            return $this->render('_form', [
                'model' => $model,
            ]);
        } 
       
    }

    /**
     * Finds the Member model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Member the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Member::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
