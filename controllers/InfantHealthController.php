<?php

namespace app\controllers;

use Yii;
use app\models\InfantHealth;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\classes\SessionHandler;
use app\models\Users;
use app\classes\RespondentType;

/**
 * InfantHealthController implements the CRUD actions for InfantHealth model.
 */
class InfantHealthController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all InfantHealth models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => InfantHealth::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single InfantHealth model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
	 
	 public function multiSelect($model){
    	 
    	if(!empty($model))
    	{
    		if(is_array($model))
    		{
    			$tempArr = $model;
    			$key = array_search("multiselect-all",$tempArr);
    			if($key!==false){
    				array_shift($tempArr);
    			}
    			$model = implode(",",array_unique($tempArr));
    		}
    		else
    		{
    			return $model;
    		}
    	}
    	else
    	{
    		$model = "";
    	}
    	return $model;
    }

    /**
     * Creates a new InfantHealth model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $motherId = Yii::$app->session->get('motherId');
        $assessmentId = Yii::$app->session->get('assessmentId');
		//$respondentId = Yii::$app->session->get('respondentId');
        $resp = new RespondentType();
        $respondentId =$resp->getTypeId('m');
    	$check = InfantHealth::findOne(['mother_id' => $motherId,'assessment_id' => $assessmentId,'respondent_id'=>$respondentId]);
    	if(isset($check['id']))
    	{
    		$model = $this->findModel($check['id']);
    	}
    	else
    	{
    		$model = new InfantHealth();
    	}
    	return $this->render('create', ['model' => $model]);
    }

	
	public function actionAjax()
    {
    	$model = new InfantHealth();
    	$postForm = $_POST['InfantHealth'];
    	$returnCreateValue = 'InfantHealth Created';
    	$returnUpdateValue = 'InfantHealth Updated';
    	$returnFalseValue = 'InfantHealth Not Inserted';
    	$motherId = Yii::$app->session->get('motherId');
        $assessmentId = Yii::$app->session->get('assessmentId');
        //$respondentId = Yii::$app->session->get('respondentId');
        $resp = new RespondentType();
        $respondentId =$resp->getTypeId('m');
    	if(isset($postForm))
    	{
    		$findModel = InfantHealth::findOne(['mother_id' => $motherId,'assessment_id' => $assessmentId,'respondent_id'=>$respondentId]);
    		if(isset($findModel['id']))
    		{
    			$model = $this->findModel($findModel['id']);
    			$returnValue = $returnUpdateValue;
    			$model->attributes = $postForm;
    		}
    		else
    		{
    			$returnValue = $returnCreateValue;
    			$model->attributes = $postForm;
                $model->scale_id=17;
				$model->created_dtm= date('Y:m:d h:i:s');
    		}
    		$model->mother_id = $motherId;
            $model->assessment_id= $assessmentId;
			$model->respondent_id= $respondentId;
			$model->dod = ($model->dod)?date('Y-m-d',strtotime($model->dod)):null;
			$model->last_updated_dtm= date('Y:m:d h:i:s');
            $model->updated_by=Users::findOne(['id' => Yii::$app->user->id])->username;
			$model->hospitalization = $this->multiSelect($model->hospitalization);
			$model->nicu_care = $this->multiSelect($model->nicu_care);
			$model->neonatal_jaundice = $this->multiSelect($model->neonatal_jaundice);
			$model->icu_care = $this->multiSelect($model->icu_care);
			$model->fevers= $this->multiSelect($model->fevers);
			$model->exanthaemotous_fever = $this->multiSelect($model->exanthaemotous_fever);
			$model->seizures = $this->multiSelect($model->seizures);
			$model->asthma= $this->multiSelect($model->asthma);
			$model->respiratory_illness = $this->multiSelect($model->respiratory_illness);
			$model->diarrhoea = $this->multiSelect($model->diarrhoea);
			$model->other_details = $this->multiSelect($model->other_details);
            /*$model->how_many_times_dropped = $this->setMinusTen($model->how_many_times_dropped);
            $model->how_many_times_burnt  = $this->setMinusTen($model->how_many_times_burnt);
            $model->how_old_was_she_while_burnt  = $this->setMinusTen($model->how_old_was_she_while_burnt);
            $model->how_old_was_she_while_burnt2  = $this->setMinusTen($model->how_old_was_she_while_burnt2);
            $model->how_old_was_she_while_burnt3  = $this->setMinusTen($model->how_old_was_she_while_burnt3);
            $model->what_did_person_with_child_do_about_burnt  = $this->setMinusTen($model->what_did_person_with_child_do_about_burnt);
            $model->what_did_person_with_child_do_about_burnt_others  = $this->setMinusTen($model->what_did_person_with_child_do_about_burnt_others);
            $model->how_old_was_she_while_dropped  = $this->setMinusTen($model->how_old_was_she_while_dropped);
            $model->how_old_was_she_while_dropped2  = $this->setMinusTen($model->how_old_was_she_while_dropped2);
            $model->how_old_was_she_while_dropped3  = $this->setMinusTen($model->how_old_was_she_while_dropped3);
            $model->what_did_person_with_child_do_about_dropp  = $this->setMinusTen($model->what_did_person_with_child_do_about_dropp);
            $model->how_many_times_swallowed  = $this->setMinusTen($model->how_many_times_swallowed);
            $model->what_did_person_with_child_do_about_swallow = $this->setMinusTen($model->what_did_person_with_child_do_about_swallow);
            $model->how_many_times_others  = $this->setMinusTen($model->how_many_times_others);
            $model->what_did_person_with_child_do_about_other  = $this->setMinusTen($model->what_did_person_with_child_do_about_other);
            $model->how_old_was_she_while_swallowed  = $this->setMinusTen($model->how_old_was_she_while_swallowed);

            $model->how_old_was_she_while_swallowed2  = $this->setMinusTen($model->how_old_was_she_while_swallowed2);

            $model->how_old_was_she_while_swallowed3  = $this->setMinusTen($model->how_old_was_she_while_swallowed3);
            $model->what_did_person_with_child_do_about_dropp_others  = $this->setMinusTen($model->what_did_person_with_child_do_about_dropp_others);
            $model->others_notes = $this->setMinusTen($model->others_notes);

            $model->how_old_was_she_while_other = $this->setMinusTen($model->how_old_was_she_while_other);
            $model->how_old_was_she_while_other2 = $this->setMinusTen($model->how_old_was_she_while_other2);
            $model->how_old_was_she_while_other3 = $this->setMinusTen($model->how_old_was_she_while_other3);
            $model->what_did_person_with_child_do_about_dropp_others = $this->setMinusTen($model->what_did_person_with_child_do_about_dropp_others);
            $model->what_did_person_with_child_do_about_swallow_others = $this->setMinusTen($model->what_did_person_with_child_do_about_swallow_others);
            $model->what_did_person_with_child_do_about_other_others = $this->setMinusTen($model->what_did_person_with_child_do_about_other_others);
            $model->others_notes = $this->setMinusTen($model->others_notes);
            $model->others_notes = $this->setMinusTen($model->others_notes);*/
    		$model->save();
            $scale = new \app\models\Scores;
            $scale->updateScales($motherId,$assessmentId,$model->scale_id,$model->score);
            
    			
    	}
    	return $returnValue;
    }

    private function setMinusTen($value){
        if($value == null || $value == "")
        {
            $value = "0";
        }
        return $value;
    }

    /**
     * Updates an existing InfantHealth model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing InfantHealth model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the InfantHealth model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return InfantHealth the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = InfantHealth::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
