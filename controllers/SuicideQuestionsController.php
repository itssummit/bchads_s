<?php

namespace app\controllers;

use Yii;
use app\models\SuicideQuestions;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\classes\SessionHandler;
use app\models\Users;
use app\classes\RespondentType;


/**
 * SuicideQuestionsController implements the CRUD actions for SuicideQuestions model.
 */
class SuicideQuestionsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SuicideQuestions models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => SuicideQuestions::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SuicideQuestions model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
	public function multiSelect($model){
    	 
    	if(!empty($model))
    	{
    		if(is_array($model))
    		{
    			$tempArr = $model;
    			$key = array_search("multiselect-all",$tempArr);
    			if($key!==false){
    				array_shift($tempArr);
    			}
    			$model = implode(",",$tempArr);
    		}
    		else
    		{
    			return $model;
    		}
    	}
    	else
    	{
    		$model = "-10";
    	}
    	return $model;
    }

    /**
     * Creates a new SuicideQuestions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $motherId = Yii::$app->session->get('motherId');
        $assessmentId = Yii::$app->session->get('assessmentId');
		//$respondentId = Yii::$app->session->get('respondentId');
        $resp = new RespondentType();
        $respondentId =$resp->getTypeId('m');
    	$check = SuicideQuestions::findOne(['mother_id' => $motherId,'assessment_id' => $assessmentId,'respondent_id' =>$respondentId]);
    	if(isset($check['id']))
    	{
    		$model = $this->findModel($check['id']);
    	}
    	else
    	{
    		$model = new SuicideQuestions();
    	}
    	return $this->render('create', ['model' => $model]);
    }
	
	public function actionAjax()
    {
    	$model = new SuicideQuestions();
    	$postForm = $_POST['SuicideQuestions'];
    	$returnCreateValue = 'SuicideQuestions Created';
    	$returnUpdateValue = 'SuicideQuestions Updated';
    	$returnFalseValue = 'SuicideQuestions Not Inserted';
    	$motherId = Yii::$app->session->get('motherId');
        $assessmentId = Yii::$app->session->get('assessmentId');
		//$respondentId = Yii::$app->session->get('respondentId');
        $resp = new RespondentType();
        $respondentId =$resp->getTypeId('m');
    	if(isset($postForm))
    	{
			
    		$findModel = SuicideQuestions::findOne(['mother_id' => $motherId,'assessment_id' => $assessmentId,'respondent_id' => $respondentId]);
    		if(isset($findModel['id']))
    		{
    			$model = $this->findModel($findModel['id']);
    			$returnValue = $returnUpdateValue;
    			$model->attributes = $postForm;
    		}
    		else
    		{
    			$returnValue = $returnCreateValue;
    			$model->attributes = $postForm;
                $model->scale_id=4;
				$model->created_dtm= date('Y:m:d h:i:s');
                
    		}
    		$model->mother_id = $motherId;
            $model->assessment_id= $assessmentId;
			$model->respondent_id= $respondentId;
			$model->last_updated_dtm= date('Y:m:d h:i:s');
            $model->updated_by=Users::findOne(['id' => Yii::$app->user->id])->username;
			$model->method_attempt = $this->multiSelect($model->method_attempt);
             $model->times_attempted = $this->setMinusTen($model->times_attempted);
             $model->reasons_attempt = $this->setMinusTen($model->reasons_attempt);
             $model->specify_others = $this->setMinusTen($model->specify_others);
    		$model->save(false);
            $scale = new \app\models\Scores;
            $scale->updateScales($motherId,$assessmentId,$model->scale_id,$model->score);
            
    			
    	}
    	return $returnValue;
    }

     private function setMinusTen($value){
        if($value == null || $value == "0")
        {
            $value = "-10";
        }
        return $value;
    }

    /**
     * Updates an existing SuicideQuestions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SuicideQuestions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SuicideQuestions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SuicideQuestions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SuicideQuestions::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
