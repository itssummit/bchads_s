<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "member".
 *
 * @property integer $id
 * @property integer $mother_id
 * @property string $first_name
 * @property string $last_name
 * @property string $relation_type
 * @property integer $contact1
 * @property integer $contact2
 * @property integer $is_active
 * @property string $created_dtm
 * @property string $last_updated_dtm
 * @property string $updated_by
 */
class Member extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'member';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mother_id','is_active'], 'integer'],
			[['dob'], 'safe'],
            [['member_code','relation_type'],'required'],
			[['relation_type','gender','age_months'], 'string'],
			[['member_code'],'unique','message' => 'Already Exists'],
			[['first_name','last_name',],'match', 'pattern' => '/^[a-zA-Z ]+$/', 'message' => 'Can Contain only Alphabets or Space'],
            [['created_dtm', 'last_updated_dtm'], 'safe'],
            [['first_name', 'last_name','member_code'], 'string', 'max' => 255],
            [['updated_by'], 'string', 'max' => 45],
			[['contact1', 'contact2'], 'string', 'max' => 15, 'min'=>10],
			[['contact1', 'contact2'], 'match', 'pattern' => '/^[0-9-+]+$/', 'message'=>'Only Numbers,+,- are allowed.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mother_id' => 'Mother ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'relation_type' => 'Relation Type',
            'contact1' => 'Contact1',
            'contact2' => 'Contact2',
            'is_active' => 'Is Active',
            'created_dtm' => 'Created Date',
            'last_updated_dtm' => 'Last Updated Date',
            'updated_by' => 'Updated By',
			'gender' =>'Gender', 
            'member_code' =>'Member Code',
        ];
    }
}
