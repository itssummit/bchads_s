<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "somatic_symptoms".
 *
 * @property integer $id
 * @property integer $respondent_id
 * @property integer $assessment_id
 * @property string $created_dtm
 * @property string $last_updated_dtm
 * @property string $updated_by
 * @property integer $pain_headache
 * @property integer $pain_backache
 * @property integer $pain_extremities
 * @property integer $pain_abdominal
 * @property integer $pain_body
 * @property integer $sensory_tingling
 * @property integer $sensory_heat_cold
 * @property integer $sensory_palpitations
 * @property integer $sensory_gas
 * @property integer $sensory_burning
 * @property integer $non_specific_weakness_body
 * @property integer $non_specific_weakness_mind
 * @property integer $non_specific_giddiness
 * @property integer $non_specific_trembling
 * @property integer $non_specific_tiredness
 * @property integer $biological_lack_sleep
 * @property integer $biological_appetite
 * @property integer $biological_libidio
 * @property integer $biological_constipation
 * @property integer $biological_diarrhoe
 * @property integer $specific_strong_urge_move_legs
 * @property integer $specific_tingling_leg
 * @property integer $specific_woresening_inactive
 * @property integer $specific_relief_walking
 * @property integer $specific_woresening_sleep
 * @property integer $specific_sleep_restlessness
 * @property integer $tinnitus_noise_ear_buzzing
 * @property integer $other_nausea
 * @property integer $other_urinary_frequency
 * @property integer $other_itching
 * @property integer $other_pelvic_pain
 * @property integer $other_white_discharge
 * @property integer $other_specify
 * @property integer $score_a
 * @property integer $score_b
 * @property integer $score_c
 * @property integer $score_d
 * @property integer $score_e
 * @property integer $score_f
 * @property integer $score
 * @property integer $number_symptoms
 * @property integer $number_significant_symptoms
 */
class SomaticSymptoms extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'somatic_symptoms';
    }
	
	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['respondent_id', 'assessment_id','scale_id','score', 'pain_headache', 'pain_backache', 'pain_extremities', 'pain_abdominal', 'pain_body', 'sensory_tingling', 'sensory_heat_cold', 'sensory_palpitations', 'sensory_gas', 'sensory_burning', 'non_specific_weakness_body', 'non_specific_weakness_mind', 'non_specific_giddiness', 'non_specific_trembling', 'non_specific_tiredness', 'biological_lack_sleep', 'biological_appetite', 'biological_libidio', 'biological_constipation', 'biological_diarrhoe', 'specific_strong_urge_move_legs', 'specific_tingling_leg', 'specific_woresening_inactive', 'specific_relief_walking', 'specific_woresening_sleep', 'specific_sleep_restlessness', 'tinnitus_noise_ear_buzzing', 'other_nausea', 'other_urinary_frequency', 'other_itching', 'other_pelvic_pain', 'other_white_discharge', 'other_specify', 'score_a', 'score_b', 'score_c', 'score_d', 'score_e', 'score_f', 'score', 'number_symptoms', 'number_significant_symptoms'], 'integer'],
            [['created_dtm', 'last_updated_dtm'], 'safe'],
            [['updated_by'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'respondent_id' => 'Patient ID',
            'assessment_id' => 'assessment ID',
            'created_dtm' => 'Created Date',
            'last_updated_dtm' => 'Last Updated Date',
            'updated_by' => 'Updated By',
            'pain_headache' => 'Pain Headache',
            'pain_backache' => 'Pain Backache',
            'pain_extremities' => 'Pain Extremities',
            'pain_abdominal' => 'Pain Abdominal',
            'pain_body' => 'Pain Body',
            'sensory_tingling' => 'Sensory Tingling',
            'sensory_heat_cold' => 'Sensory Heat Cold',
            'sensory_palpitations' => 'Sensory Palpitations',
            'sensory_gas' => 'Sensory Gas',
            'sensory_burning' => 'Sensory Burning',
            'non_specific_weakness_body' => 'Non Specific Weakness Body',
            'non_specific_weakness_mind' => 'Non Specific Weakness Mind',
            'non_specific_giddiness' => 'Non Specific Giddiness',
            'non_specific_trembling' => 'Non Specific Trembling',
            'non_specific_tiredness' => 'Non Specific Tiredness',
            'biological_lack_sleep' => 'Biological Lack Sleep',
            'biological_appetite' => 'Biological Appetite',
            'biological_libidio' => 'Biological Libidio',
            'biological_constipation' => 'Biological Constipation',
            'biological_diarrhoe' => 'Biological Diarrhoe',
            'specific_strong_urge_move_legs' => 'Specific Strong Urge Move Legs',
            'specific_tingling_leg' => 'Specific Tingling Leg',
            'specific_woresening_inactive' => 'Specific Woresening Inactive',
            'specific_relief_walking' => 'Specific Relief Walking',
            'specific_woresening_sleep' => 'Specific Woresening Sleep',
            'specific_sleep_restlessness' => 'Specific Sleep Restlessness',
            'tinnitus_noise_ear_buzzing' => 'Tinnitus Noise Ear Buzzing',
            'other_nausea' => 'Other Nausea',
            'other_urinary_frequency' => 'Other Urinary Frequency',
            'other_itching' => 'Other Itching',
            'other_pelvic_pain' => 'Other Pelvic Pain',
            'other_white_discharge' => 'Other White Discharge',
            'other_specify' => 'Other Specify',
            'score_a' => 'Score A',
            'score_b' => 'Score B',
            'score_c' => 'Score C',
            'score_d' => 'Score D',
            'score_e' => 'Score E',
            'score_f' => 'Score F',
            'score' => 'Score',
            'number_symptoms' => 'Number Symptoms',
            'number_significant_symptoms' => 'Number Significant Symptoms',
        ];
    }

      public function beforeSave($insert)
    {
        return true;
    }



public function afterFind() {
    return true; //don't forget this
}
}
