<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "other_aspects".
 *
 * @property integer $id
 * @property integer $respondent_id
 * @property integer $assessment_id
 * @property string $created_dtm
 * @property string $last_updated_dtm
 * @property string $updated_by
 * @property integer $supportive_partner
 * @property integer $opinions_yourself
 * @property integer $damaged_household
 * @property integer $affection
 * @property integer $disposed_item
 * @property integer $upset
 * @property integer $initiated_discussion
 * @property integer $damaged_clothes
 * @property integer $insulted_partner
 * @property integer $listened_carefully
 * @property integer $locked_outside
 * @property integer $told_couldnt_work
 * @property integer $stated_position
 * @property integer $stop_talking_family/friend
 * @property integer $flexible_difference_opininon
 * @property integer $repeated_point
 * @property integer $restricted_phone
 * @property integer $threats_leave
 * @property integer $cooled_physical_work
 * @property integer $turn_family_against
 * @property integer $helpful_advice
 * @property integer $ordered
 * @property integer $new_ways_dealing_problems
 * @property integer $admitted_faults
 * @property integer $frightened
 * @property integer $treated_stupid
 * @property integer $helpful_ideas
 * @property integer $revenge
 * @property integer $someone_help_settle
 * @property integer $ridiculed
 * @property integer $expressed_regret
 * @property integer $threatened_hit
 * @property integer $told_ugly
 * @property integer $compramise
 * @property integer $abusive
 * @property integer $thrown_things
 * @property integer $sorry
 * @property integer $agreement_settle_argument
 * @property integer $agreed_partner_wanted
 * @property integer $twist_arm
 * @property integer $pushed
 * @property integer $slapped
 * @property integer $forced_sex
 * @property integer $shaken
 * @property integer $thrown_bodily
 * @property integer $thrown_object
 * @property integer $chocked
 * @property integer $kicked
 * @property integer $hit_with_something
 * @property integer $beaten
 * @property integer $threatened_gun_knife
 * @property integer $used_gun_knife
 * @property integer $score
 * @property integer $self(you/yourpartner)
 * @property integer $period
 */
class OtherAspects extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'other_aspects';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['respondent_id', 'assessment_id', 'supportive_partner', 'opinions_yourself', 'damaged_household', 'affection', 'disposed_item', 'upset', 'initiated_discussion', 'damaged_clothes', 'insulted_partner', 'listened_carefully', 'locked_outside', 'told_couldnt_work', 'stated_position', 'stop_talking_family/friend', 'flexible_difference_opininon', 'repeated_point', 'restricted_phone', 'threats_leave', 'cooled_physical_work', 'turn_family_against', 'helpful_advice', 'ordered', 'new_ways_dealing_problems', 'admitted_faults', 'frightened', 'treated_stupid', 'helpful_ideas', 'revenge', 'someone_help_settle', 'ridiculed', 'expressed_regret', 'threatened_hit', 'told_ugly', 'compramise', 'abusive', 'thrown_things', 'sorry', 'agreement_settle_argument', 'agreed_partner_wanted', 'twist_arm', 'pushed', 'slapped', 'forced_sex', 'shaken', 'thrown_bodily', 'thrown_object', 'chocked', 'kicked', 'hit_with_something', 'beaten', 'threatened_gun_knife', 'used_gun_knife', 'score', 'you_your_partner', 'period'], 'integer'],
            [['created_dtm', 'last_updated_dtm'], 'safe'],
            [['updated_by'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'respondent_id' => 'Patient ID',
            'assessment_id' => 'assessment ID',
            'created_dtm' => 'Created Date',
            'last_updated_dtm' => 'Last Updated Date',
            'updated_by' => 'Updated By',
            'supportive_partner' => 'Supportive Partner',
            'opinions_yourself' => 'Opinions Yourself',
            'damaged_household' => 'Damaged Household',
            'affection' => 'Affection',
            'disposed_item' => 'Disposed Item',
            'upset' => 'Upset',
            'initiated_discussion' => 'Initiated Discussion',
            'damaged_clothes' => 'Damaged Clothes',
            'insulted_partner' => 'Insulted Partner',
            'listened_carefully' => 'Listened Carefully',
            'locked_outside' => 'Locked Outside',
            'told_couldnt_work' => 'Told Couldnt Work',
            'stated_position' => 'Stated Position',
            'stop_talking_family/friend' => 'Stop Talking Family/friend',
            'flexible_difference_opininon' => 'Flexible Difference Opininon',
            'repeated_point' => 'Repeated Point',
            'restricted_phone' => 'Restricted Phone',
            'threats_leave' => 'Threats Leave',
            'cooled_physical_work' => 'Cooled Physical Work',
            'turn_family_against' => 'Turn Family Against',
            'helpful_advice' => 'Helpful Advice',
            'ordered' => 'Ordered',
            'new_ways_dealing_problems' => 'New Ways Dealing Problems',
            'admitted_faults' => 'Admitted Faults',
            'frightened' => 'Frightened',
            'treated_stupid' => 'Treated Stupid',
            'helpful_ideas' => 'Helpful Ideas',
            'revenge' => 'Revenge',
            'someone_help_settle' => 'Someone Help Settle',
            'ridiculed' => 'Ridiculed',
            'expressed_regret' => 'Expressed Regret',
            'threatened_hit' => 'Threatened Hit',
            'told_ugly' => 'Told Ugly',
            'compramise' => 'Compramise',
            'abusive' => 'Abusive',
            'thrown_things' => 'Thrown Things',
            'sorry' => 'Sorry',
            'agreement_settle_argument' => 'Agreement Settle Argument',
            'agreed_partner_wanted' => 'Agreed Partner Wanted',
            'twist_arm' => 'Twist Arm',
            'pushed' => 'Pushed',
            'slapped' => 'Slapped',
            'forced_sex' => 'Forced Sex',
            'shaken' => 'Shaken',
            'thrown_bodily' => 'Thrown Bodily',
            'thrown_object' => 'Thrown Object',
            'chocked' => 'Chocked',
            'kicked' => 'Kicked',
            'hit_with_something' => 'Hit With Something',
            'beaten' => 'Beaten',
            'threatened_gun_knife' => 'Threatened Gun Knife',
            'used_gun_knife' => 'Used Gun Knife',
            'score' => 'Score',
            'self(you/yourpartner)' => 'Self(you/yourpartner)',
            'period' => 'Period',
        ];
    }

      public function beforeSave($insert)
    {
        return true;
    }



public function afterFind() {
    return true; //don't forget this
}
}
