<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "db_backup_config".
 *
 * @property integer $id
 * @property string $type
 * @property string $destination_folder
 * @property string $created_dtm
 * @property string $last_updated_dtm
 */
class DbBackupConfig extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_backup_config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'destination_folder', 'created_dtm', 'last_updated_dtm'], 'required'],
            [['created_dtm', 'last_updated_dtm'], 'safe'],
            [['type'], 'string', 'max' => 32],
            [['destination_folder'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'destination_folder' => 'Destination Folder',
            'created_dtm' => 'Created Date',
            'last_updated_dtm' => 'Last Updated Date',
        ];
    }
}
