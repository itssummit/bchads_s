<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "parental_bonding_father".
 *
 * @property integer $id
 * @property string $talks_friendly
 * @property string $dint_help_needed
 * @property string $allowed_loved_work
 * @property string $understands_pain
 * @property string $loving
 * @property string $allowed_own_decisions
 * @property string $hates_growing
 * @property string $stops_all_work
 * @property string $dint_allow_alone
 * @property string $happy_to_talk
 * @property string $laughs
 * @property string $treat_baby
 * @property string $didnt_understand_needs
 * @property string $decides_own
 * @property string $dont_need_feel
 * @property string $helps_in_sad
 * @property string $didnt_talk_lot
 * @property string $feels_dependent
 * @property string $didnt_lookafter_me
 * @property string $give_freedom
 * @property string $allow_outside
 * @property string $saves_more
 * @property string $praises
 * @property integer $respondent_id
 * @property integer $assessment_id
 * @property string $created_dtm
 * @property string $last_updated_dtm
 * @property string $updated_by
 * @property integer $score
 */
class ParentalBondingFather extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parental_bonding_father';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           
            [['respondent_id', 'assessment_id','talks_friendly', 'dint_help_needed', 'allowed_loved_work', 'understands_pain', 'loving', 'allowed_own_decisions', 'hates_growing', 'stops_all_work', 'dint_allow_alone', 'happy_to_talk', 'laughs', 'treat_baby', 'didnt_understand_needs', 'decides_own', 'dont_need_feel', 'helps_in_sad', 'didnt_talk_lot', 'feels_dependent', 'didnt_lookafter_me', 'give_freedom', 'allow_outside', 'saves_more', 'praises','scale_id', 'score',
			'less_helps','less_understands','less_praise','more_secure','allow_decisions','allowstyle','less_books',
			'less_stydy','family_burden','show_olavu','show_nirlakshya','less_helpings','less_undestnds','less_praises',
			'high_safes','allows_decionss','allow_style_as_boys','less_resources','less_oportunity_study',
			'boon','jokes_onme','respects_me','allow_style'], 'integer'],
            [['created_dtm'], 'safe'],
            [[ 'last_updated_dtm', 'updated_by'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'talks_friendly' => 'Talks Friendly',
            'dint_help_needed' => 'Dint Help Needed',
            'allowed_loved_work' => 'Allowed Loved Work',
            'understands_pain' => 'Understands Pain',
            'loving' => 'Loving',
            'allowed_own_decisions' => 'Allowed Own Decisions',
            'hates_growing' => 'Hates Growing',
            'stops_all_work' => 'Stops All Work',
            'dint_allow_alone' => 'Dint Allow Alone',
            'happy_to_talk' => 'Happy To Talk',
            'laughs' => 'Laughs',
            'treat_baby' => 'Treat Baby',
            'didnt_understand_needs' => 'Didnt Understand Needs',
            'decides_own' => 'Decides Own',
            'dont_need_feel' => 'Dont Need Feel',
            'helps_in_sad' => 'Helps In Sad',
            'didnt_talk_lot' => 'Didnt Talk Lot',
            'feels_dependent' => 'Feels Dependent',
            'didnt_lookafter_me' => 'Didnt Lookafter Me',
            'give_freedom' => 'Give Freedom',
            'allow_outside' => 'Allow Outside',
            'saves_more' => 'Saves More',
            'praises' => 'Praises',
            'respondent_id' => 'Patient ID',
            'assessment_id' => 'assessment ID',
            'created_dtm' => 'Created Date',
            'last_updated_dtm' => 'Last Updated Date',
            'updated_by' => 'Updated By',
            'score' => 'Score',
        ];
    }

      public function beforeSave($insert)
    {
        return true;
    }



public function afterFind() {
    return true; //don't forget this
}
}
