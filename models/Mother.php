<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mother".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $dob
 * @property integer $age
 * @property string $present_address
 * @property string $phone1
 * @property string $phone2
 * @property string $permanent_address
 * @property string $domicile
 * @property string $religion
 * @property string $educational_qualification
 * @property string $occupation_Self
 * @property string $occupation_Spouse
 * @property string $socio_economic_status
 * @property string $marital_status
 * @property string $nature_of_marriage
 * @property string $family_type
 * @property integer $no_of_family_members
 * @property integer $total_family_income
 * @property string $spouse_substance_use
 * @property string $substance_type
 * @property integer $duration_of_residence
 * @property integer $no_of_years_of_marriage_years
 * @property integer $no_of_years_of_marriage_months
 * @property string $dateofreg
 * @property string $placeofreg
 * @property string $current_visit
 * @property string $next_appointment_ANC
 * @property string $gravida
 * @property string $parity
 * @property string $lmp
 * @property string $edd
 * @property string $tri_t1
 * @property string $tri_t2
 * @property string $tri_t3
 * @property integer $gestation
 * @property integer $paternal_age_at_conception
 * @property string $medication_prior_pregnancy
 * @property string $medication
 * @property string $prescribed_medication_during_pregnancy
 * @property string $medication_during_pregnancy
 * @property string $fertility_treatment_past
 * @property string $fertility_treatment
 * @property string $planned_pregnancy
 * @property string $self_reaction_for_pregnancy
 * @property string $husband_reaction_for_pregnancy
 * @property string $family_reaction_for_pregnancy
 * @property string $sleep_disturbance
 * @property string $physical_activity
 * @property integer $walking_duration
 * @property integer $exercise_duration
 * @property integer $yoga_duration
 * @property integer $householdchores_duration
 * @property string $others_activity
 * @property integer $others_duration
 * @property string $risk_identified_for_pregnancy
 * @property string $risk_identified_in_foetus
 * @property string $delivery
 * @property integer $yearof_delivery
 * @property integer $gestation_age_of_delivery
 * @property string $mode_of_delivery
 * @property string $complications_during_pregnancy
 * @property string $complications_in_baby
 * @property string $complications_in_mother
 * @property string $risk_identified_in_foetus_past
 * @property string $abortions_miscarrieges
 * @property string $medical_surgical_termination_related_complication
 * @property string $contraception
 * @property string $experience_in_previouspregnancy_postdelivery
 * @property string $specify_experience
 * @property string $pap_test
 * @property string $lactation_related_problems
 * @property string $baby_ability_to_suck
 * @property string $production_of_breast_milk
 * @property string $congenital_anomolies
 * @property string $special_children
 * @property string $mental_illness
 * @property string $postpartum_mental_health_issues
 * @property string $postpartum_mental_health_deatails
 * @property string $created_dtm
 * @property string $last_updated_dtm
 * @property string $updated_by
 * @property string $delivery_date
 * @property string $mother_code
 */
class Mother extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mother';
    }
	
	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mother_code'], 'required'],
            [['dob', 'dateofreg', 'current_visit', 'created_dtm', 'last_updated_dtm', 'delivery_date','assessment_date'], 'safe'],
            [['age', 'no_of_family_members', 'total_family_income', 'duration_of_residence', 'no_of_years_of_marriage_years', 'no_of_years_of_marriage_months', 'gestation', 'paternal_age_at_conception', 'walking_duration', 'exercise_duration', 'yoga_duration', 'householdchores_duration', 'others_duration', 'yearof_delivery', 'gestation_age_of_delivery','age_baby'], 'integer'],
            [['first_name', 'middle_name', 'last_name', 'present_address','gender_baby', 'permanent_address', 'domicile', 'religion', 'educational_qualification', 'occupation_Self', 'occupation_Spouse', 'socio_economic_status', 'marital_status', 'nature_of_marriage', 'family_type', 'spouse_substance_use', 'substance_type', 'placeofreg', 'next_appointment_ANC', 'gravida', 'parity', 'lmp', 'edd', 'tri_t1', 'tri_t2', 'tri_t3', 'medication_prior_pregnancy', 'medication', 'prescribed_medication_during_pregnancy', 'medication_during_pregnancy', 'fertility_treatment_past', 'fertility_treatment', 'planned_pregnancy', 'self_reaction_for_pregnancy', 'husband_reaction_for_pregnancy', 'family_reaction_for_pregnancy', 'sleep_disturbance', 'physical_activity', 'others_activity', 'risk_identified_for_pregnancy', 'risk_identified_in_foetus', 'delivery', 'mode_of_delivery', 'complications_during_pregnancy', 'complications_in_baby', 'complications_in_mother', 'risk_identified_in_foetus_past', 'abortions_miscarrieges', 'medical_surgical_termination_related_complication', 'contraception', 'experience_in_previouspregnancy_postdelivery', 'specify_experience', 'pap_test', 'lactation_related_problems', 'baby_ability_to_suck', 'production_of_breast_milk', 'congenital_anomolies', 'special_children', 'mental_illness', 'postpartum_mental_health_issues', 'postpartum_mental_health_deatails', 'updated_by', 'mother_code'], 'string', 'max' => 45],
            [['phone1', 'phone2','phone3','phone4'], 'string', 'max' => 14],
            [['mother_code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'middle_name' => 'Middle Name',
            'last_name' => 'Last Name',
            'dob' => 'Dob',
            'age' => 'Age',
            'present_address' => 'Present Address',
            'phone1' => 'Phone1',
            'phone2' => 'Phone2',
			'phone3' => 'Phone3',
			'phone4' => 'Phone4',
            'permanent_address' => 'Permanent Address',
            'domicile' => 'Domicile',
            'religion' => 'Religion',
            'educational_qualification' => 'Educational Qualification',
            'occupation_Self' => 'Occupation  Self',
            'occupation_Spouse' => 'Occupation  Spouse',
            'socio_economic_status' => 'Socio Economic Status',
            'marital_status' => 'Marital Status',
            'nature_of_marriage' => 'Nature Of Marriage',
            'family_type' => 'Family Type',
            'no_of_family_members' => 'No Of Family Members',
            'total_family_income' => 'Total Family Income',
            'spouse_substance_use' => 'Spouse Substance Use',
            'substance_type' => 'Substance Type',
            'duration_of_residence' => 'Duration Of Residence',
            'no_of_years_of_marriage_years' => 'No Of Years Of Marriage Years',
            'no_of_years_of_marriage_months' => 'No Of Years Of Marriage Months',
            'dateofreg' => 'Dateofreg',
            'placeofreg' => 'Placeofreg',
            'current_visit' => 'Current Visit',
            'next_appointment_ANC' => 'Next Appointment  Anc',
            'gravida' => 'Gravida',
            'parity' => 'Parity',
            'lmp' => 'Lmp',
            'edd' => 'Edd',
            'tri_t1' => 'Tri T1',
            'tri_t2' => 'Tri T2',
            'tri_t3' => 'Tri T3',
            'gestation' => 'Gestation',
            'paternal_age_at_conception' => 'Paternal Age At Conception',
            'medication_prior_pregnancy' => 'Medication Prior Pregnancy',
            'medication' => 'Medication',
            'prescribed_medication_during_pregnancy' => 'Prescribed Medication During Pregnancy',
            'medication_during_pregnancy' => 'Medication During Pregnancy',
            'fertility_treatment_past' => 'Fertility Treatment Past',
            'fertility_treatment' => 'Fertility Treatment',
            'planned_pregnancy' => 'Planned Pregnancy',
            'self_reaction_for_pregnancy' => 'Self Reaction For Pregnancy',
            'husband_reaction_for_pregnancy' => 'Husband Reaction For Pregnancy',
            'family_reaction_for_pregnancy' => 'Family Reaction For Pregnancy',
            'sleep_disturbance' => 'Sleep Disturbance',
            'physical_activity' => 'Physical Activity',
            'walking_duration' => 'Walking Duration',
            'exercise_duration' => 'Exercise Duration',
            'yoga_duration' => 'Yoga Duration',
            'householdchores_duration' => 'Householdchores Duration',
            'others_activity' => 'Others Activity',
            'others_duration' => 'Others Duration',
            'risk_identified_for_pregnancy' => 'Risk Identified For Pregnancy',
            'risk_identified_in_foetus' => 'Risk Identified In Foetus',
            'delivery' => 'Delivery',
            'yearof_delivery' => 'Yearof Delivery',
            'gestation_age_of_delivery' => 'Gestation Age Of Delivery',
            'mode_of_delivery' => 'Mode Of Delivery',
            'complications_during_pregnancy' => 'Complications During Pregnancy',
            'complications_in_baby' => 'Complications In Baby',
            'complications_in_mother' => 'Complications In Mother',
            'risk_identified_in_foetus_past' => 'Risk Identified In Foetus Past',
            'abortions_miscarrieges' => 'Abortions Miscarrieges',
            'medical_surgical_termination_related_complication' => 'Medical Surgical Termination Related Complication',
            'contraception' => 'Contraception',
            'experience_in_previouspregnancy_postdelivery' => 'Experience In Previouspregnancy Postdelivery',
            'specify_experience' => 'Specify Experience',
            'pap_test' => 'Pap Test',
            'lactation_related_problems' => 'Lactation Related Problems',
            'baby_ability_to_suck' => 'Baby Ability To Suck',
            'production_of_breast_milk' => 'Production Of Breast Milk',
            'congenital_anomolies' => 'Congenital Anomolies',
            'special_children' => 'Special Children',
            'mental_illness' => 'Mental Illness',
            'postpartum_mental_health_issues' => 'Postpartum Mental Health Issues',
            'postpartum_mental_health_deatails' => 'Postpartum Mental Health Deatails',
            'created_dtm' => 'Created Dtm',
            'last_updated_dtm' => 'Last Updated Dtm',
            'updated_by' => 'Updated By',
            'delivery_date' => 'Delivery Date',
            'mother_code' => 'Mother Code',
        ];
    }
}
