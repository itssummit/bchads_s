<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "db_backup_history".
 *
 * @property integer $id
 * @property string $type
 * @property string $result
 * @property string $backup_by
 * @property string $timestamp
 * @property string $duration
 * @property string $filename
 * @property string $location
 * @property string $created_dtm
 * @property string $last_updated_dtm
 */
class DbBackupHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'db_backup_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'result', 'backup_by', 'timestamp', 'duration', 'filename', 'location', 'created_dtm', 'last_updated_dtm'], 'required'],
            [['created_dtm', 'last_updated_dtm'], 'safe'],
            [['type', 'result', 'backup_by', 'timestamp', 'duration'], 'string', 'max' => 32],
            [['filename', 'location'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'result' => 'Result',
            'backup_by' => 'Backup By',
            'timestamp' => 'Timestamp',
            'duration' => 'Duration',
            'filename' => 'Filename',
            'location' => 'Location',
            'created_dtm' => 'Created Date',
            'last_updated_dtm' => 'Last Updated Date',
        ];
    }
}
