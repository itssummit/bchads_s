<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "conflict_tactics_scale".
 *
 * @property integer $id
 * @property integer $supporting_your_partner_at_difficult_times
 * @property integer $keeping_your_opinions_with_yourself
 * @property integer $damaged_things_because_of_angry_on_your_wife
 * @property integer $after_arguing_with_your_partner_did_u_shown_more_love
 * @property integer $knowingly_hiding_ur_partners_loved_things
 * @property integer $did_u_felt_sad_when_things_are_undone
 * @property integer $started_meeting_to_talk_in_all_angles
 * @property integer $intentionally_destroying_partners_personal_things
 * @property integer $disrespecting_ur_partner_infront_of_others
 * @property integer $listened_to_ur_partners_words_with_concentration
 * @property integer $locked_ur_partner_from_outside_home
 * @property integer $said_to_ur_partner_like_u_r_unable_to_study_or_work
 * @property integer $specified_ur_partners_position
 * @property integer $stopping_ur_partner_from_talking_with_relatives
 * @property integer $adjusting_with_different_opinions
 * @property integer $frequently_asking_did_u_understood_or_not
 * @property integer $restritcting_ur_partner_by_using_of_mobiles_or_vehicle
 * @property integer $threatning_ur_partner_for_disconnection_of_relationship
 * @property integer $by_doing_physical_work_did_u_reliefed
 * @property integer $tried_ur_relatives_against_ur_partner
 * @property integer $while_partner_solving_their_problem_did_u_suggested_ideas
 * @property integer $ordered_or_promised_ur_partner
 * @property integer $tried_to_solve_problems_together
 * @property integer $accepted_ur_faults
 * @property integer $feared_ur_partner
 * @property integer $treated_ur_partner_as_a_fool
 * @property integer $given_useful_ideas_to_ur_partner
 * @property integer $done_preplanned_work_with_angry
 * @property integer $tried_to_join_others_to_solve_ur_problems
 * @property integer $disrespected_ur_partner
 * @property integer $self_realised_what_u_said_to_ur_partner
 * @property integer $feared_ur_partner_to_throw_things_while_angry
 * @property integer $said_u_r_not_beautiful_to_partner
 * @property integer $agreed_urself_for_adjustment
 * @property integer $after_smoking_did_u_scolded_ur_partner
 * @property integer $if_ur_partner_disagreed_u_then_did_u_thrown_any_objects
 * @property integer $asked_sorry_after_arguing
 * @property integer $forcibly_closing_the_argument
 * @property integer $agreed_to_ur_partner_wanted_things
 * @property integer $supporting_your_partner_at_difficult_times1
 * @property integer $keeping_your_opinions_with_yourself1
 * @property integer $damaged_things_because_of_angry_on_your_wife1
 * @property integer $after_arguing_with_your_partner_did_u_shown_more_love1
 * @property integer $knowingly_hiding_ur_partners_loved_things1
 * @property integer $did_u_felt_sad_when_things_are_undone1
 * @property integer $intentionally_destroying_partners_personal_things1
 * @property integer $disrespecting_ur_partner_infront_of_others1
 * @property integer $listened_to_ur_partners_words_with_concentration1
 * @property integer $locked_ur_partner_from_outside_home1
 * @property integer $said_to_ur_partner_like_u_r_unable_to_study_or_work1
 * @property integer $specified_ur_partners_position1
 * @property integer $stopping_ur_partner_from_talking_with_relatives1
 * @property integer $adjusting_with_different_opinions1
 * @property integer $frequently_asking_did_u_understood_or_not1
 * @property integer $restritcting_ur_partner_by_using_of_mobiles_or_vehicle1
 * @property integer $threatning_ur_partner_for_disconnection_of_relationship1
 * @property integer $by_doing_physical_work_did_u_reliefed1
 * @property integer $tried_ur_relatives_against_ur_partner1
 * @property integer $while_partner_solving_their_problem_did_u_suggested_ideas1
 * @property integer $ordered_or_promised_ur_partner1
 * @property integer $tried_to_solve_problems_together1
 * @property integer $accepted_ur_faults1
 * @property integer $feared_ur_partner1
 * @property integer $treated_ur_partner_as_a_fool1
 * @property integer $given_useful_ideas_to_ur_partner1
 * @property integer $done_preplanned_work_with_angry1
 * @property integer $tried_to_join_others_to_solve_ur_problems1
 * @property integer $disrespected_ur_partner1
 * @property integer $self_realised_what_u_said_to_ur_partner1
 * @property integer $feared_ur_partner_to_throw_things_while_angry1
 * @property integer $said_u_r_not_beautiful_to_partner1
 * @property integer $agreed_urself_for_adjustment1
 * @property integer $after_smoking_did_u_scolded_ur_partner1
 * @property integer $if_ur_partner_disagreed_u_then_did_u_thrown_any_objects1
 * @property integer $asked_sorry_after_arguing1
 * @property integer $forcibly_closing_the_argument1
 * @property integer $agreed_to_ur_partner_wanted_things1
 * @property integer $rotated_ur_partner_hand
 * @property integer $grabed_and_pushed_ur_husband
 * @property integer $slapped_ur_husband
 * @property integer $forced_ur_partner_for_sex
 * @property integer $shaked_ur_partner_by_holding_tight
 * @property integer $tried_to_throw_ur_husband
 * @property integer $tried_to_throw_things_on_ur_partner
 * @property integer $tried_to_hold_ur_partners_neck
 * @property integer $tried_to_hit_kick_bite
 * @property integer $tried_to_hit_or_hitted
 * @property integer $hitted_ur_partner
 * @property integer $feared_ur_partner_by_gun_knife
 * @property integer $used_gun_knife
 * @property integer $rotated_ur_partner_hand1
 * @property integer $grabed_and_pushed_ur_husband1
 * @property integer $slapped_ur_husband1
 * @property integer $forced_ur_partner_for_sex1
 * @property integer $shaked_ur_partner_by_holding_tight1
 * @property integer $tried_to_throw_ur_husband1
 * @property integer $tried_to_throw_things_on_ur_partner1
 * @property integer $tried_to_hold_ur_partners_neck1
 * @property integer $tried_to_hit_kick_bite1
 * @property integer $tried_to_hit_or_hitted1
 * @property integer $hitted_ur_partner1
 * @property integer $feared_ur_partner_by_gun_knife1
 * @property integer $used_gun_knife1
 * @property integer $rotated_ur_partner_hand12
 * @property integer $grabed_and_pushed_ur_husband12
 * @property integer $slapped_ur_husband12
 * @property integer $forced_ur_partner_for_sex12
 * @property integer $shaked_ur_partner_by_holding_tight12
 * @property integer $tried_to_throw_ur_husband12
 * @property integer $tried_to_throw_things_on_ur_partner12
 * @property integer $tried_to_hold_ur_partners_neck12
 * @property integer $tried_to_hit_kick_bite12
 * @property integer $tried_to_hit_or_hitted12
 * @property integer $hitted_ur_partner12
 * @property integer $feared_ur_partner_by_gun_knife12
 * @property integer $used_gun_knife12
 * @property integer $rotated_ur_partner_hand123
 * @property integer $grabed_and_pushed_ur_husband123
 * @property integer $slapped_ur_husband123
 * @property integer $forced_ur_partner_for_sex123
 * @property integer $shaked_ur_partner_by_holding_tight123
 * @property integer $tried_to_throw_ur_husband123
 * @property integer $tried_to_throw_things_on_ur_partner123
 * @property integer $tried_to_hold_ur_partners_neck123
 * @property integer $tried_to_hit_kick_bite123
 * @property integer $tried_to_hit_or_hitted123
 * @property integer $hitted_ur_partner123
 * @property integer $feared_ur_partner_by_gun_knife123
 * @property integer $used_gun_knife123
 * @property integer $mother_id
 * @property integer $assessment_id
 * @property string $last_updated_dtm
 * @property string $updated_by
 * @property integer $score
 * @property integer $scale_id
 * @property string $created_dtm
 * @property string $rotated_ur_partner_hand_desc1
 * @property string $rotated_ur_partner_hand_desc2
 * @property string $rotated_ur_partner_hand_desc3
 * @property string $grabed_and_pushed_ur_husband_desc1
 * @property string $grabed_and_pushed_ur_husband_desc2
 * @property string $grabed_and_pushed_ur_husband_desc3
 * @property string $slapped_ur_husband_desc1
 * @property string $slapped_ur_husband_desc2
 * @property string $slapped_ur_husband_desc3
 * @property string $forced_ur_partner_for_sex_desc1
 * @property string $forced_ur_partner_for_sex_desc2
 * @property string $forced_ur_partner_for_sex_desc3
 * @property string $shaked_ur_partner_by_holding_tight_desc1
 * @property string $shaked_ur_partner_by_holding_tight_desc2
 * @property string $shaked_ur_partner_by_holding_tight_desc3
 * @property string $tried_to_throw_ur_husband_desc1
 * @property string $tried_to_throw_ur_husband_desc2
 * @property string $tried_to_throw_ur_husband_desc3
 * @property string $tried_to_throw_things_on_ur_partner_desc1
 * @property string $tried_to_throw_things_on_ur_partner_desc2
 * @property string $tried_to_throw_things_on_ur_partner_desc3
 * @property string $tried_to_hold_ur_partners_neck_desc1
 * @property string $tried_to_hold_ur_partners_neck_desc2
 * @property string $tried_to_hold_ur_partners_neck_desc3
 * @property string $tried_to_hit_kick_bite_desc1
 * @property string $tried_to_hit_kick_bite_desc2
 * @property string $tried_to_hit_kick_bite_desc3
 * @property string $tried_to_hit_or_hitted_desc1
 * @property string $tried_to_hit_or_hitted_desc2
 * @property string $tried_to_hit_or_hitted_desc3
 * @property string $hitted_ur_partner_desc1
 * @property string $hitted_ur_partner_desc2
 * @property string $hitted_ur_partner_desc3
 * @property string $feared_ur_partner_by_gun_knife_desc1
 * @property string $feared_ur_partner_by_gun_knife_desc2
 * @property string $feared_ur_partner_by_gun_knife_desc3
 * @property string $used_gun_knife_desc1
 * @property string $used_gun_knife_desc2
 * @property string $used_gun_knife_desc3
 * @property string $rotated_ur_partner_hand1_desc1
 * @property string $rotated_ur_partner_hand1_desc2
 * @property string $rotated_ur_partner_hand1_desc3
 * @property string $grabed_and_pushed_ur_husband1_desc1
 * @property string $grabed_and_pushed_ur_husband1_desc2
 * @property string $grabed_and_pushed_ur_husband1_desc3
 * @property string $slapped_ur_husband1_desc1
 * @property string $slapped_ur_husband1_desc2
 * @property string $slapped_ur_husband1_desc3
 * @property string $forced_ur_partner_for_sex1_desc1
 * @property string $forced_ur_partner_for_sex1_desc2
 * @property string $forced_ur_partner_for_sex1_desc3
 * @property string $shaked_ur_partner_by_holding_tight1_desc1
 * @property string $shaked_ur_partner_by_holding_tight1_desc2
 * @property string $shaked_ur_partner_by_holding_tight1_desc3
 * @property string $tried_to_throw_ur_husband1_desc1
 * @property string $tried_to_throw_ur_husband1_desc2
 * @property string $tried_to_throw_ur_husband1_desc3
 * @property string $tried_to_throw_things_on_ur_partner1_desc1
 * @property string $tried_to_throw_things_on_ur_partner1_desc2
 * @property string $tried_to_throw_things_on_ur_partner1_desc3
 * @property string $tried_to_hold_ur_partners_neck1_desc1
 * @property string $tried_to_hold_ur_partners_neck1_desc2
 * @property string $tried_to_hold_ur_partners_neck1_desc3
 * @property string $tried_to_hit_kick_bite1_desc1
 * @property string $tried_to_hit_kick_bite1_desc2
 * @property string $tried_to_hit_kick_bite1_desc3
 * @property string $tried_to_hit_or_hitted1_desc1
 * @property string $tried_to_hit_or_hitted1_desc2
 * @property string $tried_to_hit_or_hitted1_desc3
 * @property string $hitted_ur_partner1_desc1
 * @property string $hitted_ur_partner1_desc2
 * @property string $hitted_ur_partner1_desc3
 * @property string $feared_ur_partner_by_gun_knife1_desc1
 * @property string $feared_ur_partner_by_gun_knife1_desc2
 * @property string $feared_ur_partner_by_gun_knife1_desc3
 * @property string $used_gun_knife1_desc1
 * @property string $used_gun_knife1_desc2
 * @property string $used_gun_knife1_desc3
 * @property string $rotated_ur_partner_hand12_desc1
 * @property string $rotated_ur_partner_hand12_desc2
 * @property string $grabed_and_pushed_ur_husband12_desc1
 * @property string $grabed_and_pushed_ur_husband12_desc2
 * @property string $slapped_ur_husband12_desc1
 * @property string $slapped_ur_husband12_desc2
 * @property string $forced_ur_partner_for_sex12_desc1
 * @property string $forced_ur_partner_for_sex12_desc2
 * @property string $shaked_ur_partner_by_holding_tight12_desc1
 * @property string $shaked_ur_partner_by_holding_tight12_desc2
 * @property string $tried_to_throw_ur_husband12_desc1
 * @property string $tried_to_throw_ur_husband12_desc2
 * @property string $tried_to_throw_things_on_ur_partner12_desc1
 * @property string $tried_to_throw_things_on_ur_partner12_desc2
 * @property string $tried_to_hold_ur_partners_neck12_desc1
 * @property string $tried_to_hold_ur_partners_neck12_desc2
 * @property string $tried_to_hit_kick_bite12_desc1
 * @property string $tried_to_hit_kick_bite12_desc2
 * @property string $tried_to_hit_or_hitted12_desc1
 * @property string $tried_to_hit_or_hitted12_desc2
 * @property string $hitted_ur_partner12_desc1
 * @property string $hitted_ur_partner12_desc2
 * @property string $feared_ur_partner_by_gun_knife12_desc1
 * @property string $feared_ur_partner_by_gun_knife12_desc2
 * @property string $used_gun_knife12_desc1
 * @property string $used_gun_knife12_desc2
 * @property string $rotated_ur_partner_hand123_desc1
 * @property string $rotated_ur_partner_hand123_desc2
 * @property string $grabed_and_pushed_ur_husband123_desc1
 * @property string $grabed_and_pushed_ur_husband123_desc2
 * @property string $slapped_ur_husband123_desc1
 * @property string $slapped_ur_husband123_desc2
 * @property string $forced_ur_partner_for_sex123_desc1
 * @property string $forced_ur_partner_for_sex123_desc2
 * @property string $shaked_ur_partner_by_holding_tight123_desc1
 * @property string $shaked_ur_partner_by_holding_tight123_desc2
 * @property string $tried_to_throw_ur_husband123_desc1
 * @property string $tried_to_throw_ur_husband123_desc2
 * @property string $tried_to_throw_things_on_ur_partner123_desc1
 * @property string $tried_to_throw_things_on_ur_partner123_desc2
 * @property string $tried_to_hold_ur_partners_neck123_desc1
 * @property string $tried_to_hold_ur_partners_neck123_desc2
 * @property string $tried_to_hit_kick_bite123_desc1
 * @property string $tried_to_hit_kick_bite123_desc2
 * @property string $tried_to_hit_or_hitted123_desc1
 * @property string $tried_to_hit_or_hitted123_desc2
 * @property string $hitted_ur_partner123_desc1
 * @property string $hitted_ur_partner123_desc2
 * @property string $feared_ur_partner_by_gun_knife123_desc1
 * @property string $feared_ur_partner_by_gun_knife123_desc2
 * @property string $used_gun_knife123_desc1
 * @property string $used_gun_knife123_desc2
 */
class ConflictTacticsScale extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'conflict_tactics_scale';
    }
	
	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['supporting_your_partner_at_difficult_times', 'keeping_your_opinions_with_yourself', 'damaged_things_because_of_angry_on_your_wife', 'after_arguing_with_your_partner_did_u_shown_more_love', 'knowingly_hiding_ur_partners_loved_things', 'did_u_felt_sad_when_things_are_undone', 'started_meeting_to_talk_in_all_angles', 'intentionally_destroying_partners_personal_things', 'disrespecting_ur_partner_infront_of_others', 'listened_to_ur_partners_words_with_concentration', 'locked_ur_partner_from_outside_home', 'said_to_ur_partner_like_u_r_unable_to_study_or_work', 'specified_ur_partners_position', 'stopping_ur_partner_from_talking_with_relatives', 'adjusting_with_different_opinions', 'frequently_asking_did_u_understood_or_not', 'restritcting_ur_partner_by_using_of_mobiles_or_vehicle', 'threatning_ur_partner_for_disconnection_of_relationship', 'by_doing_physical_work_did_u_reliefed', 'tried_ur_relatives_against_ur_partner', 'while_partner_solving_their_problem_did_u_suggested_ideas', 'ordered_or_promised_ur_partner', 'tried_to_solve_problems_together', 'accepted_ur_faults', 'feared_ur_partner', 'treated_ur_partner_as_a_fool', 'given_useful_ideas_to_ur_partner', 'done_preplanned_work_with_angry', 'tried_to_join_others_to_solve_ur_problems', 'disrespected_ur_partner', 'self_realised_what_u_said_to_ur_partner', 'feared_ur_partner_to_throw_things_while_angry', 'said_u_r_not_beautiful_to_partner', 'agreed_urself_for_adjustment', 'after_smoking_did_u_scolded_ur_partner', 'if_ur_partner_disagreed_u_then_did_u_thrown_any_objects', 'asked_sorry_after_arguing', 'forcibly_closing_the_argument', 'agreed_to_ur_partner_wanted_things', 'supporting_your_partner_at_difficult_times1', 'keeping_your_opinions_with_yourself1', 'damaged_things_because_of_angry_on_your_wife1', 'after_arguing_with_your_partner_did_u_shown_more_love1', 'knowingly_hiding_ur_partners_loved_things1', 'did_u_felt_sad_when_things_are_undone1', 'intentionally_destroying_partners_personal_things1', 'disrespecting_ur_partner_infront_of_others1', 'listened_to_ur_partners_words_with_concentration1', 'locked_ur_partner_from_outside_home1', 'said_to_ur_partner_like_u_r_unable_to_study_or_work1', 'specified_ur_partners_position1', 'stopping_ur_partner_from_talking_with_relatives1', 'adjusting_with_different_opinions1', 'frequently_asking_did_u_understood_or_not1', 'restritcting_ur_partner_by_using_of_mobiles_or_vehicle1', 'threatning_ur_partner_for_disconnection_of_relationship1', 'by_doing_physical_work_did_u_reliefed1', 'tried_ur_relatives_against_ur_partner1', 'while_partner_solving_their_problem_did_u_suggested_ideas1', 'ordered_or_promised_ur_partner1', 'tried_to_solve_problems_together1', 'accepted_ur_faults1', 'feared_ur_partner1', 'treated_ur_partner_as_a_fool1', 'given_useful_ideas_to_ur_partner1', 'done_preplanned_work_with_angry1', 'tried_to_join_others_to_solve_ur_problems1', 'disrespected_ur_partner1', 'self_realised_what_u_said_to_ur_partner1', 'feared_ur_partner_to_throw_things_while_angry1', 'said_u_r_not_beautiful_to_partner1', 'agreed_urself_for_adjustment1', 'after_smoking_did_u_scolded_ur_partner1', 'if_ur_partner_disagreed_u_then_did_u_thrown_any_objects1', 'asked_sorry_after_arguing1', 'forcibly_closing_the_argument1', 'agreed_to_ur_partner_wanted_things1', 'rotated_ur_partner_hand', 'grabed_and_pushed_ur_husband', 'slapped_ur_husband', 'forced_ur_partner_for_sex', 'shaked_ur_partner_by_holding_tight', 'tried_to_throw_ur_husband', 'tried_to_throw_things_on_ur_partner', 'tried_to_hold_ur_partners_neck', 'tried_to_hit_kick_bite', 'tried_to_hit_or_hitted', 'hitted_ur_partner', 'feared_ur_partner_by_gun_knife', 'used_gun_knife', 'rotated_ur_partner_hand1', 'grabed_and_pushed_ur_husband1', 'slapped_ur_husband1', 'forced_ur_partner_for_sex1', 'shaked_ur_partner_by_holding_tight1', 'tried_to_throw_ur_husband1', 'tried_to_throw_things_on_ur_partner1', 'tried_to_hold_ur_partners_neck1', 'tried_to_hit_kick_bite1', 'tried_to_hit_or_hitted1', 'hitted_ur_partner1', 'feared_ur_partner_by_gun_knife1', 'used_gun_knife1', 'rotated_ur_partner_hand12', 'grabed_and_pushed_ur_husband12', 'slapped_ur_husband12', 'forced_ur_partner_for_sex12', 'shaked_ur_partner_by_holding_tight12', 'tried_to_throw_ur_husband12', 'tried_to_throw_things_on_ur_partner12', 'tried_to_hold_ur_partners_neck12', 'tried_to_hit_kick_bite12', 'tried_to_hit_or_hitted12', 'hitted_ur_partner12', 'feared_ur_partner_by_gun_knife12', 'used_gun_knife12', 'rotated_ur_partner_hand123', 'grabed_and_pushed_ur_husband123', 'slapped_ur_husband123', 'forced_ur_partner_for_sex123', 'shaked_ur_partner_by_holding_tight123', 'tried_to_throw_ur_husband123', 'tried_to_throw_things_on_ur_partner123', 'tried_to_hold_ur_partners_neck123', 'tried_to_hit_kick_bite123', 'tried_to_hit_or_hitted123', 'hitted_ur_partner123', 'feared_ur_partner_by_gun_knife123', 'used_gun_knife123', 'mother_id', 'assessment_id', 'score', 'scale_id','started_meeting_to_talk_in_all_angles1'], 'integer'],
            [['last_updated_dtm', 'created_dtm'], 'safe'],
            [['updated_by', 'rotated_ur_partner_hand_desc1', 'rotated_ur_partner_hand_desc2', 'rotated_ur_partner_hand_desc3', 'grabed_and_pushed_ur_husband_desc1', 'grabed_and_pushed_ur_husband_desc2', 'grabed_and_pushed_ur_husband_desc3', 'slapped_ur_husband_desc1', 'slapped_ur_husband_desc2', 'slapped_ur_husband_desc3', 'forced_ur_partner_for_sex_desc1', 'forced_ur_partner_for_sex_desc2', 'forced_ur_partner_for_sex_desc3', 'shaked_ur_partner_by_holding_tight_desc1', 'shaked_ur_partner_by_holding_tight_desc2', 'shaked_ur_partner_by_holding_tight_desc3', 'tried_to_throw_ur_husband_desc1', 'tried_to_throw_ur_husband_desc2', 'tried_to_throw_ur_husband_desc3', 'tried_to_throw_things_on_ur_partner_desc1', 'tried_to_throw_things_on_ur_partner_desc2', 'tried_to_throw_things_on_ur_partner_desc3', 'tried_to_hold_ur_partners_neck_desc1', 'tried_to_hold_ur_partners_neck_desc2', 'tried_to_hold_ur_partners_neck_desc3', 'tried_to_hit_kick_bite_desc1', 'tried_to_hit_kick_bite_desc2', 'tried_to_hit_kick_bite_desc3', 'tried_to_hit_or_hitted_desc1', 'tried_to_hit_or_hitted_desc2', 'tried_to_hit_or_hitted_desc3', 'hitted_ur_partner_desc1', 'hitted_ur_partner_desc2', 'hitted_ur_partner_desc3', 'feared_ur_partner_by_gun_knife_desc1', 'feared_ur_partner_by_gun_knife_desc2', 'feared_ur_partner_by_gun_knife_desc3', 'used_gun_knife_desc1', 'used_gun_knife_desc2', 'used_gun_knife_desc3', 'rotated_ur_partner_hand1_desc1', 'rotated_ur_partner_hand1_desc2', 'rotated_ur_partner_hand1_desc3', 'grabed_and_pushed_ur_husband1_desc1', 'grabed_and_pushed_ur_husband1_desc2', 'grabed_and_pushed_ur_husband1_desc3', 'slapped_ur_husband1_desc1', 'slapped_ur_husband1_desc2', 'slapped_ur_husband1_desc3', 'forced_ur_partner_for_sex1_desc1', 'forced_ur_partner_for_sex1_desc2', 'forced_ur_partner_for_sex1_desc3', 'shaked_ur_partner_by_holding_tight1_desc1', 'shaked_ur_partner_by_holding_tight1_desc2', 'shaked_ur_partner_by_holding_tight1_desc3', 'tried_to_throw_ur_husband1_desc1', 'tried_to_throw_ur_husband1_desc2', 'tried_to_throw_ur_husband1_desc3', 'tried_to_throw_things_on_ur_partner1_desc1', 'tried_to_throw_things_on_ur_partner1_desc2', 'tried_to_throw_things_on_ur_partner1_desc3', 'tried_to_hold_ur_partners_neck1_desc1', 'tried_to_hold_ur_partners_neck1_desc2', 'tried_to_hold_ur_partners_neck1_desc3', 'tried_to_hit_kick_bite1_desc1', 'tried_to_hit_kick_bite1_desc2', 'tried_to_hit_kick_bite1_desc3', 'tried_to_hit_or_hitted1_desc1', 'tried_to_hit_or_hitted1_desc2', 'tried_to_hit_or_hitted1_desc3', 'hitted_ur_partner1_desc1', 'hitted_ur_partner1_desc2', 'hitted_ur_partner1_desc3', 'feared_ur_partner_by_gun_knife1_desc1', 'feared_ur_partner_by_gun_knife1_desc2', 'feared_ur_partner_by_gun_knife1_desc3', 'used_gun_knife1_desc1', 'used_gun_knife1_desc2', 'used_gun_knife1_desc3', 'rotated_ur_partner_hand12_desc1', 'rotated_ur_partner_hand12_desc2', 'grabed_and_pushed_ur_husband12_desc1', 'grabed_and_pushed_ur_husband12_desc2', 'slapped_ur_husband12_desc1', 'slapped_ur_husband12_desc2', 'forced_ur_partner_for_sex12_desc1', 'forced_ur_partner_for_sex12_desc2', 'shaked_ur_partner_by_holding_tight12_desc1', 'shaked_ur_partner_by_holding_tight12_desc2', 'tried_to_throw_ur_husband12_desc1', 'tried_to_throw_ur_husband12_desc2', 'tried_to_throw_things_on_ur_partner12_desc1', 'tried_to_throw_things_on_ur_partner12_desc2', 'tried_to_hold_ur_partners_neck12_desc1', 'tried_to_hold_ur_partners_neck12_desc2', 'tried_to_hit_kick_bite12_desc1', 'tried_to_hit_kick_bite12_desc2', 'tried_to_hit_or_hitted12_desc1', 'tried_to_hit_or_hitted12_desc2', 'hitted_ur_partner12_desc1', 'hitted_ur_partner12_desc2', 'feared_ur_partner_by_gun_knife12_desc1', 'feared_ur_partner_by_gun_knife12_desc2', 'used_gun_knife12_desc1', 'used_gun_knife12_desc2', 'rotated_ur_partner_hand123_desc1', 'rotated_ur_partner_hand123_desc2', 'grabed_and_pushed_ur_husband123_desc1', 'grabed_and_pushed_ur_husband123_desc2', 'slapped_ur_husband123_desc1', 'slapped_ur_husband123_desc2', 'forced_ur_partner_for_sex123_desc1', 'forced_ur_partner_for_sex123_desc2', 'shaked_ur_partner_by_holding_tight123_desc1', 'shaked_ur_partner_by_holding_tight123_desc2', 'tried_to_throw_ur_husband123_desc1', 'tried_to_throw_ur_husband123_desc2', 'tried_to_throw_things_on_ur_partner123_desc1', 'tried_to_throw_things_on_ur_partner123_desc2', 'tried_to_hold_ur_partners_neck123_desc1', 'tried_to_hold_ur_partners_neck123_desc2', 'tried_to_hit_kick_bite123_desc1', 'tried_to_hit_kick_bite123_desc2', 'tried_to_hit_or_hitted123_desc1', 'tried_to_hit_or_hitted123_desc2', 'hitted_ur_partner123_desc1', 'hitted_ur_partner123_desc2', 'feared_ur_partner_by_gun_knife123_desc1', 'feared_ur_partner_by_gun_knife123_desc2', 'used_gun_knife123_desc1', 'used_gun_knife123_desc2','not_applicable'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'supporting_your_partner_at_difficult_times' => 'Supporting Your Partner At Difficult Times',
            'keeping_your_opinions_with_yourself' => 'Keeping Your Opinions With Yourself',
            'damaged_things_because_of_angry_on_your_wife' => 'Damaged Things Because Of Angry On Your Wife',
            'after_arguing_with_your_partner_did_u_shown_more_love' => 'After Arguing With Your Partner Did U Shown More Love',
            'knowingly_hiding_ur_partners_loved_things' => 'Knowingly Hiding Ur Partners Loved Things',
            'did_u_felt_sad_when_things_are_undone' => 'Did U Felt Sad When Things Are Undone',
            'started_meeting_to_talk_in_all_angles' => 'Started Meeting To Talk In All Angles',
            'intentionally_destroying_partners_personal_things' => 'Intentionally Destroying Partners Personal Things',
            'disrespecting_ur_partner_infront_of_others' => 'Disrespecting Ur Partner Infront Of Others',
            'listened_to_ur_partners_words_with_concentration' => 'Listened To Ur Partners Words With Concentration',
            'locked_ur_partner_from_outside_home' => 'Locked Ur Partner From Outside Home',
            'said_to_ur_partner_like_u_r_unable_to_study_or_work' => 'Said To Ur Partner Like U R Unable To Study Or Work',
            'specified_ur_partners_position' => 'Specified Ur Partners Position',
            'stopping_ur_partner_from_talking_with_relatives' => 'Stopping Ur Partner From Talking With Relatives',
            'adjusting_with_different_opinions' => 'Adjusting With Different Opinions',
            'frequently_asking_did_u_understood_or_not' => 'Frequently Asking Did U Understood Or Not',
            'restritcting_ur_partner_by_using_of_mobiles_or_vehicle' => 'Restritcting Ur Partner By Using Of Mobiles Or Vehicle',
            'threatning_ur_partner_for_disconnection_of_relationship' => 'Threatning Ur Partner For Disconnection Of Relationship',
            'by_doing_physical_work_did_u_reliefed' => 'By Doing Physical Work Did U Reliefed',
            'tried_ur_relatives_against_ur_partner' => 'Tried Ur Relatives Against Ur Partner',
            'while_partner_solving_their_problem_did_u_suggested_ideas' => 'While Partner Solving Their Problem Did U Suggested Ideas',
            'ordered_or_promised_ur_partner' => 'Ordered Or Promised Ur Partner',
            'tried_to_solve_problems_together' => 'Tried To Solve Problems Together',
            'accepted_ur_faults' => 'Accepted Ur Faults',
            'feared_ur_partner' => 'Feared Ur Partner',
            'treated_ur_partner_as_a_fool' => 'Treated Ur Partner As A Fool',
            'given_useful_ideas_to_ur_partner' => 'Given Useful Ideas To Ur Partner',
            'done_preplanned_work_with_angry' => 'Done Preplanned Work With Angry',
            'tried_to_join_others_to_solve_ur_problems' => 'Tried To Join Others To Solve Ur Problems',
            'disrespected_ur_partner' => 'Disrespected Ur Partner',
            'self_realised_what_u_said_to_ur_partner' => 'Self Realised What U Said To Ur Partner',
            'feared_ur_partner_to_throw_things_while_angry' => 'Feared Ur Partner To Throw Things While Angry',
            'said_u_r_not_beautiful_to_partner' => 'Said U R Not Beautiful To Partner',
            'agreed_urself_for_adjustment' => 'Agreed Urself For Adjustment',
            'after_smoking_did_u_scolded_ur_partner' => 'After Smoking Did U Scolded Ur Partner',
            'if_ur_partner_disagreed_u_then_did_u_thrown_any_objects' => 'If Ur Partner Disagreed U Then Did U Thrown Any Objects',
            'asked_sorry_after_arguing' => 'Asked Sorry After Arguing',
            'forcibly_closing_the_argument' => 'Forcibly Closing The Argument',
            'agreed_to_ur_partner_wanted_things' => 'Agreed To Ur Partner Wanted Things',
            'supporting_your_partner_at_difficult_times1' => 'Supporting Your Partner At Difficult Times1',
            'keeping_your_opinions_with_yourself1' => 'Keeping Your Opinions With Yourself1',
            'damaged_things_because_of_angry_on_your_wife1' => 'Damaged Things Because Of Angry On Your Wife1',
            'after_arguing_with_your_partner_did_u_shown_more_love1' => 'After Arguing With Your Partner Did U Shown More Love1',
            'knowingly_hiding_ur_partners_loved_things1' => 'Knowingly Hiding Ur Partners Loved Things1',
            'did_u_felt_sad_when_things_are_undone1' => 'Did U Felt Sad When Things Are Undone1',
            'intentionally_destroying_partners_personal_things1' => 'Intentionally Destroying Partners Personal Things1',
            'disrespecting_ur_partner_infront_of_others1' => 'Disrespecting Ur Partner Infront Of Others1',
            'listened_to_ur_partners_words_with_concentration1' => 'Listened To Ur Partners Words With Concentration1',
            'locked_ur_partner_from_outside_home1' => 'Locked Ur Partner From Outside Home1',
            'said_to_ur_partner_like_u_r_unable_to_study_or_work1' => 'Said To Ur Partner Like U R Unable To Study Or Work1',
            'specified_ur_partners_position1' => 'Specified Ur Partners Position1',
            'stopping_ur_partner_from_talking_with_relatives1' => 'Stopping Ur Partner From Talking With Relatives1',
            'adjusting_with_different_opinions1' => 'Adjusting With Different Opinions1',
            'frequently_asking_did_u_understood_or_not1' => 'Frequently Asking Did U Understood Or Not1',
            'restritcting_ur_partner_by_using_of_mobiles_or_vehicle1' => 'Restritcting Ur Partner By Using Of Mobiles Or Vehicle1',
            'threatning_ur_partner_for_disconnection_of_relationship1' => 'Threatning Ur Partner For Disconnection Of Relationship1',
            'by_doing_physical_work_did_u_reliefed1' => 'By Doing Physical Work Did U Reliefed1',
            'tried_ur_relatives_against_ur_partner1' => 'Tried Ur Relatives Against Ur Partner1',
            'while_partner_solving_their_problem_did_u_suggested_ideas1' => 'While Partner Solving Their Problem Did U Suggested Ideas1',
            'ordered_or_promised_ur_partner1' => 'Ordered Or Promised Ur Partner1',
            'tried_to_solve_problems_together1' => 'Tried To Solve Problems Together1',
            'accepted_ur_faults1' => 'Accepted Ur Faults1',
            'feared_ur_partner1' => 'Feared Ur Partner1',
            'treated_ur_partner_as_a_fool1' => 'Treated Ur Partner As A Fool1',
            'given_useful_ideas_to_ur_partner1' => 'Given Useful Ideas To Ur Partner1',
            'done_preplanned_work_with_angry1' => 'Done Preplanned Work With Angry1',
            'tried_to_join_others_to_solve_ur_problems1' => 'Tried To Join Others To Solve Ur Problems1',
            'disrespected_ur_partner1' => 'Disrespected Ur Partner1',
            'self_realised_what_u_said_to_ur_partner1' => 'Self Realised What U Said To Ur Partner1',
            'feared_ur_partner_to_throw_things_while_angry1' => 'Feared Ur Partner To Throw Things While Angry1',
            'said_u_r_not_beautiful_to_partner1' => 'Said U R Not Beautiful To Partner1',
            'agreed_urself_for_adjustment1' => 'Agreed Urself For Adjustment1',
            'after_smoking_did_u_scolded_ur_partner1' => 'After Smoking Did U Scolded Ur Partner1',
            'if_ur_partner_disagreed_u_then_did_u_thrown_any_objects1' => 'If Ur Partner Disagreed U Then Did U Thrown Any Objects1',
            'asked_sorry_after_arguing1' => 'Asked Sorry After Arguing1',
            'forcibly_closing_the_argument1' => 'Forcibly Closing The Argument1',
            'agreed_to_ur_partner_wanted_things1' => 'Agreed To Ur Partner Wanted Things1',
            'rotated_ur_partner_hand' => 'Rotated Ur Partner Hand',
            'grabed_and_pushed_ur_husband' => 'Grabed And Pushed Ur Husband',
            'slapped_ur_husband' => 'Slapped Ur Husband',
            'forced_ur_partner_for_sex' => 'Forced Ur Partner For Sex',
            'shaked_ur_partner_by_holding_tight' => 'Shaked Ur Partner By Holding Tight',
            'tried_to_throw_ur_husband' => 'Tried To Throw Ur Husband',
            'tried_to_throw_things_on_ur_partner' => 'Tried To Throw Things On Ur Partner',
            'tried_to_hold_ur_partners_neck' => 'Tried To Hold Ur Partners Neck',
            'tried_to_hit_kick_bite' => 'Tried To Hit Kick Bite',
            'tried_to_hit_or_hitted' => 'Tried To Hit Or Hitted',
            'hitted_ur_partner' => 'Hitted Ur Partner',
            'feared_ur_partner_by_gun_knife' => 'Feared Ur Partner By Gun Knife',
            'used_gun_knife' => 'Used Gun Knife',
            'rotated_ur_partner_hand1' => 'Rotated Ur Partner Hand1',
            'grabed_and_pushed_ur_husband1' => 'Grabed And Pushed Ur Husband1',
            'slapped_ur_husband1' => 'Slapped Ur Husband1',
            'forced_ur_partner_for_sex1' => 'Forced Ur Partner For Sex1',
            'shaked_ur_partner_by_holding_tight1' => 'Shaked Ur Partner By Holding Tight1',
            'tried_to_throw_ur_husband1' => 'Tried To Throw Ur Husband1',
            'tried_to_throw_things_on_ur_partner1' => 'Tried To Throw Things On Ur Partner1',
            'tried_to_hold_ur_partners_neck1' => 'Tried To Hold Ur Partners Neck1',
            'tried_to_hit_kick_bite1' => 'Tried To Hit Kick Bite1',
            'tried_to_hit_or_hitted1' => 'Tried To Hit Or Hitted1',
            'hitted_ur_partner1' => 'Hitted Ur Partner1',
            'feared_ur_partner_by_gun_knife1' => 'Feared Ur Partner By Gun Knife1',
            'used_gun_knife1' => 'Used Gun Knife1',
            'rotated_ur_partner_hand12' => 'Rotated Ur Partner Hand12',
            'grabed_and_pushed_ur_husband12' => 'Grabed And Pushed Ur Husband12',
            'slapped_ur_husband12' => 'Slapped Ur Husband12',
            'forced_ur_partner_for_sex12' => 'Forced Ur Partner For Sex12',
            'shaked_ur_partner_by_holding_tight12' => 'Shaked Ur Partner By Holding Tight12',
            'tried_to_throw_ur_husband12' => 'Tried To Throw Ur Husband12',
            'tried_to_throw_things_on_ur_partner12' => 'Tried To Throw Things On Ur Partner12',
            'tried_to_hold_ur_partners_neck12' => 'Tried To Hold Ur Partners Neck12',
            'tried_to_hit_kick_bite12' => 'Tried To Hit Kick Bite12',
            'tried_to_hit_or_hitted12' => 'Tried To Hit Or Hitted12',
            'hitted_ur_partner12' => 'Hitted Ur Partner12',
            'feared_ur_partner_by_gun_knife12' => 'Feared Ur Partner By Gun Knife12',
            'used_gun_knife12' => 'Used Gun Knife12',
            'rotated_ur_partner_hand123' => 'Rotated Ur Partner Hand123',
            'grabed_and_pushed_ur_husband123' => 'Grabed And Pushed Ur Husband123',
            'slapped_ur_husband123' => 'Slapped Ur Husband123',
            'forced_ur_partner_for_sex123' => 'Forced Ur Partner For Sex123',
            'shaked_ur_partner_by_holding_tight123' => 'Shaked Ur Partner By Holding Tight123',
            'tried_to_throw_ur_husband123' => 'Tried To Throw Ur Husband123',
            'tried_to_throw_things_on_ur_partner123' => 'Tried To Throw Things On Ur Partner123',
            'tried_to_hold_ur_partners_neck123' => 'Tried To Hold Ur Partners Neck123',
            'tried_to_hit_kick_bite123' => 'Tried To Hit Kick Bite123',
            'tried_to_hit_or_hitted123' => 'Tried To Hit Or Hitted123',
            'hitted_ur_partner123' => 'Hitted Ur Partner123',
            'feared_ur_partner_by_gun_knife123' => 'Feared Ur Partner By Gun Knife123',
            'used_gun_knife123' => 'Used Gun Knife123',
            'mother_id' => 'Respondent ID',
            'assessment_id' => 'Assessment ID',
            'last_updated_dtm' => 'Last Updated Dtm',
            'updated_by' => 'Updated By',
            'score' => 'Score',
            'scale_id' => 'Scale ID',
            'created_dtm' => 'Created Dtm',
            'rotated_ur_partner_hand_desc1' => 'Rotated Ur Partner Hand Desc1',
            'rotated_ur_partner_hand_desc2' => 'Rotated Ur Partner Hand Desc2',
            'rotated_ur_partner_hand_desc3' => 'Rotated Ur Partner Hand Desc3',
            'grabed_and_pushed_ur_husband_desc1' => 'Grabed And Pushed Ur Husband Desc1',
            'grabed_and_pushed_ur_husband_desc2' => 'Grabed And Pushed Ur Husband Desc2',
            'grabed_and_pushed_ur_husband_desc3' => 'Grabed And Pushed Ur Husband Desc3',
            'slapped_ur_husband_desc1' => 'Slapped Ur Husband Desc1',
            'slapped_ur_husband_desc2' => 'Slapped Ur Husband Desc2',
            'slapped_ur_husband_desc3' => 'Slapped Ur Husband Desc3',
            'forced_ur_partner_for_sex_desc1' => 'Forced Ur Partner For Sex Desc1',
            'forced_ur_partner_for_sex_desc2' => 'Forced Ur Partner For Sex Desc2',
            'forced_ur_partner_for_sex_desc3' => 'Forced Ur Partner For Sex Desc3',
            'shaked_ur_partner_by_holding_tight_desc1' => 'Shaked Ur Partner By Holding Tight Desc1',
            'shaked_ur_partner_by_holding_tight_desc2' => 'Shaked Ur Partner By Holding Tight Desc2',
            'shaked_ur_partner_by_holding_tight_desc3' => 'Shaked Ur Partner By Holding Tight Desc3',
            'tried_to_throw_ur_husband_desc1' => 'Tried To Throw Ur Husband Desc1',
            'tried_to_throw_ur_husband_desc2' => 'Tried To Throw Ur Husband Desc2',
            'tried_to_throw_ur_husband_desc3' => 'Tried To Throw Ur Husband Desc3',
            'tried_to_throw_things_on_ur_partner_desc1' => 'Tried To Throw Things On Ur Partner Desc1',
            'tried_to_throw_things_on_ur_partner_desc2' => 'Tried To Throw Things On Ur Partner Desc2',
            'tried_to_throw_things_on_ur_partner_desc3' => 'Tried To Throw Things On Ur Partner Desc3',
            'tried_to_hold_ur_partners_neck_desc1' => 'Tried To Hold Ur Partners Neck Desc1',
            'tried_to_hold_ur_partners_neck_desc2' => 'Tried To Hold Ur Partners Neck Desc2',
            'tried_to_hold_ur_partners_neck_desc3' => 'Tried To Hold Ur Partners Neck Desc3',
            'tried_to_hit_kick_bite_desc1' => 'Tried To Hit Kick Bite Desc1',
            'tried_to_hit_kick_bite_desc2' => 'Tried To Hit Kick Bite Desc2',
            'tried_to_hit_kick_bite_desc3' => 'Tried To Hit Kick Bite Desc3',
            'tried_to_hit_or_hitted_desc1' => 'Tried To Hit Or Hitted Desc1',
            'tried_to_hit_or_hitted_desc2' => 'Tried To Hit Or Hitted Desc2',
            'tried_to_hit_or_hitted_desc3' => 'Tried To Hit Or Hitted Desc3',
            'hitted_ur_partner_desc1' => 'Hitted Ur Partner Desc1',
            'hitted_ur_partner_desc2' => 'Hitted Ur Partner Desc2',
            'hitted_ur_partner_desc3' => 'Hitted Ur Partner Desc3',
            'feared_ur_partner_by_gun_knife_desc1' => 'Feared Ur Partner By Gun Knife Desc1',
            'feared_ur_partner_by_gun_knife_desc2' => 'Feared Ur Partner By Gun Knife Desc2',
            'feared_ur_partner_by_gun_knife_desc3' => 'Feared Ur Partner By Gun Knife Desc3',
            'used_gun_knife_desc1' => 'Used Gun Knife Desc1',
            'used_gun_knife_desc2' => 'Used Gun Knife Desc2',
            'used_gun_knife_desc3' => 'Used Gun Knife Desc3',
            'rotated_ur_partner_hand1_desc1' => 'Rotated Ur Partner Hand1 Desc1',
            'rotated_ur_partner_hand1_desc2' => 'Rotated Ur Partner Hand1 Desc2',
            'rotated_ur_partner_hand1_desc3' => 'Rotated Ur Partner Hand1 Desc3',
            'grabed_and_pushed_ur_husband1_desc1' => 'Grabed And Pushed Ur Husband1 Desc1',
            'grabed_and_pushed_ur_husband1_desc2' => 'Grabed And Pushed Ur Husband1 Desc2',
            'grabed_and_pushed_ur_husband1_desc3' => 'Grabed And Pushed Ur Husband1 Desc3',
            'slapped_ur_husband1_desc1' => 'Slapped Ur Husband1 Desc1',
            'slapped_ur_husband1_desc2' => 'Slapped Ur Husband1 Desc2',
            'slapped_ur_husband1_desc3' => 'Slapped Ur Husband1 Desc3',
            'forced_ur_partner_for_sex1_desc1' => 'Forced Ur Partner For Sex1 Desc1',
            'forced_ur_partner_for_sex1_desc2' => 'Forced Ur Partner For Sex1 Desc2',
            'forced_ur_partner_for_sex1_desc3' => 'Forced Ur Partner For Sex1 Desc3',
            'shaked_ur_partner_by_holding_tight1_desc1' => 'Shaked Ur Partner By Holding Tight1 Desc1',
            'shaked_ur_partner_by_holding_tight1_desc2' => 'Shaked Ur Partner By Holding Tight1 Desc2',
            'shaked_ur_partner_by_holding_tight1_desc3' => 'Shaked Ur Partner By Holding Tight1 Desc3',
            'tried_to_throw_ur_husband1_desc1' => 'Tried To Throw Ur Husband1 Desc1',
            'tried_to_throw_ur_husband1_desc2' => 'Tried To Throw Ur Husband1 Desc2',
            'tried_to_throw_ur_husband1_desc3' => 'Tried To Throw Ur Husband1 Desc3',
            'tried_to_throw_things_on_ur_partner1_desc1' => 'Tried To Throw Things On Ur Partner1 Desc1',
            'tried_to_throw_things_on_ur_partner1_desc2' => 'Tried To Throw Things On Ur Partner1 Desc2',
            'tried_to_throw_things_on_ur_partner1_desc3' => 'Tried To Throw Things On Ur Partner1 Desc3',
            'tried_to_hold_ur_partners_neck1_desc1' => 'Tried To Hold Ur Partners Neck1 Desc1',
            'tried_to_hold_ur_partners_neck1_desc2' => 'Tried To Hold Ur Partners Neck1 Desc2',
            'tried_to_hold_ur_partners_neck1_desc3' => 'Tried To Hold Ur Partners Neck1 Desc3',
            'tried_to_hit_kick_bite1_desc1' => 'Tried To Hit Kick Bite1 Desc1',
            'tried_to_hit_kick_bite1_desc2' => 'Tried To Hit Kick Bite1 Desc2',
            'tried_to_hit_kick_bite1_desc3' => 'Tried To Hit Kick Bite1 Desc3',
            'tried_to_hit_or_hitted1_desc1' => 'Tried To Hit Or Hitted1 Desc1',
            'tried_to_hit_or_hitted1_desc2' => 'Tried To Hit Or Hitted1 Desc2',
            'tried_to_hit_or_hitted1_desc3' => 'Tried To Hit Or Hitted1 Desc3',
            'hitted_ur_partner1_desc1' => 'Hitted Ur Partner1 Desc1',
            'hitted_ur_partner1_desc2' => 'Hitted Ur Partner1 Desc2',
            'hitted_ur_partner1_desc3' => 'Hitted Ur Partner1 Desc3',
            'feared_ur_partner_by_gun_knife1_desc1' => 'Feared Ur Partner By Gun Knife1 Desc1',
            'feared_ur_partner_by_gun_knife1_desc2' => 'Feared Ur Partner By Gun Knife1 Desc2',
            'feared_ur_partner_by_gun_knife1_desc3' => 'Feared Ur Partner By Gun Knife1 Desc3',
            'used_gun_knife1_desc1' => 'Used Gun Knife1 Desc1',
            'used_gun_knife1_desc2' => 'Used Gun Knife1 Desc2',
            'used_gun_knife1_desc3' => 'Used Gun Knife1 Desc3',
            'rotated_ur_partner_hand12_desc1' => 'Rotated Ur Partner Hand12 Desc1',
            'rotated_ur_partner_hand12_desc2' => 'Rotated Ur Partner Hand12 Desc2',
            'grabed_and_pushed_ur_husband12_desc1' => 'Grabed And Pushed Ur Husband12 Desc1',
            'grabed_and_pushed_ur_husband12_desc2' => 'Grabed And Pushed Ur Husband12 Desc2',
            'slapped_ur_husband12_desc1' => 'Slapped Ur Husband12 Desc1',
            'slapped_ur_husband12_desc2' => 'Slapped Ur Husband12 Desc2',
            'forced_ur_partner_for_sex12_desc1' => 'Forced Ur Partner For Sex12 Desc1',
            'forced_ur_partner_for_sex12_desc2' => 'Forced Ur Partner For Sex12 Desc2',
            'shaked_ur_partner_by_holding_tight12_desc1' => 'Shaked Ur Partner By Holding Tight12 Desc1',
            'shaked_ur_partner_by_holding_tight12_desc2' => 'Shaked Ur Partner By Holding Tight12 Desc2',
            'tried_to_throw_ur_husband12_desc1' => 'Tried To Throw Ur Husband12 Desc1',
            'tried_to_throw_ur_husband12_desc2' => 'Tried To Throw Ur Husband12 Desc2',
            'tried_to_throw_things_on_ur_partner12_desc1' => 'Tried To Throw Things On Ur Partner12 Desc1',
            'tried_to_throw_things_on_ur_partner12_desc2' => 'Tried To Throw Things On Ur Partner12 Desc2',
            'tried_to_hold_ur_partners_neck12_desc1' => 'Tried To Hold Ur Partners Neck12 Desc1',
            'tried_to_hold_ur_partners_neck12_desc2' => 'Tried To Hold Ur Partners Neck12 Desc2',
            'tried_to_hit_kick_bite12_desc1' => 'Tried To Hit Kick Bite12 Desc1',
            'tried_to_hit_kick_bite12_desc2' => 'Tried To Hit Kick Bite12 Desc2',
            'tried_to_hit_or_hitted12_desc1' => 'Tried To Hit Or Hitted12 Desc1',
            'tried_to_hit_or_hitted12_desc2' => 'Tried To Hit Or Hitted12 Desc2',
            'hitted_ur_partner12_desc1' => 'Hitted Ur Partner12 Desc1',
            'hitted_ur_partner12_desc2' => 'Hitted Ur Partner12 Desc2',
            'feared_ur_partner_by_gun_knife12_desc1' => 'Feared Ur Partner By Gun Knife12 Desc1',
            'feared_ur_partner_by_gun_knife12_desc2' => 'Feared Ur Partner By Gun Knife12 Desc2',
            'used_gun_knife12_desc1' => 'Used Gun Knife12 Desc1',
            'used_gun_knife12_desc2' => 'Used Gun Knife12 Desc2',
            'rotated_ur_partner_hand123_desc1' => 'Rotated Ur Partner Hand123 Desc1',
            'rotated_ur_partner_hand123_desc2' => 'Rotated Ur Partner Hand123 Desc2',
            'grabed_and_pushed_ur_husband123_desc1' => 'Grabed And Pushed Ur Husband123 Desc1',
            'grabed_and_pushed_ur_husband123_desc2' => 'Grabed And Pushed Ur Husband123 Desc2',
            'slapped_ur_husband123_desc1' => 'Slapped Ur Husband123 Desc1',
            'slapped_ur_husband123_desc2' => 'Slapped Ur Husband123 Desc2',
            'forced_ur_partner_for_sex123_desc1' => 'Forced Ur Partner For Sex123 Desc1',
            'forced_ur_partner_for_sex123_desc2' => 'Forced Ur Partner For Sex123 Desc2',
            'shaked_ur_partner_by_holding_tight123_desc1' => 'Shaked Ur Partner By Holding Tight123 Desc1',
            'shaked_ur_partner_by_holding_tight123_desc2' => 'Shaked Ur Partner By Holding Tight123 Desc2',
            'tried_to_throw_ur_husband123_desc1' => 'Tried To Throw Ur Husband123 Desc1',
            'tried_to_throw_ur_husband123_desc2' => 'Tried To Throw Ur Husband123 Desc2',
            'tried_to_throw_things_on_ur_partner123_desc1' => 'Tried To Throw Things On Ur Partner123 Desc1',
            'tried_to_throw_things_on_ur_partner123_desc2' => 'Tried To Throw Things On Ur Partner123 Desc2',
            'tried_to_hold_ur_partners_neck123_desc1' => 'Tried To Hold Ur Partners Neck123 Desc1',
            'tried_to_hold_ur_partners_neck123_desc2' => 'Tried To Hold Ur Partners Neck123 Desc2',
            'tried_to_hit_kick_bite123_desc1' => 'Tried To Hit Kick Bite123 Desc1',
            'tried_to_hit_kick_bite123_desc2' => 'Tried To Hit Kick Bite123 Desc2',
            'tried_to_hit_or_hitted123_desc1' => 'Tried To Hit Or Hitted123 Desc1',
            'tried_to_hit_or_hitted123_desc2' => 'Tried To Hit Or Hitted123 Desc2',
            'hitted_ur_partner123_desc1' => 'Hitted Ur Partner123 Desc1',
            'hitted_ur_partner123_desc2' => 'Hitted Ur Partner123 Desc2',
            'feared_ur_partner_by_gun_knife123_desc1' => 'Feared Ur Partner By Gun Knife123 Desc1',
            'feared_ur_partner_by_gun_knife123_desc2' => 'Feared Ur Partner By Gun Knife123 Desc2',
            'used_gun_knife123_desc1' => 'Used Gun Knife123 Desc1',
            'used_gun_knife123_desc2' => 'Used Gun Knife123 Desc2',
        ];
    }

      public function beforeSave($insert)
    {
        return true;
    }



public function afterFind() {
    return true; //don't forget this
}
}
