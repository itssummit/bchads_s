<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "breastfeeding".
 *
 * @property integer $id
 * @property integer $mother_id
 * @property integer $assessment_id
 * @property string $created_dtm
 * @property string $last_updated_dtm
 * @property string $updated_by
 * @property integer $determine_enough_milk
 * @property integer $successfully_cope
 * @property integer $without_formula
 * @property integer $lached_properly
 * @property integer $manage_situation
 * @property integer $manage_crying
 * @property integer $wanting_breastfeed
 * @property integer $comfortable_family_members
 * @property integer $satisfied_experience
 * @property integer $deal_time_consuming
 * @property integer $finish_1breast
 * @property integer $continue_feeding
 * @property integer $keepup_baby_demands
 * @property integer $tell_finished
 * @property integer $score
 * @property integer $scale_id
 */
class Breastfeeding extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'breastfeeding';
    }
	
	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mother_id', 'assessment_id', 'determine_enough_milk', 'successfully_cope', 'without_formula', 'lached_properly', 'manage_situation', 'manage_crying', 'wanting_breastfeed', 'comfortable_family_members', 'satisfied_experience', 'deal_time_consuming', 'finish_1breast', 'continue_feeding', 'keepup_baby_demands', 'tell_finished', 'score', 'scale_id'], 'integer'],
            [['created_dtm', 'last_updated_dtm'], 'safe'],
            [['updated_by'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mother_id' => 'Respondent ID',
            'assessment_id' => 'assessment ID',
            'created_dtm' => 'Created Date',
            'last_updated_dtm' => 'Last Updated Date',
            'updated_by' => 'Updated By',
            'determine_enough_milk' => 'Determine Enough Milk',
            'successfully_cope' => 'Successfully Cope',
            'without_formula' => 'Without Formula',
            'lached_properly' => 'Lached Properly',
            'manage_situation' => 'Manage Situation',
            'manage_crying' => 'Manage Crying',
            'wanting_breastfeed' => 'Wanting Breastfeed',
            'comfortable_family_members' => 'Comfortable Family Members',
            'satisfied_experience' => 'Satisfied Experience',
            'deal_time_consuming' => 'Deal Time Consuming',
            'finish_1breast' => 'Finish 1breast',
            'continue_feeding' => 'Continue Feeding',
            'keepup_baby_demands' => 'Keepup Baby Demands',
            'tell_finished' => 'Tell Finished',
            'score' => 'Score',
            'scale_id' => 'Scale ID',
        ];
    }

      public function beforeSave($insert)
    {
        return true;
    }



public function afterFind() {
    return true; //don't forget this
}
}
