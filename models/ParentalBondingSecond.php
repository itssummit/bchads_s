<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "parental_bonding_second".
 *
 * @property integer $id
 * @property integer $less_helps_mother
 * @property integer $less_understands
 * @property integer $less_praise_mother
 * @property integer $more_secure_mother
 * @property integer $allow_decisions_mother
 * @property integer $allowstyle_mother
 * @property integer $less_books_mother
 * @property integer $less_stydy_mother
 * @property integer $family_burden_mother
 * @property integer $show_olavu_mother
 * @property integer $show_nirlakshya_mother
 * @property integer $less_helpings_mother
 * @property integer $less_undestnds_mother
 * @property integer $less_praises_mother
 * @property integer $high_safes_mother
 * @property integer $allows_decionss_mother
 * @property integer $allow_style_as_boys_mother
 * @property integer $less_resources_mother
 * @property integer $less_oportunity_study_mother
 * @property integer $boon_mother
 * @property integer $jokes_onme_mother
 * @property integer $respects_me_mother
 * @property integer $less_helps_father
 * @property integer $less_understands_father
 * @property integer $less_praise_father
 * @property integer $more_secure_father
 * @property integer $allow_decisions_father
 * @property integer $allowstyle_father
 * @property integer $less_books_father
 * @property integer $less_stydy_father
 * @property integer $family_burden_father
 * @property integer $show_olavu_father
 * @property integer $show_nirlakshya_father
 * @property integer $less_helpings_father
 * @property integer $less_undestnds_father
 * @property integer $less_praises_father
 * @property integer $high_safes_father
 * @property integer $allows_decionss_father
 * @property integer $allow_style_as_boys_father
 * @property integer $less_resources_father
 * @property integer $less_oportunity_study_father
 * @property integer $boon_father
 * @property integer $jokes_onme_father
 * @property integer $respects_me_father
 * @property integer $respondent_id
 * @property integer $assessment_id
 * @property string $created_dtm
 * @property string $last_updated_dtm
 * @property string $updated_by
 * @property integer $score
 * @property integer $scale_id
 */
class ParentalBondingSecond extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parental_bonding_second';
    }
	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['less_helps_mother', 'less_understands_mother', 'less_praise_mother', 'more_secure_mother', 'allow_decisions_mother', 'allowstyle_mother', 'less_books_mother', 'less_stydy_mother', 'family_burden_mother', 'show_olavu_mother', 'show_nirlakshya_mother', 'less_helpings_mother', 'less_undestnds_mother', 'less_praises_mother', 'high_safes_mother', 'allows_decionss_mother', 'allow_style_as_boys_mother', 'less_resources_mother', 'less_oportunity_study_mother', 'boon_mother', 'jokes_onme_mother', 'respects_me_mother', 'less_helps_father', 'less_understands_father', 'less_praise_father', 'more_secure_father', 'allow_decisions_father', 'allowstyle_father', 'less_books_father', 'less_stydy_father', 'family_burden_father', 'show_olavu_father', 'show_nirlakshya_father', 'less_helpings_father', 'less_undestnds_father', 'less_praises_father', 'high_safes_father', 'allows_decionss_father', 'allow_style_as_boys_father', 'less_resources_father', 'less_oportunity_study_father', 'boon_father', 'jokes_onme_father', 'respects_me_father', 'respondent_id', 'assessment_id', 'score', 'scale_id'], 'integer'],
            [['created_dtm', 'last_updated_dtm'], 'safe'],
            [['any_sisters','updated_by','no_of_brothers','no_of_sisters','no_of_cousins','no_of_cousines_brothers','not_applicable','any_siblings'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'less_helps_mother' => 'Less Helps Mother',
            'less_understands' => 'Less Understands',
            'less_praise_mother' => 'Less Praise Mother',
            'more_secure_mother' => 'More Secure Mother',
            'allow_decisions_mother' => 'Allow Decisions Mother',
            'allowstyle_mother' => 'Allowstyle Mother',
            'less_books_mother' => 'Less Books Mother',
            'less_stydy_mother' => 'Less Stydy Mother',
            'family_burden_mother' => 'Family Burden Mother',
            'show_olavu_mother' => 'Show Olavu Mother',
            'show_nirlakshya_mother' => 'Show Nirlakshya Mother',
            'less_helpings_mother' => 'Less Helpings Mother',
            'less_undestnds_mother' => 'Less Undestnds Mother',
            'less_praises_mother' => 'Less Praises Mother',
            'high_safes_mother' => 'High Safes Mother',
            'allows_decionss_mother' => 'Allows Decionss Mother',
            'allow_style_as_boys_mother' => 'Allow Style As Boys Mother',
            'less_resources_mother' => 'Less Resources Mother',
            'less_oportunity_study_mother' => 'Less Oportunity Study Mother',
            'boon_mother' => 'Boon Mother',
            'jokes_onme_mother' => 'Jokes Onme Mother',
            'respects_me_mother' => 'Respects Me Mother',
            'less_helps_father' => 'Less Helps Father',
            'less_understands_father' => 'Less Understands Father',
            'less_praise_father' => 'Less Praise Father',
            'more_secure_father' => 'More Secure Father',
            'allow_decisions_father' => 'Allow Decisions Father',
            'allowstyle_father' => 'Allowstyle Father',
            'less_books_father' => 'Less Books Father',
            'less_stydy_father' => 'Less Stydy Father',
            'family_burden_father' => 'Family Burden Father',
            'show_olavu_father' => 'Show Olavu Father',
            'show_nirlakshya_father' => 'Show Nirlakshya Father',
            'less_helpings_father' => 'Less Helpings Father',
            'less_undestnds_father' => 'Less Undestnds Father',
            'less_praises_father' => 'Less Praises Father',
            'high_safes_father' => 'High Safes Father',
            'allows_decionss_father' => 'Allows Decionss Father',
            'allow_style_as_boys_father' => 'Allow Style As Boys Father',
            'less_resources_father' => 'Less Resources Father',
            'less_oportunity_study_father' => 'Less Oportunity Study Father',
            'boon_father' => 'Boon Father',
            'jokes_onme_father' => 'Jokes Onme Father',
            'respects_me_father' => 'Respects Me Father',
            'respondent_id' => 'Respondent ID',
            'assessment_id' => 'Assessment ID',
            'created_dtm' => 'Created Dtm',
            'last_updated_dtm' => 'Last Updated Dtm',
            'updated_by' => 'Updated By',
            'score' => 'Score',
            'scale_id' => 'Scale ID',
        ];
    }
}
