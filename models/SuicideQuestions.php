<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "suicide_questions".
 *
 * @property integer $id
 * @property integer $assessment_id
 * @property string $created_dtm
 * @property string $last_updated_dtm
 * @property string $updated_by
 * @property integer $ideas
 * @property integer $plan_attempted
 * @property integer $current_pregnancy_thought_ending_life
 * @property integer $current_pregnancy_plan
 * @property integer $current_pregnancy_attempt
 * @property integer $times_attempted
 * @property integer $reasons_attempt
 * @property integer $method_attempt
 * @property integer $score
 * @property integer $respondent_id
 */
class SuicideQuestions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'suicide_questions';
    }
	
	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['assessment_id', 'mother_id' ,'ideas', 'plan_attempted','current_pregnancy_thought_ending_life', 'current_pregnancy_plan', 'current_pregnancy_attempt', 'score','scale_id'], 'integer'],
            [['created_dtm', 'last_updated_dtm'], 'safe'],
            [['updated_by','not_applicable','reasons_attempt','method_attempt','specify_others','times_attempted'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'assessment_id' => 'assessment ID',
            'created_dtm' => 'Created Date',
            'last_updated_dtm' => 'Last Updated Date',
            'updated_by' => 'Updated By',
            'ideas' => 'Ideas',
            'plan_attempted' => 'Plan Attempted',
            'current_pregnancy_thought_ending_life' => 'Current Pregnancy Thought Ending Life',
            'current_pregnancy_plan' => 'Current Pregnancy Plan',
            'current_pregnancy_attempt' => 'Current Pregnancy Attempt',
            'times_attempted' => '',
            'reasons_attempt' => '',
            'method_attempt' => 'Method Attempt',
            'score' => 'Score',
            'mother_id' => 'Patient ID',
        ];
    }

      public function beforeSave($insert)
    {
        return true;
    }



public function afterFind() {
    return true; //don't forget this
}
}
