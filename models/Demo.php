<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "demo.demo".
 *
 * @property integer $id
 * @property string $name
 * @property integer $age
 * @property string $state
 */
class Demo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'demo.demo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['age'], 'integer'],
            [['name', 'state'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'age' => 'Age',
            'state' => 'State',
        ];
    }
}
