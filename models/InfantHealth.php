<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "infant_health".
 *
 * @property integer $id
 * @property integer $mother_id
 * @property integer $assessment_id
 * @property string $created_dtm
 * @property string $last_updated_dtm
 * @property string $updated_by
 * @property integer $hospitalization
 * @property integer $nicu_care
 * @property integer $neonatal_jaundice
 * @property integer $icu_care
 * @property integer $fevers
 * @property integer $exanthaemotous_fever
 * @property integer $seizures
 * @property integer $asthma
 * @property integer $respiratory_illness
 * @property integer $diarrhoea
 * @property integer $pediatrician_visits
 * @property integer $score
 * @property integer $scale_id
 */
class InfantHealth extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'infant_health';
    }
	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mother_id', 'assessment_id','score', 'scale_id'], 'integer'],
            [['created_dtm', 'last_updated_dtm','dod'], 'safe'],
            [['other_details','updated_by','hospitalization', 'nicu_care', 'neonatal_jaundice', 'icu_care', 'fevers', 'exanthaemotous_fever', 'seizures', 'asthma', 'respiratory_illness', 'diarrhoea', 'pediatrician_visits','others','others_notes','t1_5m','t2_5m','t3_5m','t9m','t16_24m',
			'doc_visit1', 'doc_visit2','doc_visit3','doc_visit4','doc_visit5','doc_visit6', 'doc_visit7','doc_visit8','doc_visit9','doc_visit10','doc_visit11', 'doc_visit12','doc_visit13','doc_visit14','doc_visit15','doc_visit16', 'doc_visit17','doc_visit18','doc_visit19','doc_visit20','doc_visit21', 'doc_visit22','doc_visit23','doc_visit24','doc_visit25','doc_visit26', 'doc_visit27','doc_visit28','doc_visit29','doc_visit30','doc_visit31', 'doc_visit32','doc_visit33','doc_visit34','doc_visit35','what_did_person_with_child_do_about_burnt_others','what_did_person_with_child_do_about_dropp_others','what_did_person_with_child_do_about_swallow_others','what_did_person_with_child_do_about_other_others'], 'string', 'max' => 255],
			[['how_many_times_burnt','how_old_was_she_while_burnt','how_old_was_she_while_burnt2','how_old_was_she_while_burnt3','what_did_person_with_child_do_about_burnt','how_old_was_she_while_dropped2','how_old_was_she_while_dropped3',
			'how_many_times_dropped','how_old_was_she_while_dropped','what_did_person_with_child_do_about_dropp',
			'how_many_times_swallowed','how_old_was_she_while_swallowed','what_did_person_with_child_do_about_swallow','how_old_was_she_while_swallowed2','how_old_was_she_while_swallowed3',
			'how_many_times_others','how_old_was_she_while_other','what_did_person_with_child_do_about_other','how_old_was_she_while_other2','how_old_was_she_while_other3',
			'how_often_on_weekdays','how_often_at_weekdays','valid','ageofinfant'],'integer'],
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mother_id' => 'Patient ID',
            'assessment_id' => 'Assessment ID',
            'created_dtm' => 'Created Date',
            'last_updated_dtm' => 'Last Updated Date',
            'updated_by' => 'Updated By',
            'hospitalization' => '',
            'nicu_care' => '',
            'neonatal_jaundice' => '',
            'icu_care' => '',
            'fevers' => '',
            'exanthaemotous_fever' => ' ',
            'seizures' => '',
            'asthma' => '',
            'respiratory_illness' => ' ',
            'diarrhoea' => '',
            'pediatrician_visits' => ' ',
            'score' => 'Score',
            'scale_id' => 'Scale ID',
			'dod' => '',
			'ageofinfant' => 'I',
			'others' => 'O',
            'valid' => 'valid',
        ];
    }

      public function beforeSave($insert)
    {
        return true;
    }



public function afterFind() {
    return true; //don't forget this
}
}
