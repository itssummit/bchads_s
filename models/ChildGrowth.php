<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "child_growth".
 *
 * @property integer $id
 * @property integer $respondent_id
 * @property integer $assessment_id
 * @property string $created_dtm
 * @property string $last_updated_dtm
 * @property string $updated_by
 * @property string $length
 * @property string $weight
 * @property string $head_circumference
 * @property string $chest_circumference
 * @property string $mid_arm_circumference
 * @property string $waist_circumference
 * @property string $hip_circumference
 * @property string $sub_capular_skin_fold_thickness
 */
class ChildGrowth extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'child_growth';
    }
	
	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['respondent_id', 'assessment_id'], 'integer'],
            [['created_dtm', 'last_updated_dtm'], 'safe'],
            [['updated_by', 'length', 'weight', 'head_circumference', 'chest_circumference', 'mid_arm_circumference', 'waist_circumference', 'hip_circumference', 'sub_capular_skin_fold_thickness'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'respondent_id' => 'Patient ID',
            'assessment_id' => 'assessment ID',
            'created_dtm' => 'Created Date',
            'last_updated_dtm' => 'Last Updated Date',
            'updated_by' => 'Updated By',
            'length' => 'Length',
            'weight' => 'Weight',
            'head_circumference' => 'Head Circumference',
            'chest_circumference' => 'Chest Circumference',
            'mid_arm_circumference' => 'Mid Arm Circumference',
            'waist_circumference' => 'Waist Circumference',
            'hip_circumference' => 'Hip Circumference',
            'sub_capular_skin_fold_thickness' => 'Sub Capular Skin Fold Thickness',
        ];
    }

      public function beforeSave($insert)
    {
        return true;
    }



public function afterFind() {
    return true; //don't forget this
}
}
