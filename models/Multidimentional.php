<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "multidimentional".
 *
 * @property integer $id
 * @property integer $mother_id
 * @property integer $assessment_id
 * @property string $created_dtm
 * @property string $last_updated_dtm
 * @property string $updated_by
 * @property integer $special_person_need
 * @property integer $special_person_share
 * @property integer $family_help
 * @property integer $emotional_help_family
 * @property integer $special_person_comfort
 * @property integer $friend_help
 * @property integer $friends_things_go_wrong
 * @property integer $problems_family_talk
 * @property integer $friends_share
 * @property integer $special_person_feelings
 * @property integer $family_help_decision
 * @property integer $talk_problems_friends
 * @property integer $score
 * @property integer $scale_id
 */
class Multidimentional extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'multidimentional';
    }

	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mother_id', 'assessment_id', 'special_person_need', 'special_person_share', 'family_help', 'emotional_help_family', 'special_person_comfort', 'friend_help', 'friends_things_go_wrong', 'problems_family_talk', 'friends_share', 'special_person_feelings', 'family_help_decision', 'talk_problems_friends', 'score', 'scale_id'], 'integer'],
            [['created_dtm', 'last_updated_dtm'], 'safe'],
            [['updated_by'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mother_id' => 'Patient ID',
            'assessment_id' => 'Assessment ID',
            'created_dtm' => 'Created Date',
            'last_updated_dtm' => 'Last Updated Date',
            'updated_by' => 'Updated By',
            'special_person_need' => 'Special Person Need',
            'special_person_share' => 'Special Person Share',
            'family_help' => 'Family Help',
            'emotional_help_family' => 'Emotional Help Family',
            'special_person_comfort' => 'Special Person Comfort',
            'friend_help' => 'Friend Help',
            'friends_things_go_wrong' => 'Friends Things Go Wrong',
            'problems_family_talk' => 'Problems Family Talk',
            'friends_share' => 'Friends Share',
            'special_person_feelings' => 'Special Person Feelings',
            'family_help_decision' => 'Family Help Decision',
            'talk_problems_friends' => 'Talk Problems Friends',
            'score' => 'Score',
            'scale_id' => 'Scale ID',
        ];
    }

      public function beforeSave($insert)
    {
        return true;
    }



public function afterFind() {
    return true; //don't forget this
}
}
