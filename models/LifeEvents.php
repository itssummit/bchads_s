<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "life_events".
 *
 * @property integer $id
 * @property integer $mother_id
 * @property integer $assessment_id
 * @property string $created_dtm
 * @property string $last_updated_dtm
 * @property string $updated_by
 * @property integer $death_spouse
 * @property integer $divorce
 * @property integer $marital_separation
 * @property integer $death_family_member
 * @property integer $personal_injury
 * @property integer $marriage
 * @property integer $marital_reconcilation
 * @property integer $health_family_member
 * @property integer $change_financial_status
 * @property integer $death_close_friend
 * @property integer $change_argument_spouse
 * @property integer $large_loan
 * @property integer $trouble_in-laws
 * @property integer $spouse_work
 * @property integer $change_living_conditions
 * @property integer $change_working_hours
 * @property integer $change_residence
 * @property integer $major_health_family_member
 * @property integer $deterioration_finance
 * @property integer $conflict_parents
 * @property integer $argument_spouse
 * @property integer $deterioration_living_conditions
 * @property integer $abortion
 * @property integer $miscarriage
 * @property integer $diabetes
 * @property integer $pre-eclamptic
 * @property integer $eclampsia
 * @property integer $medication_exposure
 * @property integer $neonatal_death
 * @property integer $score
 */
class LifeEvents extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'life_events';
    }
	
	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mother_id', 'assessment_id','scale_id', 'death_spouse', 'divorce', 'marital_separation', 'death_family_member', 'personal_injury', 'marriage', 'marital_reconcilation', 'health_family_member', 'change_financial_status', 'death_close_friend', 'change_argument_spouse', 'large_loan', 'trouble_in_laws', 'spouse_work', 'change_living_conditions', 'change_working_hours', 'change_residence', 'major_health_family_member', 'deterioration_finance', 'conflict_parents', 'argument_spouse', 'deterioration_living_conditions','score','noofyes','abortion','miscarriage','diabetes','pre_eclamptic','eclampsia','medication_exposure','neonatal_death'], 'integer'],
            [['created_dtm', 'last_updated_dtm'], 'safe'],
            [['updated_by'], 'string', 'max' => 45],
			[['other_answeres_about_delivery','not_app'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mother_id' => 'Patient ID',
            'assessment_id' => 'assessment ID',
            'created_dtm' => 'Created Date',
            'last_updated_dtm' => 'Last Updated Date',
            'updated_by' => 'Updated By',
            'death_spouse' => 'Death Spouse',
            'divorce' => 'Divorce',
            'marital_separation' => 'Marital Separation',
            'death_family_member' => 'Death Family Member',
            'personal_injury' => 'Personal Injury',
            'marriage' => 'Marriage',
            'marital_reconcilation' => 'Marital Reconcilation',
            'health_family_member' => 'Health Family Member',
            'change_financial_status' => 'Change Financial Status',
            'death_close_friend' => 'Death Close Friend',
            'change_argument_spouse' => 'Change Argument Spouse',
            'large_loan' => 'Large Loan',
            'trouble_in_laws' => 'Trouble In Laws',
            'spouse_work' => 'Spouse Work',
            'change_living_conditions' => 'Change Living Conditions',
            'change_working_hours' => 'Change Working Hours',
            'change_residence' => 'Change Residence',
            'major_health_family_member' => 'Major Health Family Member',
            'deterioration_finance' => 'Deterioration Finance',
            'conflict_parents' => 'Conflict Parents',
            'argument_spouse' => 'Argument Spouse',
            'deterioration_living_conditions' => 'Deterioration Living Conditions',
			'score' => 'Score',
			'scale_id'=>'Scale Id',
        ];
    }

      public function beforeSave($insert)
    {
        return true;
    }



public function afterFind() {
    return true; //don't forget this
}
}
