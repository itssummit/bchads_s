<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "psychosocial_risk_factors".
 *
 * @property integer $id
 * @property integer $respondent_id
 * @property integer $assessment_id
 * @property string $created_dtm
 * @property string $last_updated_dtm
 * @property string $updated_by
 * @property integer $stress_issues
 * @property integer $lack_care
 * @property integer $violence_experience
 * @property integer $anxiety_issues
 * @property integer $mood_issues
 * @property integer $suicidal_ideation
 * @property integer $poor_pregnancy
 * @property integer $traumatic_events
 * @property integer $substance_use_spouse
 * @property integer $health_problem_of_familymember
 * @property string $delivery_postpartum
 * @property integer $score
 * @property integer $scale_id
 * @property integer $mother_id
 */
class PsychosocialRiskFactors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'psychosocial_risk_factors';
    }
	
	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['respondent_id', 'assessment_id', 'score', 'scale_id', 'mother_id'], 'integer'],
            [['created_dtm', 'last_updated_dtm'], 'safe'],
            [['mother_id'], 'required'],
            [['updated_by', 'stress_issues','financial','work','others', 'lack_care', 'spouse','in_laws','parents','violence_experience', 'psychological','physical','sexual','anxiety_issues', 'fear_about_pregnancy','fear_about_childbirth','general_worry','mood_issues', 'feeling_low','loss_of_interest','crying_spells','suicidal_ideation', 'lifetime_attempt','ideation_during_past_current_pregnancy','postpartum','poor_pregnancy','neonatal_death','still_birth','any_congenital_anomalies', 'traumatic_events', 'death_of_close_one','major_loss','substance_use_spouse', 'health_problem_of_familymember','delivery_postpartum','complications_related_to_labour_delivery_childbirth','obstetric_trauma','poor_obg_outcome','gender_of_the_infant','not_applicable','others_desc','relation','complications_related_to_labour_delivery_childbirth1','complications_related_to_labour_delivery_childbirth2','poor_obg_outcome1','poor_obg_outcome2','delivery_and_postpartum','valid'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'respondent_id' => 'Respondent ID',
            'assessment_id' => 'Assessment ID',
            'created_dtm' => 'Created Dtm',
            'last_updated_dtm' => 'Last Updated Dtm',
            'updated_by' => 'Updated By',
            'stress_issues' => 'Stress Issues',
            'lack_care' => 'Lack Care',
            'violence_experience' => 'Violence Experience',
            'anxiety_issues' => 'Anxiety Issues',
            'mood_issues' => 'Mood Issues',
            'suicidal_ideation' => 'Suicidal Ideation',
            'poor_pregnancy' => 'Poor Pregnancy',
            'traumatic_events' => 'Traumatic Events',
            'substance_use_spouse' => 'Substance Use Spouse',
            'health_problem_of_familymember' => 'Health Problem Of Familymember',
            'delivery_postpartum' => 'Delivery Postpartum',
            'score' => 'Score',
            'scale_id' => 'Scale ID',
            'mother_id' => 'Mother ID',
            'valid' => 'valid'
        ];
    }

    
	  public function beforeSave($insert)
    {
        return true;
    }



public function afterFind() {
    return true; //don't forget this
}
}
