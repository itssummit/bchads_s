<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "maternal".
 *
 * @property integer $id
 * @property integer $mother_id
 * @property integer $assessment_id
 * @property string $created_dtm
 * @property string $last_updated_dtm
 * @property string $updated_by
 * @property integer $physical_health_problems
 * @property integer $emotional_problem
 * @property integer $medicines_taken
 * @property integer $sleepin_pills
 * @property integer $anxiety_pills
 * @property integer $depression_pills
 * @property integer $sleep
 * @property integer $hours_of_sleep
 * @property integer $persistant_pain
 * @property integer $pregnant_aftr_firstborn
 * @property string $last_mensural_date
 * @property string $status_presentpregnancy
 * @property string $dob_baby
 * @property string $sex_baby
 * @property integer $score
 */
class Maternal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'maternal';
    }
	
	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mother_id', 'assessment_id','scale_id', 'physical_health_problems', 'emotional_problem', 'medicines_taken', 'sleep', 'hours_of_sleep', 'persistant_pain', 'pregnant_aftr_firstborn', 'score'], 'integer'],
            [['created_dtm', 'last_updated_dtm', 'last_mensural_date', 'dob_baby'], 'safe'],
            [['updated_by', 'status_presentpregnancy', 'sex_baby','not_applicable'], 'string', 'max' => 45],
			[['physical_health_problems_det','emotional_problem_det','medicines_taken_det'],'string','max' =>255],
			[['persistant_pain_dur','persistant_pain_site','last_menstrual_date_dont_know', 'sleepin_pills','sleepin_pills2','sleepin_pills3', 'anxiety_pills', 'depression_pills2','depression_pills3','depression_pills','anxiety_pills2','anxiety_pills3'],'string','max' =>45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mother_id' => 'Patient ID',
            'assessment_id' => 'assessment ID',
            'created_dtm' => 'Created Date',
            'last_updated_dtm' => 'Last Updated Date',
            'updated_by' => 'Updated By',
            'physical_health_problems' => 'Physical Health Problems',
            'emotional_problem' => 'Emotional Problem',
            'medicines_taken' => 'Medicines Taken',
            'sleepin_pills' => '',
			'sleepin_pills2' => '',
			'sleepin_pills3' => '',
            'anxiety_pills' => '',
			'anxiety_pills2' => '',
			'anxiety_pills3' => '',
            'depression_pills' => '',
			'depression_pills2' => '',
			'depression_pills3' => '',
            'sleep' => 'Sleep',
            'hours_of_sleep' => 'Hours Of Sleep',
            'persistant_pain' => 'Persistant Pain',
            'pregnant_aftr_firstborn' => 'Pregnant Aftr Firstborn',
            'last_mensural_date' => 'Last Mensural Date',
            'status_presentpregnancy' => 'Status Presentpregnancy',
            'dob_baby' => 'Dob Baby',
            'sex_baby' => 'Sex Baby',
            'score' => 'Score',
        ];
    }

      public function beforeSave($insert)
    {
        return true;
    }



public function afterFind() {
    return true; //don't forget this
}
}
