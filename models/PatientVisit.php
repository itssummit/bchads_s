<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "patient_visit".
 *
 * @property integer $id
 * @property integer $respondent_id
 * @property string $created_dtm
 * @property string $updated_date
 * @property string $updated_by
 */
class PatientVisit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'patient_visit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['respondent_id'], 'integer'],
            [['created_dtm', 'last_updated_dtm'], 'safe'],
            [['updated_by'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'respondent_id' => 'Patient ID',
            'created_dtm' => 'Created Date',
            'last_updated_dtm' => 'Updated Date',
            'updated_by' => 'Updated By',
        ];
    }
}
