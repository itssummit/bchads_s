<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "phychological_violence".
 *
 * @property integer $id
 * @property integer $respondent_id
 * @property integer $assessment_id
 * @property string $created_dtm
 * @property string $last_updated_dtm
 * @property string $updated_by
 * @property integer $insulted
 * @property integer $threatened
 * @property integer $threatened_parents
 * @property integer $sent_parent_house
 * @property integer $financial_hardships
 * @property integer $frightening_look
 * @property integer $proved_unfaithful
 * @property integer $indifference
 * @property integer $deprived_social_rights
 * @property integer $negleted
 * @property integer $denial_personal_needs
 * @property integer $non-involvement_decision_making
 * @property integer $restriction_mobility
 * @property integer $physical_assault
 * @property integer $scald_burnt
 * @property integer $coerced_sex
 * @property integer $denial_sex
 * @property integer $sexual_hurt
 * @property integer $score
 * @property integer $scale_id
 */
class PhychologicalViolence extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'phychological_violence';
    }
	
	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['respondent_id', 'assessment_id', 'insulted', 'threatened', 'threatened_parents', 'sent_parent_house', 'financial_hardships', 'frightening_look', 'proved_unfaithful', 'indifference', 'deprived_social_rights', 'negleted', 'denial_personal_needs', 'non-involvement_decision_making', 'restriction_mobility', 'physical_assault', 'scald_burnt', 'coerced_sex', 'denial_sex', 'sexual_hurt', 'score', 'scale_id'], 'integer'],
            [['created_dtm', 'last_updated_dtm'], 'safe'],
            [['updated_by'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'respondent_id' => 'Respondent ID',
            'assessment_id' => 'Assessment ID',
            'created_dtm' => 'Created Date',
            'last_updated_dtm' => 'Last Updated Date',
            'updated_by' => 'Updated By',
            'insulted' => 'Insulted',
            'threatened' => 'Threatened',
            'threatened_parents' => 'Threatened Parents',
            'sent_parent_house' => 'Sent Parent House',
            'financial_hardships' => 'Financial Hardships',
            'frightening_look' => 'Frightening Look',
            'proved_unfaithful' => 'Proved Unfaithful',
            'indifference' => 'Indifference',
            'deprived_social_rights' => 'Deprived Social Rights',
            'negleted' => 'Negleted',
            'denial_personal_needs' => 'Denial Personal Needs',
            'non-involvement_decision_making' => 'Non Involvement Decision Making',
            'restriction_mobility' => 'Restriction Mobility',
            'physical_assault' => 'Physical Assault',
            'scald_burnt' => 'Scald Burnt',
            'coerced_sex' => 'Coerced Sex',
            'denial_sex' => 'Denial Sex',
            'sexual_hurt' => 'Sexual Hurt',
            'score' => 'Score',
            'scale_id' => 'Scale ID',
        ];
    } 

     public function beforeSave($insert)
    {
        return true;
    }



public function afterFind() {
    return true; //don't forget this
}
}
