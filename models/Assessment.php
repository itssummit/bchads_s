<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "assessment".
 *
 * @property integer $id
 * @property integer $respondent_id
 * @property string $created_date
 * @property string $updated_date
 * @property string $updated_by
 * @property string $visit_date
 * @property string $assessment_type
 * @property string $respondent
 */
class Assessment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'assessment';
    }
	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mother_id','respondent_id','is_there_acg','relation_with_child'], 'integer'],
            [['created_dtm', 'last_updated_dtm', 'visit_date' ,'assessment_date'], 'safe'],
            [['updated_by', 'respondent','place_of_assesment','assessment_by','caregiver','assessment_date_ac',
			'place_of_assessment_ac','assessment_by_ac'], 'string', 'max' => 45],
            [['relation_type'], 'string', 'max' => 255],
            [['assessment_type'], 'string', 'max' => 10],
            [['specify_other'],'string','max'=> 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mother_id' => 'Mother ID',
            'created_dtm' => 'Created Date',
            'last_updated_dtm' => 'Updated Date',
            'updated_by' => 'Updated By',
            'visit_date' => 'Visit Date',
            'assessment_type' => 'Assessment Type',
            'respondent' => 'Respondent',
			'is_there_acg' =>'Is there ACG?',
            'acg_specify_other'=>'Specify Others'
        ];
    }
}
