<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sass".
 *
 * @property integer $id
 * @property integer $mother_id
 * @property integer $assessment_id
 * @property string $created_dtm
 * @property string $last_updated_dtm
 * @property string $updated_by
 * @property integer $head_ache
 * @property integer $back_pain
 * @property integer $hand_leg_pain
 * @property integer $stomach_pain
 * @property integer $body_pain
 * @property integer $jadatva
 * @property integer $cold_hot
 * @property integer $high_heart_beat
 * @property integer $stomach_prob
 * @property integer $burn
 * @property integer $physical_weakness
 * @property integer $psychological_weakness
 * @property integer $unconscious
 * @property integer $shivering
 * @property integer $tired
 * @property integer $no_sleep
 * @property integer $no_hungry
 * @property integer $no_interest
 * @property integer $constipation
 * @property integer $loose_motions
 * @property integer $leg_shake
 * @property integer $legs_pain
 * @property integer $disease_symptoms
 * @property integer $walks_relief
 * @property integer $night_symptoms_increases
 * @property integer $sleep_prob
 * @property integer $ringing_sounds
 * @property integer $vometings
 * @property integer $urine
 * @property integer $yoni
 * @property integer $pelvic
 * @property integer $biliseragu
 * @property integer $any_other
 */
class Sass extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sass';
    }
	
	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mother_id', 'assessment_id', 'head_ache', 'back_pain', 'hand_leg_pain', 'stomach_pain', 'body_pain', 'jadatva', 'cold_hot', 'high_heart_beat', 'stomach_prob', 'burn', 'physical_weakness', 'psychological_weakness', 'unconscious', 'shivering', 'tired', 'no_sleep', 'no_hungry', 'no_interest', 'constipation', 'loose_motions','leg_unrest', 'leg_shake', 'legs_pain', 'disease_symptoms', 'walks_relief', 'night_symptoms_increases', 'sleep_prob', 'ringing_sounds', 'vometings', 'urine', 'yoni', 'pelvic', 'biliseragu', 'any_other','score','scale_id'], 'integer'],
            [['created_dtm', 'last_updated_dtm'], 'safe'],
            [['updated_by','symptoms_others'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mother_id' => 'Patient ID',
            'assessment_id' => 'assessment ID',
            'created_dtm' => 'Created Date',
            'last_updated_dtm' => 'Last Updated Date',
            'updated_by' => 'Updated By',
            'head_ache' => 'Head Ache',
            'back_pain' => 'Back Pain',
            'hand_leg_pain' => 'Hand Leg Pain',
            'stomach_pain' => 'Stomach Pain',
            'body_pain' => 'Body Pain',
            'jadatva' => 'Jadatva',
            'cold_hot' => 'Cold Hot',
            'high_heart_beat' => 'High Heart Beat',
            'stomach_prob' => 'Stomach Prob',
            'burn' => 'Burn',
            'physical_weakness' => 'Physical Weakness',
            'psychological_weakness' => 'Psychological Weakness',
            'unconscious' => 'Unconscious',
            'shivering' => 'Shivering',
            'tired' => 'Tired',
            'no_sleep' => 'No Sleep',
            'no_hungry' => 'No Hungry',
            'no_interest' => 'No Interest',
            'constipation' => 'Constipation',
            'loose_motions' => 'Loose Motions',
            'leg_shake' => 'Leg Shake',
			'leg_unrest' => 'Leg Unrest',
            'legs_pain' => 'Legs Pain',
            'disease_symptoms' => 'Disease Symptoms',
            'walks_relief' => 'Walks Relief',
            'night_symptoms_increases' => 'Night Symptoms Increases',
            'sleep_prob' => 'Sleep Prob',
            'ringing_sounds' => 'Ringing Sounds',
            'vometings' => 'Vometings',
            'urine' => 'Urine',
            'yoni' => 'Yoni',
            'pelvic' => 'Pelvic',
            'biliseragu' => 'Biliseragu',
            'any_other' => 'Any Other',
			'score' => 'Score',
			'scale_id' => 'Scale ID',
        ];
    }

      public function beforeSave($insert)
    {
        return true;
    }



public function afterFind() {
    return true; //don't forget this
}
}
