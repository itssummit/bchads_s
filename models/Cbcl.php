<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cbcl".
 *
 * @property integer $id
 * @property integer $mother_id
 * @property integer $assessment_id
 * @property string $last_updated_dtm
 * @property string $updated_by
 * @property integer $pain
 * @property string $created_dtm
 * @property integer $behave_childish
 * @property integer $fear_for_doing_new_things
 * @property integer $far_eye_contact
 * @property integer $less_concentration
 * @property integer $patience_or_active
 * @property integer $impatience_for_displacing_object
 * @property integer $no_waiting_habit
 * @property integer $chewing_non_eatable_things
 * @property integer $depending_on_elders
 * @property integer $frequently_asks_help
 * @property integer $dont_poop
 * @property integer $cries_more
 * @property integer $cruel_for_animals
 * @property integer $perseverance
 * @property integer $finish_demands_suddenly
 * @property integer $destroys_his_or_her_belongings
 * @property integer $destroys_family_or_other_children_things
 * @property integer $loose_motion_or_Diarrhea
 * @property integer $doesnt_work_what_people_say
 * @property integer $irritation_for_change_of_regular_daily_plans
 * @property integer $doesnt_like_to_sleep_alone
 * @property integer $doesnt_respond_to_people
 * @property integer $doesnt_eats_well_explain
 * @property integer $doesnt_be_with_other_children
 * @property integer $behaves_like_elders_or_doesnt_enjoy
 * @property integer $doesnt_feels_bad_after_mischievous_things
 * @property integer $doesnt_like_to_go_outside
 * @property integer $easily_depressed
 * @property integer $easily_feels_jealous
 * @property integer $eat_or_drinks_non_eatable_food
 * @property integer $fear_for_certain_animals_at_certain_place_or_situation
 * @property integer $suddenly_hurts_his_or_her_feelings
 * @property integer $will_have_more_pain_or_accidented
 * @property integer $involves_in_many_fights
 * @property integer $interfere_in_every_matter
 * @property integer $feels_bad_when_he_or_she_is_away_from_their_parents
 * @property integer $disturbance_for_sleeping
 * @property integer $headache_without_medical_reasons
 * @property integer $hits_others
 * @property integer $holds_breath
 * @property integer $disturbing_animals_or_people_without_intention
 * @property integer $depressed_without_reason
 * @property integer $angry
 * @property integer $vomitting_sensation_or_unhealthy_without_medical_reason
 * @property integer $energyless_or_shewering_of_body_parts
 * @property integer $bad_dreams
 * @property integer $eats_more
 * @property integer $more_tired
 * @property integer $fears_without_reason
 * @property integer $pain_while_pooping_without_medical_reason
 * @property integer $suddenly_attacks_people_physically
 * @property integer $tears_skin_or_nose_or_other_body_parts
 * @property integer $plays_more_with_sex_organs
 * @property integer $doesnt_co_operate_or_doesnt_finishes_work_neatly
 * @property integer $eye_problem_without_medical_reason
 * @property integer $doesnt_changes_behaviour_even_after_punishment
 * @property integer $from_one_activity_to_another_changes_immediately
 * @property integer $itchyness_or_skin_problems_without_medical_reason
 * @property integer $disagrees_food
 * @property integer $disagrress_playing_more_active_games
 * @property integer $frequently_shakes_his_head_or_body_parts
 * @property integer $disagrees_to_sleep_at_night
 * @property integer $disagrees_toilet_coaching
 * @property integer $screeches_more
 * @property integer $looks_like_they_doesnt_respond_for_love_and_affection
 * @property integer $feels_shy_easily
 * @property integer $selfish_or_doesnt_share
 * @property integer $doesnt_show_love_for_people
 * @property integer $shows_little_interest_with_surrounding_things
 * @property integer $doesnt_fear_if_he_knows_it_will_pain_also
 * @property integer $feels_more_shy
 * @property integer $sleeps_less_in_day_night_when_compared_to_normal_children
 * @property integer $plays_with_recess_or_poop
 * @property integer $problem_in_speaking
 * @property integer $staring_continuosly_at_only_side_or_thinking_something_else
 * @property integer $stomach_ache_without_medical_reason
 * @property integer $change_in_feelings
 * @property integer $different_behaviour
 * @property integer $irritating_or_getting_angry
 * @property integer $sudden_difference_in_behaviour
 * @property integer $gets_more_angry
 * @property integer $speaks_or_cry_while_sleeping
 * @property integer $fusses_more
 * @property integer $cares_about_neatness
 * @property integer $fears_more
 * @property integer $feels_helpless_or_doesnt_listen_to_elders_words
 * @property integer $does_less_activities_or_walks_slowly
 * @property integer $not_happy_or_being_sad
 * @property integer $speaks_at_high_voice
 * @property integer $feels_sad_with_new_people_or_situation
 * @property integer $vomit
 * @property integer $frequently_wakesup_at_night
 * @property integer $walks_without_holding_hand
 * @property integer $wants_to_look_after_him_for_most_of_the_time
 * @property integer $murmuring
 * @property integer $feels_shy_or_doesnt_be_with_others
 * @property integer $thinks
 * @property integer $list_other_problems_if_he_or_she_has
 * @property integer $bites_other_children
 * @property integer $kicks_other_children
 * @property integer $helps_other_children
 * @property integer $beats_other_children
 * @property integer $share_with_other_children
 * @property integer $laughs_at_other_children
 * @property integer $score
 * @property integer $scale_id
 * @property integer $energyless_or_feared_person
 */
class Cbcl extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cbcl';
    }
	
	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mother_id', 'assessment_id', 'pain', 'behave_childish', 'fear_for_doing_new_things', 'far_eye_contact', 'less_concentration', 'patience_or_active', 'impatience_for_displacing_object', 'no_waiting_habit', 'chewing_non_eatable_things', 'depending_on_elders', 'frequently_asks_help', 'dont_poop', 'cries_more', 'cruel_for_animals', 'perseverance', 'finish_demands_suddenly', 'destroys_his_or_her_belongings', 'destroys_family_or_other_children_things', 'loose_motion_or_Diarrhea', 'doesnt_work_what_people_say', 'irritation_for_change_of_regular_daily_plans', 'doesnt_like_to_sleep_alone', 'doesnt_respond_to_people', 'doesnt_eats_well_explain', 'doesnt_be_with_other_children', 'behaves_like_elders_or_doesnt_enjoy', 'doesnt_feels_bad_after_mischievous_things', 'doesnt_like_to_go_outside', 'easily_depressed', 'easily_feels_jealous', 'eat_or_drinks_non_eatable_food', 'fear_for_certain_animals_at_certain_place_or_situation', 'suddenly_hurts_his_or_her_feelings', 'will_have_more_pain_or_accidented', 'involves_in_many_fights', 'interfere_in_every_matter', 'feels_bad_when_he_or_she_is_away_from_their_parents', 'disturbance_for_sleeping', 'headache_without_medical_reasons', 'hits_others', 'holds_breath', 'disturbing_animals_or_people_without_intention', 'depressed_without_reason', 'angry', 'vomitting_sensation_or_unhealthy_without_medical_reason', 'energyless_or_shewering_of_body_parts', 'bad_dreams', 'eats_more', 'more_tired', 'fears_without_reason', 'pain_while_pooping_without_medical_reason', 'suddenly_attacks_people_physically', 'tears_skin_or_nose_or_other_body_parts', 'plays_more_with_sex_organs', 'doesnt_co_operate_or_doesnt_finishes_work_neatly', 'eye_problem_without_medical_reason', 'doesnt_changes_behaviour_even_after_punishment', 'from_one_activity_to_another_changes_immediately', 'itchyness_or_skin_problems_without_medical_reason', 'disagrees_food', 'disagrress_playing_more_active_games', 'frequently_shakes_his_head_or_body_parts', 'disagrees_to_sleep_at_night', 'disagrees_toilet_coaching', 'screeches_more', 'looks_like_they_doesnt_respond_for_love_and_affection', 'feels_shy_easily', 'selfish_or_doesnt_share', 'doesnt_show_love_for_people', 'shows_little_interest_with_surrounding_things', 'doesnt_fear_if_he_knows_it_will_pain_also', 'feels_more_shy', 'sleeps_less_in_day_night_when_compared_to_normal_children', 'plays_with_recess_or_poop', 'problem_in_speaking', 'staring_continuosly_at_only_side_or_thinking_something_else', 'stomach_ache_without_medical_reason', 'change_in_feelings', 'different_behaviour', 'irritating_or_getting_angry', 'sudden_difference_in_behaviour', 'gets_more_angry', 'speaks_or_cry_while_sleeping', 'fusses_more', 'cares_about_neatness', 'fears_more', 'feels_helpless_or_doesnt_listen_to_elders_words', 'does_less_activities_or_walks_slowly', 'not_happy_or_being_sad', 'speaks_at_high_voice', 'feels_sad_with_new_people_or_situation', 'vomit', 'frequently_wakesup_at_night', 'walks_without_holding_hand', 'wants_to_look_after_him_for_most_of_the_time', 'murmuring', 'feels_shy_or_doesnt_be_with_others', 'thinks', 'list_other_problems_if_he_or_she_has', 'bites_other_children', 'kicks_other_children', 'helps_other_children', 'beats_other_children', 'share_with_other_children', 'laughs_at_other_children', 'score', 'scale_id', 'energyless_or_feared_person'], 'integer'],
            [['last_updated_dtm', 'created_dtm'], 'safe'],
            [['updated_by','not_applicable','doesnt_eats_well_explain_det','energyless_or_shewering_of_body_parts_det','tears_skin_or_nose_or_other_body_parts_det','eye_problem_without_medical_reason_det','disagrees_toilet_coaching_det','sleeps_less_in_day_night_when_compared_to_normal_children_det','problem_in_speaking_det','different_behaviour_det','feels_sad_with_new_people_or_situation_det','list_other_problems_if_he_or_she_has_det','visheshachethana','visheshachethana_det','athiyada_kalaji'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mother_id' => 'Respondent ID',
            'assessment_id' => 'Assessment ID',
            'last_updated_dtm' => 'Last Updated Dtm',
            'updated_by' => 'Updated By',
            'pain' => 'Pain',
            'created_dtm' => 'Created Dtm',
            'behave_childish' => 'Behave Childish',
            'fear_for_doing_new_things' => 'Fear For Doing New Things',
            'far_eye_contact' => 'Far Eye Contact',
            'less_concentration' => 'Less Concentration',
            'patience_or_active' => 'Patience Or Active',
            'impatience_for_displacing_object' => 'Impatience For Displacing Object',
            'no_waiting_habit' => 'No Waiting Habit',
            'chewing_non_eatable_things' => 'Chewing Non Eatable Things',
            'depending_on_elders' => 'Depending On Elders',
            'frequently_asks_help' => 'Frequently Asks Help',
            'dont_poop' => 'Dont Poop',
            'cries_more' => 'Cries More',
            'cruel_for_animals' => 'Cruel For Animals',
            'perseverance' => 'Perseverance',
            'finish_demands_suddenly' => 'Finish Demands Suddenly',
            'destroys_his_or_her_belongings' => 'Destroys His Or Her Belongings',
            'destroys_family_or_other_children_things' => 'Destroys Family Or Other Children Things',
            'loose_motion_or_Diarrhea' => 'Loose Motion Or  Diarrhea',
            'doesnt_work_what_people_say' => 'Doesnt Work What People Say',
            'irritation_for_change_of_regular_daily_plans' => 'Irritation For Change Of Regular Daily Plans',
            'doesnt_like_to_sleep_alone' => 'Doesnt Like To Sleep Alone',
            'doesnt_respond_to_people' => 'Doesnt Respond To People',
            'doesnt_eats_well_explain' => 'Doesnt Eats Well Explain',
            'doesnt_be_with_other_children' => 'Doesnt Be With Other Children',
            'behaves_like_elders_or_doesnt_enjoy' => 'Behaves Like Elders Or Doesnt Enjoy',
            'doesnt_feels_bad_after_mischievous_things' => 'Doesnt Feels Bad After Mischievous Things',
            'doesnt_like_to_go_outside' => 'Doesnt Like To Go Outside',
            'easily_depressed' => 'Easily Depressed',
            'easily_feels_jealous' => 'Easily Feels Jealous',
            'eat_or_drinks_non_eatable_food' => 'Eat Or Drinks Non Eatable Food',
            'fear_for_certain_animals_at_certain_place_or_situation' => 'Fear For Certain Animals At Certain Place Or Situation',
            'suddenly_hurts_his_or_her_feelings' => 'Suddenly Hurts His Or Her Feelings',
            'will_have_more_pain_or_accidented' => 'Will Have More Pain Or Accidented',
            'involves_in_many_fights' => 'Involves In Many Fights',
            'interfere_in_every_matter' => 'Interfere In Every Matter',
            'feels_bad_when_he_or_she_is_away_from_their_parents' => 'Feels Bad When He Or She Is Away From Their Parents',
            'disturbance_for_sleeping' => 'Disturbance For Sleeping',
            'headache_without_medical_reasons' => 'Headache Without Medical Reasons',
            'hits_others' => 'Hits Others',
            'holds_breath' => 'Holds Breath',
            'disturbing_animals_or_people_without_intention' => 'Disturbing Animals Or People Without Intention',
            'depressed_without_reason' => 'Depressed Without Reason',
            'angry' => 'Angry',
            'vomitting_sensation_or_unhealthy_without_medical_reason' => 'Vomitting Sensation Or Unhealthy Without Medical Reason',
            'energyless_or_shewering_of_body_parts' => 'Energyless Or Shewering Of Body Parts',
            'bad_dreams' => 'Bad Dreams',
            'eats_more' => 'Eats More',
            'more_tired' => 'More Tired',
            'fears_without_reason' => 'Fears Without Reason',
            'pain_while_pooping_without_medical_reason' => 'Pain While Pooping Without Medical Reason',
            'suddenly_attacks_people_physically' => 'Suddenly Attacks People Physically',
            'tears_skin_or_nose_or_other_body_parts' => 'Tears Skin Or Nose Or Other Body Parts',
            'plays_more_with_sex_organs' => 'Plays More With Sex Organs',
            'doesnt_co_operate_or_doesnt_finishes_work_neatly' => 'Doesnt Co Operate Or Doesnt Finishes Work Neatly',
            'eye_problem_without_medical_reason' => 'Eye Problem Without Medical Reason',
            'doesnt_changes_behaviour_even_after_punishment' => 'Doesnt Changes Behaviour Even After Punishment',
            'from_one_activity_to_another_changes_immediately' => 'From One Activity To Another Changes Immediately',
            'itchyness_or_skin_problems_without_medical_reason' => 'Itchyness Or Skin Problems Without Medical Reason',
            'disagrees_food' => 'Disagrees Food',
            'disagrress_playing_more_active_games' => 'Disagrress Playing More Active Games',
            'frequently_shakes_his_head_or_body_parts' => 'Frequently Shakes His Head Or Body Parts',
            'disagrees_to_sleep_at_night' => 'Disagrees To Sleep At Night',
            'disagrees_toilet_coaching' => 'Disagrees Toilet Coaching',
            'screeches_more' => 'Screeches More',
            'looks_like_they_doesnt_respond_for_love_and_affection' => 'Looks Like They Doesnt Respond For Love And Affection',
            'feels_shy_easily' => 'Feels Shy Easily',
            'selfish_or_doesnt_share' => 'Selfish Or Doesnt Share',
            'doesnt_show_love_for_people' => 'Doesnt Show Love For People',
            'shows_little_interest_with_surrounding_things' => 'Shows Little Interest With Surrounding Things',
            'doesnt_fear_if_he_knows_it_will_pain_also' => 'Doesnt Fear If He Knows It Will Pain Also',
            'feels_more_shy' => 'Feels More Shy',
            'sleeps_less_in_day_night_when_compared_to_normal_children' => 'Sleeps Less In Day Night When Compared To Normal Children',
            'plays_with_recess_or_poop' => 'Plays With Recess Or Poop',
            'problem_in_speaking' => 'Problem In Speaking',
            'staring_continuosly_at_only_side_or_thinking_something_else' => 'Staring Continuosly At Only Side Or Thinking Something Else',
            'stomach_ache_without_medical_reason' => 'Stomach Ache Without Medical Reason',
            'change_in_feelings' => 'Change In Feelings',
            'different_behaviour' => 'Different Behaviour',
            'irritating_or_getting_angry' => 'Irritating Or Getting Angry',
            'sudden_difference_in_behaviour' => 'Sudden Difference In Behaviour',
            'gets_more_angry' => 'Gets More Angry',
            'speaks_or_cry_while_sleeping' => 'Speaks Or Cry While Sleeping',
            'fusses_more' => 'Fusses More',
            'cares_about_neatness' => 'Cares About Neatness',
            'fears_more' => 'Fears More',
            'feels_helpless_or_doesnt_listen_to_elders_words' => 'Feels Helpless Or Doesnt Listen To Elders Words',
            'does_less_activities_or_walks_slowly' => 'Does Less Activities Or Walks Slowly',
            'not_happy_or_being_sad' => 'Not Happy Or Being Sad',
            'speaks_at_high_voice' => 'Speaks At High Voice',
            'feels_sad_with_new_people_or_situation' => 'Feels Sad With New People Or Situation',
            'vomit' => 'Vomit',
            'frequently_wakesup_at_night' => 'Frequently Wakesup At Night',
            'walks_without_holding_hand' => 'Walks Without Holding Hand',
            'wants_to_look_after_him_for_most_of_the_time' => 'Wants To Look After Him For Most Of The Time',
            'murmuring' => 'Murmuring',
            'feels_shy_or_doesnt_be_with_others' => 'Feels Shy Or Doesnt Be With Others',
            'thinks' => 'Thinks',
            'list_other_problems_if_he_or_she_has' => 'List Other Problems If He Or She Has',
            'bites_other_children' => 'Bites Other Children',
            'kicks_other_children' => 'Kicks Other Children',
            'helps_other_children' => 'Helps Other Children',
            'beats_other_children' => 'Beats Other Children',
            'share_with_other_children' => 'Share With Other Children',
            'laughs_at_other_children' => 'Laughs At Other Children',
            'score' => 'Score',
            'scale_id' => 'Scale ID',
            'energyless_or_feared_person' => 'Energyless Or Feared Person',
        ];
    } 


     public function beforeSave($insert)
    {
        return true;
    }



public function afterFind() {
    return true; //don't forget this
}
}
