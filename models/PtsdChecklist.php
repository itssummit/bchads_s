<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ptsd_checklist".
 *
 * @property integer $id
 * @property integer $mother_id
 * @property string $created_dtm
 * @property string $last_updated_dtm
 * @property string $updated_by
 * @property integer $intrusive_recollect
 * @property integer $flashbacks
 * @property integer $upset_remainders
 * @property integer $distressing_dreams
 * @property integer $physical_remainders
 * @property integer $avoid_thoughts
 * @property integer $avoid_remainders
 * @property integer $psychogenic_amnesia
 * @property integer $anhedonia
 * @property integer $estrangement_others
 * @property integer $psychic_numbing
 * @property integer $foreshortened_future
 * @property integer $sleep_difficulty
 * @property integer $irritability
 * @property integer $concentrattion_impaired
 * @property integer $hyper_vigilant
 * @property integer $exaggerated_startle
 * @property integer $score
 */
class PtsdChecklist extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ptsd_checklist';
    }
	
	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mother_id','assessment_id','scale_id', 'intrusive_recollect', 'flashbacks', 'upset_remainders', 'distressing_dreams', 'physical_remainders', 'avoid_thoughts', 'avoid_remainders', 'psychogenic_amnesia', 'anhedonia', 'estrangement_others', 'psychic_numbing', 'foreshortened_future', 'sleep_difficulty', 'irritability', 'concentrattion_impaired', 'hyper_vigilant', 'exaggerated_startle', 'score'], 'integer'],
            [['created_dtm', 'last_updated_dtm'], 'safe'],
            [['updated_by'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mother_id' => 'Patient ID',
            'created_dtm' => 'Created Date',
            'last_updated_dtm' => 'Last Updated Date',
            'updated_by' => 'Updated By',
            'intrusive_recollect' => 'Intrusive Recollect',
            'flashbacks' => 'Flashbacks',
            'upset_remainders' => 'Upset Remainders',
            'distressing_dreams' => 'Distressing Dreams',
            'physical_remainders' => 'Physical Remainders',
            'avoid_thoughts' => 'Avoid Thoughts',
            'avoid_remainders' => 'Avoid Remainders',
            'psychogenic_amnesia' => 'Psychogenic Amnesia',
            'anhedonia' => 'Anhedonia',
            'estrangement_others' => 'Estrangement Others',
            'psychic_numbing' => 'Psychic Numbing',
            'foreshortened_future' => 'Foreshortened Future',
            'sleep_difficulty' => 'Sleep Difficulty',
            'irritability' => 'Irritability',
            'concentrattion_impaired' => 'Concentrattion Impaired',
            'hyper_vigilant' => 'Hyper Vigilant',
            'exaggerated_startle' => 'Exaggerated Startle',
            'score' => 'Score',
        ];
    }

      public function beforeSave($insert)
    {
        return true;
    }



public function afterFind() {
    return true; //don't forget this
}
}
