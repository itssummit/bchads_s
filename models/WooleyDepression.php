<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "wooley_depression".
 *
 * @property integer $id
 * @property integer $mother_id
 * @property integer $assessment_id
 * @property string $last_updated_dtm
 * @property string $depressed_pastmonth
 * @property integer $little_interest/pleasure_pastmonth
 * @property integer $women_yes_needhelp
 * @property integer $score
 * @property string $updated_by
 * @property string $created_dtm
 */
class WooleyDepression extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wooley_depression';
    }
	
	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mother_id', 'assessment_id','scale_id','depressed_pastmonth','little_interest_pleasure_pastmonth', 'score'], 'integer'],
			[['depressed_pastmonth', 'little_interest_pleasure_pastmonth'], 'default', 'value' => -5],
            [['last_updated_dtm', 'created_dtm'], 'safe'],
            [['updated_by', 'women_yes_needhelp'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mother_id' => 'Patient ID',
            'assessment_id' => 'assessment ID',
            'last_updated_dtm' => 'Last Update Date',
            'depressed_pastmonth' => 'Depressed Pastmonth',
            'little_interest_pleasure_pastmonth' => 'Little Interest/pleasure Pastmonth',
            'women_yes_needhelp' => '',
            'score' => 'Score',
			'updated_by' => 'Updated By',
            'created_dtm' => 'Created Date',
        ];
    }
	public function beforeSave($insert)
	{
        return true;
	}



public function afterFind() {
    return true; //don't forget this
}
}
