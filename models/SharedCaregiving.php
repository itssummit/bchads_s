<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shared_caregiving".
 *
 * @property integer $id
 * @property integer $respondent_id
 * @property integer $assessment_id
 * @property string $created_dtm
 * @property string $last_updated_dtm
 * @property string $updated_by
 * @property integer $family_type
 * @property integer $number_adults
 * @property integer $number_infants
 * @property integer $number_children_preschool
 * @property integer $number_children
 * @property integer $number_adolescents
 * @property integer $current_stay
 * @property string $care_carer
 * @property string $care_myself
 * @property string $care_husband
 * @property string $care_maternal_grandfather
 * @property string $care_maternal_grandmother
 * @property string $care_paternal_grandfather
 * @property string $care_paternal_grandmother
 * @property string $care_siblings
 * @property integer $played
 * @property integer $taken_out
 * @property integer $introduced_newthings
 * @property integer $told_not_todo_something
 * @property integer $cleaned
 * @property integer $rocked
 * @property integer $carried
 * @property integer $scolded
 * @property integer $wasnt_too_hot
 * @property integer $care_sick
 * @property integer $taken_harm
 * @property integer $fed
 * @property integer $kissed
 * @property integer $cooed
 * @property integer $put_sleep
 * @property integer $bathed
 * @property integer $didnt_eat_harmful
 * @property integer $soothed
 * @property integer $massaged
 * @property integer $showed_what_todo
 * @property integer $cuddled_crying
 * @property integer $dressed
 * @property integer $told_off
 * @property integer $comfortable
 * @property integer $slept_beside
 * @property integer $introduced_new_people
 * @property integer $scolded_crying
 * @property integer $cradled_crying
 * @property integer $made_warm
 * @property integer $gone_wokeup
 * @property integer $prepared_formula_milk
 * @property integer $talked
 * @property integer $sung
 * @property integer $cuddled
 * @property integer $taken_nurse
 * @property integer $talked_sternly
 * @property integer $held
 * @property integer $shared_smiles
 * @property integer $distracted_crying
 * @property integer $showed_rightthing
 * @property integer $taken_visit_family
 * @property integer $cradled_arms
 * @property integer $fed_crying
 * @property integer $prayed
 * @property integer $invovled_family_rituals
 * @property integer $score
 * @property integer $scale_id
 * @property string $songs
 * @property string $any_others
 * @property integer $thoogiddene
 * @property string $other_activity
 * @property string $current_stay_others
 * @property integer $mother_id
 * @property string $songs_acg
 * @property string $songs_acg_lastweek
 * @property string $played_acg
 * @property string $played_acg_lastweek
 * @property string $taken_out_acg
 * @property string $taken_out_acg_lastweek
 * @property string $introduced_newthings_acg
 * @property string $introduced_newthings_acg_lastweek
 * @property string $told_not_todo_something_acg
 * @property string $told_not_todo_something_acg_lastweek
 * @property string $cleaned_acg
 * @property string $cleaned_acg_lastweek
 * @property string $rocked_acg
 * @property string $rocked_acg_lastweek
 * @property string $carried_acg
 * @property string $carried_acg_lastweek
 * @property string $scolded_acg
 * @property string $scolded_acg_lastweek
 * @property string $wasnt_too_hot_acg
 * @property string $wasnt_too_hot_acg_lastweek
 * @property string $care_sick_acg
 * @property string $care_sick_acg_lastweek
 * @property string $taken_harm_acg
 * @property string $taken_harm_acg_lastweek
 * @property string $fed_acg
 * @property string $fed_acg_lastweek
 * @property string $kissed_acg
 * @property string $kissed_acg_lastweek
 * @property string $cooed_acg
 * @property string $cooed_acg_lastweek
 * @property string $put_sleep_acg
 * @property string $put_sleep_acg_lastweek
 * @property string $bathed_acg
 * @property string $bathed_acg_lastweek
 * @property string $didnt_eat_harmful_acg
 * @property string $didnt_eat_harmful_acg_lastweek
 * @property string $soothed_acg
 * @property string $soothed_acg_lastweek
 * @property string $massaged_acg
 * @property string $massaged_acg_lastweek
 * @property string $showed_what_todo_acg
 * @property string $showed_what_todo_acg_lastweek
 * @property string $thoogiddene_acg
 * @property string $thoogiddene_acg_lastweek
 * @property string $cuddled_crying_acg
 * @property string $cuddled_crying_acg_lastweek
 * @property string $dressed_acg
 * @property string $dressed_acg_lastweek
 * @property string $told_off_acg
 * @property string $told_off_acg_lastweek
 * @property string $comfortable_acg
 * @property string $comfortable_acg_lastweek
 * @property string $slept_beside_acg
 * @property string $slept_beside_acg_lastweek
 * @property string $introduced_new_people_acg
 * @property string $introduced_new_people_acg_lastweek
 * @property string $_acg
 * @property string $_acg_lastweek
 * @property string $scolded_crying_acg
 * @property string $scolded_crying_acg_lastweek
 * @property string $cradled_crying_acg
 * @property string $cradled_crying_acg_lastweek
 * @property string $made_warm_acg
 * @property string $made_warm_acg_lastweek
 * @property string $gone_wokeup_acg
 * @property string $gone_wokeup_acg_lastweek
 * @property string $prepared_formula_milk_acg
 * @property string $prepared_formula_milk_acg_lastweek
 * @property string $talked_acg
 * @property string $talked_acg_lastweek
 * @property string $cuddled_acg
 * @property string $cuddled_acg_lastweek
 * @property string $sung_acg
 * @property string $sung_acg_lastweek
 * @property string $taken_nurse_acg
 * @property string $taken_nurse_acg_lastweek
 * @property string $talked_sternly_acg
 * @property string $talked_sternly_acg_lastweek
 * @property string $held_acg
 * @property string $held_acg_lastweek
 * @property string $shared_smiles_acg
 * @property string $shared_smiles_acg_lastweek
 * @property string $distracted_crying_acg
 * @property string $distracted_crying_acg_lastweek
 * @property string $showed_rightthing_acg
 * @property string $showed_rightthing_acg_lastweek
 * @property string $taken_visit_family_acg
 * @property string $taken_visit_family_acg_lastweek
 * @property string $cradled_arms_acg
 * @property string $cradled_arms_acg_lastweek
 * @property string $fed_crying_acg
 * @property string $fed_crying_acg_lastweek
 * @property string $prayed_acg
 * @property string $prayed_acg_lastweek
 * @property string $invovled_family_rituals_acg
 * @property string $invovled_family_rituals_acg_lastweek
 * @property string $songs_other
 * @property string $played_other
 * @property string $taken_out_other
 * @property string $introduced_newthings_other
 * @property string $told_not_todo_something_other
 * @property string $cleaned_other
 * @property string $rocked_other
 * @property string $carried_other
 * @property string $scolded_other
 * @property string $wasnt_too_hot_other
 * @property string $care_sick_other
 * @property string $taken_harm_other
 * @property string $fed_other
 * @property string $kissed_other
 * @property string $cooed_other
 * @property string $put_sleep_other
 * @property string $bathed_other
 * @property string $didnt_eat_harmful_other
 * @property string $soothed_other
 * @property string $massaged_other
 * @property string $showed_what_todo_other
 * @property string $thoogiddene_other
 * @property string $cuddled_crying_other
 * @property string $dressed_other
 * @property string $told_off_other
 * @property string $comfortable_other
 * @property string $slept_beside_other
 * @property string $introduced_new_people_other
 * @property string $_other
 * @property string $scolded_crying_other
 * @property string $cradled_crying_other
 * @property string $made_warm_other
 * @property string $gone_wokeup_other
 * @property string $prepared_formula_milk_other
 * @property string $talked_other
 * @property string $cuddled_other
 * @property string $sung_other
 * @property string $taken_nurse_other
 * @property string $talked_sternly_other
 * @property string $held_other
 * @property string $shared_smiles_other
 * @property string $distracted_crying_other
 * @property string $showed_rightthing_other
 * @property string $taken_visit_family_other
 * @property string $cradled_arms_other
 * @property string $fed_crying_other
 * @property string $prayed_other
 * @property string $invovled_family_rituals_other
 */
class SharedCaregiving extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shared_caregiving';
    }
	
	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['respondent_id', 'assessment_id', 'family_type', 'number_adults', 'number_infants', 'number_children_preschool', 'number_children', 'number_adolescents', 'current_stay', 'played', 'taken_out', 'introduced_newthings', 'told_not_todo_something', 'cleaned', 'rocked', 'carried', 'scolded', 'wasnt_too_hot', 'care_sick', 'taken_harm', 'fed', 'kissed', 'cooed', 'put_sleep', 'bathed', 'didnt_eat_harmful', 'soothed', 'massaged', 'showed_what_todo', 'cuddled_crying', 'dressed', 'told_off', 'comfortable', 'slept_beside', 'introduced_new_people', 'scolded_crying', 'cradled_crying', 'made_warm', 'gone_wokeup', 'prepared_formula_milk', 'talked', 'sung', 'cuddled', 'taken_nurse', 'talked_sternly', 'held', 'shared_smiles', 'distracted_crying', 'showed_rightthing', 'taken_visit_family', 'cradled_arms', 'fed_crying', 'prayed', 'invovled_family_rituals', 'score', 'scale_id', 'thoogiddene', 'mother_id'], 'integer'],
            [['created_dtm', 'last_updated_dtm'], 'safe'],
            [['mother_id'], 'required'],
            [['other_activity_acg','updated_by', 'care_carer', 'care_myself', 'care_husband', 'care_maternal_grandfather', 'care_maternal_grandmother', 'care_paternal_grandfather', 'care_paternal_grandmother', 'care_siblings', 'songs', 'any_others', 'other_activity', 'current_stay_others', 'songs_acg', 'songs_acg_lastweek', 'played_acg', 'played_acg_lastweek', 'taken_out_acg', 'taken_out_acg_lastweek', 'introduced_newthings_acg', 'introduced_newthings_acg_lastweek', 'told_not_todo_something_acg', 'told_not_todo_something_acg_lastweek', 'cleaned_acg', 'cleaned_acg_lastweek', 'rocked_acg', 'rocked_acg_lastweek', 'carried_acg', 'carried_acg_lastweek', 'scolded_acg', 'scolded_acg_lastweek', 'wasnt_too_hot_acg', 'wasnt_too_hot_acg_lastweek', 'care_sick_acg', 'care_sick_acg_lastweek', 'taken_harm_acg', 'taken_harm_acg_lastweek', 'fed_acg', 'fed_acg_lastweek', 'kissed_acg', 'kissed_acg_lastweek', 'cooed_acg', 'cooed_acg_lastweek', 'put_sleep_acg', 'put_sleep_acg_lastweek', 'bathed_acg', 'bathed_acg_lastweek', 'didnt_eat_harmful_acg', 'didnt_eat_harmful_acg_lastweek', 'soothed_acg', 'soothed_acg_lastweek', 'massaged_acg', 'massaged_acg_lastweek', 'showed_what_todo_acg', 'showed_what_todo_acg_lastweek', 'thoogiddene_acg', 'thoogiddene_acg_lastweek', 'cuddled_crying_acg', 'cuddled_crying_acg_lastweek', 'dressed_acg', 'dressed_acg_lastweek', 'told_off_acg', 'told_off_acg_lastweek', 'comfortable_acg', 'comfortable_acg_lastweek', 'slept_beside_acg', 'slept_beside_acg_lastweek', 'introduced_new_people_acg', 'introduced_new_people_acg_lastweek', '_acg', '_acg_lastweek', 'scolded_crying_acg', 'scolded_crying_acg_lastweek', 'cradled_crying_acg', 'cradled_crying_acg_lastweek', 'made_warm_acg', 'made_warm_acg_lastweek', 'gone_wokeup_acg', 'gone_wokeup_acg_lastweek', 'prepared_formula_milk_acg', 'prepared_formula_milk_acg_lastweek', 'talked_acg', 'talked_acg_lastweek', 'cuddled_acg', 'cuddled_acg_lastweek', 'sung_acg', 'sung_acg_lastweek', 'taken_nurse_acg', 'taken_nurse_acg_lastweek', 'talked_sternly_acg', 'talked_sternly_acg_lastweek', 'held_acg', 'held_acg_lastweek', 'shared_smiles_acg', 'shared_smiles_acg_lastweek', 'distracted_crying_acg', 'distracted_crying_acg_lastweek', 'showed_rightthing_acg', 'showed_rightthing_acg_lastweek', 'taken_visit_family_acg', 'taken_visit_family_acg_lastweek', 'cradled_arms_acg', 'cradled_arms_acg_lastweek', 'fed_crying_acg', 'fed_crying_acg_lastweek', 'prayed_acg', 'prayed_acg_lastweek', 'invovled_family_rituals_acg', 'invovled_family_rituals_acg_lastweek', 'songs_other', 'played_other', 'taken_out_other', 'introduced_newthings_other', 'told_not_todo_something_other', 'cleaned_other', 'rocked_other', 'carried_other', 'scolded_other', 'wasnt_too_hot_other', 'care_sick_other', 'taken_harm_other', 'fed_other', 'kissed_other', 'cooed_other', 'put_sleep_other', 'bathed_other', 'didnt_eat_harmful_other', 'soothed_other', 'massaged_other', 'showed_what_todo_other', 'thoogiddene_other', 'cuddled_crying_other', 'dressed_other', 'told_off_other', 'comfortable_other', 'slept_beside_other', 'introduced_new_people_other', '_other', 'scolded_crying_other', 'cradled_crying_other', 'made_warm_other', 'gone_wokeup_other', 'prepared_formula_milk_other', 'talked_other', 'cuddled_other', 'sung_other', 'taken_nurse_other', 'talked_sternly_other', 'held_other', 'shared_smiles_other', 'distracted_crying_other', 'showed_rightthing_other', 'taken_visit_family_other', 'cradled_arms_other', 'fed_crying_other', 'prayed_other', 'invovled_family_rituals_other','not_applicable'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'respondent_id' => 'Respondent ID',
            'assessment_id' => 'Assessment ID',
            'created_dtm' => 'Created Dtm',
            'last_updated_dtm' => 'Last Updated Dtm',
            'updated_by' => 'Updated By',
            'family_type' => 'Family Type',
            'number_adults' => 'Number Adults',
            'number_infants' => 'Number Infants',
            'number_children_preschool' => 'Number Children Preschool',
            'number_children' => 'Number Children',
            'number_adolescents' => 'Number Adolescents',
            'current_stay' => 'Current Stay',
            'care_carer' => 'Care Carer',
            'care_myself' => 'Care Myself',
            'care_husband' => 'Care Husband',
            'care_maternal_grandfather' => 'Care Maternal Grandfather',
            'care_maternal_grandmother' => 'Care Maternal Grandmother',
            'care_paternal_grandfather' => 'Care Paternal Grandfather',
            'care_paternal_grandmother' => 'Care Paternal Grandmother',
            'care_siblings' => 'Care Siblings',
            'played' => 'Played',
            'taken_out' => 'Taken Out',
            'introduced_newthings' => 'Introduced Newthings',
            'told_not_todo_something' => 'Told Not Todo Something',
            'cleaned' => 'Cleaned',
            'rocked' => 'Rocked',
            'carried' => 'Carried',
            'scolded' => 'Scolded',
            'wasnt_too_hot' => 'Wasnt Too Hot',
            'care_sick' => 'Care Sick',
            'taken_harm' => 'Taken Harm',
            'fed' => 'Fed',
            'kissed' => 'Kissed',
            'cooed' => 'Cooed',
            'put_sleep' => 'Put Sleep',
            'bathed' => 'Bathed',
            'didnt_eat_harmful' => 'Didnt Eat Harmful',
            'soothed' => 'Soothed',
            'massaged' => 'Massaged',
            'showed_what_todo' => 'Showed What Todo',
            'cuddled_crying' => 'Cuddled Crying',
            'dressed' => 'Dressed',
            'told_off' => 'Told Off',
            'comfortable' => 'Comfortable',
            'slept_beside' => 'Slept Beside',
            'introduced_new_people' => 'Introduced New People',
            'scolded_crying' => 'Scolded Crying',
            'cradled_crying' => 'Cradled Crying',
            'made_warm' => 'Made Warm',
            'gone_wokeup' => 'Gone Wokeup',
            'prepared_formula_milk' => 'Prepared Formula Milk',
            'talked' => 'Talked',
            'sung' => 'Sung',
            'cuddled' => 'Cuddled',
            'taken_nurse' => 'Taken Nurse',
            'talked_sternly' => 'Talked Sternly',
            'held' => 'Held',
            'shared_smiles' => 'Shared Smiles',
            'distracted_crying' => 'Distracted Crying',
            'showed_rightthing' => 'Showed Rightthing',
            'taken_visit_family' => 'Taken Visit Family',
            'cradled_arms' => 'Cradled Arms',
            'fed_crying' => 'Fed Crying',
            'prayed' => 'Prayed',
            'invovled_family_rituals' => 'Invovled Family Rituals',
            'score' => 'Score',
            'scale_id' => 'Scale ID',
            'songs' => 'Songs',
            'any_others' => 'Any Others',
            'thoogiddene' => 'Thoogiddene',
            'other_activity' => 'Other Activity',
            'current_stay_others' => 'Current Stay Others',
            'mother_id' => 'Mother ID',
            'songs_acg' => 'Songs Acg',
            'songs_acg_lastweek' => 'Songs Acg Lastweek',
            'played_acg' => 'Played Acg',
            'played_acg_lastweek' => 'Played Acg Lastweek',
            'taken_out_acg' => 'Taken Out Acg',
            'taken_out_acg_lastweek' => 'Taken Out Acg Lastweek',
            'introduced_newthings_acg' => 'Introduced Newthings Acg',
            'introduced_newthings_acg_lastweek' => 'Introduced Newthings Acg Lastweek',
            'told_not_todo_something_acg' => 'Told Not Todo Something Acg',
            'told_not_todo_something_acg_lastweek' => 'Told Not Todo Something Acg Lastweek',
            'cleaned_acg' => 'Cleaned Acg',
            'cleaned_acg_lastweek' => 'Cleaned Acg Lastweek',
            'rocked_acg' => 'Rocked Acg',
            'rocked_acg_lastweek' => 'Rocked Acg Lastweek',
            'carried_acg' => 'Carried Acg',
            'carried_acg_lastweek' => 'Carried Acg Lastweek',
            'scolded_acg' => 'Scolded Acg',
            'scolded_acg_lastweek' => 'Scolded Acg Lastweek',
            'wasnt_too_hot_acg' => 'Wasnt Too Hot Acg',
            'wasnt_too_hot_acg_lastweek' => 'Wasnt Too Hot Acg Lastweek',
            'care_sick_acg' => 'Care Sick Acg',
            'care_sick_acg_lastweek' => 'Care Sick Acg Lastweek',
            'taken_harm_acg' => 'Taken Harm Acg',
            'taken_harm_acg_lastweek' => 'Taken Harm Acg Lastweek',
            'fed_acg' => 'Fed Acg',
            'fed_acg_lastweek' => 'Fed Acg Lastweek',
            'kissed_acg' => 'Kissed Acg',
            'kissed_acg_lastweek' => 'Kissed Acg Lastweek',
            'cooed_acg' => 'Cooed Acg',
            'cooed_acg_lastweek' => 'Cooed Acg Lastweek',
            'put_sleep_acg' => 'Put Sleep Acg',
            'put_sleep_acg_lastweek' => 'Put Sleep Acg Lastweek',
            'bathed_acg' => 'Bathed Acg',
            'bathed_acg_lastweek' => 'Bathed Acg Lastweek',
            'didnt_eat_harmful_acg' => 'Didnt Eat Harmful Acg',
            'didnt_eat_harmful_acg_lastweek' => 'Didnt Eat Harmful Acg Lastweek',
            'soothed_acg' => 'Soothed Acg',
            'soothed_acg_lastweek' => 'Soothed Acg Lastweek',
            'massaged_acg' => 'Massaged Acg',
            'massaged_acg_lastweek' => 'Massaged Acg Lastweek',
            'showed_what_todo_acg' => 'Showed What Todo Acg',
            'showed_what_todo_acg_lastweek' => 'Showed What Todo Acg Lastweek',
            'thoogiddene_acg' => 'Thoogiddene Acg',
            'thoogiddene_acg_lastweek' => 'Thoogiddene Acg Lastweek',
            'cuddled_crying_acg' => 'Cuddled Crying Acg',
            'cuddled_crying_acg_lastweek' => 'Cuddled Crying Acg Lastweek',
            'dressed_acg' => 'Dressed Acg',
            'dressed_acg_lastweek' => 'Dressed Acg Lastweek',
            'told_off_acg' => 'Told Off Acg',
            'told_off_acg_lastweek' => 'Told Off Acg Lastweek',
            'comfortable_acg' => 'Comfortable Acg',
            'comfortable_acg_lastweek' => 'Comfortable Acg Lastweek',
            'slept_beside_acg' => 'Slept Beside Acg',
            'slept_beside_acg_lastweek' => 'Slept Beside Acg Lastweek',
            'introduced_new_people_acg' => 'Introduced New People Acg',
            'introduced_new_people_acg_lastweek' => 'Introduced New People Acg Lastweek',
            '_acg' => 'Acg',
            '_acg_lastweek' => 'Acg Lastweek',
            'scolded_crying_acg' => 'Scolded Crying Acg',
            'scolded_crying_acg_lastweek' => 'Scolded Crying Acg Lastweek',
            'cradled_crying_acg' => 'Cradled Crying Acg',
            'cradled_crying_acg_lastweek' => 'Cradled Crying Acg Lastweek',
            'made_warm_acg' => 'Made Warm Acg',
            'made_warm_acg_lastweek' => 'Made Warm Acg Lastweek',
            'gone_wokeup_acg' => 'Gone Wokeup Acg',
            'gone_wokeup_acg_lastweek' => 'Gone Wokeup Acg Lastweek',
            'prepared_formula_milk_acg' => 'Prepared Formula Milk Acg',
            'prepared_formula_milk_acg_lastweek' => 'Prepared Formula Milk Acg Lastweek',
            'talked_acg' => 'Talked Acg',
            'talked_acg_lastweek' => 'Talked Acg Lastweek',
            'cuddled_acg' => 'Cuddled Acg',
            'cuddled_acg_lastweek' => 'Cuddled Acg Lastweek',
            'sung_acg' => 'Sung Acg',
            'sung_acg_lastweek' => 'Sung Acg Lastweek',
            'taken_nurse_acg' => 'Taken Nurse Acg',
            'taken_nurse_acg_lastweek' => 'Taken Nurse Acg Lastweek',
            'talked_sternly_acg' => 'Talked Sternly Acg',
            'talked_sternly_acg_lastweek' => 'Talked Sternly Acg Lastweek',
            'held_acg' => 'Held Acg',
            'held_acg_lastweek' => 'Held Acg Lastweek',
            'shared_smiles_acg' => 'Shared Smiles Acg',
            'shared_smiles_acg_lastweek' => 'Shared Smiles Acg Lastweek',
            'distracted_crying_acg' => 'Distracted Crying Acg',
            'distracted_crying_acg_lastweek' => 'Distracted Crying Acg Lastweek',
            'showed_rightthing_acg' => 'Showed Rightthing Acg',
            'showed_rightthing_acg_lastweek' => 'Showed Rightthing Acg Lastweek',
            'taken_visit_family_acg' => 'Taken Visit Family Acg',
            'taken_visit_family_acg_lastweek' => 'Taken Visit Family Acg Lastweek',
            'cradled_arms_acg' => 'Cradled Arms Acg',
            'cradled_arms_acg_lastweek' => 'Cradled Arms Acg Lastweek',
            'fed_crying_acg' => 'Fed Crying Acg',
            'fed_crying_acg_lastweek' => 'Fed Crying Acg Lastweek',
            'prayed_acg' => 'Prayed Acg',
            'prayed_acg_lastweek' => 'Prayed Acg Lastweek',
            'invovled_family_rituals_acg' => 'Invovled Family Rituals Acg',
            'invovled_family_rituals_acg_lastweek' => 'Invovled Family Rituals Acg Lastweek',
            'songs_other' => 'Songs Other',
            'played_other' => 'Played Other',
            'taken_out_other' => 'Taken Out Other',
            'introduced_newthings_other' => 'Introduced Newthings Other',
            'told_not_todo_something_other' => 'Told Not Todo Something Other',
            'cleaned_other' => 'Cleaned Other',
            'rocked_other' => 'Rocked Other',
            'carried_other' => 'Carried Other',
            'scolded_other' => 'Scolded Other',
            'wasnt_too_hot_other' => 'Wasnt Too Hot Other',
            'care_sick_other' => 'Care Sick Other',
            'taken_harm_other' => 'Taken Harm Other',
            'fed_other' => 'Fed Other',
            'kissed_other' => 'Kissed Other',
            'cooed_other' => 'Cooed Other',
            'put_sleep_other' => 'Put Sleep Other',
            'bathed_other' => 'Bathed Other',
            'didnt_eat_harmful_other' => 'Didnt Eat Harmful Other',
            'soothed_other' => 'Soothed Other',
            'massaged_other' => 'Massaged Other',
            'showed_what_todo_other' => 'Showed What Todo Other',
            'thoogiddene_other' => 'Thoogiddene Other',
            'cuddled_crying_other' => 'Cuddled Crying Other',
            'dressed_other' => 'Dressed Other',
            'told_off_other' => 'Told Off Other',
            'comfortable_other' => 'Comfortable Other',
            'slept_beside_other' => 'Slept Beside Other',
            'introduced_new_people_other' => 'Introduced New People Other',
            '_other' => 'Other',
            'scolded_crying_other' => 'Scolded Crying Other',
            'cradled_crying_other' => 'Cradled Crying Other',
            'made_warm_other' => 'Made Warm Other',
            'gone_wokeup_other' => 'Gone Wokeup Other',
            'prepared_formula_milk_other' => 'Prepared Formula Milk Other',
            'talked_other' => 'Talked Other',
            'cuddled_other' => 'Cuddled Other',
            'sung_other' => 'Sung Other',
            'taken_nurse_other' => 'Taken Nurse Other',
            'talked_sternly_other' => 'Talked Sternly Other',
            'held_other' => 'Held Other',
            'shared_smiles_other' => 'Shared Smiles Other',
            'distracted_crying_other' => 'Distracted Crying Other',
            'showed_rightthing_other' => 'Showed Rightthing Other',
            'taken_visit_family_other' => 'Taken Visit Family Other',
            'cradled_arms_other' => 'Cradled Arms Other',
            'fed_crying_other' => 'Fed Crying Other',
            'prayed_other' => 'Prayed Other',
            'invovled_family_rituals_other' => 'Invovled Family Rituals Other',
        ];
    }

      public function beforeSave($insert)
    {
        return true;
    }



public function afterFind() {
    return true; //don't forget this
}
}
