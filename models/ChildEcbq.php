<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "child_ecbq".
 *
 * @property integer $id
 * @property integer $mother_id
 * @property integer $assesment_id
 * @property string $created_dtm
 * @property string $last_updated_dtm
 * @property string $updated_by
 * @property integer $publicplace_unfamiliarperson
 * @property integer $work_incomplete
 * @property integer $familiar_baby
 * @property integer $other_activities
 * @property integer $happy_sing
 * @property integer $play_outside
 * @property integer $toys1
 * @property integer $toys2
 * @property integer $like_elders
 * @property integer $simple_activity
 * @property integer $play_inside
 * @property integer $hug
 * @property integer $new_activity
 * @property integer $concentrate
 * @property integer $public_place
 * @property integer $play_out
 * @property integer $stop_work
 * @property integer $sad_cry
 * @property integer $sad_look
 * @property integer $walk_in_home
 * @property integer $new_toys
 * @property integer $sad_on_no
 * @property integer $patience
 * @property integer $slow_smile
 * @property integer $lap_sleep
 * @property integer $talks_familiar
 * @property integer $breakable_things
 * @property integer $dint_go_inside
 * @property integer $cry_morethan_3min
 * @property integer $fast_samadhana
 * @property integer $looks_other_activity
 * @property integer $plays_withdiff_people
 */
class ChildEcbq extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'child_ecbq';
    }
	
	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mother_id', 'assessment_id', 'publicplace_unfamiliarperson', 'work_incomplete', 'familiar_baby', 'other_activities', 
			'happy_sing', 'play_outside', 'toys1', 'toys2', 'like_elders', 'simple_activity', 'play_inside', 'hug', 'new_activity', 
			'concentrate', 'public_place', 'play_out', 'stop_work', 'sad_cry', 'sad_look', 'walk_in_home', 'new_toys', 'sad_on_no', 
			'patience', 'slow_smile', 'lap_sleep', 'talks_familiar', 'breakable_things', 'dint_go_inside', 'cry_morethan_3min', 
			'fast_samadhana', 'looks_other_activity', 'plays_withdiff_people','score','scale_id','attention','kirikiri_clothes','kirikiri_sounds','active_sunset'], 'integer'],
            [['created_dtm', 'last_updated_dtm'], 'safe'],
            [['updated_by','not_applicable'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mother_id' => 'Respondent ID',
            'assessment_id' => 'Assesment ID',
            'created_dtm' => 'Created Date',
            'last_updated_dtm' => 'Last Updated Date',
            'updated_by' => 'Updated By',
            'publicplace_unfamiliarperson' => 'Publicplace Unfamiliarperson',
            'work_incomplete' => 'Work Incomplete',
            'familiar_baby' => 'Familiar Baby',
            'other_activities' => 'Other Activities',
            'happy_sing' => 'Happy Sing',
            'play_outside' => 'Play Outside',
            'toys1' => 'Toys1',
            'toys2' => 'Toys2',
            'like_elders' => 'Like Elders',
            'simple_activity' => 'Simple Activity',
            'play_inside' => 'Play Inside',
            'hug' => 'Hug',
            'new_activity' => 'New Activity',
            'concentrate' => 'Concentrate',
            'public_place' => 'Public Place',
            'play_out' => 'Play Out',
            'stop_work' => 'Stop Work',
            'sad_cry' => 'Sad Cry',
            'sad_look' => 'Sad Look',
            'walk_in_home' => 'Walk In Home',
            'new_toys' => 'New Toys',
            'sad_on_no' => 'Sad On No',
            'patience' => 'Patience',
            'slow_smile' => 'Slow Smile',
            'lap_sleep' => 'Lap Sleep',
            'talks_familiar' => 'Talks Familiar',
            'breakable_things' => 'Breakable Things',
            'dint_go_inside' => 'Dint Go Inside',
            'cry_morethan_3min' => 'Cry Morethan 3min',
            'fast_samadhana' => 'Fast Samadhana',
            'looks_other_activity' => 'Looks Other Activity',
            'plays_withdiff_people' => 'Plays Withdiff People',
			'score' => 'Score',
			'scale_id'=> 'Scale Id',
        ];
    }


      public function beforeSave($insert)
    {
        return true;
    }



public function afterFind() {
    return true; //don't forget this
}
}
