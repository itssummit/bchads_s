<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "scores".
 *
 * @property integer $id
 * @property integer $respondent_id
 * @property integer $assessment_id
 * @property integer $scale_id
 * @property integer $score
 * @property string $last_updated_dtm
 * @property string $updated_by
 */
class Scores extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scores';
    }
	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['respondent_id', 'assessment_id', 'scale_id', 'score'], 'integer'],
            [['last_updated_dtm','created_dtm'], 'safe'],
            [['updated_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'respondent_id' => 'Respondent ID',
            'assessment_id' => 'assessment ID',
            'scale_id' => 'Scale ID',
            'score' => 'Score',
            'last_updated_dtm' => 'Last Updated Date',
			'created_dtm' => 'Created 	Date',
            'updated_by' => 'Updated By',
        ];
    }
     public function updateScales($respondentId,$assessmentId,$scaleId,$score)
    {
        $model= Scores::findOne(['respondent_id'=>$respondentId,'assessment_id'=>$assessmentId,'scale_id'=>$scaleId]);
       
        if(isset($model['id'])){
             $model->score=$score;
             $model->last_updated_dtm = date('Y-m-d h:i:s');
			 $model->created_dtm = date('Y-m-d h:i:s');
             $model->updated_by = \app\models\Users::findOne(['id'=>Yii::$app->user->id])->username;
             $model->save();
        }
        else{
        $model = new \app\models\Scores;
        $model->respondent_id= $respondentId;
        $model->assessment_id=$assessmentId;
    	$model->scale_id=$scaleId;
        $model->score=$score;
        $model->last_updated_dtm = date('Y-m-d h:i:s');
		$model->created_dtm = date('Y-m-d h:i:s');
        $model->updated_by = \app\models\Users::findOne(['id'=>Yii::$app->user->id])->username; 
    	$model->save();
        }
    }
}
