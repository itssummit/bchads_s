<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "icmr".
 *
 * @property integer $id
 * @property integer $mother_id
 * @property integer $assessment_id
 * @property string $created_dtm
 * @property string $last_updated_dtm
 * @property string $updated_by
 * @property string $bad_words
 * @property string $threatening
 * @property string $threatening_sending_home
 * @property string $send_home
 * @property string $finance_prob
 * @property string $fear_look
 * @property string $avidheya
 * @property string $udasinathe
 * @property string $social_rights
 * @property string $nirlakshya
 * @property string $personal_needs
 * @property string $decision_taking
 * @property string $restrict
 * @property string $harm
 * @property string $harm_fire
 * @property string $forced
 * @property string $avoid
 * @property string $wound
 * @property integer $score
 * @property integer $scale_id
 * @property string $bad_words1
 * @property string $bad_words2
 * @property string $bad_words3
 * @property string $bad_words4
 * @property string $bad_words5
 * @property string $bad_words6
 * @property string $bad_words7
 * @property string $bad_words8
 * @property string $threatening1
 * @property string $threatening2
 * @property string $threatening3
 * @property string $threatening4
 * @property string $threatening5
 * @property string $threatening6
 * @property string $threatening7
 * @property string $threatening8
 * @property string $threatening_sending_home1
 * @property string $threatening_sending_home2
 * @property string $threatening_sending_home3
 * @property string $threatening_sending_home4
 * @property string $threatening_sending_home5
 * @property string $threatening_sending_home6
 * @property string $threatening_sending_home7
 * @property string $threatening_sending_home8
 * @property string $send_home1
 * @property string $send_home2
 * @property string $send_home3
 * @property string $send_home4
 * @property string $send_home5
 * @property string $send_home6
 * @property string $send_home7
 * @property string $send_home8
 * @property string $finance_prob1
 * @property string $finance_prob2
 * @property string $finance_prob3
 * @property string $finance_prob4
 * @property string $finance_prob5
 * @property string $finance_prob6
 * @property string $finance_prob7
 * @property string $finance_prob8
 * @property string $fear_look1
 * @property string $fear_look2
 * @property string $fear_look3
 * @property string $fear_look4
 * @property string $fear_look5
 * @property string $fear_look6
 * @property string $fear_look7
 * @property string $fear_look8
 * @property string $avidheya1
 * @property string $avidheya2
 * @property string $avidheya3
 * @property string $avidheya4
 * @property string $avidheya5
 * @property string $avidheya6
 * @property string $avidheya7
 * @property string $avidheya8
 * @property string $udasinathe1
 * @property string $udasinathe2
 * @property string $udasinathe3
 * @property string $udasinathe4
 * @property string $udasinathe5
 * @property string $udasinathe6
 * @property string $udasinathe7
 * @property string $udasinathe8
 * @property string $social_rights1
 * @property string $social_rights2
 * @property string $social_rights3
 * @property string $social_rights4
 * @property string $social_rights5
 * @property string $social_rights6
 * @property string $social_rights7
 * @property string $social_rights8
 * @property string $nirlakshya1
 * @property string $nirlakshya2
 * @property string $nirlakshya3
 * @property string $nirlakshya4
 * @property string $nirlakshya5
 * @property string $nirlakshya6
 * @property string $nirlakshya7
 * @property string $nirlakshya8
 * @property string $personal_needs1
 * @property string $personal_needs2
 * @property string $personal_needs3
 * @property string $personal_needs4
 * @property string $personal_needs5
 * @property string $personal_needs6
 * @property string $personal_needs7
 * @property string $personal_needs8
 * @property string $decision_taking1
 * @property string $decision_taking2
 * @property string $decision_taking3
 * @property string $decision_taking4
 * @property string $decision_taking5
 * @property string $decision_taking6
 * @property string $decision_taking7
 * @property string $decision_taking8
 * @property string $restrict1
 * @property string $restrict2
 * @property string $restrict3
 * @property string $restrict4
 * @property string $restrict5
 * @property string $restrict6
 * @property string $restrict7
 * @property string $restrict8
 * @property string $harm1
 * @property string $harm2
 * @property string $harm3
 * @property string $harm4
 * @property string $harm5
 * @property string $harm6
 * @property string $harm7
 * @property string $harm8
 * @property string $harm9
 * @property string $harm_fire1
 * @property string $harm_fire2
 * @property string $harm_fire3
 * @property string $harm_fire4
 * @property string $harm_fire5
 * @property string $harm_fire6
 * @property string $harm_fire7
 * @property string $harm_fire8
 * @property string $harm_fire9
 * @property string $forced1
 * @property string $forced2
 * @property string $forced3
 * @property string $forced4
 * @property string $forced5
 * @property string $forced6
 * @property string $forced7
 * @property string $forced8
 * @property string $avoid1
 * @property string $avoid2
 * @property string $avoid3
 * @property string $avoid4
 * @property string $avoid5
 * @property string $avoid6
 * @property string $avoid7
 * @property string $avoid8
 * @property string $wound1
 * @property string $wound2
 * @property string $wound3
 * @property string $wound4
 * @property string $wound5
 * @property string $wound6
 * @property string $wound7
 * @property string $wound8
 */
class Icmr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'icmr';
    }
	
	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mother_id', 'assessment_id', 'score', 'scale_id'], 'integer'],
            [['created_dtm', 'last_updated_dtm'], 'safe'],
            [['updated_by', 'bad_words', 'threatening', 'threatening_sending_home', 'send_home', 'finance_prob', 'fear_look', 'avidheya', 'udasinathe', 'social_rights', 'nirlakshya', 'personal_needs', 'decision_taking', 'restrict', 'harm', 'harm_fire', 'forced', 'avoid', 'wound', 'bad_words1', 'bad_words2', 'bad_words3', 'bad_words4', 'bad_words5', 'bad_words6', 'bad_words7', 'bad_words8', 'bad_words9','bad_words10','bad_words11','threatening1', 'threatening2', 'threatening3', 'threatening4', 'threatening5', 'threatening6', 'threatening7', 'threatening8', 'threatening9','threatening10','threatening11','threatening_sending_home1', 'threatening_sending_home2', 'threatening_sending_home3', 'threatening_sending_home4', 'threatening_sending_home5', 'threatening_sending_home6', 'threatening_sending_home7', 'threatening_sending_home8','threatening_sending_home9','threatening_sending_home10','threatening_sending_home11', 'send_home1', 'send_home2', 'send_home3', 'send_home4', 'send_home5', 'send_home6', 'send_home7', 'send_home8', 'send_home9','send_home10','send_home11','finance_prob1', 'finance_prob2', 'finance_prob3', 'finance_prob4', 'finance_prob5', 'finance_prob6', 'finance_prob7', 'finance_prob8', 'fear_look1', 'fear_look2', 'fear_look3', 'fear_look4', 'fear_look5', 'fear_look6', 'fear_look7', 'fear_look8','fear_look9','fear_look10','fear_look11','finance_prob9','finance_prob10','finance_prob11', 'avidheya1', 'avidheya2', 'avidheya3', 'avidheya4', 'avidheya5', 'avidheya6', 'avidheya7', 'avidheya8','avidheya9','avidheya10','avidheya11', 'udasinathe1', 'udasinathe2', 'udasinathe3', 'udasinathe4', 'udasinathe5', 'udasinathe6', 'udasinathe7', 'udasinathe8', 'udasinathe9','udasinathe10','udasinathe11','social_rights1', 'social_rights2', 'social_rights3', 'social_rights4', 'social_rights5', 'social_rights6', 'social_rights7', 'social_rights8','social_rights9','social_rights10','social_rights11', 'nirlakshya1', 'nirlakshya2', 'nirlakshya3', 'nirlakshya4', 'nirlakshya5', 'nirlakshya6', 'nirlakshya7', 'nirlakshya8','nirlakshya9','nirlakshya10','nirlakshya11', 'personal_needs1', 'personal_needs2', 'personal_needs3', 'personal_needs4', 'personal_needs5', 'personal_needs6', 'personal_needs7', 'personal_needs8', 'personal_needs9','personal_needs10','personal_needs11','decision_taking1', 'decision_taking2', 'decision_taking3', 'decision_taking4', 'decision_taking5', 'decision_taking6', 'decision_taking7', 'decision_taking8','decision_taking9','decision_taking10','decision_taking10', 'restrict1', 'restrict2', 'restrict3', 'restrict4', 'restrict5', 'restrict6', 'restrict7', 'restrict8','restrict9','restrict10','restrict11', 'harm1', 'harm2', 'harm3', 'harm4', 'harm5', 'harm6', 'harm7', 'harm8', 'harm9','harm10','harm11','harm12', 'harm_fire1', 'harm_fire2', 'harm_fire3', 'harm_fire4', 'harm_fire5', 'harm_fire6', 'harm_fire7', 'harm_fire8', 'harm_fire9','harm_fire10','harm_fire11','harm_fire12', 'forced1', 'forced2', 'forced3', 'forced4', 'forced5', 'forced6', 'forced7', 'forced8','forced9','forced10','forced11', 'avoid1', 'avoid2', 'avoid3', 'avoid4', 'avoid5', 'avoid6', 'avoid7', 'avoid8','avoid9','avoid10','avoid11', 'wound1', 'wound2', 'wound3', 'wound4', 'wound5', 'wound6', 'wound7', 'wound8','wound9','wound10','wound11'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mother_id' => 'Respondent ID',
            'assessment_id' => 'Assessment ID',
            'created_dtm' => 'Created Dtm',
            'last_updated_dtm' => 'Last Updated Dtm',
            'updated_by' => 'Updated By',
            'bad_words' => 'Bad Words',
            'threatening' => 'Threatening',
            'threatening_sending_home' => 'Threatening Sending Home',
            'send_home' => 'Send Home',
            'finance_prob' => 'Finance Prob',
            'fear_look' => 'Fear Look',
            'avidheya' => 'Avidheya',
            'udasinathe' => 'Udasinathe',
            'social_rights' => 'Social Rights',
            'nirlakshya' => 'Nirlakshya',
            'personal_needs' => 'Personal Needs',
            'decision_taking' => 'Decision Taking',
            'restrict' => 'Restrict',
            'harm' => 'Harm',
            'harm_fire' => 'Harm Fire',
            'forced' => 'Forced',
            'avoid' => 'Avoid',
            'wound' => 'Wound',
            'score' => 'Score',
            'scale_id' => 'Scale ID',
            'bad_words1' => 'Bad Words1',
            'bad_words2' => 'Bad Words2',
            'bad_words3' => 'Bad Words3',
            'bad_words4' => 'Bad Words4',
            'bad_words5' => 'Bad Words5',
            'bad_words6' => 'Bad Words6',
            'bad_words7' => 'Bad Words7',
            'bad_words8' => 'Bad Words8',
            'threatening1' => 'Threatening1',
            'threatening2' => 'Threatening2',
            'threatening3' => 'Threatening3',
            'threatening4' => 'Threatening4',
            'threatening5' => 'Threatening5',
            'threatening6' => 'Threatening6',
            'threatening7' => 'Threatening7',
            'threatening8' => 'Threatening8',
            'threatening_sending_home1' => 'Threatening Sending Home1',
            'threatening_sending_home2' => 'Threatening Sending Home2',
            'threatening_sending_home3' => 'Threatening Sending Home3',
            'threatening_sending_home4' => 'Threatening Sending Home4',
            'threatening_sending_home5' => 'Threatening Sending Home5',
            'threatening_sending_home6' => 'Threatening Sending Home6',
            'threatening_sending_home7' => 'Threatening Sending Home7',
            'threatening_sending_home8' => 'Threatening Sending Home8',
            'send_home1' => 'Send Home1',
            'send_home2' => 'Send Home2',
            'send_home3' => 'Send Home3',
            'send_home4' => 'Send Home4',
            'send_home5' => 'Send Home5',
            'send_home6' => 'Send Home6',
            'send_home7' => 'Send Home7',
            'send_home8' => 'Send Home8',
            'finance_prob1' => 'Finance Prob1',
            'finance_prob2' => 'Finance Prob2',
            'finance_prob3' => 'Finance Prob3',
            'finance_prob4' => 'Finance Prob4',
            'finance_prob5' => 'Finance Prob5',
            'finance_prob6' => 'Finance Prob6',
            'finance_prob7' => 'Finance Prob7',
            'finance_prob8' => 'Finance Prob8',
            'fear_look1' => 'Fear Look1',
            'fear_look2' => 'Fear Look2',
            'fear_look3' => 'Fear Look3',
            'fear_look4' => 'Fear Look4',
            'fear_look5' => 'Fear Look5',
            'fear_look6' => 'Fear Look6',
            'fear_look7' => 'Fear Look7',
            'fear_look8' => 'Fear Look8',
            'avidheya1' => 'Avidheya1',
            'avidheya2' => 'Avidheya2',
            'avidheya3' => 'Avidheya3',
            'avidheya4' => 'Avidheya4',
            'avidheya5' => 'Avidheya5',
            'avidheya6' => 'Avidheya6',
            'avidheya7' => 'Avidheya7',
            'avidheya8' => 'Avidheya8',
            'udasinathe1' => 'Udasinathe1',
            'udasinathe2' => 'Udasinathe2',
            'udasinathe3' => 'Udasinathe3',
            'udasinathe4' => 'Udasinathe4',
            'udasinathe5' => 'Udasinathe5',
            'udasinathe6' => 'Udasinathe6',
            'udasinathe7' => 'Udasinathe7',
            'udasinathe8' => 'Udasinathe8',
            'social_rights1' => 'Social Rights1',
            'social_rights2' => 'Social Rights2',
            'social_rights3' => 'Social Rights3',
            'social_rights4' => 'Social Rights4',
            'social_rights5' => 'Social Rights5',
            'social_rights6' => 'Social Rights6',
            'social_rights7' => 'Social Rights7',
            'social_rights8' => 'Social Rights8',
            'nirlakshya1' => 'Nirlakshya1',
            'nirlakshya2' => 'Nirlakshya2',
            'nirlakshya3' => 'Nirlakshya3',
            'nirlakshya4' => 'Nirlakshya4',
            'nirlakshya5' => 'Nirlakshya5',
            'nirlakshya6' => 'Nirlakshya6',
            'nirlakshya7' => 'Nirlakshya7',
            'nirlakshya8' => 'Nirlakshya8',
            'personal_needs1' => 'Personal Needs1',
            'personal_needs2' => 'Personal Needs2',
            'personal_needs3' => 'Personal Needs3',
            'personal_needs4' => 'Personal Needs4',
            'personal_needs5' => 'Personal Needs5',
            'personal_needs6' => 'Personal Needs6',
            'personal_needs7' => 'Personal Needs7',
            'personal_needs8' => 'Personal Needs8',
            'decision_taking1' => 'Decision Taking1',
            'decision_taking2' => 'Decision Taking2',
            'decision_taking3' => 'Decision Taking3',
            'decision_taking4' => 'Decision Taking4',
            'decision_taking5' => 'Decision Taking5',
            'decision_taking6' => 'Decision Taking6',
            'decision_taking7' => 'Decision Taking7',
            'decision_taking8' => 'Decision Taking8',
            'restrict1' => 'Restrict1',
            'restrict2' => 'Restrict2',
            'restrict3' => 'Restrict3',
            'restrict4' => 'Restrict4',
            'restrict5' => 'Restrict5',
            'restrict6' => 'Restrict6',
            'restrict7' => 'Restrict7',
            'restrict8' => 'Restrict8',
            'harm1' => 'Harm1',
            'harm2' => 'Harm2',
            'harm3' => 'Harm3',
            'harm4' => 'Harm4',
            'harm5' => 'Harm5',
            'harm6' => 'Harm6',
            'harm7' => 'Harm7',
            'harm8' => 'Harm8',
            'harm9' => 'Harm9',
            'harm_fire1' => 'Harm Fire1',
            'harm_fire2' => 'Harm Fire2',
            'harm_fire3' => 'Harm Fire3',
            'harm_fire4' => 'Harm Fire4',
            'harm_fire5' => 'Harm Fire5',
            'harm_fire6' => 'Harm Fire6',
            'harm_fire7' => 'Harm Fire7',
            'harm_fire8' => 'Harm Fire8',
            'harm_fire9' => 'Harm Fire9',
            'forced1' => 'Forced1',
            'forced2' => 'Forced2',
            'forced3' => 'Forced3',
            'forced4' => 'Forced4',
            'forced5' => 'Forced5',
            'forced6' => 'Forced6',
            'forced7' => 'Forced7',
            'forced8' => 'Forced8',
            'avoid1' => 'Avoid1',
            'avoid2' => 'Avoid2',
            'avoid3' => 'Avoid3',
            'avoid4' => 'Avoid4',
            'avoid5' => 'Avoid5',
            'avoid6' => 'Avoid6',
            'avoid7' => 'Avoid7',
            'avoid8' => 'Avoid8',
            'wound1' => 'Wound1',
            'wound2' => 'Wound2',
            'wound3' => 'Wound3',
            'wound4' => 'Wound4',
            'wound5' => 'Wound5',
            'wound6' => 'Wound6',
            'wound7' => 'Wound7',
            'wound8' => 'Wound8',
        ];
    }

      public function beforeSave($insert)
    {
        return true;
    }



public function afterFind() {
    return true; //don't forget this
}
}
