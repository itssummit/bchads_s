<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bitsea".
 *
 * @property integer $id
 * @property integer $mother_id
 * @property integer $assessment_id
 * @property string $last_updated_dtm
 * @property string $created_dtm
 * @property string $updated_by
 * @property integer $happy_won
 * @property integer $hurt_himself
 * @property integer $stress_fear
 * @property integer $cant_sit_quitely
 * @property integer $follows_rules
 * @property integer $wakeup_midnight
 * @property integer $cry
 * @property integer $animal_fear
 * @property integer $less_enjoy
 * @property integer $ifsad_searches_parents
 * @property integer $cry_left
 * @property integer $thinks
 * @property integer $looked_on_call
 * @property integer $didnt_respond_pain
 * @property integer $happy_with_loved
 * @property integer $didnt_touch_things
 * @property integer $problem_in_sleep
 * @property integer $runs
 * @property integer $play_with_others
 * @property integer $concentrates
 * @property integer $problem_in_udjustment
 * @property integer $helps_others
 * @property integer $not_happy
 * @property integer $eating_problem
 * @property integer $imitates_sound
 * @property integer $rejects_food
 * @property integer $fights
 * @property integer $breaks_things
 * @property integer $shows_fingers
 * @property integer $beats_bites
 * @property integer $plays_with_toys
 * @property integer $sad
 * @property integer $hurts_parents
 * @property integer $didnt_move_angry
 * @property integer $keeps_things
 * @property integer $repeats_sounds
 * @property integer $repeats_work
 * @property integer $separate_from_others
 * @property integer $hurts_himself
 * @property integer $eats_nonhealthy
 * @property integer $feelings_about_baby
 * @property integer $feelings_about_language
 */
class Bitsea extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bitsea';
    }
	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mother_id', 'assessment_id','score','scale_id', 'happy_won', 'hurt_himself', 'stress_fear', 'cant_sit_quitely', 'follows_rules', 'wakeup_midnight', 'cry', 'animal_fear', 'less_enjoy', 'ifsad_searches_parents', 'cry_left', 'thinks', 'looked_on_call', 'didnt_respond_pain', 'happy_with_loved', 'didnt_touch_things', 'problem_in_sleep', 'runs', 'play_with_others', 'concentrates', 'problem_in_udjustment', 'helps_others', 'not_happy', 'eating_problem', 'imitates_sound', 'rejects_food', 'fights', 'breaks_things', 'shows_fingers', 'beats_bites', 'plays_with_toys', 'sad', 'hurts_parents', 'didnt_move_angry', 'keeps_things', 'repeats_sounds', 'repeats_work', 'separate_from_others', 'hurts_himself', 'eats_nonhealthy', 'feelings_about_baby', 'feelings_about_language','physical_contact','see_straight'], 'integer'],
            [['last_updated_dtm', 'created_dtm'], 'safe'],
            [['updated_by','animal_fear_det','repeats_sounds_det','repeats_work_det','hurts_himself_det','eats_nonhealthy_det'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mother_id' => 'Patient ID',
            'assessment_id' => 'assessment ID',
            'last_updated_dtm' => 'Last Updated Date',
            'created_dtm' => 'Created Date',
            'updated_by' => 'Updated By',
            'happy_won' => 'Happy Won',
            'hurt_himself' => 'Hurt Himself',
            'stress_fear' => 'Stress Fear',
            'cant_sit_quitely' => 'Cant Sit Quitely',
            'follows_rules' => 'Follows Rules',
            'wakeup_midnight' => 'Wakeup Midnight',
            'cry' => 'Cry',
            'animal_fear' => 'Animal Fear',
            'less_enjoy' => 'Less Enjoy',
            'ifsad_searches_parents' => 'Ifsad Searches Parents',
            'cry_left' => 'Cry Left',
            'thinks' => 'Thinks',
            'looked_on_call' => 'Looked On Call',
            'didnt_respond_pain' => 'Didnt Respond Pain',
            'happy_with_loved' => 'Happy With Loved',
            'didnt_touch_things' => 'Didnt Touch Things',
            'problem_in_sleep' => 'Problem In Sleep',
            'runs' => 'Runs',
            'play_with_others' => 'Play With Others',
            'concentrates' => 'Concentrates',
            'problem_in_udjustment' => 'Problem In Udjustment',
            'helps_others' => 'Helps Others',
            'not_happy' => 'Not Happy',
            'eating_problem' => 'Eating Problem',
            'imitates_sound' => 'Imitates Sound',
            'rejects_food' => 'Rejects Food',
            'fights' => 'Fights',
            'breaks_things' => 'Breaks Things',
            'shows_fingers' => 'Shows Fingers',
            'beats_bites' => 'Beats Bites',
            'plays_with_toys' => 'Plays With Toys',
            'sad' => 'Sad',
            'hurts_parents' => 'Hurts Parents',
            'didnt_move_angry' => 'Didnt Move Angry',
            'keeps_things' => 'Keeps Things',
            'repeats_sounds' => 'Repeats Sounds',
            'repeats_work' => 'Repeats Work',
            'separate_from_others' => 'Separate From Others',
            'hurts_himself' => 'Hurts Himself',
            'eats_nonhealthy' => 'Eats Nonhealthy',
            'feelings_about_baby' => 'Feelings About Baby',
            'feelings_about_language' => 'Feelings About Language',
			'score' => 'Score',
			'animal_fear_det' => 'Animal'
        ];
    }

      public function beforeSave($insert)
    {
        return true;
    }



public function afterFind() {
    return true; //don't forget this
}
}
