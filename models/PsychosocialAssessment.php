<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "psychosocial_assessment".
 *
 * @property integer $id
 * @property integer $mother_id
 * @property integer $assessment_id
 * @property string $created_dtm
 * @property string $last_updated_dtm
 * @property string $updated_by
 * @property integer $financial_worries
 * @property integer $money_worries
 * @property integer $family_problems
 * @property integer $move_recently_future
 * @property integer $loss_loved
 * @property integer $current_pregnancy
 * @property integer $current_abuse
 * @property integer $problem_alcohol
 * @property integer $problem_work
 * @property integer $problem_friends
 * @property integer $overloaded
 * @property integer $psychosocial_assessmentcol
 * @property integer $score
 */
class PsychosocialAssessment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'psychosocial_assessment';
    }
	
	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mother_id', 'assessment_id','scale_id', 'financial_worries', 'money_worries', 'family_problems', 'move_recently_future', 'loss_loved', 'current_pregnancy', 'current_abuse', 'problem_alcohol', 'problem_work', 'problem_friends', 'overloaded','score'], 'integer'],
            [['created_dtm', 'last_updated_dtm'], 'safe'],
            [['updated_by'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mother_id' => 'Patient ID',
            'assessment_id' => 'assessment ID',
            'created_dtm' => 'Created Date',
            'last_updated_dtm' => 'Last Updated Date',
            'updated_by' => 'Updated By',
            'financial_worries' => 'Financial Worries',
            'money_worries' => 'Money Worries',
            'family_problems' => 'Family Problems',
            'move_recently_future' => 'Move Recently Future',
            'loss_loved' => 'Loss Loved',
            'current_pregnancy' => 'Current Pregnancy',
            'current_abuse' => 'Current Abuse',
            'problem_alcohol' => 'Problem Alcohol',
            'problem_work' => 'Problem Work',
            'problem_friends' => 'Problem Friends',
            'overloaded' => 'Overloaded',
            'score' => 'Score',
        ];
    }

      public function beforeSave($insert)
    {
        return true;
    }



public function afterFind() {
    return true; //don't forget this
}
}
