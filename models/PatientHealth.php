<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "patient_health".
 *
 * @property integer $id
 * @property integer $mother_id
 * @property integer $assessment_id
 * @property string $created_dtm
 * @property string $last_updated_dtm
 * @property string $updated_by
 * @property integer $little_interest/pleasure
 * @property integer $down_depressed/hopeless
 * @property integer $falling_sleep
 * @property integer $appetite_overeating
 * @property integer $bad_yourself
 * @property integer $trouble_concentrating
 * @property integer $moving/speaking_slowly
 * @property integer $thought_hurting_yourself
 * @property integer $anxiety_attack
 * @property integer $happen_before
 * @property integer $attack_suddenly
 * @property integer $attack_bother
 * @property integer $short_breath
 * @property integer $heart_race
 * @property integer $chest_pain
 * @property integer $sweat
 * @property integer $choking
 * @property integer $hot_flashes/chills
 * @property integer $nausea
 * @property integer $dizzy_unsteady
 * @property integer $tingling_numbness
 * @property integer $tremble
 * @property integer $afraid_dieing
 * @property integer $nervous
 * @property integer $restless
 * @property integer $tired_easily
 * @property integer $muscle_tension
 * @property integer $trouble_falling/staying_asleep
 * @property integer $troble_concentrating
 * @property integer $easily_annoyed
 * @property integer $difficulty_problem
 * @property integer $patient_healthcol
 * @property integer $score
 */
class PatientHealth extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'patient_health';
    }
	
	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mother_id', 'assessment_id','scale_id' ,'little_interest_pleasure', 'down_depressed_hopeless', 'falling_sleep','tired', 'appetite_overeating', 'bad_yourself', 'trouble_concentrating', 'moving_speaking_slowly', 'thought_hurting_yourself', 'anxiety_attack', 'happen_before', 'attack_suddenly', 'attack_bother', 'short_breath', 'heart_race', 'chest_pain', 'sweat', 'choking', 'hot_flashes_chills', 'nausea', 'dizzy_unsteady', 'tingling_numbness', 'tremble', 'afraid_dieing', 'nervous', 'restless', 'tired_easily', 'muscle_tension', 'trouble_falling_staying_asleep', 'troble_concentrating', 'easily_annoyed', 'difficulty_problem', 'score'], 'integer'],
            [['created_dtm', 'last_updated_dtm'], 'safe'],
            [['updated_by'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mother_id' => 'Patient ID',
            'assessment_id' => 'assessment ID',
            'created_dtm' => 'Created Date',
            'last_updated_dtm' => 'Last Updated Date',
            'updated_by' => 'Updated By',
            'little_interest_pleasure' => 'Little Interest/pleasure',
            'down_depressed_hopeless' => 'Down Depressed/hopeless',
            'falling_sleep' => 'Falling Sleep',
            'appetite_overeating' => 'Appetite Overeating',
            'bad_yourself' => 'Bad Yourself',
            'trouble_concentrating' => 'Trouble Concentrating',
            'moving_speaking_slowly' => 'Moving/speaking Slowly',
            'thought_hurting_yourself' => 'Thought Hurting Yourself',
            'anxiety_attack' => 'Anxiety Attack',
            'happen_before' => 'Happen Before',
            'attack_suddenly' => 'Attack Suddenly',
            'attack_bother' => 'Attack Bother',
            'short_breath' => 'Short Breath',
            'heart_race' => 'Heart Race',
            'chest_pain' => 'Chest Pain',
            'sweat' => 'Sweat',
            'choking' => 'Choking',
            'hot_flashes_chills' => 'Hot Flashes/chills',
            'nausea' => 'Nausea',
            'dizzy_unsteady' => 'Dizzy Unsteady',
            'tingling_numbness' => 'Tingling Numbness',
            'tremble' => 'Tremble',
            'afraid_dieing' => 'Afraid Dieing',
            'nervous' => 'Nervous',
            'restless' => 'Restless',
            'tired_easily' => 'Tired Easily',
            'muscle_tension' => 'Muscle Tension',
            'trouble_falling_staying_asleep' => 'Trouble Falling/staying Asleep',
            'troble_concentrating' => 'Troble Concentrating',
            'easily_annoyed' => 'Easily Annoyed',
            'difficulty_problem' => 'Difficulty Problem',
            'score' => 'Score',
			
        ];
    }

      public function beforeSave($insert)
    {
        return true;
    }



public function afterFind() {
    return true; //don't forget this
}
}
