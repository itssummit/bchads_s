<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pics".
 *
 * @property integer $id
 * @property integer $mother_id
 * @property integer $assessment_id
 * @property string $created_dtm
 * @property string $last_updated_dtm
 * @property string $updated_by
 * @property integer $like_touched
 * @property integer $like_talked
 * @property integer $like_sing
 * @property integer $like_leave
 * @property integer $like_hold
 * @property integer $like_put_down
 * @property integer $like_dummy
 * @property integer $suck_hands
 * @property integer $stroke_tummy
 * @property integer $stroke_back
 * @property integer $stroke_face
 * @property integer $stroke_arms
 * @property integer $pick_up
 * @property integer $talk
 * @property integer $cuddle
 * @property integer $rock
 * @property integer $kiss
 * @property integer $hold
 * @property integer $watch
 * @property integer $leave_lie_down
 * @property integer $score
 * @property integer $scale_id
 */
class Pics extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pics';
    }
	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mother_id', 'assessment_id', 'like_touched', 'like_talked', 'like_sing', 'like_leave', 'like_hold', 'like_put_down', 'like_dummy', 'suck_hands', 'stroke_tummy', 'stroke_back', 'stroke_face', 'stroke_arms', 'pick_up', 'talk', 'cuddle', 'rock', 'kiss', 'hold', 'watch', 'leave_lie_down', 'score', 'scale_id'], 'integer'],
            [['created_dtm', 'last_updated_dtm'], 'safe'],
            [['updated_by','best_of_likes'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mother_id' => 'Respondent ID',
            'assessment_id' => 'Assessment ID',
            'created_dtm' => 'Created Date',
            'last_updated_dtm' => 'Last Updated Date',
            'updated_by' => 'Updated By',
            'like_touched' => 'Like Touched',
            'like_talked' => 'Like Talked',
            'like_sing' => 'Like Sing',
            'like_leave' => 'Like Leave',
            'like_hold' => 'Like Hold',
            'like_put_down' => 'Like Put Down',
            'like_dummy' => 'Like Dummy',
            'suck_hands' => 'Suck Hands',
            'stroke_tummy' => 'Stroke Tummy',
            'stroke_back' => 'Stroke Back',
            'stroke_face' => 'Stroke Face',
            'stroke_arms' => 'Stroke Arms',
            'pick_up' => 'Pick Up',
            'talk' => 'Talk',
            'cuddle' => 'Cuddle',
            'rock' => 'Rock',
            'kiss' => 'Kiss',
            'hold' => 'Hold',
            'watch' => 'Watch',
            'leave_lie_down' => 'Leave Lie Down',
            'score' => 'Score',
            'scale_id' => 'Scale ID',
        ];
    }

      public function beforeSave($insert)
    {
        return true;
    }



public function afterFind() {
    return true; //don't forget this
}
}
