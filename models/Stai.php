<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "stai".
 *
 * @property integer $id
 * @property integer $mother_id
 * @property integer $assessment_id
 * @property string $created_dtm
 * @property string $last_updated_dtm
 * @property string $updated_by
 * @property string $feel_peace
 * @property string $feel_safe
 * @property string $feel_pressure
 * @property string $feel_tired
 * @property string $feel_nemmadi
 * @property string $feel_sad
 * @property string $feel_future_disaster
 * @property string $feel_enough
 * @property string $feel_fear
 * @property string $feel_free
 * @property string $feel_confident
 * @property string $feel_weak
 * @property string $feel_feared
 * @property string $cant_take_decisions
 * @property string $feel_samadhana
 * @property string $feel_santhushti
 * @property string $feel_sadness
 * @property string $feel_galibili
 * @property string $feel_drudamanasssu
 * @property string $feel_happy
 * @property string $happy
 * @property string $weak
 * @property string $thrupthi
 * @property string $happy_like_others
 * @property string $fail
 * @property string $free
 * @property string $safe
 * @property string $more_prob
 * @property string $thinks_lot
 * @property string $happiest
 * @property string $stopping_thoughts
 * @property string $not_confident
 * @property string $secure
 * @property string $takes_decisions
 * @property string $asamarpaka
 * @property string $santhushta
 * @property integer $score
 * @property integer $scale_id
 */
class Stai extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stai';
    }
	
	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mother_id', 'assessment_id', 'score', 'scale_id'], 'integer'],
            [['created_dtm', 'last_updated_dtm'], 'safe'],
            [['updated_by', 'feel_peace', 'feel_safe', 'feel_pressure', 'feel_tired', 'feel_nemmadi', 'feel_sad', 'feel_future_disaster', 'feel_enough', 'feel_fear', 'feel_free', 'feel_confident', 'feel_weak', 'feel_feared', 'cant_take_decisions', 'feel_samadhana', 'feel_santhushti', 'feel_sadness', 'feel_galibili', 'feel_drudamanasssu', 'feel_happy', 'happy', 'weak', 'thrupthi', 'happy_like_others', 'fail', 'free', 'safe', 'more_prob', 'thinks_lot', 'happiest', 'stopping_thoughts', 'not_confident', 'secure', 'takes_decisions', 'asamarpaka', 'santhushta','notimp_thoughts','nirashe','dhrudmanassu','udvega','not_app'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mother_id' => 'Respondent ID',
            'assessment_id' => 'Assessment ID',
            'created_dtm' => 'Created Dtm',
            'last_updated_dtm' => 'Last Updated Dtm',
            'updated_by' => 'Updated By',
            'feel_peace' => 'Feel Peace',
            'feel_safe' => 'Feel Safe',
            'feel_pressure' => 'Feel Pressure',
            'feel_tired' => 'Feel Tired',
            'feel_nemmadi' => 'Feel Nemmadi',
            'feel_sad' => 'Feel Sad',
            'feel_future_disaster' => 'Feel Future Disaster',
            'feel_enough' => 'Feel Enough',
            'feel_fear' => 'Feel Fear',
            'feel_free' => 'Feel Free',
            'feel_confident' => 'Feel Confident',
            'feel_weak' => 'Feel Weak',
            'feel_feared' => 'Feel Feared',
            'cant_take_decisions' => 'Cant Take Decisions',
            'feel_samadhana' => 'Feel Samadhana',
            'feel_santhushti' => 'Feel Santhushti',
            'feel_sadness' => 'Feel Sadness',
            'feel_galibili' => 'Feel Galibili',
            'feel_drudamanasssu' => 'Feel Drudamanasssu',
            'feel_happy' => 'Feel Happy',
            'happy' => 'Happy',
            'weak' => 'Weak',
            'thrupthi' => 'Thrupthi',
            'happy_like_others' => 'Happy Like Others',
            'fail' => 'Fail',
            'free' => 'Free',
            'safe' => 'Safe',
            'more_prob' => 'More Prob',
            'thinks_lot' => 'Thinks Lot',
            'happiest' => 'Happiest',
            'stopping_thoughts' => 'Stopping Thoughts',
            'not_confident' => 'Not Confident',
            'secure' => 'Secure',
            'takes_decisions' => 'Takes Decisions',
            'asamarpaka' => 'Asamarpaka',
            'santhushta' => 'Santhushta',
			'notimp_thoughts' =>'Notimp_thoughts',
			'nirashe' =>'Nirashe',
			'dhrudmanassu' =>'Dhrudmanassu',
			'udvega' =>'Udvega',
			'score' => 'Score',
            'scale_id' => 'Scale ID',
        ];
    }

      public function beforeSave($insert)
    {
        return true;
    }



public function afterFind() {
    return true; //don't forget this
}
}
