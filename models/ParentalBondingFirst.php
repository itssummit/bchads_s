<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "parental_bonding_first".
 *
 * @property integer $id
 * @property string $talks_friendly_mother
 * @property string $dint_help_needed_mother
 * @property string $allowed_loved_work_mother
 * @property string $emotional_mother
 * @property string $understands_pain_mother
 * @property string $loving_mother
 * @property string $allowed_own_decisions_mother
 * @property string $hates_growing_mother
 * @property string $stops_all_work_mother
 * @property string $dint_allow_alone_mother
 * @property string $happy_to_talk_mother
 * @property string $laughs_mother
 * @property string $treat_baby_mother
 * @property string $didnt_understand_needs_mother
 * @property string $decides_own_mother
 * @property string $dont_need_feel_mother
 * @property string $helps_in_sad_mother
 * @property string $didnt_talk_lot_mother
 * @property string $feels_dependent_mother
 * @property string $didnt_lookafter_mother
 * @property string $give_freedom_mother
 * @property string $allow_outside_mother
 * @property string $saves_more_mother
 * @property string $praises_mother
 * @property string $allow_own_style_mother
 * @property string $talks_friendly_father
 * @property string $dint_help_needed_father
 * @property string $allowed_loved_work_father
 * @property string $emotional_father
 * @property string $understands_pain_father
 * @property string $loving_father
 * @property string $allowed_own_decisions_father
 * @property string $hates_growing_father
 * @property string $stops_all_work_father
 * @property string $dint_allow_alone_father
 * @property string $happy_to_talk_father
 * @property string $laughs_father
 * @property string $treat_baby_father
 * @property string $didnt_understand_needs_father
 * @property string $decides_own_father
 * @property string $dont_need_feel_father
 * @property string $helps_in_sad_father
 * @property string $didnt_talk_lot_father
 * @property string $feels_dependent_father
 * @property string $didnt_lookafter_father
 * @property string $give_freedom_father
 * @property string $allow_outside_father
 * @property string $saves_more_father
 * @property string $praises_father
 * @property integer $allow_own_style_father
 * @property integer $respondent_id
 * @property integer $assessment_id
 * @property string $created_dtm
 * @property string $last_updated_dtm
 * @property string $updated_by
 * @property integer $score
 * @property integer $scale_id
 */
class ParentalBondingFirst extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parental_bonding_first';
    }
	
	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'respondent_id', 'assessment_id', 'score', 'scale_id'], 'integer'],
            [['created_dtm', 'last_updated_dtm'], 'safe'],
            [['talks_friendly_mother', 'not_applicable','allow_own_style_father','dint_help_needed_mother', 'allowed_loved_work_mother', 'emotional_mother', 'understands_pain_mother', 'loving_mother', 'allowed_own_decisions_mother', 'hates_growing_mother', 'stops_all_work_mother', 'dint_allow_alone_mother', 'happy_to_talk_mother', 'laughs_mother', 'treat_baby_mother', 'didnt_understand_needs_mother', 'decides_own_mother', 'dont_need_feel_mother', 'helps_in_sad_mother', 'didnt_talk_lot_mother', 'feels_dependent_mother', 'didnt_lookafter_mother', 'give_freedom_mother', 'allow_outside_mother', 'saves_more_mother', 'praises_mother', 'allow_own_style_mother', 'talks_friendly_father', 'dint_help_needed_father', 'allowed_loved_work_father', 'emotional_father', 'understands_pain_father', 'loving_father', 'allowed_own_decisions_father', 'hates_growing_father', 'stops_all_work_father', 'dint_allow_alone_father', 'happy_to_talk_father', 'laughs_father', 'treat_baby_father', 'didnt_understand_needs_father', 'decides_own_father', 'dont_need_feel_father', 'helps_in_sad_father', 'didnt_talk_lot_father', 'feels_dependent_father', 'didnt_lookafter_father', 'give_freedom_father', 'allow_outside_father', 'saves_more_father', 'praises_father', 'updated_by'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'talks_friendly_mother' => 'Talks Friendly Mother',
            'dint_help_needed_mother' => 'Dint Help Needed Mother',
            'allowed_loved_work_mother' => 'Allowed Loved Work Mother',
            'emotional_mother' => 'Emotional Mother',
            'understands_pain_mother' => 'Understands Pain Mother',
            'loving_mother' => 'Loving Mother',
            'allowed_own_decisions_mother' => 'Allowed Own Decisions Mother',
            'hates_growing_mother' => 'Hates Growing Mother',
            'stops_all_work_mother' => 'Stops All Work Mother',
            'dint_allow_alone_mother' => 'Dint Allow Alone Mother',
            'happy_to_talk_mother' => 'Happy To Talk Mother',
            'laughs_mother' => 'Laughs Mother',
            'treat_baby_mother' => 'Treat Baby Mother',
            'didnt_understand_needs_mother' => 'Didnt Understand Needs Mother',
            'decides_own_mother' => 'Decides Own Mother',
            'dont_need_feel_mother' => 'Dont Need Feel Mother',
            'helps_in_sad_mother' => 'Helps In Sad Mother',
            'didnt_talk_lot_mother' => 'Didnt Talk Lot Mother',
            'feels_dependent_mother' => 'Feels Dependent Mother',
            'didnt_lookafter_mother' => 'Didnt Lookafter Mother',
            'give_freedom_mother' => 'Give Freedom Mother',
            'allow_outside_mother' => 'Allow Outside Mother',
            'saves_more_mother' => 'Saves More Mother',
            'praises_mother' => 'Praises Mother',
            'allow_own_style_mother' => 'Allow Own Style Mother',
            'talks_friendly_father' => 'Talks Friendly Father',
            'dint_help_needed_father' => 'Dint Help Needed Father',
            'allowed_loved_work_father' => 'Allowed Loved Work Father',
            'emotional_father' => 'Emotional Father',
            'understands_pain_father' => 'Understands Pain Father',
            'loving_father' => 'Loving Father',
            'allowed_own_decisions_father' => 'Allowed Own Decisions Father',
            'hates_growing_father' => 'Hates Growing Father',
            'stops_all_work_father' => 'Stops All Work Father',
            'dint_allow_alone_father' => 'Dint Allow Alone Father',
            'happy_to_talk_father' => 'Happy To Talk Father',
            'laughs_father' => 'Laughs Father',
            'treat_baby_father' => 'Treat Baby Father',
            'didnt_understand_needs_father' => 'Didnt Understand Needs Father',
            'decides_own_father' => 'Decides Own Father',
            'dont_need_feel_father' => 'Dont Need Feel Father',
            'helps_in_sad_father' => 'Helps In Sad Father',
            'didnt_talk_lot_father' => 'Didnt Talk Lot Father',
            'feels_dependent_father' => 'Feels Dependent Father',
            'didnt_lookafter_father' => 'Didnt Lookafter Father',
            'give_freedom_father' => 'Give Freedom Father',
            'allow_outside_father' => 'Allow Outside Father',
            'saves_more_father' => 'Saves More Father',
            'praises_father' => 'Praises Father',
            'allow_own_style_father' => 'Allow Own Style Father',
            'respondent_id' => 'Respondent ID',
            'assessment_id' => 'Assessment ID',
            'created_dtm' => 'Created Dtm',
            'last_updated_dtm' => 'Last Updated Dtm',
            'updated_by' => 'Updated By',
            'score' => 'Score',
            'scale_id' => 'Scale ID',
        ];
    }
}
