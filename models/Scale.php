<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "scale".
 *
 * @property integer $id
 * @property string $scale_name
 * @property integer $scale_duration
 * @property integer $assessment_type
 * @property string $desc
 */
class Scale extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scale';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['scale_duration', 'assessment_type','display_sequence'], 'integer'],
        	[['table_name'],'string','max' => 45],
            [['scale_name', 'desc'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'scale_name' => 'Scale Name',
            'scale_duration' => 'Scale Duration',
            'assessment_type' => 'Assessment Type',
            'desc' => 'Desc',
        ];
    }
}
