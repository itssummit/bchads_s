<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "infant_immunization".
 *
 * @property integer $id
 * @property integer $respondent_id
 * @property integer $assessment_id
 * @property string $created_dtm
 * @property string $last_update_date
 * @property string $updated_by
 * @property string $delivery_date
 * @property integer $age
 * @property integer $hospitalization
 * @property integer $niuc_care
 * @property integer $neonatal_jaundice
 * @property integer $icu_care
 * @property integer $fevers
 * @property integer $exanthematous_fevers
 * @property integer $seizures
 * @property integer $asthma
 * @property integer $respiratory_illness
 * @property integer $diarrhoea
 * @property integer $no_of_visits
 * @property integer $burnt_times
 * @property integer $age_when_burnt
 * @property integer $what_person_did_burnt
 * @property integer $dropped_times
 * @property integer $age_when_dropped
 * @property integer $what_person_did_dropped
 * @property integer $swallow_times
 * @property integer $age_when_swallowed
 * @property integer $what_person_did_swallow
 * @property integer $accidents_times
 * @property integer $age_when_accident
 * @property integer $what_person_did_accident
 * @property integer $child_enclosedplace(smoking)_weekdays
 * @property integer $child_enclosedplace(smoking)_weekends
 * @property integer $vaccination_1.5_months
 * @property integer $vaccination_2.5_months
 * @property integer $vaccination_3.5_months
 * @property integer $vaccination_9_months
 * @property integer $vaccination_16-24_months
 * @property integer $score
 */
class InfantImmunization extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'infant_immunization';
    }
	
	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['respondent_id', 'assessment_id','scale_id', 'age', 'hospitalization', 'niuc_care', 'neonatal_jaundice', 'icu_care', 'fevers', 'exanthematous_fevers', 'seizures', 'asthma', 'respiratory_illness', 'diarrhoea', 'no_of_visits', 'burnt_times', 'age_when_burnt', 'what_person_did_burnt', 'dropped_times', 'age_when_dropped', 'what_person_did_dropped', 'swallow_times', 'age_when_swallowed', 'what_person_did_swallow', 'accidents_times', 'age_when_accident', 'what_person_did_accident', 'child_enclosedplace(smoking)_weekdays', 'child_enclosedplace(smoking)_weekends', 'vaccination_1.5_months', 'vaccination_2.5_months', 'vaccination_3.5_months', 'vaccination_9_months', 'vaccination_16-24_months', 'score'], 'integer'],
            [['created_dtm', 'last_updated_dtm', 'delivery_date'], 'safe'],
            [['updated_by'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'respondent_id' => 'Patient ID',
            'assessment_id' => 'assessment ID',
            'created_dtm' => 'Created Date',
            'last_updated_dtm' => 'Last Update Date',
            'updated_by' => 'Updated By',
            'delivery_date' => 'Delivery Date',
            'age' => 'Age',
            'hospitalization' => 'Hospitalization',
            'niuc_care' => 'Niuc Care',
            'neonatal_jaundice' => 'Neonatal Jaundice',
            'icu_care' => 'Icu Care',
            'fevers' => 'Fevers',
            'exanthematous_fevers' => 'Exanthematous Fevers',
            'seizures' => 'Seizures',
            'asthma' => 'Asthma',
            'respiratory_illness' => 'Respiratory Illness',
            'diarrhoea' => 'Diarrhoea',
            'no_of_visits' => 'No Of Visits',
            'burnt_times' => 'Burnt Times',
            'age_when_burnt' => 'Age When Burnt',
            'what_person_did_burnt' => 'What Person Did Burnt',
            'dropped_times' => 'Dropped Times',
            'age_when_dropped' => 'Age When Dropped',
            'what_person_did_dropped' => 'What Person Did Dropped',
            'swallow_times' => 'Swallow Times',
            'age_when_swallowed' => 'Age When Swallowed',
            'what_person_did_swallow' => 'What Person Did Swallow',
            'accidents_times' => 'Accidents Times',
            'age_when_accident' => 'Age When Accident',
            'what_person_did_accident' => 'What Person Did Accident',
            'child_enclosedplace(smoking)_weekdays' => 'Child Enclosedplace(smoking) Weekdays',
            'child_enclosedplace(smoking)_weekends' => 'Child Enclosedplace(smoking) Weekends',
            'vaccination_1.5_months' => 'Vaccination 1 5 Months',
            'vaccination_2.5_months' => 'Vaccination 2 5 Months',
            'vaccination_3.5_months' => 'Vaccination 3 5 Months',
            'vaccination_9_months' => 'Vaccination 9 Months',
            'vaccination_16-24_months' => 'Vaccination 16 24 Months',
            'score' => 'Score',
        ];
    }

      public function beforeSave($insert)
    {
        return true;
    }



public function afterFind() {
    return true; //don't forget this
}
}
