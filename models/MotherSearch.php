<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Mother;

/**
 * MotherSearch represents the model behind the search form about `app\models\Mother`.
 */
class MotherSearch extends Mother
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'age', 'phone1', 'phone2', 'no_of_family_members', 'total_family_income', 'duration_of_residence', 'no_of_years_of_marriage_years', 'no_of_years_of_marriage_months', 'current_visit', 'gestation', 'paternal_age_at_conception', 'walking_duration', 'exercise_duration', 'yoga_duration', 'householdchores_duration', 'others_duration', 'yearof_delivery', 'gestation_age_of_delivery'], 'integer'],
            [['first_name', 'middle_name', 'last_name', 'dob', 'present_address', 'permanent_address', 'domicile', 'religion', 'educational_qualification', 'occupation_Self', 'occupation_Spouse', 'socio_economic_status', 'marital_status', 'nature_of_marriage', 'family_type', 'spouse_substance_use', 'substance_type', 'dateofreg', 'placeofreg', 'next_appointment_ANC', 'gravida', 'parity', 'lmp', 'edd', 'tri_t1', 'tri_t2', 'tri_t3', 'medication_prior_pregnancy', 'medication', 'prescribed_medication_during_pregnancy', 'medication_during_pregnancy', 'fertility_treatment_past', 'fertility_treatment', 'planned_pregnancy', 'self_reaction_for_pregnancy', 'husband_reaction_for_pregnancy', 'family_reaction_for_pregnancy', 'sleep_disturbance', 'physical_activity', 'others_activity', 'risk_identified_for_pregnancy', 'risk_identified_in_foetus', 'delivery', 'mode_of_delivery', 'complications_during_pregnancy', 'complications_in_baby', 'complications_in_mother', 'risk_identified_in_foetus_past', 'abortions_miscarrieges', 'medical_surgical_termination_related_complication', 'contraception', 'experience_in_previouspregnancy_postdelivery', 'specify_experience', 'pap_test', 'lactation_related_problems', 'baby_ability_to_suck', 'production_of_breast_milk', 'congenital_anomolies', 'special_children', 'mental_illness', 'postpartum_mental_health_issues', 'postpartum_mental_health_deatails', 'created_dtm', 'last_updated_dtm', 'updated_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Mother::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'dob' => $this->dob,
            'age' => $this->age,
            'phone1' => $this->phone1,
            'phone2' => $this->phone2,
            'no_of_family_members' => $this->no_of_family_members,
            'total_family_income' => $this->total_family_income,
            'duration_of_residence' => $this->duration_of_residence,
            'no_of_years_of_marriage_years' => $this->no_of_years_of_marriage_years,
            'no_of_years_of_marriage_months' => $this->no_of_years_of_marriage_months,
            'dateofreg' => $this->dateofreg,
            'current_visit' => $this->current_visit,
            'gestation' => $this->gestation,
            'paternal_age_at_conception' => $this->paternal_age_at_conception,
            'walking_duration' => $this->walking_duration,
            'exercise_duration' => $this->exercise_duration,
            'yoga_duration' => $this->yoga_duration,
            'householdchores_duration' => $this->householdchores_duration,
            'others_duration' => $this->others_duration,
            'yearof_delivery' => $this->yearof_delivery,
            'gestation_age_of_delivery' => $this->gestation_age_of_delivery,
            'created_dtm' => $this->created_dtm,
            'last_updated_dtm' => $this->last_updated_dtm,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'middle_name', $this->middle_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'present_address', $this->present_address])
            ->andFilterWhere(['like', 'permanent_address', $this->permanent_address])
            ->andFilterWhere(['like', 'domicile', $this->domicile])
            ->andFilterWhere(['like', 'religion', $this->religion])
            ->andFilterWhere(['like', 'educational_qualification', $this->educational_qualification])
            ->andFilterWhere(['like', 'occupation_Self', $this->occupation_Self])
            ->andFilterWhere(['like', 'occupation_Spouse', $this->occupation_Spouse])
            ->andFilterWhere(['like', 'socio_economic_status', $this->socio_economic_status])
            ->andFilterWhere(['like', 'marital_status', $this->marital_status])
            ->andFilterWhere(['like', 'nature_of_marriage', $this->nature_of_marriage])
            ->andFilterWhere(['like', 'family_type', $this->family_type])
            ->andFilterWhere(['like', 'spouse_substance_use', $this->spouse_substance_use])
            ->andFilterWhere(['like', 'substance_type', $this->substance_type])
            ->andFilterWhere(['like', 'placeofreg', $this->placeofreg])
            ->andFilterWhere(['like', 'next_appointment_ANC', $this->next_appointment_ANC])
            ->andFilterWhere(['like', 'gravida', $this->gravida])
            ->andFilterWhere(['like', 'parity', $this->parity])
            ->andFilterWhere(['like', 'lmp', $this->lmp])
            ->andFilterWhere(['like', 'edd', $this->edd])
            ->andFilterWhere(['like', 'tri_t1', $this->tri_t1])
            ->andFilterWhere(['like', 'tri_t2', $this->tri_t2])
            ->andFilterWhere(['like', 'tri_t3', $this->tri_t3])
            ->andFilterWhere(['like', 'medication_prior_pregnancy', $this->medication_prior_pregnancy])
            ->andFilterWhere(['like', 'medication', $this->medication])
            ->andFilterWhere(['like', 'prescribed_medication_during_pregnancy', $this->prescribed_medication_during_pregnancy])
            ->andFilterWhere(['like', 'medication_during_pregnancy', $this->medication_during_pregnancy])
            ->andFilterWhere(['like', 'fertility_treatment_past', $this->fertility_treatment_past])
            ->andFilterWhere(['like', 'fertility_treatment', $this->fertility_treatment])
            ->andFilterWhere(['like', 'planned_pregnancy', $this->planned_pregnancy])
            ->andFilterWhere(['like', 'self_reaction_for_pregnancy', $this->self_reaction_for_pregnancy])
            ->andFilterWhere(['like', 'husband_reaction_for_pregnancy', $this->husband_reaction_for_pregnancy])
            ->andFilterWhere(['like', 'family_reaction_for_pregnancy', $this->family_reaction_for_pregnancy])
            ->andFilterWhere(['like', 'sleep_disturbance', $this->sleep_disturbance])
            ->andFilterWhere(['like', 'physical_activity', $this->physical_activity])
            ->andFilterWhere(['like', 'others_activity', $this->others_activity])
            ->andFilterWhere(['like', 'risk_identified_for_pregnancy', $this->risk_identified_for_pregnancy])
            ->andFilterWhere(['like', 'risk_identified_in_foetus', $this->risk_identified_in_foetus])
            ->andFilterWhere(['like', 'delivery', $this->delivery])
            ->andFilterWhere(['like', 'mode_of_delivery', $this->mode_of_delivery])
            ->andFilterWhere(['like', 'complications_during_pregnancy', $this->complications_during_pregnancy])
            ->andFilterWhere(['like', 'complications_in_baby', $this->complications_in_baby])
            ->andFilterWhere(['like', 'complications_in_mother', $this->complications_in_mother])
            ->andFilterWhere(['like', 'risk_identified_in_foetus_past', $this->risk_identified_in_foetus_past])
            ->andFilterWhere(['like', 'abortions_miscarrieges', $this->abortions_miscarrieges])
            ->andFilterWhere(['like', 'medical_surgical_termination_related_complication', $this->medical_surgical_termination_related_complication])
            ->andFilterWhere(['like', 'contraception', $this->contraception])
            ->andFilterWhere(['like', 'experience_in_previouspregnancy_postdelivery', $this->experience_in_previouspregnancy_postdelivery])
            ->andFilterWhere(['like', 'specify_experience', $this->specify_experience])
            ->andFilterWhere(['like', 'pap_test', $this->pap_test])
            ->andFilterWhere(['like', 'lactation_related_problems', $this->lactation_related_problems])
            ->andFilterWhere(['like', 'baby_ability_to_suck', $this->baby_ability_to_suck])
            ->andFilterWhere(['like', 'production_of_breast_milk', $this->production_of_breast_milk])
            ->andFilterWhere(['like', 'congenital_anomolies', $this->congenital_anomolies])
            ->andFilterWhere(['like', 'special_children', $this->special_children])
            ->andFilterWhere(['like', 'mental_illness', $this->mental_illness])
            ->andFilterWhere(['like', 'postpartum_mental_health_issues', $this->postpartum_mental_health_issues])
            ->andFilterWhere(['like', 'postpartum_mental_health_deatails', $this->postpartum_mental_health_deatails])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by]);

        return $dataProvider;
    }
}
