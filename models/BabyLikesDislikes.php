<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "baby_likes_dislikes".
 *
 * @property integer $id
 * @property integer $mother_id
 * @property integer $assessment_id
 * @property string $created_dtm
 * @property string $last_updated_dtm
 * @property string $updated_by
 * @property integer $idbaby_likes_dislikes
 * @property integer $fussing_crying
 * @property integer $toss_cot
 * @property integer $fuss_immediately
 * @property integer $play_quitely
 * @property integer $cry_someone_didnt_come
 * @property integer $angry
 * @property integer $contented_corner
 * @property integer $cry_before_nap
 * @property integer $lie_sit_quietly
 * @property integer $squirm
 * @property integer $fuss_facewash
 * @property integer $fuss_hairwash
 * @property integer $cry_removed_playrhing
 * @property integer $not_bothered_removed_plaything
 * @property integer $protest
 * @property integer $fuss_back
 * @property integer $upset_wanted_something
 * @property integer $tantrums_wanted_something
 * @property integer $distress_infant_seat
 * @property integer $cry_parents_chane_appearence
 * @property integer $startle_sudden_change
 * @property integer $startle_noise
 * @property integer $cling_parent_unfamiliar_person
 * @property integer $refuse_unfamiliar_person
 * @property integer $hang_back_unfamiliar_person
 * @property integer $never_warmup_unfamiliar_person
 * @property integer $cling_several_unfamiliar_persons
 * @property integer $cry_unfamiliar_adults
 * @property integer $upset_morethan_10min
 * @property integer $distress_new_place
 * @property integer $upset_new_place
 * @property integer $allow_unfamiliar_person_pickup
 * @property integer $distress_outside
 * @property integer $cry_taken_outside
 * @property integer $cry_unfamilar_person_pickup
 * @property integer $score
 */
class BabyLikesDislikes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'baby_likes_dislikes';
    }
	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mother_id', 'assessment_id','scale_id', 'fussing_crying', 'toss_cot', 'fuss_immediately', 'play_quitely', 'cry_someone_didnt_come', 'angry', 'contented_corner', 'cry_before_nap', 'lie_sit_quietly', 'squirm', 'fuss_facewash', 'fuss_hairwash', 'cry_removed_playrhing', 'not_bothered_removed_plaything', 'protest', 'fuss_back', 'upset_wanted_something', 'tantrums_wanted_something', 'distress_infant_seat', 'cry_parents_chane_appearence', 'startle_sudden_change', 'startle_noise', 'cling_parent_unfamiliar_person', 'refuse_unfamiliar_person', 'hang_back_unfamiliar_person', 'never_warmup_unfamiliar_person', 'cling_several_unfamiliar_persons', 'cry_unfamiliar_adults', 'upset_morethan_10min', 'distress_new_place', 'upset_new_place', 'allow_unfamiliar_person_pickup', 'distress_outside', 'cry_taken_outside', 'cry_unfamilar_person_pickup', 'score'], 'integer'],
            [['created_dtm', 'last_updated_dtm'], 'safe'],
            [['updated_by'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mother_id' => 'Patient ID',
            'assessment_id' => 'assessment ID',
            'created_dtm' => 'Created Date',
            'last_updated_dtm' => 'Last Updated Date',
            'updated_by' => 'Updated By',
            'idbaby_likes_dislikes' => 'Idbaby Likes Dislikes',
            'fussing_crying' => 'Fussing Crying',
            'toss_cot' => 'Toss Cot',
            'fuss_immediately' => 'Fuss Immediately',
            'play_quitely' => 'Play Quitely',
            'cry_someone_didnt_come' => 'Cry Someone Didnt Come',
            'angry' => 'Angry',
            'contented_corner' => 'Contented Corner',
            'cry_before_nap' => 'Cry Before Nap',
            'lie_sit_quietly' => 'Lie Sit Quietly',
            'squirm' => 'Squirm',
            'fuss_facewash' => 'Fuss Facewash',
            'fuss_hairwash' => 'Fuss Hairwash',
            'cry_removed_playrhing' => 'Cry Removed Playrhing',
            'not_bothered_removed_plaything' => 'Not Bothered Removed Plaything',
            'protest' => 'Protest',
            'fuss_back' => 'Fuss Back',
            'upset_wanted_something' => 'Upset Wanted Something',
            'tantrums_wanted_something' => 'Tantrums Wanted Something',
            'distress_infant_seat' => 'Distress Infant Seat',
            'cry_parents_chane_appearence' => 'Cry Parents Chane Appearence',
            'startle_sudden_change' => 'Startle Sudden Change',
            'startle_noise' => 'Startle Noise',
            'cling_parent_unfamiliar_person' => 'Cling Parent Unfamiliar Person',
            'refuse_unfamiliar_person' => 'Refuse Unfamiliar Person',
            'hang_back_unfamiliar_person' => 'Hang Back Unfamiliar Person',
            'never_warmup_unfamiliar_person' => 'Never Warmup Unfamiliar Person',
            'cling_several_unfamiliar_persons' => 'Cling Several Unfamiliar Persons',
            'cry_unfamiliar_adults' => 'Cry Unfamiliar Adults',
            'upset_morethan_10min' => 'Upset Morethan 10min',
            'distress_new_place' => 'Distress New Place',
            'upset_new_place' => 'Upset New Place',
            'allow_unfamiliar_person_pickup' => 'Allow Unfamiliar Person Pickup',
            'distress_outside' => 'Distress Outside',
            'cry_taken_outside' => 'Cry Taken Outside',
            'cry_unfamilar_person_pickup' => 'Cry Unfamilar Person Pickup',
            'score' => 'Score',
        ];
    }  

     public function beforeSave($insert)
    {
        return true;
    }



public function afterFind() {
    return true; //don't forget this
}
}
