<?php

namespace app\models;

use Yii;
use app\models\DbBackupHistory;

class Services extends \yii\db\ActiveRecord
{
    public static function TriggerBackUp($type,$backup_by,$dir){
        $dateTime = date('m-d-Y_hia');
        $timestamp = date_default_timezone_get();
        $dir = $dir."/cronJob/";
        $my_file = $dir.$type.'.bat';
        $filename = $dateTime.'.sql';
        $destination = Services::GetDestinationFolder($type,$filename);
        if(Services::WriteToCommonbat($my_file,$destination,$dir)){
            $date_before_start = date('Y-m-d H:i:s');
            exec($dir.'final.bat');
            $date_after_end = date('Y-m-d H:i:s');
            $diff = strtotime($date_after_end) - strtotime($date_before_start);
            $diff = (string)$diff." Sec";
            Services::SaveData($type,"success",$diff,$filename,$destination,$backup_by);
            return json_encode(array("status"=>"success","newLocaltion"=>$destination));
        }
        return json_encode(array("status"=>"failure"));
    }

    public static function WriteToCommonbat($my_file,$destination,$dir){
        $handle = fopen($my_file, 'r');
        $data = fread($handle,filesize($my_file));
        $data = str_replace("{{destination}}",$destination,$data);
        $my_file = $dir.'final.bat';
        $handle = fopen($my_file, 'w') or die('Cannot open file:  '.$my_file);
        fwrite($handle, $data);
        return true;
    }

    public static function SaveData($type,$result,$duration,$filename,$destination,$backup_by){
        $data = array();
        $data['type'] = $type;
        $data['result'] = $result;
        $data['backup_by'] = $backup_by;
        $data['timestamp'] = date_default_timezone_get();
        $data['duration'] = $duration;
        $data['filename'] = $filename;
        $data['location'] = $destination;
        $data['created_dtm'] = date('Y-m-d H:i:s');
        $data['last_updated_dtm'] = date('Y-m-d H:i:s');
        $model = new DbBackupHistory();
        $model->attributes = $data;
        //var_dump($model);exit;
        return $model->save();
    }
    public static function GetDestinationFolder($type,$filename){
        $data = Yii::$app->db->createCommand("
                    SELECT destination_folder FROM db_backup_config
                    WHERE type = '".$type."'"
                    )->queryOne();
        return $data['destination_folder']."/".$filename;
    }
}
