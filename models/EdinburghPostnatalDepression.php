<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "edinburgh_postnatal_depression".
 *
 * @property integer $id
 * @property integer $mother_id
 * @property integer $assessment_id
 * @property string $created_dtm
 * @property string $last_updated_dtm
 * @property string $updated_by
 * @property integer $feeling_past_7days_laugh
 * @property integer $feeling_past_7days_enjoyment
 * @property integer $feeling_past_7days_blamingmyself
 * @property integer $feeling_past_7days_anxious_worried
 * @property integer $feeling_past_7days_scared_panicky
 * @property integer $feeling_past_7days_things_getting_topofme
 * @property integer $feeling_past_7days_unhappy_difficulty_sleeping
 * @property integer $feeling_past_7days_sad_miserable
 * @property integer $feeling_past_7days_unhappy_crying
 * @property integer $feeling_past_7days_harming_myself
 * @property integer $score
 * @property integer $scale_id
 */
class EdinburghPostnatalDepression extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'edinburgh_postnatal_depression';
    }
	
	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mother_id', 'assessment_id','feeling_past_7days_laugh','feeling_past_7days_enjoyment', 'feeling_past_7days_blamingmyself', 'feeling_past_7days_anxious_worried', 'feeling_past_7days_scared_panicky', 'feeling_past_7days_things_getting_topofme', 'feeling_past_7days_unhappy_difficulty_sleeping', 'feeling_past_7days_sad_miserable', 'feeling_past_7days_unhappy_crying', 'feeling_past_7days_harming_myself', 'score', 'scale_id'], 'integer'],
            [['created_dtm', 'last_updated_dtm'], 'safe'],
            [['updated_by'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mother_id' => 'Respondent ID',
            'assessment_id' => 'assessment ID',
            'created_dtm' => 'Created Date',
            'last_updated_dtm' => 'Last Updated Date',
            'updated_by' => 'Updated By',
            'feeling_past_7days_laugh' => 'Feeling Past 7days Laugh',
            'feeling_past_7days_enjoyment' => 'Feeling Past 7days Enjoyment',
            'feeling_past_7days_blamingmyself' => 'Feeling Past 7days Blamingmyself',
            'feeling_past_7days_anxious_worried' => 'Feeling Past 7days Anxious Worried',
            'feeling_past_7days_scared_panicky' => 'Feeling Past 7days Scared Panicky',
            'feeling_past_7days_things_getting_topofme' => 'Feeling Past 7days Things Getting Topofme',
            'feeling_past_7days_unhappy_difficulty_sleeping' => 'Feeling Past 7days Unhappy Difficulty Sleeping',
            'feeling_past_7days_sad_miserable' => 'Feeling Past 7days Sad Miserable',
            'feeling_past_7days_unhappy_crying' => 'Feeling Past 7days Unhappy Crying',
            'feeling_past_7days_harming_myself' => 'Feeling Past 7days Harming Myself',
            'score' => 'Score',
            'scale_id' => 'Scale ID',
			
        ];
    }

      public function beforeSave($insert)
    {
        return true;
    }



public function afterFind() {
    return true; //don't forget this
}
}
