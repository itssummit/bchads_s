<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "infant_anthropomatric".
 *
 * @property integer $id
 * @property integer $respondent_id
 * @property integer $assessment_id
 * @property string $created_dtm
 * @property string $last_updated_dtm
 * @property string $updated_by
 * @property double $length_8_weeks
 * @property double $length_6_months
 * @property double $length_1_year
 * @property double $length_2_year
 * @property double $weight_8_weeks
 * @property double $weight_6_months
 * @property double $weight_1_year
 * @property double $weight_2_year
 * @property double $head_circumference_8_weeks
 * @property double $head_circumference_6_months
 * @property double $head_circumference_1_year
 * @property double $head_circumference_2_year
 * @property double $chest_circumference_8_weeks
 * @property double $chest_circumference_6_months
 * @property double $chest_circumference_1_year
 * @property double $chest_circumference_2_year
 * @property double $midarm_circumference_8_weeks
 * @property double $midarm_circumference_6_months
 * @property double $midarm_circumference_1_year
 * @property double $midarm_circumference_2_year
 * @property double $waist_circumference_8_weeks
 * @property double $waist_circumference_6_months
 * @property double $waist_circumference_1_year
 * @property double $waist_circumference_2_year
 * @property double $hip_circumference_8_weeks
 * @property double $hip_circumference_6_months
 * @property double $hip_circumference_1_year
 * @property double $hip_circumference_2_year
 * @property double $sub_scapular_skin_fold_thickness_8_weeks
 * @property double $sub_scapular_skin_fold_thickness_6_months
 * @property double $sub_scapular_skin_fold_thickness_1_year
 * @property double $sub_scapular_skin_fold_thickness_2_year
 * @property integer $score
 * @property integer $scale_id
 */
class InfantAnthropomatric extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'infant_anthropomatric';
    }
	private static $_db;
    
    public static function getDb() {
        if (isset(self::$_db)) {
            return self::$_db;
        }
        return \yii\db\ActiveRecord::getDb();
    }
    
    public static function setDb($db) {
        self::$_db = $db;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['respondent_id', 'assessment_id', 'score', 'scale_id'], 'integer'],
            [['created_dtm', 'last_updated_dtm'], 'safe'],
            [['length_8_weeks', 'length_6_months', 'length_1_year', 'length_2_year', 'weight_8_weeks', 'weight_6_months', 'weight_1_year', 'weight_2_year', 'head_circumference_8_weeks', 'head_circumference_6_months', 'head_circumference_1_year', 'head_circumference_2_year', 'chest_circumference_8_weeks', 'chest_circumference_6_months', 'chest_circumference_1_year', 'chest_circumference_2_year', 'midarm_circumference_8_weeks', 'midarm_circumference_6_months', 'midarm_circumference_1_year', 'midarm_circumference_2_year', 'waist_circumference_8_weeks', 'waist_circumference_6_months', 'waist_circumference_1_year', 'waist_circumference_2_year', 'hip_circumference_8_weeks', 'hip_circumference_6_months', 'hip_circumference_1_year', 'hip_circumference_2_year', 'sub_scapular_skin_fold_thickness_8_weeks', 'sub_scapular_skin_fold_thickness_6_months', 'sub_scapular_skin_fold_thickness_1_year', 'sub_scapular_skin_fold_thickness_2_year'], 'number'],
            [['updated_by','length_6_months_desc','weight_6_months_desc','head_circumference_6_months_desc','chest_circumference_6_months_desc','midarm_circumference_6_months_desc','waist_circumference_6_months_desc','hip_circumference_6_months_desc','length_1_year_desc','weight_1_year_desc','head_circumference_1_year_desc','chest_circumference_1_year_desc','midarm_circumference_1_year_desc','waist_circumference_1_year_desc','hip_circumference_1_year_desc','length_2_year_desc','weight_2_year_desc','head_circumference_2_year_desc','chest_circumference_2_year_desc','midarm_circumference_2_year_desc','waist_circumference_2_year_desc','hip_circumference_2_year_desc'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'respondent_id' => 'Respondent ID',
            'assessment_id' => 'Assessment ID',
            'created_dtm' => 'Created Dtm',
            'last_updated_dtm' => 'Last Updated Dtm',
            'updated_by' => 'Updated By',
            'length_8_weeks' => 'Length 8 Weeks',
            'length_6_months' => 'Length 6 Months',
            'length_1_year' => 'Length 1 Year',
            'length_2_year' => 'Length 2 Year',
            'weight_8_weeks' => 'Weight 8 Weeks',
            'weight_6_months' => 'Weight 6 Months',
            'weight_1_year' => 'Weight 1 Year',
            'weight_2_year' => 'Weight 2 Year',
            'head_circumference_8_weeks' => 'Head Circumference 8 Weeks',
            'head_circumference_6_months' => 'Head Circumference 6 Months',
            'head_circumference_1_year' => 'Head Circumference 1 Year',
            'head_circumference_2_year' => 'Head Circumference 2 Year',
            'chest_circumference_8_weeks' => 'Chest Circumference 8 Weeks',
            'chest_circumference_6_months' => 'Chest Circumference 6 Months',
            'chest_circumference_1_year' => 'Chest Circumference 1 Year',
            'chest_circumference_2_year' => 'Chest Circumference 2 Year',
            'midarm_circumference_8_weeks' => 'Midarm Circumference 8 Weeks',
            'midarm_circumference_6_months' => 'Midarm Circumference 6 Months',
            'midarm_circumference_1_year' => 'Midarm Circumference 1 Year',
            'midarm_circumference_2_year' => 'Midarm Circumference 2 Year',
            'waist_circumference_8_weeks' => 'Waist Circumference 8 Weeks',
            'waist_circumference_6_months' => 'Waist Circumference 6 Months',
            'waist_circumference_1_year' => 'Waist Circumference 1 Year',
            'waist_circumference_2_year' => 'Waist Circumference 2 Year',
            'hip_circumference_8_weeks' => 'Hip Circumference 8 Weeks',
            'hip_circumference_6_months' => 'Hip Circumference 6 Months',
            'hip_circumference_1_year' => 'Hip Circumference 1 Year',
            'hip_circumference_2_year' => 'Hip Circumference 2 Year',
            'sub_scapular_skin_fold_thickness_8_weeks' => 'Sub Scapular Skin Fold Thickness 8 Weeks',
            'sub_scapular_skin_fold_thickness_6_months' => 'Sub Scapular Skin Fold Thickness 6 Months',
            'sub_scapular_skin_fold_thickness_1_year' => 'Sub Scapular Skin Fold Thickness 1 Year',
            'sub_scapular_skin_fold_thickness_2_year' => 'Sub Scapular Skin Fold Thickness 2 Year',
            'score' => 'Score',
            'scale_id' => 'Scale ID',
        ];
    }
}
