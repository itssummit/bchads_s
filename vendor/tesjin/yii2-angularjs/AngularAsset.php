<?php
/**
 * @link http://www.tesjin.ru/
 * @copyright Copyright (c) 2014 TesJin Group
 * @license BSD-3-Clause
 */
 

namespace yii\angularjs;

use yii\web\AssetBundle;

/**
 * This asset bundle provides the [angular javascript library](https://angularjs.org/)
 *
 * @author Vladislav Orlov <orlov@tesjin.ru>
 */
class AngularAsset extends AssetBundle
{
    public $sourcePath = '@vendor/tesjin/yii2-angularjs';
    public $js = [
        'js/angular.js',
    	'js/daypilot.js',
        'js/ui-bootstrap-tpls-2.0.0.min.js',
    	'js/angularjs-dropdown-multiselect.min.js',
    	'js/dirPagination.js',
    	'js/app.js'
        
    ];
}
