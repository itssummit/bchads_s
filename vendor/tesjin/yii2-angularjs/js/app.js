var app = angular.module('bchadsApp',['ui.bootstrap','daypilot','angularUtils.directives.dirPagination']);

//Csrf Validation Enable for $http post
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.headers.post['X-CSRF-Token'] = $('meta[name="csrf-token"]').attr("content");
    $httpProvider.defaults.headers.common['Accept'] = 'application/json, text/javascript';
    $httpProvider.defaults.headers.common['Content-Type'] = 'application/json; charset=utf-8';
}]);

app.filter('unique', function() {
   return function(collection, keyname) {
      var output = [], 
          keys = [];

      angular.forEach(collection, function(item) {
          var key = item[keyname];
          if(keys.indexOf(key) === -1) {
              keys.push(key);
              output.push(item);
          }
      });

      return output;
   };
});

app.directive('stringToNumber', function() {
  return {
	    require: 'ngModel',
	    link: function(scope, element, attrs, ngModel) {
	      ngModel.$parsers.push(function(value) {
	        return '' + value;
	      });
	      ngModel.$formatters.push(function(value) {
	        return parseFloat(value);
	      });
	    }
	  };
});

app.directive('printIt',function(){
	return {
		restrict : 'E',
		replace : true,
		scope : {
			divId : '@printingid'
		},
		template : '<div class="hidden-print pull-right">'+
						'<button class="btn btn-info btn-md">'+
							'<span class="glyphicon glyphicon-print"></span>'+
						'</button>'+
					'</div>',
		link : function(scope,element,attrs){
			$(element).on('click',function(e){
				PrintSummary();
			})
			var PrintSummary = function(){
		        var printContents = document.getElementById(scope.divId).innerHTML;
		        var myWindow = window.open('');
		        //var myWindow = window.open('', '', 'width=1024,height=300');
		        var prefix = '<link rel="stylesheet" href="/bchads/web/css/bootstrap/bootstrap.min.css">';
		        myWindow.document.write(prefix + printContents);
		        myWindow.document.close();
		        myWindow.focus();
		        setTimeout(function(){
		        	myWindow.print();
				}, 500);
			}
		}
	};
})