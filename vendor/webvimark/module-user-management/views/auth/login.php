<?php
/**
 * @var $this yii\web\View
 * @var $model webvimark\modules\UserManagement\models\forms\LoginForm
 */

use webvimark\modules\UserManagement\components\GhostHtml;
use webvimark\modules\UserManagement\UserManagementModule;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
?>
<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
    <div id="login-wrapper">
    
	<div class="row">
		 <div class="col-xs-12 col-sm-5" >
		 	<span class="float-left">
                         <img src="/bchads/web/img/newlogo.png" alt="nimhans" height="100%" width="100%">
					</div></span>
					<div class="col-xs-12 col-sm-7" >
      <span class="float-right">
        
		<div  id="login-sheet" style="margin-top:5%;margin-left:20%">
			<div class="panel panel-danger" >
				<div class="panel-heading">

					<div class="panel-title">
                         <img src="/bchads/web/img/perinatal.png" alt="nimhans" height="25%" width="100%">
					</div>
				</div>
               
				<div class="panel-body" >

					<?php $form = ActiveForm::begin([
						'id'      => 'login-form',
						'options'=>['autocomplete'=>'off'],
						'validateOnBlur'=>false,
						'fieldConfig' => [
							'template'=>"{input}\n{error}",
						],
					]) ?>
                    
					    <?= $form->field($model, 'username')
						    ->textInput(['placeholder'=>$model->getAttributeLabel('username'), 'autocomplete'=>'off']) ?>

					    <?= $form->field($model, 'password')
						    ->passwordInput(['placeholder'=>$model->getAttributeLabel('password'), 'autocomplete'=>'off']) ?>

					    <?= $form->field($model, 'rememberMe')->checkbox(['value'=>true]) ?>
						
						
                   
						<?= Html::submitButton(
						UserManagementModule::t('front', 'Submit'),
						['class' => 'btn btn-lg btn-primary btn-block']
						) ?>
						<br/>
						<div class="pull-left">
							<?= Html::a('Forgot Password?', ['/user-management/auth/password-recovery'], ['class' => 'profile-link']) ?>
						</div>
						<!--<div class="pull-right">
						< ?= Html::a('New User?', ['/user-management/auth/registration'], ['class' => 'profile-link']) ?>
						</div>-->
						
					<div class="row registration-block">
						<div class="col-sm-6">
							<?= GhostHtml::a(
								UserManagementModule::t('front', "Registration"),
								['/user-management/auth/registration']
							) ?>
						</div>
						<div class="col-sm-6 text-right">
							<?= GhostHtml::a(
								UserManagementModule::t('front', "Forgot password ?"),
								['/user-management/auth/password-recovery']
							) ?>
						</div>
					</span>
					</div>




					<?php ActiveForm::end() ?>
				</div>
			</div>
		</div>

      
	</div>
    </div>
</div>
<?php
$css = <<<CSS
html, body{
  height: 100%;
}
body {
	//background-image: url('/bchads/web/img/image20134.jpg');
    background-repeat: no-repeat;
    background-attachment: fixed;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
    width: 100%;
    height: auto;
}
#login-sheet{
	margin-left:700px;
}
#login-wrapper {
	position: relative;
	top: 30%;
}
#login-wrapper .registration-block {
	margin-top: 15px;
}

.panel-body {
    background-image: url('/bchads/web/img/image20134.jpg');
    position: relative;
    background-repeat: no-repeat;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
    
}
.loginclass{
    background-image: url('/perinatal/web/img/noise.png');
     background: rgba(241,231,103,1);
background: -moz-linear-gradient(45deg, rgba(241,231,103,1) 0%, rgba(254,182,69,0.86) 28%, rgba(242,111,190,0.71) 57%, rgba(208,155,235,0.58) 82%, rgba(208,155,235,0.49) 100%);
background: -webkit-gradient(left bottom, right top, color-stop(0%, rgba(241,231,103,1)), color-stop(28%, rgba(254,182,69,0.86)), color-stop(57%, rgba(242,111,190,0.71)), color-stop(82%, rgba(208,155,235,0.58)), color-stop(100%, rgba(208,155,235,0.49)));
background: -webkit-linear-gradient(45deg, rgba(241,231,103,1) 0%, rgba(254,182,69,0.86) 28%, rgba(242,111,190,0.71) 57%, rgba(208,155,235,0.58) 82%, rgba(208,155,235,0.49) 100%);
background: -o-linear-gradient(45deg, rgba(241,231,103,1) 0%, rgba(254,182,69,0.86) 28%, rgba(242,111,190,0.71) 57%, rgba(208,155,235,0.58) 82%, rgba(208,155,235,0.49) 100%);
background: -ms-linear-gradient(45deg, rgba(241,231,103,1) 0%, rgba(254,182,69,0.86) 28%, rgba(242,111,190,0.71) 57%, rgba(208,155,235,0.58) 82%, rgba(208,155,235,0.49) 100%);
background: linear-gradient(45deg, rgba(241,231,103,1) 0%, rgba(254,182,69,0.86) 28%, rgba(242,111,190,0.71) 57%, rgba(208,155,235,0.58) 82%, rgba(208,155,235,0.49) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f1e767', endColorstr='#d09beb', GradientType=1 );

}
CSS;
$this->registerCss($css);
?>
<?php
$this->registerJs("
var gly = 'glyphicon glyphicon-';
var eyeClose =  '<span class = \"'+gly+'eye-close\"></span>';
var eyeOpen =  '<span class = \"'+gly+'eye-open\"></span>';
var divTag = '<div class=\"input-group\" id = \"passwordDiv\">';
var html = $('#loginform-password').clone();
var spanTag = '<span class=\"input-group-addon\" id = \"reveal-password\">'+eyeOpen+'</span></div>';
var type = 'password';
$('#loginform-password').before(divTag);
$('#loginform-password').remove();
$('#passwordDiv').append(html);
$('#passwordDiv').append(spanTag);
$('#reveal-password').click(function(){
	$('#reveal-password > span').remove();
	if(type == 'password')
	{
		type = 'text';
		$('#loginform-password').attr('type',type)
		$('#reveal-password').append(eyeClose);
	}
	else
	{
		type = 'password';
		$('#loginform-password').attr('type',type)
		$('#reveal-password').append(eyeOpen);
	}
});
",View::POS_READY);
?>
