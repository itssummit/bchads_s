<?php

namespace webvimark\modules\UserManagement\controllers;

use webvimark\components\AdminDefaultController;
use Yii;
use webvimark\modules\UserManagement\models\User;
use webvimark\modules\UserManagement\models\search\UserSearch;
use yii\web\NotFoundHttpException;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends AdminDefaultController
{
	/**
	 * @var User
	 */
	public $modelClass = 'webvimark\modules\UserManagement\models\User';

	/**
	 * @var UserSearch
	 */
	public $modelSearchClass = 'webvimark\modules\UserManagement\models\search\UserSearch';	
	
	public function multiSelect($model){
	
		if(!empty($model))
		{
			$tempArr = $model;
			$key = array_search("multiselect-all",$tempArr);
			if($key!==false){
				array_shift($tempArr);
			}
			$model = implode(",",$tempArr);
		}
		else
		{
			$model = null;
		}
		return $model;
	}
	
	/**
	 * @return mixed|string|\yii\web\Response
	 */
	public function actionCreate()
	{
		$model = new User(['scenario'=>'newUser']);

		if ( $model->load(Yii::$app->request->post()))
		{
			//$model->languages_spoken = $this->multiSelect($model->languages_spoken);
			//$model->languages_spoken_other = $this->multiSelect($model->languages_spoken_other);
			$model->save();
			return $this->redirect(['view',	'id' => $model->id]);
		}

		return $this->renderIsAjax('create', compact('model'));
	}
	
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		
		if ( $model->load(Yii::$app->request->post()))
		{
			$model->languages_spoken = $this->multiSelect($model->languages_spoken);
			$model->languages_spoken_other = $this->multiSelect($model->languages_spoken_other);
			$model->save();
			return $this->redirect(['view',	'id' => $model->id]);
		}
	
		return $this->renderIsAjax('update', compact('model'));
	}

	/**
	 * @param int $id User ID
	 *
	 * @throws \yii\web\NotFoundHttpException
	 * @return string
	 */
	public function actionChangePassword($id)
	{
		$model = User::findOne($id);

		if ( !$model )
		{
			throw new NotFoundHttpException('User not found');
		}

		$model->scenario = 'changePassword';

		if ( $model->load(Yii::$app->request->post()) && $model->save() )
		{
			return $this->redirect(['view',	'id' => $model->id]);
		}

		return $this->renderIsAjax('changePassword', compact('model'));
	}

}
