<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '210278',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],

        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\DbTarget',
                    'logTable' => 'error_log', 
                    'levels' => ['error'],
                ],
            ],
        ],
		'user' => [
        	'class' => 'webvimark\modules\UserManagement\components\UserConfig',
		    // Comment this if you don't want to record user logins
        	'on afterLogin' => function($event) {
            	    \webvimark\modules\UserManagement\models\UserVisitLog::newVisitor($event->identity->id);
	            }
	    ],


/*
'urlManager' => [
        'enablePrettyUrl' => true,
        'showScriptName' => false,
        'enableStrictParsing' => false,
       'rules' => array(
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
        ),
],*/

        'db' => require(__DIR__ . '/db.php'),
        'db1' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=192.168.1.7;dbname=bchads',
			'username' => 'root',
			'password' => 'root',
			'charset' => 'utf8',
        ],
    ],

	'modules'=>[
    	'user-management' => [
        	'class' => 'webvimark\modules\UserManagement\UserManagementModule',

	        // Here you can set your handler to change layout for any controller or action
    	    // Tip: you can use this event in any module
        	'on beforeAction'=>function(yii\base\ActionEvent $event) {
                if ( $event->action->uniqueId == 'user-management/auth/login' )
                {
                    $event->action->controller->layout = 'loginLayout.php';
                };
            },
    	],

	],        
    'params' => $params,
    'timeZone' => 'Asia/Calcutta'
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    	'allowedIPs' => ['127.0.0.1']
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['127.0.0.1', '::1', '192.168.1.*','192.168.0.*']
    ];
}

return $config;
