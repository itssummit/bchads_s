<?php

return [
    'adminEmail' => 'admin@example.com',
	//Day Before Yesterday -2 
	//Day After Tomorrow +2
	'dateIntervalCondition' => " < DATE_ADD(CURDATE(),INTERVAL -10 DAY)",
    'autoIncrementStarts' => 20000,
    'autoIncrementEnds' => 30000
];
