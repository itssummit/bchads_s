<?php 

namespace app\classes;

use Yii;
use yii\helpers\ArrayHelper;

class SyncOperations{
    
    private function getScaleTables(){
        return ['PatientHealth','BabyLikesDislikes','Bitsea','Breastfeeding','Cbcl','ChildEcbq','ChildGrowth','ConflictTacticsScale','EdinburghPostnatalDepression','Icmr','InfantAnthropomatric','InfantHealth','InfantImmunization','LifeEvents','Maternal','Multidimentional','ParentalBondingFirst','ParentalBondingSecond','PatientHealth','PhychologicalViolence','Pics','PsychosocialAssessment','PsychosocialRiskFactors','PtsdChecklist','Sass','Scores','SharedCaregiving','SomaticSymptoms','Stai','SuicideQuestions','WooleyDepression'];
    }
    
    private function update($obj,$tableModel){
        $id = $obj['id'];
        unset($obj['id']);
        $tableModel::updateAll($obj->toArray(),['id' => $id]);
    }
    
    private function insert($obj,$tableModel,$type){
        $model = new $tableModel();
        $model = $obj;
        $model->setIsNewRecord(true);
        if($type)
        {
            $model->id = $obj['id'];
        }
        else
        {
            unset($obj['id']);
        }
        $model->save(false);
    }
    
    private function processTables($tableName,$type = null){
        $tableModel = "app\\models\\$tableName";
        $tableModel::setDb(Yii::$app->db);
        $client = $tableModel::find()->all();
        $tableModel::setDb(Yii::$app->db1);
        $server = $tableModel::find()->all();
        $serverColumns = [];
        $clientColumns = [];
        $serverDiff = [];
        $clientDiff = [];
        //Type will define Assessment and All Scale Tables
        if($type)
        {
            $id = 'id';
        }
        else
        {
            $id = 'assessment_id';
        }
        
        /*
         * Get newly created Rows from server and client logic follows,
         *  For example, get only id columns from client and server table
         *  Compare both sides,it will produce not matching rows based on id column
         *  then insert server rows into client and insert client rows into server
         *  after inserting remove those rows from array because later it should not be used
         */
        $serverColumns = ArrayHelper::getColumn($server, $id);
        $clientColumns = ArrayHelper::getColumn($client, $id);
        $serverRows = array_diff($serverColumns,$clientColumns);
        $clientRows = array_diff($clientColumns,$serverColumns);
        
        if(count($serverRows) > 0)
        {
            //Client Database Connection
            $tableModel::setDb(Yii::$app->db);
            // Get Array keys -> only indexes
            $serverRows = array_keys($serverRows);
            // Reset Array Index
            $serverRows = array_values($serverRows);
            for($i = 0;$i < count($serverRows);$i++)
            {
                $index = $serverRows[$i];
                $this->insert($server[$index],$tableModel,$type);
                // Remove index
                unset($server[$index]);
            }
        }
        
        if(count($clientRows) > 0)
        {
            //Server Database Connection
            $tableModel::setDb(Yii::$app->db1);
            $clientRows = array_keys($clientRows);
            $clientRows = array_values($clientRows);
            for($i = 0;$i < count($clientRows);$i++)
            {
                $index = $clientRows[$i];
                $this->insert($client[$clientRows[$i]],$tableModel,$type);
                unset($client[$index]);
            }
        }
        
        /*
         * First will Check Client Results length
         * if
         *   it will check for Server Results length
         *      if  
         *          Client will check in server rows for exact matching all columns if it is not matching it will updated in server
         *      else
         *           all the client rows will be inserted into server
         * else
         *   all the server rows will be inserted into client
         */
        
        if(count($client) > 0)
        {
            $client = array_values($client);
            for($c = 0;$c < count($client);$c++)
            {
                if(count($server) > 0)
                {
                    $server = array_values($server);
                    for($s = 0;$s < count($server);$s++)
                    {
                        if($client[$c][$id] === $server[$s][$id])
                        {
                            //Comparing two arrays
                            if($client[$c]->toArray() !== $server[$s]->toArray())
                            {
                                $tableModel::setDb(Yii::$app->db1);
                                $this->update($client[$c], $tableModel);
                            }
                            break;
                        }
                    }
                }
                else 
                {
                    $tableModel::setDb(Yii::$app->db);
                    $this->insert($client[$c],$tableModel,$type);
                }
             }
         }         
         else if(count($server) > 0)
         {
            $server = array_values($server);
            $tableModel::setDb(Yii::$app->db);
            for($i = 0;$i < count($server);$i++)
            {
                $this->insert($server[$i],$tableModel,$type);
            }
         }
    }
    
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function process()
    {
        //Check DB Connection
        $checkDBConnection = Yii::$app->db1->createCommand('SHOW TABLES')->queryAll();
        
        if(count($checkDBConnection) > 0)
        {
            //Check Already Any Machine Connected
            $checkSyncLog = Yii::$app->db1->createCommand('Select * from sync_log where is_active = 1')->queryOne();
            
            //Checking Any Other Connected
            if(isset($checkSyncLog['id']))
            {
                return 'Already Some Other Device is Synchronizing';
            }
            else
            {
                //No Other Device has connected
                $machineName = gethostbyaddr($_SERVER['REMOTE_ADDR']);
                $scaleTables = $this->getScaleTables();
                
                //Making Entry in Sync Log;
                Yii::$app->db1->createCommand("INSERT INTO sync_log(`machine_name`,`is_active`) VALUES('$machineName',1)")->execute();
                
                $this->processTables('Assessment',1);
                
                for($i = 0;$i < count($scaleTables);$i++)
                {
                    $this->processTables($scaleTables[$i]);
                }
                
                //Closes the Connection
                Yii::$app->db1->createCommand("UPDATE sync_log SET `is_active` = 0 WHERE machine_name = '$machineName'")->execute();
                
                return 'Synchronized Successfully';
            }
        }
        else
        {
            return 'No Tables Found';
        }
    }   
}
?>