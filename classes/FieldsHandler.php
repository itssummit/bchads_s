<?php
namespace app\classes;

use Yii;
use app\models\Mother;
use app\models\Assessment;

class FieldsHandler{


     public $select= " id as MotherId, 
                       first_name as Firstname,
                       last_name as Lastname,
                       age as AGE
                     ";

	function checkfields($obj,$col){
	    foreach($col as $c)
		{
		    if($obj[$c] == null || $obj[$c] == '')
		    {
			    return false;
		    }
		}
		return true;
	}

    function checkAssessment()
	{
		$col = ['id','mother_id','assessment_type','respondent_id','respondent','relation_type','relationship_id'];
		$obj = Assessment::findOne(['id' => $this->assessmentId]);
		return $this->checkfields($obj,$col);
	}


}

?>