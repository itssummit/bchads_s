<?php

namespace app\classes;

use Yii;

class ExportData
{
        public function get(){
            $t = new TableNames();
            $tNames = $t->getScaleTables();
            $result = [];
            $motherData = \app\models\Mother::find()->select($t->mother['columns'])->asArray()->all();
            //$col=[""]
            foreach($motherData as $mdValue)
            {
                $assessmentData = \app\models\Assessment::find()->select($t->assessment['columns'])->where(['mother_id' => $mdValue['motherId']])->asArray()->all();        
                for($i = 0;$i < count($assessmentData);$i++)
                {
                    $scaleResult = [];
                    foreach($tNames as $tValue)
                    {
                        $tableName = $tValue['tableName'];
                        $className = "app\\models\\$tableName";
                        $model = new $className();
                        $scaleResult = $model::find()->select(array_keys($tValue['columns']))->where(['assessment_id' => $assessmentData[$i]['assessmentId']])->asArray()->one();
                        if(empty($scaleResult))
                        {
                            $assessmentData[$i] = array_merge($assessmentData[$i],$tValue['columns']);
                        }
                        else
                        {
                            $assessmentData[$i] = array_merge($assessmentData[$i],$scaleResult);    
                        }
                    }                    
                    array_push($result,array_merge($mdValue,$assessmentData[$i]));
                }             
            }
           
            return $result;
        }


       

}
?>

