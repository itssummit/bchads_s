<?php
namespace app\classes;

use Yii;

class RespondentType{
	
	public function getTypeId($type){
		switch($type){
			case 'm'	:	return 1;break;
			case 'i'	:	return 2;break;
			case 'm,ac'	:	return 3;break;
		}
	}
	
	public function getType($typeId){
		switch($typeId){
			case 1	:	return 'm';break;
			case 2	:	return 'i';break;
			case 3	:	return 'm,ac';break;
		}
	}
	
	public function getTypeName($typeId){
		switch($typeId){
			case 1	:	return 'Mother';break;
			case 2	:	return 'Interviewer';break;
			case 3	:	return 'CareGiver';break;
		}
	}
}