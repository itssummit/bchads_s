<?php
namespace app\classes;

use Yii;
use app\classes\AssessmentType;
use app\models\Scale;
use app\classes\RespondentType;

class NavigationHandler{
    
    public $data = ['prev' => null,'next' => null];


    public function setNavigation(){
    	$resp = new RespondentType();
        $displaySequence = Yii::$app->session->get('displaySequence');
        $assessType = Yii::$app->session->get('assessmentType');
		
		if($displaySequence)
        {
        	$respondentId = Yii::$app->session->get('respondentId');
        	if($respondentId==1 || $respondentId==3)
        	{
        		$prevQuery = "SELECT table_name,display_sequence from scale where ($assessType not in ('x') and $assessType in ('m','m,ac') and display_sequence < $displaySequence and table_name is not null) order by id desc limit 1";
        		$nextQuery = "SELECT table_name,display_sequence from scale where ($assessType not in ('x') and $assessType in ('m','m,ac') and display_sequence > $displaySequence and table_name is not null) limit 1";
        	}
        	else 
        	{
        		$respondentType = $resp->getType($respondentId);
        		$prevQuery = "SELECT table_name,display_sequence from scale where ($assessType not in ('x') and $assessType in ('$respondentType') and display_sequence < $displaySequence and table_name is not null) order by id desc limit 1";
        		$nextQuery = "SELECT table_name,display_sequence from scale where ($assessType not in ('x') and $assessType in ('$respondentType') and display_sequence > $displaySequence and table_name is not null) limit 1";
        	}
            
			$this->data['prev'] = Yii::$app->db->createCommand($prevQuery)->queryOne(); 
        	$this->data['next'] = Yii::$app->db->createCommand($nextQuery)->queryOne();
		}
		
		/*
        $respondentId = Yii::$app->session->get('respondentId');
		$relationshipId = Yii::$app->session->get('relationshipId');
        $scaleType = Yii::$app->session->get('scaleType');
        if($displaySequence)
        {
            $assement = new AssessmentType();
            $condition1 = $assement->getType($assessType,$relationshipId);
            $condition2 = ($scaleType)?"find_in_set('$scaleType',type)":"type is not null";
            if($condition1)
            {
                $this->data['prev'] = Scale::findBySql("SELECT table_name,display_sequence from scale where  (".$condition1.") and $condition2 and display_sequence < $displaySequence and table_name is not null order by id desc limit 1")->one();
                $this->data['next'] = Scale::findBySql("SELECT table_name,display_sequence from scale where  (".$condition1.") and $condition2 and display_sequence > $displaySequence and table_name is not null limit 1")->one();
            }
        }  */
    }
}
?>
