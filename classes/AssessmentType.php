<?php
namespace app\classes;

use Yii;
use app\models\Relationship;

class AssessmentType{
    

    public function getRelationType($assessType,$relationshipId){
        $model = new RelationShip();
        $model = Relationship::findBySql("SELECT r.type FROM assessment as a 
                        INNER JOIN relationship as r ON r.type_id = a.relationship_id 
                        where a.assessment_type = '$assessType' and a.relationship_id = $relationshipId")->one();
        return $model;
    }

    public function getType($assessType,$relationshipId){

        $model = $this->getRelationType($assessType,$relationshipId);
        $type = ($model['type'])?$model['type']:null;
        if(!$type)
        {
            return null;
        }
        $str = null;
        switch($type)
        {
            case 'mother'   :   $str = "$assessType = 'm' || $assessType='m,ac'";
                                break;
            case 'member'   :   $str = "$assessType='m,ac'";
                                break;
            case 'user'     :   $str = "$assessType = 'i'";
                                break;
        }
        return $str;
    }   
}
?>
