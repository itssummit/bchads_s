<?php 
namespace app\classes;

use Yii;
use app\models\Member;
use app\models\Mother;

class SessionHandler
{
    public $motherId;
	public function clearSessions()
	{
		Yii::$app->session->set('patientId',0);
        Yii::$app->session->set('motherId',0);
        Yii::$app->session->set('memberId',0);
		Yii::$app->session->set('assessmentId',0);
		Yii::$app->session->set('assessmentType',null);
        Yii::$app->session->set('respondentType',null);
        Yii::$app->session->set('respondentId',null);
        Yii::$app->session->set('displaySequence',null);
        Yii::$app->session->set('scaleType',null);
	}
    public function setmotherIdInSession($motherId)
	{
		$this->motherId = intval($patientId);
		Yii::$app->session->set('motherId',$motherId);
		
	}
    public function getMotherId()
	{
		$motherId = Yii::$app->session->get('motherId');
	    return $motherId;
		
	}
}
?>