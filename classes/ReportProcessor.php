<?php 
use app\models\infant_health;

namespace app\classes;

use Yii;

class ReportProcessor{
	
	public function getReport($assessType){
		
		$motherColumns = $this->motherTable();
		$pColImp = implode($motherColumns,",");
		
		$pObj = Yii::$app->db->createCommand("Select $pColImp from mother")->queryAll();
		if ($assessType == 'm6'){
			
		}
		for($i = 0;$i < count($pObj);$i++)
		{
			$motherId = $pObj[$i]['id'];
			$pvColumns = $this->motherVisitTable();
			$pvColImp = implode($pvColumns,",");
			$obj = Yii::$app->db->createCommand("Select $pvColImp from `assessment` where mother_id = $motherId and assessment_type = '$assessType'")->queryAll();
			for($j = 0;$j < count($obj);$j++)
			{

				$assessmentId = $obj[$j]['id'];
				$assessmentType = $obj[$j]['assessment_type'];
				switch($assessmentType){
					
					
					case 'm6':
					$obj[$j]['m_Date_assessment'] = date('d-M-Y',strtotime($obj[$j]['m_Date_assessment']));
					$obj[$j]['m_Date_entry'] = date('d-M-Y',strtotime($obj[$j]['m_Date_entry']));
					$obj[$j] = array_merge($obj[$j],$this->psychosocialriskfactorRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->wooleydepressionRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->epdsRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->phqRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->suicidequestionsRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->sassRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->staiRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->psychosocialassessmentRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->lifeeventsRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->ptsdchecklistRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->multidimentionalRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->icmrRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->ctsRetrieval($assessmentId,$motherId));
					//$obj[$j] = array_merge($obj[$j],$this->picsmRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->picsacRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->brestfeedingRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->babylikesanddislikesmotherRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->babylikesanddislikesacRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->infanthealthRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->maternalRetrieval($assessmentId,$motherId));
					//$obj[$j] = array_merge($obj[$j],$this->sharedcaregivingmotherRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->sharedcaregivingacRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->infantanthrosixthRetrieval($assessmentId,$motherId));
					unset($obj[$j]['id']);
					unset($obj[$j]['mother_id']);
					unset($obj[$j]['assessment_type']);
					
					foreach($obj[$j] as $key => $value)
					{
					$code = "T".(6)."_".$key;
					$code = str_replace('`','',$code);
					$pObj[$i][$code] = $value;
					}
					break;
					
					
					case 'm12':
					$obj[$j]['m_Date_assessment'] = date('d-M-Y',strtotime($obj[$j]['m_Date_assessment']));
					$obj[$j]['m_Date_entry'] = date('d-M-Y',strtotime($obj[$j]['m_Date_entry']));
					$obj[$j] = array_merge($obj[$j],$this->psychosocialriskfactorRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->wooleydepressionRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->epdsRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->phqRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->suicidequestionsRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->sassRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->staiRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->psychosocialassessmentRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->lifeeventsRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->ptsdchecklistRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->multidimentionalRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->icmrRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->ctsRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->babylikesanddislikesmotherRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->babylikesanddislikesacRetrieval($assessmentId,$motherId));
					//$obj[$j] = array_merge($obj[$j],$this->infanthealthRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->maternalRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->bitseaRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->bitseaacRetrieval($assessmentId,$motherId));
					//$obj[$j] = array_merge($obj[$j],$this->sharedcaregivingmotherRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->sharedcaregivingacRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->infantanthrotwelvethRetrieval($assessmentId,$motherId));
					unset($obj[$j]['id']);
					unset($obj[$j]['mother_id']);
					unset($obj[$j]['assessment_type']);
					
					foreach($obj[$j] as $key => $value)
					{
					$code = "T".(8)."_".$key;
					$code = str_replace('`','',$code);
					$pObj[$i][$code] = $value;
					}
					break;
					case 'm24':
					$obj[$j]['m_Date_assessment'] = date('d-M-Y',strtotime($obj[$j]['m_Date_assessment']));
					$obj[$j]['m_Date_entry'] = date('d-M-Y',strtotime($obj[$j]['m_Date_entry']));
					$obj[$j] = array_merge($obj[$j],$this->psychosocialriskfactorRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->wooleydepressionRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->epdsRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->phqRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->suicidequestionsRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->sassRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->staiRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->psychosocialassessmentRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->lifeeventsRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->ptsdchecklistRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->multidimentionalRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->icmrRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->ctsRetrieval($assessmentId,$motherId));
				
					$obj[$j] =array_merge($obj[$j],$this->childecbqmotherRetrieval($assessmentId,$motherId)); 
					$obj[$j] = array_merge($obj[$j],$this->childecbqacRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->infanthealthRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->maternalRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->bitseaRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->bitseaacRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->infantanthrotwentyfourthRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->cbclmotherRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->cbclacRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->sharedcaregivingmotherRetrieval($assessmentId,$motherId));
					//$obj[$j] = array_merge($obj[$j],$this->sharedcaregivingacRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->pbfRetrieval($assessmentId,$motherId));
					$obj[$j] = array_merge($obj[$j],$this->pbsRetrieval($assessmentId,$motherId));
					unset($obj[$j]['id']);
					unset($obj[$j]['mother_id']);
					unset($obj[$j]['assessment_type']);
					
					foreach($obj[$j] as $key => $value)
					{
					$code = "T".(9)."_".$key;
					$code = str_replace('`','',$code);
					$pObj[$i][$code] = $value;
					}
					break;
				}
				
			}
		}
		
		//Sorting to get Highest Count among objects
		$map = array_map("count", $pObj);
		$index = array_search(max($map),$map);
		$get = $pObj[$index];
		unset($pObj[$index]);
		array_unshift($pObj,$get);
		return $pObj;
	}

	private function motherTable(){
		return ["id","mother_code as participant_ID","gender_baby as Gender_Child"];
	}
	
	private function motherVisitTable(){
		return ["id","mother_id","assessment_date as m_Date_assessment","created_dtm as m_Date_entry","assessment_by as m_Interviewer","assessment_type","relation_with_child as type_of_ACG","assessment_date_ac as assessment_of_caregiver","place_of_assessment_ac as place_of_assessment_for_caregiver","assessment_by_ac as assessment_by_for_caregiver","is_there_acg","specify_other" ];
	}
	private function wooleydepressionTable(){
		return ["depressed_pastmonth as '1'" => null,"little_interest_pleasure_pastmonth as '2'" => null ,"women_yes_needhelp as '3'" => null];
	}
	
	private function epdsTable(){
		return ["feeling_past_7days_laugh as '01'" => null,"feeling_past_7days_enjoyment as '02'" => null ,"feeling_past_7days_blamingmyself as '03'" => null,"feeling_past_7days_anxious_worried as '04'"=>null,"feeling_past_7days_scared_panicky as '05'"=>null,"feeling_past_7days_things_getting_topofme as '06'"=>null,"feeling_past_7days_unhappy_difficulty_sleeping as '07'"=>null,"feeling_past_7days_sad_miserable as '08'"=>null,"feeling_past_7days_unhappy_crying as '09'"=>null,"feeling_past_7days_harming_myself as '10'"=>null];
	}
	
	private function phqTable(){
		return ["little_interest_pleasure as 1A" => null,"down_depressed_hopeless as 1B" => null ,"falling_sleep as 1C" => null,"tired as 1D"=>null,"appetite_overeating as 1E"=>null,"bad_yourself as 1F"=>null,"trouble_concentrating as 1G"=>null,"moving_speaking_slowly as 1H"=>null,"thought_hurting_yourself as 1I"=>null,"anxiety_attack as 2A"=>null,"happen_before as 2B"=>null,"attack_suddenly as 2C"=>null,"attack_bother as 2D"=>null,"short_breath as 3A"=>null,"heart_race as 3B"=>null,"chest_pain as 3C"=>null,"sweat as 3D"=>null,"choking as 3E"=>null,"hot_flashes_chills as 3F"=>null,"nausea as 3G"=>null,"dizzy_unsteady as 3H"=>null,"tingling_numbness as 3I"=>null,"tremble as 3J"=>null,"afraid_dieing as 3K"=>null,"nervous as 4A"=>null,"restless as 4B"=>null,"tired_easily as 4C"=>null,"muscle_tension as 4D"=>null,"trouble_falling_staying_asleep as 4E"=>null,"troble_concentrating as 4F"=>null,"easily_annoyed as 4G"=>null,"difficulty_problem as '5'"=>null];
	}
	private function suicidequestionsTable(){
		return ["ideas as '1'"=>null,"plan_attempted as '2'"=>null,"current_pregnancy_thought_ending_life as '3'"=>null,"current_pregnancy_plan as '4'"=>null,"current_pregnancy_attempt as '5'"=>null,"times_attempted as '6'"=>null,"reasons_attempt as '7'"=>null,"method_attempt as '8'"=>null,"specify_others as '8A'"=>null];
	}
	
	private function sassTable(){
		return ["head_ache as A1"=>null,"back_pain as A2"=>null,"hand_leg_pain as A3"=>null,"stomach_pain as A4"=>null,"body_pain as A5"=>null,"jadatva as B1"=>null,"cold_hot as B2"=>null,"high_heart_beat as B3"=>null,"stomach_prob as B4"=>null,"burn as B5"=>null,"physical_weakness as C1"=>null,"psychological_weakness as C2"=>null,"unconscious as C3"=>null,"shivering as C4"=>null,"tired as C5"=>null,"no_sleep as D1"=>null,"no_hungry as D2"=>null,"no_interest as D3"=>null,"constipation as D4"=>null,"loose_motions as D5"=>null,"leg_shake as E1"=>null,"legs_pain as E2"=>null,"disease_symptoms as E3"=>null,"walks_relief as E4"=>null,"night_symptoms_increases as E5"=>null,"sleep_prob as E6"=>null,"ringing_sounds as E7"=>null,"vometings as F1"=>null,"urine as F2"=>null,"yoni as F3"=>null,"pelvic as F4"=>null,"biliseragu as F5"=>null,"any_other as F6"=>null];
	}
	
	private function staiTable(){
		return ["feel_peace as '01'"=>null,"feel_safe as '02'"=>null,"feel_pressure as '03'"=>null,"feel_tired as '04'"=>null,"feel_nemmadi as '05'"=>null,"feel_sad as '06'"=>null,"feel_future_disaster as '07'"=>null,"feel_enough as '08'"=>null,"feel_fear as '09'"=>null,"feel_free as '10'"=>null,"feel_confident as '11'"=>null,"feel_weak as '12'"=>null,"feel_feared as '13'"=>null,"cant_take_decisions as '14'"=>null,"feel_samadhana as '15'"=>null,"feel_santhushti as '16'"=>null,"feel_sadness as '17'"=>null,"feel_galibili as '18'"=>null,"feel_drudamanasssu as '19'"=>null,"feel_happy  as '20'"=>null,"happy as '21'"=>null,"weak as '22'"=>null,"thrupthi as '23'"=>null,"happy_like_others as '24'"=>null,"fail as '25'"=>null,"free as '26'"=>null,"safe as '27'"=>null,"more_prob as '28'"=>null,"thinks_lot as '29'"=>null,"happiest as '30'"=>null,"stopping_thoughts as '31'"=>null,"not_confident as '32'"=>null,"secure as '33'"=>null,"takes_decisions as '34'"=>null,"asamarpaka as '35'"=>null,"santhushta as '36'"=>null,"notimp_thoughts as '37'"=>null,"nirashe as '38'"=>null,"dhrudmanassu as '39'"=>null,"udvega as '40'"=>null];
	}
	
	private function psychosocialassessmentTable(){
		return ["financial_worries as PAT_01"=>null,"money_worries as PAT_02"=>null,"family_problems as PAT_03"=>null,"move_recently_future as PAT_04"=>null,"loss_loved as PAT_05"=>null,"current_pregnancy as PAT_06"=>null,"current_abuse as PAT_07"=>null,"problem_alcohol as PAT_08"=>null,"problem_work as PAT_09"=>null,"problem_friends as PAT_10"=>null,"overloaded as PAT_11"=>null];
	}
	
	private function lifeeventsTable(){
		return ["death_spouse as A1"=>null,"divorce as A2"=>null,"marital_separation as A3"=>null,"death_family_member as A4"=>null,"personal_injury as A5"=>null,"marriage as A6"=>null,"marital_reconcilation as A7"=>null,"health_family_member as A8"=>null,"change_financial_status as A9"=>null,"change_argument_spouse as A10"=>null,"death_close_friend as A11"=>null,"large_loan as A12"=>null,"trouble_in_laws as A13"=>null,"spouse_work as A14"=>null,"change_living_conditions as A15"=>null,"change_working_hours as A16"=>null,"change_residence as A17"=>null,"major_health_family_member as B1"=>null,"deterioration_finance as B2"=>null,"conflict_parents as B3"=>null,"argument_spouse as B4"=>null,"deterioration_living_conditions as B5"=>null,"abortion as C1"=>null,"miscarriage as C2"=>null,"diabetes as C3"=>null,"pre_eclamptic as C4"=>null,"eclampsia as C5"=>null,"medication_exposure as C6"=>null,"neonatal_death as C7"=>null];
	}
	
	private function ptsdchecklistTable(){
		return ["intrusive_recollect as '1'"=>null,"flashbacks as '2'"=>null,"upset_remainders as '3'"=>null,"distressing_dreams as '4'"=>null,"physical_remainders as '5'"=>null,"avoid_thoughts as '6'"=>null,"avoid_remainders as '7'"=>null,"psychogenic_amnesia as '8'"=>null,"anhedonia as '9'"=>null,"estrangement_others as '10'"=>null,"psychic_numbing as '11'"=>null,"foreshortened_future as '12'"=>null,"sleep_difficulty as '13'"=>null,"irritability as '14'"=>null,"concentrattion_impaired as '15'"=>null,"hyper_vigilant as '16'"=>null,"exaggerated_startle as '17'"=>null];
	}
	
	private function multidimentionalTable(){
		return ["special_person_need as S1"=>null,"special_person_share as S2"=>null,"family_help as S3"=>null,"emotional_help_family as S4"=>null,"special_person_comfort as S5"=>null,"friend_help as S6"=>null,"friends_things_go_wrong as S7"=>null,"problems_family_talk as S8"=>null,"friends_share as S9"=>null,"special_person_feelings as S10"=>null,"family_help_decision as S11"=>null,"talk_problems_friends as S12"=>null];
	}
	
	
	
	private function psychosocialriskfactorTable(){
		return ["stress_issues as '01'"=>null,"financial as '02'"=>null,"work as '03'"=>null,"others as '04'"=>null,"others_desc as '05'"=>null,
		"lack_care as '06'"=>null,"spouse as '07'"=>null,"in_laws as '08'"=>null,"parents as '09'"=>null,
		"violence_experience as '10'"=>null,"psychological as '11'"=>null,"physical as '12'"=>null,"sexual as '13'"=>null,
		"anxiety_issues as '14'"=>null,
		"mood_issues as '15'"=>null,
		"suicidal_ideation as '16'"=>null,
		"poor_pregnancy as '17'"=>null,
		"traumatic_events as '18'"=>null,
		"substance_use_spouse as '19'"=>null,
		"health_problem_of_familymember as '20'"=>null,"relation as '21'"=>null,
		"complications_related_to_labour_delivery_childbirth as '22'"=>null,"complications_related_to_labour_delivery_childbirth1 as '23'"=>null,"complications_related_to_labour_delivery_childbirth2 as '24'"=>null,"obstetric_trauma as '25'"=>null,
		"poor_obg_outcome as '26'"=>null,"poor_obg_outcome1 as '27'"=>null,"poor_obg_outcome2 as '28'"=>null,
		"gender_of_the_infant as '29'"=>null,"delivery_and_postpartum as '30'"=>null];
	}
	
	private function icmrTable(){
		return ["bad_words as INSLT_YN"=>null,"bad_words1 as AINSLT_A"=>null,"bad_words2 as AINSLT_B"=>null,"bad_words3 as AINSLT_C"=>null,"bad_words9 as AINSLT_C1"=>null,"bad_words4 as AINSLT_D"=>null,"bad_words5 as AINSLT_E"=>null,"bad_words6 as INSLT_F1"=>null,"bad_words10 as INSLT_F2"=>null,"bad_words11 as INSLT_F3"=>null,"bad_words7 as AINSLT_G"=>null,"bad_words8 as AINSLT_H"=>null,
		"threatening as BTHRT_YN"=>null,"threatening1 as BTHRT_A"=>null,"threatening2 as BTHRT_B"=>null,"threatening3 as BTHRT_C"=>null,"threatening9 as BTHRT_C1"=>null,"threatening4 as BTHRT_D"=>null,"threatening5 as BTHRT_E"=>null,"threatening6 as BTHRT_F1"=>null,"threatening10 as BTHRT_F2"=>null,"threatening11 as BTHRT_F3"=>null,"threatening7 as BTHRT_G"=>null,"threatening8 as BTHRT_H"=>null,
		"threatening_sending_home as CTHRT_YN"=>null,"threatening_sending_home1 as CTHRT_A"=>null,"threatening_sending_home2 as CTHRT_B"=>null,"threatening_sending_home3 as CTHRT_C"=>null,"threatening_sending_home9 as CTHRT_C1"=>null,"threatening_sending_home4 as CTHRT_D"=>null,"threatening_sending_home5 as CTHRT_E"=>null,"threatening_sending_home6 as CTHRT_F1"=>null,"threatening_sending_home10 as CTHRT_F2"=>null,"threatening_sending_home11 as CTHRT_F3"=>null,"threatening_sending_home7 as CTHRT_G"=>null,"threatening_sending_home8 as CTHRT_H"=>null,
		"send_home as DSNT_YN"=>null,"send_home1 as DSNT_A"=>null,"send_home2 as DSNT_B"=>null,"send_home3 as DSNT_C"=>null,"send_home9 as DSNT_C1"=>null,"send_home4 as DSNT_D"=>null,"send_home5 as DSNT_E"=>null,"send_home6 as DSNT_F1"=>null,"send_home10 as DSNT_F2"=>null,"send_home11 as DSNT_F3"=>null,"send_home7 as DSNT_G"=>null,"send_home8 as DSNT_H"=>null,
		"finance_prob as EFIN_YN"=>null,"finance_prob1 as EFIN_A"=>null,"finance_prob2 as EFIN_B"=>null,"finance_prob3 as EFIN_C"=>null,"finance_prob9 as EFIN_C1"=>null,"finance_prob4 as EFIN_D"=>null,"finance_prob5 as EFIN_E"=>null,"finance_prob6 as EFIN_F1"=>null,"finance_prob10 as EFIN_F2"=>null,"finance_prob11 as EFIN_F3"=>null,"finance_prob7 as EFIN_G"=>null,"finance_prob8 as EFIN_H"=>null,
		"fear_look as FRGHT_YN"=>null,"fear_look1 as FRGHT_A"=>null,"fear_look2 as FRGHT_B"=>null,"fear_look3 as FRGHT_C"=>null,"fear_look9 as FRGHT_C1"=>null,"fear_look4 as FRGHT_D"=>null,"fear_look5 as FRGHT_E"=>null,"fear_look6 as FRGHT_F1"=>null,"fear_look10 as FRGHT_F2"=>null,"fear_look11 as FRGHT_F3"=>null,"fear_look7 as FRGHT_G"=>null,"fear_look8 as FRGHT_H"=>null,
		"avidheya as GPROV_YN"=>null,"avidheya1 as GPROV_A"=>null,"avidheya2 as GPROV_B"=>null,"avidheya3 as GPROV_C"=>null,"avidheya9 as GPROV_C1"=>null,"avidheya4 as GPROV_D"=>null,"avidheya5 as GPROV_E"=>null,"avidheya6 as GPROV_F1"=>null,"avidheya10 as GPROV_F2"=>null,"avidheya11 as GPROV_F3"=>null,"avidheya7 as GPROV_G"=>null,"avidheya8 as GPROV_H"=>null,
		"udasinathe as HSHWD_YN"=>null,"udasinathe1 as HSHWD_A"=>null,"udasinathe2 as HSHWD_B"=>null,"udasinathe3 as HSHWD_C"=>null,"udasinathe9 as HSHWD_C1"=>null,"udasinathe4 as HSHWD_D"=>null,"udasinathe5 as HSHWD_E"=>null,"udasinathe6 as HSHWD_F1"=>null,"udasinathe10 as HSHWD_F2"=>null,"udasinathe11 as HSHWD_F3"=>null,"udasinathe7 as HSHWD_G"=>null,"udasinathe8 as HSHWD_H"=>null,
		"social_rights as IRIGT_YN"=>null,"social_rights1 as IRIGT_A"=>null,"social_rights2 as IRIGT_B"=>null,"social_rights3 as IRIGT_C"=>null,"social_rights9 as IRIGT_C1"=>null,"social_rights4 as IRIGT_D"=>null,"social_rights5 as IRIGT_E"=>null,"social_rights6 as IRIGT_F1"=>null,"social_rights10 as IRIGT_F2"=>null,"social_rights11 as IRIGT_F3"=>null,"social_rights7 as IRIGT_G"=>null,"social_rights8 as IRIGT_H"=>null,
		"nirlakshya as JNEG_YN"=>null,"nirlakshya1 as JNEG_A"=>null,"nirlakshya2 as JNEG_B"=>null,"nirlakshya3 as JNEG_C"=>null,"nirlakshya9 as JNEG_C1"=>null,"nirlakshya4 as JNEG_D"=>null,"nirlakshya5 as JNEG_E"=>null,"nirlakshya6 as JNEG_F1"=>null,"nirlakshya10 as JNEG_F2"=>null,"nirlakshya11 as JNEG_F3"=>null,"nirlakshya7 as JNEG_G"=>null,"nirlakshya8 as JNEG_H"=>null,
		"personal_needs as KPNED_YN"=>null,"personal_needs1 as KPNED_A"=>null,"personal_needs2 as KPNED_B"=>null,"personal_needs3 as KPNED_C"=>null,"personal_needs9 as KPNED_C1"=>null,"personal_needs4 as KPNED_D"=>null,"personal_needs5 as KPNED_E"=>null,"personal_needs6 as KPNED_F1"=>null,"personal_needs10 as KPNED_F2"=>null,"personal_needs11 as KPNED_F3"=>null,"personal_needs7 as KPNED_G"=>null,"personal_needs8 as KPNED_H"=>null,
		"decision_taking as LDECN_YN"=>null,"decision_taking1 as LDECN_A"=>null,"decision_taking2 as LDECN_B"=>null,"decision_taking3 as LDECN_C"=>null,"decision_taking9 as LDECN_C1"=>null,"decision_taking4 as LDECN_D"=>null,"decision_taking5 as LDECN_E"=>null,"decision_taking6 as LDECN_F1"=>null,"decision_taking10 as LDECN_F2"=>null,"decision_taking11 as LDECN_F3"=>null,"decision_taking7 as LDECN_G"=>null,"decision_taking8 as LDECN_H"=>null,
		"`restrict` as MREST_YN"=>null,"restrict1 as MREST_A"=>null,"restrict2 as MREST_B"=>null,"restrict3 as MREST_C"=>null,"restrict9 as MREST_C1"=>null,"restrict4 as MREST_D"=>null,"restrict5 as MREST_E"=>null,"restrict6 as MREST_F1"=>null,"restrict10 as MREST_F2"=>null,"restrict11 as MREST_F3"=>null,"restrict7 as MREST_G"=>null,"restrict8 as MREST_H"=>null,
		"harm as NHIT_YN"=>null,"harm1 as NHIT_A"=>null,"harm2 as NHIT_B"=>null,"harm3 as NHIT_C"=>null,"harm10 as NHIT_C1"=>null,"harm4 as NHIT_D"=>null,"harm5 as NHIT_E"=>null,"harm6 as NHIT_F1"=>null,"harm11 as NHIT_F2"=>null,"harm12 as NHIT_F3"=>null,"harm7 as NHIT_G"=>null,"harm8 as NHIT_H"=>null,"harm9 as NHIT_I"=>null,
		"harm_fire as OSCLD_YN"=>null,"harm_fire1 as OSCLD_A"=>null,"harm_fire2 as OSCLD_B"=>null,"harm_fire3 as OSCLD_C"=>null,"harm_fire10 as OSCLD_C1"=>null,"harm_fire4 as OSCLD_D"=>null,"harm_fire5 as OSCLD_E"=>null,"harm_fire6 as OSCLD_F1"=>null,"harm_fire11 as OSCLD_F2"=>null,"harm_fire12 as OSCLD_F3"=>null,"harm_fire7 as OSCLD_G"=>null,"harm_fire8 as OSCLD_H"=>null,"harm_fire9 as OSCLD_I"=>null,
		"forced as PSEX_YN"=>null,"forced1 as PSEX_A"=>null,"forced2 as PSEX_B"=>null,"forced3 as PSEX_C"=>null,"forced9 as PSEX_C1"=>null,"forced4 as PSEX_D"=>null,"forced5 as PSEX_E"=>null,"forced6 as PSEX_F1"=>null,"forced10 as PSEX_F2"=>null,"forced11 as PSEX_F3"=>null,"forced7 as PSEX_G"=>null,"forced8 as PSEX_H"=>null,
		"avoid as QDNSX_YN"=>null,"avoid1 as QDNSX_A"=>null,"avoid2 as QDNSX_B"=>null,"avoid3 as QDNSX_C"=>null,"avoid9 as QDNSX_C1"=>null,"avoid4 as QDNSX_D"=>null,"avoid5 as QDNSX_E"=>null,"avoid6 as QDNSX_F1"=>null,"avoid10 as QDNSX_F2"=>null,"avoid11 as QDNSX_F3"=>null,"avoid7 as QDNSX_G"=>null,"avoid8 as QDNSX_H"=>null,
		"wound as RINJ_YN"=>null,"wound1 as RINJ_A"=>null,"wound2 as RINJ_B"=>null,"wound3 as RINJ_C"=>null,"wound9 as RINJ_C1"=>null,"wound4 as RINJ_D"=>null,"wound5 as RINJ_E"=>null,"wound6 as RINJ_F1"=>null,"wound10 as RINJ_F2"=>null,"wound11 as RINJ_F3"=>null,"wound7 as RINJ_G"=>null,"wound8 as RINJ_H"=>null];
	}
	
	
	private function ctsTable(){
		return ["supporting_your_partner_at_difficult_times as Self_01"=>null,"keeping_your_opinions_with_yourself as Self_02"=>null,"damaged_things_because_of_angry_on_your_wife as Self_03"=>null,"after_arguing_with_your_partner_did_u_shown_more_love as Self_04"=>null,"knowingly_hiding_ur_partners_loved_things as Self_05"=>null,"did_u_felt_sad_when_things_are_undone as Self_06"=>null,"started_meeting_to_talk_in_all_angles as Self_07"=>null,"intentionally_destroying_partners_personal_things as Self_08"=>null,"disrespecting_ur_partner_infront_of_others as Self_09"=>null,"listened_to_ur_partners_words_with_concentration as Self_10"=>null,
	"locked_ur_partner_from_outside_home as Self_11"=>null,"said_to_ur_partner_like_u_r_unable_to_study_or_work as Self_12"=>null,
	"specified_ur_partners_position as Self_13"=>null,"stopping_ur_partner_from_talking_with_relatives as Self_14"=>null,"adjusting_with_different_opinions as Self_15"=>null,"frequently_asking_did_u_understood_or_not as Self_16"=>null,"restritcting_ur_partner_by_using_of_mobiles_or_vehicle as Self_17"=>null,"threatning_ur_partner_for_disconnection_of_relationship as Self_18"=>null,"by_doing_physical_work_did_u_reliefed as Self_19"=>null,"tried_ur_relatives_against_ur_partner as Self_20"=>null,"while_partner_solving_their_problem_did_u_suggested_ideas as Self_21"=>null,"ordered_or_promised_ur_partner as Self_22"=>null,"tried_to_solve_problems_together as Self_23"=>null,"accepted_ur_faults as Self_24"=>null,"feared_ur_partner as Self_25"=>null,"treated_ur_partner_as_a_fool as Self_26"=>null,"given_useful_ideas_to_ur_partner as Self_27"=>null,"done_preplanned_work_with_angry as Self_28"=>null,"tried_to_join_others_to_solve_ur_problems as Self_29"=>null,
	"disrespected_ur_partner as Self_30"=>null,"self_realised_what_u_said_to_ur_partner as Self_31"=>null,"feared_ur_partner_to_throw_things_while_angry as Self_32"=>null,"said_u_r_not_beautiful_to_partner as Self_33"=>null,"agreed_urself_for_adjustment as Self_34"=>null,"after_smoking_did_u_scolded_ur_partner as Self_35"=>null,"if_ur_partner_disagreed_u_then_did_u_thrown_any_objects as Self_36"=>null,"asked_sorry_after_arguing as Self_37"=>null,"forcibly_closing_the_argument as Self_38"=>null,"agreed_to_ur_partner_wanted_things as Self_39"=>null,
	
	
	"supporting_your_partner_at_difficult_times1 as Partner_01"=>null,"keeping_your_opinions_with_yourself1 as Partner_02"=>null,"damaged_things_because_of_angry_on_your_wife1 as Partner_03"=>null,"after_arguing_with_your_partner_did_u_shown_more_love1 as Partner_04"=>null,"knowingly_hiding_ur_partners_loved_things1 as Partner_05"=>null,"did_u_felt_sad_when_things_are_undone1 as Partner_06"=>null,"started_meeting_to_talk_in_all_angles1 as Partner_07"=>null,"intentionally_destroying_partners_personal_things1 as Partner_08"=>null,"disrespecting_ur_partner_infront_of_others1 as Partner_09"=>null,
	"listened_to_ur_partners_words_with_concentration1 as Partner_10"=>null,"locked_ur_partner_from_outside_home1 as Partner_11"=>null,"said_to_ur_partner_like_u_r_unable_to_study_or_work1 as Partner_12"=>null,"specified_ur_partners_position1 as Partner_13"=>null,"stopping_ur_partner_from_talking_with_relatives1 as Partner_14"=>null,"adjusting_with_different_opinions1 as Partner_15"=>null,"frequently_asking_did_u_understood_or_not1 as Partner_16"=>null,"restritcting_ur_partner_by_using_of_mobiles_or_vehicle1 as Partner_17"=>null,"threatning_ur_partner_for_disconnection_of_relationship1 as Partner_18"=>null,"by_doing_physical_work_did_u_reliefed1 as Partner_19"=>null,"tried_ur_relatives_against_ur_partner1 as Partner_20"=>null,"while_partner_solving_their_problem_did_u_suggested_ideas1 as Partner_21"=>null,"ordered_or_promised_ur_partner1 as Partner_22"=>null,"tried_to_solve_problems_together1 as Partner_23"=>null,"accepted_ur_faults1 as Partner_24"=>null,"feared_ur_partner1 as Partner_25"=>null,"treated_ur_partner_as_a_fool1 as Partner_26"=>null,"given_useful_ideas_to_ur_partner1 as Partner_27"=>null,"done_preplanned_work_with_angry1 as Partner_28"=>null,"tried_to_join_others_to_solve_ur_problems1 as Partner_29"=>null,"disrespected_ur_partner1 as Partner_30"=>null,"self_realised_what_u_said_to_ur_partner1 as Partner_31"=>null,"feared_ur_partner_to_throw_things_while_angry1 as Partner_32"=>null,"said_u_r_not_beautiful_to_partner1 as Partner_33"=>null,"agreed_urself_for_adjustment1 as Partner_34"=>null,"after_smoking_did_u_scolded_ur_partner1 as Partner_35"=>null,"if_ur_partner_disagreed_u_then_did_u_thrown_any_objects1 as Partner_36"=>null,"asked_sorry_after_arguing1 as Partner_37"=>null,"forcibly_closing_the_argument1 as Partner_38"=>null,"agreed_to_ur_partner_wanted_things1 as Partner_39"=>null,
	
	"rotated_ur_partner_hand as Self_abuse_01_a"=>null,"rotated_ur_partner_hand_desc1 as Self_abuse_01_b"=>null,"rotated_ur_partner_hand_desc2 as Self_abuse_01_c"=>null,"rotated_ur_partner_hand_desc3 as Self_abuse_01_d"=>null,"grabed_and_pushed_ur_husband as Self_abuse_02_a"=>null,"grabed_and_pushed_ur_husband_desc1 as Self_abuse_02_b"=>null,"grabed_and_pushed_ur_husband_desc2 as Self_abuse_02_c"=>null,"grabed_and_pushed_ur_husband_desc3 as Self_abuse_02_d"=>null,
	
	"slapped_ur_husband as Self_abuse_03_a"=>null,"slapped_ur_husband_desc1 as Self_abuse_03_b"=>null,"slapped_ur_husband_desc2 as Self_abuse_03_c"=>null,"slapped_ur_husband_desc3 as Self_abuse_03_d"=>null,
	
	"forced_ur_partner_for_sex as Self_abuse_04_a"=>null,"forced_ur_partner_for_sex_desc1 as Self_abuse_04_b"=>null,"forced_ur_partner_for_sex_desc2 as Self_abuse_04_c"=>null,"forced_ur_partner_for_sex_desc3 as Self_abuse_04_d"=>null,
	
	"shaked_ur_partner_by_holding_tight as Self_abuse_05_a"=>null,"shaked_ur_partner_by_holding_tight_desc1 as Self_abuse_05_b"=>null,"shaked_ur_partner_by_holding_tight_desc2 as Self_abuse_05_c"=>null,"shaked_ur_partner_by_holding_tight_desc3 as Self_abuse_05_d"=>null
	
	,"tried_to_throw_ur_husband as Self_abuse_06_a"=>null,"tried_to_throw_ur_husband_desc1 as Self_abuse_06_b"=>null,"tried_to_throw_ur_husband_desc2 as Self_abuse_06_c"=>null,"tried_to_throw_ur_husband_desc3 as Self_abuse_06_d"=>null,
	
	"tried_to_throw_things_on_ur_partner as Self_abuse_07_a"=>null,"tried_to_throw_things_on_ur_partner_desc1 as Self_abuse_07_b"=>null,"tried_to_throw_things_on_ur_partner_desc2 as Self_abuse_07_c"=>null,"tried_to_throw_things_on_ur_partner_desc3 as Self_abuse_07_d"=>null,
	
	"tried_to_hold_ur_partners_neck as Self_abuse_08_a"=>null,"tried_to_hold_ur_partners_neck_desc1 as Self_abuse_08_b"=>null,"tried_to_hold_ur_partners_neck_desc2 as Self_abuse_08_c"=>null,"tried_to_hold_ur_partners_neck_desc3 as Self_abuse_08_d"=>null,
	
	"tried_to_hit_kick_bite as Self_abuse_09_a"=>null,"tried_to_hit_kick_bite_desc1 as Self_abuse_09_b"=>null,"tried_to_hit_kick_bite_desc2 as Self_abuse_09_c"=>null,"tried_to_hit_kick_bite_desc3 as Self_abuse_09_d"=>null,
	
	"tried_to_hit_or_hitted as Self_abuse_10_a"=>null,"tried_to_hit_or_hitted_desc1 as Self_abuse_10_b"=>null,"tried_to_hit_or_hitted_desc2 as Self_abuse_10_c"=>null,"tried_to_hit_or_hitted_desc3 as Self_abuse_10_d"=>null,
	
	"hitted_ur_partner as Self_abuse_11_a"=>null,"hitted_ur_partner_desc1 as Self_abuse_11_b"=>null,"hitted_ur_partner_desc2 as Self_abuse_11_c"=>null,"hitted_ur_partner_desc3 as Self_abuse_11_d"=>null,
	
	"feared_ur_partner_by_gun_knife as Self_abuse_12_a"=>null,"feared_ur_partner_by_gun_knife_desc1 as Self_abuse_12_b"=>null,"feared_ur_partner_by_gun_knife_desc2 as Self_abuse_12_c"=>null,"feared_ur_partner_by_gun_knife_desc3 as Self_abuse_12_d"=>null,
	
	"used_gun_knife as Self_abuse_13_a"=>null,"used_gun_knife_desc1 as Self_abuse_13_b"=>null,"used_gun_knife_desc2 as Self_abuse_13_c"=>null,"used_gun_knife_desc3 as Self_abuse_13_d"=>null,
	
	
	
	"rotated_ur_partner_hand1 as Partner_abuse_01_a"=>null,"rotated_ur_partner_hand1_desc1 as Partner_abuse_01_b"=>null,"rotated_ur_partner_hand1_desc2 as Partner_abuse_01_c"=>null,"rotated_ur_partner_hand1_desc3 as Partner_abuse_01_d"=>null,
	
	"grabed_and_pushed_ur_husband1 as Partner_abuse_02_a"=>null,"grabed_and_pushed_ur_husband1_desc1 as Partner_abuse_02_b"=>null,"grabed_and_pushed_ur_husband1_desc2 as Partner_abuse_02_c"=>null,"grabed_and_pushed_ur_husband1_desc3 as Partner_abuse_02_d"=>null,
	
	"slapped_ur_husband1 as Partner_abuse_03_a"=>null,"slapped_ur_husband1_desc1 as Partner_abuse_03_b"=>null,"slapped_ur_husband1_desc2 as Partner_abuse_03_c"=>null,"slapped_ur_husband1_desc3 as Partner_abuse_03_d"=>null,
	
	"forced_ur_partner_for_sex1 as Partner_abuse_04_a"=>null,"forced_ur_partner_for_sex1_desc1 as Partner_abuse_04_b"=>null,"forced_ur_partner_for_sex1_desc2 as Partner_abuse_04_c"=>null,"forced_ur_partner_for_sex1_desc3 as Partner_abuse_04_d"=>null,
	
	"shaked_ur_partner_by_holding_tight1 as Partner_abuse_05_a"=>null,"shaked_ur_partner_by_holding_tight1_desc1 as Partner_abuse_05_b"=>null,"shaked_ur_partner_by_holding_tight1_desc2 as Partner_abuse_05_c"=>null,"shaked_ur_partner_by_holding_tight1_desc3 as Partner_abuse_05_d"=>null,
	
	"tried_to_throw_ur_husband1 as Partner_abuse_06_a"=>null,"tried_to_throw_ur_husband1_desc1 as Partner_abuse_06_b"=>null,"tried_to_throw_ur_husband1_desc2 as Partner_abuse_06_c"=>null,"tried_to_throw_ur_husband1_desc3 as Partner_abuse_06_d"=>null,
	
	"tried_to_throw_things_on_ur_partner1 as Partner_abuse_07_a"=>null,"tried_to_throw_things_on_ur_partner1_desc1 as Partner_abuse_07_b"=>null,"tried_to_throw_things_on_ur_partner1_desc2 as Partner_abuse_07_c"=>null,"tried_to_throw_things_on_ur_partner1_desc3 as Partner_abuse_07_d"=>null,
	
	"tried_to_hold_ur_partners_neck1 as Partner_abuse_08_a"=>null,"tried_to_hold_ur_partners_neck1_desc1 as Partner_abuse_08_b"=>null,"tried_to_hold_ur_partners_neck1_desc2 as Partner_abuse_08_c"=>null,"tried_to_hold_ur_partners_neck1_desc3 as Partner_abuse_08_d"=>null,
	
	"tried_to_hit_kick_bite1 as Partner_abuse_09_a"=>null,"tried_to_hit_kick_bite1_desc1 as Partner_abuse_09_b"=>null,"tried_to_hit_kick_bite1_desc2 as Partner_abuse_09_c"=>null,"tried_to_hit_kick_bite1_desc3 as Partner_abuse_09_d"=>null,
	
	"tried_to_hit_or_hitted1 as Partner_abuse_10_a"=>null,"tried_to_hit_or_hitted1_desc1 as Partner_abuse_10_b"=>null,"tried_to_hit_or_hitted1_desc2 as Partner_abuse_10_c"=>null,"tried_to_hit_or_hitted1_desc3 as Partner_abuse_10_d"=>null,
	
	"hitted_ur_partner1 as Partner_abuse_11_a"=>null,"hitted_ur_partner1_desc1 as Partner_abuse_11_b"=>null,"hitted_ur_partner1_desc2 as Partner_abuse_11_c"=>null,"hitted_ur_partner1_desc3 as Partner_abuse_11_d"=>null,
	
	"feared_ur_partner_by_gun_knife1 as Partner_abuse_12_a"=>null,"feared_ur_partner_by_gun_knife1_desc1 as Partner_abuse_12_b"=>null,"feared_ur_partner_by_gun_knife1_desc2 as Partner_abuse_12_c"=>null,"feared_ur_partner_by_gun_knife1_desc3 as Partner_abuse_12_d"=>null,
	
	"used_gun_knife1 as Partner_abuse_13_a"=>null,"used_gun_knife1_desc1 as Partner_abuse_13_b"=>null,"used_gun_knife1_desc2 as Partner_abuse_13_c"=>null,"used_gun_knife1_desc3 as Partner_abuse_13_d"=>null,
	
	
	
	
	"rotated_ur_partner_hand12 as Self_Abuse_PostPreg_01_a"=>null,"rotated_ur_partner_hand12_desc1 as Self_Abuse_PostPreg_01_b"=>null,"rotated_ur_partner_hand12_desc2 as Self_Abuse_PostPreg_01_c"=>null,
	
	"grabed_and_pushed_ur_husband12 as Self_Abuse_PostPreg_02_a"=>null,"grabed_and_pushed_ur_husband12_desc1 as Self_Abuse_PostPreg_02_b"=>null,"grabed_and_pushed_ur_husband12_desc2 as Self_Abuse_PostPreg_02_c"=>null,
	
	"slapped_ur_husband12 as Self_Abuse_PostPreg_03_a"=>null,"slapped_ur_husband12_desc1 as Self_Abuse_PostPreg_03_b"=>null,"slapped_ur_husband12_desc2 as Self_Abuse_PostPreg_03_c"=>null,
	
	"forced_ur_partner_for_sex12 as Self_Abuse_PostPreg_04_a"=>null,"forced_ur_partner_for_sex12_desc1 as Self_Abuse_PostPreg_04_b"=>null,"forced_ur_partner_for_sex12_desc2 as Self_Abuse_PostPreg_04_c"=>null,
	
	"shaked_ur_partner_by_holding_tight12 as Self_Abuse_PostPreg_05_a"=>null,"shaked_ur_partner_by_holding_tight12_desc1 as Self_Abuse_PostPreg_05_b"=>null,"shaked_ur_partner_by_holding_tight12_desc2 as Self_Abuse_PostPreg_05_c"=>null,
	
	"tried_to_throw_ur_husband12 as Self_Abuse_PostPreg_06_a"=>null,"tried_to_throw_ur_husband12_desc1 as Self_Abuse_PostPreg_06_b"=>null,"tried_to_throw_ur_husband12_desc2 as Self_Abuse_PostPreg_06_c"=>null,
	
	"tried_to_throw_things_on_ur_partner12 as Self_Abuse_PostPreg_07_a"=>null,"tried_to_throw_things_on_ur_partner12_desc1 as Self_Abuse_PostPreg_07_b"=>null,"tried_to_throw_things_on_ur_partner12_desc2 as Self_Abuse_PostPreg_07_c"=>null,
	
	"tried_to_hold_ur_partners_neck12 as Self_Abuse_PostPreg_08_a"=>null,"tried_to_hold_ur_partners_neck12_desc1 as Self_Abuse_PostPreg_08_b"=>null,"tried_to_hold_ur_partners_neck12_desc2 as Self_Abuse_PostPreg_08_c"=>null,
	
	"tried_to_hit_kick_bite12 as Self_Abuse_PostPreg_09_a"=>null,"tried_to_hit_kick_bite12_desc1 as Self_Abuse_PostPreg_09_b"=>null,"tried_to_hit_kick_bite12_desc2 as Self_Abuse_PostPreg_09_c"=>null,
	
	"tried_to_hit_or_hitted12 as Self_Abuse_PostPreg_10_a"=>null,"tried_to_hit_or_hitted12_desc1 as Self_Abuse_PostPreg_10_b"=>null,"tried_to_hit_or_hitted12_desc2 as Self_Abuse_PostPreg_10_c"=>null,
	
	"hitted_ur_partner12 as Self_Abuse_PostPreg_11_a"=>null,"hitted_ur_partner12_desc1 as Self_Abuse_PostPreg_11_b"=>null,"hitted_ur_partner12_desc2 as Self_Abuse_PostPreg_11_c"=>null,
	
	"feared_ur_partner_by_gun_knife12 as Self_Abuse_PostPreg_12_a"=>null,"feared_ur_partner_by_gun_knife12_desc1 as Self_Abuse_PostPreg_12_b"=>null,"feared_ur_partner_by_gun_knife12_desc2 as Self_Abuse_PostPreg_12_c"=>null,
	
	"used_gun_knife12 as Self_Abuse_PostPreg_13_a"=>null,"used_gun_knife12_desc1 as Self_Abuse_PostPreg_13_b"=>null,"used_gun_knife12_desc2 as Self_Abuse_PostPreg_13_c"=>null,
	
	"rotated_ur_partner_hand123 as Partner_Abuse_PostPreg_01_a"=>null,"rotated_ur_partner_hand123_desc1 as Partner_Abuse_PostPreg_01_b"=>null,"rotated_ur_partner_hand123_desc2 as Partner_Abuse_PostPreg_01_c"=>null,
	
	"grabed_and_pushed_ur_husband123 as Partner_Abuse_PostPreg_02_a"=>null,"grabed_and_pushed_ur_husband123_desc1 as Partner_Abuse_PostPreg_02_b"=>null,"grabed_and_pushed_ur_husband123_desc2 as Partner_Abuse_PostPreg_02_c"=>null,
	
	"slapped_ur_husband123 as Partner_Abuse_PostPreg_03_a"=>null,"slapped_ur_husband123_desc1 as Partner_Abuse_PostPreg_03_b"=>null,"slapped_ur_husband123_desc2 as Partner_Abuse_PostPreg_03_c"=>null,
	
	"forced_ur_partner_for_sex123 as Partner_Abuse_PostPreg_04_a"=>null,"forced_ur_partner_for_sex123_desc1 as Partner_Abuse_PostPreg_04_b"=>null,
	"forced_ur_partner_for_sex123_desc2 as Partner_Abuse_PostPreg_04_c"=>null,
	
	"shaked_ur_partner_by_holding_tight123 as Partner_Abuse_PostPreg_05_a"=>null,"shaked_ur_partner_by_holding_tight123_desc1 as Partner_Abuse_PostPreg_05_b"=>null,"shaked_ur_partner_by_holding_tight123_desc2 as Partner_Abuse_PostPreg_05_c"=>null,
	
	"tried_to_throw_ur_husband123 as Partner_Abuse_PostPreg_06_a"=>null,"tried_to_throw_ur_husband123_desc1 as Partner_Abuse_PostPreg_06_b"=>null,"tried_to_throw_ur_husband123_desc2 as Partner_Abuse_PostPreg_06_c"=>null,
	
	"tried_to_throw_things_on_ur_partner123 as Partner_Abuse_PostPreg_07_a"=>null,"tried_to_throw_things_on_ur_partner123_desc1 as Partner_Abuse_PostPreg_07_b"=>null,"tried_to_throw_things_on_ur_partner123_desc2 as Partner_Abuse_PostPreg_07_c"=>null,
	
	"tried_to_hold_ur_partners_neck123 as Partner_Abuse_PostPreg_08_a"=>null,"tried_to_hold_ur_partners_neck123_desc1 as Partner_Abuse_PostPreg_08_b"=>null,"tried_to_hold_ur_partners_neck123_desc2 as Partner_Abuse_PostPreg_08_c"=>null,
	
	"tried_to_hit_kick_bite123 as Partner_Abuse_PostPreg_09_a"=>null,"tried_to_hit_kick_bite123_desc1 as Partner_Abuse_PostPreg_09_b"=>null,"tried_to_hit_kick_bite123_desc2 as Partner_Abuse_PostPreg_09_c"=>null,
	
	"tried_to_hit_or_hitted123 as Partner_Abuse_PostPreg_10_a"=>null,"tried_to_hit_or_hitted123_desc1 as Partner_Abuse_PostPreg_10_b"=>null,"tried_to_hit_or_hitted123_desc2 as Partner_Abuse_PostPreg_10_c"=>null,
	
	"hitted_ur_partner123 as Partner_Abuse_PostPreg_11_a"=>null,"hitted_ur_partner123_desc1 as Partner_Abuse_PostPreg_11_b"=>null,"hitted_ur_partner123_desc2 as Partner_Abuse_PostPreg_11_c"=>null,
	
	"feared_ur_partner_by_gun_knife123 as Partner_Abuse_PostPreg_12_a"=>null,"feared_ur_partner_by_gun_knife123_desc1 as Partner_Abuse_PostPreg_12_b"=>null,"feared_ur_partner_by_gun_knife123_desc2 as Partner_Abuse_PostPreg_12_c"=>null,
	
	"used_gun_knife123 as Partner_Abuse_PostPreg_13_a"=>null,"used_gun_knife123_desc1 as Partner_Abuse_PostPreg_13_b"=>null,"used_gun_knife123_desc2 as Partner_Abuse_PostPreg_13_c"=>null];
	}
	
	private function picsTable(){
		return ["stroke_tummy as Self_01"=>null,"stroke_back as Self_02"=>null,"stroke_face as Self_03"=>null,"stroke_arms as Self_04"=>null,"pick_up as Self_05"=>null,"talk as Self_06"=>null,"cuddle as Self_07"=>null,"rock as Self_08"=>null,"kiss as Self_09"=>null,"hold as Self_10"=>null,"watch as Self_11"=>null,"leave_lie_down as Self_12"=>null,"like_touched as Baby_01"=>null,"like_talked as Baby_02"=>null,"like_sing as Baby_03"=>null,"like_leave as Baby_04"=>null,"like_hold as Baby_05"=>null,"like_put_down as Baby_06"=>null,"like_dummy as Baby_07"=>null,"suck_hands as Baby_08"=>null,"best_of_likes as Baby_Likes"=>null];
	}
	
	
	private function breastfeedingTable(){
		return ["determine_enough_milk as '01'"=>null,"successfully_cope as '02'"=>null,"without_formula as '03'"=>null,"lached_properly as '04'"=>null,"manage_situation as '05'"=>null,"manage_crying as '06'"=>null,"wanting_breastfeed as '07'"=>null,"comfortable_family_members as '08'"=>null,"satisfied_experience as '09'"=>null,"deal_time_consuming as '10'"=>null,"finish_1breast as '11'"=>null,"continue_feeding as '12'"=>null,"keepup_baby_demands as '13'"=>null,"tell_finished as '14'"=>null];
	}
	
	private function babylikesanddislikesTable(){
		return ["fussing_crying as '01'"=>null,"toss_cot as '02'"=>null,"fuss_immediately as '03'"=>null,"play_quitely as '04'"=>null,"cry_someone_didnt_come as '05'"=>null,"angry as '06'"=>null,"contented_corner as '07'"=>null,"cry_before_nap as '08'"=>null,"lie_sit_quietly as '09'"=>null,"squirm as '10'"=>null,"fuss_facewash as '11'"=>null,"fuss_hairwash as '12'"=>null,"cry_removed_playrhing as '13'"=>null,"not_bothered_removed_plaything as '14'"=>null,"protest as '15'"=>null,"fuss_back as '16'"=>null,"upset_wanted_something as '17'"=>null,"tantrums_wanted_something as '18'"=>null,"distress_infant_seat as '19'"=>null,"cry_parents_chane_appearence as '20'"=>null,"startle_sudden_change as '21'"=>null,"startle_noise as '22'"=>null,"cling_parent_unfamiliar_person as '23'"=>null,"refuse_unfamiliar_person as '24'"=>null,"hang_back_unfamiliar_person as '25'"=>null,"never_warmup_unfamiliar_person as '26'"=>null,"cling_several_unfamiliar_persons as '27'"=>null,"cry_unfamiliar_adults as '28'"=>null,"upset_morethan_10min as '29'"=>null,"distress_new_place as '30'"=>null,"upset_new_place as '31'"=>null,"allow_unfamiliar_person_pickup as '32'"=>null,"distress_outside as '33'"=>null,"cry_taken_outside as '34'"=>null,"cry_unfamilar_person_pickup as '35'"=>null];
	}
	
	private function childecbqTable(){
		return ["publicplace_unfamiliarperson as '1'"=>null,"work_incomplete as '2'"=>null,"familiar_baby as '3'"=>null,"other_activities as '4'"=>null,"happy_sing as '5'"=>null,"play_outside as '6'"=>null,"toys1 as '7'"=>null,"toys2 as '8'"=>null,"like_elders as '9'"=>null,"simple_activity as '10'"=>null,"play_inside as '11'"=>null,"hug as '12'"=>null,"new_activity as '13'"=>null,"concentrate as '14'"=>null,"attention as '15'"=>null,"kirikiri_clothes as '16'"=>null,"kirikiri_sounds as '17'"=>null,"active_sunset as '18'"=>null,"public_place as '19'"=>null,"play_out as '20'"=>null,"stop_work as '21'"=>null,"sad_cry as '22'"=>null,"sad_look as '23'"=>null,"walk_in_home as '24'"=>null,"new_toys as '25'"=>null,"sad_on_no as '26'"=>null,"patience as '27'"=>null,"slow_smile as '28'"=>null,"lap_sleep as '29'"=>null,"talks_familiar as '30'"=>null,"breakable_things as '31'"=>null,"dint_go_inside as '32'"=>null,"cry_morethan_3min as '33'"=>null,"fast_samadhana as '34'"=>null,"looks_other_activity as '35'"=>null,"plays_withdiff_people as '36'"=>null];
	}
	
	
	private function infantanthrosixthTable(){
		return ["length_6_months as '01'"=>null,"weight_6_months as '02'"=>null,"head_circumference_6_months as '03'"=>null,"chest_circumference_6_months as '04'"=>null,"midarm_circumference_6_months as '05'"=>null,"waist_circumference_6_months as '06'"=>null,"hip_circumference_6_months as '07'"=>null];
	}
	private function infantanthrotwelveTable(){
		return["length_1_year as '01'"=>null,"weight_1_year as '02'"=>null,"head_circumference_1_year as '03'"=>null,"chest_circumference_1_year as '04'"=>null,"midarm_circumference_1_year as '05'"=>null,"waist_circumference_1_year as '06'"=>null,"hip_circumference_1_year as '07'"=>null];
	}
	private function infantanthrotwentyfourthTable(){
		return["length_2_year as '01'"=>null,"weight_2_year as '02'"=>null,"head_circumference_2_year as '03'"=>null,"chest_circumference_2_year as '04'"=>null,"midarm_circumference_2_year as '05'"=>null,"waist_circumference_2_year as '06'"=>null,"hip_circumference_2_year as '07'"=>null];
	}
	
	
	private function maternalTable(){
		return ["physical_health_problems as '1'"=>null,"physical_health_problems_det as 1_details"=>null,"emotional_problem as '2'"=>null,
		"emotional_problem_det as 2_details"=>null,"medicines_taken as '3'"=>null,"medicines_taken_det as 3_details"=>null,"sleepin_pills as 4_a_1"=>null,"sleepin_pills2 as 4_a_2"=>null,"sleepin_pills3 as 4_a_3"=>null
		,"depression_pills as 4_b_1"=>null,"depression_pills2 as 4_b_2"=>null,"depression_pills3 as 4_b_3"=>null
		,"anxiety_pills as 4_c_1"=>null,"anxiety_pills2 as 4_c_2"=>null,"anxiety_pills3 as 4_c_3"=>null,"sleep as '5'"=>null,"hours_of_sleep as '6'"=>null,"persistant_pain as 7_a"=>null,"persistant_pain_dur as 7_b"=>null,"persistant_pain_site as 7_c"=>null,"pregnant_aftr_firstborn as 8_a"=>null,"last_mensural_date as 8_b"=>null,"status_presentpregnancy as 8_c"=>null,"dob_baby as 8_d"=>null,"sex_baby as 8_e"=>null];
	}
	private function sharedcaredgivingacTable(){
		return ["songs_acg_lastweek as 1c"=>null,"played_acg_lastweek as 2c"=>null,"taken_out_acg_lastweek as 3c"=>null,"introduced_newthings_acg_lastweek as 4c"=>null,"told_not_todo_something_acg_lastweek as 5c"=>null,"cleaned_acg_lastweek as 6c"=>null,"rocked_acg_lastweek as 7c"=>null,"carried_acg_lastweek as 8c"=>null,"scolded_acg_lastweek as 9c"=>null,"wasnt_too_hot_acg_lastweek as 10c"=>null,"care_sick_acg_lastweek as 11c"=>null,"taken_harm_acg_lastweek as 12c"=>null,"fed_acg_lastweek as 13c"=>null,"kissed_acg_lastweek as 14c"=>null,"cooed_acg_lastweek as 15c"=>null,"put_sleep_acg_lastweek as 16c"=>null,"bathed_acg_lastweek as 17c"=>null,"didnt_eat_harmful_acg_lastweek as 18c"=>null,"soothed_acg_lastweek as 19c"=>null,"massaged_acg_lastweek as 20c"=>null,"showed_what_todo_acg_lastweek as 21c"=>null,"thoogiddene_acg_lastweek as 22c"=>null,"cuddled_crying_acg_lastweek as 23c"=>null,"dressed_acg_lastweek as 24c"=>null,"told_off_acg_lastweek as 25c"=>null,"comfortable_acg_lastweek as 26c"=>null,"slept_beside_acg_lastweek as 27c"=>null,
		"introduced_new_people_acg_lastweek as 28c"=>null,"scolded_crying_acg_lastweek as 29c"=>null,"cradled_crying_acg_lastweek as 30c"=>null,"made_warm_acg_lastweek as 31c"=>null,"gone_wokeup_acg_lastweek as 32c"=>null,"prepared_formula_milk_acg_lastweek as 33c"=>null,"talked_acg_lastweek as 34c"=>null,"cuddled_acg_lastweek as 35c"=>null,"sung_acg_lastweek as 36c"=>null,"taken_nurse_acg_lastweek as 37c"=>null,"talked_sternly_acg_lastweek as 38c"=>null,"held_acg_lastweek as 39c"=>null,"shared_smiles_acg_lastweek as 40c"=>null,"distracted_crying_acg_lastweek as 41c"=>null,"showed_rightthing_acg_lastweek as 42c"=>null,"taken_visit_family_acg_lastweek as 43c"=>null,"cradled_arms_acg_lastweek as 44c"=>null,"fed_crying_acg_lastweek as 45c"=>null,"prayed_acg_lastweek as 46c"=>null,"invovled_family_rituals_acg_lastweek as 47c"=>null];
		
	}
	private function sharedcaregivingTable(){
		return ["family_type as A"=>null,"number_adults as B"=>null,"number_infants as C"=>null,"number_children_preschool as D"=>null,
		"number_children as E"=>null,"number_adolescents as F"=>null,"current_stay as G"=>null,"care_myself as H"=>null,"care_husband as I"=>null,
		"care_maternal_grandmother as J"=>null,"any_others as K"=>null,
		"songs as 1a"=>null,"played as 2a"=>null,"taken_out as 3a"=>null,"introduced_newthings as 4a"=>null,"told_not_todo_something as 5a"=>null,"cleaned as 6a"=>null,"rocked as 7a"=>null,"carried as 8a"=>null,"scolded as 9a"=>null,"wasnt_too_hot as 10a"=>null,"care_sick as 11a"=>null,"taken_harm as 12a"=>null,"fed as 13a"=>null,"kissed as 14a"=>null,"cooed as 15a"=>null,"put_sleep as 16a"=>null,"bathed as 17a"=>null,"didnt_eat_harmful as 18a"=>null,"soothed as 19a"=>null,"massaged as 20a"=>null,"showed_what_todo as 21a"=>null,"thoogiddene as 22a"=>null,"cuddled_crying as 23a"=>null,"dressed as 24a"=>null,"told_off as 25a"=>null,"comfortable as 26a"=>null,"slept_beside as 27a"=>null,"introduced_new_people as 28a"=>null,"scolded_crying as 29a"=>null,"cradled_crying as 30a"=>null,"made_warm as 31a"=>null,"gone_wokeup as 32a"=>null,"prepared_formula_milk as 33a"=>null,"talked as 34a"=>null,"cuddled as 35a"=>null,"sung as 36a"=>null,"taken_nurse as 37a"=>null,"talked_sternly as 38a"=>null,"held as 39a"=>null,"shared_smiles as 40a"=>null,"distracted_crying as 41a"=>null,"showed_rightthing as 42a"=>null,"taken_visit_family as 43a"=>null,"cradled_arms as 44a"=>null,"fed_crying as 45a"=>null,"prayed as 46a"=>null,"invovled_family_rituals as 47a"=>null,
		"songs_acg as 1b"=>null,"played_acg as 2b"=>null,"taken_out_acg as 3b"=>null,"introduced_newthings_acg as 4b"=>null,"told_not_todo_something_acg as 5b"=>null,"cleaned_acg as 6b"=>null,"rocked_acg as 7b"=>null,"carried_acg as 8b"=>null,"scolded_acg as 9b"=>null,"wasnt_too_hot_acg as 10b"=>null,"care_sick_acg as 11b"=>null,"taken_harm_acg as 12b"=>null,"fed_acg as 13b"=>null,"kissed_acg as 14b"=>null,"cooed_acg as 15b"=>null,"put_sleep_acg as 16b"=>null,"bathed_acg as 17b"=>null,"didnt_eat_harmful_acg as 18b"=>null,"soothed_acg as 19b"=>null,"massaged_acg as 20b"=>null,"showed_what_todo_acg as 21b"=>null,"thoogiddene_acg as 22b"=>null,"cuddled_crying_acg as 23b"=>null,"dressed_acg as 24b"=>null,"told_off_acg as 25b"=>null,"comfortable_acg as 26b"=>null,"slept_beside_acg as 27b"=>null,"introduced_new_people_acg as 28b"=>null,"scolded_crying_acg as 29b"=>null,"cradled_crying_acg as 30b"=>null,
		"made_warm_acg as 31b"=>null,"gone_wokeup_acg as 32b"=>null,"prepared_formula_milk_acg as 33b"=>null,"talked_acg as 34b"=>null,"cuddled_acg as 35b"=>null,"sung_acg as 36b"=>null,"taken_nurse_acg as 37b"=>null,"talked_sternly_acg as 38b"=>null,"held_acg as 39b"=>null,"shared_smiles_acg as 40b"=>null,"distracted_crying_acg as 41b"=>null,"showed_rightthing_acg as 42b"=>null,"taken_visit_family_acg as 43b"=>null,"cradled_arms_acg as 44b"=>null,"fed_crying_acg as 45b"=>null,"prayed_acg as 46b"=>null,"invovled_family_rituals_acg as 47b"=>null,
		"other_activity as _L"=>null,"other_activity_acg as _M"=>null];
	}
	
	
	
	private function bitseaTable(){
		return ["happy_won as '1'"=>null,"hurt_himself as '2'"=>null,"stress_fear as '3'"=>null,"cant_sit_quitely as '4'"=>null,"follows_rules as '5'"=>null,"wakeup_midnight as '6'"=>null,"cry as '7'"=>null,"animal_fear as '8A'"=>null,"animal_fear_det as '8B'"=>null,"less_enjoy as '9'"=>null,"ifsad_searches_parents as '10'"=>null,"cry_left as '11'"=>null,"thinks as '12'"=>null,"looked_on_call as '13'"=>null,"didnt_respond_pain as '14'"=>null,"happy_with_loved as '15'"=>null,"didnt_touch_things as '16'"=>null,"problem_in_sleep as '17'"=>null,"runs as '18'"=>null,"play_with_others as '19'"=>null,"concentrates as '20'"=>null,"problem_in_udjustment as '21'"=>null,"helps_others as '22'"=>null,"not_happy as '23'"=>null,"eating_problem as '24'"=>null,"imitates_sound as '25'"=>null,"rejects_food as '26'"=>null,"fights as '27'"=>null,"breaks_things as '28'"=>null,"shows_fingers as '29'"=>null,"beats_bites as '30'"=>null,"plays_with_toys as '31'"=>null,"sad as '32'"=>null,"hurts_parents as '33'"=>null,"didnt_move_angry as '34'"=>null,"keeps_things as '35'"=>null,"repeats_sounds as '36A'"=>null,"repeats_sounds_det as '36B'"=>null,"repeats_work as '37A'"=>null,"repeats_work_det as '37B'"=>null,"separate_from_others as '38'"=>null,"see_straight as '39'"=>null,"physical_contact as '40'"=>null,"hurts_himself as '41A'"=>null,"hurts_himself_det as '41B'"=>null,"eats_nonhealthy as '42A'"=>null,"eats_nonhealthy_det as '42B'"=>null,"feelings_about_baby as A"=>null,"feelings_about_language as B"=>null];
	}
	
	private function cbclTable(){
		return ["pain as '1'"=>null,"behave_childish as '2'"=>null,"fear_for_doing_new_things as '3'"=>null,"far_eye_contact as '4'"=>null,"less_concentration as '5'"=>null,"patience_or_active as '6'"=>null,"impatience_for_displacing_object as '7'"=>null,"no_waiting_habit as '8'"=>null,"chewing_non_eatable_things as '9'"=>null,"depending_on_elders as '10'"=>null,"frequently_asks_help as '11'"=>null,"dont_poop as '12'"=>null,"cries_more as '13'"=>null,"cruel_for_animals as '14'"=>null,"perseverance as '15'"=>null,"finish_demands_suddenly as '16'"=>null,"destroys_his_or_her_belongings as '17'"=>null,"destroys_family_or_other_children_things as '18'"=>null,"loose_motion_or_Diarrhea as '19'"=>null,"doesnt_work_what_people_say as '20'"=>null,"irritation_for_change_of_regular_daily_plans as '21'"=>null,"doesnt_like_to_sleep_alone as '22'"=>null,"doesnt_respond_to_people as '23'"=>null,"doesnt_eats_well_explain as '24A'"=>null,"doesnt_eats_well_explain_det as '24B'"=>null,"doesnt_be_with_other_children as '25'"=>null,"behaves_like_elders_or_doesnt_enjoy as '26'"=>null,"doesnt_feels_bad_after_mischievous_things as '27'"=>null,"doesnt_like_to_go_outside as '28'"=>null,"easily_depressed as '29'"=>null,"easily_feels_jealous as '30'"=>null,"eat_or_drinks_non_eatable_food as '31'"=>null,"fear_for_certain_animals_at_certain_place_or_situation as '32'"=>null,"suddenly_hurts_his_or_her_feelings as '33'"=>null,"will_have_more_pain_or_accidented as '34'"=>null,"involves_in_many_fights as '35'"=>null,"interfere_in_every_matter as '36'"=>null,"feels_bad_when_he_or_she_is_away_from_their_parents as '37'"=>null,"disturbance_for_sleeping as '38'"=>null,"headache_without_medical_reasons as '39'"=>null,"hits_others as '40'"=>null,"holds_breath as '41'"=>null,"disturbing_animals_or_people_without_intention as '42'"=>null,"depressed_without_reason as '43'"=>null,"angry as '44'"=>null,"vomitting_sensation_or_unhealthy_without_medical_reason as '45'"=>null,"energyless_or_shewering_of_body_parts as '46A'"=>null,"energyless_or_shewering_of_body_parts_det as '46B'"=>null,"energyless_or_feared_person as '47'"=>null,"bad_dreams as '48'"=>null,"eats_more as '49'"=>null,"more_tired as '50'"=>null,"fears_without_reason as '51'"=>null,"pain_while_pooping_without_medical_reason as '52'"=>null,"suddenly_attacks_people_physically as '53'"=>null,"tears_skin_or_nose_or_other_body_parts as '54A'"=>null,"tears_skin_or_nose_or_other_body_parts_det as '54B'"=>null,"plays_more_with_sex_organs as '55'"=>null,"doesnt_co_operate_or_doesnt_finishes_work_neatly as '56'"=>null,"eye_problem_without_medical_reason as '57A'"=>null,"eye_problem_without_medical_reason_det as '57B'"=>null,"doesnt_changes_behaviour_even_after_punishment as '58'"=>null,"from_one_activity_to_another_changes_immediately as '59'"=>null,"itchyness_or_skin_problems_without_medical_reason as '60'"=>null,"disagrees_food as '61'"=>null,"disagrress_playing_more_active_games as '62'"=>null,"frequently_shakes_his_head_or_body_parts as '63'"=>null,"disagrees_to_sleep_at_night as '64'"=>null,"disagrees_toilet_coaching as '65A'"=>null,"disagrees_toilet_coaching_det as '65B'"=>null,"screeches_more as '66'"=>null,"looks_like_they_doesnt_respond_for_love_and_affection as '67'"=>null,"feels_shy_easily as '68'"=>null,"selfish_or_doesnt_share as '69'"=>null,"doesnt_show_love_for_people as '70'"=>null,"shows_little_interest_with_surrounding_things as '71'"=>null,"doesnt_fear_if_he_knows_it_will_pain_also as '72'"=>null,"feels_more_shy as '73'"=>null,"sleeps_less_in_day_night_when_compared_to_normal_children as '74A'"=>null,"sleeps_less_in_day_night_when_compared_to_normal_children_det as '74B'"=>null,
		"plays_with_recess_or_poop as '75'"=>null,"problem_in_speaking as '76A'"=>null,"problem_in_speaking_det as '76B'"=>null,"staring_continuosly_at_only_side_or_thinking_something_else as '77'"=>null,"stomach_ache_without_medical_reason as '78'"=>null,"change_in_feelings as '79'"=>null,"different_behaviour as '80A'"=>null,"different_behaviour_det as '80B'"=>null,"irritating_or_getting_angry as '81'"=>null,"sudden_difference_in_behaviour as '82'"=>null,"gets_more_angry as '83'"=>null,"speaks_or_cry_while_sleeping as '84'"=>null,"fusses_more as '85'"=>null,"cares_about_neatness as '86'"=>null,"fears_more as '87'"=>null,"feels_helpless_or_doesnt_listen_to_elders_words as '88'"=>null,"does_less_activities_or_walks_slowly as '89'"=>null,"not_happy_or_being_sad as '90'"=>null,"speaks_at_high_voice as '91'"=>null,"feels_sad_with_new_people_or_situation as '92A'"=>null,"feels_sad_with_new_people_or_situation_det as '92B'"=>null,"vomit as '93'"=>null,"frequently_wakesup_at_night as '94'"=>null,"walks_without_holding_hand as '95'"=>null,"wants_to_look_after_him_for_most_of_the_time as '96'"=>null,"murmuring as '97'"=>null,"feels_shy_or_doesnt_be_with_others as '98'"=>null,"thinks as '99'"=>null,"list_other_problems_if_he_or_she_has as '100A'"=>null,"list_other_problems_if_he_or_she_has_det as '100B'"=>null,"bites_other_children as '101'"=>null,"kicks_other_children as '102'"=>null,"helps_other_children as '103'"=>null,"beats_other_children as '104'"=>null,"share_with_other_children as '105'"=>null,"laughs_at_other_children as '106'"=>null,"visheshachethana as 'A1'"=> null,"visheshachethana_det as 'A2'"=>null,"athiyada_kalaji as 'B'"=>null];
	}
	
	private function infanthealthTable(){
		return ["dod as DATE"=>null,"ageofinfant as AGE"=>null,"hospitalization as '01'"=>null,"nicu_care as '02'"=>null,"neonatal_jaundice as '03'"=>null,"icu_care as '04'"=>null,"fevers as '05'"=>null,"exanthaemotous_fever as '06'"=>null,"seizures as '07'"=>null,"asthma as '08'"=>null,"respiratory_illness as '09'"=>null,"diarrhoea as '10'"=>null,"doc_visit1 as '11_01mnth'"=>null,"doc_visit2 as '11_02mnth'"=>null,"doc_visit3 as '11_03mnth'"=>null,"doc_visit4 as '11_04mnth'"=>null,"doc_visit5 as '11_05mnth'"=>null,"doc_visit6 as '11_06mnth'"=>null,"doc_visit7 as '11_07mnth'"=>null,"doc_visit8 as '11_08mnth'"=>null,"doc_visit9 as '11_09mnth'"=>null,"doc_visit10 as '11_10mnth'"=>null,"doc_visit11 as '11_11mnth'"=>null,"doc_visit12 as '11_12mnth'"=>null,"doc_visit13 as '11_13mnth'"=>null,"doc_visit14 as '11_14mnth'"=>null,"doc_visit15 as '11_15mnth'"=>null,"doc_visit16 as '11_16mnth'"=>null,"doc_visit17 as '11_17mnth'"=>null,"doc_visit18 as '11_18mnth'"=>null,"doc_visit19 as '11_19mnth'"=>null,"doc_visit20 as '11_20mnth'"=>null,"doc_visit21 as '11_21mnth'"=>null,"doc_visit22 as '11_22mnth'"=>null,"doc_visit23 as '11_23mnth'"=>null,"doc_visit24 as '11_24mnth'"=>null,"doc_visit25 as '11_25mnth'"=>null,"doc_visit26 as '11_26mnth'"=>null,"doc_visit27 as '11_27mnth'"=>null,"doc_visit28 as '11_28mnth'"=>null,"doc_visit29 as '11_29mnth'"=>null,"doc_visit30 as '11_30mnth'"=>null,"doc_visit31 as '11_31mnth'"=>null,"doc_visit32 as '11_32mnth'"=>null,"doc_visit33 as '11_33mnth'"=>null,"doc_visit34 as '11_34mnth'"=>null,"doc_visit35 as '11_35mnth'"=>null,"others as '12 '"=>null,"other_details as '12'"=>null,
		
		"how_many_times_burnt as 2_a_1"=>null,"how_old_was_she_while_burnt as 2_a_2_age1"=>null,"how_old_was_she_while_burnt2 as 2_a_2_age2"=>null,"how_old_was_she_while_burnt3 as 2_a_2_age3"=>null,"what_did_person_with_child_do_about_burnt as 2_a_3"=>null,"what_did_person_with_child_do_about_burnt_others as 2_a_3_details"=>null,
		
		"how_many_times_dropped as 2_b_1"=>null,"how_old_was_she_while_dropped as 2_b_2_age1"=>null,"how_old_was_she_while_dropped2 as 2_b_2_age2"=>null,"how_old_was_she_while_dropped3 as 2_b_2_age3"=>null,"what_did_person_with_child_do_about_dropp as 2_b_3"=>null,"what_did_person_with_child_do_about_dropp_others as 2_b_3_details"=>null,
		
		"how_many_times_swallowed as 2_c_1"=>null,"how_old_was_she_while_swallowed as 2_c_2_age1"=>null,"how_old_was_she_while_swallowed2 as 2_c_2_age2"=>null,"how_old_was_she_while_swallowed3 as 2_c_2_age3"=>null,"what_did_person_with_child_do_about_swallow as 2_c_3"=>null,"what_did_person_with_child_do_about_swallow_others as 2_c_3_details"=>null,
		
		"how_many_times_others as 2_d_1"=>null,"how_old_was_she_while_other as 2_d_2_age1"=>null,"how_old_was_she_while_other2 as 2_d_2_age2"=>null,"how_old_was_she_while_other3 as 2_d_2_age3"=>null,"what_did_person_with_child_do_about_other as 2_d_3"=>null,"what_did_person_with_child_do_about_other_others as 2_d_3_details"=>null,
		
		"how_often_on_weekdays as 3_a"=>null,"how_often_at_weekdays as 3_b"=>null,
		
		"t1_5m as 4_a"=>null,"t2_5m as 4_b"=>null,"t3_5m as 4_c"=>null,"t9m as 4_d"=>null,"t16_24m as 4_e"=>null,];
	}
	
	private function pbfTable(){
		return ["talks_friendly_mother as 1m"=>null,"dint_help_needed_mother as 2m"=>null,"allowed_loved_work_mother as 3m"=>null,
		"emotional_mother as 4m"=>null,"understands_pain_mother as 5m"=>null,"loving_mother as 6m"=>null,"allowed_own_decisions_mother as 7m"=>null,
		"hates_growing_mother as 8m"=>null,"stops_all_work_mother as 9m"=>null,"dint_allow_alone_mother as 10m"=>null,"happy_to_talk_mother as 11m"=>null,
		"laughs_mother as 12m"=>null,"treat_baby_mother as 13m"=>null,"didnt_understand_needs_mother as 14m"=>null,"decides_own_mother as 15m"=>null,
		"dont_need_feel_mother as 16m"=>null,"helps_in_sad_mother as 17m"=>null,"didnt_talk_lot_mother as 18m"=>null,"feels_dependent_mother as 19m"=>null,
		"didnt_lookafter_mother as 20m"=>null,"give_freedom_mother as 21m"=>null,"allow_outside_mother as 22m"=>null,"saves_more_mother as 23m"=>null,
		"praises_mother as 24m"=>null,"allow_own_style_mother as 25m"=>null,
		
		"talks_friendly_father as 1f"=>null,"dint_help_needed_father as 2f"=>null,
		"allowed_loved_work_father as 3f"=>null,"emotional_father as 4f"=>null,"understands_pain_father as 5f"=>null,"loving_father as 6f"=>null,
		"allowed_own_decisions_father as 7f"=>null,"hates_growing_father as 8f"=>null,"stops_all_work_father as 9f"=>null,
		"dint_allow_alone_father as 10f"=>null,"happy_to_talk_father as 11f"=>null,"laughs_father as 12f"=>null,"treat_baby_father as 13f"=>null,
		"didnt_understand_needs_father as 14f"=>null,"decides_own_father as 15f"=>null,"dont_need_feel_father as 16f"=>null,
		"helps_in_sad_father as 17f"=>null,"didnt_talk_lot_father as 18f"=>null,"feels_dependent_father as 19f"=>null,"didnt_lookafter_father as 20f"=>null,
		"give_freedom_father as 21f"=>null,"allow_outside_father as 22f"=>null,"saves_more_father as 23f"=>null,"praises_father as 24f"=>null,
		"allow_own_style_father as 25f"=>null];
	}
	
	private function pbsTable(){
		return ["less_helps_mother as 26m"=>null,"any_siblings as 26A"=>null,"no_of_brothers as 26B"=>null,"no_of_cousines_brothers as 26C"=>null,"any_sisters as 26D"=>null,"no_of_sisters as 26E"=>null,"no_of_cousins as 26F"=>null,"less_understands_mother as 27m"=>null,"less_praise_mother as 28m"=>null,"more_secure_mother as 29m"=>null,
		"allow_decisions_mother as 30m"=>null,"allowstyle_mother as 31m"=>null,"less_books_mother as 32m"=>null,"less_stydy_mother as 33m"=>null,
		"family_burden_mother as 34m"=>null,"show_olavu_mother as 35m"=>null,"show_nirlakshya_mother as 36m"=>null,"less_helpings_mother as 37m"=>null,
		"less_undestnds_mother as 38m"=>null,"less_praises_mother as 39m"=>null,"high_safes_mother as 40m"=>null,"allows_decionss_mother as 41m"=>null,
		"allow_style_as_boys_mother as 42m"=>null,"less_resources_mother as 43m"=>null,"less_oportunity_study_mother as 44m"=>null,"boon_mother as 45m"=>null,
		"jokes_onme_mother as 46m"=>null,"respects_me_mother as 47m"=>null,
		
		"less_helps_father as 26f"=>null,"less_understands_father as 27f"=>null,
		"less_praise_father as 28f"=>null,"more_secure_father as 29f"=>null,"allow_decisions_father as 30f"=>null,"allowstyle_father as 31f"=>null,
		"less_books_father as 32f"=>null,"less_stydy_father as 33f"=>null,"family_burden_father as 34f"=>null,"show_olavu_father as 35f"=>null,
		"show_nirlakshya_father as 36f"=>null,"less_helpings_father as 37f"=>null,"less_undestnds_father as 38f"=>null,"less_praises_father as 39f"=>null,
		"high_safes_father as 40f"=>null,"allows_decionss_father as 41f"=>null,"allow_style_as_boys_father as 42f"=>null,"less_resources_father as 43f"=>null,
		"less_oportunity_study_father as 44f"=>null,"boon_father as 45f"=>null,"jokes_onme_father as 46f"=>null,"respects_me_father as 47f"=>null];
	}
	
	
	private function psychosocialriskfactorRetrieval($assessmentId,$motherId){
		$cols = $this->psychosocialriskfactorTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `psychosocial_risk_factors` WHERE assessment_id = $assessmentId and mother_id = $motherId")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->keyConcate($result,'m_PRFC');
	}
	
	private function babylikesanddislikesmotherRetrieval($assessmentId,$motherId){
		$cols = $this->babylikesanddislikesTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `baby_likes_dislikes` WHERE assessment_id = $assessmentId and mother_id = $motherId and respondent_id = 1")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->keyConcate($result,'m_IBQR');
	}
	private function babylikesanddislikesacRetrieval($assessmentId,$motherId){
		$cols = $this->babylikesanddislikesTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `baby_likes_dislikes` WHERE assessment_id = $assessmentId and mother_id = $motherId and respondent_id = 3")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->keyConcate($result,'ac_IBQR');
	}
	
	
	private function wooleydepressionRetrieval($assessmentId,$motherId){
		$cols = $this->wooleydepressionTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `wooley_depression` WHERE assessment_id = $assessmentId and mother_id = $motherId")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->keyConcate($result,'m_WSD');
	}
	
	private function epdsRetrieval($assessmentId,$motherId){
		$cols = $this->epdsTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `edinburgh_postnatal_depression` WHERE assessment_id = $assessmentId and mother_id = $motherId")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->keyConcate($result,'m_EPDS');
	}
	
	private function phqRetrieval($assessmentId,$motherId){
		$cols = $this->phqTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `patient_health` WHERE assessment_id = $assessmentId and mother_id = $motherId")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->keyConcate($result,'m_PHQ');
	}
	
	private function suicidequestionsRetrieval($assessmentId,$motherId){
		$cols = $this->suicidequestionsTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `suicide_questions` WHERE assessment_id = $assessmentId and mother_id = $motherId")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->SbqkeyConcate($result,'m_SBQ');
	}
	private function sassRetrieval($assessmentId,$motherId){
		$cols = $this->sassTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `sass` WHERE assessment_id = $assessmentId and mother_id = $motherId")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->keyConcate($result,'m_SASS');
	}
	
	private function staiRetrieval($assessmentId,$motherId){
		$cols = $this->staiTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `stai` WHERE assessment_id = $assessmentId and mother_id = $motherId")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->keyConcate($result,'m_STAI');
	}
	private function psychosocialassessmentRetrieval($assessmentId,$motherId){
		$cols = $this->psychosocialassessmentTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `psychosocial_assessment` WHERE assessment_id = $assessmentId and mother_id = $motherId")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->keyConcate($result,'m_Assess_of_Stress');
	}
	
	private function lifeeventsRetrieval($assessmentId,$motherId){
		$cols = $this->lifeeventsTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `life_events` WHERE assessment_id = $assessmentId and mother_id = $motherId")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->keyConcate($result,'m_LEC');
	}
	
		private function ptsdchecklistRetrieval($assessmentId,$motherId){
		$cols = $this->ptsdchecklistTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `ptsd_checklist` WHERE assessment_id = $assessmentId and mother_id = $motherId")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->keyConcate($result,'m_PTSD');
	}
	
	private function multidimentionalRetrieval($assessmentId,$motherId){
		$cols = $this->multidimentionalTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `multidimentional` WHERE assessment_id = $assessmentId and mother_id = $motherId")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->keyConcate($result,'m_ZIMET');
	}
	
	private function icmrRetrieval($assessmentId,$motherId){
		$cols = $this->icmrTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `icmr` WHERE assessment_id = $assessmentId and mother_id = $motherId")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->keyConcate($result,'m_ICMR_Violence_Assess');
	}
	
	private function ctsRetrieval($assessmentId,$motherId){
		$cols = $this->ctsTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `conflict_tactics_scale` WHERE assessment_id = $assessmentId and mother_id = $motherId")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->keyConcate($result,'m_CTS');
	}
	
	private function picsmRetrieval($assessmentId,$motherId){
		$cols = $this->picsTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `pics` WHERE assessment_id = $assessmentId and mother_id = $motherId and respondent_id = 1")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->keyConcate($result,'m_PICS');
	}
	private function picsacRetrieval($assessmentId,$motherId){
		$cols = $this->picsTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `pics` WHERE assessment_id = $assessmentId and mother_id = $motherId and respondent_id = 3")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->keyConcate($result,'ac_PICS');
	}
	
	private function brestfeedingRetrieval($assessmentId,$motherId){
		$cols = $this->breastfeedingTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `breastfeeding` WHERE assessment_id = $assessmentId and mother_id = $motherId")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->keyConcate($result,'m_Breastfeeding_Self_Efficacy_Scale');
	}
	
	private function infantanthrosixthRetrieval($assessmentId,$motherId){
		$cols = $this->infantanthrosixthTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `infant_anthropomatric` WHERE assessment_id = $assessmentId and mother_id = $motherId")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->keyConcatei($result,'m_Infant_Anthropometric_Measure');
	}
	
	private function infantanthrotwelvethRetrieval($assessmentId,$motherId){
		$cols = $this->infantanthrotwelveTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `infant_anthropomatric` WHERE assessment_id = $assessmentId and mother_id = $motherId")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->keyConcatei($result,'m_Infant_Anthropometric_Measure');
	}
	
	private function infantanthrotwentyfourthRetrieval($assessmentId,$motherId){
		$cols = $this->infantanthrotwentyfourthTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `infant_anthropomatric` WHERE assessment_id = $assessmentId and mother_id = $motherId")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->keyConcatei($result,'m_Infant_Anthropometric_Measure');
	}
	
	
	
	private function maternalRetrieval($assessmentId,$motherId){
		$cols = $this->maternalTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `maternal` WHERE assessment_id = $assessmentId and mother_id = $motherId")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->keyConcate($result,'m_Mtrnl_hlth_chklst');
	}
	
	private function sharedcaregivingmotherRetrieval($assessmentId,$motherId){
		$cols = $this->sharedcaregivingTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `shared_caregiving` WHERE assessment_id = $assessmentId and mother_id = $motherId ")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->keyConcate($result,'m_Shared_Caregiving_Checklist');
	}
	
	private function sharedcaregivingacRetrieval($assessmentId,$motherId){
		$cols = $this->sharedcaredgivingacTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `shared_caregiving` WHERE assessment_id = $assessmentId and mother_id = $motherId ")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->keyConcate($result,'ac_Shared_Caregiving_Checklist');
	}
	
	private function childecbqmotherRetrieval($assessmentId,$motherId){
		$cols = $this->childecbqTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `child_ecbq` WHERE assessment_id = $assessmentId and mother_id = $motherId and respondent_id = 1")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->keyConcate($result,'m_ECBQ');
	}
	private function childecbqacRetrieval($assessmentId,$motherId){
		$cols = $this->childecbqTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `child_ecbq` WHERE assessment_id = $assessmentId and mother_id = $motherId and respondent_id = 3")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->keyConcate($result,'ac_ECBQ');
	}
	
	private function bitseaRetrieval($assessmentId,$motherId){
		$cols = $this->bitseaTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `bitsea` WHERE assessment_id = $assessmentId and mother_id = $motherId")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->keyConcate($result,'m_BITSEA');
	}
	
	private function cbclmotherRetrieval($assessmentId,$motherId){
		$cols = $this->cbclTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `cbcl` WHERE assessment_id = $assessmentId and mother_id = $motherId and respondent_id = 1")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->keyConcate($result,'m_CBCL');
	}
	
	private function cbclacRetrieval($assessmentId,$motherId){
		$cols = $this->cbclTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `cbcl` WHERE assessment_id = $assessmentId and mother_id = $motherId and respondent_id = 3")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->keyConcate($result,'ac_CBCL');
	}


	private function bitseaacRetrieval($assessmentId,$motherId){
		$cols = $this->bitseaTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `bitsea` WHERE assessment_id = $assessmentId and mother_id = $motherId and respondent_id = 3")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->keyConcate($result,'ac_BITSEA');
	}
	
	private function infanthealthRetrieval($assessmentId,$motherId){
		$cols = $this->infanthealthTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `infant_health` WHERE assessment_id = $assessmentId and mother_id = $motherId")->queryOne();
		
		$temp = $result;
		$bb = $result['AGE'] - 1;
		//var_dump($bb);exit;
		if(!$result)
		{
			$result = $cols;
			unset($result["hospitalization as '01'"]);
			unset($result["nicu_care as '02'"]);
			unset($result["neonatal_jaundice as '03'"]);
			unset($result["icu_care as '04'"]);
			unset($result["fevers as '05'"]);
			unset($result["exanthaemotous_fever as '06'"]);
			unset($result["seizures as '07'"]);
			unset($result["asthma as '08'"]);
			unset($result["respiratory_illness as '09'"]);
			unset($result["diarrhoea as '10'"]);
			unset($result["other_details as '12'"]);
		}
		$fields = ['01' => 35,'02'=>3,'03'=>3,'04'=>35,'05'=>35,'06'=>35,'07'=>35,'08'=>35,'09'=>35,'10'=>35,'12'=>35];
		foreach($fields as $key => $value)
		{
			$val = null;
			if($temp)
			{
				$val = $result[$key];
			}
			if($val)
			{
				$val = explode(",",$val);
			}
			else
			{
				$val = [];
			}
			unset($result[$key]);
			for($i = 0;$i < $value;$i++)
			{
				$in = $i+1;
				$in = (strlen($in)>1)?$in:"0".$in;
				$index = $key."_".($in).'mnth';
				if($i <=$bb)
				{
				if(in_array($i+1,$val))
				{
					$result[$index] = 1;
				}
				else
				{
					$result[$index] = 0;
				}
			}else
			{
				if(in_array($i+1,$val))
				{
					$result[$index] = 1;
				}
				else
				{
					$result[$index] = -10;
				}
			}
			}
		}
		
		$result = $this->keyConcatei($result,'m_Infant_Hlth_chklst_immun');
		ksort($result);
		return $result;
	}
	
	private function pbfRetrieval($assessmentId,$motherId){
		$cols = $this->pbfTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `parental_bonding_first` WHERE assessment_id = $assessmentId and mother_id = $motherId")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->keyConcate($result,'m_PBI');
	}
	private function pbsRetrieval($assessmentId,$motherId){
		$cols = $this->pbsTable();
		$colImp = implode(array_keys($cols),",");
		$result = Yii::$app->db->createCommand("SELECT $colImp FROM `parental_bonding_second` WHERE assessment_id = $assessmentId and mother_id = $motherId")->queryOne();
		if(!$result)
		{
			$result = $cols;
		}
		return $this->keyConcate($result,'m_PBI');
	}
	
	private function keyConcate($obj,$str){
		$temp = [];
		//ksort($obj);
		foreach($obj as $key => $value)
		{ 
			$index = strpos($key," as");
			if($index)
			{
				$keyValue = substr($key,$index+4);
			}
			else
			{
				$keyValue = $key;
			}
			if($value == null)
			{
				$value = -5;
			}
			$keyValue = str_replace("'","",$keyValue);
			$temp[$str."_".$keyValue] = str_replace(","," ",$value);
		}
		return $temp;
	}


	private function keyConcatei($obj,$str){
		$temp = [];
		//ksort($obj);
		foreach($obj as $key => $value)
		{ 
			$index = strpos($key," as");
			if($index)
			{
				$keyValue = substr($key,$index+4);
			}
			else
			{
				$keyValue = $key;
			}
			if($value == null)
			{
				$value = 0;
			}
			$keyValue = str_replace("'","",$keyValue);
			$temp[$str."_".$keyValue] = str_replace(","," ",$value);
		}
		return $temp;
	}


	private function SbqkeyConcate($obj,$str){
		$temp = [];
		//ksort($obj);
		foreach($obj as $key => $value)
		{ 
			$index = strpos($key," as");
			if($index)
			{
				$keyValue = substr($key,$index+4);
			}
			else
			{
				$keyValue = $key;
			}
			if($value == null)
			{
				$value = -10;
			}
			$keyValue = str_replace("'","",$keyValue);
			$temp[$str."_".$keyValue] = str_replace(","," ",$value);
		}
		return $temp;
	}

}
?>
