<?php

namespace app\classes;

use Yii;

class TableNames{
    
    public $mother = ["tableName" => "mother","columns" => "id as motherId,first_name,last_name, age, present_address, phone1, mother_code,dob, dateofreg, current_visit,delivery_date,age,no_of_family_members, total_family_income, duration_of_residence, no_of_years_of_marriage_years, no_of_years_of_marriage_months, gestation, paternal_age_at_conception, walking_duration, exercise_duration, yoga_duration, householdchores_duration, others_duration, yearof_delivery, gestation_age_of_delivery,
        permanent_address, domicile, religion, educational_qualification, occupation_Self, occupation_Spouse, socio_economic_status, marital_status, nature_of_marriage, family_type, spouse_substance_use, substance_type, placeofreg, next_appointment_ANC, gravida, parity, lmp, edd, tri_t1, tri_t2, tri_t3, medication_prior_pregnancy, medication, prescribed_medication_during_pregnancy, medication_during_pregnancy, fertility_treatment_past, fertility_treatment, planned_pregnancy, self_reaction_for_pregnancy, husband_reaction_for_pregnancy, family_reaction_for_pregnancy, sleep_disturbance, physical_activity, others_activity, risk_identified_for_pregnancy, risk_identified_in_foetus, delivery, mode_of_delivery, complications_during_pregnancy, complications_in_baby, complications_in_mother, risk_identified_in_foetus_past, abortions_miscarrieges, medical_surgical_termination_related_complication, contraception, experience_in_previouspregnancy_postdelivery, specify_experience, pap_test, lactation_related_problems, baby_ability_to_suck, production_of_breast_milk, congenital_anomolies, special_children, mental_illness, postpartum_mental_health_issues, postpartum_mental_health_deatails,
         phone3,phone4"];
     
   // public $member =["tableNames"=>"member","columns"=>""]   
    public $assessment = ["tableName" => "assessment","columns" => "id as assessmentId,respondent_id,respondent,relation_type,assessment_type"];

    public $wooleyDepression = ["tableName" => "WooleyDepression","columns" => ["depressed_pastmonth" => null,"little_interest_pleasure_pastmonth" => null,"women_yes_needhelp" => null,"score" => null]];

    public $edinburghPostnatalDepression = ["tableName"=> "EdinburghPostnatalDepression" ,"columns"=>["feeling_past_7days_laugh" =>null,"feeling_past_7days_enjoyment" =>null,"feeling_past_7days_blamingmyself" =>null,"feeling_past_7days_anxious_worried" =>null,"feeling_past_7days_scared_panicky" =>null,"feeling_past_7days_things_getting_topofme" =>null,"feeling_past_7days_unhappy_difficulty_sleeping" =>null,"feeling_past_7days_sad_miserable" =>null,"feeling_past_7days_unhappy_crying" =>null,"feeling_past_7days_harming_myself" =>null,"score"=>null]];
    
    public $babyLikesDislikes=["tableNames"=>"babyLikesDislikes","columns"=>["fussing_crying" =>null, "toss_cot" =>null, "fuss_immediately" =>null, "play_quitely" =>null, "cry_someone_didnt_come" =>null, "angry" =>null, "contented_corner" =>null, "cry_before_nap" =>null, "lie_sit_quietly" =>null, "squirm" =>null, "fuss_facewash" =>null, "fuss_hairwash" =>null, "cry_removed_playrhing" =>null, "not_bothered_removed_plaything" =>null, "protest" =>null, "fuss_back" =>null, "upset_wanted_something" =>null, "tantrums_wanted_something" =>null, "distress_infant_seat" =>null, "cry_parents_chane_appearence" =>null, "startle_sudden_change" =>null, "startle_noise" =>null, "cling_parent_unfamiliar_person" =>null, "refuse_unfamiliar_person" =>null, "hang_back_unfamiliar_person" =>null, "never_warmup_unfamiliar_person" =>null, "cling_several_unfamiliar_persons" =>null, "cry_unfamiliar_adults" =>null, "upset_morethan_10min" =>null, "distress_new_place" =>null, "upset_new_place" =>null, "allow_unfamiliar_person_pickup" =>null, "distress_outside" =>null, "cry_taken_outside" =>null, "cry_unfamilar_person_pickup" =>null, "score"=>null]];
    
    public $bitsea =["tableNames"=>"bitsea","columns"=>["happy_won" =>null, "hurt_himself" =>null, "stress_fear" =>null, "cant_sit_quitely" =>null, "follows_rules" =>null, "wakeup_midnight" =>null, "cry" =>null, "animal_fear" =>null, "less_enjoy" =>null, "ifsad_searches_parents" =>null, "cry_left" =>null, "thinks" =>null, "looked_on_call" =>null, "didnt_respond_pain" =>null, "happy_with_loved" =>null, "didnt_touch_things" =>null, "problem_in_sleep" =>null, "runs" =>null, "play_with_others" =>null, "concentrates" =>null, "problem_in_udjustment" =>null, "helps_others" =>null, "not_happy" =>null, "eating_problem" =>null, "imitates_sound" =>null, "rejects_food" =>null, "fights" =>null, "breaks_things" =>null, "shows_fingers" =>null, "beats_bites" =>null, "plays_with_toys" =>null, "sad" =>null, "hurts_parents" =>null, "didnt_move_angry" =>null, "keeps_things" =>null, "repeats_sounds" =>null, "repeats_work" =>null, "separate_from_others" =>null, "hurts_himself" =>null, "eats_nonhealthy" =>null, "feelings_about_baby" =>null, "feelings_about_language" =>null,"score" =>null]];

    public function getScaleTables(){
        
        return array_merge([$this->wooleyDepression],[$this->edinburghPostnatalDepression]);
        
    }

}
?>