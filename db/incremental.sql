/* 9/10/2018*/
ALTER TABLE `bchads`.`parental_bonding_second` 
ADD COLUMN `any_cousins` VARCHAR(45) NULL AFTER `any_sisters`;

/* 23-oct-2018*/
CREATE TABLE `error_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` int(11) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `log_time` int(11) DEFAULT NULL,
  `prefix` varchar(100) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=183 DEFAULT CHARSET=utf8;

/*24/10/18 */
CREATE TABLE `bchads`.`sync_log` (
  `machine_name` VARCHAR(45) NULL,
  `is_active` INT(1) NULL,);

