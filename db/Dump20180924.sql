CREATE DATABASE  IF NOT EXISTS `bchads` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `bchads`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: bchads
-- ------------------------------------------------------
-- Server version	5.7.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `assessment`
--

DROP TABLE IF EXISTS `assessment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assessment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mother_id` int(11) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `visit_date` timestamp(6) NULL DEFAULT NULL,
  `assessment_type` varchar(10) DEFAULT NULL,
  `respondent` varchar(45) DEFAULT NULL,
  `respondent_id` int(11) DEFAULT NULL,
  `relation_type` varchar(255) DEFAULT NULL,
  `relationship_id` int(11) DEFAULT NULL,
  `assessment_date` timestamp(6) NULL DEFAULT NULL,
  `place_of_assesment` varchar(45) DEFAULT NULL,
  `assessment_by` varchar(45) DEFAULT NULL,
  `caregiver` varchar(45) DEFAULT NULL,
  `relation_with_child` varchar(45) DEFAULT NULL,
  `assessment_date_ac` timestamp(6) NULL DEFAULT NULL,
  `place_of_assessment_ac` varchar(45) DEFAULT NULL,
  `assessment_by_ac` varchar(45) DEFAULT NULL,
  `is_there_acg` varchar(45) DEFAULT NULL,
  `specify_other` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20005 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assessment`
--

LOCK TABLES `assessment` WRITE;
/*!40000 ALTER TABLE `assessment` DISABLE KEYS */;
INSERT INTO `assessment` VALUES (20000,1,'2018-09-19 06:19:03','2018-09-19 06:19:03','superadmin',NULL,'m6',NULL,NULL,NULL,NULL,NULL,'','',NULL,'',NULL,'','','',''),(20001,4,'2018-09-19 06:25:34','2018-09-19 06:25:34','superadmin',NULL,'m6',NULL,NULL,NULL,NULL,NULL,'','',NULL,'',NULL,'','','',''),(20002,2,'2018-09-19 06:26:24','2018-09-19 06:26:24','superadmin',NULL,'m6',NULL,NULL,NULL,NULL,NULL,'','',NULL,'',NULL,'','','',''),(20003,3,'2018-09-19 06:28:14','2018-09-19 06:28:14','superadmin',NULL,'m6',NULL,NULL,NULL,NULL,NULL,'','',NULL,'',NULL,'','','',''),(20004,4,'2018-09-20 23:36:43','2018-09-20 23:36:43','superadmin',NULL,'m24',NULL,NULL,NULL,NULL,NULL,'','',NULL,'',NULL,'','','','');
/*!40000 ALTER TABLE `assessment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_assignment`
--

DROP TABLE IF EXISTS `auth_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_assignment`
--

LOCK TABLES `auth_assignment` WRITE;
/*!40000 ALTER TABLE `auth_assignment` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item`
--

DROP TABLE IF EXISTS `auth_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `rule_name` varchar(64) DEFAULT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `group_code` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item`
--

LOCK TABLES `auth_item` WRITE;
/*!40000 ALTER TABLE `auth_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item_child`
--

DROP TABLE IF EXISTS `auth_item_child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item_child`
--

LOCK TABLES `auth_item_child` WRITE;
/*!40000 ALTER TABLE `auth_item_child` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_item_child` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item_group`
--

DROP TABLE IF EXISTS `auth_item_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item_group` (
  `code` varchar(64) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item_group`
--

LOCK TABLES `auth_item_group` WRITE;
/*!40000 ALTER TABLE `auth_item_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_item_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_rule`
--

LOCK TABLES `auth_rule` WRITE;
/*!40000 ALTER TABLE `auth_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `baby_likes_dislikes`
--

DROP TABLE IF EXISTS `baby_likes_dislikes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `baby_likes_dislikes` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `respondent_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `fussing_crying` int(3) DEFAULT NULL,
  `toss_cot` int(3) DEFAULT NULL,
  `fuss_immediately` int(3) DEFAULT NULL,
  `play_quitely` int(3) DEFAULT NULL,
  `cry_someone_didnt_come` int(3) DEFAULT NULL,
  `angry` int(3) DEFAULT NULL,
  `contented_corner` int(3) DEFAULT NULL,
  `cry_before_nap` int(3) DEFAULT NULL,
  `lie_sit_quietly` int(3) DEFAULT NULL,
  `squirm` int(3) DEFAULT NULL,
  `fuss_facewash` int(3) DEFAULT NULL,
  `fuss_hairwash` int(3) DEFAULT NULL,
  `cry_removed_playrhing` int(3) DEFAULT NULL,
  `not_bothered_removed_plaything` int(3) DEFAULT NULL,
  `protest` int(3) DEFAULT NULL,
  `fuss_back` int(3) DEFAULT NULL,
  `upset_wanted_something` int(3) DEFAULT NULL,
  `tantrums_wanted_something` int(3) DEFAULT NULL,
  `distress_infant_seat` int(3) DEFAULT NULL,
  `cry_parents_chane_appearence` int(3) DEFAULT NULL,
  `startle_sudden_change` int(3) DEFAULT NULL,
  `startle_noise` int(3) DEFAULT NULL,
  `cling_parent_unfamiliar_person` int(3) DEFAULT NULL,
  `refuse_unfamiliar_person` int(3) DEFAULT NULL,
  `hang_back_unfamiliar_person` int(3) DEFAULT NULL,
  `never_warmup_unfamiliar_person` int(3) DEFAULT NULL,
  `cling_several_unfamiliar_persons` int(3) DEFAULT NULL,
  `cry_unfamiliar_adults` int(3) DEFAULT NULL,
  `upset_morethan_10min` int(3) DEFAULT NULL,
  `distress_new_place` int(3) DEFAULT NULL,
  `upset_new_place` int(3) DEFAULT NULL,
  `allow_unfamiliar_person_pickup` int(3) DEFAULT NULL,
  `distress_outside` int(3) DEFAULT NULL,
  `cry_taken_outside` int(3) DEFAULT NULL,
  `cry_unfamilar_person_pickup` int(3) DEFAULT NULL,
  `score` int(3) DEFAULT NULL,
  `scale_id` int(11) DEFAULT NULL,
  `mother_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `baby_likes_dislikes`
--

LOCK TABLES `baby_likes_dislikes` WRITE;
/*!40000 ALTER TABLE `baby_likes_dislikes` DISABLE KEYS */;
INSERT INTO `baby_likes_dislikes` VALUES (1,1,20001,'2018-08-28 00:04:54','2018-08-28 00:05:04','superadmin',-11,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,15,4);
/*!40000 ALTER TABLE `baby_likes_dislikes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bitsea`
--

DROP TABLE IF EXISTS `bitsea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bitsea` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `respondent_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `happy_won` int(11) DEFAULT NULL,
  `hurt_himself` int(11) DEFAULT NULL,
  `stress_fear` int(11) DEFAULT NULL,
  `cant_sit_quitely` int(11) DEFAULT NULL,
  `follows_rules` int(11) DEFAULT NULL,
  `wakeup_midnight` int(11) DEFAULT NULL,
  `cry` int(11) DEFAULT NULL,
  `animal_fear` int(11) DEFAULT NULL,
  `less_enjoy` int(11) DEFAULT NULL,
  `ifsad_searches_parents` int(11) DEFAULT NULL,
  `cry_left` int(11) DEFAULT NULL,
  `thinks` int(11) DEFAULT NULL,
  `looked_on_call` int(11) DEFAULT NULL,
  `didnt_respond_pain` int(11) DEFAULT NULL,
  `happy_with_loved` int(11) DEFAULT NULL,
  `didnt_touch_things` int(11) DEFAULT NULL,
  `problem_in_sleep` int(11) DEFAULT NULL,
  `runs` int(11) DEFAULT NULL,
  `play_with_others` int(11) DEFAULT NULL,
  `concentrates` int(11) DEFAULT NULL,
  `problem_in_udjustment` int(11) DEFAULT NULL,
  `helps_others` int(11) DEFAULT NULL,
  `not_happy` int(11) DEFAULT NULL,
  `eating_problem` int(11) DEFAULT NULL,
  `imitates_sound` int(11) DEFAULT NULL,
  `rejects_food` int(11) DEFAULT NULL,
  `fights` int(11) DEFAULT NULL,
  `breaks_things` int(11) DEFAULT NULL,
  `shows_fingers` int(11) DEFAULT NULL,
  `beats_bites` int(11) DEFAULT NULL,
  `plays_with_toys` int(11) DEFAULT NULL,
  `sad` int(11) DEFAULT NULL,
  `hurts_parents` int(11) DEFAULT NULL,
  `didnt_move_angry` int(11) DEFAULT NULL,
  `keeps_things` int(11) DEFAULT NULL,
  `repeats_sounds` int(11) DEFAULT NULL,
  `repeats_work` int(11) DEFAULT NULL,
  `separate_from_others` int(11) DEFAULT NULL,
  `hurts_himself` int(11) DEFAULT NULL,
  `eats_nonhealthy` int(11) DEFAULT NULL,
  `feelings_about_baby` int(11) DEFAULT NULL,
  `feelings_about_language` int(11) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `scale_id` int(11) DEFAULT NULL,
  `see_straight` int(11) DEFAULT NULL,
  `physical_contact` int(11) DEFAULT NULL,
  `animal_fear_det` varchar(45) DEFAULT NULL,
  `repeats_sounds_det` varchar(45) DEFAULT NULL,
  `repeats_work_det` varchar(45) DEFAULT NULL,
  `hurts_himself_det` varchar(45) DEFAULT NULL,
  `eats_nonhealthy_det` varchar(45) DEFAULT NULL,
  `mother_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bitsea`
--

LOCK TABLES `bitsea` WRITE;
/*!40000 ALTER TABLE `bitsea` DISABLE KEYS */;
/*!40000 ALTER TABLE `bitsea` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `breastfeeding`
--

DROP TABLE IF EXISTS `breastfeeding`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `breastfeeding` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `respondent_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `determine_enough_milk` int(3) DEFAULT NULL,
  `successfully_cope` int(3) DEFAULT NULL,
  `without_formula` int(3) DEFAULT NULL,
  `lached_properly` int(3) DEFAULT NULL,
  `manage_situation` int(3) DEFAULT NULL,
  `manage_crying` int(3) DEFAULT NULL,
  `wanting_breastfeed` int(3) DEFAULT NULL,
  `comfortable_family_members` int(3) DEFAULT NULL,
  `satisfied_experience` int(3) DEFAULT NULL,
  `deal_time_consuming` int(3) DEFAULT NULL,
  `finish_1breast` int(3) DEFAULT NULL,
  `continue_feeding` int(3) DEFAULT NULL,
  `keepup_baby_demands` int(3) DEFAULT NULL,
  `tell_finished` int(3) DEFAULT NULL,
  `score` int(3) DEFAULT NULL,
  `scale_id` int(11) DEFAULT NULL,
  `mother_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `breastfeeding`
--

LOCK TABLES `breastfeeding` WRITE;
/*!40000 ALTER TABLE `breastfeeding` DISABLE KEYS */;
INSERT INTO `breastfeeding` VALUES (1,1,20001,'2018-08-28 00:04:40','2018-08-28 00:04:46','superadmin',-14,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,14,4);
/*!40000 ALTER TABLE `breastfeeding` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cbcl`
--

DROP TABLE IF EXISTS `cbcl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cbcl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `respondent_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `pain` varchar(45) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `behave_childish` varchar(45) DEFAULT NULL,
  `fear_for_doing_new_things` varchar(45) DEFAULT NULL,
  `far_eye_contact` varchar(45) DEFAULT NULL,
  `less_concentration` varchar(45) DEFAULT NULL,
  `patience_or_active` varchar(45) DEFAULT NULL,
  `impatience_for_displacing_object` varchar(45) DEFAULT NULL,
  `no_waiting_habit` varchar(45) DEFAULT NULL,
  `chewing_non_eatable_things` varchar(45) DEFAULT NULL,
  `depending_on_elders` varchar(45) DEFAULT NULL,
  `frequently_asks_help` varchar(45) DEFAULT NULL,
  `dont_poop` varchar(45) DEFAULT NULL,
  `cries_more` varchar(45) DEFAULT NULL,
  `cruel_for_animals` varchar(45) DEFAULT NULL,
  `perseverance` varchar(45) DEFAULT NULL,
  `finish_demands_suddenly` varchar(45) DEFAULT NULL,
  `destroys_his_or_her_belongings` varchar(45) DEFAULT NULL,
  `destroys_family_or_other_children_things` varchar(45) DEFAULT NULL,
  `loose_motion_or_Diarrhea` varchar(45) DEFAULT NULL,
  `doesnt_work_what_people_say` varchar(45) DEFAULT NULL,
  `irritation_for_change_of_regular_daily_plans` varchar(45) DEFAULT NULL,
  `doesnt_like_to_sleep_alone` varchar(45) DEFAULT NULL,
  `doesnt_respond_to_people` varchar(45) DEFAULT NULL,
  `doesnt_eats_well_explain` varchar(255) DEFAULT NULL,
  `doesnt_be_with_other_children` varchar(45) DEFAULT NULL,
  `behaves_like_elders_or_doesnt_enjoy` varchar(45) DEFAULT NULL,
  `doesnt_feels_bad_after_mischievous_things` varchar(45) DEFAULT NULL,
  `doesnt_like_to_go_outside` varchar(45) DEFAULT NULL,
  `easily_depressed` varchar(45) DEFAULT NULL,
  `easily_feels_jealous` varchar(45) DEFAULT NULL,
  `eat_or_drinks_non_eatable_food` varchar(45) DEFAULT NULL,
  `fear_for_certain_animals_at_certain_place_or_situation` varchar(45) DEFAULT NULL,
  `suddenly_hurts_his_or_her_feelings` varchar(45) DEFAULT NULL,
  `will_have_more_pain_or_accidented` varchar(45) DEFAULT NULL,
  `involves_in_many_fights` varchar(45) DEFAULT NULL,
  `interfere_in_every_matter` varchar(45) DEFAULT NULL,
  `feels_bad_when_he_or_she_is_away_from_their_parents` varchar(45) DEFAULT NULL,
  `disturbance_for_sleeping` varchar(45) DEFAULT NULL,
  `headache_without_medical_reasons` varchar(45) DEFAULT NULL,
  `hits_others` varchar(45) DEFAULT NULL,
  `holds_breath` varchar(45) DEFAULT NULL,
  `disturbing_animals_or_people_without_intention` varchar(45) DEFAULT NULL,
  `depressed_without_reason` varchar(45) DEFAULT NULL,
  `angry` varchar(45) DEFAULT NULL,
  `vomitting_sensation_or_unhealthy_without_medical_reason` varchar(45) DEFAULT NULL,
  `energyless_or_shewering_of_body_parts` varchar(45) DEFAULT NULL,
  `bad_dreams` varchar(45) DEFAULT NULL,
  `eats_more` varchar(45) DEFAULT NULL,
  `more_tired` varchar(45) DEFAULT NULL,
  `fears_without_reason` varchar(45) DEFAULT NULL,
  `pain_while_pooping_without_medical_reason` varchar(45) DEFAULT NULL,
  `suddenly_attacks_people_physically` varchar(45) DEFAULT NULL,
  `tears_skin_or_nose_or_other_body_parts` varchar(45) DEFAULT NULL,
  `plays_more_with_sex_organs` varchar(45) DEFAULT NULL,
  `doesnt_co_operate_or_doesnt_finishes_work_neatly` varchar(45) DEFAULT NULL,
  `eye_problem_without_medical_reason` varchar(45) DEFAULT NULL,
  `doesnt_changes_behaviour_even_after_punishment` varchar(45) DEFAULT NULL,
  `from_one_activity_to_another_changes_immediately` varchar(45) DEFAULT NULL,
  `itchyness_or_skin_problems_without_medical_reason` varchar(45) DEFAULT NULL,
  `disagrees_food` varchar(45) DEFAULT NULL,
  `disagrress_playing_more_active_games` varchar(45) DEFAULT NULL,
  `frequently_shakes_his_head_or_body_parts` varchar(45) DEFAULT NULL,
  `disagrees_to_sleep_at_night` varchar(45) DEFAULT NULL,
  `disagrees_toilet_coaching` varchar(45) DEFAULT NULL,
  `screeches_more` varchar(45) DEFAULT NULL,
  `looks_like_they_doesnt_respond_for_love_and_affection` varchar(45) DEFAULT NULL,
  `feels_shy_easily` varchar(45) DEFAULT NULL,
  `selfish_or_doesnt_share` varchar(45) DEFAULT NULL,
  `doesnt_show_love_for_people` varchar(45) DEFAULT NULL,
  `shows_little_interest_with_surrounding_things` varchar(45) DEFAULT NULL,
  `doesnt_fear_if_he_knows_it_will_pain_also` varchar(45) DEFAULT NULL,
  `feels_more_shy` varchar(45) DEFAULT NULL,
  `sleeps_less_in_day_night_when_compared_to_normal_children` varchar(45) DEFAULT NULL,
  `plays_with_recess_or_poop` varchar(45) DEFAULT NULL,
  `problem_in_speaking` varchar(45) DEFAULT NULL,
  `staring_continuosly_at_only_side_or_thinking_something_else` varchar(45) DEFAULT NULL,
  `stomach_ache_without_medical_reason` varchar(45) DEFAULT NULL,
  `change_in_feelings` varchar(45) DEFAULT NULL,
  `different_behaviour` varchar(255) DEFAULT NULL,
  `irritating_or_getting_angry` varchar(45) DEFAULT NULL,
  `sudden_difference_in_behaviour` varchar(45) DEFAULT NULL,
  `gets_more_angry` varchar(45) DEFAULT NULL,
  `speaks_or_cry_while_sleeping` varchar(45) DEFAULT NULL,
  `fusses_more` varchar(45) DEFAULT NULL,
  `cares_about_neatness` varchar(45) DEFAULT NULL,
  `fears_more` varchar(45) DEFAULT NULL,
  `feels_helpless_or_doesnt_listen_to_elders_words` varchar(45) DEFAULT NULL,
  `does_less_activities_or_walks_slowly` varchar(45) DEFAULT NULL,
  `not_happy_or_being_sad` varchar(45) DEFAULT NULL,
  `speaks_at_high_voice` varchar(45) DEFAULT NULL,
  `feels_sad_with_new_people_or_situation` varchar(255) DEFAULT NULL,
  `vomit` varchar(45) DEFAULT NULL,
  `frequently_wakesup_at_night` varchar(45) DEFAULT NULL,
  `walks_without_holding_hand` varchar(45) DEFAULT NULL,
  `wants_to_look_after_him_for_most_of_the_time` varchar(45) DEFAULT NULL,
  `murmuring` varchar(45) DEFAULT NULL,
  `feels_shy_or_doesnt_be_with_others` varchar(45) DEFAULT NULL,
  `thinks` varchar(45) DEFAULT NULL,
  `list_other_problems_if_he_or_she_has` varchar(45) DEFAULT NULL,
  `bites_other_children` varchar(45) DEFAULT NULL,
  `kicks_other_children` varchar(45) DEFAULT NULL,
  `helps_other_children` varchar(45) DEFAULT NULL,
  `beats_other_children` varchar(45) DEFAULT NULL,
  `share_with_other_children` varchar(45) DEFAULT NULL,
  `laughs_at_other_children` varchar(45) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `scale_id` int(11) DEFAULT NULL,
  `energyless_or_feared_person` int(11) DEFAULT NULL,
  `doesnt_eats_well_explain_det` varchar(45) DEFAULT NULL,
  `energyless_or_shewering_of_body_parts_det` varchar(45) DEFAULT NULL,
  `tears_skin_or_nose_or_other_body_parts_det` varchar(45) DEFAULT NULL,
  `eye_problem_without_medical_reason_det` varchar(45) DEFAULT NULL,
  `disagrees_toilet_coaching_det` varchar(45) DEFAULT NULL,
  `sleeps_less_in_day_night_when_compared_to_normal_children_det` varchar(45) DEFAULT NULL,
  `problem_in_speaking_det` varchar(45) DEFAULT NULL,
  `different_behaviour_det` varchar(45) DEFAULT NULL,
  `feels_sad_with_new_people_or_situation_det` varchar(45) DEFAULT NULL,
  `list_other_problems_if_he_or_she_has_det` varchar(45) DEFAULT NULL,
  `visheshachethana` int(11) DEFAULT NULL,
  `visheshachethana_det` varchar(45) DEFAULT NULL,
  `athiyada_kalaji` varchar(45) DEFAULT NULL,
  `mother_id` int(11) NOT NULL,
  `not_applicable` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cbcl`
--

LOCK TABLES `cbcl` WRITE;
/*!40000 ALTER TABLE `cbcl` DISABLE KEYS */;
INSERT INTO `cbcl` VALUES (1,3,20002,'2018-08-28 19:43:56','superadmin','1','2018-08-28 19:43:49','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',1,20,NULL,'','','','','','','','','','',NULL,'','',4,'');
/*!40000 ALTER TABLE `cbcl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `child_ecbq`
--

DROP TABLE IF EXISTS `child_ecbq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `child_ecbq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `respondent_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `publicplace_unfamiliarperson` int(11) DEFAULT NULL,
  `work_incomplete` int(11) DEFAULT NULL,
  `familiar_baby` int(11) DEFAULT NULL,
  `other_activities` int(11) DEFAULT NULL,
  `happy_sing` int(11) DEFAULT NULL,
  `play_outside` int(11) DEFAULT NULL,
  `toys1` int(11) DEFAULT NULL,
  `toys2` int(11) DEFAULT NULL,
  `like_elders` int(11) DEFAULT NULL,
  `simple_activity` int(11) DEFAULT NULL,
  `play_inside` int(11) DEFAULT NULL,
  `hug` int(11) DEFAULT NULL,
  `new_activity` int(11) DEFAULT NULL,
  `concentrate` int(11) DEFAULT NULL,
  `public_place` int(11) DEFAULT NULL,
  `play_out` int(11) DEFAULT NULL,
  `stop_work` int(11) DEFAULT NULL,
  `sad_cry` int(11) DEFAULT NULL,
  `sad_look` int(11) DEFAULT NULL,
  `walk_in_home` int(11) DEFAULT NULL,
  `new_toys` int(11) DEFAULT NULL,
  `sad_on_no` int(11) DEFAULT NULL,
  `patience` int(11) DEFAULT NULL,
  `slow_smile` int(11) DEFAULT NULL,
  `lap_sleep` int(11) DEFAULT NULL,
  `talks_familiar` int(11) DEFAULT NULL,
  `breakable_things` int(11) DEFAULT NULL,
  `dint_go_inside` int(11) DEFAULT NULL,
  `cry_morethan_3min` int(11) DEFAULT NULL,
  `fast_samadhana` int(11) DEFAULT NULL,
  `looks_other_activity` int(11) DEFAULT NULL,
  `plays_withdiff_people` int(11) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `scale_id` int(11) DEFAULT NULL,
  `lookedoncall` int(11) DEFAULT NULL,
  `dress_irrititate` int(11) DEFAULT NULL,
  `sound_irrititate` int(11) DEFAULT NULL,
  `active_after_sunset` int(11) DEFAULT NULL,
  `attention` int(11) DEFAULT NULL,
  `kirikiri_clothes` int(11) DEFAULT NULL,
  `kirikiri_sounds` int(11) DEFAULT NULL,
  `active_sunset` int(11) DEFAULT NULL,
  `mother_id` int(11) NOT NULL,
  `not_applicable` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `child_ecbq`
--

LOCK TABLES `child_ecbq` WRITE;
/*!40000 ALTER TABLE `child_ecbq` DISABLE KEYS */;
INSERT INTO `child_ecbq` VALUES (1,3,20002,'2018-08-28 19:43:25','2018-08-28 19:43:42','superadmin',-8,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,16,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,'');
/*!40000 ALTER TABLE `child_ecbq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `child_growth`
--

DROP TABLE IF EXISTS `child_growth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `child_growth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `respondent_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `length` varchar(45) DEFAULT NULL,
  `weight` varchar(45) DEFAULT NULL,
  `head_circumference` varchar(45) DEFAULT NULL,
  `chest_circumference` varchar(45) DEFAULT NULL,
  `mid_arm_circumference` varchar(45) DEFAULT NULL,
  `waist_circumference` varchar(45) DEFAULT NULL,
  `hip_circumference` varchar(45) DEFAULT NULL,
  `sub_capular_skin_fold_thickness` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `child_growth`
--

LOCK TABLES `child_growth` WRITE;
/*!40000 ALTER TABLE `child_growth` DISABLE KEYS */;
/*!40000 ALTER TABLE `child_growth` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conflict_tactics_scale`
--

DROP TABLE IF EXISTS `conflict_tactics_scale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conflict_tactics_scale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supporting_your_partner_at_difficult_times` int(11) DEFAULT NULL,
  `keeping_your_opinions_with_yourself` int(11) DEFAULT NULL,
  `damaged_things_because_of_angry_on_your_wife` int(11) DEFAULT NULL,
  `after_arguing_with_your_partner_did_u_shown_more_love` int(11) DEFAULT NULL,
  `knowingly_hiding_ur_partners_loved_things` int(11) DEFAULT NULL,
  `did_u_felt_sad_when_things_are_undone` int(11) DEFAULT NULL,
  `started_meeting_to_talk_in_all_angles` int(11) DEFAULT NULL,
  `intentionally_destroying_partners_personal_things` int(11) DEFAULT NULL,
  `disrespecting_ur_partner_infront_of_others` int(11) DEFAULT NULL,
  `listened_to_ur_partners_words_with_concentration` int(11) DEFAULT NULL,
  `locked_ur_partner_from_outside_home` int(11) DEFAULT NULL,
  `said_to_ur_partner_like_u_r_unable_to_study_or_work` int(11) DEFAULT NULL,
  `specified_ur_partners_position` int(11) DEFAULT NULL,
  `stopping_ur_partner_from_talking_with_relatives` int(11) DEFAULT NULL,
  `adjusting_with_different_opinions` int(11) DEFAULT NULL,
  `frequently_asking_did_u_understood_or_not` int(11) DEFAULT NULL,
  `restritcting_ur_partner_by_using_of_mobiles_or_vehicle` int(11) DEFAULT NULL,
  `threatning_ur_partner_for_disconnection_of_relationship` int(11) DEFAULT NULL,
  `by_doing_physical_work_did_u_reliefed` int(11) DEFAULT NULL,
  `tried_ur_relatives_against_ur_partner` int(11) DEFAULT NULL,
  `while_partner_solving_their_problem_did_u_suggested_ideas` int(11) DEFAULT NULL,
  `ordered_or_promised_ur_partner` int(11) DEFAULT NULL,
  `tried_to_solve_problems_together` int(11) DEFAULT NULL,
  `accepted_ur_faults` int(11) DEFAULT NULL,
  `feared_ur_partner` int(11) DEFAULT NULL,
  `treated_ur_partner_as_a_fool` int(11) DEFAULT NULL,
  `given_useful_ideas_to_ur_partner` int(11) DEFAULT NULL,
  `done_preplanned_work_with_angry` int(11) DEFAULT NULL,
  `tried_to_join_others_to_solve_ur_problems` int(11) DEFAULT NULL,
  `disrespected_ur_partner` int(11) DEFAULT NULL,
  `self_realised_what_u_said_to_ur_partner` int(11) DEFAULT NULL,
  `feared_ur_partner_to_throw_things_while_angry` int(11) DEFAULT NULL,
  `said_u_r_not_beautiful_to_partner` int(11) DEFAULT NULL,
  `agreed_urself_for_adjustment` int(11) DEFAULT NULL,
  `after_smoking_did_u_scolded_ur_partner` int(11) DEFAULT NULL,
  `if_ur_partner_disagreed_u_then_did_u_thrown_any_objects` int(11) DEFAULT NULL,
  `asked_sorry_after_arguing` int(11) DEFAULT NULL,
  `forcibly_closing_the_argument` int(11) DEFAULT NULL,
  `agreed_to_ur_partner_wanted_things` int(11) DEFAULT NULL,
  `supporting_your_partner_at_difficult_times1` int(11) DEFAULT NULL,
  `keeping_your_opinions_with_yourself1` int(11) DEFAULT NULL,
  `damaged_things_because_of_angry_on_your_wife1` int(11) DEFAULT NULL,
  `after_arguing_with_your_partner_did_u_shown_more_love1` int(11) DEFAULT NULL,
  `knowingly_hiding_ur_partners_loved_things1` int(11) DEFAULT NULL,
  `did_u_felt_sad_when_things_are_undone1` int(11) DEFAULT NULL,
  `intentionally_destroying_partners_personal_things1` int(11) DEFAULT NULL,
  `disrespecting_ur_partner_infront_of_others1` int(11) DEFAULT NULL,
  `listened_to_ur_partners_words_with_concentration1` int(11) DEFAULT NULL,
  `locked_ur_partner_from_outside_home1` int(11) DEFAULT NULL,
  `said_to_ur_partner_like_u_r_unable_to_study_or_work1` int(11) DEFAULT NULL,
  `specified_ur_partners_position1` int(11) DEFAULT NULL,
  `stopping_ur_partner_from_talking_with_relatives1` int(11) DEFAULT NULL,
  `adjusting_with_different_opinions1` int(11) DEFAULT NULL,
  `frequently_asking_did_u_understood_or_not1` int(11) DEFAULT NULL,
  `restritcting_ur_partner_by_using_of_mobiles_or_vehicle1` int(11) DEFAULT NULL,
  `threatning_ur_partner_for_disconnection_of_relationship1` int(11) DEFAULT NULL,
  `by_doing_physical_work_did_u_reliefed1` int(11) DEFAULT NULL,
  `tried_ur_relatives_against_ur_partner1` int(11) DEFAULT NULL,
  `while_partner_solving_their_problem_did_u_suggested_ideas1` int(11) DEFAULT NULL,
  `ordered_or_promised_ur_partner1` int(11) DEFAULT NULL,
  `tried_to_solve_problems_together1` int(11) DEFAULT NULL,
  `accepted_ur_faults1` int(11) DEFAULT NULL,
  `feared_ur_partner1` int(11) DEFAULT NULL,
  `treated_ur_partner_as_a_fool1` int(11) DEFAULT NULL,
  `given_useful_ideas_to_ur_partner1` int(11) DEFAULT NULL,
  `done_preplanned_work_with_angry1` int(11) DEFAULT NULL,
  `tried_to_join_others_to_solve_ur_problems1` int(11) DEFAULT NULL,
  `disrespected_ur_partner1` int(11) DEFAULT NULL,
  `self_realised_what_u_said_to_ur_partner1` int(11) DEFAULT NULL,
  `feared_ur_partner_to_throw_things_while_angry1` int(11) DEFAULT NULL,
  `said_u_r_not_beautiful_to_partner1` int(11) DEFAULT NULL,
  `agreed_urself_for_adjustment1` int(11) DEFAULT NULL,
  `after_smoking_did_u_scolded_ur_partner1` int(11) DEFAULT NULL,
  `if_ur_partner_disagreed_u_then_did_u_thrown_any_objects1` int(11) DEFAULT NULL,
  `asked_sorry_after_arguing1` int(11) DEFAULT NULL,
  `forcibly_closing_the_argument1` int(11) DEFAULT NULL,
  `agreed_to_ur_partner_wanted_things1` int(11) DEFAULT NULL,
  `rotated_ur_partner_hand` int(11) DEFAULT NULL,
  `grabed_and_pushed_ur_husband` int(11) DEFAULT NULL,
  `slapped_ur_husband` int(11) DEFAULT NULL,
  `forced_ur_partner_for_sex` int(11) DEFAULT NULL,
  `shaked_ur_partner_by_holding_tight` int(11) DEFAULT NULL,
  `tried_to_throw_ur_husband` int(11) DEFAULT NULL,
  `tried_to_throw_things_on_ur_partner` int(11) DEFAULT NULL,
  `tried_to_hold_ur_partners_neck` int(11) DEFAULT NULL,
  `tried_to_hit_kick_bite` int(11) DEFAULT NULL,
  `tried_to_hit_or_hitted` int(11) DEFAULT NULL,
  `hitted_ur_partner` int(11) DEFAULT NULL,
  `feared_ur_partner_by_gun_knife` int(11) DEFAULT NULL,
  `used_gun_knife` int(11) DEFAULT NULL,
  `rotated_ur_partner_hand1` int(11) DEFAULT NULL,
  `grabed_and_pushed_ur_husband1` int(11) DEFAULT NULL,
  `slapped_ur_husband1` int(11) DEFAULT NULL,
  `forced_ur_partner_for_sex1` int(11) DEFAULT NULL,
  `shaked_ur_partner_by_holding_tight1` int(11) DEFAULT NULL,
  `tried_to_throw_ur_husband1` int(11) DEFAULT NULL,
  `tried_to_throw_things_on_ur_partner1` int(11) DEFAULT NULL,
  `tried_to_hold_ur_partners_neck1` int(11) DEFAULT NULL,
  `tried_to_hit_kick_bite1` int(11) DEFAULT NULL,
  `tried_to_hit_or_hitted1` int(11) DEFAULT NULL,
  `hitted_ur_partner1` int(11) DEFAULT NULL,
  `feared_ur_partner_by_gun_knife1` int(11) DEFAULT NULL,
  `used_gun_knife1` int(11) DEFAULT NULL,
  `rotated_ur_partner_hand12` int(11) DEFAULT NULL,
  `grabed_and_pushed_ur_husband12` int(11) DEFAULT NULL,
  `slapped_ur_husband12` int(11) DEFAULT NULL,
  `forced_ur_partner_for_sex12` int(11) DEFAULT NULL,
  `shaked_ur_partner_by_holding_tight12` int(11) DEFAULT NULL,
  `tried_to_throw_ur_husband12` int(11) DEFAULT NULL,
  `tried_to_throw_things_on_ur_partner12` int(11) DEFAULT NULL,
  `tried_to_hold_ur_partners_neck12` int(11) DEFAULT NULL,
  `tried_to_hit_kick_bite12` int(11) DEFAULT NULL,
  `tried_to_hit_or_hitted12` int(11) DEFAULT NULL,
  `hitted_ur_partner12` int(11) DEFAULT NULL,
  `feared_ur_partner_by_gun_knife12` int(11) DEFAULT NULL,
  `used_gun_knife12` int(11) DEFAULT NULL,
  `rotated_ur_partner_hand123` int(11) DEFAULT NULL,
  `grabed_and_pushed_ur_husband123` int(11) DEFAULT NULL,
  `slapped_ur_husband123` int(11) DEFAULT NULL,
  `forced_ur_partner_for_sex123` int(11) DEFAULT NULL,
  `shaked_ur_partner_by_holding_tight123` int(11) DEFAULT NULL,
  `tried_to_throw_ur_husband123` int(11) DEFAULT NULL,
  `tried_to_throw_things_on_ur_partner123` int(11) DEFAULT NULL,
  `tried_to_hold_ur_partners_neck123` int(11) DEFAULT NULL,
  `tried_to_hit_kick_bite123` int(11) DEFAULT NULL,
  `tried_to_hit_or_hitted123` int(11) DEFAULT NULL,
  `hitted_ur_partner123` int(11) DEFAULT NULL,
  `feared_ur_partner_by_gun_knife123` int(11) DEFAULT NULL,
  `used_gun_knife123` int(11) DEFAULT NULL,
  `respondent_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `scale_id` int(11) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `rotated_ur_partner_hand_desc1` varchar(45) DEFAULT NULL,
  `rotated_ur_partner_hand_desc2` varchar(45) DEFAULT NULL,
  `rotated_ur_partner_hand_desc3` varchar(45) DEFAULT NULL,
  `grabed_and_pushed_ur_husband_desc1` varchar(45) DEFAULT NULL,
  `grabed_and_pushed_ur_husband_desc2` varchar(45) DEFAULT NULL,
  `grabed_and_pushed_ur_husband_desc3` varchar(45) DEFAULT NULL,
  `slapped_ur_husband_desc1` varchar(45) DEFAULT NULL,
  `slapped_ur_husband_desc2` varchar(45) DEFAULT NULL,
  `slapped_ur_husband_desc3` varchar(45) DEFAULT NULL,
  `forced_ur_partner_for_sex_desc1` varchar(45) DEFAULT NULL,
  `forced_ur_partner_for_sex_desc2` varchar(45) DEFAULT NULL,
  `forced_ur_partner_for_sex_desc3` varchar(45) DEFAULT NULL,
  `shaked_ur_partner_by_holding_tight_desc1` varchar(45) DEFAULT NULL,
  `shaked_ur_partner_by_holding_tight_desc2` varchar(45) DEFAULT NULL,
  `shaked_ur_partner_by_holding_tight_desc3` varchar(45) DEFAULT NULL,
  `tried_to_throw_ur_husband_desc1` varchar(45) DEFAULT NULL,
  `tried_to_throw_ur_husband_desc2` varchar(45) DEFAULT NULL,
  `tried_to_throw_ur_husband_desc3` varchar(45) DEFAULT NULL,
  `tried_to_throw_things_on_ur_partner_desc1` varchar(45) DEFAULT NULL,
  `tried_to_throw_things_on_ur_partner_desc2` varchar(45) DEFAULT NULL,
  `tried_to_throw_things_on_ur_partner_desc3` varchar(45) DEFAULT NULL,
  `tried_to_hold_ur_partners_neck_desc1` varchar(45) DEFAULT NULL,
  `tried_to_hold_ur_partners_neck_desc2` varchar(45) DEFAULT NULL,
  `tried_to_hold_ur_partners_neck_desc3` varchar(45) DEFAULT NULL,
  `tried_to_hit_kick_bite_desc1` varchar(45) DEFAULT NULL,
  `tried_to_hit_kick_bite_desc2` varchar(45) DEFAULT NULL,
  `tried_to_hit_kick_bite_desc3` varchar(45) DEFAULT NULL,
  `tried_to_hit_or_hitted_desc1` varchar(45) DEFAULT NULL,
  `tried_to_hit_or_hitted_desc2` varchar(45) DEFAULT NULL,
  `tried_to_hit_or_hitted_desc3` varchar(45) DEFAULT NULL,
  `hitted_ur_partner_desc1` varchar(45) DEFAULT NULL,
  `hitted_ur_partner_desc2` varchar(45) DEFAULT NULL,
  `hitted_ur_partner_desc3` varchar(45) DEFAULT NULL,
  `feared_ur_partner_by_gun_knife_desc1` varchar(45) DEFAULT NULL,
  `feared_ur_partner_by_gun_knife_desc2` varchar(45) DEFAULT NULL,
  `feared_ur_partner_by_gun_knife_desc3` varchar(45) DEFAULT NULL,
  `used_gun_knife_desc1` varchar(45) DEFAULT NULL,
  `used_gun_knife_desc2` varchar(45) DEFAULT NULL,
  `used_gun_knife_desc3` varchar(45) DEFAULT NULL,
  `rotated_ur_partner_hand1_desc1` varchar(45) DEFAULT NULL,
  `rotated_ur_partner_hand1_desc2` varchar(45) DEFAULT NULL,
  `rotated_ur_partner_hand1_desc3` varchar(45) DEFAULT NULL,
  `grabed_and_pushed_ur_husband1_desc1` varchar(45) DEFAULT NULL,
  `grabed_and_pushed_ur_husband1_desc2` varchar(45) DEFAULT NULL,
  `grabed_and_pushed_ur_husband1_desc3` varchar(45) DEFAULT NULL,
  `slapped_ur_husband1_desc1` varchar(45) DEFAULT NULL,
  `slapped_ur_husband1_desc2` varchar(45) DEFAULT NULL,
  `slapped_ur_husband1_desc3` varchar(45) DEFAULT NULL,
  `forced_ur_partner_for_sex1_desc1` varchar(45) DEFAULT NULL,
  `forced_ur_partner_for_sex1_desc2` varchar(45) DEFAULT NULL,
  `forced_ur_partner_for_sex1_desc3` varchar(45) DEFAULT NULL,
  `shaked_ur_partner_by_holding_tight1_desc1` varchar(45) DEFAULT NULL,
  `shaked_ur_partner_by_holding_tight1_desc2` varchar(45) DEFAULT NULL,
  `shaked_ur_partner_by_holding_tight1_desc3` varchar(45) DEFAULT NULL,
  `tried_to_throw_ur_husband1_desc1` varchar(45) DEFAULT NULL,
  `tried_to_throw_ur_husband1_desc2` varchar(45) DEFAULT NULL,
  `tried_to_throw_ur_husband1_desc3` varchar(45) DEFAULT NULL,
  `tried_to_throw_things_on_ur_partner1_desc1` varchar(45) DEFAULT NULL,
  `tried_to_throw_things_on_ur_partner1_desc2` varchar(45) DEFAULT NULL,
  `tried_to_throw_things_on_ur_partner1_desc3` varchar(45) DEFAULT NULL,
  `tried_to_hold_ur_partners_neck1_desc1` varchar(45) DEFAULT NULL,
  `tried_to_hold_ur_partners_neck1_desc2` varchar(45) DEFAULT NULL,
  `tried_to_hold_ur_partners_neck1_desc3` varchar(45) DEFAULT NULL,
  `tried_to_hit_kick_bite1_desc1` varchar(45) DEFAULT NULL,
  `tried_to_hit_kick_bite1_desc2` varchar(45) DEFAULT NULL,
  `tried_to_hit_kick_bite1_desc3` varchar(45) DEFAULT NULL,
  `tried_to_hit_or_hitted1_desc1` varchar(45) DEFAULT NULL,
  `tried_to_hit_or_hitted1_desc2` varchar(45) DEFAULT NULL,
  `tried_to_hit_or_hitted1_desc3` varchar(45) DEFAULT NULL,
  `hitted_ur_partner1_desc1` varchar(45) DEFAULT NULL,
  `hitted_ur_partner1_desc2` varchar(45) DEFAULT NULL,
  `hitted_ur_partner1_desc3` varchar(45) DEFAULT NULL,
  `feared_ur_partner_by_gun_knife1_desc1` varchar(45) DEFAULT NULL,
  `feared_ur_partner_by_gun_knife1_desc2` varchar(45) DEFAULT NULL,
  `feared_ur_partner_by_gun_knife1_desc3` varchar(45) DEFAULT NULL,
  `used_gun_knife1_desc1` varchar(45) DEFAULT NULL,
  `used_gun_knife1_desc2` varchar(45) DEFAULT NULL,
  `used_gun_knife1_desc3` varchar(45) DEFAULT NULL,
  `rotated_ur_partner_hand12_desc1` varchar(45) DEFAULT NULL,
  `rotated_ur_partner_hand12_desc2` varchar(45) DEFAULT NULL,
  `grabed_and_pushed_ur_husband12_desc1` varchar(45) DEFAULT NULL,
  `grabed_and_pushed_ur_husband12_desc2` varchar(45) DEFAULT NULL,
  `slapped_ur_husband12_desc1` varchar(45) DEFAULT NULL,
  `slapped_ur_husband12_desc2` varchar(45) DEFAULT NULL,
  `forced_ur_partner_for_sex12_desc1` varchar(45) DEFAULT NULL,
  `forced_ur_partner_for_sex12_desc2` varchar(45) DEFAULT NULL,
  `shaked_ur_partner_by_holding_tight12_desc1` varchar(45) DEFAULT NULL,
  `shaked_ur_partner_by_holding_tight12_desc2` varchar(45) DEFAULT NULL,
  `tried_to_throw_ur_husband12_desc1` varchar(45) DEFAULT NULL,
  `tried_to_throw_ur_husband12_desc2` varchar(45) DEFAULT NULL,
  `tried_to_throw_things_on_ur_partner12_desc1` varchar(45) DEFAULT NULL,
  `tried_to_throw_things_on_ur_partner12_desc2` varchar(45) DEFAULT NULL,
  `tried_to_hold_ur_partners_neck12_desc1` varchar(45) DEFAULT NULL,
  `tried_to_hold_ur_partners_neck12_desc2` varchar(45) DEFAULT NULL,
  `tried_to_hit_kick_bite12_desc1` varchar(45) DEFAULT NULL,
  `tried_to_hit_kick_bite12_desc2` varchar(45) DEFAULT NULL,
  `tried_to_hit_or_hitted12_desc1` varchar(45) DEFAULT NULL,
  `tried_to_hit_or_hitted12_desc2` varchar(45) DEFAULT NULL,
  `hitted_ur_partner12_desc1` varchar(45) DEFAULT NULL,
  `hitted_ur_partner12_desc2` varchar(45) DEFAULT NULL,
  `feared_ur_partner_by_gun_knife12_desc1` varchar(45) DEFAULT NULL,
  `feared_ur_partner_by_gun_knife12_desc2` varchar(45) DEFAULT NULL,
  `used_gun_knife12_desc1` varchar(45) DEFAULT NULL,
  `used_gun_knife12_desc2` varchar(45) DEFAULT NULL,
  `rotated_ur_partner_hand123_desc1` varchar(45) DEFAULT NULL,
  `rotated_ur_partner_hand123_desc2` varchar(45) DEFAULT NULL,
  `grabed_and_pushed_ur_husband123_desc1` varchar(45) DEFAULT NULL,
  `grabed_and_pushed_ur_husband123_desc2` varchar(45) DEFAULT NULL,
  `slapped_ur_husband123_desc1` varchar(45) DEFAULT NULL,
  `slapped_ur_husband123_desc2` varchar(45) DEFAULT NULL,
  `forced_ur_partner_for_sex123_desc1` varchar(45) DEFAULT NULL,
  `forced_ur_partner_for_sex123_desc2` varchar(45) DEFAULT NULL,
  `shaked_ur_partner_by_holding_tight123_desc1` varchar(45) DEFAULT NULL,
  `shaked_ur_partner_by_holding_tight123_desc2` varchar(45) DEFAULT NULL,
  `tried_to_throw_ur_husband123_desc1` varchar(45) DEFAULT NULL,
  `tried_to_throw_ur_husband123_desc2` varchar(45) DEFAULT NULL,
  `tried_to_throw_things_on_ur_partner123_desc1` varchar(45) DEFAULT NULL,
  `tried_to_throw_things_on_ur_partner123_desc2` varchar(45) DEFAULT NULL,
  `tried_to_hold_ur_partners_neck123_desc1` varchar(45) DEFAULT NULL,
  `tried_to_hold_ur_partners_neck123_desc2` varchar(45) DEFAULT NULL,
  `tried_to_hit_kick_bite123_desc1` varchar(45) DEFAULT NULL,
  `tried_to_hit_kick_bite123_desc2` varchar(45) DEFAULT NULL,
  `tried_to_hit_or_hitted123_desc1` varchar(45) DEFAULT NULL,
  `tried_to_hit_or_hitted123_desc2` varchar(45) DEFAULT NULL,
  `hitted_ur_partner123_desc1` varchar(45) DEFAULT NULL,
  `hitted_ur_partner123_desc2` varchar(45) DEFAULT NULL,
  `feared_ur_partner_by_gun_knife123_desc1` varchar(45) DEFAULT NULL,
  `feared_ur_partner_by_gun_knife123_desc2` varchar(45) DEFAULT NULL,
  `used_gun_knife123_desc1` varchar(45) DEFAULT NULL,
  `used_gun_knife123_desc2` varchar(45) DEFAULT NULL,
  `started_meeting_to_talk_in_all_angles1` int(11) DEFAULT NULL,
  `mother_id` int(11) NOT NULL,
  `not_applicable` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conflict_tactics_scale`
--

LOCK TABLES `conflict_tactics_scale` WRITE;
/*!40000 ALTER TABLE `conflict_tactics_scale` DISABLE KEYS */;
INSERT INTO `conflict_tactics_scale` VALUES (1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,1,20004,'2018-09-21 00:07:19','superadmin',4,12,'2018-09-20 23:45:35','','0','0','1','0','0','0','0','0','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',NULL,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','0','1','0','0','0',NULL,4,''),(2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2018-09-21 01:02:01','superadmin',1,12,'2018-09-21 00:12:58','','','','','','','','','','','','','','','','-10','','','','','','','','','','','','','','','','','','','','','','','','-10','-10','','','',NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',NULL,0,'');
/*!40000 ALTER TABLE `conflict_tactics_scale` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_backup_config`
--

DROP TABLE IF EXISTS `db_backup_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_backup_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL,
  `destination_folder` varchar(256) NOT NULL,
  `createdon` datetime NOT NULL,
  `updatedon` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_backup_config`
--

LOCK TABLES `db_backup_config` WRITE;
/*!40000 ALTER TABLE `db_backup_config` DISABLE KEYS */;
/*!40000 ALTER TABLE `db_backup_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_backup_history`
--

DROP TABLE IF EXISTS `db_backup_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_backup_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL,
  `result` varchar(32) NOT NULL,
  `backup_by` varchar(32) NOT NULL,
  `timestamp` varchar(32) NOT NULL,
  `duration` varchar(32) NOT NULL,
  `filename` varchar(256) NOT NULL,
  `location` varchar(256) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_backup_history`
--

LOCK TABLES `db_backup_history` WRITE;
/*!40000 ALTER TABLE `db_backup_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `db_backup_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edinburgh_postnatal_depression`
--

DROP TABLE IF EXISTS `edinburgh_postnatal_depression`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edinburgh_postnatal_depression` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `respondent_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `feeling_past_7days_laugh` int(4) DEFAULT NULL,
  `feeling_past_7days_enjoyment` int(4) DEFAULT NULL,
  `feeling_past_7days_blamingmyself` int(3) DEFAULT NULL,
  `feeling_past_7days_anxious_worried` int(3) DEFAULT NULL,
  `feeling_past_7days_scared_panicky` int(3) DEFAULT NULL,
  `feeling_past_7days_things_getting_topofme` int(3) DEFAULT NULL,
  `feeling_past_7days_unhappy_difficulty_sleeping` int(3) DEFAULT NULL,
  `feeling_past_7days_sad_miserable` int(3) DEFAULT NULL,
  `feeling_past_7days_unhappy_crying` int(3) DEFAULT NULL,
  `feeling_past_7days_harming_myself` int(3) DEFAULT NULL,
  `score` int(3) DEFAULT NULL,
  `scale_id` int(11) DEFAULT NULL,
  `mother_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edinburgh_postnatal_depression`
--

LOCK TABLES `edinburgh_postnatal_depression` WRITE;
/*!40000 ALTER TABLE `edinburgh_postnatal_depression` DISABLE KEYS */;
INSERT INTO `edinburgh_postnatal_depression` VALUES (1,1,1,'2018-01-24 04:00:24','2018-01-24 04:00:24','superadmin',0,1,0,2,1,1,1,NULL,2,NULL,8,2,0),(2,1,20000,'2018-08-20 07:05:58','2018-08-20 07:05:58','superadmin',0,0,3,0,NULL,2,3,1,2,3,14,2,3),(3,1,3,'2018-08-19 20:05:00','2018-08-19 20:05:13','superadmin',-9,0,3,1,3,1,1,1,3,3,16,2,2),(4,1,20001,'2018-08-27 21:44:56','2018-08-27 23:58:15','superadmin',-9,NULL,NULL,NULL,NULL,NULL,NULL,-9,0,NULL,0,2,4),(5,1,1,'2018-09-19 05:43:08','2018-09-19 06:05:58','superadmin',0,1,NULL,NULL,-10,NULL,NULL,NULL,NULL,NULL,1,2,1),(6,1,20000,'2018-09-20 05:47:16','2018-09-20 05:47:16','superadmin',3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,2,1);
/*!40000 ALTER TABLE `edinburgh_postnatal_depression` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `icmr`
--

DROP TABLE IF EXISTS `icmr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `icmr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `respondent_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `bad_words` varchar(45) DEFAULT NULL,
  `threatening` varchar(45) DEFAULT NULL,
  `threatening_sending_home` varchar(45) DEFAULT NULL,
  `send_home` varchar(45) DEFAULT NULL,
  `finance_prob` varchar(45) DEFAULT NULL,
  `fear_look` varchar(45) DEFAULT NULL,
  `avidheya` varchar(45) DEFAULT NULL,
  `udasinathe` varchar(45) DEFAULT NULL,
  `social_rights` varchar(45) DEFAULT NULL,
  `nirlakshya` varchar(45) DEFAULT NULL,
  `personal_needs` varchar(45) DEFAULT NULL,
  `decision_taking` varchar(45) DEFAULT NULL,
  `restrict` varchar(45) DEFAULT NULL,
  `harm` varchar(45) DEFAULT NULL,
  `harm_fire` varchar(45) DEFAULT NULL,
  `forced` varchar(45) DEFAULT NULL,
  `avoid` varchar(45) DEFAULT NULL,
  `wound` varchar(45) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `scale_id` int(11) DEFAULT NULL,
  `bad_words1` varchar(45) DEFAULT NULL,
  `bad_words2` varchar(45) DEFAULT NULL,
  `bad_words3` varchar(45) DEFAULT NULL,
  `bad_words4` varchar(45) DEFAULT NULL,
  `bad_words5` varchar(45) DEFAULT NULL,
  `bad_words6` varchar(45) DEFAULT NULL,
  `bad_words7` varchar(45) DEFAULT NULL,
  `bad_words8` varchar(45) DEFAULT NULL,
  `threatening1` varchar(45) DEFAULT NULL,
  `threatening2` varchar(45) DEFAULT NULL,
  `threatening3` varchar(45) DEFAULT NULL,
  `threatening4` varchar(45) DEFAULT NULL,
  `threatening5` varchar(45) DEFAULT NULL,
  `threatening6` varchar(45) DEFAULT NULL,
  `threatening7` varchar(45) DEFAULT NULL,
  `threatening8` varchar(45) DEFAULT NULL,
  `threatening_sending_home1` varchar(45) DEFAULT NULL,
  `threatening_sending_home2` varchar(45) DEFAULT NULL,
  `threatening_sending_home3` varchar(45) DEFAULT NULL,
  `threatening_sending_home4` varchar(45) DEFAULT NULL,
  `threatening_sending_home5` varchar(45) DEFAULT NULL,
  `threatening_sending_home6` varchar(45) DEFAULT NULL,
  `threatening_sending_home7` varchar(45) DEFAULT NULL,
  `threatening_sending_home8` varchar(45) DEFAULT NULL,
  `send_home1` varchar(45) DEFAULT NULL,
  `send_home2` varchar(45) DEFAULT NULL,
  `send_home3` varchar(45) DEFAULT NULL,
  `send_home4` varchar(45) DEFAULT NULL,
  `send_home5` varchar(45) DEFAULT NULL,
  `send_home6` varchar(45) DEFAULT NULL,
  `send_home7` varchar(45) DEFAULT NULL,
  `send_home8` varchar(45) DEFAULT NULL,
  `finance_prob1` varchar(45) DEFAULT NULL,
  `finance_prob2` varchar(45) DEFAULT NULL,
  `finance_prob3` varchar(45) DEFAULT NULL,
  `finance_prob4` varchar(45) DEFAULT NULL,
  `finance_prob5` varchar(45) DEFAULT NULL,
  `finance_prob6` varchar(45) DEFAULT NULL,
  `finance_prob7` varchar(45) DEFAULT NULL,
  `finance_prob8` varchar(45) DEFAULT NULL,
  `fear_look1` varchar(45) DEFAULT NULL,
  `fear_look2` varchar(45) DEFAULT NULL,
  `fear_look3` varchar(45) DEFAULT NULL,
  `fear_look4` varchar(45) DEFAULT NULL,
  `fear_look5` varchar(45) DEFAULT NULL,
  `fear_look6` varchar(45) DEFAULT NULL,
  `fear_look7` varchar(45) DEFAULT NULL,
  `fear_look8` varchar(45) DEFAULT NULL,
  `avidheya1` varchar(45) DEFAULT NULL,
  `avidheya2` varchar(45) DEFAULT NULL,
  `avidheya3` varchar(45) DEFAULT NULL,
  `avidheya4` varchar(45) DEFAULT NULL,
  `avidheya5` varchar(45) DEFAULT NULL,
  `avidheya6` varchar(45) DEFAULT NULL,
  `avidheya7` varchar(45) DEFAULT NULL,
  `avidheya8` varchar(45) DEFAULT NULL,
  `udasinathe1` varchar(45) DEFAULT NULL,
  `udasinathe2` varchar(45) DEFAULT NULL,
  `udasinathe3` varchar(45) DEFAULT NULL,
  `udasinathe4` varchar(45) DEFAULT NULL,
  `udasinathe5` varchar(45) DEFAULT NULL,
  `udasinathe6` varchar(45) DEFAULT NULL,
  `udasinathe7` varchar(45) DEFAULT NULL,
  `udasinathe8` varchar(45) DEFAULT NULL,
  `social_rights1` varchar(45) DEFAULT NULL,
  `social_rights2` varchar(45) DEFAULT NULL,
  `social_rights3` varchar(45) DEFAULT NULL,
  `social_rights4` varchar(45) DEFAULT NULL,
  `social_rights5` varchar(45) DEFAULT NULL,
  `social_rights6` varchar(45) DEFAULT NULL,
  `social_rights7` varchar(45) DEFAULT NULL,
  `social_rights8` varchar(45) DEFAULT NULL,
  `nirlakshya1` varchar(45) DEFAULT NULL,
  `nirlakshya2` varchar(45) DEFAULT NULL,
  `nirlakshya3` varchar(45) DEFAULT NULL,
  `nirlakshya4` varchar(45) DEFAULT NULL,
  `nirlakshya5` varchar(45) DEFAULT NULL,
  `nirlakshya6` varchar(45) DEFAULT NULL,
  `nirlakshya7` varchar(45) DEFAULT NULL,
  `nirlakshya8` varchar(45) DEFAULT NULL,
  `personal_needs1` varchar(45) DEFAULT NULL,
  `personal_needs2` varchar(45) DEFAULT NULL,
  `personal_needs3` varchar(45) DEFAULT NULL,
  `personal_needs4` varchar(45) DEFAULT NULL,
  `personal_needs5` varchar(45) DEFAULT NULL,
  `personal_needs6` varchar(45) DEFAULT NULL,
  `personal_needs7` varchar(45) DEFAULT NULL,
  `personal_needs8` varchar(45) DEFAULT NULL,
  `decision_taking1` varchar(45) DEFAULT NULL,
  `decision_taking2` varchar(45) DEFAULT NULL,
  `decision_taking3` varchar(45) DEFAULT NULL,
  `decision_taking4` varchar(45) DEFAULT NULL,
  `decision_taking5` varchar(45) DEFAULT NULL,
  `decision_taking6` varchar(45) DEFAULT NULL,
  `decision_taking7` varchar(45) DEFAULT NULL,
  `decision_taking8` varchar(45) DEFAULT NULL,
  `restrict1` varchar(45) DEFAULT NULL,
  `restrict2` varchar(45) DEFAULT NULL,
  `restrict3` varchar(45) DEFAULT NULL,
  `restrict4` varchar(45) DEFAULT NULL,
  `restrict5` varchar(45) DEFAULT NULL,
  `restrict6` varchar(45) DEFAULT NULL,
  `restrict7` varchar(45) DEFAULT NULL,
  `restrict8` varchar(45) DEFAULT NULL,
  `harm1` varchar(45) DEFAULT NULL,
  `harm2` varchar(45) DEFAULT NULL,
  `harm3` varchar(45) DEFAULT NULL,
  `harm4` varchar(45) DEFAULT NULL,
  `harm5` varchar(45) DEFAULT NULL,
  `harm6` varchar(45) DEFAULT NULL,
  `harm7` varchar(45) DEFAULT NULL,
  `harm8` varchar(45) DEFAULT NULL,
  `harm9` varchar(45) DEFAULT NULL,
  `harm_fire1` varchar(45) DEFAULT NULL,
  `harm_fire2` varchar(45) DEFAULT NULL,
  `harm_fire3` varchar(45) DEFAULT NULL,
  `harm_fire4` varchar(45) DEFAULT NULL,
  `harm_fire5` varchar(45) DEFAULT NULL,
  `harm_fire6` varchar(45) DEFAULT NULL,
  `harm_fire7` varchar(45) DEFAULT NULL,
  `harm_fire8` varchar(45) DEFAULT NULL,
  `harm_fire9` varchar(45) DEFAULT NULL,
  `forced1` varchar(45) DEFAULT NULL,
  `forced2` varchar(45) DEFAULT NULL,
  `forced3` varchar(45) DEFAULT NULL,
  `forced4` varchar(45) DEFAULT NULL,
  `forced5` varchar(45) DEFAULT NULL,
  `forced6` varchar(45) DEFAULT NULL,
  `forced7` varchar(45) DEFAULT NULL,
  `forced8` varchar(45) DEFAULT NULL,
  `avoid1` varchar(45) DEFAULT NULL,
  `avoid2` varchar(45) DEFAULT NULL,
  `avoid3` varchar(45) DEFAULT NULL,
  `avoid4` varchar(45) DEFAULT NULL,
  `avoid5` varchar(45) DEFAULT NULL,
  `avoid6` varchar(45) DEFAULT NULL,
  `avoid7` varchar(45) DEFAULT NULL,
  `avoid8` varchar(45) DEFAULT NULL,
  `wound1` varchar(45) DEFAULT NULL,
  `wound2` varchar(45) DEFAULT NULL,
  `wound3` varchar(45) DEFAULT NULL,
  `wound4` varchar(45) DEFAULT NULL,
  `wound5` varchar(45) DEFAULT NULL,
  `wound6` varchar(45) DEFAULT NULL,
  `wound7` varchar(45) DEFAULT NULL,
  `wound8` varchar(45) DEFAULT NULL,
  `mother_id` int(11) NOT NULL,
  `bad_words9` varchar(45) DEFAULT NULL,
  `bad_words10` varchar(45) DEFAULT NULL,
  `bad_words11` varchar(45) DEFAULT NULL,
  `threatening9` varchar(45) DEFAULT NULL,
  `threatening10` varchar(45) DEFAULT NULL,
  `threatening11` varchar(45) DEFAULT NULL,
  `threatening_sending_home9` varchar(45) DEFAULT NULL,
  `threatening_sending_home10` varchar(45) DEFAULT NULL,
  `threatening_sending_home11` varchar(45) DEFAULT NULL,
  `send_home9` varchar(45) DEFAULT NULL,
  `send_home10` varchar(45) DEFAULT NULL,
  `send_home11` varchar(45) DEFAULT NULL,
  `finance_prob9` varchar(45) DEFAULT NULL,
  `finance_prob10` varchar(45) DEFAULT NULL,
  `finance_prob11` varchar(45) DEFAULT NULL,
  `fear_look9` varchar(45) DEFAULT NULL,
  `fear_look10` varchar(45) DEFAULT NULL,
  `fear_look11` varchar(45) DEFAULT NULL,
  `avidheya9` varchar(45) DEFAULT NULL,
  `avidheya10` varchar(45) DEFAULT NULL,
  `avidheya11` varchar(45) DEFAULT NULL,
  `udasinathe9` varchar(45) DEFAULT NULL,
  `udasinathe10` varchar(45) DEFAULT NULL,
  `udasinathe11` varchar(45) DEFAULT NULL,
  `social_rights9` varchar(45) DEFAULT NULL,
  `social_rights10` varchar(45) DEFAULT NULL,
  `social_rights11` varchar(45) DEFAULT NULL,
  `nirlakshya9` varchar(45) DEFAULT NULL,
  `nirlakshya10` varchar(45) DEFAULT NULL,
  `nirlakshya11` varchar(45) DEFAULT NULL,
  `personal_needs10` varchar(45) DEFAULT NULL,
  `personal_needs9` varchar(45) DEFAULT NULL,
  `personal_needs11` varchar(45) DEFAULT NULL,
  `decision_taking9` varchar(45) DEFAULT NULL,
  `decision_taking10` varchar(45) DEFAULT NULL,
  `decision_taking11` varchar(45) DEFAULT NULL,
  `restrict9` varchar(45) DEFAULT NULL,
  `restrict10` varchar(45) DEFAULT NULL,
  `restrict11` varchar(45) DEFAULT NULL,
  `harm10` varchar(45) DEFAULT NULL,
  `harm11` varchar(45) DEFAULT NULL,
  `harm12` varchar(45) DEFAULT NULL,
  `harm_fire10` varchar(45) DEFAULT NULL,
  `harm_fire11` varchar(45) DEFAULT NULL,
  `harm_fire12` varchar(45) DEFAULT NULL,
  `forced9` varchar(45) DEFAULT NULL,
  `forced10` varchar(45) DEFAULT NULL,
  `forced11` varchar(45) DEFAULT NULL,
  `avoid9` varchar(45) DEFAULT NULL,
  `avoid10` varchar(45) DEFAULT NULL,
  `avoid11` varchar(45) DEFAULT NULL,
  `wound9` varchar(45) DEFAULT NULL,
  `wound10` varchar(45) DEFAULT NULL,
  `wound11` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `icmr`
--

LOCK TABLES `icmr` WRITE;
/*!40000 ALTER TABLE `icmr` DISABLE KEYS */;
INSERT INTO `icmr` VALUES (1,1,20001,'2018-08-28 00:03:13','2018-08-28 00:03:29','superadmin','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,NULL,NULL,1,11,'','','',NULL,'','',NULL,NULL,'','','',NULL,'','',NULL,NULL,'','','',NULL,'','',NULL,NULL,'','','',NULL,'','',NULL,NULL,'','','',NULL,'','',NULL,NULL,'','','',NULL,'','',NULL,NULL,'','','',NULL,'','',NULL,NULL,'','','',NULL,'','',NULL,NULL,'','','',NULL,'','',NULL,NULL,'','','',NULL,'','',NULL,NULL,'','','',NULL,'','',NULL,NULL,'','','',NULL,'','',NULL,NULL,'','','',NULL,'','',NULL,NULL,'','','',NULL,'','',NULL,NULL,'','','','',NULL,'','',NULL,NULL,'','','','',NULL,'','',NULL,NULL,'','','',NULL,'','',NULL,NULL,'','','',NULL,'','',NULL,NULL,4,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',NULL,'','','','','','','','','','','','','','','','','','');
/*!40000 ALTER TABLE `icmr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `infant_anthropomatric`
--

DROP TABLE IF EXISTS `infant_anthropomatric`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `infant_anthropomatric` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `respondent_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `length_8_weeks` float DEFAULT NULL,
  `length_6_months` float DEFAULT NULL,
  `length_1_year` float DEFAULT NULL,
  `length_2_year` float DEFAULT NULL,
  `weight_8_weeks` float DEFAULT NULL,
  `weight_6_months` float DEFAULT NULL,
  `weight_1_year` float DEFAULT NULL,
  `weight_2_year` float DEFAULT NULL,
  `head_circumference_8_weeks` float DEFAULT NULL,
  `head_circumference_6_months` float DEFAULT NULL,
  `head_circumference_1_year` float DEFAULT NULL,
  `head_circumference_2_year` float DEFAULT NULL,
  `chest_circumference_8_weeks` float DEFAULT NULL,
  `chest_circumference_6_months` float DEFAULT NULL,
  `chest_circumference_1_year` float DEFAULT NULL,
  `chest_circumference_2_year` float DEFAULT NULL,
  `midarm_circumference_8_weeks` float DEFAULT NULL,
  `midarm_circumference_6_months` float DEFAULT NULL,
  `midarm_circumference_1_year` float DEFAULT NULL,
  `midarm_circumference_2_year` float DEFAULT NULL,
  `waist_circumference_8_weeks` float DEFAULT NULL,
  `waist_circumference_6_months` float DEFAULT NULL,
  `waist_circumference_1_year` float DEFAULT NULL,
  `waist_circumference_2_year` float DEFAULT NULL,
  `hip_circumference_8_weeks` float DEFAULT NULL,
  `hip_circumference_6_months` float DEFAULT NULL,
  `hip_circumference_1_year` float DEFAULT NULL,
  `hip_circumference_2_year` float DEFAULT NULL,
  `sub_scapular_skin_fold_thickness_8_weeks` float DEFAULT NULL,
  `sub_scapular_skin_fold_thickness_6_months` float DEFAULT NULL,
  `sub_scapular_skin_fold_thickness_1_year` float DEFAULT NULL,
  `sub_scapular_skin_fold_thickness_2_year` float DEFAULT NULL,
  `score` int(2) DEFAULT NULL,
  `scale_id` int(11) DEFAULT NULL,
  `mother_id` int(11) DEFAULT NULL,
  `length_6_months_desc` varchar(45) DEFAULT NULL,
  `weight_6_months_desc` varchar(45) DEFAULT NULL,
  `head_circumference_6_months_desc` varchar(45) DEFAULT NULL,
  `chest_circumference_6_months_desc` varchar(45) DEFAULT NULL,
  `midarm_circumference_6_months_desc` varchar(45) DEFAULT NULL,
  `waist_circumference_6_months_desc` varchar(45) DEFAULT NULL,
  `hip_circumference_6_months_desc` varchar(45) DEFAULT NULL,
  `length_1_year_desc` varchar(45) DEFAULT NULL,
  `weight_1_year_desc` varchar(45) DEFAULT NULL,
  `head_circumference_1_year_desc` varchar(45) DEFAULT NULL,
  `chest_circumference_1_year_desc` varchar(45) DEFAULT NULL,
  `midarm_circumference_1_year_desc` varchar(45) DEFAULT NULL,
  `waist_circumference_1_year_desc` varchar(45) DEFAULT NULL,
  `hip_circumference_1_year_desc` varchar(45) DEFAULT NULL,
  `length_2_year_desc` varchar(45) DEFAULT NULL,
  `weight_2_year_desc` varchar(45) DEFAULT NULL,
  `head_circumference_2_year_desc` varchar(45) DEFAULT NULL,
  `chest_circumference_2_year_desc` varchar(45) DEFAULT NULL,
  `midarm_circumference_2_year_desc` varchar(45) DEFAULT NULL,
  `waist_circumference_2_year_desc` varchar(45) DEFAULT NULL,
  `hip_circumference_2_year_desc` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `infant_anthropomatric`
--

LOCK TABLES `infant_anthropomatric` WRITE;
/*!40000 ALTER TABLE `infant_anthropomatric` DISABLE KEYS */;
/*!40000 ALTER TABLE `infant_anthropomatric` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `infant_health`
--

DROP TABLE IF EXISTS `infant_health`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `infant_health` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `respondent_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `hospitalization` varchar(255) DEFAULT NULL,
  `nicu_care` varchar(255) DEFAULT NULL,
  `neonatal_jaundice` varchar(255) DEFAULT NULL,
  `icu_care` varchar(255) DEFAULT NULL,
  `fevers` varchar(255) DEFAULT NULL,
  `exanthaemotous_fever` varchar(255) DEFAULT NULL,
  `seizures` varchar(255) DEFAULT NULL,
  `asthma` varchar(255) DEFAULT NULL,
  `respiratory_illness` varchar(255) DEFAULT NULL,
  `diarrhoea` varchar(255) DEFAULT NULL,
  `pediatrician_visits` varchar(45) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `scale_id` int(11) DEFAULT NULL,
  `dod` date DEFAULT NULL,
  `ageofinfant` int(11) DEFAULT NULL,
  `others` varchar(255) DEFAULT NULL,
  `how_many_times_burnt` int(11) DEFAULT NULL,
  `how_old_was_she_while_burnt` int(11) DEFAULT NULL,
  `what_did_person_with_child_do_about_burnt` varchar(255) DEFAULT NULL,
  `how_many_times_dropped` int(11) DEFAULT NULL,
  `how_old_was_she_while_dropped` int(11) DEFAULT NULL,
  `what_did_person_with_child_do_about_dropp` varchar(255) DEFAULT NULL,
  `how_many_times_swallowed` int(11) DEFAULT NULL,
  `how_old_was_she_while_swallowed` int(11) DEFAULT NULL,
  `what_did_person_with_child_do_about_swallow` varchar(255) DEFAULT NULL,
  `how_many_times_others` int(11) DEFAULT NULL,
  `how_old_was_she_while_other` int(11) DEFAULT NULL,
  `what_did_person_with_child_do_about_other` varchar(255) DEFAULT NULL,
  `how_often_on_weekdays` int(11) DEFAULT NULL,
  `how_often_at_weekdays` int(11) DEFAULT NULL,
  `t1_5m` varchar(45) DEFAULT NULL,
  `t2_5m` varchar(45) DEFAULT NULL,
  `t3_5m` varchar(45) DEFAULT NULL,
  `t9m` varchar(45) DEFAULT NULL,
  `t16_24m` varchar(45) DEFAULT NULL,
  `mother_id` int(11) NOT NULL,
  `doc_visit1` varchar(45) DEFAULT NULL,
  `doc_visit2` varchar(45) DEFAULT NULL,
  `doc_visit3` varchar(45) DEFAULT NULL,
  `doc_visit4` varchar(45) DEFAULT NULL,
  `doc_visit5` varchar(45) DEFAULT NULL,
  `doc_visit6` varchar(45) DEFAULT NULL,
  `doc_visit7` varchar(45) DEFAULT NULL,
  `doc_visit8` varchar(45) DEFAULT NULL,
  `doc_visit9` varchar(45) DEFAULT NULL,
  `doc_visit10` varchar(45) DEFAULT NULL,
  `doc_visit11` varchar(45) DEFAULT NULL,
  `doc_visit12` varchar(45) DEFAULT NULL,
  `doc_visit13` varchar(45) DEFAULT NULL,
  `doc_visit14` varchar(45) DEFAULT NULL,
  `doc_visit15` varchar(45) DEFAULT NULL,
  `doc_visit16` varchar(45) DEFAULT NULL,
  `doc_visit17` varchar(45) DEFAULT NULL,
  `doc_visit18` varchar(45) DEFAULT NULL,
  `doc_visit19` varchar(45) DEFAULT NULL,
  `doc_visit20` varchar(45) DEFAULT NULL,
  `doc_visit21` varchar(45) DEFAULT NULL,
  `doc_visit22` varchar(45) DEFAULT NULL,
  `doc_visit23` varchar(45) DEFAULT NULL,
  `doc_visit24` varchar(45) DEFAULT NULL,
  `doc_visit25` varchar(45) DEFAULT NULL,
  `doc_visit26` varchar(45) DEFAULT NULL,
  `doc_visit27` varchar(45) DEFAULT NULL,
  `doc_visit28` varchar(45) DEFAULT NULL,
  `doc_visit29` varchar(45) DEFAULT NULL,
  `doc_visit30` varchar(45) DEFAULT NULL,
  `doc_visit31` varchar(45) DEFAULT NULL,
  `doc_visit32` varchar(45) DEFAULT NULL,
  `doc_visit33` varchar(45) DEFAULT NULL,
  `doc_visit34` varchar(45) DEFAULT NULL,
  `doc_visit35` varchar(45) DEFAULT NULL,
  `others_notes` varchar(45) DEFAULT NULL,
  `what_did_person_with_child_do_about_burnt_others` varchar(45) DEFAULT NULL,
  `what_did_person_with_child_do_about_dropp_others` varchar(45) DEFAULT NULL,
  `what_did_person_with_child_do_about_swallow_others` varchar(45) DEFAULT NULL,
  `what_did_person_with_child_do_about_other_others` varchar(45) DEFAULT NULL,
  `other_details` varchar(255) DEFAULT NULL,
  `how_old_was_she_while_burnt2` varchar(45) DEFAULT NULL,
  `how_old_was_she_while_burnt3` varchar(45) DEFAULT NULL,
  `how_old_was_she_while_dropped2` varchar(45) DEFAULT NULL,
  `how_old_was_she_while_dropped3` varchar(45) DEFAULT NULL,
  `how_old_was_she_while_swallowed2` varchar(45) DEFAULT NULL,
  `how_old_was_she_while_swallowed3` varchar(45) DEFAULT NULL,
  `how_old_was_she_while_other2` varchar(45) DEFAULT NULL,
  `how_old_was_she_while_other3` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `infant_health`
--

LOCK TABLES `infant_health` WRITE;
/*!40000 ALTER TABLE `infant_health` DISABLE KEYS */;
/*!40000 ALTER TABLE `infant_health` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `infant_immunization`
--

DROP TABLE IF EXISTS `infant_immunization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `infant_immunization` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `respondent_id` int(2) DEFAULT NULL,
  `assessment_id` int(2) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `delivery_date` timestamp(6) NULL DEFAULT NULL,
  `age` int(2) DEFAULT NULL,
  `hospitalization` int(2) DEFAULT NULL,
  `niuc_care` int(2) DEFAULT NULL,
  `neonatal_jaundice` int(2) DEFAULT NULL,
  `icu_care` int(2) DEFAULT NULL,
  `fevers` int(2) DEFAULT NULL,
  `exanthematous_fevers` int(2) DEFAULT NULL,
  `seizures` int(2) DEFAULT NULL,
  `asthma` int(2) DEFAULT NULL,
  `respiratory_illness` int(2) DEFAULT NULL,
  `diarrhoea` int(2) DEFAULT NULL,
  `no_of_visits` int(4) DEFAULT NULL,
  `burnt_times` int(3) DEFAULT NULL,
  `age_when_burnt` int(2) DEFAULT NULL,
  `what_person_did_burnt` int(2) DEFAULT NULL,
  `dropped_times` int(2) DEFAULT NULL,
  `age_when_dropped` int(2) DEFAULT NULL,
  `what_person_did_dropped` int(2) DEFAULT NULL,
  `swallow_times` int(2) DEFAULT NULL,
  `age_when_swallowed` int(2) DEFAULT NULL,
  `what_person_did_swallow` int(2) DEFAULT NULL,
  `accidents_times` int(2) DEFAULT NULL,
  `age_when_accident` int(2) DEFAULT NULL,
  `what_person_did_accident` int(2) DEFAULT NULL,
  `child_enclosedplace(smoking)_weekdays` int(2) DEFAULT NULL,
  `child_enclosedplace(smoking)_weekends` int(2) DEFAULT NULL,
  `vaccination_1.5_months` int(2) DEFAULT NULL,
  `vaccination_2.5_months` int(2) DEFAULT NULL,
  `vaccination_3.5_months` int(2) DEFAULT NULL,
  `vaccination_9_months` int(2) DEFAULT NULL,
  `vaccination_16-24_months` int(2) DEFAULT NULL,
  `score` int(3) DEFAULT NULL,
  `scale_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `infant_immunization`
--

LOCK TABLES `infant_immunization` WRITE;
/*!40000 ALTER TABLE `infant_immunization` DISABLE KEYS */;
/*!40000 ALTER TABLE `infant_immunization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `life_events`
--

DROP TABLE IF EXISTS `life_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `life_events` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `respondent_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `death_spouse` int(3) DEFAULT NULL,
  `divorce` int(3) DEFAULT NULL,
  `marital_separation` int(3) DEFAULT NULL,
  `death_family_member` int(3) DEFAULT NULL,
  `personal_injury` int(3) DEFAULT NULL,
  `marriage` int(3) DEFAULT NULL,
  `marital_reconcilation` int(3) DEFAULT NULL,
  `health_family_member` int(3) DEFAULT NULL,
  `change_financial_status` int(3) DEFAULT NULL,
  `death_close_friend` int(3) DEFAULT NULL,
  `change_argument_spouse` int(3) DEFAULT NULL,
  `large_loan` int(3) DEFAULT NULL,
  `trouble_in_laws` int(3) DEFAULT NULL,
  `spouse_work` int(3) DEFAULT NULL,
  `change_living_conditions` int(3) DEFAULT NULL,
  `change_working_hours` int(3) DEFAULT NULL,
  `change_residence` int(3) DEFAULT NULL,
  `major_health_family_member` int(3) DEFAULT NULL,
  `deterioration_finance` int(3) DEFAULT NULL,
  `conflict_parents` int(3) DEFAULT NULL,
  `argument_spouse` int(3) DEFAULT NULL,
  `deterioration_living_conditions` int(3) DEFAULT NULL,
  `abortion` int(3) DEFAULT NULL,
  `miscarriage` int(3) DEFAULT NULL,
  `diabetes` int(3) DEFAULT NULL,
  `pre_eclamptic` int(3) DEFAULT NULL,
  `eclampsia` int(3) DEFAULT NULL,
  `medication_exposure` int(3) DEFAULT NULL,
  `neonatal_death` int(3) DEFAULT NULL,
  `score` int(3) DEFAULT NULL,
  `scale_id` int(11) DEFAULT NULL,
  `mother_id` int(11) NOT NULL,
  `other_answeres_about_delivery` varchar(255) DEFAULT NULL,
  `noofyes` int(3) DEFAULT NULL,
  `not_app` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `life_events`
--

LOCK TABLES `life_events` WRITE;
/*!40000 ALTER TABLE `life_events` DISABLE KEYS */;
INSERT INTO `life_events` VALUES (1,1,20001,'2018-08-28 00:02:21','2018-08-28 00:02:35','superadmin',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,-14,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,-9,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,8,4,NULL,0,''),(2,1,20000,'2018-09-20 05:48:45','2018-09-20 05:49:53','superadmin',0,73,65,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,138,8,1,NULL,2,'YES'),(3,NULL,0,'2018-09-20 06:59:18','2018-09-19 20:57:11','superadmin',100,73,65,63,53,NULL,45,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,401,8,0,NULL,8,'NO'),(4,2,20003,'2018-09-20 07:18:51','2018-09-20 07:20:58','superadmin',NULL,0,65,0,53,NULL,NULL,NULL,-14,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,119,8,3,NULL,3,'YES'),(5,1,20003,'2018-09-20 07:27:15','2018-09-19 20:01:01','superadmin',100,73,65,63,53,50,45,44,38,37,35,31,29,28,25,20,20,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,756,8,3,NULL,17,'YES');
/*!40000 ALTER TABLE `life_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maternal`
--

DROP TABLE IF EXISTS `maternal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `maternal` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `respondent_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `physical_health_problems` int(3) DEFAULT NULL,
  `emotional_problem` int(3) DEFAULT NULL,
  `medicines_taken` int(3) DEFAULT NULL,
  `sleepin_pills` varchar(45) DEFAULT NULL,
  `anxiety_pills` varchar(45) DEFAULT NULL,
  `depression_pills` varchar(45) DEFAULT NULL,
  `sleep` int(5) DEFAULT NULL,
  `hours_of_sleep` int(6) DEFAULT NULL,
  `persistant_pain` int(3) DEFAULT NULL,
  `pregnant_aftr_firstborn` int(3) DEFAULT NULL,
  `last_mensural_date` timestamp(6) NULL DEFAULT NULL,
  `status_presentpregnancy` varchar(45) DEFAULT NULL,
  `dob_baby` timestamp(6) NULL DEFAULT NULL,
  `sex_baby` varchar(45) DEFAULT NULL,
  `score` int(3) DEFAULT NULL,
  `scale_id` int(11) DEFAULT NULL,
  `physical_health_problems_det` varchar(255) DEFAULT NULL,
  `emotional_problem_det` varchar(255) DEFAULT NULL,
  `medicines_taken_det` varchar(255) DEFAULT NULL,
  `persistant_pain_dur` varchar(45) DEFAULT NULL,
  `persistant_pain_site` varchar(45) DEFAULT NULL,
  `last_menstrual_date_dont_know` varchar(45) DEFAULT NULL,
  `mother_id` int(11) NOT NULL,
  `not_applicable` varchar(45) DEFAULT NULL,
  `sleepin_pills2` varchar(45) DEFAULT NULL,
  `sleepin_pills3` varchar(45) DEFAULT NULL,
  `depression_pills2` varchar(45) DEFAULT NULL,
  `depression_pills3` varchar(45) DEFAULT NULL,
  `anxiety_pills2` varchar(45) DEFAULT NULL,
  `anxiety_pills3` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maternal`
--

LOCK TABLES `maternal` WRITE;
/*!40000 ALTER TABLE `maternal` DISABLE KEYS */;
INSERT INTO `maternal` VALUES (1,1,20003,'2018-09-19 19:32:44','2018-09-19 19:33:53','superadmin',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,18,'','','','','',NULL,3,'NO',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `maternal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member`
--

DROP TABLE IF EXISTS `member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mother_id` int(11) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `relation_type` enum('Self','Infant','Spouse','CareGiver') NOT NULL,
  `contact1` varchar(20) DEFAULT NULL,
  `contact2` varchar(20) DEFAULT NULL,
  `is_active` int(1) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `age_months` varchar(45) DEFAULT NULL,
  `ag_relation` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member`
--

LOCK TABLES `member` WRITE;
/*!40000 ALTER TABLE `member` DISABLE KEYS */;
INSERT INTO `member` VALUES (1,1,'Jenny','','CareGiver','4659870098','',1,'2018-01-24 03:55:27',NULL,'superadmin',NULL,'',NULL,NULL),(2,1,'Macy','Thomas','Infant','','',1,'2018-01-24 03:56:33',NULL,'superadmin',NULL,'Female',NULL,NULL);
/*!40000 ALTER TABLE `member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mother`
--

DROP TABLE IF EXISTS `mother`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mother` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) DEFAULT NULL,
  `middle_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `age` int(3) DEFAULT NULL,
  `present_address` varchar(200) DEFAULT NULL,
  `phone1` varchar(20) DEFAULT NULL,
  `phone2` varchar(20) DEFAULT NULL,
  `permanent_address` varchar(200) DEFAULT NULL,
  `domicile` varchar(20) DEFAULT NULL,
  `religion` varchar(20) DEFAULT NULL,
  `educational_qualification` varchar(20) DEFAULT NULL,
  `occupation_Self` varchar(20) DEFAULT NULL,
  `occupation_Spouse` varchar(20) DEFAULT NULL,
  `socio_economic_status` varchar(20) DEFAULT NULL,
  `marital_status` varchar(20) DEFAULT NULL,
  `nature_of_marriage` varchar(20) DEFAULT NULL,
  `family_type` varchar(20) DEFAULT NULL,
  `no_of_family_members` int(20) DEFAULT NULL,
  `total_family_income` int(20) DEFAULT NULL,
  `spouse_substance_use` varchar(20) DEFAULT NULL,
  `substance_type` varchar(20) DEFAULT NULL,
  `duration_of_residence` int(20) DEFAULT NULL,
  `no_of_years_of_marriage_years` int(10) DEFAULT NULL,
  `no_of_years_of_marriage_months` int(10) DEFAULT NULL,
  `dateofreg` date DEFAULT NULL,
  `placeofreg` varchar(45) DEFAULT NULL,
  `current_visit` date DEFAULT NULL,
  `next_appointment_ANC` varchar(45) DEFAULT NULL,
  `gravida` varchar(45) DEFAULT NULL,
  `parity` varchar(45) DEFAULT NULL,
  `lmp` varchar(45) DEFAULT NULL,
  `edd` varchar(45) DEFAULT NULL,
  `tri_t1` varchar(45) DEFAULT NULL,
  `tri_t2` varchar(45) DEFAULT NULL,
  `tri_t3` varchar(45) DEFAULT NULL,
  `gestation` int(11) DEFAULT NULL,
  `paternal_age_at_conception` int(11) DEFAULT NULL,
  `medication_prior_pregnancy` varchar(45) DEFAULT NULL,
  `medication` varchar(45) DEFAULT NULL,
  `prescribed_medication_during_pregnancy` varchar(45) DEFAULT NULL,
  `medication_during_pregnancy` varchar(45) DEFAULT NULL,
  `fertility_treatment_past` varchar(45) DEFAULT NULL,
  `fertility_treatment` varchar(45) DEFAULT NULL,
  `planned_pregnancy` varchar(45) DEFAULT NULL,
  `self_reaction_for_pregnancy` varchar(45) DEFAULT NULL,
  `husband_reaction_for_pregnancy` varchar(45) DEFAULT NULL,
  `family_reaction_for_pregnancy` varchar(45) DEFAULT NULL,
  `sleep_disturbance` varchar(45) DEFAULT NULL,
  `physical_activity` varchar(45) DEFAULT NULL,
  `walking_duration` int(11) DEFAULT NULL,
  `exercise_duration` int(11) DEFAULT NULL,
  `yoga_duration` int(11) DEFAULT NULL,
  `householdchores_duration` int(11) DEFAULT NULL,
  `others_activity` varchar(45) DEFAULT NULL,
  `others_duration` int(11) DEFAULT NULL,
  `risk_identified_for_pregnancy` varchar(45) DEFAULT NULL,
  `risk_identified_in_foetus` varchar(45) DEFAULT NULL,
  `delivery` varchar(45) DEFAULT NULL,
  `yearof_delivery` int(11) DEFAULT NULL,
  `gestation_age_of_delivery` int(11) DEFAULT NULL,
  `mode_of_delivery` varchar(45) DEFAULT NULL,
  `complications_during_pregnancy` varchar(45) DEFAULT NULL,
  `complications_in_baby` varchar(45) DEFAULT NULL,
  `complications_in_mother` varchar(45) DEFAULT NULL,
  `risk_identified_in_foetus_past` varchar(45) DEFAULT NULL,
  `abortions_miscarrieges` varchar(45) DEFAULT NULL,
  `medical_surgical_termination_related_complication` varchar(45) DEFAULT NULL,
  `contraception` varchar(45) DEFAULT NULL,
  `experience_in_previouspregnancy_postdelivery` varchar(45) DEFAULT NULL,
  `specify_experience` varchar(45) DEFAULT NULL,
  `pap_test` varchar(45) DEFAULT NULL,
  `lactation_related_problems` varchar(45) DEFAULT NULL,
  `baby_ability_to_suck` varchar(45) DEFAULT NULL,
  `production_of_breast_milk` varchar(45) DEFAULT NULL,
  `congenital_anomolies` varchar(45) DEFAULT NULL,
  `special_children` varchar(45) DEFAULT NULL,
  `mental_illness` varchar(45) DEFAULT NULL,
  `postpartum_mental_health_issues` varchar(45) DEFAULT NULL,
  `postpartum_mental_health_deatails` varchar(45) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `delivery_date` date DEFAULT NULL,
  `mother_code` varchar(45) NOT NULL,
  `phone3` varchar(20) DEFAULT NULL,
  `phone4` varchar(20) DEFAULT NULL,
  `gender_baby` varchar(45) DEFAULT NULL,
  `assessment_date` date DEFAULT NULL,
  `age_baby` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='		';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mother`
--

LOCK TABLES `mother` WRITE;
/*!40000 ALTER TABLE `mother` DISABLE KEYS */;
INSERT INTO `mother` VALUES (1,'Mary',NULL,'Thomas',NULL,25,'2nd Main, 3rd Cross, New Nagar','9879879870',NULL,'2nd Main, 3rd Cross, New Nagar',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-01-24 03:54:00','2018-01-24 04:42:12','superadmin','2017-07-13','AC354678',NULL,NULL,NULL,NULL,NULL),(2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-03-26 22:34:11','2018-03-26 22:34:11','superadmin',NULL,'M02',NULL,NULL,NULL,NULL,NULL),(3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-20 07:04:04','2018-08-20 07:04:04','superadmin',NULL,'C01',NULL,NULL,'Male',NULL,NULL),(4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-19 21:47:54','2018-08-19 21:47:54','superadmin',NULL,'C101',NULL,NULL,'Female',NULL,NULL);
/*!40000 ALTER TABLE `mother` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `multidimentional`
--

DROP TABLE IF EXISTS `multidimentional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `multidimentional` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `respondent_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `special_person_need` int(3) DEFAULT NULL,
  `special_person_share` int(3) DEFAULT NULL,
  `family_help` int(3) DEFAULT NULL,
  `emotional_help_family` int(3) DEFAULT NULL,
  `special_person_comfort` int(3) DEFAULT NULL,
  `friend_help` int(3) DEFAULT NULL,
  `friends_things_go_wrong` int(3) DEFAULT NULL,
  `problems_family_talk` int(3) DEFAULT NULL,
  `friends_share` int(3) DEFAULT NULL,
  `special_person_feelings` int(3) DEFAULT NULL,
  `family_help_decision` int(3) DEFAULT NULL,
  `talk_problems_friends` int(3) DEFAULT NULL,
  `score` int(3) DEFAULT NULL,
  `scale_id` int(11) DEFAULT NULL,
  `mother_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `multidimentional`
--

LOCK TABLES `multidimentional` WRITE;
/*!40000 ALTER TABLE `multidimentional` DISABLE KEYS */;
INSERT INTO `multidimentional` VALUES (1,1,20001,'2018-08-28 00:02:55','2018-08-28 00:03:02','superadmin',NULL,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,10,4);
/*!40000 ALTER TABLE `multidimentional` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offline_data_sync`
--

DROP TABLE IF EXISTS `offline_data_sync`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offline_data_sync` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(256) DEFAULT NULL,
  `last_sync_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `updated_user_id` int(11) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offline_data_sync`
--

LOCK TABLES `offline_data_sync` WRITE;
/*!40000 ALTER TABLE `offline_data_sync` DISABLE KEYS */;
INSERT INTO `offline_data_sync` VALUES (1,'mother','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(2,'member','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(3,'assessment','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(4,'edinburgh_postnatal_depression','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(5,'psychosocial_assessment','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(6,'psychosocial_risk_factors','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(7,'phychological_violence','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(8,'suicide_questions','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(9,'stai','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(10,'somatic_symptoms','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(11,'shared_caregiving','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(12,'sass','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(13,'ptsd_checklist','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(14,'pics','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(15,'scores','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(16,'patient_health','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(17,'parental_bonding_mother','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(18,'parental_bonding_father','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(19,'other_aspects','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(20,'multidimentional','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(21,'maternal','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(22,'life_events','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(23,'infant_immunization','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(24,'infant_health','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(25,'infant_anthropomatric','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(26,'icmr','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(27,'conflict_tactics_scale','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(28,'cbcl','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(29,'child_growth','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(30,'child_ecbq','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(31,'breastfeeding','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(32,'bitsea','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14'),(33,'baby_likes_dislikes','2018-08-20 07:12:14.000000',0,'2018-08-20 07:12:14','2018-08-20 07:12:14');
/*!40000 ALTER TABLE `offline_data_sync` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `other_aspects`
--

DROP TABLE IF EXISTS `other_aspects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `other_aspects` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `respondent_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `supportive_partner` int(3) DEFAULT NULL,
  `opinions_yourself` int(3) DEFAULT NULL,
  `damaged_household` int(3) DEFAULT NULL,
  `affection` int(3) DEFAULT NULL,
  `disposed_item` int(3) DEFAULT NULL,
  `upset` int(3) DEFAULT NULL,
  `initiated_discussion` int(3) DEFAULT NULL,
  `damaged_clothes` int(3) DEFAULT NULL,
  `insulted_partner` int(3) DEFAULT NULL,
  `listened_carefully` int(3) DEFAULT NULL,
  `locked_outside` int(3) DEFAULT NULL,
  `told_couldnt_work` int(3) DEFAULT NULL,
  `stated_position` int(3) DEFAULT NULL,
  `stop_talking_family/friend` int(3) DEFAULT NULL,
  `flexible_difference_opininon` int(3) DEFAULT NULL,
  `repeated_point` int(3) DEFAULT NULL,
  `restricted_phone` int(3) DEFAULT NULL,
  `threats_leave` int(3) DEFAULT NULL,
  `cooled_physical_work` int(3) DEFAULT NULL,
  `turn_family_against` int(3) DEFAULT NULL,
  `helpful_advice` int(3) DEFAULT NULL,
  `ordered` int(3) DEFAULT NULL,
  `new_ways_dealing_problems` int(3) DEFAULT NULL,
  `admitted_faults` int(3) DEFAULT NULL,
  `frightened` int(3) DEFAULT NULL,
  `treated_stupid` int(3) DEFAULT NULL,
  `helpful_ideas` int(3) DEFAULT NULL,
  `revenge` int(3) DEFAULT NULL,
  `someone_help_settle` int(3) DEFAULT NULL,
  `ridiculed` int(3) DEFAULT NULL,
  `expressed_regret` int(3) DEFAULT NULL,
  `threatened_hit` int(3) DEFAULT NULL,
  `told_ugly` int(3) DEFAULT NULL,
  `compramise` int(3) DEFAULT NULL,
  `abusive` int(3) DEFAULT NULL,
  `thrown_things` int(3) DEFAULT NULL,
  `sorry` int(3) DEFAULT NULL,
  `agreement_settle_argument` int(3) DEFAULT NULL,
  `agreed_partner_wanted` int(3) DEFAULT NULL,
  `twist_arm` int(3) DEFAULT NULL,
  `pushed` int(3) DEFAULT NULL,
  `slapped` int(3) DEFAULT NULL,
  `forced_sex` int(3) DEFAULT NULL,
  `shaken` int(3) DEFAULT NULL,
  `thrown_bodily` int(3) DEFAULT NULL,
  `thrown_object` int(3) DEFAULT NULL,
  `chocked` int(3) DEFAULT NULL,
  `kicked` int(3) DEFAULT NULL,
  `hit_with_something` int(3) DEFAULT NULL,
  `beaten` int(3) DEFAULT NULL,
  `threatened_gun_knife` int(3) DEFAULT NULL,
  `used_gun_knife` int(3) DEFAULT NULL,
  `score` int(3) DEFAULT NULL,
  `you_your_partner` int(3) DEFAULT NULL,
  `period` int(3) DEFAULT NULL,
  `scale_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `other_aspects`
--

LOCK TABLES `other_aspects` WRITE;
/*!40000 ALTER TABLE `other_aspects` DISABLE KEYS */;
/*!40000 ALTER TABLE `other_aspects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parental_bonding_father`
--

DROP TABLE IF EXISTS `parental_bonding_father`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parental_bonding_father` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `talks_friendly` varchar(45) DEFAULT NULL,
  `dint_help_needed` varchar(45) DEFAULT NULL,
  `allowed_loved_work` varchar(45) DEFAULT NULL,
  `understands_pain` varchar(45) DEFAULT NULL,
  `loving` varchar(45) DEFAULT NULL,
  `allowed_own_decisions` varchar(45) DEFAULT NULL,
  `hates_growing` varchar(45) DEFAULT NULL,
  `stops_all_work` varchar(45) DEFAULT NULL,
  `dint_allow_alone` varchar(45) DEFAULT NULL,
  `happy_to_talk` varchar(45) DEFAULT NULL,
  `laughs` varchar(45) DEFAULT NULL,
  `treat_baby` varchar(45) DEFAULT NULL,
  `didnt_understand_needs` varchar(45) DEFAULT NULL,
  `decides_own` varchar(45) DEFAULT NULL,
  `dont_need_feel` varchar(45) DEFAULT NULL,
  `helps_in_sad` varchar(45) DEFAULT NULL,
  `didnt_talk_lot` varchar(45) DEFAULT NULL,
  `feels_dependent` varchar(45) DEFAULT NULL,
  `didnt_lookafter_me` varchar(45) DEFAULT NULL,
  `give_freedom` varchar(45) DEFAULT NULL,
  `allow_outside` varchar(45) DEFAULT NULL,
  `saves_more` varchar(45) DEFAULT NULL,
  `praises` varchar(45) DEFAULT NULL,
  `respondent_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `scale_id` int(11) DEFAULT NULL,
  `allow_style` int(11) DEFAULT NULL,
  `less_helps` int(11) DEFAULT NULL,
  `less_praise` int(11) DEFAULT NULL,
  `more_secure` int(11) DEFAULT NULL,
  `allow_decisions` int(11) DEFAULT NULL,
  `allowstyle` int(11) DEFAULT NULL,
  `less_books` int(11) DEFAULT NULL,
  `less_stydy` int(11) DEFAULT NULL,
  `family_burden` int(11) DEFAULT NULL,
  `show_olavu` int(11) DEFAULT NULL,
  `show_nirlakshya` int(11) DEFAULT NULL,
  `less_helpings` int(11) DEFAULT NULL,
  `less_undestnds` int(11) DEFAULT NULL,
  `less_praises` int(11) DEFAULT NULL,
  `high_safes` int(11) DEFAULT NULL,
  `allows_decionss` int(11) DEFAULT NULL,
  `allow_style_as_boys` int(11) DEFAULT NULL,
  `less_resources` int(11) DEFAULT NULL,
  `less_oportunity_study` int(11) DEFAULT NULL,
  `boon` int(11) DEFAULT NULL,
  `jokes_onme` int(11) DEFAULT NULL,
  `respects_me` int(11) DEFAULT NULL,
  `less_understands` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parental_bonding_father`
--

LOCK TABLES `parental_bonding_father` WRITE;
/*!40000 ALTER TABLE `parental_bonding_father` DISABLE KEYS */;
/*!40000 ALTER TABLE `parental_bonding_father` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parental_bonding_first`
--

DROP TABLE IF EXISTS `parental_bonding_first`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parental_bonding_first` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `talks_friendly_mother` varchar(45) DEFAULT NULL,
  `dint_help_needed_mother` varchar(45) DEFAULT NULL,
  `allowed_loved_work_mother` varchar(45) DEFAULT NULL,
  `emotional_mother` varchar(45) DEFAULT NULL,
  `understands_pain_mother` varchar(45) DEFAULT NULL,
  `loving_mother` varchar(45) DEFAULT NULL,
  `allowed_own_decisions_mother` varchar(45) DEFAULT NULL,
  `hates_growing_mother` varchar(45) DEFAULT NULL,
  `stops_all_work_mother` varchar(45) DEFAULT NULL,
  `dint_allow_alone_mother` varchar(45) DEFAULT NULL,
  `happy_to_talk_mother` varchar(45) DEFAULT NULL,
  `laughs_mother` varchar(45) DEFAULT NULL,
  `treat_baby_mother` varchar(45) DEFAULT NULL,
  `didnt_understand_needs_mother` varchar(45) DEFAULT NULL,
  `decides_own_mother` varchar(45) DEFAULT NULL,
  `dont_need_feel_mother` varchar(45) DEFAULT NULL,
  `helps_in_sad_mother` varchar(45) DEFAULT NULL,
  `didnt_talk_lot_mother` varchar(45) DEFAULT NULL,
  `feels_dependent_mother` varchar(45) DEFAULT NULL,
  `didnt_lookafter_mother` varchar(45) DEFAULT NULL,
  `give_freedom_mother` varchar(45) DEFAULT NULL,
  `allow_outside_mother` varchar(45) DEFAULT NULL,
  `saves_more_mother` varchar(45) DEFAULT NULL,
  `praises_mother` varchar(45) DEFAULT NULL,
  `allow_own_style_mother` varchar(45) DEFAULT NULL,
  `talks_friendly_father` varchar(45) DEFAULT NULL,
  `dint_help_needed_father` varchar(45) DEFAULT NULL,
  `allowed_loved_work_father` varchar(45) DEFAULT NULL,
  `emotional_father` varchar(45) DEFAULT NULL,
  `understands_pain_father` varchar(45) DEFAULT NULL,
  `loving_father` varchar(45) DEFAULT NULL,
  `allowed_own_decisions_father` varchar(45) DEFAULT NULL,
  `hates_growing_father` varchar(45) DEFAULT NULL,
  `stops_all_work_father` varchar(45) DEFAULT NULL,
  `dint_allow_alone_father` varchar(45) DEFAULT NULL,
  `happy_to_talk_father` varchar(45) DEFAULT NULL,
  `laughs_father` varchar(45) DEFAULT NULL,
  `treat_baby_father` varchar(45) DEFAULT NULL,
  `didnt_understand_needs_father` varchar(45) DEFAULT NULL,
  `decides_own_father` varchar(45) DEFAULT NULL,
  `dont_need_feel_father` varchar(45) DEFAULT NULL,
  `helps_in_sad_father` varchar(45) DEFAULT NULL,
  `didnt_talk_lot_father` varchar(45) DEFAULT NULL,
  `feels_dependent_father` varchar(45) DEFAULT NULL,
  `didnt_lookafter_father` varchar(45) DEFAULT NULL,
  `give_freedom_father` varchar(45) DEFAULT NULL,
  `allow_outside_father` varchar(45) DEFAULT NULL,
  `saves_more_father` varchar(45) DEFAULT NULL,
  `praises_father` varchar(45) DEFAULT NULL,
  `allow_own_style_father` int(11) DEFAULT NULL,
  `respondent_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `scale_id` int(11) DEFAULT NULL,
  `mother_id` int(11) NOT NULL,
  `not_applicable` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parental_bonding_first`
--

LOCK TABLES `parental_bonding_first` WRITE;
/*!40000 ALTER TABLE `parental_bonding_first` DISABLE KEYS */;
/*!40000 ALTER TABLE `parental_bonding_first` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parental_bonding_mother`
--

DROP TABLE IF EXISTS `parental_bonding_mother`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parental_bonding_mother` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `talks_friendly` varchar(45) DEFAULT NULL,
  `dint_help_needed` varchar(45) DEFAULT NULL,
  `allowed_loved_work` varchar(45) DEFAULT NULL,
  `understands_pain` varchar(45) DEFAULT NULL,
  `loving` varchar(45) DEFAULT NULL,
  `allowed_own_decisions` varchar(45) DEFAULT NULL,
  `hates_growing` varchar(45) DEFAULT NULL,
  `stops_all_work` varchar(45) DEFAULT NULL,
  `dint_allow_alone` varchar(45) DEFAULT NULL,
  `happy_to_talk` varchar(45) DEFAULT NULL,
  `laughs` varchar(45) DEFAULT NULL,
  `treat_baby` varchar(45) DEFAULT NULL,
  `didnt_understand_needs` varchar(45) DEFAULT NULL,
  `decides_own` varchar(45) DEFAULT NULL,
  `dont_need_feel` varchar(45) DEFAULT NULL,
  `helps_in_sad` varchar(45) DEFAULT NULL,
  `didnt_talk_lot` varchar(45) DEFAULT NULL,
  `feels_dependent` varchar(45) DEFAULT NULL,
  `didnt_lookafter_me` varchar(45) DEFAULT NULL,
  `give_freedom` varchar(45) DEFAULT NULL,
  `allow_outside` varchar(45) DEFAULT NULL,
  `saves_more` varchar(45) DEFAULT NULL,
  `praises` varchar(45) DEFAULT NULL,
  `respondent_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `scale_id` int(11) DEFAULT NULL,
  `allow_style` int(11) DEFAULT NULL,
  `less_helps` int(11) DEFAULT NULL,
  `less_praise` int(11) DEFAULT NULL,
  `more_secure` int(11) DEFAULT NULL,
  `allow_decisions` int(11) DEFAULT NULL,
  `allowstyle` int(11) DEFAULT NULL,
  `less_books` int(11) DEFAULT NULL,
  `less_stydy` int(11) DEFAULT NULL,
  `family_burden` int(11) DEFAULT NULL,
  `show_olavu` int(11) DEFAULT NULL,
  `show_nirlakshya` int(11) DEFAULT NULL,
  `less_helpings` int(11) DEFAULT NULL,
  `less_undestnds` int(11) DEFAULT NULL,
  `less_praises` int(11) DEFAULT NULL,
  `high_safes` int(11) DEFAULT NULL,
  `allows_decionss` int(11) DEFAULT NULL,
  `allow_style_as_boys` int(11) DEFAULT NULL,
  `less_resources` int(11) DEFAULT NULL,
  `less_oportunity_study` int(11) DEFAULT NULL,
  `boon` int(11) DEFAULT NULL,
  `jokes_onme` int(11) DEFAULT NULL,
  `respects_me` int(11) DEFAULT NULL,
  `less_understands` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parental_bonding_mother`
--

LOCK TABLES `parental_bonding_mother` WRITE;
/*!40000 ALTER TABLE `parental_bonding_mother` DISABLE KEYS */;
/*!40000 ALTER TABLE `parental_bonding_mother` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parental_bonding_second`
--

DROP TABLE IF EXISTS `parental_bonding_second`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parental_bonding_second` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `less_helps_mother` int(11) DEFAULT NULL,
  `less_understands_mother` int(11) DEFAULT NULL,
  `less_praise_mother` int(11) DEFAULT NULL,
  `more_secure_mother` int(11) DEFAULT NULL,
  `allow_decisions_mother` int(11) DEFAULT NULL,
  `allowstyle_mother` int(11) DEFAULT NULL,
  `less_books_mother` int(11) DEFAULT NULL,
  `less_stydy_mother` int(11) DEFAULT NULL,
  `family_burden_mother` int(11) DEFAULT NULL,
  `show_olavu_mother` int(11) DEFAULT NULL,
  `show_nirlakshya_mother` int(11) DEFAULT NULL,
  `less_helpings_mother` int(11) DEFAULT NULL,
  `less_undestnds_mother` int(11) DEFAULT NULL,
  `less_praises_mother` int(11) DEFAULT NULL,
  `high_safes_mother` int(11) DEFAULT NULL,
  `allows_decionss_mother` int(11) DEFAULT NULL,
  `allow_style_as_boys_mother` int(11) DEFAULT NULL,
  `less_resources_mother` int(11) DEFAULT NULL,
  `less_oportunity_study_mother` int(11) DEFAULT NULL,
  `boon_mother` int(11) DEFAULT NULL,
  `jokes_onme_mother` int(11) DEFAULT NULL,
  `respects_me_mother` int(11) DEFAULT NULL,
  `less_helps_father` int(11) DEFAULT NULL,
  `less_understands_father` int(11) DEFAULT NULL,
  `less_praise_father` int(11) DEFAULT NULL,
  `more_secure_father` int(11) DEFAULT NULL,
  `allow_decisions_father` int(11) DEFAULT NULL,
  `allowstyle_father` int(11) DEFAULT NULL,
  `less_books_father` int(11) DEFAULT NULL,
  `less_stydy_father` int(11) DEFAULT NULL,
  `family_burden_father` int(11) DEFAULT NULL,
  `show_olavu_father` int(11) DEFAULT NULL,
  `show_nirlakshya_father` int(11) DEFAULT NULL,
  `less_helpings_father` int(11) DEFAULT NULL,
  `less_undestnds_father` int(11) DEFAULT NULL,
  `less_praises_father` int(11) DEFAULT NULL,
  `high_safes_father` int(11) DEFAULT NULL,
  `allows_decionss_father` int(11) DEFAULT NULL,
  `allow_style_as_boys_father` int(11) DEFAULT NULL,
  `less_resources_father` int(11) DEFAULT NULL,
  `less_oportunity_study_father` int(11) DEFAULT NULL,
  `boon_father` int(11) DEFAULT NULL,
  `jokes_onme_father` int(11) DEFAULT NULL,
  `respects_me_father` int(11) DEFAULT NULL,
  `respondent_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `scale_id` int(11) DEFAULT NULL,
  `mother_id` int(11) NOT NULL,
  `no_of_brothers` varchar(45) DEFAULT NULL,
  `no_of_sisters` varchar(45) DEFAULT NULL,
  `no_of_cousins` varchar(45) DEFAULT NULL,
  `not_applicable` varchar(45) DEFAULT NULL,
  `any_siblings` varchar(45) DEFAULT NULL,
  `no_of_cousines_brothers` varchar(45) DEFAULT NULL,
  `any_sisters` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parental_bonding_second`
--

LOCK TABLES `parental_bonding_second` WRITE;
/*!40000 ALTER TABLE `parental_bonding_second` DISABLE KEYS */;
/*!40000 ALTER TABLE `parental_bonding_second` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient_health`
--

DROP TABLE IF EXISTS `patient_health`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient_health` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `respondent_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `little_interest_pleasure` int(3) DEFAULT NULL,
  `down_depressed_hopeless` int(3) DEFAULT NULL,
  `falling_sleep` int(3) DEFAULT NULL,
  `tired` int(3) DEFAULT NULL,
  `appetite_overeating` int(3) DEFAULT NULL,
  `bad_yourself` int(3) DEFAULT NULL,
  `trouble_concentrating` int(3) DEFAULT NULL,
  `moving_speaking_slowly` int(3) DEFAULT NULL,
  `thought_hurting_yourself` int(3) DEFAULT NULL,
  `anxiety_attack` int(3) DEFAULT NULL,
  `happen_before` int(3) DEFAULT NULL,
  `attack_suddenly` int(3) DEFAULT NULL,
  `attack_bother` int(3) DEFAULT NULL,
  `short_breath` int(3) DEFAULT NULL,
  `heart_race` int(3) DEFAULT NULL,
  `chest_pain` int(3) DEFAULT NULL,
  `sweat` int(3) DEFAULT NULL,
  `choking` int(3) DEFAULT NULL,
  `hot_flashes_chills` int(3) DEFAULT NULL,
  `nausea` int(3) DEFAULT NULL,
  `dizzy_unsteady` int(3) DEFAULT NULL,
  `tingling_numbness` int(3) DEFAULT NULL,
  `tremble` int(3) DEFAULT NULL,
  `afraid_dieing` int(3) DEFAULT NULL,
  `nervous` int(3) DEFAULT NULL,
  `restless` int(3) DEFAULT NULL,
  `tired_easily` int(3) DEFAULT NULL,
  `muscle_tension` int(3) DEFAULT NULL,
  `trouble_falling_staying_asleep` int(3) DEFAULT NULL,
  `troble_concentrating` int(3) DEFAULT NULL,
  `easily_annoyed` int(3) DEFAULT NULL,
  `difficulty_problem` int(3) DEFAULT NULL,
  `score` int(3) DEFAULT NULL,
  `scale_id` int(11) DEFAULT NULL,
  `mother_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient_health`
--

LOCK TABLES `patient_health` WRITE;
/*!40000 ALTER TABLE `patient_health` DISABLE KEYS */;
INSERT INTO `patient_health` VALUES (1,1,20000,'2018-08-20 07:06:07','2018-08-20 07:06:07','superadmin',NULL,NULL,1,NULL,1,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,3,3),(2,1,20001,'2018-08-27 20:08:44','2018-08-27 23:59:33','superadmin',-9,-9,1,2,2,2,NULL,-6,-10,1,1,-11,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,12,3,4),(3,1,20003,'2018-09-19 19:49:20','2018-09-19 19:58:43','superadmin',1,1,1,4,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,8,3,3);
/*!40000 ALTER TABLE `patient_health` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phychological_violence`
--

DROP TABLE IF EXISTS `phychological_violence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phychological_violence` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `respondent_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `insulted` int(3) DEFAULT NULL,
  `threatened` int(3) DEFAULT NULL,
  `threatened_parents` int(3) DEFAULT NULL,
  `sent_parent_house` int(3) DEFAULT NULL,
  `financial_hardships` int(3) DEFAULT NULL,
  `frightening_look` int(3) DEFAULT NULL,
  `proved_unfaithful` int(3) DEFAULT NULL,
  `indifference` int(3) DEFAULT NULL,
  `deprived_social_rights` int(3) DEFAULT NULL,
  `negleted` int(3) DEFAULT NULL,
  `denial_personal_needs` int(3) DEFAULT NULL,
  `non-involvement_decision_making` int(3) DEFAULT NULL,
  `restriction_mobility` int(3) DEFAULT NULL,
  `physical_assault` int(3) DEFAULT NULL,
  `scald_burnt` int(3) DEFAULT NULL,
  `coerced_sex` int(3) DEFAULT NULL,
  `denial_sex` int(3) DEFAULT NULL,
  `sexual_hurt` int(3) DEFAULT NULL,
  `score` int(3) DEFAULT NULL,
  `scale_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phychological_violence`
--

LOCK TABLES `phychological_violence` WRITE;
/*!40000 ALTER TABLE `phychological_violence` DISABLE KEYS */;
/*!40000 ALTER TABLE `phychological_violence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pics`
--

DROP TABLE IF EXISTS `pics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pics` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `respondent_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `like_touched` int(3) DEFAULT NULL,
  `like_talked` int(3) DEFAULT NULL,
  `like_sing` int(3) DEFAULT NULL,
  `like_leave` int(3) DEFAULT NULL,
  `like_hold` int(3) DEFAULT NULL,
  `like_put_down` int(3) DEFAULT NULL,
  `like_dummy` int(3) DEFAULT NULL,
  `suck_hands` int(3) DEFAULT NULL,
  `stroke_tummy` int(3) DEFAULT NULL,
  `stroke_back` int(3) DEFAULT NULL,
  `stroke_face` int(3) DEFAULT NULL,
  `stroke_arms` int(3) DEFAULT NULL,
  `pick_up` int(3) DEFAULT NULL,
  `talk` int(3) DEFAULT NULL,
  `cuddle` int(3) DEFAULT NULL,
  `rock` int(3) DEFAULT NULL,
  `kiss` int(3) DEFAULT NULL,
  `hold` int(3) DEFAULT NULL,
  `watch` int(3) DEFAULT NULL,
  `leave_lie_down` int(3) DEFAULT NULL,
  `score` int(3) DEFAULT NULL,
  `scale_id` int(11) DEFAULT NULL,
  `mother_id` int(11) NOT NULL,
  `best_of_likes` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pics`
--

LOCK TABLES `pics` WRITE;
/*!40000 ALTER TABLE `pics` DISABLE KEYS */;
INSERT INTO `pics` VALUES (1,1,20001,'2018-08-28 00:04:18','2018-08-28 00:04:32','superadmin',-9,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,-14,NULL,0,13,4,'');
/*!40000 ALTER TABLE `pics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `psychosocial_assessment`
--

DROP TABLE IF EXISTS `psychosocial_assessment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `psychosocial_assessment` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `respondent_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `financial_worries` int(3) DEFAULT NULL,
  `money_worries` int(3) DEFAULT NULL,
  `family_problems` int(3) DEFAULT NULL,
  `move_recently_future` int(3) DEFAULT NULL,
  `loss_loved` int(3) DEFAULT NULL,
  `current_pregnancy` int(3) DEFAULT NULL,
  `current_abuse` int(3) DEFAULT NULL,
  `problem_alcohol` int(3) DEFAULT NULL,
  `problem_work` int(3) DEFAULT NULL,
  `problem_friends` int(3) DEFAULT NULL,
  `overloaded` int(3) DEFAULT NULL,
  `psychosocial_assessmentcol` int(3) DEFAULT NULL,
  `score` int(3) DEFAULT NULL,
  `scale_id` int(11) DEFAULT NULL,
  `mother_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `psychosocial_assessment`
--

LOCK TABLES `psychosocial_assessment` WRITE;
/*!40000 ALTER TABLE `psychosocial_assessment` DISABLE KEYS */;
INSERT INTO `psychosocial_assessment` VALUES (1,1,20001,'2018-08-28 00:02:11','2018-08-28 00:02:15','superadmin',NULL,NULL,NULL,NULL,NULL,NULL,-8,NULL,NULL,NULL,NULL,NULL,0,7,4),(2,1,20000,'2018-09-20 05:40:04','2018-09-20 05:40:04','superadmin',NULL,NULL,NULL,NULL,NULL,1,2,NULL,NULL,NULL,NULL,NULL,3,7,1);
/*!40000 ALTER TABLE `psychosocial_assessment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `psychosocial_risk_factors`
--

DROP TABLE IF EXISTS `psychosocial_risk_factors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `psychosocial_risk_factors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `respondent_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `stress_issues` int(2) DEFAULT NULL,
  `lack_care` int(2) DEFAULT NULL,
  `violence_experience` int(2) DEFAULT NULL,
  `anxiety_issues` varchar(45) DEFAULT NULL,
  `mood_issues` varchar(45) DEFAULT NULL,
  `suicidal_ideation` varchar(45) DEFAULT NULL,
  `poor_pregnancy` varchar(45) DEFAULT NULL,
  `traumatic_events` varchar(45) DEFAULT NULL,
  `substance_use_spouse` int(2) DEFAULT NULL,
  `health_problem_of_familymember` int(2) DEFAULT NULL,
  `delivery_postpartum` varchar(45) DEFAULT NULL,
  `score` int(2) DEFAULT NULL,
  `scale_id` int(11) DEFAULT NULL,
  `mother_id` int(11) NOT NULL,
  `not_applicable` varchar(45) DEFAULT NULL,
  `financial` varchar(45) DEFAULT NULL,
  `work` varchar(45) DEFAULT NULL,
  `others` varchar(45) DEFAULT NULL,
  `spouse` varchar(45) DEFAULT NULL,
  `in_laws` varchar(45) DEFAULT NULL,
  `parents` varchar(45) DEFAULT NULL,
  `psychological` varchar(45) DEFAULT NULL,
  `physical` varchar(45) DEFAULT NULL,
  `sexual` varchar(45) DEFAULT NULL,
  `fear_about_pregnancy` varchar(45) DEFAULT NULL,
  `fear_about_childbirth` varchar(45) DEFAULT NULL,
  `general_worry` varchar(45) DEFAULT NULL,
  `feeling_low` varchar(45) DEFAULT NULL,
  `loss_of_interest` varchar(45) DEFAULT NULL,
  `crying_spells` varchar(45) DEFAULT NULL,
  `lifetime_attempt` varchar(45) DEFAULT NULL,
  `ideation_during_past_current_pregnancy` varchar(45) DEFAULT NULL,
  `postpartum` varchar(45) DEFAULT NULL,
  `neonatal_death` varchar(45) DEFAULT NULL,
  `still_birth` varchar(45) DEFAULT NULL,
  `any_congenital_anomalies` varchar(45) DEFAULT NULL,
  `death_of_close_one` varchar(45) DEFAULT NULL,
  `major_loss` varchar(45) DEFAULT NULL,
  `complications_related_to_labour_delivery_childbirth` varchar(45) DEFAULT NULL,
  `obstetric_trauma` varchar(45) DEFAULT NULL,
  `poor_obg_outcome` varchar(45) DEFAULT NULL,
  `gender_of_the_infant` varchar(45) DEFAULT NULL,
  `others_desc` varchar(45) DEFAULT NULL,
  `relation` varchar(45) DEFAULT NULL,
  `complications_related_to_labour_delivery_childbirth1` varchar(45) DEFAULT NULL,
  `complications_related_to_labour_delivery_childbirth2` varchar(45) DEFAULT NULL,
  `poor_obg_outcome1` varchar(45) DEFAULT NULL,
  `poor_obg_outcome2` varchar(45) DEFAULT NULL,
  `delivery_and_postpartum` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `psychosocial_risk_factors`
--

LOCK TABLES `psychosocial_risk_factors` WRITE;
/*!40000 ALTER TABLE `psychosocial_risk_factors` DISABLE KEYS */;
INSERT INTO `psychosocial_risk_factors` VALUES (1,2,20000,'2018-09-20 05:55:28','2018-09-20 05:55:38','superadmin',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,23,1,'YES',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,2,20003,'2018-09-20 06:05:17','2018-09-20 07:23:32','superadmin',NULL,1,1,'njhmkjm','nuymuy','muimmuym',' bfdbdfnb','yctytv',0,0,NULL,NULL,23,3,'',NULL,NULL,NULL,NULL,NULL,NULL,'1','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',''),(3,1,20001,'2018-09-20 07:12:44','2018-09-20 07:12:44','superadmin',NULL,1,1,'njhmkjm','nuymuy','muimmuym',' bfdbdfnb','yctytv',1,0,NULL,NULL,23,4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','',NULL,'','','','','','','');
/*!40000 ALTER TABLE `psychosocial_risk_factors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ptsd_checklist`
--

DROP TABLE IF EXISTS `ptsd_checklist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ptsd_checklist` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `respondent_id` int(11) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `intrusive_recollect` int(3) DEFAULT NULL,
  `flashbacks` int(3) DEFAULT NULL,
  `upset_remainders` int(3) DEFAULT NULL,
  `distressing_dreams` int(3) DEFAULT NULL,
  `physical_remainders` int(3) DEFAULT NULL,
  `avoid_thoughts` int(3) DEFAULT NULL,
  `avoid_remainders` int(3) DEFAULT NULL,
  `psychogenic_amnesia` int(3) DEFAULT NULL,
  `anhedonia` int(3) DEFAULT NULL,
  `estrangement_others` int(3) DEFAULT NULL,
  `psychic_numbing` int(3) DEFAULT NULL,
  `foreshortened_future` int(3) DEFAULT NULL,
  `sleep_difficulty` int(3) DEFAULT NULL,
  `irritability` int(3) DEFAULT NULL,
  `concentrattion_impaired` int(3) DEFAULT NULL,
  `hyper_vigilant` int(3) DEFAULT NULL,
  `exaggerated_startle` int(4) DEFAULT NULL,
  `score` int(3) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `scale_id` int(11) DEFAULT NULL,
  `mother_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ptsd_checklist`
--

LOCK TABLES `ptsd_checklist` WRITE;
/*!40000 ALTER TABLE `ptsd_checklist` DISABLE KEYS */;
INSERT INTO `ptsd_checklist` VALUES (1,1,'2018-08-28 00:02:42','2018-08-28 00:02:51','superadmin',-10,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,20001,9,4),(2,1,'2018-08-29 06:15:07','2018-08-29 06:16:26','superadmin',-7,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,20000,9,3);
/*!40000 ALTER TABLE `ptsd_checklist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relationship`
--

DROP TABLE IF EXISTS `relationship`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationship` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relationship`
--

LOCK TABLES `relationship` WRITE;
/*!40000 ALTER TABLE `relationship` DISABLE KEYS */;
INSERT INTO `relationship` VALUES (1,'mother',1),(2,'user',2),(3,'member',3);
/*!40000 ALTER TABLE `relationship` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sass`
--

DROP TABLE IF EXISTS `sass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sass` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `respondent_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `head_ache` int(11) DEFAULT NULL,
  `back_pain` int(11) DEFAULT NULL,
  `hand_leg_pain` int(11) DEFAULT NULL,
  `stomach_pain` int(11) DEFAULT NULL,
  `body_pain` int(11) DEFAULT NULL,
  `jadatva` int(11) DEFAULT NULL,
  `cold_hot` int(11) DEFAULT NULL,
  `high_heart_beat` int(11) DEFAULT NULL,
  `stomach_prob` int(11) DEFAULT NULL,
  `burn` int(11) DEFAULT NULL,
  `physical_weakness` int(11) DEFAULT NULL,
  `psychological_weakness` int(11) DEFAULT NULL,
  `unconscious` int(11) DEFAULT NULL,
  `shivering` int(11) DEFAULT NULL,
  `tired` int(11) DEFAULT NULL,
  `no_sleep` int(11) DEFAULT NULL,
  `no_hungry` int(11) DEFAULT NULL,
  `no_interest` int(11) DEFAULT NULL,
  `constipation` int(11) DEFAULT NULL,
  `loose_motions` int(11) DEFAULT NULL,
  `leg_shake` int(11) DEFAULT NULL,
  `legs_pain` int(11) DEFAULT NULL,
  `disease_symptoms` int(11) DEFAULT NULL,
  `walks_relief` int(11) DEFAULT NULL,
  `night_symptoms_increases` int(11) DEFAULT NULL,
  `sleep_prob` int(11) DEFAULT NULL,
  `ringing_sounds` int(11) DEFAULT NULL,
  `vometings` int(11) DEFAULT NULL,
  `urine` int(11) DEFAULT NULL,
  `yoni` int(11) DEFAULT NULL,
  `pelvic` int(11) DEFAULT NULL,
  `biliseragu` int(11) DEFAULT NULL,
  `any_other` int(11) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `scale_id` int(11) DEFAULT NULL,
  `leg_unrest` int(11) DEFAULT NULL,
  `mother_id` int(11) NOT NULL,
  `symptoms_others` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sass`
--

LOCK TABLES `sass` WRITE;
/*!40000 ALTER TABLE `sass` DISABLE KEYS */;
INSERT INTO `sass` VALUES (1,1,20001,'2018-08-28 00:00:12','2018-08-28 00:01:40','superadmin',1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,-11,3,5,NULL,4,'');
/*!40000 ALTER TABLE `sass` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scale`
--

DROP TABLE IF EXISTS `scale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scale_name` varchar(1000) DEFAULT NULL,
  `m6` varchar(20) DEFAULT NULL,
  `m12` varchar(20) DEFAULT NULL,
  `m24` varchar(20) DEFAULT NULL,
  `table_name` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `display_sequence` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scale`
--

LOCK TABLES `scale` WRITE;
/*!40000 ALTER TABLE `scale` DISABLE KEYS */;
INSERT INTO `scale` VALUES (1,'Whooley\'s Screening Questions','m','m','m','wooley-depression','Maternal',1),(2,'Edinburgh Postnatal Depression Scale','m','m','m','edinburgh-postnatal-depression','Maternal',2),(3,'Patient Health Questionnaire','m','m','m','patient-health','Maternal',3),(4,'Suicide Behaviours Questionnaire','m','m','m','suicide-questions','Maternal',4),(5,'Scale for Assessment of Somatic Symptoms','m','m','m','sass','Maternal',5),(6,'State Trait Anxiety Inventory','m','m','m','stai','Maternal',6),(7,'Psychosocial Profile - Assessment of Stress Scale','m','m','m','psychosocial-assessment','Maternal',7),(8,'The Life Events Checklist ','m','m','m','life-events','Maternal',8),(9,'PTSD Checklist','m','m','m','ptsd-checklist','Maternal',9),(10,'Multidimensional Scale of Perceived Social Support','m','m','m','multidimentional','Maternal',10),(11,'ICMR Domestic Violence Assessment Questionnaire','m','m','m','icmr','Maternal',11),(12,'Conflict Tactics Scale (CTS)','m','m','m','conflict-tactics-scale','Maternal',12),(13,'Parent-Infant Caregiving Scale','m,ac','x','x','pics','Child',13),(14,'Breastfeeding Self Efficacy Scale','m','x','x','breastfeeding','Child',14),(15,'Child temperament: IBQ-R','m,ac','m,ac','x','baby-likes-dislikes','Child',15),(16,'Child temperament: ECBQ','x','x','m,ac','child-ecbq','Child',16),(17,'Infant health checklist for common physical illnesses & infections; Postnatal healthcare utilization and immunization checks','m','m','m','infant-health','Child',17),(18,'Maternal Health checklist for common physical illnesses','m','m','m','maternal','Maternal',18),(19,'Brief Infant Toddler Social and Emotional Assessment (BITSEA)','x','m','m','bitsea','Child',19),(20,'Preschool CBCL','x','x','m,ac','cbcl','Child',20),(21,'Shared care-giving checklist','m,ac','m,ac','m,ac','shared-caregiving','Child',21),(22,'Parental Bonding Instrument -PBI','x','x','m',NULL,'Maternal',22),(23,'Psychosocial risk factors checklist ','i','i','i','psychosocial-risk-factors','Maternal',23),(24,'Video - Observed sensitivity with infant during play (to be only mentioned in the grid)','m,ac','m,ac','x',NULL,'Child',24),(25,'Bayley (to be only mentioned in the grid)','x','x','m',NULL,'Child',25),(26,'Five minute speech sample','m','m','m',NULL,'Maternal',26),(27,'Anthropometric measures','m','m','m','infant-anthropomatric','Maternal,Child',27),(28,'Parental Bonding Instrument-Father','x','x','m',NULL,'Maternal',NULL),(29,'Parental Bonding Instrument-Mother','x','x','m',NULL,'Maternal',NULL),(30,'Parental Bonding Instrument-First','x','x','m','parental-bonding-first','Maternal',28),(31,'Parental Bonding Instrument-Second','x','x','m','parental-bonding-second','Maternal',29),(32,'Parental Bonding Instrument-Second','x','x','m','parental-bonding-second','Maternal',29);
/*!40000 ALTER TABLE `scale` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scores`
--

DROP TABLE IF EXISTS `scores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scores` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `respondent_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `scale_id` int(11) DEFAULT NULL,
  `score` int(30) DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scores`
--

LOCK TABLES `scores` WRITE;
/*!40000 ALTER TABLE `scores` DISABLE KEYS */;
INSERT INTO `scores` VALUES (1,1,1,1,0,'2018-09-19 05:42:05','superadmin','2018-09-19 05:42:05'),(2,1,1,2,1,'2018-09-19 06:05:59','superadmin','2018-09-19 06:05:59'),(3,1,2,21,7,'2018-01-24 04:46:18','superadmin','2018-01-24 04:46:18'),(4,3,20000,2,14,'2018-08-20 07:05:58','superadmin','2018-08-20 07:05:58'),(5,3,20000,3,3,'2018-08-20 07:06:07','superadmin','2018-08-20 07:06:07'),(6,2,3,2,16,'2018-08-19 20:05:13','superadmin','2018-08-19 20:05:13'),(7,4,20001,1,0,'2018-09-21 06:43:31','superadmin','2018-09-21 06:43:31'),(8,4,20001,3,12,'2018-08-27 23:59:33','superadmin','2018-08-27 23:59:33'),(9,4,20001,4,1,'2018-09-19 06:49:10','superadmin','2018-09-19 06:49:10'),(10,4,20001,6,3,'2018-09-19 21:26:55','superadmin','2018-09-19 21:26:55'),(11,4,20001,2,0,'2018-08-27 23:58:15','superadmin','2018-08-27 23:58:15'),(12,4,20001,5,3,'2018-08-28 00:01:41','superadmin','2018-08-28 00:01:41'),(13,4,20001,7,0,'2018-08-28 00:02:15','superadmin','2018-08-28 00:02:15'),(14,4,20001,8,238,'2018-09-20 07:23:19','superadmin','2018-09-20 07:23:19'),(15,4,20001,9,0,'2018-08-28 00:02:51','superadmin','2018-08-28 00:02:51'),(16,4,20001,10,3,'2018-08-28 00:03:02','superadmin','2018-08-28 00:03:02'),(17,4,20001,11,1,'2018-08-28 00:03:29','superadmin','2018-08-28 00:03:29'),(18,1,20001,12,0,'2018-08-28 00:04:12','superadmin','2018-08-28 00:04:12'),(19,4,20001,13,0,'2018-08-28 00:04:32','superadmin','2018-08-28 00:04:32'),(20,4,20001,14,0,'2018-08-28 00:04:46','superadmin','2018-08-28 00:04:46'),(21,4,20001,15,0,'2018-08-28 00:05:04','superadmin','2018-08-28 00:05:04'),(22,4,20001,21,0,'2018-08-28 19:42:24','superadmin','2018-08-28 19:42:24'),(23,3,20000,21,0,'2018-08-29 06:45:47','superadmin','2018-08-29 06:45:47'),(24,3,20000,9,0,'2018-08-29 06:16:26','superadmin','2018-08-29 06:16:26'),(25,4,20002,16,4,'2018-08-28 19:43:42','superadmin','2018-08-28 19:43:42'),(26,4,20002,20,1,'2018-08-28 19:43:56','superadmin','2018-08-28 19:43:56'),(27,4,20002,21,0,'2018-08-28 19:52:28','superadmin','2018-08-28 19:52:28'),(28,4,20002,6,3,'2018-08-28 20:00:45','superadmin','2018-08-28 20:00:45'),(29,0,0,4,0,'2018-09-19 05:38:43','superadmin','2018-09-19 05:38:43'),(30,1,20000,1,1,'2018-09-19 06:19:17','superadmin','2018-09-19 06:19:17'),(31,2,20002,1,1,'2018-09-19 06:26:58','superadmin','2018-09-19 06:26:58'),(32,3,20003,1,0,'2018-09-19 06:29:20','superadmin','2018-09-19 06:29:20'),(33,3,20003,6,6,'2018-09-19 19:47:29','superadmin','2018-09-19 19:47:29'),(34,1,20000,7,3,'2018-09-20 05:40:04','superadmin','2018-09-20 05:40:04'),(35,1,20000,2,4,'2018-09-20 05:47:16','superadmin','2018-09-20 05:47:16'),(36,1,20000,8,138,'2018-09-20 05:49:53','superadmin','2018-09-20 05:49:53'),(37,1,20000,23,NULL,'2018-09-20 05:55:38','superadmin','2018-09-20 05:55:38'),(38,3,20003,23,NULL,'2018-09-20 07:23:32','superadmin','2018-09-20 07:23:32'),(39,2,20003,12,0,'2018-09-20 06:28:50','superadmin','2018-09-20 06:28:50'),(40,0,0,8,401,'2018-09-19 20:57:11','superadmin','2018-09-19 20:57:11'),(41,4,20001,23,NULL,'2018-09-20 07:12:44','superadmin','2018-09-20 07:12:44'),(42,1,20003,12,2,'2018-09-19 19:30:49','superadmin','2018-09-19 19:30:49'),(43,3,20003,8,756,'2018-09-19 20:01:01','superadmin','2018-09-19 20:01:01'),(44,3,20003,18,0,'2018-09-19 19:33:53','superadmin','2018-09-19 19:33:53'),(45,3,20003,21,0,'2018-09-19 19:35:41','superadmin','2018-09-19 19:35:41'),(46,3,20003,3,8,'2018-09-19 19:58:43','superadmin','2018-09-19 19:58:43'),(47,1,20004,12,4,'2018-09-21 00:07:20','superadmin','2018-09-21 00:07:20'),(48,NULL,0,12,1,'2018-09-21 01:02:01','superadmin','2018-09-21 01:02:01');
/*!40000 ALTER TABLE `scores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shared_caregiving`
--

DROP TABLE IF EXISTS `shared_caregiving`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shared_caregiving` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `respondent_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `family_type` int(3) DEFAULT NULL,
  `number_adults` int(3) DEFAULT NULL,
  `number_infants` int(3) DEFAULT NULL,
  `number_children_preschool` int(3) DEFAULT NULL,
  `number_children` int(3) DEFAULT NULL,
  `number_adolescents` int(3) DEFAULT NULL,
  `current_stay` int(3) DEFAULT NULL,
  `care_carer` varchar(45) DEFAULT NULL,
  `care_myself` varchar(45) DEFAULT NULL,
  `care_husband` varchar(45) DEFAULT NULL,
  `care_maternal_grandfather` varchar(45) DEFAULT NULL,
  `care_maternal_grandmother` varchar(45) DEFAULT NULL,
  `care_paternal_grandfather` varchar(45) DEFAULT NULL,
  `care_paternal_grandmother` varchar(45) DEFAULT NULL,
  `care_siblings` varchar(45) DEFAULT NULL,
  `played` int(3) DEFAULT NULL,
  `taken_out` int(3) DEFAULT NULL,
  `introduced_newthings` int(3) DEFAULT NULL,
  `told_not_todo_something` int(3) DEFAULT NULL,
  `cleaned` int(3) DEFAULT NULL,
  `rocked` int(3) DEFAULT NULL,
  `carried` int(3) DEFAULT NULL,
  `scolded` int(3) DEFAULT NULL,
  `wasnt_too_hot` int(3) DEFAULT NULL,
  `care_sick` int(3) DEFAULT NULL,
  `taken_harm` int(3) DEFAULT NULL,
  `fed` int(3) DEFAULT NULL,
  `kissed` int(3) DEFAULT NULL,
  `cooed` int(3) DEFAULT NULL,
  `put_sleep` int(3) DEFAULT NULL,
  `bathed` int(3) DEFAULT NULL,
  `didnt_eat_harmful` int(3) DEFAULT NULL,
  `soothed` int(3) DEFAULT NULL,
  `massaged` int(3) DEFAULT NULL,
  `showed_what_todo` int(3) DEFAULT NULL,
  `cuddled_crying` int(3) DEFAULT NULL,
  `dressed` int(3) DEFAULT NULL,
  `told_off` int(3) DEFAULT NULL,
  `comfortable` int(3) DEFAULT NULL,
  `slept_beside` int(3) DEFAULT NULL,
  `introduced_new_people` int(3) DEFAULT NULL,
  `scolded_crying` int(3) DEFAULT NULL,
  `cradled_crying` int(3) DEFAULT NULL,
  `made_warm` int(3) DEFAULT NULL,
  `gone_wokeup` int(3) DEFAULT NULL,
  `prepared_formula_milk` int(3) DEFAULT NULL,
  `talked` int(3) DEFAULT NULL,
  `sung` int(3) DEFAULT NULL,
  `cuddled` int(3) DEFAULT NULL,
  `taken_nurse` int(3) DEFAULT NULL,
  `talked_sternly` int(3) DEFAULT NULL,
  `held` int(3) DEFAULT NULL,
  `shared_smiles` int(3) DEFAULT NULL,
  `distracted_crying` int(3) DEFAULT NULL,
  `showed_rightthing` int(3) DEFAULT NULL,
  `taken_visit_family` int(3) DEFAULT NULL,
  `cradled_arms` int(3) DEFAULT NULL,
  `fed_crying` int(3) DEFAULT NULL,
  `prayed` int(3) DEFAULT NULL,
  `invovled_family_rituals` int(3) DEFAULT NULL,
  `score` int(3) DEFAULT NULL,
  `scale_id` int(11) DEFAULT NULL,
  `songs` int(3) DEFAULT NULL,
  `any_others` varchar(45) DEFAULT NULL,
  `thoogiddene` int(11) DEFAULT NULL,
  `other_activity` varchar(45) DEFAULT NULL,
  `current_stay_others` varchar(45) DEFAULT NULL,
  `mother_id` int(11) NOT NULL,
  `songs_acg` varchar(45) DEFAULT NULL,
  `songs_acg_lastweek` varchar(45) DEFAULT NULL,
  `played_acg` varchar(45) DEFAULT NULL,
  `played_acg_lastweek` varchar(45) DEFAULT NULL,
  `taken_out_acg` varchar(45) DEFAULT NULL,
  `taken_out_acg_lastweek` varchar(45) DEFAULT NULL,
  `introduced_newthings_acg` varchar(45) DEFAULT NULL,
  `introduced_newthings_acg_lastweek` varchar(45) DEFAULT NULL,
  `told_not_todo_something_acg` varchar(45) DEFAULT NULL,
  `told_not_todo_something_acg_lastweek` varchar(45) DEFAULT NULL,
  `cleaned_acg` varchar(45) DEFAULT NULL,
  `cleaned_acg_lastweek` varchar(45) DEFAULT NULL,
  `rocked_acg` varchar(45) DEFAULT NULL,
  `rocked_acg_lastweek` varchar(45) DEFAULT NULL,
  `carried_acg` varchar(45) DEFAULT NULL,
  `carried_acg_lastweek` varchar(45) DEFAULT NULL,
  `scolded_acg` varchar(45) DEFAULT NULL,
  `scolded_acg_lastweek` varchar(45) DEFAULT NULL,
  `wasnt_too_hot_acg` varchar(45) DEFAULT NULL,
  `wasnt_too_hot_acg_lastweek` varchar(45) DEFAULT NULL,
  `care_sick_acg` varchar(45) DEFAULT NULL,
  `care_sick_acg_lastweek` varchar(45) DEFAULT NULL,
  `taken_harm_acg` varchar(45) DEFAULT NULL,
  `taken_harm_acg_lastweek` varchar(45) DEFAULT NULL,
  `fed_acg` varchar(45) DEFAULT NULL,
  `fed_acg_lastweek` varchar(45) DEFAULT NULL,
  `kissed_acg` varchar(45) DEFAULT NULL,
  `kissed_acg_lastweek` varchar(45) DEFAULT NULL,
  `cooed_acg` varchar(45) DEFAULT NULL,
  `cooed_acg_lastweek` varchar(45) DEFAULT NULL,
  `put_sleep_acg` varchar(45) DEFAULT NULL,
  `put_sleep_acg_lastweek` varchar(45) DEFAULT NULL,
  `bathed_acg` varchar(45) DEFAULT NULL,
  `bathed_acg_lastweek` varchar(45) DEFAULT NULL,
  `didnt_eat_harmful_acg` varchar(45) DEFAULT NULL,
  `didnt_eat_harmful_acg_lastweek` varchar(45) DEFAULT NULL,
  `soothed_acg` varchar(45) DEFAULT NULL,
  `soothed_acg_lastweek` varchar(45) DEFAULT NULL,
  `massaged_acg` varchar(45) DEFAULT NULL,
  `massaged_acg_lastweek` varchar(45) DEFAULT NULL,
  `showed_what_todo_acg` varchar(45) DEFAULT NULL,
  `showed_what_todo_acg_lastweek` varchar(45) DEFAULT NULL,
  `thoogiddene_acg` varchar(45) DEFAULT NULL,
  `thoogiddene_acg_lastweek` varchar(45) DEFAULT NULL,
  `cuddled_crying_acg` varchar(45) DEFAULT NULL,
  `cuddled_crying_acg_lastweek` varchar(45) DEFAULT NULL,
  `dressed_acg` varchar(45) DEFAULT NULL,
  `dressed_acg_lastweek` varchar(45) DEFAULT NULL,
  `told_off_acg` varchar(45) DEFAULT NULL,
  `told_off_acg_lastweek` varchar(45) DEFAULT NULL,
  `comfortable_acg` varchar(45) DEFAULT NULL,
  `comfortable_acg_lastweek` varchar(45) DEFAULT NULL,
  `slept_beside_acg` varchar(45) DEFAULT NULL,
  `slept_beside_acg_lastweek` varchar(45) DEFAULT NULL,
  `introduced_new_people_acg` varchar(45) DEFAULT NULL,
  `introduced_new_people_acg_lastweek` varchar(45) DEFAULT NULL,
  `_acg` varchar(45) DEFAULT NULL,
  `_acg_lastweek` varchar(45) DEFAULT NULL,
  `scolded_crying_acg` varchar(45) DEFAULT NULL,
  `scolded_crying_acg_lastweek` varchar(45) DEFAULT NULL,
  `cradled_crying_acg` varchar(45) DEFAULT NULL,
  `cradled_crying_acg_lastweek` varchar(45) DEFAULT NULL,
  `made_warm_acg` varchar(45) DEFAULT NULL,
  `made_warm_acg_lastweek` varchar(45) DEFAULT NULL,
  `gone_wokeup_acg` varchar(45) DEFAULT NULL,
  `gone_wokeup_acg_lastweek` varchar(45) DEFAULT NULL,
  `prepared_formula_milk_acg` varchar(45) DEFAULT NULL,
  `prepared_formula_milk_acg_lastweek` varchar(45) DEFAULT NULL,
  `talked_acg` varchar(45) DEFAULT NULL,
  `talked_acg_lastweek` varchar(45) DEFAULT NULL,
  `cuddled_acg` varchar(45) DEFAULT NULL,
  `cuddled_acg_lastweek` varchar(45) DEFAULT NULL,
  `sung_acg` varchar(45) DEFAULT NULL,
  `sung_acg_lastweek` varchar(45) DEFAULT NULL,
  `taken_nurse_acg` varchar(45) DEFAULT NULL,
  `taken_nurse_acg_lastweek` varchar(45) DEFAULT NULL,
  `talked_sternly_acg` varchar(45) DEFAULT NULL,
  `talked_sternly_acg_lastweek` varchar(45) DEFAULT NULL,
  `held_acg` varchar(45) DEFAULT NULL,
  `held_acg_lastweek` varchar(45) DEFAULT NULL,
  `shared_smiles_acg` varchar(45) DEFAULT NULL,
  `shared_smiles_acg_lastweek` varchar(45) DEFAULT NULL,
  `distracted_crying_acg` varchar(45) DEFAULT NULL,
  `distracted_crying_acg_lastweek` varchar(45) DEFAULT NULL,
  `showed_rightthing_acg` varchar(45) DEFAULT NULL,
  `showed_rightthing_acg_lastweek` varchar(45) DEFAULT NULL,
  `taken_visit_family_acg` varchar(45) DEFAULT NULL,
  `taken_visit_family_acg_lastweek` varchar(45) DEFAULT NULL,
  `cradled_arms_acg` varchar(45) DEFAULT NULL,
  `cradled_arms_acg_lastweek` varchar(45) DEFAULT NULL,
  `fed_crying_acg` varchar(45) DEFAULT NULL,
  `fed_crying_acg_lastweek` varchar(45) DEFAULT NULL,
  `prayed_acg` varchar(45) DEFAULT NULL,
  `prayed_acg_lastweek` varchar(45) DEFAULT NULL,
  `invovled_family_rituals_acg` varchar(45) DEFAULT NULL,
  `invovled_family_rituals_acg_lastweek` varchar(45) DEFAULT NULL,
  `songs_other` varchar(45) DEFAULT NULL,
  `played_other` varchar(45) DEFAULT NULL,
  `taken_out_other` varchar(45) DEFAULT NULL,
  `introduced_newthings_other` varchar(45) DEFAULT NULL,
  `told_not_todo_something_other` varchar(45) DEFAULT NULL,
  `cleaned_other` varchar(45) DEFAULT NULL,
  `rocked_other` varchar(45) DEFAULT NULL,
  `carried_other` varchar(45) DEFAULT NULL,
  `scolded_other` varchar(45) DEFAULT NULL,
  `wasnt_too_hot_other` varchar(45) DEFAULT NULL,
  `care_sick_other` varchar(45) DEFAULT NULL,
  `taken_harm_other` varchar(45) DEFAULT NULL,
  `fed_other` varchar(45) DEFAULT NULL,
  `kissed_other` varchar(45) DEFAULT NULL,
  `cooed_other` varchar(45) DEFAULT NULL,
  `put_sleep_other` varchar(45) DEFAULT NULL,
  `bathed_other` varchar(45) DEFAULT NULL,
  `didnt_eat_harmful_other` varchar(45) DEFAULT NULL,
  `soothed_other` varchar(45) DEFAULT NULL,
  `massaged_other` varchar(45) DEFAULT NULL,
  `showed_what_todo_other` varchar(45) DEFAULT NULL,
  `thoogiddene_other` varchar(45) DEFAULT NULL,
  `cuddled_crying_other` varchar(45) DEFAULT NULL,
  `dressed_other` varchar(45) DEFAULT NULL,
  `told_off_other` varchar(45) DEFAULT NULL,
  `comfortable_other` varchar(45) DEFAULT NULL,
  `slept_beside_other` varchar(45) DEFAULT NULL,
  `introduced_new_people_other` varchar(45) DEFAULT NULL,
  `_other` varchar(45) DEFAULT NULL,
  `scolded_crying_other` varchar(45) DEFAULT NULL,
  `cradled_crying_other` varchar(45) DEFAULT NULL,
  `made_warm_other` varchar(45) DEFAULT NULL,
  `gone_wokeup_other` varchar(45) DEFAULT NULL,
  `prepared_formula_milk_other` varchar(45) DEFAULT NULL,
  `talked_other` varchar(45) DEFAULT NULL,
  `cuddled_other` varchar(45) DEFAULT NULL,
  `sung_other` varchar(45) DEFAULT NULL,
  `taken_nurse_other` varchar(45) DEFAULT NULL,
  `talked_sternly_other` varchar(45) DEFAULT NULL,
  `held_other` varchar(45) DEFAULT NULL,
  `shared_smiles_other` varchar(45) DEFAULT NULL,
  `distracted_crying_other` varchar(45) DEFAULT NULL,
  `showed_rightthing_other` varchar(45) DEFAULT NULL,
  `taken_visit_family_other` varchar(45) DEFAULT NULL,
  `cradled_arms_other` varchar(45) DEFAULT NULL,
  `fed_crying_other` varchar(45) DEFAULT NULL,
  `prayed_other` varchar(45) DEFAULT NULL,
  `invovled_family_rituals_other` varchar(45) DEFAULT NULL,
  `not_applicable` varchar(45) DEFAULT NULL,
  `other_activity_acg` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shared_caregiving`
--

LOCK TABLES `shared_caregiving` WRITE;
/*!40000 ALTER TABLE `shared_caregiving` DISABLE KEYS */;
INSERT INTO `shared_caregiving` VALUES (1,1,2,'2018-01-24 04:46:18','2018-01-24 04:46:18','superadmin',1,4,1,2,0,NULL,1,NULL,'','','','','','','',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,2,1,NULL,0,1,NULL,NULL,7,21,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,3,20001,'2018-08-28 00:07:12','2018-08-28 19:42:24','superadmin',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,'',NULL,NULL,NULL,1,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,21,1,'',NULL,'','',4,'1','1','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','1','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',''),(3,3,20000,'2018-08-29 05:36:26','2018-08-29 06:45:47','superadmin',NULL,NULL,4,NULL,NULL,NULL,NULL,NULL,'','',NULL,'',NULL,NULL,NULL,1,1,1,-15,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,21,1,'',NULL,'','',3,'1','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',''),(4,3,20002,'2018-08-28 19:52:01','2018-08-28 19:52:27','superadmin',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,21,1,'',NULL,'','',4,'1',NULL,NULL,NULL,'1',NULL,NULL,NULL,NULL,'1',NULL,NULL,'1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,''),(5,3,20003,'2018-09-19 19:35:41','2018-09-19 19:35:41','superadmin',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,21,NULL,'',NULL,'','',3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
/*!40000 ALTER TABLE `shared_caregiving` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `somatic_symptoms`
--

DROP TABLE IF EXISTS `somatic_symptoms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `somatic_symptoms` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `respondent_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `pain_headache` int(3) DEFAULT NULL,
  `pain_backache` int(3) DEFAULT NULL,
  `pain_extremities` int(3) DEFAULT NULL,
  `pain_abdominal` int(3) DEFAULT NULL,
  `pain_body` int(3) DEFAULT NULL,
  `sensory_tingling` int(3) DEFAULT NULL,
  `sensory_heat_cold` int(3) DEFAULT NULL,
  `sensory_palpitations` int(3) DEFAULT NULL,
  `sensory_gas` int(3) DEFAULT NULL,
  `sensory_burning` int(3) DEFAULT NULL,
  `non_specific_weakness_body` int(3) DEFAULT NULL,
  `non_specific_weakness_mind` int(3) DEFAULT NULL,
  `non_specific_giddiness` int(3) DEFAULT NULL,
  `non_specific_trembling` int(3) DEFAULT NULL,
  `non_specific_tiredness` int(3) DEFAULT NULL,
  `biological_lack_sleep` int(3) DEFAULT NULL,
  `biological_appetite` int(3) DEFAULT NULL,
  `biological_libidio` int(3) DEFAULT NULL,
  `biological_constipation` int(3) DEFAULT NULL,
  `biological_diarrhoe` int(3) DEFAULT NULL,
  `specific_strong_urge_move_legs` int(3) DEFAULT NULL,
  `specific_tingling_leg` int(3) DEFAULT NULL,
  `specific_woresening_inactive` int(3) DEFAULT NULL,
  `specific_relief_walking` int(3) DEFAULT NULL,
  `specific_woresening_sleep` int(3) DEFAULT NULL,
  `specific_sleep_restlessness` int(3) DEFAULT NULL,
  `tinnitus_noise_ear_buzzing` int(3) DEFAULT NULL,
  `other_nausea` int(3) DEFAULT NULL,
  `other_urinary_frequency` int(3) DEFAULT NULL,
  `other_itching` int(3) DEFAULT NULL,
  `other_pelvic_pain` int(3) DEFAULT NULL,
  `other_white_discharge` int(3) DEFAULT NULL,
  `other_specify` int(3) DEFAULT NULL,
  `score_a` int(3) DEFAULT NULL,
  `score_b` int(3) DEFAULT NULL,
  `score_c` int(3) DEFAULT NULL,
  `score_d` int(3) DEFAULT NULL,
  `score_e` int(3) DEFAULT NULL,
  `score_f` int(3) DEFAULT NULL,
  `score` int(3) DEFAULT NULL,
  `number_symptoms` int(3) DEFAULT NULL,
  `number_significant_symptoms` int(3) DEFAULT NULL,
  `scale_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `somatic_symptoms`
--

LOCK TABLES `somatic_symptoms` WRITE;
/*!40000 ALTER TABLE `somatic_symptoms` DISABLE KEYS */;
/*!40000 ALTER TABLE `somatic_symptoms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stai`
--

DROP TABLE IF EXISTS `stai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `respondent_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `feel_peace` varchar(45) DEFAULT NULL,
  `feel_safe` varchar(45) DEFAULT NULL,
  `feel_pressure` varchar(45) DEFAULT NULL,
  `feel_tired` varchar(45) DEFAULT NULL,
  `feel_nemmadi` varchar(45) DEFAULT NULL,
  `feel_sad` varchar(45) DEFAULT NULL,
  `feel_future_disaster` varchar(45) DEFAULT NULL,
  `feel_enough` varchar(45) DEFAULT NULL,
  `feel_fear` varchar(45) DEFAULT NULL,
  `feel_free` varchar(45) DEFAULT NULL,
  `feel_confident` varchar(45) DEFAULT NULL,
  `feel_weak` varchar(45) DEFAULT NULL,
  `feel_feared` varchar(45) DEFAULT NULL,
  `cant_take_decisions` varchar(45) DEFAULT NULL,
  `feel_samadhana` varchar(45) DEFAULT NULL,
  `feel_santhushti` varchar(45) DEFAULT NULL,
  `feel_sadness` varchar(45) DEFAULT NULL,
  `feel_galibili` varchar(45) DEFAULT NULL,
  `feel_drudamanasssu` varchar(45) DEFAULT NULL,
  `feel_happy` varchar(45) DEFAULT NULL,
  `happy` varchar(45) DEFAULT NULL,
  `weak` varchar(45) DEFAULT NULL,
  `thrupthi` varchar(45) DEFAULT NULL,
  `happy_like_others` varchar(45) DEFAULT NULL,
  `fail` varchar(45) DEFAULT NULL,
  `free` varchar(45) DEFAULT NULL,
  `safe` varchar(45) DEFAULT NULL,
  `more_prob` varchar(45) DEFAULT NULL,
  `thinks_lot` varchar(45) DEFAULT NULL,
  `happiest` varchar(45) DEFAULT NULL,
  `stopping_thoughts` varchar(45) DEFAULT NULL,
  `not_confident` varchar(45) DEFAULT NULL,
  `secure` varchar(45) DEFAULT NULL,
  `takes_decisions` varchar(45) DEFAULT NULL,
  `asamarpaka` varchar(45) DEFAULT NULL,
  `santhushta` varchar(45) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `scale_id` int(11) DEFAULT NULL,
  `notimp_thoughts` varchar(45) DEFAULT NULL,
  `nirashe` varchar(45) DEFAULT NULL,
  `dhrudmanassu` varchar(45) DEFAULT NULL,
  `udvega` varchar(45) DEFAULT NULL,
  `mother_id` int(11) NOT NULL,
  `not_app` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stai`
--

LOCK TABLES `stai` WRITE;
/*!40000 ALTER TABLE `stai` DISABLE KEYS */;
INSERT INTO `stai` VALUES (1,1,20001,'2018-08-27 21:31:28','2018-08-28 00:02:05','superadmin','-11','','','','','','','','','','','','','-5','','','','','','-8','','','','','','','','','','','','','','','','',0,6,'','','','',4,''),(2,1,20002,'2018-08-28 20:00:38','2018-08-28 20:00:45','superadmin','3','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',3,6,'','','','',4,''),(3,1,20003,'2018-09-18 19:38:58','2018-09-19 19:47:29','superadmin','1','2','','','','1','2','','','','','','','','','','','','-6','-8','-10','-10','-10','-10','-10','-10','-10','-10','-10','-10','-10','-10','-10','-10','-10','-10',6,6,'-10','-10','-10','-10',3,'YES');
/*!40000 ALTER TABLE `stai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suicide_questions`
--

DROP TABLE IF EXISTS `suicide_questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suicide_questions` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `assessment_id` int(11) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `ideas` int(3) DEFAULT NULL,
  `plan_attempted` int(3) DEFAULT NULL,
  `current_pregnancy_thought_ending_life` int(3) DEFAULT NULL,
  `current_pregnancy_plan` int(3) DEFAULT NULL,
  `current_pregnancy_attempt` int(3) DEFAULT NULL,
  `times_attempted` varchar(45) DEFAULT NULL,
  `reasons_attempt` varchar(45) DEFAULT NULL,
  `method_attempt` varchar(45) DEFAULT NULL,
  `score` int(3) DEFAULT NULL,
  `respondent_id` int(11) DEFAULT NULL,
  `scale_id` int(11) DEFAULT NULL,
  `specify_others` varchar(45) DEFAULT NULL,
  `mother_id` int(11) NOT NULL,
  `not_applicable` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suicide_questions`
--

LOCK TABLES `suicide_questions` WRITE;
/*!40000 ALTER TABLE `suicide_questions` DISABLE KEYS */;
INSERT INTO `suicide_questions` VALUES (1,20001,'2018-09-19 06:46:24','2018-09-19 06:49:10','superadmin',1,NULL,NULL,NULL,NULL,'','',NULL,1,1,4,'',4,'YES');
/*!40000 ALTER TABLE `suicide_questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `confirmation_token` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `superadmin` smallint(6) DEFAULT '0',
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `updated_at` timestamp(6) NOT NULL DEFAULT '0000-00-00 00:00:00.000000',
  `registration_ip` varchar(15) DEFAULT NULL,
  `bind_to_ip` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `email_confirmed` smallint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'superadmin','lAD7WmzMjoKM4hZtmiEmQn7rA9boDahm','$2y$13$jHI47N9ge6dtPAqyf4SDm.ieDRNmB0hpW8RnozPcOPmT/GQTD/Sme',NULL,1,1,'2017-10-17 09:18:26.376746','Superadmin','last','0000-00-00 00:00:00.000000',NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_visit_log`
--

DROP TABLE IF EXISTS `user_visit_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_visit_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `language` char(2) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `visit_time` int(11) NOT NULL,
  `browser` varchar(30) DEFAULT NULL,
  `os` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_visit_log`
--

LOCK TABLES `user_visit_log` WRITE;
/*!40000 ALTER TABLE `user_visit_log` DISABLE KEYS */;
INSERT INTO `user_visit_log` VALUES (1,'5aba1c7481b2b','::1','en','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36',1,1522146420,'Chrome','Windows'),(2,'5aba30b07df78','::1','en','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36',1,1522151600,'Chrome','Windows'),(3,'5b7a68c499c4c','::1','en','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,1534748868,'Chrome','Windows'),(4,'5b7aabfa0d28d','::1','en','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,1534766074,'Chrome','Windows'),(5,'5b862ff7a4e7c','::1','en','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,1535520759,'Chrome','Windows'),(6,'5b86421c6e89d','::1','en','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,1535525404,'Chrome','Windows'),(7,'5ba1ee909ba60','::1','en','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,1537339024,'Chrome','Windows'),(8,'5ba1fc459dd1f','::1','en','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,1537342533,'Chrome','Windows'),(9,'5ba23688126ab','::1','en','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,1537357448,'Chrome','Windows'),(10,'5ba3318d19f94','::1','en','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,1537421709,'Chrome','Windows'),(11,'5ba345036cfb1','::1','en','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,1537426691,'Chrome','Windows'),(12,'5ba361e538754','::1','en','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,1537434085,'Chrome','Windows'),(13,'5ba494bbd2a01','::1','en','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36',1,1537512635,'Chrome','Windows'),(14,'5ba4d7c1994d8','::1','en','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36',1,1537529793,'Chrome','Windows');
/*!40000 ALTER TABLE `user_visit_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wooley_depression`
--

DROP TABLE IF EXISTS `wooley_depression`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wooley_depression` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `respondent_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `last_updated_dtm` timestamp NULL DEFAULT NULL,
  `depressed_pastmonth` int(11) DEFAULT NULL,
  `little_interest_pleasure_pastmonth` int(3) DEFAULT NULL,
  `women_yes_needhelp` varchar(45) DEFAULT NULL,
  `score` int(3) DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `created_dtm` timestamp NULL DEFAULT NULL,
  `scale_id` int(1) DEFAULT '1',
  `mother_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wooley_depression`
--

LOCK TABLES `wooley_depression` WRITE;
/*!40000 ALTER TABLE `wooley_depression` DISABLE KEYS */;
INSERT INTO `wooley_depression` VALUES (1,1,1,'2018-09-19 05:42:05',-7,-8,'',0,'superadmin','2018-09-19 05:40:23',1,1),(2,1,20001,'2018-09-21 06:43:31',0,0,'',0,'superadmin','2018-09-19 06:00:29',1,4),(3,1,20000,'2018-09-19 06:19:17',1,-10,'',1,'superadmin','2018-09-19 06:19:14',1,1),(4,1,20002,'2018-09-19 06:26:58',1,-10,'',1,'superadmin','2018-09-19 06:26:56',1,2),(5,1,20003,'2018-09-19 06:29:20',-10,0,'',0,'superadmin','2018-09-19 06:28:22',1,3);
/*!40000 ALTER TABLE `wooley_depression` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `bchads`.`wooley_depression_AFTER_INSERT` AFTER INSERT ON `wooley_depression` FOR EACH ROW
BEGIN
update wooley_depression 
SET 
    women_yes_needhelp = '-10'
WHERE
    women_yes_needhelp = null;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping routines for database 'bchads'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-24 17:07:22
