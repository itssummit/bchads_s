$(document).ready(function(){
	
	$(".toggle-switch-req").parent('label').addClass('switch');
	$(".toggle-switch-req").addClass('switch-input');
	
	$.each($(".toggle-switch-req").parent('label'),function(){
		var index = $(this).html().indexOf(">");
		var html = $(this).html().substr(0,index+1);
		$(this).html(html);
		//console.log();
	});
	
	$(".toggle-switch-req").after(
				'<span class="switch-label" data-on="On" data-off="Off"></span>'+
				'<span class="switch-handle"></span>');
	
	$.each($(".toggle-switch-req"),function(){
		if($(this).attr('dataon'))
		{
			$(this).parent('label').find('.switch-label').attr('data-on',$(this).attr('dataon'));
		}
		if($(this).attr('dataoff'))
		{
			
			$(this).parent('label').find('.switch-label').attr('data-off',$(this).attr('dataoff'));
		}
	})
	
	$('#withBoxShadow td .switch input[type="checkbox"]').change(function(){
		var attrName = $(this).attr("name");
		$("select[name=\'"+attrName+"\']").val("");
		calc();
	});
})