$(document).ready(function(){
	
	$("#withBoxShadow input:radio").click(function(){
		var optradioSelector = $(this).closest("div").attr("id");
		$("#"+optradioSelector+".form-control").val("");
	});
	
	$("#withBoxShadow td select").change(function(){
		var attrName = $(this).attr("name");
		$("input[name=\'"+attrName+"\']:radio").prop("checked",false);
	});
})