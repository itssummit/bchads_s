var autoSaveObj = {'url' : null,'formName' : null};
var autoSaveFormObj = {'url' : null,'formName' : null};

function autoSaveObjPush(url,formName){
	
	/*if(autoSaveObj.url)
	{
		if(autoSaveObj.url != url)
		{
			autoSave(autoSaveObj);
			autoSaveObj = {'url' : url,'formName' : formName};
		}
	}
	else
	{
		autoSaveObj = {'url' : url,'formName' : formName};
	}*/
	for(var i in autoSaveObj)
	{
		if(autoSaveObj[i].formName == formName)
		{
			return false;
		}
	}
	var obj = {'formName' : formName,'url' : url};
	autoSaveObj.push(obj);
	console.log(autoSaveObj);
}

function autoSaveObjArr(obj){
	for(var i in obj)
	{
		var url = '/bchads/web/index.php?r='+obj.url;
		var idForm = "#"+obj.formName; 
		$.ajax
		({
			type : 'POST',
			url : url,
			data : $(idForm).serialize(),
			async: false
		}).success(function(data){ 
			console.log(data);
		}).done(function(response){
			    console.log("success");
		}).fail(function(response){
				errorMsg(response);
		}).always(function(){
			    console.log("complete");
		});
	}
	autoSaveObj = [];
}

function errorMsg(resp){
	switch(resp.status)	{
		
		case 400	:	console.log(resp.responseText);
						alert(resp.responseText);
						break;
						
		case 401	:	console.log(resp.responseText);
						alert(resp.responseText);
						break;
						
		case 402	:	console.log(resp.responseText);
						alert(resp.responseText);
						break;
		
		case 403	:	console.log(resp.responseText);
						alert("Permission Denied for you");
						break;
		
		case 404	:	console.log(resp.responseText);
						alert(resp.responseText);
						break;
						
		case 500	:	console.log(resp.responseText);
						alert("This Problem Occurs because of Assesment Id Issue Please Contact Developers");
						break;
						
		case 501	:	console.log(resp.responseText);
						alert(resp.responseText);
						break;			
	}
						
}

function autoSaveForm(obj,id){
	var url = '/bchads/web/index.php?r='+obj.url;
	var idForm = "#"+obj.formName;
	var data = $(idForm).serialize();
	$.ajax
	({
		type : 'POST',
		url : url,
		data : data,
		async: false
	}).success(function(data){
		autoSaveFormObj = {'url' : null,'formName' : null};
		if(data && id)
		{
			$('#'+id).show();
			$('#'+id).attr("href",'/bchads/web/index.php?r=assessment/scaleform&aid='+data);
		}
	}).done(function(response){
		    console.log("success");
	}).fail(function(response){
			errorMsg(response);
			autoSaveFormObj = {'url' : null,'formName' : null};
	}).always(function(){
		    console.log("complete");
	});
}

function autoSave(obj){
	
	//for(var i in obj)
	//{
	var url = '/bchads/web/index.php?r='+obj.url;
	var idForm = "#"+obj.formName;
	//For Same Control Name
	var checkBoxData = $(idForm+" :checkbox:checked").serialize();
	var radioData = $(idForm+" :radio:checked").serialize();
	var data = $(idForm+" :input").serialize();
	if(checkBoxData)
	{
		data = data+'&'+checkBoxData;
	}
	if(radioData)
	{
		data = data+'&'+radioData;
	}
	$.ajax
	({
		type : 'POST',
		url : url,
		data : data,
		async: false
	}).success(function(data){ 
		console.log("Rec : ",data);
		autoSaveObj = {'url' : null,'formName' : null};
	}).done(function(response){
			console.log("success");
	}).fail(function(response){
			errorMsg(response);
			autoSaveObj = {'url' : null,'formName' : null};
	}).always(function(){
			console.log("complete");
	});
}

var setNotApplicableOnLoad = function(divId,colName){
	$(divId).find(":input").attr("disabled",true);
	$(colName).prop("disabled", false);
}

var setNotApplicableYes = function(divId,selector){
	var value = $(selector).val();
	$(divId).find("select").val("-10");
	$(divId).find("textarea").val("-10");
	$(divId).find("input[type=\'text\']").val("-10");
	$(divId).find("input[type=\'checkbox\']").val("-10");
	$(divId).find("input[type=\'radio\']").prop("checked",false);
	$(divId).find("input[type=\'checkbox\']").prop("checked",false);
	$(selector).val(value);
	autoSave(formObj);
	$(divId).find(":input").attr("disabled",true);
	$(selector).attr("disabled",false);
}

var setNotApplicableNo = function(divId,selector){
	var value = $(selector).val();
	$(divId).find("select").val("");
	$(divId).find("input[type=\'text\']").val("");
	$(divId).find("input[type=\'checkbox\']").val("");
	$(divId).find("textarea").val("");
	$(divId).find(":input").attr("disabled",false);
	$(selector).val(value);
}

var setNotApplicableToggleOnLoad = function(divId){
	$(divId).show();
}

var setNotApplicableShow = function(divId,selector){
	var value = $(selector).val();
	$(divId).find("select").val("");
	$(divId).find("textarea").val("");
	$(divId).find("input[type=\'text\']").val("");
	$(divId).find("input[type=\'checkbox\']").val("");
	$(divId).find("input[type=\'radio\']").prop("checked",false);
	$(divId).find("input[type=\'checkbox\']").prop("checked",false);
	$(selector).val(value);
	autoSave(formObj);
	$(divId).show();
	$(selector).attr("disabled",false);
}

var setNotApplicableHide = function(divId,selector){
	var value = $(selector).val();
	$(divId).find("select").val("-10");
	$(divId).find("input[type=\'text\']").val("-10");
	$(divId).find("input[type=\'checkbox\']").val("-10");
	$(divId).find("textarea").val("-10");
	$(divId).hide();
	$(selector).val(value);
}