$(document).ready(function(){
	$("#withBoxShadow td").click(function(){
		var radioSelector = $(this).find("input:radio");
		var checkBoxSelector = $(this).find("input:checkbox");
		if(radioSelector.length)
		{
			if(!radioSelector.attr('disabled'))
			{
				var val = radioSelector.val();
				var rradioSelector = radioSelector.attr("name");
				$("input[name=\'"+rradioSelector+"\'][value=\'"+val+"\']").prop("checked",true);
				var optradioSelector = $(this).parent().find("select").attr("name");
				$("select[name=\'"+optradioSelector+"\']").val("");
				checkLegShake();
				calc();
			}
		}
		else if(checkBoxSelector.length)
		{
			if(!checkBoxSelector.attr('disabled'))
			{
				var val = checkBoxSelector.is(':checked');
				var rcheckBoxSelector = checkBoxSelector.attr("name");
				$("input[name=\'"+rcheckBoxSelector+"\']").prop("checked",!val);
				calc();
				if(!autoSaveFormObj.url)
				{
					autoSaveFormObj = formObj;
				}
			}
		}
	});
	
	$("#withBoxShadow td select").change(function(){
		var attrName = $(this).attr("name");
		$("input[name=\'"+attrName+"\']:radio").prop("checked",false);
		$("input[name=\'"+attrName+"\']:checkbox").attr('checked',false);
		checkLegShake();
		calc();
	});
	
})