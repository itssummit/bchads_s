app.controller('ReportController', function($scope, $timeout, $http, $location, $rootScope, $window) {		 
	 $scope.assesstypes = ["m6","m12","m24"];
	function getStartEndDate()
	{
		var daterange = $('#daterangeforline').val();
		var index = daterange.indexOf("to");
		var start = daterange.substring(0,index-1);
		var end = daterange.substring(index+2);
		return {'start' : start,'end' : end};
	}
	
	$scope.getReportGenerator = function(){
		var assessmodel = $scope.selectedType;
		window.location.href = $rootScope.url+'site/reportgenerator&assesstype='+assessmodel;
	}
	
	
	  
});
