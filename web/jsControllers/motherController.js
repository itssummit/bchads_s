app.controller('MotherController', function ($scope, $http, $rootScope, $window, $timeout, $filter) {
    $rootScope.motherObj = { 'id': null };
    $rootScope.assessmentList = [];

    var modalUrl = '/bchads/views/layouts/';
    $rootScope.modal = [
		modalUrl + 'parentModal.php'
        ]

    /* index listing*/
    $scope.getAllSubject = function () {
        $http.get($rootScope.url + 'mother/allsubjects').success(function (data) {
            console.log(data);
            $scope.subjectList = data;
        })
    }
    $scope.getAllMember = function () {
        $http.get($rootScope.url + 'mother/allmemberlist').success(function (data) {
            console.log(data);
            $scope.memberList = data;
        })
    }
    $scope.getAllMember();

    /* modal popup*/
    $scope.openParentModal = function (id) {
        $('#parentBodyModal').load($rootScope.url + 'member/addmember&motherId=' + id);
        $('#parentModal').modal('show');
    }

    $scope.closeParentModal = function () {
        $('#parentModal').modal('hide');
    }
    $rootScope.checkMotherCode = function (val) {
        $http.get($rootScope.url + 'mother/checkmothercode&val=' + val).success(function (data) {
            if (data) {
                $rootScope.errorMotherCode = true;
				$('#save').prop('disabled', true);
            }
            else {
                $rootScope.errorMotherCode = false;
				$('#save').prop('disabled', false);
            }
        });
    }
	
	var loadData = function(motherId){
		$('#loadAssessTypes').load($rootScope.url + 'assessment/index&motherId='+motherId);
	}
	
    /* save demographic obsetetric assessment of mother*/
    $scope.save = function (formName, navigateTo) {
		
        $scope.$broadcast('show-errors-check-validity');
        if ($scope.checkFormIsValid(formName)) {
            $http.post($rootScope.url + 'mother/save', $rootScope.motherObj).success(function (data) {
				
                console.log(data);
                if (data.id) {
					
                    $rootScope.motherObj = data;
					loadData(data.id);
                    alert("Saved");
					
				}
                else{
                    alert(data);
                }
            })
        }
    };
    $scope.checkFormIsValid = function (formName) {
        if (formName.$valid) {
            return true;
        }
        else {
            return false;
        }
    }
    $scope.navigate = function (val) {
        Reveal.slide(val);
    };
    $scope.getData = function (id) {
        $http.get($rootScope.url + 'mother/get&id=' + id).success(function (data) {
            console.log(data);
            $rootScope.motherObj = data.model;
			loadData(data.model.id);
            $rootScope.assessmentList = data.assessmentList;
        });
    }
    $scope.addassessment6 = function () {
        var obj6 = {
            'type': 6,
			'assessment_date' : $scope.assessment_date6,
			'place_of_assesment' : $scope.place_of_assesment6,
			'assessment_by' : $scope.assessment_by6,
			'caregiver' : $scope.caregiver6,
			'relation_with_child' : $scope.relation_with_child6,
			'assessment_date_ac' : $scope.assessment_date_ac6,
			'place_of_assessment_ac' : $scope.place_of_assessment_ac6,
			'assessment_by_ac' : $scope.assessment_by_ac6
        };
		$scope.add(obj6);
		 }
		 $scope.addassessment12 = function () {
        var obj12 = {
            'type': 12,
			'assessment_date' : $scope.assessment_date6,
			'place_of_assesment' : $scope.place_of_assesment6,
			'assessment_by' : $scope.assessment_by6,
			'caregiver' : $scope.caregiver6,
			'relation_with_child' : $scope.relation_with_child6,
			'assessment_date_ac' : $scope.assessment_date_ac6,
			'place_of_assessment_ac' : $scope.place_of_assessment_ac6,
			'assessment_by_ac' : $scope.assessment_by_ac6
        };
		$scope.add(obj12);
		 }
		 $scope.addassessment24 = function () {
        var obj24 = {
            'type': 24,
			'assessment_date' : $scope.assessment_date6,
			'place_of_assesment' : $scope.place_of_assesment6,
			'assessment_by' : $scope.assessment_by6,
			'caregiver' : $scope.caregiver6,
			'relation_with_child' : $scope.relation_with_child6,
			'assessment_date_ac' : $scope.assessment_date_ac6,
			'place_of_assessment_ac' : $scope.place_of_assessment_ac6,
			'assessment_by_ac' : $scope.assessment_by_ac6
        };
			$scope.add(obj24);
		 }
		 
		$scope.add = function(obj){
		
        console.log(obj);
        $http.post($rootScope.url + 'assessment/addassessment', obj).success(function (data) {
            console.log(data);
			if(data == false){
			alert("Please choose other options");
			
			}
			else{
				$rootScope.assessmentList.push(data);
			}
            
            $scope.patient_assessment = '';
            $scope.respondent_type ='';
			
        });
		
		
    }
    $rootScope.updateDetails = function (id) {
        $window.location.href = $rootScope.url + 'mother/update&id=' + id;
    }
	$rootScope.updatemem = function (id) {
        $window.location.href = $rootScope.url + 'member/update&id=' + id;
    }


    /* assessment functions*/
    $scope.getAllassessment = function () {

        $http.get($rootScope.url + 'assessment/allassessment').success(function (data) {
            console.log(data);
            $rootScope.assessmentList = data;
        })
    }
    $scope.getAllrespondent = function () {

        $http.get($rootScope.url + 'assessment/allrespondent').success(function (data) {
            console.log(data);
            $rootScope.respondentList = data;
        })
    }

    $scope.memberscales = function () {
        $window.location.href = $rootScope.url + 'assessment/scaleform&aid=' + aid;
    }
    $scope.setAge = function () {

        $rootScope.motherObj.dob = new Date($rootScope.motherObj.dob);
        var dobyear = $rootScope.motherObj.dob.getFullYear();
        if ($rootScope.motherObj.dob !== null) {
            var curDate = new Date();
            var curyear = curDate.getFullYear();
            $rootScope.motherObj.age = (curyear - dobyear);
            //alert($rootScope.motherObj.age);
            return $rootScope.motherObj.age;
        }
        else {
            $rootScope.motherObj.age = null;
        }

    }
    $scope.handleClick = function () {

        if ($rootScope.motherObj.present_address) {
            $rootScope.motherObj.permanent_address = $rootScope.motherObj.present_address;
        }
        else {
            $rootScope.motherObj.permanent_address = angular.copy($rootScope.motherObj.permanent_address);
        }
    };
	




});