app.controller('BackupController',function($scope,$http,$window) {
	var url = '/bchads/web/index.php?r='
	$scope.Back = function(){ 
		$window.history.back();
	}
	$scope.Index = function(){
		$scope.AllBackups = [];
		$scope.BackUpTypes = ["full","incremental"]
		$scope.ListError = false;
		$scope.newLocaltion = "";
		$scope.loadData = function(){
		    $http.get(url+'backup/getbackuphistory')
		    .then(function(response) {
		        $scope.AllBackups = response.data;
		        if($scope.AllBackups.length == 0) $scope.ListError = true;
		        else $scope.ListError = false;
		    });
		}
		$scope.loadData();
	    $scope.TakeBackUp = function(type){
		    $http.post(url+'backup/takebackup',{"type":type})
			.then(function(response) {
				if(response.data.status == 'success') {
					$(".alert-success").show();
					$scope.newLocaltion  = response.data.newLocaltion;
					$scope.loadData();
				}
				else {
					$(".alert-danger").show();
				}
		    });
	    }
	}
	$scope.Configuration = function(){
		$scope.BackupConfig = {};
		$scope.loadData = function(){
		    $http.get(url+'backup/getbackupconfig')
		    .then(function(response) {
		        $scope.BackupConfig = response.data;
		    });
		}
		$scope.loadData();
		$scope.Savedata = function(){
		    $http.post(url+'backup/savebackupconfig',{"data":$scope.BackupConfig})
			.then(function(response) {
				if(response.data.status == 'success') {
					$(".alert-success").show();
					$scope.loadData();
				}
				else {
					$(".alert-danger").show();
				}
		    });
		}
	}
});