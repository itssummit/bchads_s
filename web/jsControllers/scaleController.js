app.controller('ScaleController', function ($scope, $http, $rootScope, $window, $timeout) {


	$scope.search = {'type' : '' ,'respId' : 1};
	
    $scope.getAllScale = function () {
        $http.get($rootScope.url + 'assessment/allscales').success(function (data) {
            console.log(data);
            $scope.id = data.respondentId;
            $scope.assessmentId = data.assessmentId;
            $scope.assessmentType = data.assessmentType;
            $scope.respondentType = data.respondentType;
            $scope.scales = data.model;
        })
    }
    $scope.getAllScale();
    
    $scope.filterMatch = function(search) {
        return function( item ) {
        	if(search.type)
        	{
        		return item.type == search.type && item.respId == search.respId;
        	}
        	else
        	{
        		return item.respId == search.respId;
        	}
        };
     };

    $scope.getScores = function () {
        $http.get($rootScope.url + 'assessment/scores').success(function (data) {
            $scope.scores = data;
        })
    }
    $scope.getScores();

    $scope.scaleFun = function (scale) {
        var route = '/create';
        $window.location.href = $rootScope.url +'scale/selection&table='+ scale.table_name+'&display='+scale.display_sequence+'&respId='+scale.respId+'&type='+scale.type;
    }


});
