app.controller('AppController', function ($scope, $http, $window, $rootScope, $interval) {


    $scope.settingValue = false;
    $rootScope.url = '/bchads/web/index.php?r=';

    $scope.sync = {'progress' : false,'message' : null};
    
    //For Queue List Modal
    $rootScope.enableTimeout = false;
    //Initially Enable Search List Tag 
    $scope.showSearchLi = function () {
        $('#idSearchText').show();
    }

    //Toggle Search Text Box
    $scope.toggleSearch = function () {
        if ($scope.settingValue) {
            $window.location.href = $rootScope.url + 'site/index&patient=' + $scope.searchPatient;
        }
        else {
            $scope.searchInput = !$scope.searchInput;
        }
    }
    //Set Variable for Patient Name search
    $scope.setSearchPatient = function (event) {
        $scope.settingValue = true;
        if (event.keyCode == 13) {
            $scope.toggleSearch();
        }
    }

    $scope.startOfflineDataSync = function () {
        $http.get($rootScope.url + 'datasync/start').success(function (data) {
            if (data) alert('Offline data sync completed successfully');
            else alert('Opps, Looks like something went wrong!');
        });
    }

    $scope.startSynchronization = function(){
    	$scope.sync.progress =  true;
    	$scope.sync.message = null;
    	$http.get($rootScope.url+ 'site/syncprocess').success(function(data){
    		$scope.sync.progress = false;
    		$scope.sync.message = data;
    	}).error(function(data,status,headers,config){
    		$scope.sync.progress = false;
    		$scope.sync.message = 'Some Problem happened while Synchronizing with Server';
    	});
    }

});